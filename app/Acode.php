<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Acode extends Model {

    protected $table = 'tbl_acode';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'ref_type', 'ref_id','created_by', 'updated_by', 'is_active', 'comments'
    ];
    protected $dates = [
        'created_at',
        'updated_at'
        ];
   
    public static $CUSTOMER_REF = 'customer';
    public static $ACCOUNT_REF = 'account';
    

//    /**
//     * The attributes excluded from the model's JSON form.
//     *
//     * @var array
//     */
//    protected $hidden = [
//        
//    ];
}
