<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class AdvancePayment extends Model {
    
    protected $table = 'tbl_adv_payment' ;
    protected $fillable = ['is_active','customer_id','comments','date','amount','used_amount','balance_amount','prefix',
                            'payment_mode','reference_no','status','tra_category','voucher_type','voucher_no','created_by','updated_by',
                        'account','account_id','day_code','cheque_no'];
    protected $dates = ['created_at', 'updated_at'];
    //put your code here
}

?>

