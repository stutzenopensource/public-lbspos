<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Albumassetcomments extends Model {
    
    protected $table = 'tbl_album_asset_comments' ;
    protected $fillable = ['id', 'album_id','album_code','album_asset_id','comments','is_active','created_by_name','updated_by_name','created_by','updated_by'];
    protected $dates = ['created_at', 'updated_at'];
    //put your code here
}
?>
