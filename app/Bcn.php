<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Purchaseinvoiceitem
 *
 * @author Deepa
 */
class Bcn extends Model  {
    //put your code here
     protected  $table ='tbl_bcn';
    protected  $fillable =['code','purchase_invoice_id','ref_id','product_id','product_name','product_sku','qty','selling_price',
                            'created_at','status'
                            ,'uom_id','uom_name','is_active','created_by','updated_at','updated_by' 
                            ,'qty_in_hand','sales_qty','purchase_price',
                            'mrp_price','discount_mode','discount_value','discount_amount'
                            ,'is_new','previous_bcn_id'];
    protected $dates = ['updated_at','created_at'];
}

?>
