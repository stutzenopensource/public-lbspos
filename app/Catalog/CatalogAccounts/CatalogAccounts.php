<?php
namespace App\Catalog\CatalogAccounts;
use Illuminate\Database\Eloquent\Model;

class CatalogAccounts extends Model {
    protected  $table = 'tbl_catelog_accounts';
    protected $fillable = ['accountId','accountName' ,'createdBy','updatedBy'
        ,'isActive'];
    protected $dates = ['created_at','updated_at'];
}

?>
