<?php
namespace App\Catalog\CatalogMedia;
use Illuminate\Database\Eloquent\Model;

class CatalogMedia extends Model {
    protected  $table = 'tbl_catelog_media';
    protected $fillable = ['mediaId','fileLoc','belongsTo','belongsToId','createdBy','updatedBy'
        ,'isActive'];
    protected $dates = ['created_at','updated_at'];
}

?>
