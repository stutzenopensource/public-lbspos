<?php
namespace App\Catalog\Category;
use Illuminate\Database\Eloquent\Model;

class Category extends Model {
    protected  $table = 'tbl_catalog_category';
    protected $primaryKey = 'categoryId';
    protected $fillable = ['categoryId','accountId','name','imageId','description'
         ,'isActive','createdBy','updatedBy','categoryPhoto'];
    protected $dates = ['created_at','updated_at'];
}

?>
