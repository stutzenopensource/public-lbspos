<?php
namespace App\Catalog\Category;
use Illuminate\Database\Eloquent\Model;

class CategoryMapping extends Model {
    protected  $table = 'tbl_categorymapping';
    protected $fillable = ['categorymappingId','itemId','categoryId','updatedBy','createdBy','isActive'];
    protected $dates = ['created_at','updated_at'];
}

?>
