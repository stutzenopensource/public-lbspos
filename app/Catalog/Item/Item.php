<?php

namespace App\Catalog\Item;

use Illuminate\Database\Eloquent\Model;

class Item extends Model {

    protected $table = 'tbl_catelog_item';
    protected $primaryKey = 'itemId';
    protected $fillable = ['itemId', 'accountId', 'categoryId', 'code', 'name'
        , 'itemPhoto', 'itemThumbnailPhoto'
        , 'description', 'weight', 'isActive', 'createdBy', 'updatedBy' ];
    protected $dates = ['created_at', 'updated_at'];

}

?>
