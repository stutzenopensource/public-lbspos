<?php

namespace App\Catalog\Price;

use Illuminate\Database\Eloquent\Model;

class Price extends Model {

    protected $table = 'tbl_catelog_price';
    protected $primaryKey = 'priceId';
    protected $fillable = ['priceId', 'itemId', 'desc', 'pricevalue', 'priceisprimary' 
        , 'isActive', 'createdBy', 'updatedBy'];
    protected $dates = ['created_at', 'updated_at'];

}

?>
