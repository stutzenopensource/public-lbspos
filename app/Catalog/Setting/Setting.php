<?php

namespace App\Catalog\Setting;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model {

    protected $table = 'tbl_catelog_setting';
    protected $primaryKey = 'settingId';
    protected $fillable = ['settingId', 'type', 'setting_key', 'value' 
        , 'isActive', 'createdBy', 'updatedBy'];
    protected $dates = ['created_at', 'updated_at'];

}

?>
