<?php

namespace App\Catalog\Tag;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model {

    protected $table = 'tbl_tag';
    protected $primaryKey = 'tagId';
    protected $fillable = ['tagId', 'name', 'tagPhoto', 'tag_icon', 'accountId', 'imageId'
        , 'isActive', 'createdBy', 'updatedBy'];
    protected $dates = ['created_at', 'updated_at'];

}

?>
