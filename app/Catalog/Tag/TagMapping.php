<?php
namespace App\Catalog\Tag;
use Illuminate\Database\Eloquent\Model;

class TagMapping extends Model {
    protected  $table = 'tbl_tagmapping';
    protected $fillable = ['tagmappingId','itemId','tagId','updatedBy','createdBy','isActive'];
    protected $dates = ['created_at','updated_at'];
}

?>
