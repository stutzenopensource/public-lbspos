<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App;
/**
 * Description of Category
 *
 * @author Stutzen Admin
 */
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Category extends Model{
    
    protected $table = 'tbl_category';
     
        protected $fillable = [
        'name','description' ];
      protected $dates = ['deleted_at'];
}
