<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DayLog
 *
 * @author Stutzen Admin
 */
namespace App;
use Illuminate\Database\Eloquent\Model;
class ClosingStock extends Model{
   protected $table = 'tbl_closing_stock' ;
    protected $fillable = ['day_code', 'product_id','sku','closing_stock_qty','opening_stock_qty','uom_id','created_by','updated_by','is_active','comments'];
    protected $dates = ['created_at', 'updated_at'];
}
