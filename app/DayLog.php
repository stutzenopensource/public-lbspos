<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DayLog
 *
 * @author Stutzen Admin
 */
namespace App;
use Illuminate\Database\Eloquent\Model;
class DayLog extends Model{
   protected $table = 'tbl_day_log' ;
    protected $fillable = ['total_amount', 'created_by','updated_by','is_active','comments' 
        ,'cash','credit','card','others','advance'];
    protected $dates = ['created_at', 'updated_at','start_on','end_on'];
}
