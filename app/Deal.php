<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ActivityLog
 *
 * @author Deepa
 */
class Deal extends Model {
    //put your code here
    protected  $table = 'tbl_deal';
    protected $fillable = ['name','stage_id','stage_name','type','amount','created_by','created_at','updated_by','updated_at','created_by_name','updated_by_name','is_active','contact_id','colsed_date','user_id','user_name','contact_name','next_follow_up'];
    protected $dates = ['created_at','updated_at','colsed_date','next_follow_up'];
}

?>
