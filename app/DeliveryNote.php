<?php
namespace app;
use Illuminate\Database\Eloquent\Model;
class DeliveryNote extends Model {
    //put your code here
    
    protected  $table ='tbl_delivery_note';
    protected  $fillable = ['customer_id','type','date','overall_qty','overall_mrp','overall_purchase_price','overall_selling_price',
        'is_active','comments','created_by','updated_by','ref_no'];
    protected $dates = ['created_at','updated_at'];
}

?>
