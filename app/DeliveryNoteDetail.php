<?php
namespace app;
use Illuminate\Database\Eloquent\Model;
class DeliveryNoteDetail extends Model {
    //put your code here
    
    protected  $table ='tbl_delivery_note_detail';
    protected  $fillable = ['customer_id','delivery_note_id','product_sku','product_id','product_name','uom_id','uom_name'
        ,'ref_id','ref_type','barcode','given_qty','received_qty','balance_qty','mrp','purchase_price','selling_price'
        ,'is_active','comments','created_by','updated_by'];
    protected $dates = ['created_at','updated_at'];
}

?>
