<?php
namespace app;
use Illuminate\Database\Eloquent\Model;
class DeliveryNoteHistory extends Model {
    //put your code here
    
    protected  $table ='tbl_delivery_note_history';
    protected  $fillable = ['delivery_note_id','in_delivery_note_id','in_delivery_note_detail_id','delivery_note_detail_id','product_id','qty','is_active','comments','created_by','updated_by'];
    protected $dates = ['created_at','updated_at'];
}

?>
