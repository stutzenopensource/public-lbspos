<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DayLog
 *
 * @author Stutzen Admin
 */
namespace App;
use Illuminate\Database\Eloquent\Model;
class Denomination extends Model{
   protected $table = 'tbl_denomination' ;
    protected $fillable = ['denomination_user_id', 'unit','count','amount','type','created_by','updated_by','is_active','comments','day_code'];
    protected $dates = ['created_at', 'updated_at'];
}
