<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DayLog
 *
 * @author Stutzen Admin
 */
namespace App;
use Illuminate\Database\Eloquent\Model;
class DenominationUser extends Model{
   protected $table = 'tbl_denomination_user' ;
    protected $fillable = ['user_id', 'day_code','created_by','updated_by','is_active','comments'];
    protected $dates = ['created_at', 'updated_at'];
}
