<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

Class Designation extends Model {

    protected $table = 'tbl_designation';
    protected $fillable = ['name', 'is_active', 'created_by', 'updated_by', 'comments', 'is_default'];
    protected $dates = ['created_at', 'updated_at'];

}
