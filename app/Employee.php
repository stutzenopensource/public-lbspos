<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

Class Employee extends Model {

    protected $table = 'tbl_employee';
    protected $fillable = ['name', 'mobile', 'email', 'dob', 'gender', 'employee_code', 'address', 'country_id', 'state_id', 'city_id', 'pincode', 'designation_id', 'is_active', 'created_by', 'updated_by', 'comments'];
    protected $dates = ['created_at', 'updated_at'];

}
