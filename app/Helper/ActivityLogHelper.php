<?php

namespace App\Helper;

use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\ActivityLog;
use DB;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AccountHelper
 *
 * @author Stuzten
 */
class ActivityLogHelper {

    public static $INVOICE_SAVE = 'invoice_save';
    public static $INVOICE_DELETE = 'invoice_delete';
    public static $INVOICE_MODIFY = 'invoice_modify';
    public static $INVOICE_PAYMENT_SAVE = 'invoice_payment_save';
    public static $INVOICE_PAYMENT_DELETE = 'invoice_payment_delete';
    public static $PURCHASE_INVOICE_SAVE = 'purchase_invoice_save';
    public static $PURCHASE_INVOICE_DELETE = 'purchase_invoice_delete';
    public static $PURCHASE_INVOICE_MODIFY = 'purchase_invoice_modify';
    public static $PURCHASE_INVOICE_PAYMENT_SAVE = 'purchase_invoice_payment_save';
    public static $PURCHASE_INVOICE_PAYMENT_DELETE = 'purchase_invoice_payment_delete';
    public static $TREANSACTION_DIRECT_INCOME_SAVE = 'transaction_direct_income_save';
    public static $TREANSACTION_DIRECT_INCOME_DELETE = 'transaction_direct_income_delete';
    public static $TREANSACTION_DIRECT_EXPENSE_SAVE = 'transaction_direct_expense_save';
    public static $TREANSACTION_DIRECT_EXPENSE_DELETE = 'transaction_direct_expense_delete';
    public static $TRANSFER_SAVE = 'transfer_save';
    public static $TRANSFER_DELETE = 'transfer_delete';
    public static $DEAL_SAVE = 'deal_save';
    public static $DEAL_UPDATE = 'deal_update';
    public static $DEAL_DELETE = 'deal_delete';
    public static $DEAL_Activity_Save_NOTE = 'deal_activity_note_save';
    public static $DEAL_Activity_Save_LOG = 'deal_activity_logActivity_save';
    public static $DEAL_Activity_UPDATE_NOTE = 'deal_activity_note_update';
    public static $DEAL_Activity_UPDATE_LOG = 'deal_activity_logActivity_update';
    public static $DEAL_Activity_DELETE_NOTE = 'deal_activity_note_delete';
    public static $DEAL_Activity_DELETE_LOG = 'deal_activity_logActivity_delete';
    public static $QUOTE_SAVE = 'quote_save';
    public static $QUOTE_DELETE = 'quote_delete';
    public static $QUOTE_MODIFY = 'quote_modify';
    public static $SALES_ORDER_SAVE = 'sales_order_save';
    public static $SALES_ORDER_DELETE = 'sales_order_delete';
    public static $SALES_ORDER_MODIFY = 'sales_order_modify';

    public static function salesOrderSave($sales) {
        $currentuser = Auth::user();
        $activitylogo = new ActivityLog;
        $activitylogo->date = Carbon::now();
        if (!empty($quote->deal_id)) {
            $activitylogo->type = ActivityLogHelper::$SALES_ORDER_SAVE;
            $activitylogo->description = "Sales Order Created For Deal #" . $sales->deal_id;
        } else {
            $activitylogo->type = ActivityLogHelper::$SALES_ORDER_SAVE;
            $activitylogo->description = "Sales Order Created #" . $sales->id;
        }         
        $activitylogo->ref_id = $sales->id;
        $activitylogo->amount = $sales->total_amount;
        $activitylogo->userid = $currentuser->id;
        $activitylogo->is_active = 1;
        $activitylogo->created_by = $currentuser->id;
        $activitylogo->updated_by = $currentuser->id;
        $activitylogo->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->save();
    }

    public static function salesOrderDelete($sales) {
        $currentuser = Auth::user();
        $activitylogo = new ActivityLog;
        $activitylogo->date = Carbon::now();
        if (!empty($quote->deal_id)) {
            $activitylogo->type = ActivityLogHelper::$SALES_ORDER_DELETE;
            $activitylogo->description = "Deal #" . $sales->deal_id . " Sales Order is Deleted";
        } else {
            $activitylogo->type = ActivityLogHelper::$SALES_ORDER_DELETE;
            $activitylogo->description = "Sales Order Deleted #" . $sales->id;
        }
        $activitylogo->ref_id = $sales->id;
        $activitylogo->amount = $sales->total_amount;
        $activitylogo->userid = $currentuser->id;
        $activitylogo->is_active = 1;
        $activitylogo->created_by = $currentuser->id;
        $activitylogo->updated_by = $currentuser->id;
        $activitylogo->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->save();
    }

    public static function salesOrderModify($sales) {
        $currentuser = Auth::user();
        $activitylogo = new ActivityLog;
        $activitylogo->date = Carbon::now();
        if (!empty($quote->deal_id)) {
            $activitylogo->type = ActivityLogHelper::$QUOTE_MODIFY;
            $activitylogo->description = "Deal #" . $sales->deal_id . " Sales Order is Updated";
        } else {
            $activitylogo->type = ActivityLogHelper::$QUOTE_MODIFY;
            $activitylogo->description = "Sales Order Updated #" . $sales->id;
        }         
        $activitylogo->ref_id = $sales->id;
        $activitylogo->amount = $sales->total_amount;
        $activitylogo->userid = $currentuser->id;
        $activitylogo->is_active = 1;
        $activitylogo->created_by = $currentuser->id;
        $activitylogo->updated_by = $currentuser->id;
        $activitylogo->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->save();
    }

    public static function quoteSave($quote) {
        $currentuser = Auth::user();
        $activitylogo = new ActivityLog;
        $activitylogo->date = Carbon::now();
        if (!empty($quote->deal_id)) {
            $activitylogo->type = ActivityLogHelper::$QUOTE_SAVE;
            $activitylogo->description = "Quote Created For Deal #" . $quote->deal_id;
        } else {
            $activitylogo->type = ActivityLogHelper::$INVOICE_SAVE;
            $activitylogo->description = "Quote Created #" . $quote->id;
        }
        $activitylogo->ref_id = $quote->id;
        $activitylogo->amount = $quote->total_amount;
        $activitylogo->userid = $currentuser->id;
        $activitylogo->is_active = 1;
        $activitylogo->created_by = $currentuser->id;
        $activitylogo->updated_by = $currentuser->id;
        $activitylogo->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->save();
    }

    public static function quoteDelete($quote) {
        $currentuser = Auth::user();
        $activitylogo = new ActivityLog;
        $activitylogo->date = Carbon::now();
        if (!empty($quote->deal_id)) {
            $activitylogo->type = ActivityLogHelper::$QUOTE_DELETE;
            $activitylogo->description = "Deal #" . $quote->deal_id . " Quote is Deleted";
        } else {
            $activitylogo->type = ActivityLogHelper::$QUOTE_DELETE;
            $activitylogo->description = "Quote Deleted #" . $quote->id;
        }

        $activitylogo->ref_id = $quote->id;
        $activitylogo->amount = $quote->total_amount;
        $activitylogo->userid = $currentuser->id;
        $activitylogo->is_active = 1;
        $activitylogo->created_by = $currentuser->id;
        $activitylogo->updated_by = $currentuser->id;
        $activitylogo->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->save();
    }

    public static function quoteModify($quote) {
        $currentuser = Auth::user();
        $activitylogo = new ActivityLog;
        $activitylogo->date = Carbon::now();
        if (!empty($quote->deal_id)) {
            $activitylogo->type = ActivityLogHelper::$QUOTE_MODIFY;
            $activitylogo->description = "Deal #" . $quote->deal_id . " Quote is Updated";
        } else {
            $activitylogo->type = ActivityLogHelper::$QUOTE_MODIFY;
            $activitylogo->description = "Quote Updated #" . $quote->id;
        }
        $activitylogo->ref_id = $quote->id;
        $activitylogo->amount = $quote->total_amount;
        $activitylogo->userid = $currentuser->id;
        $activitylogo->is_active = 1;
        $activitylogo->created_by = $currentuser->id;
        $activitylogo->updated_by = $currentuser->id;
        $activitylogo->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->save();
    }

    public static function dealActivityDelete($deal) {
        $currentuser = Auth::user();
        $activitylogo = new ActivityLog;
        $activitylogo->date = Carbon::now();
        if ($deal->type == 'note') {
            $activitylogo->type = ActivityLogHelper::$DEAL_Activity_DELETE_NOTE;
            $activitylogo->description = "Deal Activity Deleted For Note #" . $deal->deal_id;
        } else {
            $activitylogo->type = ActivityLogHelper::$DEAL_Activity_DELETE_LOG;
            if ($deal->log_activity_type == 'meeting')
                $activitylogo->description = $deal->log_activity_type . " arranged For Deal Activity  #" . $deal->deal_id . " Deleted";
            else if ($deal->log_activity_type == 'call')
                $activitylogo->description = $deal->log_activity_type . " made For Deal Activity  #" . $deal->deal_id . " Deleted";
            else if ($deal->log_activity_type == 'mail')
                $activitylogo->description = $deal->log_activity_type . " Send For Deal Activity  #" . $deal->deal_id . " Deleted";
        }
        $activitylogo->ref_id = $deal->id;
        $activitylogo->userid = $currentuser->id;
        $activitylogo->is_active = 1;
        $activitylogo->created_by = $currentuser->id;
        $activitylogo->updated_by = $currentuser->id;
        $activitylogo->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->save();
    }

    public static function dealActivityUpdate($deal) {
        $currentuser = Auth::user();
        $activitylogo = new ActivityLog;
        $activitylogo->date = Carbon::now();
        if ($deal->type == 'note') {
            $activitylogo->type = ActivityLogHelper::$DEAL_Activity_UPDATE_NOTE;
            $activitylogo->description = "Deal Activity Updated For Note #" . $deal->deal_id;
        } else {
            $activitylogo->type = ActivityLogHelper::$DEAL_Activity_UPDATE_LOG;
            if ($deal->log_activity_type == 'meeting')
                $activitylogo->description = $deal->log_activity_type . " arranged For Deal Activity  #" . $deal->deal_id . " Updated";
            else if ($deal->log_activity_type == 'call')
                $activitylogo->description = $deal->log_activity_type . " made For Deal Activity  #" . $deal->deal_id . " Updated";
            else if ($deal->log_activity_type == 'mail')
                $activitylogo->description = $deal->log_activity_type . " Send For Deal Activity  #" . $deal->deal_id . " Updated";
        }
        $activitylogo->ref_id = $deal->id;
        $activitylogo->userid = $currentuser->id;
        $activitylogo->is_active = 1;
        $activitylogo->created_by = $currentuser->id;
        $activitylogo->updated_by = $currentuser->id;
        $activitylogo->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->save();
    }

    public static function dealActivitySave($deal) {
        $currentuser = Auth::user();
        $activitylogo = new ActivityLog;
        $activitylogo->date = Carbon::now();
        if ($deal->type == 'note') {
            $activitylogo->type = ActivityLogHelper::$DEAL_Activity_Save_NOTE;
            $activitylogo->description = "Deal Activity Created For Note #" . $deal->deal_id;
        } else {
            $activitylogo->type = ActivityLogHelper::$DEAL_Activity_Save_LOG;
            if ($deal->log_activity_type == 'meeting')
                $activitylogo->description = $deal->log_activity_type . " arranged For Deal Activity  #" . $deal->deal_id . " Created";
            else if ($deal->log_activity_type == 'call')
                $activitylogo->description = $deal->log_activity_type . " made For Deal Activity  #" . $deal->deal_id . " Created";
            else if ($deal->log_activity_type == 'mail')
                $activitylogo->description = $deal->log_activity_type . " Send For Deal Activity  #" . $deal->deal_id . " Created";
        }
        $activitylogo->ref_id = $deal->id;
        $activitylogo->userid = $currentuser->id;
        $activitylogo->is_active = 1;
        $activitylogo->created_by = $currentuser->id;
        $activitylogo->updated_by = $currentuser->id;
        $activitylogo->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->save();
    }

    public static function dealSave($deal) {
        $currentuser = Auth::user();
        $activitylogo = new ActivityLog;
        $activitylogo->date = Carbon::now();
        $activitylogo->type = ActivityLogHelper::$DEAL_SAVE;
        $activitylogo->description = "Deal Created #" . $deal->id;
        $activitylogo->ref_id = $deal->id;
        $activitylogo->amount = $deal->amount;
        $activitylogo->userid = $currentuser->id;
        $activitylogo->is_active = 1;
        $activitylogo->created_by = $currentuser->id;
        $activitylogo->updated_by = $currentuser->id;
        $activitylogo->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->save();
    }

    public static function dealUpdate($deal) {
        $currentuser = Auth::user();
        $activitylogo = new ActivityLog;
        $activitylogo->date = Carbon::now();
        $activitylogo->type = ActivityLogHelper::$DEAL_UPDATE;
        $activitylogo->description = "Deal Updated #" . $deal->id;
        $activitylogo->ref_id = $deal->id;
        $activitylogo->amount = $deal->amount;
        $activitylogo->userid = $currentuser->id;
        $activitylogo->is_active = 1;
        $activitylogo->created_by = $currentuser->id;
        $activitylogo->updated_by = $currentuser->id;
        $activitylogo->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->save();
    }

    public static function dealDelete($deal) {
        $currentuser = Auth::user();
        $activitylogo = new ActivityLog;
        $activitylogo->date = Carbon::now();
        $activitylogo->type = ActivityLogHelper::$DEAL_DELETE;
        $activitylogo->description = "Deal Deleted #" . $deal->id;
        $activitylogo->ref_id = $deal->id;
        $activitylogo->amount = $deal->amount;
        $activitylogo->userid = $currentuser->id;
        $activitylogo->is_active = 1;
        $activitylogo->created_by = $currentuser->id;
        $activitylogo->updated_by = $currentuser->id;
        $activitylogo->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->save();
    }

    public static function invoiceSave($invoice) {
        $currentuser = Auth::user();
        $activitylogo = new ActivityLog;
        $activitylogo->date = Carbon::now();
        if (!empty($invoice->deal_id)) {
            $activitylogo->type = ActivityLogHelper::$INVOICE_SAVE;
            $activitylogo->description = "Invoice Created For Deal #" . $invoice->deal_id;
        } else {
            $activitylogo->type = ActivityLogHelper::$INVOICE_SAVE;
            $activitylogo->description = "Invoice Created #" . $invoice->invoice_code;
        }
        $activitylogo->ref_id = $invoice->id;
        $activitylogo->amount = $invoice->total_amount;
        $activitylogo->userid = $currentuser->id;
        $activitylogo->is_active = 1;
        $activitylogo->created_by = $currentuser->id;
        $activitylogo->updated_by = $currentuser->id;
        $activitylogo->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->save();
    }

    public static function invoiceDelete($invoice) {
        $currentuser = Auth::user();
        $activitylogo = new ActivityLog;
        $activitylogo->date = Carbon::now();
        if (!empty($invoice->deal_id)) {
            $activitylogo->type = ActivityLogHelper::$INVOICE_DELETE;
            $activitylogo->description = "Deal #" . $invoice->deal_id . " Invoice is Deleted";
        } else {
            $activitylogo->type = ActivityLogHelper::$INVOICE_DELETE;
            $activitylogo->description = "Invoice Deleted #" . $invoice->invoice_code;
        }

        $activitylogo->ref_id = $invoice->id;
        $activitylogo->amount = $invoice->total_amount;
        $activitylogo->userid = $currentuser->id;
        $activitylogo->is_active = 1;
        $activitylogo->created_by = $currentuser->id;
        $activitylogo->updated_by = $currentuser->id;
        $activitylogo->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->save();
    }

    public static function invoiceModify($invoice) {
        $currentuser = Auth::user();
        $activitylogo = new ActivityLog;
        $activitylogo->date = Carbon::now();
        if (!empty($invoice->deal_id)) {
            $activitylogo->type = ActivityLogHelper::$INVOICE_MODIFY;
            $activitylogo->description = "Deal #" . $invoice->deal_id . " Invoice is Updated";
        } else {
            $activitylogo->type = ActivityLogHelper::$INVOICE_MODIFY;
            $activitylogo->description = "Invoice Updated #" . $invoice->invoice_code;
        }
        $activitylogo->ref_id = $invoice->id;
        $activitylogo->amount = $invoice->total_amount;
        $activitylogo->userid = $currentuser->id;
        $activitylogo->is_active = 1;
        $activitylogo->created_by = $currentuser->id;
        $activitylogo->updated_by = $currentuser->id;
        $activitylogo->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->save();
    }

    public static function purchaseinvoiceSave($invoice) {
        $currentuser = Auth::user();
        $activitylogo = new ActivityLog;
        $activitylogo->date = Carbon::now();
        $activitylogo->type = ActivityLogHelper::$PURCHASE_INVOICE_SAVE;
        $activitylogo->description = "Purchase Invoice Created #" . $invoice->id;
        $activitylogo->ref_id = $invoice->id;
        $activitylogo->amount = $invoice->total_amount;
        $activitylogo->userid = $currentuser->id;
        $activitylogo->is_active = 1;
        $activitylogo->created_by = $currentuser->id;
        $activitylogo->updated_by = $currentuser->id;
        $activitylogo->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->save();
    }

    public static function purchaseinvoiceDelete($invoice) {
        $currentuser = Auth::user();
        $activitylogo = new ActivityLog;
        $activitylogo->date = Carbon::now();
        $activitylogo->type = ActivityLogHelper::$PURCHASE_INVOICE_DELETE;
        $activitylogo->description = "Purchase Invoice Deleted #" . $invoice->id;
        $activitylogo->ref_id = $invoice->id;
        $activitylogo->amount = $invoice->total_amount;
        $activitylogo->userid = $currentuser->id;
        $activitylogo->is_active = 1;
        $activitylogo->created_by = $currentuser->id;
        $activitylogo->updated_by = $currentuser->id;
        $activitylogo->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->save();
    }

    public static function purchaseinvoiceModify($invoice) {
        $currentuser = Auth::user();
        $activitylogo = new ActivityLog;
        $activitylogo->date = Carbon::now();
        $activitylogo->type = ActivityLogHelper::$PURCHASE_INVOICE_MODIFY;
        $activitylogo->description = "Purchase Invoice Updated #" . $invoice->id;
        $activitylogo->ref_id = $invoice->id;
        $activitylogo->amount = $invoice->total_amount;
        $activitylogo->userid = $currentuser->id;
        $activitylogo->is_active = 1;
        $activitylogo->created_by = $currentuser->id;
        $activitylogo->updated_by = $currentuser->id;
        $activitylogo->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->save();
    }

    public static function invoicePaymentSave($invoice, $payment) {
        $currentuser = Auth::user();
        $activitylogo = new ActivityLog;
        $activitylogo->date = Carbon::now();
        $activitylogo->type = ActivityLogHelper::$INVOICE_PAYMENT_SAVE;
        $activitylogo->description = "Payment received for invoice #" . $invoice->invoice_code;
        $activitylogo->ref_id = $payment->id;
        $activitylogo->amount = $payment->amount;
        $activitylogo->userid = $currentuser->id;
        $activitylogo->is_active = 1;
        $activitylogo->created_by = $currentuser->id;
        $activitylogo->updated_by = $currentuser->id;
        $activitylogo->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->save();
    }

    public static function invoicePaymentDelete($invoice, $payment) {
        $currentuser = Auth::user();
        $activitylogo = new ActivityLog;
        $activitylogo->date = Carbon::now();
        $activitylogo->type = ActivityLogHelper::$INVOICE_PAYMENT_DELETE;
        $activitylogo->description = "Payment deleted for invoice #" . $invoice->invoice_code;
        $activitylogo->ref_id = $payment->id;
        $activitylogo->amount = $payment->amount;
        $activitylogo->userid = $currentuser->id;
        $activitylogo->is_active = 1;
        $activitylogo->created_by = $currentuser->id;
        $activitylogo->updated_by = $currentuser->id;
        $activitylogo->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->save();
    }

    public static function purchaseinvoicePaymentSave($invoice, $payment) {
        $currentuser = Auth::user();
        $activitylogo = new ActivityLog;
        $activitylogo->date = Carbon::now();
        $activitylogo->type = ActivityLogHelper::$PURCHASE_INVOICE_PAYMENT_SAVE;
        $activitylogo->description = "Payment received for purchase invoice #" . $invoice->id;
        $activitylogo->ref_id = $payment->id;
        $activitylogo->amount = $payment->amount;
        $activitylogo->userid = $currentuser->id;
        $activitylogo->is_active = 1;
        $activitylogo->created_by = $currentuser->id;
        $activitylogo->updated_by = $currentuser->id;
        $activitylogo->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->save();
    }

    public static function purchaseinvoicePaymentDelete($invoice, $payment) {
        $currentuser = Auth::user();
        $activitylogo = new ActivityLog;
        $activitylogo->date = Carbon::now();
        $activitylogo->type = ActivityLogHelper::$PURCHASE_INVOICE_PAYMENT_DELETE;
        $activitylogo->description = "Payment deleted for purchase invoice #" . $invoice->id;
        $activitylogo->ref_id = $payment->id;
        $activitylogo->amount = $payment->amount;
        $activitylogo->userid = $currentuser->id;
        $activitylogo->is_active = 1;
        $activitylogo->created_by = $currentuser->id;
        $activitylogo->updated_by = $currentuser->id;
        $activitylogo->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->save();
    }

    public static function transactionDirectExpenseSave($transaction) {
        $currentuser = Auth::user();
        $activitylogo = new ActivityLog;
        $activitylogo->date = Carbon::now();
        $activitylogo->type = ActivityLogHelper::$TREANSACTION_DIRECT_EXPENSE_SAVE;
        $activitylogo->description = "Transaction Direct Expense Created #" . $transaction->id;
        $activitylogo->ref_id = $transaction->id;
        $activitylogo->amount = $transaction->debit;
        $activitylogo->userid = $currentuser->id;
        $activitylogo->is_active = 1;
        $activitylogo->created_by = $currentuser->id;
        $activitylogo->updated_by = $currentuser->id;
        $activitylogo->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->save();
    }

    public static function transactionDirectIncomeSave($transaction) {
        $currentuser = Auth::user();
        $activitylogo = new ActivityLog;
        $activitylogo->date = Carbon::now();
        $activitylogo->type = ActivityLogHelper::$TREANSACTION_DIRECT_INCOME_SAVE;
        $activitylogo->description = "Transaction Direct Income Created #" . $transaction->id;
        $activitylogo->ref_id = $transaction->id;
        $activitylogo->amount = $transaction->credit;
        $activitylogo->userid = $currentuser->id;
        $activitylogo->is_active = 1;
        $activitylogo->created_by = $currentuser->id;
        $activitylogo->updated_by = $currentuser->id;
        $activitylogo->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->save();
    }

    public static function transactionDirectIncomeDelete($transaction) {
        $currentuser = Auth::user();
        $activitylogo = new ActivityLog;
        $activitylogo->date = Carbon::now();
        $activitylogo->type = ActivityLogHelper::$TREANSACTION_DIRECT_INCOME_DELETE;
        $activitylogo->description = "Transaction Direct Income Deleted #" . $transaction->id;
        $activitylogo->ref_id = $transaction->id;
        $activitylogo->amount = $transaction->credit;
        $activitylogo->userid = $currentuser->id;
        $activitylogo->is_active = 1;
        $activitylogo->created_by = $currentuser->id;
        $activitylogo->updated_by = $currentuser->id;
        $activitylogo->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->save();
    }

    public static function transactionDirectExpenseDelete($transaction) {
        $currentuser = Auth::user();
        $activitylogo = new ActivityLog;
        $activitylogo->date = Carbon::now();
        $activitylogo->type = ActivityLogHelper::$TREANSACTION_DIRECT_EXPENSE_DELETE;
        $activitylogo->description = "Transaction Direct Expense Deleted #" . $transaction->id;
        $activitylogo->ref_id = $transaction->id;
        $activitylogo->amount = $transaction->credit;
        $activitylogo->userid = $currentuser->id;
        $activitylogo->is_active = 1;
        $activitylogo->created_by = $currentuser->id;
        $activitylogo->updated_by = $currentuser->id;
        $activitylogo->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->save();
    }

    public static function transferSave($transaction) {
        $currentuser = Auth::user();
        $activitylogo = new ActivityLog;
        $activitylogo->date = Carbon::now();
        $activitylogo->type = ActivityLogHelper::$TRANSFER_SAVE;
        $activitylogo->description = "Account Transfer Save #" . $transaction->id;
        $activitylogo->ref_id = $transaction->id;
        $activitylogo->amount = $transaction->credit;
        $activitylogo->userid = $currentuser->id;
        $activitylogo->is_active = 1;
        $activitylogo->created_by = $currentuser->id;
        $activitylogo->updated_by = $currentuser->id;
        $activitylogo->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->save();
    }

    public static function transferDelete($transaction) {
        $currentuser = Auth::user();
        $activitylogo = new ActivityLog;
        $activitylogo->date = Carbon::now();
        $activitylogo->type = ActivityLogHelper::$TRANSFER_DELETE;
        $activitylogo->description = "Account Transfer Delete #" . $transaction->id;
        $activitylogo->ref_id = $transaction->id;
        $activitylogo->amount = $transaction->credit;
        $activitylogo->userid = $currentuser->id;
        $activitylogo->is_active = 1;
        $activitylogo->created_by = $currentuser->id;
        $activitylogo->updated_by = $currentuser->id;
        $activitylogo->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $activitylogo->save();
    }

}

?>
