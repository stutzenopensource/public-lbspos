<?php

namespace App\Helper;

use App\Account;
use App\Acode;
use App\Invoice;
use App\Inventory;
use App\InventoryAdjustment;
use Carbon\Carbon;
use App\Product;
use App\Invoiceitem;
use App\InvoiceItemTax;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Purchaseinvoiceitem;
use App\Invoicetax;
use App\Bcn;
use App\Purchaseinvoicetax;
use App\StockAdjustmentDetail;
use App\DeliveryNoteDetail;
use App\Helper\NotificationHelper;
use App\SalesOrderItemTax;
use App\QuoteItemTax;
use App\InvoiceReturn;
use App\DeliveryNote;
use App\StockAdjustment;
use App\Purchaseinvoice;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Helper
 *
 * @author Deepa
 */
class GeneralHelper {

    //put your code here

    public static function getDateFormat() {
        $dateFormat = '';
        $get_currency_format = DB::table("tbl_app_config")
                ->where('setting', '=', 'date_format')
                ->first();
        $dateFormat = $get_currency_format->value;
        return $dateFormat;
    }

    public static function formatDate($date, $format) {


        if (empty($date) || empty($format)) {

            return $date;
        }
        $new_format = str_replace("YYYY", "Y", $format);
        $new_format = str_replace("yyyy", "y", $new_format);
        $new_format = str_replace("MM", "M", $new_format);
        $new_format = str_replace("mm", "m", $new_format);
        $new_format = str_replace("DD", "D", $new_format);
        $new_format = str_replace("dd", "d", $new_format);

        $new_date = date_create($date);

        return date_format($new_date, $new_format);
    }

    public static function setCurrency() {
        $get_currency_format = DB::table("tbl_app_config")
                ->where('setting', '=', 'currency')
                ->first();
        $symbol = $get_currency_format->value;
        return $symbol;
    }

    public static function getCurrencySymbol() {
        $get_currency_format = DB::table("tbl_app_config")
                ->where('setting', '=', 'currency')
                ->first();
        $symbol = '';
        if (!empty($get_currency_format)) {
            $symbol = $get_currency_format->value;
        }

        return "( " . $symbol . ". " . ")";
    }

    public static function setSignature() {
        $get_currency_format = DB::table("tbl_app_config")
                ->where('setting', '=', 'signature')
                ->first();
        $sign = $get_currency_format->value;
        return $sign;
    }

    public static function setDateFormat($date) {
        $get_date_format = DB::table("tbl_app_config")
                ->where('setting', '=', 'date_format')
                ->first();
        $symbol = $get_date_format->value;
        return $date;
    }

    public static function getThosandSeperatorFormat() {
        $get_thousand_format = DB::table("tbl_app_config")
                ->where('setting', '=', 'thousand_seperator')
                ->first();
        $thous_sep = 'dd,dd,ddd';
        if (!empty($get_thousand_format)) {
            $thous_sep = $get_thousand_format->value;
        }
        return $thous_sep;
    }

    public static function convertNumberFormat($number, $thous_sep) {

        $number = number_format((float) $number, 2, '.', '');
        list($whole, $decimal) = explode('.', $number);
        $number = (float) $number;
        if ($thous_sep == 'd,dddd,dddd') {
            $number = preg_replace("/\B(?=(?:\d{4})+(?!\d))/", ",", $whole);
            $number = $number . "." . $decimal;
        } else if ($thous_sep == 'd dddd dddd') {
            $number = preg_replace("/\B(?=(?:\d{4})+(?!\d))/", " ", $whole);
            $number = $number . "." . $decimal;
        } else if ($thous_sep == 'dd,dd,ddd') {
            //$number = preg_replace("/\B(?=(?:\d{4})+(?!\d))/",",",$whole);
            if (strlen($whole) < 4) {
                $number = $whole;
            } else {
                $tail = substr($whole, -3);
                $head = substr($whole, 0, -3);
                $head = preg_replace("/\B(?=(?:\d{2})+(?!\d))/", ",", $head);
                $whole = $head . "," . $tail;
            }
            $number = $whole . "." . $decimal;
        } else if ($thous_sep == 'dd ddd ddd') {
            $number = preg_replace("/\B(?=(?:\d{3})+(?!\d))/", " ", $whole);
            $number = $number . "." . $decimal;
        } else if ($thous_sep == 'dd,ddd,ddd') {
            $number = preg_replace("/\B(?=(?:\d{3})+(?!\d))/", ",", $whole);
            $number = $number . "." . $decimal;
        }
        return $number;
    }

    public static function StockAdjustmentSave($request, $code, $id,$date) {
        $inventory = new Inventory;
        if ($code == 'purchase' || $code == 'update' || $code == 'invoice_return' || $code == 'purchase_return_delete' || $code == 'delivery_note') {
            $productArray = $request;
        } else {
            $productArray = $request->product;
        }
        $currentuser = Auth::user();
        $coArray = array();
        foreach ($productArray as $product) {

            $inventory_adu = new InventoryAdjustment;
            $inventory_adu->product_id = $product['product_id'];

            if ($product['product_id'] != '') {
                $get_product_det = DB::table('tbl_product')
                                ->select('has_inventory', 'is_active')
                                ->where('has_inventory', '=', '1')
                                ->where('is_active', '=', '1')
                                ->where('id', '=', $product['product_id'])->get();
                if ($get_product_det->count() >= 1) {
                    if ($code == 'adjust')
                        $product_id = $product['product_id'];
                    if ($product['product_id'] != '')
                        $product_id = $product['product_id'];

                    if ($id) {
                        $inventory_adu->ref_id = $id;
                        $pur_id = $id;
                    }if ($code == 'purchase')
                        $inventory_adu->type = "purchase_create";
                    else if ($code == 'update')
                        $inventory_adu->type = "purchase_update";
                    else if ($code == 'invoice_return')
                        $inventory_adu->type = "invoice_return";
                    else if ($code == 'purchase_return_delete')
                        $inventory_adu->type = "purchase_return_delete";
                    else if ($code == 'delivery_note')
                        $inventory_adu->type = "delivery_note_create";
                    else {
                        $inventory_adu->type = 'stock_adjust_create';
                        $pur_id = $product['purchase_invoice_id'];
                    }
                    $inventotyCollection = Inventory::where('product_id', "=", $product['product_id'])->get();

                    $get_uom_name = DB::table('tbl_uom')
                                    ->select('name')
                                    ->where('id', '=', $product['uom_id'])->get();

                    $inventotyCollection = $inventotyCollection->first();
                    if ($code == 'purchase_return_delete') {
                        $item = DB::table('tbl_purchase_invoice_item_return as pi')->leftjoin('tbl_product as p', 'p.id', '=', 'pi.product_id')->select('pi.purchase_invoice_item_id as id', 'pi.product_id')->where('purchase_invoice_return_id', '=', $id)->where('p.has_inventory', '=', '1')
                                        ->where('p.is_active', '=', 1)
                                        ->where('p.id', '=', $product['product_id'])
                                        ->orderBy('pi.id')
                                        ->get();
                    } else {
                        $item = DB::table('tbl_purchase_invoice_item as pi')->leftjoin('tbl_product as p', 'p.id', '=', 'pi.product_id')->select('pi.id', 'pi.product_id')->where('purchase_invoice_id', '=', $pur_id)->where('p.has_inventory', '=', '1')
                                        ->where('p.is_active', '=', 1)
                                        ->where('p.id', '=', $product['product_id'])
                                        ->orderBy('pi.id')
                                        ->get();
                    }

                    $data1 = 0;
                    if ($inventory_adu->type == 'stock_adjust_create') {
                        if ($product['is_active'] == 0)
                            $data1 = 1;
                    }

                    if ($inventotyCollection != null && $data1 == 0) {

                        if ($code == 'delivery_note')
                            $product['qty'] = $product['given_qty'];

                        $total_stock = $inventotyCollection->quantity + $product['qty'];
                        $inventotyCollection->quantity = $total_stock;
                        $inventotyCollection->save();

                        $inventory_adu->sku = $product['product_sku'];
                        $inventory_adu->uom_id = $product['uom_id'];
                        if ($code == 'invoice')
                            $inventory_adu->comments = "From Invoice";
                        else if ($code == 'purchase' || $code == 'update')
                            $inventory_adu->comments = "From Purchase";
                        else if ($code == 'invoice_return')
                            $inventory_adu->comments = "From Sales invoice return";
                        else if ($code == 'purchase_return_delete')
                            $inventory_adu->comments = "From Purchase invoice return Delete";
                        else if ($inventory_adu->type == 'stock_adjust_create')
                            $inventory_adu->comments = "From Stock Adjustment";
                        else if ($inventory_adu->type == 'stock_wastage_create')
                            $inventory_adu->comments = "From Stock Wastage";
                        else if ($code == 'delivery_note')
                            $inventory_adu->comments = "From Delivery Note InWard";
                        else
                            $inventory_adu->comments = $product['comments'];
                        if ($get_uom_name->count() >= 1) {
                            $get_uom_name1 = $get_uom_name->first();
                            $inventory_adu->uom_name = $get_uom_name1->name;
                        } else
                            $inventory_adu->uom_name = "";
                        $inventory_adu->total_stock = $total_stock;
                        $inventory_adu->quantity_credit = $product['qty'];
                        $inventory_adu->created_by = $currentuser->id;
                        $inventory_adu->updated_by = $currentuser->id;
                        $inventory_adu->is_active = "1";
                        $inventory_adu->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
                        $inventory_adu->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
                        $type = GeneralHelper::checkConfiqSetting();
                        
                        if ($type == 'purchase based' && $inventory_adu->type != 'stock_adjust_create' && ($code != 'invoice_return')) {
                            foreach ($item as $i) {
                                $bcn_id = DB::table('tbl_bcn')->select('id','code')->where('ref_id', '=', $i->id)->where('ref_type', '=', $type)->first();
                                if (!in_array($i->id, $coArray)) {
                                    $inventory_adu->bcn_id = $bcn_id->id;
                                    $inventory_adu->barcode = $bcn_id->code;
                                    array_push($coArray, $i->id);
                                     break;
                                }
                            }
                        }

                        if ($type == 'purchase based') {

                            if ($code == 'invoice_return') {
                                $barcode= GeneralHelper::getBarCode( $product['purchase_invoice_item_id']);
                                $inventory_adu->bcn_id = $product['purchase_invoice_item_id'];  //this type purchaseInvoiceItemId is BcnID
                                $inventory_adu->barcode = $barcode;
                            }

                            if ($inventory_adu->type == 'stock_adjust_create') {
                                $bcn_id = DB::table('tbl_bcn')->select('id','code')->where('ref_id', '=', $product['purchase_invoice_item_id'])->where('ref_type', '=', $type)->first();
                                $inventory_adu->bcn_id = $bcn_id->id;
                                $inventory_adu->barcode = $bcn_id->code;
                                $data = array();
                                $data['qty'] = $product['qty'];
                                $data['purchase_invoice_item_id'] = $product['purchase_invoice_item_id'];
                                $data['bcn_id'] = $bcn_id->id;
                                GeneralHelper::StockInventoryIncrease($data);
                            } else if ($inventory_adu->type == 'delivery_note_create') {
                                $bcn_id = DB::table('tbl_bcn')->select('id','code')->where('ref_id', '=', $product['ref_id'])->where('ref_type', '=', $type)->first();
                                $inventory_adu->bcn_id = $bcn_id->id;
                                $inventory_adu->barcode = $bcn_id->code;
                                $data = array();
                                $data['qty'] = $product['qty'];
                                $data['purchase_invoice_item_id'] = $product['ref_id'];
                                $data['bcn_id'] = $bcn_id->id;
                                GeneralHelper::StockInventoryIncrease($data);
                            }
                        }

                        if ($type == 'product based') {
                            $bcn_id = DB::table('tbl_bcn')->select('id','code')->where('ref_id', '=', $product['product_id'])->where('ref_type', '=', $type)->first();
                            if (($bcn_id) != '') {
                                $inventory_adu->bcn_id = $bcn_id->id;
                                $inventory_adu->barcode = $bcn_id->code;
                            }
                            GeneralHelper::incrementBcnQty($product['product_id'], $product['qty']);
                        }
                        $inventory_adu->date = $date;
                        $inventory_adu->save();
                    }
                }
            }
        }
        return $inventory_adu->id;
    }

    public static function StockAdjustmentDelete($request, $code, $id) {
        $inventory = new Inventory;
        if ($code == 'pupdate' || $code == 'iupdate') {
            $productArray = $request;
        }

        $currentuser = Auth::user();
        $inventory_adu = new InventoryAdjustment;

        $date=null;
        if ($code == 'iupdate' || $code == 'idelete') {
            $invoiceItemCollection = Invoiceitem::where('invoice_id', "=", $id)
                    ->get();
            $data=Invoice::where('id',$id)->first();
            if($data!= '')
            $date=$data->date;
        } else if ($code == 'pupdate' || $code == 'pdelete') {
            $invoiceItemCollection = Purchaseinvoiceitem::where('purchase_invoice_id', "=", $id)
                    ->get();
            $data=Purchaseinvoice::where('id',$id)->first();
            if($data!= '')
            $date=$data->date;
        } else if ($code == 'item_delete') {
            $invoiceItemCollection = Purchaseinvoiceitem::where('id', "=", $id)
                    ->get();
            $purchaseList=$invoiceItemCollection->first();
            if($purchaseList != ''){
            $data=Purchaseinvoice::where('id',$purchaseList->purchase_invoice_id)->first();
            if($data!= '')
            $date=$data->date;
            }
        } else if ($code == 'invoice_return_delete') {
            $invoiceItemCollection = $request;
            $data=InvoiceReturn::where('id',$id)->first();
            if($data!= '')
            $date=$data->date;
        } else if ($code == 'stock_adjust' || $code == 'stock_wastage' || $code == 'stock_adjust_delete' || $code == 'stock_wastage_delete') {
            $invoiceItemCollection = StockAdjustmentDetail::where('stock_adjustment_id', "=", $id)
                    ->where('is_active', '=', 1)
                    ->get();
            $data=StockAdjustment::where('id',$id)->first();
            if($data!= '')
            $date=$data->date;
        } else if ($code == 'delivery_note_outWard_delete' || $code == 'delivery_note_inWard_delete' || $code=='delivery_note_inWard_delete_forUpdate'||$code=='delivery_note_outWard_delete_forUpdate') {
            $invoiceItemCollection = DeliveryNoteDetail::where('delivery_note_id', "=", $id)
                    ->where('is_active', '=', 1)
                    ->get();
            $data=DeliveryNote::where('id',$id)->first();
            if($data!= '')
            $date=$data->date;
        }


        //print_r($invoiceItemCollection);
        foreach ($invoiceItemCollection as $product) {
            $product_id = $product->product_id;
            if ($product_id != 0 || $product_id != '') {
                $get_product_det = DB::table('tbl_product')
                                ->select('has_inventory', 'is_active')
                                ->where('has_inventory', '=', '1')
                                ->where('is_active', '=', '1')
                                ->where('id', '=', $product_id)->get();
                if ($get_product_det->count() >= 1) {
                    $qty = $product->qty;

                    if ($code == 'delivery_note_outWard_delete' || $code == 'delivery_note_inWard_delete'|| $code=='delivery_note_inWard_delete_forUpdate'||$code=='delivery_note_outWard_delete_forUpdate')
                        $qty = $product->given_qty;

                    $get_inventory = Inventory::where('product_id', '=', $product_id)
                            ->first();
                    $type = GeneralHelper::checkConfiqSetting();
                    if ($code == 'iupdate' || $code == 'idelete') {
                        $get_inventory->quantity = $get_inventory->quantity + $qty;

                        if ($type == 'product based') {
                            GeneralHelper::incrementBcnQty($product_id, $qty);
                        }
                    } else if ($code == 'item_delete' || $code == 'pupdate' || $code == 'pdelete' || $code == 'invoice_return_delete') {
                        $get_inventory->quantity = $get_inventory->quantity - $qty;
                        $type = GeneralHelper::checkConfiqSetting();
                        if ($type == 'product based') {
                            GeneralHelper::decrementBcnQty($product_id, $qty);
                        }
                    } else {
                        if ($type == 'purchase based') {
                            if ($code == 'stock_adjust' || $code == 'stock_wastage' || $code == 'stock_wastage_delete'|| $code == 'stock_adjust_delete') {
                                $bcn_id = DB::table('tbl_bcn')->select('id','code')->where('ref_id', '=', $product->purchase_invoice_item_id)->where('ref_type', '=', $type)->first();
                                $inventory_adu->bcn_id = $bcn_id->id;
                                $inventory_adu->barcode = $bcn_id->code;
                                $data = array();
                                $data['qty'] = $product['qty'];
                                $data['purchase_invoice_item_id'] = $product->purchase_invoice_item_id;
                                $data['bcn_id'] = $bcn_id->id;
                                if ($code == 'stock_adjust' || $code == 'stock_adjust_delete') {
                                    GeneralHelper::StockInventoryDecrease($data);
                                    $get_inventory->quantity = $get_inventory->quantity - $qty;
                                } else if ($code == 'stock_wastage' || $code == 'stock_wastage_delete') {
                                    GeneralHelper::StockInventoryIncrease($data);
                                    $get_inventory->quantity = $get_inventory->quantity + $qty;
                                }
                            } else if ($code == 'delivery_note_outWard_delete' || $code == 'delivery_note_inWard_delete'|| $code=='delivery_note_inWard_delete_forUpdate'||$code=='delivery_note_outWard_delete_forUpdate') {
                                $bcn_id = DB::table('tbl_bcn')->select('id','code')->where('ref_id', '=', $product->ref_id)->where('ref_type', '=', $type)->first();
                                $inventory_adu->bcn_id = $bcn_id->id;
                                $inventory_adu->barcode = $bcn_id->code;
                                $data = array();
                                $data['qty'] = $qty;
                                $data['purchase_invoice_item_id'] = $product->ref_id;
                                $data['bcn_id'] = $bcn_id->id;
                                if ($code == 'delivery_note_outWard_delete'|| $code=='delivery_note_outWard_delete_forUpdate') {
                                    GeneralHelper::StockInventoryIncrease($data);
                                    $get_inventory->quantity = $get_inventory->quantity + $qty;
                                } else if ($code == 'delivery_note_inWard_delete'|| $code=='delivery_note_inWard_delete_forUpdate') {
                                    GeneralHelper::StockInventoryDecrease($data);
                                    $get_inventory->quantity = $get_inventory->quantity - $qty;
                                }
                            }else if($code == 'item_delete' || $code == 'pupdate' || $code == 'pdelete'){
                                $bcn_id = DB::table('tbl_bcn')->select('id')->where('ref_id', '=', $product->id)->where('ref_type', '=', $type)->first(); // $product->id is PurchaseInvoiceItemId
                                    $inventory_adu->bcn_id = $bcn_id->id;
                            }else if($code == 'iupdate' || $code == 'idelete' || $code == 'invoice_return_delete'){
                                    $inventory_adu->bcn_id = $product->purchase_invoice_item_id;        // This PurchaseInvoiceItemId is bcnId
                            }
                        } else if ($type == 'product based') {

                            if ($code == 'stock_adjust' ||$code == 'stock_adjust_delete' ) {
                                GeneralHelper::decrementBcnQty($product_id, $qty);
                                $get_inventory->quantity = $get_inventory->quantity - $qty;
                            } else if ($code == 'stock_wastage'||$code == 'stock_wastage_delete' ) {
                                GeneralHelper::incrementBcnQty($product_id, $qty);
                                $get_inventory->quantity = $get_inventory->quantity + $qty;
                            } else if ($code == 'delivery_note_outWard_delete'||$code=='delivery_note_outWard_delete_forUpdate') {
                                GeneralHelper::incrementBcnQty($product_id, $qty);
                                $get_inventory->quantity = $get_inventory->quantity + $qty;
                            } else if ($code == 'delivery_note_inWard_delete'|| $code=='delivery_note_inWard_delete_forUpdate') {
                                GeneralHelper::decrementBcnQty($product_id, $qty);
                                $get_inventory->quantity = $get_inventory->quantity - $qty;
                            }
                        }
                    }
                    $get_inventory->save();
                    if ($code == 'item_delete_update') {
                        $get_inventory_adjust = InventoryAdjustment::where('product_id', '=', $product_id)
                                ->where('ref_id', '=', $id)
                                ->where('is_active', '=', 1)
                                ->whereRaw(DB::raw("(type = 'purchase_create' OR type='purchase_update')"))
//                                ->where('type', '=', 'purchase_create')
//                                ->orWhere('type', '=', 'purchase_update')
                                ->first();
                        if ($get_inventory_adjust != '') {
                            $get_inventory_adjust->is_active = 0;
                            $get_inventory_adjust->save();
                        }
                    }
//                    else if ($code == 'stock_adjust') {
//                        $get_inventory_adjust = InventoryAdjustment::where('product_id', '=', $product_id)
//                                ->where('ref_id', '=', $id)
//                                ->whereRaw(DB::raw("(type = 'stock_adjust_create' OR type='stock_adjust_update')"))
//                                ->where('is_active', '=', 1)
//                                ->first();
//                        if ($get_inventory_adjust != '') {
//                            $get_inventory_adjust->is_active = '0';
//                            $get_inventory_adjust->save();
//                        }
//                    } 
//                    else if ($code == 'stock_wastage') {
//                        $get_inventory_adjust = InventoryAdjustment::where('product_id', '=', $product_id)
//                                ->where('ref_id', '=', $id)
//                                ->whereRaw(DB::raw("(type = 'stock_wastage')"))
//                                ->where('is_active', '=', 1)
//                                ->first();
//                        if ($get_inventory_adjust != '') {
//                            $get_inventory_adjust->is_active = '0';
//                            $get_inventory_adjust->save();
//                        }
//                    } 
//                    else if ($code == 'delivery_note_outWard_delete' || $code == 'delivery_note_inWard_delete') {
//                        $get_inventory_adjust = InventoryAdjustment::where('product_id', '=', $product_id)
//                                ->where('ref_id', '=', $id)
//                                ->whereRaw(DB::raw("(type = 'delivery_note_create')"))
//                                ->where('is_active', '=', 1)
//                                ->first();
//                        if ($get_inventory_adjust != '') {
//                            $get_inventory_adjust->is_active = '0';
//                            $get_inventory_adjust->save();
//                        }
//                    } 
                    else {
                        $inventory_adu = new InventoryAdjustment;
                        $inventory_adu->product_id = $product_id;

                        if ($type == 'product based'){
                            $bcn_id = DB::table('tbl_bcn')->select('id','code')->where('ref_id', '=', $product_id)->where('ref_type', '=', $type)->first();
                         $inventory_adu->bcn_id = $bcn_id->id;
                         $inventory_adu->barcode = $bcn_id->code;
                        }

                        $inventory_adu->ref_id = $id;
                        if ($code == 'iupdate' || $code == 'idelete') {
                            if ($type == 'purchase based')
                                $bcn_id = DB::table('tbl_bcn')->select('id','code')->where('id', '=', $product->purchase_invoice_item_id)->where('ref_type', '=', $type)->first();
                            else if ($type == 'product based')
                                $bcn_id = DB::table('tbl_bcn')->select('id','code')->where('ref_id', '=', $product_id)->where('ref_type', '=', $type)->first();
                            $inventory_adu->bcn_id = $bcn_id->id;
                            $inventory_adu->barcode = $bcn_id->code;
                            $inventory_adu->type = "invoice_delete";
                            if ($code == 'iupdate')
                                $inventory_adu->comments = "Invoice Delete For Update";
                            else
                                $inventory_adu->comments = "Invoice Delete";
                        } else if ($code == 'pupdate' || $code == 'pdelete' || $code=='item_delete') {
                            $inventory_adu->type = "purchase_delete";
                            if ($code == 'pupdate')
                                $inventory_adu->comments = "Purchase Delete For Update";
                            else
                                $inventory_adu->comments = "Purchase Delete";
                        } else if ($code == 'invoice_return_delete') {
                            $inventory_adu->type = "invoice_return_delete";
                            $inventory_adu->comments = "Invoice Return Delete";
                        } else if ($code == 'stock_adjust') {
                            $inventory_adu->type = "stock_adjust";
                            $inventory_adu->comments = "Stock Adjustment Delete For Update";
                        } else if ($code == 'stock_adjust_delete') {
                            $inventory_adu->type = "stock_adjust_delete";
                            $inventory_adu->comments = "Stock Adjustment Delete";
                        } else if ($code == 'stock_wastage_delete') {
                            $inventory_adu->type = "stock_wastage_delete";
                            $inventory_adu->comments = "Stock Wastage Delete";
                        } else if ($code == 'stock_wastage') {
                            $inventory_adu->type = "stock_wastage";
                            $inventory_adu->comments = "Stock Wastage Delete For Update";
                        }
                        else if ($code == 'delivery_note_outWard_delete') {
                            $inventory_adu->type = "delivery_note_outWard_delete";
                            $inventory_adu->comments = "Delivery Note Outward Delete";
                        }
                        else if ($code == 'delivery_note_inWard_delete') {
                            $inventory_adu->type = "delivery_note_inWard_delete";
                            $inventory_adu->comments = "Delivery Note Inward Delete";
                        }
                        else if ($code == 'delivery_note_outWard_delete_forUpdate') {
                            $inventory_adu->type = "delivery_note_outWard_delete";
                            $inventory_adu->comments = "Delivery Note Outward Delete For Update";
                        }
                        else if ($code == 'delivery_note_inWard_delete_forUpdate') {
                            $inventory_adu->type = "delivery_note_inWard_delete";
                            $inventory_adu->comments = "Delivery Note Inward Delete For Update";
                        }
                        $inventotyCollection = Inventory::where('product_id', "=", $product_id)->get();

                        $get_uom_name = DB::table('tbl_uom')
                                        ->select('name')
                                        ->where('id', '=', $product->uom_id)->get();

                        $inventotyCollection = $inventotyCollection->first();
                        $total_stock = $inventotyCollection->quantity;


                        $inventory_adu->sku = $product->product_sku;
                        $inventory_adu->uom_id = $product->uom_id;

                        if ($get_uom_name->count() >= 1) {
                            $get_uom_name1 = $get_uom_name->first();
                            $inventory_adu->uom_name = $get_uom_name1->name;
                        } else
                            $inventory_adu->uom_name = "";
                        $inventory_adu->total_stock = $total_stock;
                        if ($code == 'iupdate' || $code == 'idelete' || $code == 'invoice_return_delete' || $code == 'stock_wastage' || $code == 'stock_wastage_delete'||$code == 'delivery_note_outWard_delete'|| $code=='delivery_note_outWard_delete_forUpdate')
                            $inventory_adu->quantity_credit = $qty;
                        else if ($code == 'pupdate' || $code == 'pdelete' || $code=='item_delete')
                            $inventory_adu->quantity_debit = $qty;
                        else if ($code == 'stock_adjust' || $code == 'stock_adjust_delete'||$code == 'delivery_note_inWard_delete'|| $code=='delivery_note_inWard_delete_forUpdate')
                            $inventory_adu->quantity_debit = $qty;
                        
                        
                        if ($type == 'purchase based') {
                            if ($code == 'stock_adjust' || $code == 'stock_wastage' || $code == 'stock_wastage_delete'|| $code == 'stock_adjust_delete') {
                                $bcn_id = DB::table('tbl_bcn')->select('id','code')->where('ref_id', '=', $product->purchase_invoice_item_id)->where('ref_type', '=', $type)->first();
                                $inventory_adu->bcn_id = $bcn_id->id;
                                $inventory_adu->barcode = $bcn_id->code;
                            } else if ($code == 'delivery_note_outWard_delete' || $code == 'delivery_note_inWard_delete'|| $code=='delivery_note_inWard_delete_forUpdate'||$code=='delivery_note_outWard_delete_forUpdate') {
                                $bcn_id = DB::table('tbl_bcn')->select('id','code')->where('ref_id', '=', $product->ref_id)->where('ref_type', '=', $type)->first();
                                $inventory_adu->bcn_id = $bcn_id->id;
                                $inventory_adu->barcode = $bcn_id->code;
                            }
                            else if($code == 'item_delete' || $code == 'pupdate' || $code == 'pdelete'){
                                $bcn_id = DB::table('tbl_bcn')->select('id','code')->where('ref_id', '=', $product->id)->where('ref_type', '=', $type)->first(); // $product->id is PurchaseInvoiceItemId
                                    $inventory_adu->bcn_id = $bcn_id->id;
                                    $inventory_adu->barcode = $bcn_id->code;
                            }else if($code == 'iupdate' || $code == 'idelete' || $code == 'invoice_return_delete'){
                                $barcode= GeneralHelper::getBarCode($product->purchase_invoice_item_id);  
                                $inventory_adu->bcn_id = $product->purchase_invoice_item_id;        // This PurchaseInvoiceItemId is bcnId
                                $inventory_adu->barcode=$barcode;
                             }
                        } 
                        
                         $inventory_adu->date = $date;

                        $inventory_adu->created_by = $currentuser->id;
                        $inventory_adu->updated_by = $currentuser->id;
                        $inventory_adu->is_active = "1";
                        $inventory_adu->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
                        $inventory_adu->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
                        $inventory_adu->save();
                    }
                }
            }
        }
    }

    public static function StockWastageSave($request, $code, $id,$date) {
        $inventory = new Inventory;
        if ($code == 'invoice' || $code == 'update' || $code == 'purchase_return' || $code == 'delivery_note') {
            $productArray = $request;
        } else {
            $productArray = $request->product;
        }
        $currentuser = Auth::user();
        $coArray = array();
        foreach ($productArray as $product) {
            $inventory_adu = new InventoryAdjustment;
            $inventory_adu->product_id = $product['product_id'];

            if ($product['product_id'] != '') {
                $get_product_det = DB::table('tbl_product')
                                ->select('has_inventory', 'is_active')
                                ->where('has_inventory', '=', '1')
                                ->where('is_active', '=', '1')
                                ->where('id', '=', $product['product_id'])->get();
                if ($get_product_det->count() >= 1) {
                    if ($code == 'adjust')
                        $product_id = $product['product_id'];

                    if ($id) {
                        $inventory_adu->ref_id = $id;
                        $pur_id = $id;
                    }
                    if ($code == 'invoice')
                        $inventory_adu->type = "invoice_create";
                    else if ($code == 'update')
                        $inventory_adu->type = "invoice_update";
                    else if ($code == 'purchase_return')
                        $inventory_adu->type = "purchase_return";
                    else if ($code == 'delivery_note')
                        $inventory_adu->type = "delivery_note_create";
                    else {
                        $inventory_adu->type = "stock_wastage_create";
                        $pur_id = $product['purchase_invoice_id'];
                    }
                    $inventotyCollection = Inventory::where('product_id', "=", $product['product_id'])->get();

                    $get_uom_name = DB::table('tbl_uom')
                                    ->select('name')
                                    ->where('id', '=', $product['uom_id'])->get();
                    $inventotyCollection = $inventotyCollection->first();

                    if ($code == 'invoice' || $code == 'update') {
                        $barcode= GeneralHelper::getBarCode($product['purchase_invoice_item_id']);  
                        $inventory_adu->bcn_id = $product['purchase_invoice_item_id']; //this Type purchaseInvoiceItemId is bcnId
                         $inventory_adu->barcode=$barcode;
                    } else if ($code == 'purchase_return') {
                        $item = DB::table('tbl_purchase_invoice_item_return as pi')->leftjoin('tbl_product as p', 'p.id', '=', 'pi.product_id')->select('pi.purchase_invoice_item_id as id', 'pi.product_id')->where('purchase_invoice_return_id', '=', $pur_id)->where('p.has_inventory', '=', '1')
                                        ->where('p.is_active', '=', '1')
                                        ->where('p.id', '=', $product['product_id'])->get();
                    } else {
                        $item = DB::table('tbl_purchase_invoice_item as pi')->leftjoin('tbl_product as p', 'p.id', '=', 'pi.product_id')->select('pi.id', 'pi.product_id')->where('purchase_invoice_id', '=', $pur_id)->where('p.has_inventory', '=', '1')
                                        ->where('p.is_active', '=', '1')
                                        ->where('p.id', '=', $product['product_id'])->get();
                    }

                    $data1 = 0;
                    if ($inventory_adu->type == 'stock_wastage'  || $inventory_adu->type == 'stock_wastage_create') {
                        if ($product['is_active'] == 0)
                            $data1 = 1;
                    }
                    if ($inventotyCollection != null && $data1 == 0) {
                        if ($code == 'delivery_note')
                            $product['qty'] = $product['given_qty'];

                        $total_stock = $inventotyCollection->quantity - $product['qty'];
                        $inventotyCollection->quantity = $total_stock;
                        $inventotyCollection->save();

                        $inventory_adu->sku = $product['product_sku'];
                        $inventory_adu->uom_id = $product['uom_id'];
                        if ($code == 'invoice' || $code == 'update')
                            $inventory_adu->comments = "From Invoice";
                        else if ($code == 'purchase_return')
                            $inventory_adu->comments = "From Purchase invoice return";
                        else if ($inventory_adu->type == 'stock_wastage' || $inventory_adu->type == 'stock_wastage_create')
                            $inventory_adu->comments = "From Stock Wastage";
                        else if ($inventory_adu->type == 'delivery_note_create')
                            $inventory_adu->comments = "From Delivery Note OutWard";
                        else
                            $inventory_adu->comments = $product['comments'];
                        if ($get_uom_name->count() >= 1) {
                            $get_uom_name1 = $get_uom_name->first();
                            $inventory_adu->uom_name = $get_uom_name1->name;
                        } else
                            $inventory_adu->uom_name = "";
                        $inventory_adu->total_stock = $total_stock;
                        $inventory_adu->quantity_debit = $product['qty'];
                        $inventory_adu->created_by = $currentuser->id;
                        $inventory_adu->updated_by = $currentuser->id;
                        $inventory_adu->is_active = "1";
                        $inventory_adu->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
                        $inventory_adu->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
                        $type = GeneralHelper::checkConfiqSetting();
                        if ($type == 'purchase based') {
                            if ($inventory_adu->type == 'purchase_return') {
                                $barcode= GeneralHelper::getBarCode($product['bcn_id']);  
                                $inventory_adu->bcn_id = $product['bcn_id'];
                                $inventory_adu->barcode = $barcode;
                                $data = array();
                                $data['qty'] = $product['qty'];
                                $data['purchase_invoice_item_id'] = $product['purchase_invoice_item_id'];
                                $data['bcn_id'] = $product['bcn_id'];
                                GeneralHelper::StockInventoryDecrease($data);
                            }
                            if ($inventory_adu->type == 'stock_wastage'  || $inventory_adu->type == 'stock_wastage_create') {
                                $bcn_id = DB::table('tbl_bcn')->select('id','code')->where('ref_id', '=', $product['purchase_invoice_item_id'])->where('ref_type', '=', $type)->first();
                                $inventory_adu->bcn_id = $bcn_id->id;
                                $inventory_adu->barcode = $bcn_id->code;
                                $data = array();
                                $data['qty'] = $product['qty'];
                                $data['purchase_invoice_item_id'] = $product['purchase_invoice_item_id'];
                                $data['bcn_id'] = $bcn_id->id;
                                GeneralHelper::StockInventoryDecrease($data);
                            } else if ($inventory_adu->type == 'delivery_note_create') {
                                $bcn_id = DB::table('tbl_bcn')->select('id','code')->where('ref_id', '=', $product['ref_id'])->where('ref_type', '=', $type)->first();
                                $inventory_adu->bcn_id = $bcn_id->id;
                                $inventory_adu->barcode = $bcn_id->code;
                                $data = array();
                                $data['qty'] = $product['qty'];
                                $data['purchase_invoice_item_id'] = $product['ref_id'];
                                $data['bcn_id'] = $bcn_id->id;
                                GeneralHelper::StockInventoryDecrease($data);
                            }
                        }


//                        if ($code == 'invoice' || $code == 'update' || $code == 'purchase_return') {
//                            $purchaseInvoiceItemId = $product['purchase_invoice_item_id'];
//                            $bcn_id = DB::table('tbl_bcn')->select('id')->where('ref_id', '=', $purchaseInvoiceItemId)->where('ref_type', '=', $type)->first();
//                            $inventory_adu->bcn_id = $bcn_id->id;
//                        }

                        if ($type == 'product based') {
                            $bcn_id = DB::table('tbl_bcn')->select('id','code')->where('ref_id', '=', $product['product_id'])->where('ref_type', '=', $type)->first();
                            if (($bcn_id) != '') {
                                $inventory_adu->bcn_id = $bcn_id->id;
                                $inventory_adu->barcode = $bcn_id->code;
                            }
                            GeneralHelper::decrementBcnQty($product['product_id'], $product['qty']);
                        }
                        $inventory_adu->date = $date;
                        $inventory_adu->save();
                    }
                }
            }
        }
        return $inventory_adu->id;
    }

    // Bcn Qty Increment
    public static function incrementBcnQty($id, $qty) {

        DB::table('tbl_bcn')->where('ref_id', '=', $id)
                ->update(
                        array(
                            "qty_in_hand" => DB::raw('qty_in_hand +' . $qty),
                            "purchase_qty" => DB::raw('purchase_qty +' . $qty)
                        )
        );
    }

    public static function decrementBcnQty($id, $qty) {

        DB::table('tbl_bcn')->where('ref_id', '=', $id)
                ->update(
                        array(
                            "qty_in_hand" => DB::raw('qty_in_hand -' . $qty),
                            "sales_qty" => DB::raw('sales_qty +' . $qty)
                        )
        );
    }

    public static function checkConfiqSetting() {

        $settings = DB::table('tbl_app_config')
                ->select('value')->where('setting', '=', 'sales_product_list')
                ->first();

        $type = $settings->value;
        return $type;
    }

    //This for LBSPOS calculate tax from product

    public static function getProductTax($item, $customerId) {

        $product_tax_det = array();
        $current_date = Carbon::now();
        $current_date = $current_date->format('Y-m-d');
        foreach ($item as $product) {
            $product_tax_det1 = array();
            $pro_id = $product['product_id'];
            $builder = DB::table('tbl_tax as i')
                    ->leftJoin('tbl_tax_mapping as c', 'c.tax_id', '=', 'i.id')
                    ->select('i.*')
                    ->where('c.is_active', '=', 1)
                    ->where('c.product_id', '=', $pro_id)
                    ->whereRaw(DB::raw("(start_date <= '$current_date' or  `start_date` is null) and (end_date >= '$current_date' or end_date is null) "))
                    ->where('i.is_active', '=', 1)
                    ->first();

            $product['tax_list'] = array();
            if ($builder != "") {
                $product['tax_id'] = $builder->id;
                $tax_list = GeneralHelper::getGroupProductTax($builder->id, $customerId);
                $product['tax_list'] = $tax_list['tax_list'];
                $product['tax_percentage'] = $tax_list['tax_percentage'];
            }
            array_push($product_tax_det, $product);
        }
        return $product_tax_det;
    }

    public static function getProductTaxforPurchase($item, $customerId) {

        $product_tax_det = array();
        $current_date = Carbon::now();
        $current_date = $current_date->format('Y-m-d');
        foreach ($item as $product) {
            $product_tax_det1 = array();
            $pro_id = $product['product_id'];
            $builder = DB::table('tbl_tax')
                    ->select('*')
                    ->where('id', '=', $product['tax_id'])
                    ->first();

            $product['tax_list'] = array();
            if ($builder != "") {
                $product['tax_id'] = $builder->id;
                $tax_list = GeneralHelper::getGroupProductTax($builder->id, $customerId);
                $product['tax_list'] = $tax_list['tax_list'];
                $product['tax_percentage'] = $tax_list['tax_percentage'];
            }
            array_push($product_tax_det, $product);
        }

        return $product_tax_det;
    }

    public static function getGroupProductTax($tax_id, $customerId) {
        $tax_list_det = DB::table('tbl_tax')
                ->select('*')
                ->where('id', '=', $tax_id)
                ->first();
        $tax_list = array();
        $gstState = GeneralHelper::checkGstState();
        $custStateId = GeneralHelper::getCustomerState($customerId);

        if ($tax_list_det->is_group == 1) {

            if (isset($tax_list_det->intra_group_tax_ids) && $tax_list_det->intra_group_tax_ids != null && $tax_list_det->intra_group_tax_ids != '') {
                if ($gstState == $custStateId)
                    $associate_tax_id = explode(',', $tax_list_det->group_tax_ids);
                else
                    $associate_tax_id = explode(',', $tax_list_det->intra_group_tax_ids);
            }else {
                $associate_tax_id = explode(',', $tax_list_det->group_tax_ids);
            }

            $bilder = DB::table('tbl_tax')->select('*')
                            ->where('is_active', '=', 1)
                            ->whereIn('id', $associate_tax_id)->get();

            $tax_percentage = DB::table('tbl_tax')->where('is_active', '=', 1)
                    ->whereIn('id', $associate_tax_id)
                    ->sum('tax_percentage');
            $tax_list['tax_list'] = $bilder;
            $tax_list['tax_percentage'] = $tax_percentage;
        } else {
            $bilder = DB::table('tbl_tax')
                    ->select('*')
                    ->where('id', '=', $tax_id)
                    ->get();
            $tax_list['tax_list'] = $bilder;
            $tax_list['tax_percentage'] = $tax_list_det->tax_percentage;
        }
        return $tax_list;
    }

    public static function insertProductTax($product_tax_list, $id, $invoice_item_id, $code) {
        $type = GeneralHelper::checkConfiqSetting();
        $currentuser = Auth::user();
        $tax_amount = 0;
        $total_tax_amount = 0;
        $overAllTaxPer = 0;
        if (array_key_exists('tax_percentage', $product_tax_list)) {
            if ($product_tax_list['tax_percentage'] != 0)
                $overAllTaxPer = 100 / $product_tax_list['tax_percentage'];
        }
        $tax_list = $product_tax_list['tax_list'];
        foreach ($tax_list as $tax) {
            if ($code == 'invoice') {
                $invoicetax = new InvoiceItemTax;
                $invoicetax->invoice_id = $id;
                $invoicetax->invoice_item_id = $invoice_item_id;
            } else if ($code == 'sales_order') {
                $invoicetax = new SalesOrderItemTax;
                $invoicetax->sales_order_id = $id;
                $invoicetax->sales_order_item_id = $invoice_item_id;
            } else if ($code == 'quotes') {
                $invoicetax = new QuoteItemTax;
                $invoicetax->quote_id = $id;
                $invoicetax->quote_item_id = $invoice_item_id;
            }
            $invoicetax->tax_id = $tax->id;
            $invoicetax->tax_name = $tax->tax_name;
            $invoicetax->tax_percentage = $tax->tax_percentage;
            $product_amount = $product_tax_list['unit_price'] * $product_tax_list['qty'];
            $invoicetax->product_amt = $product_amount;
            $invoicetax->tax_applicable_amt = $tax->tax_applicable_amt;
            if ($type == 'purchase based') {
                $tax1 = ($product_tax_list['produt_rem_amount'] ) - ($product_tax_list['produt_rem_amount'] / (1 + $product_tax_list['tax_percentage'] / 100));
                $seperate = $overAllTaxPer * $tax->tax_percentage;
                $tax_amount = $tax1 * ($seperate / 100);
            } else if ($type == 'product based')
                $tax_amount = ($product_tax_list['produt_rem_amount']) * ($tax->tax_percentage / 100);
            $invoicetax->tax_amount = $tax_amount;
            $total_tax_amount = $total_tax_amount + $tax_amount;
            $invoicetax->is_active = 1;
            $invoicetax->created_by = $currentuser->id;
            $invoicetax->updated_by = $currentuser->id;
            $invoicetax->save();
        }
        return $total_tax_amount;
    }

    public static function calculateDiscount($item) {
        if ($item['discount_mode'] == "%") {
            $product_discount_amount = ($item['qty'] * $item['unit_price']) * ( $item['discount_value'] / 100);
        } else {
            $product_discount_amount = $item['discount_value'];
        }
        return $product_discount_amount;
    }

    //This is for maintain purchase inventory

    public static function updatePurchaseInventory($item) {
        $currentuser = Auth::user();
        foreach ($item as $product) {

            if (gettype($product) == 'array') {
                $purchase_invoice_item_id = $product['purchase_invoice_item_id'];
                $qty = $product['qty'];
            } else {
                $purchase_invoice_item_id = $product->purchase_invoice_item_id;
                $qty = $product->qty;
            }

            if (!empty($purchase_invoice_item_id) || $purchase_invoice_item_id != 0) {

                DB::table('tbl_bcn')->where('id', '=', $purchase_invoice_item_id)
                        ->update(
                                array(
                                    "qty_in_hand" => DB::raw('qty_in_hand -' . $qty),
                                    "sales_qty" => DB::raw('sales_qty +' . $qty)
                                )
                );

                $get_item_id = DB::table('tbl_bcn')->where('id', '=', $purchase_invoice_item_id)->first();
                $item_id = $get_item_id->ref_id;

                DB::table('tbl_purchase_invoice_item')->where('id', '=', $item_id)
                        ->update(
                                array(
                                    "qty_in_hand" => DB::raw('qty_in_hand -' . $qty),
                                    "sales_qty" => DB::raw('sales_qty +' . $qty)
                                )
                );
            }
        }
    }

    public static function updatePurchaseInventoryForUpdate($item) {
        $currentuser = Auth::user();
        foreach ($item as $product) {
            if (gettype($product) == 'array') {
                $purchase_invoice_item_id = $product['purchase_invoice_item_id'];
                $qty = $product['qty'];
            } else {
                $purchase_invoice_item_id = $product->purchase_invoice_item_id;
                $qty = $product->qty;
            }
            if (!empty($purchase_invoice_item_id) || $purchase_invoice_item_id != 0) {


                DB::table('tbl_bcn')->where('id', '=', $purchase_invoice_item_id)
                        ->update(
                                array(
                                    "qty_in_hand" => DB::raw('qty_in_hand +' . $qty),
                                    "sales_qty" => DB::raw('sales_qty -' . $qty)
                                )
                );

                $get_item_id = DB::table('tbl_bcn')->where('id', '=', $purchase_invoice_item_id)->first();
                $item_id = $get_item_id->ref_id;

                DB::table('tbl_purchase_invoice_item')->where('id', '=', $item_id)
                        ->update(
                                array(
                                    "qty_in_hand" => DB::raw('qty_in_hand +' . $qty),
                                    "sales_qty" => DB::raw('sales_qty -' . $qty)
                                )
                );
            }
        }
    }

    public static function InvoiceTaxSave($id, $tax_id, $taxable_amount, $sub_amount, $code) {
        if ($tax_id != null) {

            $currentuser = Auth::user();
            $tax_list = GeneralHelper::calculatedInvoiceTax($id, $tax_id, $taxable_amount, $code);

            foreach ($tax_list as $tax) {
                if ($code == 'sales')
                    $invoicetax = new Invoicetax;
                else if ($code == 'purchase')
                    $invoicetax = new Purchaseinvoicetax;
                $invoicetax->invoice_id = $id;
                $invoicetax->tax_id = $tax->id;
                $invoicetax->tax_name = $tax->tax_name;
                $invoicetax->tax_percentage = $tax->tax_percentage;
                $invoicetax->product_amt = 0;
                $invoicetax->applicable_amt = $tax->tax_applicable_amt;
                $invoicetax->taxable_amount = $tax->taxable_amount;
                $invoicetax->tax_amount = $tax->tax_amount;
                $invoicetax->tax_type = $tax->tax_type;
                $invoicetax->is_active = 1;
                $invoicetax->created_by = $currentuser->id;
                $invoicetax->updated_by = $currentuser->id;

                $invoicetax->save();
            }
        }
    }

    public static function calculatedInvoiceTax($invoide_id, $tax_id, $amount, $code) {
        $tax_list = array();

        if ($tax_id == 'invoiced_tax_rate') {
            $tax_list = GeneralHelper::calculatedInvoiceTax_BasedOnInvoiceGenerate($invoide_id, $amount, $code);
        } else {
            $tax_list = GeneralHelper::calculatedInvoiceTax_BasedOnCurrentTaxRate($tax_id, $amount);
        }
        return $tax_list;
    }

    public static function calculatedInvoiceTax_BasedOnCurrentTaxRate($tax_id, $amount) {
        $tax_collection = DB::table('tbl_tax')->select('*')
                        ->where('id', '=', $tax_id)
                        ->where('is_active', '=', 1)->get();
        $total_tax_amount = 0;
        $tax_list = array();
        if (count($tax_collection) > 0) {

            $tax = $tax_collection->first();
            if (!empty($tax->is_group)) {
                $associate_tax_id = explode(',', $tax->group_tax_ids);
                $chargeAmount = 0;
                $associate_tax_collection = DB::table('tbl_tax')->select('*')
                                ->where('is_active', '=', 1)
                                ->whereIn('id', $associate_tax_id)->get();

                foreach ($associate_tax_collection as $associate_tax) {

                    $tax_amount = 0;

                    if ($associate_tax->tax_type != 'tax+charge') {
                        $tax_amount = $amount * $associate_tax->tax_percentage / 100;
                        $tax_amount = round($tax_amount, 2);
                        $total_tax_amount += $tax_amount;

                        if ($associate_tax->tax_type == 'charge') {
                            $chargeAmount += $tax_amount;
                        }
                        $associate_tax->taxable_amount = $amount;
                        $associate_tax->tax_amount = $tax_amount;
                    }
                }
                foreach ($associate_tax_collection as $associate_tax) {

                    $tax_amount = 0;

                    if ($associate_tax->tax_type == 'tax+charge') {
                        $tax_amount += ($amount + $chargeAmount) * $associate_tax->tax_percentage / 100;
                        $tax_amount = round($tax_amount, 2);
                        $total_tax_amount += $tax_amount;
                        $associate_tax->taxable_amount = $amount + $chargeAmount;
                        $associate_tax->tax_amount = $tax_amount;
                    }
                }

                //sorting the tax in creation order
                foreach ($associate_tax_id as $asso_tax_id) {

                    foreach ($associate_tax_collection as $associate_tax) {
                        if ($asso_tax_id == $associate_tax->id) {
                            array_push($tax_list, $associate_tax);
                            break;
                        }
                    }
                }
            } else {

                $tax_amount = $amount * $tax->tax_percentage / 100;
                $tax_amount = round($tax_amount, 2);
                $total_tax_amount = $tax_amount;
                $tax->taxable_amount = $amount;
                $tax->tax_amount = $tax_amount;
                array_push($tax_list, $tax);
            }
        }
        return $tax_list;
    }

    public static function calculatedInvoiceTax_BasedOnInvoiceGenerate($invoide_id, $amount, $code) {

        if ($code == 'sales') {
            $tax_collection = DB::table('tbl_invoice_tax')->select('*')
                            ->where('invoice_id', '=', $invoide_id)
                            ->where('is_active', '=', 1)->get();
        } else if ($code == 'purchase') {
            $tax_collection = DB::table('tbl_purchase_invoice_tax')->select('*')
                            ->where('invoice_id', '=', $invoide_id)
                            ->where('is_active', '=', 1)->get();
        }

        $total_tax_amount = 0;
        $tax_list = array();
        $chargeAmount = 0;
        if (count($tax_collection) > 0) {


            foreach ($tax_collection as $associate_tax) {

                $tax_amount = 0;

                if ($associate_tax->tax_type != 'tax+charge') {
                    $tax_amount = $amount * $associate_tax->tax_percentage / 100;
                    $tax_amount = round($tax_amount, 2);
                    $total_tax_amount += $tax_amount;

                    if ($associate_tax->tax_type == 'charge') {
                        $chargeAmount += $tax_amount;
                    }
                    $associate_tax->taxable_amount = $amount;
                    $associate_tax->tax_amount = $tax_amount;
                }
            }

            foreach ($tax_collection as $associate_tax) {

                $tax_amount = 0;

                if ($associate_tax->tax_type == 'tax+charge') {
                    $tax_amount += ($amount + $chargeAmount) * $associate_tax->tax_percentage / 100;
                    $tax_amount = round($tax_amount, 2);
                    $total_tax_amount += $tax_amount;
                    $associate_tax->taxable_amount = $amount + $chargeAmount;
                    $associate_tax->tax_amount = $tax_amount;
                }
            }
        }
        return $tax_collection;
    }

    public static function InvoiceTaxUpdate($id, $tax_id, $taxable_amount, $sub_amount, $code) {
        $currentuser = Auth::user();
        if ($tax_id == 'invoiced_tax_rate') {
            $invoice_tax_list = GeneralHelper::calculatedInvoiceTax_BasedOnInvoiceGenerate($id, $taxable_amount, $code);

            try {

                foreach ($invoice_tax_list as $tax) {
                    if ($code == 'sales') {
                        $invoicetax = Invoicetax::findOrFail($tax->id);
                    } else if ($code == 'purchase') {
                        $invoicetax = Purchaseinvoicetax::findOrFail($tax->id);
                    }
                    $invoicetax->taxable_amount = $tax->taxable_amount;
                    $invoicetax->tax_amount = $tax->tax_amount;
                    $invoicetax->is_active = 1;
                    $invoicetax->updated_by = $currentuser->id;
                    $invoicetax->save();
                }
            } catch (ModelNotFoundException $e) {
                
            }
        } else {
            if ($code == 'sales') {
                DB::table('tbl_invoice_tax')
                        ->where('invoice_id', '=', $id)
                        ->delete();
            } else if ($code == 'purchase') {
                DB::table('tbl_purchase_invoice_tax')
                        ->where('invoice_id', '=', $id)
                        ->delete();
            }
            GeneralHelper::InvoiceTaxSave($id, $tax_id, $taxable_amount, $sub_amount, $code);
        }
    }

    public static function InvoiceTaxDelete($id, $code) {
        if ($code == 'sales') {
            DB::table('tbl_invoice_tax')->where('invoice_id', $id)
                    ->update(['is_active' => 0]);
        } else if ($code == 'purchase') {
            DB::table('tbl_purchase_invoice_tax')->where('invoice_id', $id)
                    ->update(['is_active' => 0]);
        }
    }

    public static function purchaseInvoiceItemTax($purchaseinvoiceitems) {
        $currentuser = Auth::user();
        $taxAmt = 0.00;
        $taxPercentage = 0;
        if ($purchaseinvoiceitems->tax_id != null || $purchaseinvoiceitems->tax_id != 0) {
            $taxes = DB::table('tbl_tax')
                            ->select('*')
                            ->where('id', $purchaseinvoiceitems->tax_id)->get();
            foreach ($taxes as $tax) {
                if ($tax->is_group == 0) {
                    $invoicetax = new Purchaseinvoicetax;
                    $invoicetax->purchase_invoice_item_id = $purchaseinvoiceitems->id;
                    $invoicetax->invoice_id = $purchaseinvoiceitems->purchase_invoice_id;
                    $invoicetax->is_active = 1;
                    $invoicetax->created_by = $currentuser->id;
                    $invoicetax->updated_by = $currentuser->id;
                    $taxPercentage = $tax->tax_percentage + $taxPercentage;
                    $invoicetax->tax_id = $tax->id;
                    $invoicetax->product_amt = $purchaseinvoiceitems->purchase_value; //qty*selling_price
                    $invoicetax->tax_amount = ($purchaseinvoiceitems->purchase_value - $purchaseinvoiceitems->discount_amount) * ($tax->tax_percentage / 100);
                    $taxAmt = $invoicetax->tax_amount + $taxAmt;
                    $invoicetax->tax_name = $tax->tax_name;
                    $invoicetax->tax_percentage = $tax->tax_percentage;
                    $invoicetax->tax_applicable_amt = $tax->tax_applicable_amt;
                    $invoicetax->save();
                } else {

                    $gstState = GeneralHelper::checkGstState();
                    $custStateId = GeneralHelper::getCustomerState($purchaseinvoiceitems->customer_id);

                    if (isset($tax->intra_group_tax_ids) && $tax->intra_group_tax_ids != null && $tax->intra_group_tax_ids != '') {
                        if ($gstState == $custStateId)
                            $splittedstring = explode(",", $tax->group_tax_ids);
                        else
                            $splittedstring = explode(",", $tax->intra_group_tax_ids);
                    }else {
                        $splittedstring = explode(",", $tax->group_tax_ids);
                    }

                    foreach ($splittedstring as $key => $value) {
                        $gTs = DB::table('tbl_tax')
                                        ->select('*')
                                        ->where('id', $value)->get();
                        foreach ($gTs as $gT) {
                            //print_r($gT->id);
                            $invoicetax = new Purchaseinvoicetax;
                            $invoicetax->purchase_invoice_item_id = $purchaseinvoiceitems->id;
                            $invoicetax->invoice_id = $purchaseinvoiceitems->purchase_invoice_id;
                            $invoicetax->is_active = 1;
                            $invoicetax->created_by = $currentuser->id;
                            $invoicetax->updated_by = $currentuser->id;
                            $invoicetax->product_amt = $purchaseinvoiceitems->purchase_price_wo_tax;
                            $invoicetax->product_amt = $purchaseinvoiceitems->purchase_value; //qty*selling_price
                            $invoicetax->tax_amount = ($purchaseinvoiceitems->purchase_value - $purchaseinvoiceitems->discount_amount) * ($gT->tax_percentage / 100);
                            $taxAmt = $invoicetax->tax_amount + $taxAmt;
                            $taxPercentage = $gT->tax_percentage + $taxPercentage;
                            $invoicetax->tax_id = $gT->id;
                            $invoicetax->tax_name = $gT->tax_name;
                            $invoicetax->tax_percentage = $gT->tax_percentage;
                            $invoicetax->tax_applicable_amt = $gT->tax_applicable_amt;
                            $invoicetax->save();
                        }
                    }
                }
            }
            $purchaseinvoiceitems->tax_amount = $taxAmt;
            $purchaseinvoiceitems->tax_percentage = $taxPercentage;
            $purchaseinvoiceitems->total_price = $purchaseinvoiceitems->purchase_value - $purchaseinvoiceitems->discount_amount + $purchaseinvoiceitems->tax_amount;
            $purchaseinvoiceitems->save();
        }
    }

    // For Insert Labaeling

    public static function insertLabelingItem($items) {

        $settings = DB::table('tbl_app_config')
                ->select('value')->where('setting', '=', 'sales_product_list')
                ->first();

        $type = $settings->value;
        $labelingitem = new Bcn;
        if ($type == 'purchase based') {

            $labelingitem->ref_id = $items->id;
            $labelingitem->purchase_invoice_id = $items->purchase_invoice_id;
            $labelingitem->ref_type = $settings->value;
            $labelingitem->product_id = $items->product_id;
            $labelingitem->product_name = $items->product_name;
            $labelingitem->product_sku = $items->product_sku;
            $labelingitem->code = $items->code;
            $labelingitem->qty = $items->qty;
            $labelingitem->qty_in_hand = $items->qty_in_hand;
            $labelingitem->sales_qty = 0;
            $labelingitem->selling_price = $items->selling_price;
            $labelingitem->purchase_price = $items->purchase_price;
            $labelingitem->mrp_price = $items->mrp_price;
            $labelingitem->status = 'purchase';
            $labelingitem->discount_mode = $items->discount_mode;
            $labelingitem->discount_value = $items->discount_value;
            $labelingitem->discount_amount = $items->discount_amount;
            $labelingitem->code = $items->code;
            $labelingitem->uom_id = $items->uom_id;
            $labelingitem->is_active = $items->is_active;
            $labelingitem->uom_name = $items->uom_name;
        } else {
            $labelingitem->ref_id = $items->id;
            //  $labelingitem->purchase_invoice_id = $items->purchase_invoice_id;
            $labelingitem->ref_type = $settings->value;
            $labelingitem->product_id = $items->id;
            $labelingitem->product_name = $items->name;
            $labelingitem->product_sku = $items->sku;
            $labelingitem->code = $items->code;
            $labelingitem->qty = $items->qty;
            $labelingitem->qty_in_hand = $items->qty;
            $labelingitem->sales_qty = 0;
            $labelingitem->selling_price = $items->sales_price;
            //  $labelingitem->purchase_price = $items->purchase_price;
            //$labelingitem->mrp_price = $items->mrp_price;
            $labelingitem->status = 'product';
            //  $labelingitem->discount_mode = $items->discount_mode;
            //  $labelingitem->discount_value = $items->discount_value;
            //   $labelingitem->discount_amount = $items->discount_amount;
            $labelingitem->code = $items->code;
            $labelingitem->uom_id = $items->uom_id;
            $labelingitem->is_active = $items->is_active;
            $labelingitem->uom_name = $items->uom;
        }

        $currentuser = Auth::user();
        $labelingitem->created_by = $currentuser->id;
        $labelingitem->updated_by = $currentuser->id;

        $labelingitem->save();
    }

    public static function getCustomerAcode($customer_id) {
        $retValue = '';
        if (!empty($customer_id)) {
            $customer = DB::table('tbl_customer')->where('id', $customer_id)->first();
            if ($customer != null) {
                $retValue = $customer->acode;
            }
        }
        return $retValue;
    }

    public static function getPaymentAcode($account_id) {
        $retValue = '';
        if (!empty($account_id)) {
            $account = DB::table('tbl_accounts')->where('id', $account_id)->first();
            if ($account != null) {
                $retValue = $account->acode;
            }
        }
        return $retValue;
    }

    public static function getCustomerAccountId($customer_id) {
        $retValue = '';
        if (!empty($customer_id)) {
            $account = DB::table('tbl_acode')->where('is_active', 1)->where('ref_id', $customer_id)
                            ->where('ref_type', 'customer')->first();
            ;
            if ($account != null) {
                $retValue = $account->id;
            }
        }
        return $retValue;
    }

    public static function getCustomerAccount($account_id) {
        $retValue = '';
        if (!empty($account_id)) {
            $account = DB::table('tbl_accounts')->where('is_active', 1)->where('id', $account_id)->first();
            ;
            if ($account != null) {
                $retValue = $account->account;
            }
        }
        return $retValue;
    }

    public static function getContraAccountId($currentuser, $type) {
        if ($type == 'creditNote')
            $contra_account = DB::table('tbl_accounts')->where('is_active', 1)->where('account', 'creditNote')->get();
        else
            $contra_account = DB::table('tbl_accounts')->where('is_active', 1)->where('account', 'debitNote')->get();
        if (count($contra_account) > 0) {
            $contra_account = $contra_account->first();
            $contra_account_id = $contra_account->id;
        } else {
            $con_acc = new Account;
            $con_acc->account = $type;
            $con_acc->is_active = 1;
            $con_acc->created_by = $currentuser->id;
            $con_acc->updated_by = $currentuser->id;
            $con_acc->save();

//            $acode = new Acode;
//            $acode->created_by = $currentuser->id;
//            $acode->updated_by = $currentuser->id;
//            $acode->ref_type = Acode::$ACCOUNT_REF;
//            $acode->ref_id = $con_acc->id;
//            $acode->name = $con_acc->account;
//            $acode->save();
//            $acode->code = $acode->id;
//            $acode->save();
//
//            $con_acc->acode = $acode->code;

            $con_acc->save();
            $contra_account_id = $con_acc->id;
        }
        return $contra_account_id;
    }

    public static function updatePurchaseReturnInventory($item) {
        $currentuser = Auth::user();
        foreach ($item as $product) {

            if (gettype($product) == 'array') {
                $purchase_invoice_item_id = $product['purchase_invoice_item_id'];
                $bcn_id = $product['bcn_id'];
                $qty = $product['qty'];
            } else {
                $purchase_invoice_item_id = $product->purchase_invoice_item_id;
                $bcn_id = $product->bcn_id;
                $qty = $product->qty;
            }

            if (!empty($purchase_invoice_item_id) || $purchase_invoice_item_id != 0) {

                DB::table('tbl_purchase_invoice_item')->where('id', '=', $purchase_invoice_item_id)
                        ->update(
                                array(
                                    "qty_in_hand" => DB::raw('qty_in_hand -' . $qty)
                                )
                );



                DB::table('tbl_bcn')->where('id', '=', $bcn_id)
                        ->update(
                                array(
                                    "qty_in_hand" => DB::raw('qty_in_hand -' . $qty)
                                )
                );
            }
        }
    }

    public static function PurchaseReturnInventory($item) {
        $currentuser = Auth::user();
        foreach ($item as $product) {

            if (gettype($product) == 'array') {
                $purchase_invoice_item_id = $product['purchase_invoice_item_id'];
                $bcn_id = $product['bcn_id'];
                $qty = $product['qty'];
            } else {
                $purchase_invoice_item_id = $product->purchase_invoice_item_id;
                $bcn_id = $product->bcn_id;
                $qty = $product->qty;
            }

            if (!empty($purchase_invoice_item_id) || $purchase_invoice_item_id != 0) {

                DB::table('tbl_purchase_invoice_item')->where('id', '=', $purchase_invoice_item_id)
                        ->update(
                                array(
                                    "qty_in_hand" => DB::raw('qty_in_hand +' . $qty)
                                )
                );



                DB::table('tbl_bcn')->where('id', '=', $bcn_id)
                        ->update(
                                array(
                                    "qty_in_hand" => DB::raw('qty_in_hand +' . $qty)
                                )
                );
            }
        }
    }

    public static function StockInventoryIncrease($item) {

        $purchase_invoice_item_id = $item['purchase_invoice_item_id'];
        $bcn_id = $item['bcn_id'];
        $qty = $item['qty'];

        if (!empty($purchase_invoice_item_id) || $purchase_invoice_item_id != 0) {

            DB::table('tbl_purchase_invoice_item')->where('id', '=', $purchase_invoice_item_id)
                    ->update(
                            array(
                                "qty_in_hand" => DB::raw('qty_in_hand +' . $qty)
                            //"qty" => DB::raw('qty +' . $qty)
                            )
            );


            DB::table('tbl_bcn')->where('id', '=', $bcn_id)
                    ->update(
                            array(
                                "qty_in_hand" => DB::raw('qty_in_hand +' . $qty)
                            //"purchase_qty" => DB::raw('purchase_qty +' . $qty)
                            )
            );
        }
    }

    public static function StockInventoryDecrease($item) {

        $purchase_invoice_item_id = $item['purchase_invoice_item_id'];
        $bcn_id = $item['bcn_id'];
        $qty = $item['qty'];

        if (!empty($purchase_invoice_item_id) || $purchase_invoice_item_id != 0) {

            DB::table('tbl_purchase_invoice_item')->where('id', '=', $purchase_invoice_item_id)
                    ->update(
                            array(
                                "qty_in_hand" => DB::raw('qty_in_hand -' . $qty)
                            // "qty" => DB::raw('qty -' . $qty)
                            )
            );


            DB::table('tbl_bcn')->where('id', '=', $bcn_id)
                    ->update(
                            array(
                                "qty_in_hand" => DB::raw('qty_in_hand -' . $qty)
//                                "purchase_qty" => DB::raw('purchase_qty -' . $qty)
                            )
            );
        }
    }

    public static function InventoryCredit($request) {
        $productArray = $request;
        $currentuser = Auth::user();
        foreach ($productArray as $product) {
            $inventory_adu = new InventoryAdjustment;
            $inventory_adu->product_id = $product['product_id'];
            if ($product['product_id'] != '') {
                $inventotyCollection = Inventory::where('product_id', "=", $product['product_id'])->get();
                $inventotyCollection = $inventotyCollection->first();
                if ($inventotyCollection != null) {
                    $total_stock = $inventotyCollection->quantity + $product['qty'];
                    $inventotyCollection->quantity = $total_stock;
                    $inventotyCollection->save();
                }
            }
        }
    }

    public static function InventoryDebit($request) {
        $productArray = $request;
        $currentuser = Auth::user();
        foreach ($productArray as $product) {
            $inventory_adu = new InventoryAdjustment;
            $inventory_adu->product_id = $product['product_id'];
            if ($product['product_id'] != '') {
                $inventotyCollection = Inventory::where('product_id', "=", $product['product_id'])->get();
                $inventotyCollection = $inventotyCollection->first();
                if ($inventotyCollection != null) {
                    $total_stock = $inventotyCollection->quantity - $product['qty'];
                    $inventotyCollection->quantity = $total_stock;
                    $inventotyCollection->save();
                }
            }
        }
    }

    public static function InsertInventoryAdjustment($request, $code, $id,$date) {
        $productArray = $request;
        $type = GeneralHelper::checkConfiqSetting();

        $currentuser = Auth::user();
        foreach ($productArray as $product) {

            $inventory_adu = new InventoryAdjustment;
            $inventory_adu->product_id = $product['product_id'];

            if ($product['product_id'] != '') {
                $get_product_det = DB::table('tbl_product')
                                ->select('has_inventory', 'is_active')
                                ->where('has_inventory', '=', '1')
                                ->where('is_active', '=', '1')
                                ->where('id', '=', $product['product_id'])->get();
                if ($get_product_det->count() >= 1) {
                    $inventory_adu->ref_id = $id;
                    $inventory_adu->type = $code;
                    $inventotyCollection = Inventory::where('product_id', "=", $product['product_id'])->first();
                    $get_uom_name = DB::table('tbl_uom')
                                    ->select('name')
                                    ->where('id', '=', $product['uom_id'])->first();
                    if ($inventotyCollection != null) {
                        $inventory_adu->sku = $product['product_sku'];
                        $inventory_adu->uom_id = $product['uom_id'];
                        if ($code == 'purchase_return_delete') {
                            $inventory_adu->quantity_credit = $product['qty'];
                            $inventory_adu->comments = "From purchase return delete";
                        }
                        if ($code == 'purchase_return_update') {
                            $inventory_adu->quantity_debit = $product['qty'];
                            $inventory_adu->comments = "From purchase return update";
                        }
                        if (!empty($get_uom_name)) {
                            $inventory_adu->uom_name = $get_uom_name->name;
                        } else
                            $inventory_adu->uom_name = "";
                        $inventory_adu->total_stock = $inventotyCollection->quantity;

                        $inventory_adu->created_by = $currentuser->id;
                        $inventory_adu->updated_by = $currentuser->id;
                        $inventory_adu->is_active = "1";
                        $inventory_adu->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
                        $inventory_adu->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;

                        if ($type == 'purchase based') {
                        if ($code == 'purchase_return_delete' || $code == 'purchase_return_update') {
                            $purchaseInvoiceItemId = $product['purchase_invoice_item_id'];
                            $bcn_id = DB::table('tbl_bcn')->select('id','code')->where('ref_id', '=', $purchaseInvoiceItemId)->where('ref_type', '=', $type)->first();
                            $inventory_adu->bcn_id = $bcn_id->id;
                            $inventory_adu->barcode = $bcn_id->code;
                        }
                        }
                        if ($type == 'product based') {
                            $bcn_id = DB::table('tbl_bcn')->select('id','code')->where('ref_id', '=', $product['product_id'])->where('ref_type', '=', $type)->first();
                            if (count($bcn_id) > 1) {
                                $inventory_adu->bcn_id = $bcn_id->id;
                                $inventory_adu->barcode = $bcn_id->code;
                            }
                            GeneralHelper::decrementBcnQty($product['product_id'], $product['qty']);
                        }

                        $inventory_adu->date=$date;
                        $inventory_adu->save();
                    }
                }
            }
        }
    }

    public static function decimalFormat() {
        $decimalFormat = 0;
        $value = DB::table('tbl_app_config')
                ->where('setting', '=', 'qty_decimal_format')
                ->first();
        if ($value != '') {
            if (!empty($value->value))
                $decimalFormat = $value->value;
        }

        return $decimalFormat;
    }

    public static function getCustomerByAcode($acode) {
        $customer_id = '';
        if (!empty($acode)) {
            $cus = DB::table('tbl_acode')->where('code', '=', $acode)->where('ref_type', '=', 'customer')->first();
            if (!empty($cus)) {
                $customer_id = $cus->ref_id;
            }
        }
        return $customer_id;
    }

    public static function generateRandomString($length = 9) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function checkAutoGstSetting() {
        $type = 0;
        $settings = DB::table('tbl_app_config')
                ->select('value')->where('setting', '=', 'gst_auto_suggest')
                ->first();

        if ($settings != '')
            $type = $settings->value;

        return $type;
    }

    public static function checkGstState() {
        $type = 0;
        $settings = DB::table('tbl_app_config')
                ->select('value')->where('setting', '=', 'gst_state')
                ->first();

        if ($settings != null && $settings != '')
            $type = $settings->value;

        return $type;
    }

    public static function getCustomerState($customerId) {
        $stateId = 0;
        $customer = DB::table('tbl_customer')
                ->where('is_active', '=', 1)
                ->where('id', '=', $customerId)
                ->first();

        if ($customer != null &&  $customer != '') {
            $stateId = $customer->billing_state_id;
        }
        return $stateId;
    }
    
    public static function CsvDate($date, $format) {

        if (empty($date) || empty($format)) {

            return $date;
        }
        $new_format = str_replace("YYYY", "Y", $format);
        $new_format = str_replace("yyyy", "Y", $new_format);
        $new_format = str_replace("MM", "m", $new_format);
        $new_format = str_replace("mm", "m", $new_format);
        $new_format = str_replace("DD", "d", $new_format);
        $new_format = str_replace("dd", "d", $new_format);

        $new_date = date_create($date);

        return date_format($new_date, $new_format);
    }
    
    public static function getBarCode($id){
        $code='';
        $builder=DB::table('tbl_bcn')
                ->where('is_active','=',1)
                ->where('id','=',$id)
                ->first();
        
        if($builder != ''){
            $code=$builder->code;
        }
        
        return $code;
    }
    
     public static function get_mode(){
        $mode = '';
        $data=DB::table('tbl_payment_term')
                ->where('is_active','=',1)
                ->where('is_default',1)
                ->first();

        if(!empty($data))
          $mode = $data->name;

        return $mode;
        }
        
        public static Function getCustName($customerId){
            $cusName='';
           $customer = DB::table('tbl_customer')
                ->where('is_active', '=', 1)
                ->where('id', '=', $customerId)
                ->first(); 
           
           if($customer != null && $customer != ''){
               $cusName=$customer->fname." ".$customer->lname;
           }
           return $cusName;
        }
        
         public static function hsnCodeTaxId($hsnCode, $price) {

        $taxIds = DB::table('tbl_hsn_tax_mapping as ht')->select('*')
                ->leftjoin('tbl_hsn_code as hc', 'ht.hsn_code', '=', 'hc.hsn_code')
                ->where('ht.hsn_code', '=', $hsnCode)
                ->whereRaw(DB::raw("(from_price <= '" . $price . "' or from_price = 0) and (to_price >= '" . $price . "' or to_price = 0)"))
                ->where('ht.is_active', '=', 1)
                ->first();

        $group_tax_id = 0;
        if ($taxIds != '') {
            $group_tax_id = $taxIds->group_tax_id;
        }
        return $group_tax_id;
    }
        
    public static function getGroupTaxId($taxId){
        $groupTaxIds=array();
        
        $builder=DB::table('tbl_tax')
                ->where('is_active','=',1)
                ->where('is_group','=',1)
                ->Whereraw(DB::raw("(FIND_IN_SET('" . $taxId . "',group_tax_ids)) or (FIND_IN_SET('" . $taxId . "',intra_group_tax_ids))"))
                ->get();
         foreach($builder as $build){
             array_push($groupTaxIds,$build->id);
}

        return implode(',',$groupTaxIds);
    }
}

?>
