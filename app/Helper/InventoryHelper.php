<?php

namespace App\Helper;
use App\InventoryAdjustment;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Helper\GeneralHelper;
use App\Inventory;

use DB;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of InventoryHelper
 *
 * @author Deepa
 */
class InventoryHelper {
    //put your code here
    public static function increase ($request ,$id , $code){
       
         $currentuser = Auth::user();
        $productArray = $request;
       
         $inventory_adu = new InventoryAdjustment;
          $inventory_adu->product_id =$productArray['product_id'];
          $get_product_det = DB::table('tbl_product')
                                ->select('has_inventory', 'is_active')
                                ->where('has_inventory', '=', '1')
                                ->where('is_active', '=', '1')
                                ->where('id', '=',$productArray['product_id'])->get();
          $productDet=$get_product_det->first();
         $inventory_adu->ref_id = $id;
         $inventory_adu->type = $code;
         if($productDet != '' ){
          $inventotyCollection = Inventory::where('product_id', "=", $productArray['product_id'])->get();
          $get_uom_name = DB::table('tbl_uom')
                                    ->select('name')
                                    ->where('id', '=', $productArray['uom_id'])->get();
          
                    $inventotyCollection = $inventotyCollection->first();
                    if($inventotyCollection != ''){
                    $total_stock = $inventotyCollection->quantity + $productArray['qty'];
                    $inventotyCollection->quantity = $total_stock;
                    $inventotyCollection->save();
                                      
                    
                    $inventory_adu->sku = $productArray['product_sku'];
                    $inventory_adu->uom_id = $productArray['uom_id'];
                    
                    $inventory_adu->uom_name = $productArray['uom_name'];
                    $inventory_adu->total_stock = $total_stock;
                    $inventory_adu->quantity_credit = $productArray['qty'];
                    $inventory_adu->created_by = $currentuser->id;
                    $inventory_adu->updated_by = $currentuser->id;
                    $inventory_adu->is_active = "1";
                    $inventory_adu->comments="From Delivery Qty Update ";
                    $inventory_adu->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
                    $inventory_adu->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
                    $inventory_adu->save();
                    $type = GeneralHelper::checkConfiqSetting();
                    $bcn_id = DB::table('tbl_bcn')->select('id')->where('ref_id', '=', $productArray['purchase_invoice_item_id'])->where('ref_type', '=', $type)->first();
                    if($bcn_id != '')   
                    $inventory_adu->bcn_id = $bcn_id->id;
                    
                    if($type=='purchase based'){
                        $productArray['bcn_id']=$bcn_id->id;
                        GeneralHelper::StockInventoryIncrease($productArray);
                    }else{
                         GeneralHelper::decrementBcnQty($productArray['product_id'], $productArray['qty']);
                    }
         }
         }
         
      
    }
    public static function decrease ($id ,$amount){
               
 DB::table('tbl_accounts')->where('id', $id)
         ->update(['balance' =>DB::raw('balance -'. $amount)]);
    }
}

?>
