<?php

/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

/**
* Description of LogHelper
*
* @author Stutzen Admin
*/
namespace App\Helper;
use Illuminate\Support\Facades\Log;
class LogHelper {
   public static function info($string){
      
       Log::info($_SERVER['SERVER_NAME']. ' ' .$string);
   }
   
    public static function info1($string,$request){
 

       Log::info($_SERVER['SERVER_NAME']. ' ' .$string,$request);
   }
   public static function warning($string){
        Log::warning($_SERVER['SERVER_NAME']. ' ' .$string);
   }
   
   public static function error($string){
        Log::error($_SERVER['SERVER_NAME']. ' ' .$string);
   }
}