<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LogHelper
 *
 * @author Stutzen Admin
 */

namespace App\Helper;

use Illuminate\Support\Facades\Log;
use App\Notification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

class NotificationHelper {

    public static function saveNotification(Request $request, $name, $type, $screen_code, $id) {
        $builder = DB::table('tbl_screen as s')
                ->leftJoin('tbl_role_access as r', 'r.screen_code', '=', 's.name')
                ->leftJoin('tbl_user as u', 'u.role_id', '=', 'r.role_id')
                ->select('u.id as user_id', 's.short_url')
                ->where('s.name', $screen_code)
                ->where('s.is_active', 1)
                ->where('r.is_active', 1)
                ->where('u.is_active', 1)
                ->where('r.is_notify', 1);

        $currentuser = Auth::user();

        if ($type == 'save')
            $builder = $builder->where('r.is_create', 1);

        if ($type == 'update')
            $builder = $builder->where('r.is_modify', 1);

        if ($type == 'delete')
            $builder = $builder->where('r.is_delete', 1);


        $builder = $builder->get();

        foreach ($builder as $bu) {

            $current_date = Carbon::now();
            $current_date1 = $current_date->format('d-m-Y g:i A');

            $descr = "";
            if ($type == 'save')
                $descr = $currentuser->f_name . " " . $currentuser->l_name . " Created new " . $name ;

            if ($type == 'update')
                $descr = $currentuser->f_name . " " . $currentuser->l_name . " Updated  " . $name ;

            if ($type == 'delete')
                $descr = $currentuser->f_name . " " . $currentuser->l_name . " Deleted " . $name ;;



            $noti = new Notification;

            $noti->date = $current_date1;
            $noti->description = $descr;
            $noti->url = $request->fullurl();
            $noti->method = $request->method();
            $noti->user_agent = $request->header('user-agent');
            $noti->user_id = $bu->user_id;
            $noti->type = $type;
            $noti->ip_address = $request->ip();
            $noti->notification_type = " System generated critical events (default)";
            $noti->read_or_not = 0;
            $noti->request = json_encode($request->all());

            $noti->to_display = 1;

            if (strpos($bu->short_url, '{{id}}') !== false) {
                $url =   str_replace("{{id}}",$id,$bu->short_url);
                $noti->short_url = $url  ;
            } else {
                $noti->short_url =$bu->short_url . $id;
            }

            $noti->ref_id = $id;
            $noti->ref_name = $name;
            $noti->screen_code = $screen_code;
            $noti->created_by_name = $currentuser->f_name . " " . $currentuser->l_name;
            $noti->created_by = $currentuser->id;
            $noti->updated_by = $currentuser->id;
            $noti->is_active = 1;
            $noti->save();
        }
    }

}
