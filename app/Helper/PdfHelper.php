<?php

namespace App\Helper;

use DateTime;
use App\User;
use App\Customer;
use Carbon\Carbon;
use App\PersistentLogin;
use Faker\Provider\Uuid;
use Illuminate\Http\Request;
use App\Transformer\UserTransformer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use GuzzleHttp\Client as HttpClient;
use Illuminate\Mail\Message;
use DB;
use App\Helper\GeneralHelper;
use mPDF;
use mpdfform;
use Log;
use ReplacePlaceHolder;
require_once __DIR__.'/../../public/ReplacePlaceHolder.php';
use App\PlaceHolders\ReplacePlaceHolderForPurchaseInvoiceReturn;


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/* * 922
 * Description of Helper
 *
 * @author Deepa
 */

class PdfHelper {

    public static function pdfGenerate($no)
    {
        $replace = new ReplacePlaceHolder;
        $replace_msg = $replace->placeHolderFun($no,'for_pdf','');       

        $mpdf = new Mpdf();
        $mpdf->WriteHTML($replace_msg);

        $serverName = $_SERVER['SERVER_NAME'];

        $filePath = "download/$serverName/pdf";
        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);

            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {

                if ($folderName == '')
                    $folderName = $folderList[$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index];

                if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        $filePath = getcwd() . "/" . $filePath;
        $mpdf->Output($filePath . '/invoice' . $no . '.pdf', 'F');
    }
     public static function pdfPreview($no)
    {
        $replace = new ReplacePlaceHolder;
        $replace_msg = $replace->placeHolderFun($no,'for_pdf','');       

        $mpdf = new Mpdf();
        $mpdf->WriteHTML($replace_msg);
        $mpdf->Output();
    }
    
     public static function purchaseReturnpdfPreview($id)
    {
         $replace = new ReplacePlaceHolderForPurchaseInvoiceReturn;
        $replace_msg = $replace->placeHolderFun($id,'for_pdf','','','');      

        $mpdf = new Mpdf();
        $mpdf->WriteHTML($replace_msg);
        $mpdf->Output();
    }
}

?>
