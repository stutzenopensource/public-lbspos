<?php

namespace App\Helper;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use DB;
use App\Helper\GeneralHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Helper
 *
 * @author Deepa
 */
class SmsHelper {

    //put your code here

    public static function invoiceCreatedNotification($invoice) {

        $get_template = DB::table("tbl_email_templates")
                ->where('is_active', '=', 1)
                ->where('tplname', '=', "invoice_created_sms")
                ->first();

        $msg = $get_template->message;
        //print_r($msg);

        $new_msg = str_replace("{{invoiceid}}", $invoice->invoice_code, $msg);

        
        $currency = GeneralHelper::setCurrency($invoice->total_amount);
        $totalAmount = $currency . "." . $invoice->total_amount;
        $new_msg = str_replace("{{invoice_amount}}", $totalAmount, $new_msg);

        $get_cus_phone = DB::table("tbl_customer")
                ->where('is_active', '=', 1)
                ->where('id', '=', $invoice->customer_id)
                ->first();
        $phone = '';
        if ($get_cus_phone != null) {
            $phone = $get_cus_phone->phone;
            $new_msg = str_replace("{{customername}}", $get_cus_phone->fname, $new_msg);
        }
        $phone = explode(",", $phone);
        for ($i = 0; $i < count($phone); $i++) {
            SmsHelper::sendSms($phone[$i], $new_msg);
        }
    }

    public static function invoicePaymentNotification($payment) {

        $get_template = DB::table("tbl_email_templates")
                ->where('is_active', '=', 1)
                ->where('tplname', '=', "invoice_payment_sms")
                ->first();

        //print_r($get_template);
        $msg = $get_template->message;
        //print_r($msg);
        $new_msg = str_replace("{{payment_id}}", $payment->id, $msg);        
        $curreny = GeneralHelper::setCurrency();
        $paymentAmount = $curreny . "." . $payment->amount;
        $new_msg = str_replace("{{payment_amount}}", $paymentAmount, $new_msg);

        $get_cus_phone = DB::table("tbl_customer")
                ->where('is_active', '=', 1)
                ->where('id', '=', $payment->customer_id)
                ->first();
        $phone = '';
        if ($get_cus_phone != null) {
            $phone = $get_cus_phone->phone;
            $new_msg = str_replace("{{customername}}", $get_cus_phone->fname, $new_msg);
        }


        $phone = explode(",", $phone);
        for ($i = 0; $i < count($phone); $i++) {
            SmsHelper::sendSms($phone[$i], $new_msg);
        }
    }

    public static function invoiceReminderNotification($id) {

        $get_template = DB::table("tbl_email_templates")
                ->where('is_active', '=', 1)
                ->where('tplname', '=', "invoice_reminder_sms")
                ->first();

        $current_date = Carbon::now();
        $current_date = $current_date->format('Y-m-d');

        $amount = DB::table('tbl_invoice')
                ->where('id', '=', $id)
                ->sum('total_amount');

        $paid_amount = DB::table('tbl_payment')
                ->where('date', "<", $current_date)
                ->where('invoice_id', '=', $id)
                ->sum('amount');


        $bal_amount = $amount - $paid_amount;

        $get_cus_id = DB::table("tbl_invoice")
                ->where('id', '=', $id)
                ->first();

        $cus_id = $get_cus_id->customer_id;

        $msg = $get_template->message;
        
        $curreny = GeneralHelper::setCurrency();
        $bal_amount = $curreny . "." . $bal_amount;
        

        $new_msg = str_replace("{{invoiceid}}", $get_cus_id->invoice_code, $msg);

        $new_msg = str_replace("{{invoice_amount}}", $bal_amount, $new_msg);
        $get_cus_phone = DB::table("tbl_customer")
                ->where('is_active', '=', 1)
                ->where('id', '=', $cus_id)
                ->first();
        $phone = $get_cus_phone->phone;

        $phone = explode(",", $phone);
        for ($i = 0; $i < count($phone); $i++) {
            SmsHelper::sendSms($phone[$i], $new_msg);
        }
    }

    public static function sendSms($phone, $content) {

        if (empty($phone) || empty($content)) {
            return;
        }

        $smsSetting = DB::table("tbl_app_config")
                ->where('setting', '=', "sms_gateway_url")
                ->first();
        if (empty($smsSetting) || empty($smsSetting->value)) {
            return;
        }
        $smsvalue = DB::table('tbl_app_config')
                        ->select('*')
                        ->where('setting', '=', 'sms_count')->first();
        $sms = $smsvalue->value;
        if($sms>0) {
        $url = str_replace("{{phoneno}}", $phone, $smsSetting->value);
        $url = str_replace("{{message}}", urlencode($content), $url);
                
        $con_status = SmsHelper::check_internet_connection('www.api.sendgrid.com');
        $app_stage = env('APP_STAGE');
        if ($app_stage == 'LIVE' && $con_status == 1) {
            try {

                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => $url,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_POSTFIELDS => "",
                ));
                $response = curl_exec($curl);
                $err = curl_error($curl);
                curl_close($curl);
                
                $send = DB::table('tbl_app_config')
                            ->where('setting', '=', 'sms_count')
                            ->decrement('value', 1);
            } catch (Guzzle\Http\Exception\ConnectException $e) {

                return "SMS NOT SEND";
            }
        }
    }
    }

    static function check_internet_connection($sCheckHost) {
        return (bool) @fsockopen($sCheckHost, 80, $iErrno, $sErrStr, 5);
    }
    
    public static function paymentDetails($sales, $type) {
    
        $currency = GeneralHelper::setCurrency();
        $thosandSeperator = GeneralHelper::getThosandSeperatorFormat();

        if ($type == "sales") {
            $get_template = DB::table("tbl_email_templates")
                    ->where('is_active', '=', 1)
                    ->where('tplname', '=', "sales_order_created_sms")
                    ->first();

            $msg = $get_template->message;

            $date = $sales['date']->format('Y-m-d');
            $totalAmount = $sales['total_amount'];
            $advance = DB::table("tbl_adv_payment")
                    ->select(DB::raw("SUM(amount) as amount"))
                    ->where('voucher_type', '=', "advance_invoice")
                    ->where('voucher_no', '=', $sales['id'])
                    ->where('is_active', '=', 1)
                    ->groupBy('voucher_no')
                    ->first();
            $advAmount = 0;
            $balance = 0;

            if ($advance != "") {
                $advAmount = $advance;
                $balance = $totalAmount - $advance;
            } else {
                $balance = $totalAmount;
            }

            $new_msg = str_replace("{{date}}", $date, $msg);
            $new_msg = str_replace("{{sales_order_no}}", $sales['sales_order_no'], $new_msg);
            $new_msg = str_replace("{{totalAmount}}", $currency . '.' . $totalAmount, $new_msg);
            $new_msg = str_replace("{{advAmt}}", $currency . '.' . $advAmount, $new_msg);
            $new_msg = str_replace("{{balance}}", $currency . '.' . $balance, $new_msg);
            $get_cus_phone = DB::table("tbl_customer")
                    ->where('is_active', '=', 1)
                    ->where('id', '=', $sales->customer_id)
                    ->first();
            $phone = '';
            if ($get_cus_phone != null) {
                $phone = $get_cus_phone->phone;
                $new_msg = str_replace("{{customername}}", $get_cus_phone->fname, $new_msg);
            }
            $phone = explode(",", $phone);
            for ($i = 0; $i < count($phone); $i++) {
                SmsHelper::sendSms($phone[$i], $new_msg);
            }
        } else if ($type == "advancePayment" && !empty($sales['voucher_no'])) {
            $get_templates = DB::table("tbl_email_templates")
                    ->where('is_active', '=', 1)
                    ->where('tplname', '=', "advance_payment_created_sms")
                    ->first();

            $msg = $get_templates->message;

            $totalAmount = $sales['amount'];
            
            $sales_order_id = DB::table("tbl_sales_order")
                    ->select('sales_order_no', 'total_amount')
                    ->where('id', '=', $sales['voucher_no'])
                    ->first();
            $advAmount = 0;
            $balance = 0;
            $advance = DB::table("tbl_adv_payment")
                    ->select(DB::raw("SUM(amount) as amount"))
                    ->where('voucher_type', '=', "advance_invoice")
                    ->where('voucher_no', '=', $sales['voucher_no'])
                    ->where('is_active', '=', 1)
                    ->groupBy('voucher_no')
                    ->first();

            if ($advance != "") {
                $balance = $sales_order_id->total_amount - $advance->amount;
            } else {
                $balance = $totalAmount;
            }

            $new_msg = str_replace("{{totalAmount}}", $currency . '.' . $totalAmount, $msg);
            $new_msg = str_replace("{{sales_order_no}}", $sales_order_id->sales_order_no, $new_msg);
            $new_msg = str_replace("{{balance}}", $currency . '.' . $balance, $new_msg);
            $get_cus_phone = DB::table("tbl_customer")
                    ->where('is_active', '=', 1)
                    ->where('id', '=', $sales->customer_id)
                    ->first();
            $phone = '';
            if ($get_cus_phone != null) {
                $phone = $get_cus_phone->phone;
                $new_msg = str_replace("{{customername}}", $get_cus_phone->fname, $new_msg);
            }
            $phone = explode(",", $phone);
            for ($i = 0; $i < count($phone); $i++) {
                SmsHelper::sendSms($phone[$i], $new_msg);
            }
        } else if ($type == "advancePayment" && $sales['voucher_no'] == 0) {
            $get_templates = DB::table("tbl_email_templates")
                    ->where('is_active', '=', 1)
                    ->where('tplname', '=', "direct_payment_created_sms")
                    ->first();

            $msg = $get_templates->message;

            $totalAmount = $sales['amount'];
            

            $new_msg = str_replace("{{totalAmount}}", $currency . '.' . $totalAmount, $msg);
            $get_cus_phone = DB::table("tbl_customer")
                    ->where('is_active', '=', 1)
                    ->where('id', '=', $sales->customer_id)
                    ->first();
            $phone = '';
            if ($get_cus_phone != null) {
                $phone = $get_cus_phone->phone;
                $new_msg = str_replace("{{customername}}", $get_cus_phone->fname, $new_msg);
            }
            $phone = explode(",", $phone);
            for ($i = 0; $i < count($phone); $i++) {
                SmsHelper::sendSms($phone[$i], $new_msg);
            }
        }
    }

    public static function dayCloseSms($request, $type, $invoice) {

        $currency = GeneralHelper::setCurrency();
         $get_template = DB::table("tbl_email_templates")
                    ->where('is_active', '=', 1)
                    ->where('tplname', '=', "day_close_sms")
                    ->first();
        $current_date = date('d-m-Y');
        $timestamp = date('H:i:s');
        $timestamp = strtotime($timestamp);
        if ($type == 1) {
            $msg = $get_template->message;
            $new_msg = str_replace("{{date}}", $current_date, $msg);
            $new_msg = str_replace("{{cash}}", $request->cash, $new_msg);
            $new_msg = str_replace("{{card}}", $request->card, $new_msg);
            $new_msg = str_replace("{{advance}}", $request->advance, $new_msg);
            $new_msg = str_replace("{{others}}", $request->others, $new_msg);
            $new_msg = str_replace("{{count}}", $invoice->count, $new_msg);
            $new_msg = str_replace("{{total}}", $invoice->total, $new_msg);
            $get_phone = DB::table("tbl_app_config")
                    ->where('setting', '=', "day_close_sms")
                    ->first();
            $phone = '';
            if ($get_phone != null) {
                 $phone = $get_phone->value;
            }
            $phone = explode(",", $phone);
            for ($i = 0; $i < count($phone); $i++) {
                SmsHelper::sendSms($phone[$i], $new_msg);
            }
        } else if ($type == 0) {
             $msg = $get_template->message;
            $new_msg = str_replace("{{date}}", $current_date, $msg);
            $new_msg = str_replace("{{cash}}", 0, $new_msg);
            $new_msg = str_replace("{{card}}", 0, $new_msg);
            $new_msg = str_replace("{{advance}}", 0, $new_msg);
            $new_msg = str_replace("{{others}}", 0, $new_msg);
            $new_msg = str_replace("{{count}}", 0, $new_msg);
            $new_msg = str_replace("{{total}}", 0, $new_msg);
            $get_phone = DB::table("tbl_app_config")
                    ->where('setting', '=', "day_close_sms")
                    ->first();
            $phone = '';
            if ($get_phone != null) {
                $phone = $get_phone->value;
            }
            $phone = explode(",", $phone);
            for ($i = 0; $i < count($phone); $i++) {
                SmsHelper::sendSms($phone[$i], $new_msg);
            }
        }
    }

}

?>
