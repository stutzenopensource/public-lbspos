<?php

namespace App\Helper;

use Carbon\Carbon;
use DB;
use App\Transaction;
use Illuminate\Support\Facades\Auth;
Use App\Helper\GeneralHelper;
use PDO;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AccountHelper
 *
 * @author Deepa
 */
class TransactionHelper {

    //Have to work on voucherType
    public static function transactionSave($transactionData) {
        if (array_key_exists("id", $transactionData)) {
            $transaction = Transaction::findOrFail($transactionData['id']);
        } else {
            $transaction = new Transaction;
        }


        $mydate = date("Y/m/d");
        $month = date("m", strtotime($mydate));
        $fiscalMonthCollection = DB::table('tbl_fiscal_month')->where('code', "=", $month)->get();

        $transaction->fiscal_month_code = $month;
        if ($fiscalMonthCollection->count() > 0) {
            $fiscalMonth = $fiscalMonthCollection->first();
            $transaction->fiscal_month_code = $fiscalMonth->id;
        }
        $year = date("Y", strtotime($mydate));
        $fiscalYearCollection = DB::table('tbl_fiscal_year')->where('is_active', 1)->get();

        $transaction->fiscal_year_id = 0;
        $transaction->fiscal_year_code = $year;
        if ($fiscalYearCollection->count() > 0) {
            $fiscalYear = $fiscalYearCollection->first();
            $transaction->fiscal_year_id = $fiscalYear->id;
            $transaction->fiscal_year_code = $fiscalYear->code;
        }
        $vt = $transactionData['voucher_type'];
        //  print_r("SAd".$vt);
        //$transaction->voucher_type = Transaction::$DIRECT_EXPENSE;
        //  $transaction->voucher_type = Transaction::. '$' .$vt;
        //   $transactionData = $transactionData[0]->toArray();
        $transaction->fill($transactionData);
        $transaction->save();

        //ForchildTransction
            
        return $transaction;
    }
            

    public static function transactionDelete($refId, $refType, $type) {
        $currentuser = Auth::user();

        $transaction = Transaction::where('voucher_number', "=", $refId)
                ->where('voucher_type', "=", $refType)
                ->first();


        if ($type == 1) {
            DB::table('tbl_transaction')->where('voucher_type', $refType)->where('voucher_number', $refId)->delete();
        } else {
            DB::table('tbl_transaction')->where('voucher_number', '=', $refId)->where('voucher_type', '=', $refType)
                    ->update(['is_active' => 0, 'updated_by' => $currentuser->id]);
        }
     }
            

    public static function transactionDeleteByPrimary($id) {
        $currentuser = Auth::user();

        $transaction = Transaction::where('id', "=", $id)
                ->first();

        DB::table('tbl_transaction')->where('id', '=', $id)
                ->update(['is_active' => 0, 'updated_by' => $currentuser->id]);

     }
 
}
?>
