<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class HsnCode extends Model {

    protected $table = 'tbl_hsn_code';
    protected $fillable = ['id','hsn_code','description','type','is_active','comments','created_by','updated_by','is_approved'];
    protected $dates = ['created_at','updated_at' ];
}

?>
