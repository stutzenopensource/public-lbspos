<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class HsnTaxMapping extends Model {
    //put your code here
    protected $table = 'tbl_hsn_tax_mapping';
    protected $fillable = ['id','group_tax_id','from_price','to_price','hsn_code','description','type','hsn_id', 'is_active','comments','created_by','updated_by'];
    protected $dates = ['created_at','updated_at' ];
}

?>
