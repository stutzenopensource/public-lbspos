<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\Account;
use App\Acode;
use App\Transaction;
use Illuminate\Http\Request;
use App\Transformer\UomTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\LogHelper;
use App\Helper\NotificationHelper;
use App\Helper\TransactionHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AccountsController
 *
 * @author Deepa
 */
class AccountController extends Controller {

    //put your code here
    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Account Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
//        $validator = Validator::make($request->all(), [
//                    'account' => 'required|unique:tbl_accounts,account'
//        ]);
//        if ($validator->fails()) {
//            $resVal['success'] = FALSE;
//            $resVal['message'] = 'Error In Request Format';
//            if (array_key_exists('account', $validator->failed())) {
//                $resVal['message'] = 'Account Name Is Already Exist';
//            }
//
//            return $resVal;
//        }
        
        if ($request->input('account') != NULL) 
        {
        $account_name = Account::where('account', '=', $request->input('account'))
                    ->where('is_active','=',1)->get();
        if (count($account_name) > 0)
        {
                $resVal['success'] = FALSE;
                $resVal['message'] = 'Account Name Is Already Exists';
                return $resVal;
        }
        }

        $currentuser = Auth::user();

        $item = new Account;

        $item->created_by = $currentuser->id;
        $item->updated_by = $currentuser->id;
        $item->fill($request->all());
        $item->save();


        $resVal['id'] = $item->id;

        $mydate = date("Y/m/d");
        $month = date("m", strtotime($mydate));
        $currentUserId = $currentuser->id;
        $currentUserName = $currentuser->f_name . ' ' . $currentuser->l_name;
        $refId = $item->id;

        $transactionData = array(
            "name" => $currentuser->f_name,
            "transaction_date" => $mydate,
            "voucher_type" => "inital_balance",
            "voucher_number" => 0,
            "credit" => $request->input('balance'),
            "debit" => 0,
            "pmtcode" => $item->acode,
            "pmtcoderef" => $item->id,
            "account_id" => $item->id,
            "is_cash_flow" => 1, //have to check  account in save and voucher type too
            "account" => $request->input('account'),
            "acode" => NULL,
            "acoderef" => NULL,
            "particulars" => "Initial Balance of ".$item->account,
            "created_by" => $currentUserId,
            "updated_by" => $currentUserId,
            "created_by_name" => $currentUserName,
            "updated_by_name" => $currentUserName,
            "is_active" => 1,
        );
        TransactionHelper::transactionSave($transactionData);


        LogHelper::info1('Account Save ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Account', 'save', $screen_code, $item->id);


        return $resVal;
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Account Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $transaction = DB::table('tbl_transaction')
                        ->select('voucher_type', 'id')
                        ->where('account_id', '=', $id)
                        ->where('is_active', '=', 1)
                        ->where('voucher_type', '!=', 'inital_balance')
                        ->orderby('id', 'desc')->get();
        if (count($transaction) > 0) {
            $resVal['success'] = False;
            $resVal['message'] = "Transaction Process Started. Can't Delete The Account";
            return $resVal;
        }
        try {
            $item = Account::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Accounts Not Found';
            return $resVal;
        }

        $item->delete();

        LogHelper::info1('Account Delete ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Account', 'delete', $screen_code, $item->id);


        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $account = $request->input('account');
        $bank_name = $request->input('bank_name', '');
        $account_number = $request->input('account_number');
        $pos_visibility = $request->input('pos_visibility');
        $builder = DB::table('tbl_accounts')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if (!empty($account)) {
            $builder->where('account', 'like', '%' . $account . '%');
        }
        if (!empty($bank_name)) {
            $builder->where('bank_name', 'like', '%' . $bank_name . '%');
        }
        if (!empty($account_number)) {
            $builder->where('account_number', '=', $account_number);
        }
        if ($pos_visibility != '') {
            $builder->where('pos_visibility', '=', $pos_visibility);
        }
        if ($pos_visibility == "all") {
            $builder->where('pos_visibility', '=', 0)
                    ->orWhere('pos_visibility', '=', 1);
        }
        $builder->orderBy('id', 'desc');
        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {

            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }

        LogHelper::info1('Account List All ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));

        return ($resVal);
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Accounts Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        if ($request->input('account') != NULL) {
            $account_name = Account::where('account', "=", $request->input('account'))
                            ->where('is_active', '=', 1)
                            ->where('id', '!=', $id)->get();
            if (count($account_name) > 0) {
                $resVal['success'] = FALSE;
                $resVal['message'] = 'Account Name Is Already Exists';
                return $resVal;
            }
        }

        try {
            $item = Account::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Account Not Found';
            return $resVal;
        }
        $item->created_by = $currentuser->id;
        $item->fill($request->all());
        $item->save();

        LogHelper::info1('Account Update ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Account', 'update', $screen_code, $item->id);



        return $resVal;
    }

    public function checkAccountTransaction(Request $request) {
        $resVal = array();
        $resVal['success'] = True;
        $resVal['message'] = 'Success';

        $id = $request->input('account_id', '');

        $transaction = DB::table('tbl_transaction')
                        ->select('voucher_type', 'id')
                        ->where('account_id', '=', $id)
                        ->where('is_active', '=', 1)
                        ->where('voucher_type', '!=', 'inital_balance')
                        ->orderby('id', 'desc')->get();
        if (count($transaction) > 0) {
            $count = count($transaction);
            $resVal['count'] = count($transaction);
            $resVal['success'] = False;
            $resVal['message'] = "Account Has Been $count Transaction";
            return $resVal;
        }
        LogHelper::info1('check Account transaction ' . $request->fullurl(), $request->all());
        return $resVal;
    }

}

?>
