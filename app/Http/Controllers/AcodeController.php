<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\Acode;
use App\Transaction;
use Illuminate\Http\Request;
use App\Transformer\UomTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\LogHelper;
use App\Helper\NotificationHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AcodeController
 *
 * @author Stutzen
 */
class AcodeController extends Controller {

    //put your code here
    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Acode Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $validator = Validator::make($request->all(), [
                    'code' => 'required|unique:tbl_acode,code'
        ]);
        if ($validator->fails()) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Error In Request Format';
            if (array_key_exists('code', $validator->failed())) {
                $resVal['message'] = 'Acode Name Is Already Exist';
            }

            return $resVal;
        }

        $currentuser = Auth::user();

        $item = new Acode;

        $item->created_by = $currentuser->id;
        $item->updated_by = $currentuser->id;
        $item->fill($request->all());
        $item->save();

        $resVal['id'] = $item->id;
        LogHelper::info1('Acode Save ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Acode', 'save', $screen_code, $item->id);
        return $resVal;
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Acode Deleted Successfully';
        $currentuser = Auth::user();
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        try {
            $item = Acode::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Acode Not Found';
            return $resVal;
        }

        $item->updated_by = $currentuser->id;
        $item->is_active = 0;
        $item->save();
        LogHelper::info1('Acode Delete ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Acode', 'delete', $screen_code, $item->id);
        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $code = $request->input('code');
        $ref_type = $request->input('ref_type', '');
        $ref_id = $request->input('ref_id');
        $is_active = $request->input('is_active');
        
        $builder = DB::table('tbl_acode')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if (!empty($code)) {
            $builder->where('code', 'like', '%' . $code . '%');
        }
        if (!empty($ref_type)) {
            $builder->where('ref_type', $ref_type);
        }
        if (!empty($ref_id)) {
            $builder->where('ref_id', '=', $ref_id);
        }
        if ($is_active != '') {
            $builder->where('is_active', '=', $is_active);
        }

        $builder->orderBy('id', 'desc');
        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {

            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }
        LogHelper::info1('Acode List ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
       
        return ($resVal);
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Acode Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $item = Acode::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Acode Not Found';
            return $resVal;
        }
        $item->created_by = $currentuser->id;
        $item->fill($request->all());
        $item->save();
        LogHelper::info1('Acode Update ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Acode', 'update', $screen_code, $item->id);
        return $resVal;
    }

}

?>
