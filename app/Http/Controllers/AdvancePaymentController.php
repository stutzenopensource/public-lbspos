<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\AdvancePayment;
use App\Payment;
use App\Transaction;
use Illuminate\Http\Request;
use App\Transformer\UomTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Http\Controllers\Carbon;
use App\Helper\AccountHelper;
use App\Helper\SalesInvoiceHelper;
use App\Helper\SmsHelper;
use App\Helper\TransactionHelper;
use App\Helper\GeneralHelper;
use App\Helper\LogHelper;
use App\Helper\NotificationHelper;

class AdvancePaymentController extends Controller {

    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Advance Payment Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        $advance_payment = new AdvancePayment;
        $advance_payment->created_by = $currentuser->id;
        $advance_payment->updated_by = $currentuser->id;
        $advance_payment->fill($request->all());


        //Finding DayCode
        $dayLog = DB::table('tbl_day_log')->select('*')
                        ->whereRaw('id = (select max(`id`) from tbl_day_log)')->first();
        if ($dayLog != "")
            $advance_payment->day_code = $dayLog->id;

        $advance_payment->save();

        //This is for save payment when vouher type is invoice_payment
        //Transaction save
        //  $transaction = new Transaction;

        $voch_type = $advance_payment->voucher_type;


        //AdvancePaymentController::transactionSave($advance_payment, $transaction, $currentuser);
        //Transaction save


        $currentUserId = $currentuser->id;
        $currentUserName = $currentuser->f_name . ' ' . $currentuser->l_name;

        if ($voch_type == 'creditNote' || $voch_type == 'debitNote') {

            $customer_id = $request->input('customer_id', '');
            $account_id12 = GeneralHelper::getCustomerAccountId($customer_id);
            $account = GeneralHelper::getCustomerAccount($account_id12);
            $advance_payment->save();

            $account_id = $advance_payment->account_id;
            $account = $advance_payment->account;
            $pmtcode = GeneralHelper::getPaymentAcode($account_id12);
            $pmtcoderef = $account_id12;
        } else {
            $account = $request->input('account', '');
            $account_id = $request->input('account_id', '');
            $pmtcode = GeneralHelper::getPaymentAcode($request->input('account_id', ''));
            $pmtcoderef = $request->input('account_id', '');
        }

        if ($advance_payment->voucher_type == 'advance_invoice') {
            $credit = $advance_payment->amount;
            $debit = 0.00;
            $particulars = 'Advance Payment amount is received ';
        } else if ($advance_payment->voucher_type == 'debitNote') { // sales Invoice
            $debit = 0.00;
            $credit = $advance_payment->amount;
            $particulars = 'Debit Note received ';

            AccountHelper::deposit($account_id, $advance_payment->amount);

            $contra_account_id = GeneralHelper::getContraAccountId($currentuser, 'debitNote');
            $contra_account = GeneralHelper::getCustomerAccount($contra_account_id);

            AccountHelper::withDraw($contra_account_id, $advance_payment->amount);

            $con_account_id = $contra_account_id;
            $con_account = $contra_account;
            $con_pmtcode = GeneralHelper::getPaymentAcode($contra_account_id);
            $con_pmtcoderef = $contra_account_id;
            $con_voucher_number = $advance_payment->id;

            $con_credit = 0.00;
            $con_debit = $advance_payment->amount;
            $con_particulars = 'Debit Note ContraEntry ';
        } else if ($advance_payment->voucher_type == 'creditNote') { // purchase Invoice
            $credit = 0.00;
            $debit = $advance_payment->amount;
            $particulars = 'Credit Note received';
            AccountHelper::withDraw($account_id, $advance_payment->amount);


            $contra_account_id = GeneralHelper::getContraAccountId($currentuser, 'creditNote');
            $contra_account = GeneralHelper::getCustomerAccount($contra_account_id);
            AccountHelper::deposit($contra_account_id, $advance_payment->amount);

            $con_account_id = $contra_account_id;
            $con_account = $contra_account;
            $con_pmtcode = GeneralHelper::getPaymentAcode($contra_account_id);
            $con_pmtcoderef = $contra_account_id;
            $con_debit = 0.00;
            $con_credit = $advance_payment->amount;
            $con_particulars = 'Credit Note ContraEntry';
        } else {        //advance_purchase
            $credit = 0.00;
            $debit = $advance_payment->amount;
            $particulars = 'Advance Payment amount is paid ';
        }

        $transactionData = array(
            "name" => $currentUserName,
            "transaction_date" => $advance_payment->date,
            "voucher_type" => $advance_payment->voucher_type,
            "voucher_number" => $advance_payment->id,
            "credit" => $credit,
            "debit" => $debit,
            "pmtcode" => $pmtcode,
            "pmtcoderef" => $pmtcoderef,
            "is_cash_flow" => 1,
            "account" => $account,
            "account_category" => $request->input('tra_category', ''),
            "payment_mode" => $advance_payment->payment_mode,
            "account_id" => $account_id,
            "customer_id" => $advance_payment->customer_id,
            "prefix" => $advance_payment->prefix,
            "acode" => GeneralHelper::getCustomerAcode($advance_payment->customer_id),
            "acoderef" => $request->input('customer_id', ''),
            "particulars" => $particulars,
            "created_by" => $currentUserId,
            "updated_by" => $currentUserId,
            "created_by_name" => $currentUserName,
            "updated_by_name" => $currentUserName,
            "is_active" => 1,
        );
        TransactionHelper::transactionSave($transactionData);
        if ($voch_type == 'creditNote' || $voch_type == 'debitNote') {
            $ContraData = array(
                "name" => $currentUserName,
                "transaction_date" => $advance_payment->date,
                "voucher_type" => $advance_payment->voucher_type,
                "voucher_number" => $advance_payment->id,
                "credit" => $con_credit,
                "debit" => $con_debit,
                "pmtcode" => $con_pmtcode,
                "pmtcoderef" => $con_pmtcoderef,
                "is_cash_flow" => 1,
                "account" => $con_account,
                "account_category" => $request->input('tra_category', ''),
                "payment_mode" => $advance_payment->payment_mode,
                "account_id" => $con_account_id,
                "customer_id" => $advance_payment->customer_id,
                "prefix" => $advance_payment->prefix,
                "acode" => GeneralHelper::getCustomerAcode($advance_payment->customer_id),
                "acoderef" => $request->input('customer_id', ''),
                "particulars" => $con_particulars,
                "created_by" => $currentUserId,
                "updated_by" => $currentUserId,
                "created_by_name" => $currentUserName,
                "updated_by_name" => $currentUserName,
                "is_active" => 1,
            );
            TransactionHelper::transactionSave($ContraData);
        }

        if ($advance_payment->voucher_type == 'invoice_payment') { // sales Invoice
            $payment = new Payment;
            $payment->date = $advance_payment->date;
            $payment->comments = "Invoice Payment";
            $payment->amount = $advance_payment->amount;
            $payment->tra_category = $advance_payment->tra_category;
            $payment->customer_id = $advance_payment->customer_id;
            $payment->account = $advance_payment->account;
            $payment->account_id = $advance_payment->account_id;
            $payment->payment_mode = $advance_payment->payment_mode;
            $payment->invoice_id = $advance_payment->voucher_no;
            $payment->cheque_no = $advance_payment->reference_no;
            $payment->adv_payment_id = $advance_payment->id;
            $payment->is_active = 1;
            $payment->created_by = $currentuser->id;
            $payment->updated_by = $currentuser->id;
            $payment->save();
            SalesInvoiceHelper::updateInvoiceStatus($advance_payment->voucher_no);
            //Update Sales Order status 
            $get_sales_id = DB::table('tbl_invoice')->where('is_active', 1)
                            ->where('id', '=', $advance_payment->voucher_no)->first();
            if (!empty($get_sales_id->quote_id)) {
                SalesInvoiceHelper::updateSalesOrderStatus($get_sales_id->quote_id);
            }
        }



        if ($advance_payment->voucher_type == 'advance_invoice') {

            //Update Account Balance
            AccountHelper::deposit($advance_payment->account_id, $advance_payment->amount);
            //Update advance amount balance amount
            SalesInvoiceHelper::updateAdvPaymentStatusAndBalance($advance_payment->id);
        } if ($advance_payment->voucher_type == 'advance_purchase') {
            //Update Account Balance
            AccountHelper::withDraw($advance_payment->account_id, $advance_payment->amount);
            //Update advance amount balance amount
            SalesInvoiceHelper::updateAdvPaymentStatusAndBalance($advance_payment->id);
        }



        $resVal['id'] = $advance_payment->id;
        LogHelper::info1('Advance Payment Save ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Advance Payment', 'save', $screen_code, $advance_payment->id);

        return $resVal;
    }

    public function creditDebtUpdate(Request $request, $id) {
        $resVal = array();
        $resVal['message'] = 'Advance Payment Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        $currentUserId = $currentuser->id;
        $currentUserName = $currentuser->f_name . ' ' . $currentuser->l_name;
        try {
            $advance_payment = AdvancePayment::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Advance Payment Not Found';
            return $resVal;
        }

        //This is for save payment when vouher type is invoice_payment
        //Transaction save

        $voch_type = $advance_payment->voucher_type;
        $adv_acc_id = $advance_payment->account_id;
        $adv_amount = $advance_payment->amount;

        if ($voch_type == 'creditNote') {
            AccountHelper::deposit($adv_acc_id, $adv_amount);
            $contra_account_id = GeneralHelper::getContraAccountId($currentuser, 'creditNote');
            AccountHelper::withDraw($contra_account_id, $advance_payment->amount);
        }
        if ($voch_type == 'debitNote') {
            AccountHelper::withDraw($adv_acc_id, $adv_amount);
            $contra_account_id = GeneralHelper::getContraAccountId($currentuser, 'debitNote');
            AccountHelper::deposit($contra_account_id, $advance_payment->amount);
        }

        TransactionHelper::transactionDelete($id, $voch_type, 1);



        $advance_payment->updated_by = $currentuser->id;
        $advance_payment->fill($request->all());
        $advance_payment->save();
        //Transaction save
        $transaction = new Transaction;
        $mydate = $advance_payment->date;
        $month = date("m", strtotime($mydate));
        $transaction->fiscal_month_code = $month;

        $fiscalMonthCollection = DB::table('tbl_fiscal_month')->where('code', "=", $month)->get();
        if ($fiscalMonthCollection->count() > 0) {
            $fiscalMonth = $fiscalMonthCollection->first();
            $transaction->fiscal_month_code = $fiscalMonth->id;
        }
        $year = date("Y", strtotime($mydate));
        $fiscalYearCollection = DB::table('tbl_fiscal_year')->where('is_active', 1)->get();
        $transaction->fiscal_year_id = 0;
        $transaction->fiscal_year_code = $year;
        if ($fiscalYearCollection->count() > 0) {
            $fiscalYear = $fiscalYearCollection->first();
            $transaction->fiscal_year_id = $fiscalYear->id;
            $transaction->fiscal_year_code = $fiscalYear->code;
        }
        $transaction->transaction_date = $advance_payment->date;
        $transaction->voucher_type = $advance_payment->voucher_type;
        $transaction->voucher_number = $advance_payment->id;
        $contraTrans = new Transaction;
        $contraTrans = clone $transaction;

        if ($voch_type == 'creditNote' || $voch_type == 'debitNote') {

            $customer_id = $request->input('customer_id', '');
            $account_id12 = GeneralHelper::getCustomerAccountId($customer_id);
            $account = GeneralHelper::getCustomerAccount($account_id12);

            $advance_payment->save();

            $account_id = $advance_payment->account_id;
            $account = $advance_payment->account;
            $pmtcode = GeneralHelper::getPaymentAcode($account_id12);
            $pmtcoderef = $account_id12;
        } else {
            $account = $request->input('account', '');
            $account_id = $request->input('account_id', '');
            $pmtcode = GeneralHelper::getPaymentAcode($request->input('account_id', ''));
            $pmtcoderef = $request->input('account_id', '');
        }

        if ($advance_payment->voucher_type == 'advance_invoice') {
            $credit = $advance_payment->amount;
            $debit = 0.00;
            $particulars = 'Advance Payment amount is received ';
        } else if ($advance_payment->voucher_type == 'debitNote') { // sales Invoice
            $debit = 0.00;
            $credit = $advance_payment->amount;
            $particulars = 'Debit Note received ';

            AccountHelper::deposit($account_id, $advance_payment->amount);

            $contra_account_id = GeneralHelper::getContraAccountId($currentuser, 'debitNote');
            $contra_account = GeneralHelper::getCustomerAccount($contra_account_id);

            AccountHelper::withDraw($contra_account_id, $advance_payment->amount);

            $con_account_id = $contra_account_id;
            $con_account = $contra_account;
            $con_pmtcode = GeneralHelper::getPaymentAcode($contra_account_id);
            $con_pmtcoderef = $contra_account_id;
            $con_voucher_number = $advance_payment->id;

            $con_credit = 0.00;
            $con_debit = $advance_payment->amount;
            $con_particulars = 'Debit Note ContraEntry ';
        } else if ($advance_payment->voucher_type == 'creditNote') { // purchase Invoice
            $credit = 0.00;
            $debit = $advance_payment->amount;
            $particulars = 'Credit Note received';
            AccountHelper::withDraw($account_id, $advance_payment->amount);


            $contra_account_id = GeneralHelper::getContraAccountId($currentuser, 'creditNote');
            $contra_account = GeneralHelper::getCustomerAccount($contra_account_id);
            AccountHelper::deposit($contra_account_id, $advance_payment->amount);

            $con_account_id = $contra_account_id;
            $con_account = $contra_account;
            $con_pmtcode = GeneralHelper::getPaymentAcode($contra_account_id);
            $con_pmtcoderef = $contra_account_id;
            $con_debit = 0.00;
            $con_credit = $advance_payment->amount;
            $con_particulars = 'Credit Note ContraEntry';
        } else {        //advance_purchase
            $credit = 0.00;
            $debit = $advance_payment->amount;
            $particulars = 'Advance Payment amount is paid ';
        }

        $transactionData = array(
            "name" => $currentUserName,
            "transaction_date" => $advance_payment->date,
            "voucher_type" => $advance_payment->voucher_type,
            "voucher_number" => $advance_payment->id,
            "credit" => $credit,
            "debit" => $debit,
            "pmtcode" => $pmtcode,
            "pmtcoderef" => $pmtcoderef,
            "is_cash_flow" => 1,
            "account" => $account,
            "account_category" => $request->input('tra_category', ''),
            "payment_mode" => $advance_payment->payment_mode,
            "account_id" => $account_id,
            "customer_id" => $advance_payment->customer_id,
            "prefix" => $advance_payment->prefix,
            "acode" => GeneralHelper::getCustomerAcode($advance_payment->customer_id),
            "acoderef" => $request->input('customer_id', ''),
            "particulars" => $particulars,
            "created_by" => $currentUserId,
            "updated_by" => $currentUserId,
            "created_by_name" => $currentUserName,
            "updated_by_name" => $currentUserName,
            "is_active" => 1,
        );
        TransactionHelper::transactionSave($transactionData);
        if ($voch_type == 'creditNote' || $voch_type == 'debitNote') {
            $ContraData = array(
                "name" => $currentUserName,
                "transaction_date" => $advance_payment->date,
                "voucher_type" => $advance_payment->voucher_type,
                "voucher_number" => $advance_payment->id,
                "credit" => $con_credit,
                "debit" => $con_debit,
                "pmtcode" => $con_pmtcode,
                "pmtcoderef" => $con_pmtcoderef,
                "is_cash_flow" => 1,
                "account" => $con_account,
                "account_category" => $request->input('tra_category', ''),
                "payment_mode" => $advance_payment->payment_mode,
                "account_id" => $con_account_id,
                "customer_id" => $advance_payment->customer_id,
                "prefix" => $advance_payment->prefix,
                "acode" => GeneralHelper::getCustomerAcode($advance_payment->customer_id),
                "acoderef" => $request->input('customer_id', ''),
                "particulars" => $con_particulars,
                "created_by" => $currentUserId,
                "updated_by" => $currentUserId,
                "created_by_name" => $currentUserName,
                "updated_by_name" => $currentUserName,
                "is_active" => 1,
            );
            TransactionHelper::transactionSave($ContraData);
        }

        $resVal['id'] = $advance_payment->id;
        LogHelper::info1('Credit Debit Update ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Credit Debit', 'save', $screen_code, $advance_payment->id);
        return $resVal;
    }

    public function transactionSave($advance_payment, $transaction, $currentuser) {
        $transaction->account_category = $advance_payment->tra_category;
        $transaction->customer_id = $advance_payment->customer_id;
        $transaction->acode = GeneralHelper::getCustomerAcode($advance_payment->customer_id);
        $transaction->acoderef = $advance_payment->customer_id;
        $transaction->payment_mode = $advance_payment->payment_mode;
        $transaction->is_active = 1;
        $transaction->created_by = $currentuser->id;
        $transaction->updated_by = $currentuser->id;
        $transaction->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $transaction->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $transaction->prefix = $advance_payment->prefix;
        $transaction->save();
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $id = $request->input('id');
        $customer_id = $request->input('customer_id');
        $created_by = $request->input('created_by');
        $created_by_id = $request->input('created_by_id');
        $amount = $request->input('amount');
        $account = $request->input('account');
        $payment_mode = $request->input('payment_mode');
        $sales_payment = $request->input('sales_payment');
        $status = $request->input('status');
        $voucher = $request->input('voucher_type');
        $voucher_no = $request->input('voucher_no');
        $is_active = $request->input('is_active');
        $mode = $request->input('mode');
        $purchase_invoice_id = $request->input('purchase_invoice_id');
        $invoice_id = $request->input('invoice_id');
        $prefix = $request->input('prefix', '');

        $from_date = $request->input('from_date', '');
        $to_date = $request->input('to_date', '');
        $type = $request->input('type', '');

        $builder = DB::table('tbl_adv_payment as p')
                ->leftJoin('tbl_customer as c', 'p.customer_id', '=', 'c.id')
                ->leftJoin('tbl_user as u', 'p.created_by', '=', 'u.id')
                ->leftjoin('tbl_purchase_payment as pp', 'pp.id', '=', 'p.voucher_no')
                ->leftjoin('tbl_payment as pa', 'pa.id', '=', 'p.voucher_no')
                ->select('p.*', 'c.fname', 'u.f_name as created_by_user', DB::raw("(CASE WHEN p.voucher_type='purchase_invoice_payment' THEN pp.purchase_invoice_id  WHEN p.voucher_type='sales_invoice_payment' THEN pa.invoice_id END) as ref_id"));


        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($created_by)) {
            $builder->where('u.f_name', 'like', $created_by . '%');
        }
        if (!empty($created_by_id)) {
            $builder->where('p.created_by', '=', $created_by_id);
        }
        if (!empty($account)) {
            $builder->where('p.account_id', '=', $account);
        }
        if (!empty($payment_mode)) {
            $builder->where('p.payment_mode', 'like', $payment_mode . '%');
        }
        if (!empty($amount)) {
            $builder->where('p.amount', '=', $amount);
        }
        if (!empty($prefix)) {
            $builder->where('p.prefix', '=', $prefix);
        }
        //advance invoice to advance payment
        if ($sales_payment == 1) {
            $builder->where('p.voucher_type', '=', 'advance_invoice');
        } else if ($sales_payment == 2) {
            $builder->where('p.voucher_type', '=', 'advance_purchase');
        }
        if (!empty($id)) {
            $builder->where('p.id', '=', $id);
        }
        if (!empty($customer_id)) {
            $builder->where('p.customer_id', '=', $customer_id);
        }
        if (!empty($purchase_invoice_id)) {
            $builder->where('pp.purchase_invoice_id', '=', $purchase_invoice_id);
        }
        if (!empty($invoice_id)) {
            $builder->where('pa.invoice_id', '=', $invoice_id);
        }
        if (!empty($status)) {
            $builder->where('p.status', '=', $status);
        }

        if (!empty($voucher)) {
            $builder->where('p.voucher_type', '=', $voucher);
        }
        if (!empty($is_active)) {
            $builder->where('p.is_active', '=', $is_active);
        }
        if (!empty($voucher_no)) {
            $builder->where('p.voucher_no', '=', $voucher_no);
        }
        if (!empty($status)) {
            $builder->where('p.status', '=', $status);
        }

        if ($type == 1) {
            $builder->whereRaw(DB::raw("(p.voucher_type = 'advance_invoice' or  p.voucher_type = 'sales_invoice_payment') "));
        } else if ($type == 2) {
            $builder->whereRaw(DB::raw("(p.voucher_type = 'purchase_invoice_payment' or  p.voucher_type = 'advance_purchase') "));
        }
        if (!empty($from_date)) {
            $builder->whereDate('p.date', '>=', $from_date);
        }

        if (!empty($to_date)) {

            $builder->whereDate('p.date', '<=', $to_date);
        }
        
        if (!empty($mode)) {
            if ($mode == 2)
                $builder->where('p.balance_amount', '>', 0);
        }
        $builder->orderBy('p.id', 'desc');

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $paymentCollection = $builder->get();
        } else {

            $paymentCollection = $builder->skip($start)->take($limit)->get();
        }
//        LogHelper::info('Advance Payment Filters' . $request->fullurl());
//        LogHelper::info('Advance Payment List' . $paymentCollection);
        $resVal['list'] = $paymentCollection;
        LogHelper::info1('Advance Payment List All ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Advance Payment Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $advance_payment = AdvancePayment::findOrFail($id);
            if ($advance_payment->is_active == 0) {
                return $resVal;
            }
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Advance Payment List Not Found';
            return $resVal;
        }
         $voucher_no = $advance_payment->voucher_no;
         
        if ($voucher_no != 0 ) {
          $resVal['success'] = FALSE;
          $resVal['message'] = 'Unable To Delete.This Is Advance For Return Amount';
          return $resVal;
        }
        
        $voch_type = $advance_payment->voucher_type;
        $adv_acc_id = $advance_payment->account_id;
        $adv_amount = $advance_payment->amount;

        if ($voch_type == 'creditNote') {
            AccountHelper::deposit($adv_acc_id, $adv_amount);
            $contra_account_id = GeneralHelper::getContraAccountId($currentuser, 'creditNote');
            AccountHelper::withDraw($contra_account_id, $advance_payment->amount);
        }
        if ($voch_type == 'debitNote') {
            AccountHelper::withDraw($adv_acc_id, $adv_amount);
            $contra_account_id = GeneralHelper::getContraAccountId($currentuser, 'debitNote');
            AccountHelper::deposit($contra_account_id, $advance_payment->amount);
        }
        if ($voch_type == 'creditNote' || $voch_type == 'debitNote') {
//            DB::table('tbl_transaction')->where('voucher_number', $id)
//                    ->where('voucher_type', $voch_type)->update(['is_active' => 0]);

            TransactionHelper::transactionDelete($id, $voch_type, 0);
        } else {

            if ($advance_payment->voucher_type == 'invoice_payment') {
                DB::table('tbl_payment')->where('adv_payment_id', $id)->update(['is_active' => 0]);

                SalesInvoiceHelper::updateInvoiceStatus($advance_payment->voucher_no);
                AccountHelper::withDraw($advance_payment->account_id, $advance_payment->amount);
                $get_sales_id = DB::table('tbl_invoice')->where('is_active', 1)
                                ->where('id', '=', $advance_payment->voucher_no)->first();
                if (!empty($get_sales_id->quote_id)) {
                    SalesInvoiceHelper::updateSalesOrderStatus($get_sales_id->quote_id);
                }
            } else if($advance_payment->voucher_type == 'advance_invoice'){
                $builder = DB::table('tbl_payment')->where('adv_payment_id', $id)
                        ->where('is_active', '=', 1)
                        ->get();
                AccountHelper::withDraw($advance_payment->account_id, $advance_payment->amount);
                DB::table('tbl_payment')->where('adv_payment_id', $id)->update(['is_active' => 0]);
                foreach ($builder as $pament_det) {
                    $invoice_id = $pament_det->invoice_id;
                    SalesInvoiceHelper::updateInvoiceStatus($invoice_id);
                    $get_sales_id = DB::table('tbl_invoice')->where('is_active', 1)
                                    ->where('id', '=', $invoice_id)->first();
                    if (!empty($get_sales_id->quote_id)) {
                        SalesInvoiceHelper::updateSalesOrderStatus($get_sales_id->quote_id);
                    }
                }
            } else if ($advance_payment->voucher_type == 'advance_purchase') {
                        $builder = DB::table('tbl_purchase_payment')->where('adv_payment_id', $id)
                                ->where('is_active', '=', 1)
                                ->get();
                        DB::table('tbl_purchase_payment')->where('adv_payment_id', $id)->update(['is_active' => 0]);

                         AccountHelper::deposit($advance_payment->account_id, $advance_payment->amount);
                        foreach ($builder as $pament_det) {
                            $invoice_id = $pament_det->purchase_invoice_id;
                            SalesInvoiceHelper::updatePurchaseInvoiceStatus($invoice_id);
                            $get_sales_id = DB::table('tbl_purchase_invoice')->where('is_active', 1)
                                    ->where('id', '=', $invoice_id)->first();
                    if (!empty($get_sales_id->purchase_quote_id)) {
                        SalesInvoiceHelper::updateSalesOrderStatus($get_sales_id->purchase_quote_id);
                    }
                        }
                    }
//            DB::table('tbl_transaction')->where('voucher_number', $id)
//                    ->where('voucher_type', '=', 'invoice_payment')
//                    ->orWhere('voucher_type', '=', 'advance_payment')->update(['is_active' => 0]);

            TransactionHelper::transactionDelete($id, 'invoice_payment', 0);
            TransactionHelper::transactionDelete($id, $voch_type, 0);

            //Update Advance balance Status
            SalesInvoiceHelper::updateAdvPaymentStatusAndBalance($advance_payment->id);
        }
        $advance_payment->is_active = 0;
        $advance_payment->updated_by = $currentuser->id;
        $advance_payment->update();

        $resVal['id'] = $advance_payment->id;

        LogHelper::info1('Advance Payment Delete ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Advance Payment', 'delete', $screen_code, $advance_payment->id);

        return $resVal;
    }

    public function update1(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Advance Payment Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $payment = AdvancePayment::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Advance Payment Not Found';
            return $resVal;
        }
        $payment->created_by = $currentuser->id;
        $payment->updated_by = $currentuser->id;
        $payment->fill($request->all());
        $payment->save();
        LogHelper::info1('Advance Payment Update ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Advance Payment', 'update', $screen_code, $payment->id);
        return $resVal;
    }
    
    public function update(Request $request,$id){
          $resVal = array();
                    $resVal['success'] = TRUE;
                    $resVal['message'] = 'Advance Payment Updated Successfully';
                    $screen_code = $request->header('screen-code');
                    $ret_auth = AuthorizationHelper::check($screen_code, 'update');
                    if (!$ret_auth) {
                        $resVal['message'] = 'Access Denied';
                        $resVal['success'] = FALSE;
                        return $resVal;
                    }

                    $currentuser = Auth::user();
                    try {
                        $advPayment = AdvancePayment::findOrFail($id);
                    } catch (ModelNotFoundException $e) {

                        $resVal['success'] = FALSE;
                        $resVal['message'] = 'Advance Payment Not Found';
                        return $resVal;
                    }
                    $advDetail = DB::table('tbl_adv_payment')
                                    ->where('id', $advPayment->id)
                                    ->where('is_active', '=', 1)
                                    ->where('used_amount', '=', 0.00)->first();
                    if ($advDetail != '') {

                        $prevAmt = $advDetail->amount;
                        $curAmount = $request->amount;
                        if (strcasecmp($request->input('payment_mode'), 'Cheque') == 0) {
                            $advPayment->cheque_no = $request->input('cheque_no');
                        }
                        if ($advDetail->voucher_type == 'advance_invoice') {
                            //Debit Prev amount in transaction
                            AccountHelper::withDraw( $advDetail->account_id, $prevAmt);
                            // Credit current amount in transaction
                            AccountHelper::deposit($request->account_id,$curAmount );
                            $trans = DB::table('tbl_transaction')
                                            ->where('voucher_type', 'advance_invoice')
                                            ->where('is_active', '=', 1)
                                            ->where('voucher_number', '=', $advDetail->id)->first();

                            $tr_id = $trans->id;
                            //$transaction = Transaction::findOrFail($trans->id);
                        } else if ($advDetail->voucher_type == 'advance_purchase') {
                            //Debit Prev amount in transaction
                            AccountHelper::withDraw( $advDetail->account_id, $prevAmt);
                            // Credit current amount in transaction
                            AccountHelper::deposit($request->account_id,$curAmount );
                            $trans = DB::table('tbl_transaction')
                                            ->where('voucher_type', 'advance_purchase')
                                            ->where('is_active', '=', 1)
                                            ->where('voucher_number', '=', $advDetail->id)->first();
                            $tr_id = $trans->id;
                            //$transaction = Transaction::findOrFail($trans->id);
                        }
                        $advPayment->balance_amount = $curAmount;
                        $advPayment->created_by = $currentuser->id;
                        $advPayment->updated_by = $currentuser->id;
                        $advPayment->fill($request->all());
                        $advPayment->save();



                        $currentUserId = $currentuser->id;
                        $currentUserName = $currentuser->f_name . ' ' . $currentuser->l_name;

                        if ($advDetail->voucher_type == 'advance_invoice') {
                            $credit = $advDetail->amount;
                            $debit = 0.00;
                        }

                        if ($advDetail->voucher_type == 'advance_invoice')
                            $particulars = 'Advance Payment amount is Received';
                        else
                            $particulars = 'Advance Payment amount is Paid ';

                        if ($advDetail->voucher_type == 'advance_invoice') {
                            $credit = $request->amount;
                            $debit = 0.00;
                        } else {
                            $credit = 0.00;
                            $debit = $request->amount;
                        }


                        $transactionData = array(
                            "id" => $tr_id,
                            "name" => $currentuser->f_name,
                            "transaction_date" => $request->date,
                            "voucher_type" => $request->voucher_type,
                            "voucher_number" => $advDetail->id,
                            "credit" => $credit,
                            "debit" => $debit,
                            "pmtcode" => GeneralHelper::getPaymentAcode($request->input('account_id', '')),
                            "pmtcoderef" => $request->input('account_id', ''),
                            "account_category" => $request->input('tra_category', ''),
                            "payment_mode" => $request->input('payment_mode', ''),
                            "account_id" => $advPayment->account_id,
                            "customer_id" => $request->input('customer_id', ''),
                            "is_cash_flow" => 1, //have to check  account in save and voucher type too
                            "account" => $request->input('account'),
                            "acode" => GeneralHelper::getCustomerAcode($request->customer_id),
                            "acoderef" => $request->customer_id,
                            "particulars" => $particulars,
                            "created_by" => $currentUserId,
                            "updated_by" => $currentUserId,
                            "created_by_name" => $currentUserName,
                            "updated_by_name" => $currentUserName,
                            "is_active" => 1,
                        );
                        TransactionHelper::transactionSave($transactionData);
                    }
                    
                     LogHelper::info1('Advance Payment Update ' . $request->fullurl(), $request->all());
                     NotificationHelper::saveNotification($request, 'Advance Payment', 'update', $screen_code, $advPayment->id);
                    return $resVal;
    }
    

}

?>