<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\Albumasset;
use Illuminate\Http\Request;
use App\Transformer\UomTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\SmsHelper;
use App\Helper\LogHelper;
use App\Helper\NotificationHelper;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UomController
 *
 * @author Deepa
 */
class AlbumassetController extends Controller {

    //put your code here

    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Album Asset Added Successfully.';
        $resVal['success'] = TRUE;
        $currentuser = Auth::user();
        $album = new Albumasset;
        $album->created_by = 1;
        $album->updated_by = 1;
        $album->is_active = $request->input('is_active', 1);
        $album->fill($request->all());
        $album->save();
        $resVal['id'] = $album->id;
        //$smssend=$this->testsendsms($request);
        LogHelper::info1('Album asset Save ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Album asset', 'save', $screen_code, $album->id);
        return $resVal;
    }

    public function listAll(Request $request) {
        $resVal = array();
        $id = $request->input('id');
        $Album_id = $request->input('album_id', '');
        $Album_code = $request->input('album_code', '');
        $URL = $request->input('url', '');
        $Comments = $request->input('comments', '');
        $Seq_no = $request->input('seq_no', '');
        $is_active = $request->input('is_active', '');
        //$status = $request->input('status', '');
        $builder = DB::table('tbl_album_asset')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if ($is_active != '') {
            $builder->where('is_active', '=', $is_active);
        }
        if (!empty($Album_id)) {
            $builder->where('Album_id', '=', $Album_id);
        }
        if (!empty($Album_code)) {
            $builder->where('$Album_code', 'like', '%' . $Album_code . '%');
        }

        if (!empty($URL)) {
            $builder->where('URL', 'like', '%' . $URL . '%');
        }
        if (!empty($Comments)) {
            $builder->where('Comments', 'like', '%' . $Comments . '%');
        }
        if (!empty($Seq_no)) {
            $builder->where('Seq_no', '=', $Seq_no);
        }
        $builder->orderBy('id', 'asc');
        $resVal['total'] = $builder->count();
        $resVal['list'] = $builder->skip($start)->take($limit)->get();
        
        LogHelper::info1('Album asset List All ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));

        return $resVal;
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Album Asset Record Updated Successfully';

        try {
            $album = Albumasset::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Album Asset Record Not Found';
            return $resVal;
        }
        $album->created_by = 1;
        $album->fill($request->all());
        $album->save();
        LogHelper::info1('Album asset Update ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Album asset', 'update', $screen_code, $album->id);
        return $resVal;
    }

    public function delete(Request $request, $id) {

        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Album_asset Record Deleted Successfully';

        try {
            $album = Albumasset::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Album_asset Not Found';
            return $resVal;
        }

        $album->delete();
        LogHelper::info1('Album asset Delete ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Album asset', 'delete', $screen_code, $album->id);
        return $resVal;
    }

}

?>
