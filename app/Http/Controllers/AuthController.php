<?php

namespace App\Http\Controllers;

use DateTime;
use App\User;
use Carbon\Carbon;
use App\PersistentLogin;
use Faker\Provider\Uuid;
use Illuminate\Http\Request;
use App\Transformer\UserTransformer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use GuzzleHttp\Client as HttpClient;
use Illuminate\Mail\Message;
use Swift_Message;
use Sichikawa\LaravelSendgridDriver\Transport\SendgridTransport;
use PDO;

class AuthController extends Controller {

    public function convertSSOTokenToAPIToken(Request $request) {

        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Successfully.';

        $ssoToken = $request->input('ssoToken');
        $ssoToken = explode(":", $ssoToken);
        $currentWorkingDir = getcwd();

        $currentDir = substr($currentWorkingDir, 0, strlen($currentWorkingDir) - 6);
        $envArray = parse_ini_file($currentDir . '.env');

        $sso_server = $envArray['DB_HOST'];
        $sso_db_username = $envArray['SSO_DB_USERNAME'];
        $sso_db_password = $envArray['SSO_DB_PASSWORD'];
        $sso_db_name = $envArray['SSO_DB_DATABASE'];

        $database = "";
        $database_username = "";
        $password = "";

        $app_user_name = '';
        try {
            $conn = new PDO("mysql:host=$sso_server;dbname=$sso_db_name", $sso_db_username, $sso_db_password);
        } catch (PDOException $e) {
            $resVal['error'] = $conn->connect_error;
            return $resVal;
        }

        $sql = "select username from persistent_logins as ps where token='" . $ssoToken[0] . "' and series = '" . $ssoToken[1] . "'";
        if ($res = $conn->query($sql)) {

            /* Check the number of rows that match the SELECT statement */
            if ($res->rowCount() > 0) {
                foreach ($res as $row) {

                    $app_user_name = $row["username"];
                }
            } else {
                $resVal['error'] = 'Invalid SSO Token';
                return $resVal;
            }
        }
        $conn = null;

        //Get the subdomain database detail
        $domain = $_SERVER['SERVER_NAME'];
        try {
            $conn = new PDO("mysql:host=$sso_server;dbname=$sso_db_name", $sso_db_username, $sso_db_password);
        } catch (PDOException $e) {
            $resVal['error'] = $conn->connect_error;
            return $resVal;
        }

        $sql = "select db_name,db_host, db_username,db_password from tbl_bu where name ='" . $domain . "'";
        if ($res = $conn->query($sql)) {

            /* Check the number of rows that match the SELECT statement */
            if ($res->rowCount() > 0) {

                foreach ($res as $row) {

                    $app_host=$row["db_host"];
                    $database = $row["db_name"];
                    $database_username = $row["db_username"];
                    $password = $row["db_password"];
                }
            } else {

                $resVal['error'] = $conn->connect_error;
                return $resVal;
            }
        }
        $conn = null;
        //Validate User Access In This domain
        $userValid = false;
        try {
            $conn = new PDO("mysql:host=$app_host;dbname=$database", $database_username, $password);
        } catch (PDOException $e) {
            $resVal['error'] = $conn->connect_error;
            return $resVal;
            ;
        }

        $sql = "select username, status from tbl_user where username ='" . $app_user_name . "' and status ='verified' and is_active=1";

        if ($res = $conn->query($sql)) {

            /* Check the number of rows that match the SELECT statement */
            if ($res->rowCount() > 0) {

                $userValid = true;
            }
        }
        // $conn = null;
        //Create API token for user
        if ($userValid) {

            do {
                $series = Uuid::uuid();
                $token = Uuid::uuid();
                $sql = "select * from persistent_logins where token='" . $token . "' and series='" . $series . "'";

                if ($res = $conn->query($sql)) {

                    $persistentLoginCount = $res->rowCount();
                }
            } while ($persistentLoginCount > 0);


            $stmt = $conn->prepare("insert into persistent_logins (username, series, token) VALUES (:username, :series, :token)");
            $stmt->bindParam(':username', $app_user_name);
            $stmt->bindParam(':token', $token);
            $stmt->bindParam(':series', $series);
            $stmt->execute();
            // setcookie('sso-token', $_REQUEST['sso-token'], 0, '/');
            // setcookie('api-token', $token . ':' . $series, 0, '/');
            $resVal['api-token'] = $token . ':' . $series;
        } else {
            $resVal['error'] = 'Permissin Denied ';
            return $resVal;
            ;
        }
        return $resVal;
    }

    public function login(Request $request) {

        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'User Login Successfully.';

        $userName = $request->input('username');
        $password = $request->input('password');

        $userCollection = User::where('username', "=", $userName)->get();

        if (!$userCollection->isEmpty()) {

            $user = $userCollection->first();
            $currentWorkingDir = getcwd();

            $currentDir = substr($currentWorkingDir, 0, strlen($currentWorkingDir) - 6);
            $envArray = parse_ini_file($currentDir . '.env');
            $sso_server = $envArray['DB_HOST'];
            $sso_db_username = $envArray['SSO_DB_USERNAME'];
            $sso_db_password = $envArray['SSO_DB_PASSWORD'];
            $sso_db_name = $envArray['SSO_DB_DATABASE'];
            $app_user_name = '';



            try {
                $conn = new PDO("mysql:host=$sso_server;dbname=$sso_db_name", $sso_db_username, $sso_db_password);
            } catch (Exception $e) {
                die("Connection failed: " . $conn->connect_error);
            }
            $domain = $_SERVER['SERVER_NAME'];
            $sql = "select * from tbl_user as u left Join tbl_bu as b on  u.username = b.register_byemail where b.name  = '" . $domain . "'";
            $res = $conn->query($sql);
            $new_pass = $request->input('newPassword');
            foreach ($res as $row) {

                $username = $row['username'];
                $password_sso = $row['password'];
            }
            if (Hash::check($password, $password_sso)) {

                do {
                    $series = Uuid::uuid();
                    $token = Uuid::uuid();
                    $persistentLoginCollection = PersistentLogin::where('token', '=', $token)
                                    ->where('series', '=', $series)->get();
                    $persistentLoginCount = count($persistentLoginCollection);
                } while ($persistentLoginCount > 0);
                $persistentLogin = new PersistentLogin;
                $persistentLogin->username = $user->username;
                $persistentLogin->series = $series;
                $persistentLogin->token = $token;
                $dt = new DateTime;
                $persistentLogin->last_used = Carbon::now();
                $persistentLogin->save();
                $resVal['data'] = $user;
                $resVal['api-token'] = $persistentLogin->token . ':' . $persistentLogin->series;
            } else {
                $resVal['success'] = FALSE;
                $resVal['message'] = 'Password Is Invalid.';
            }
        } else {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'UserName Is InValid.';
        }

        return $resVal;
    }

    public function logout(Request $request) {

        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Logout Successfully.';
        $token = $request->header('api-token');
        $splitapitoken = explode(':', $token);

        $persistentLoginCollection = PersistentLogin::where('token', '=', $splitapitoken[0])
                        ->where('series', '=', $splitapitoken[1])->get();

        if (!$persistentLoginCollection->isEmpty()) {
            $persistentLogin = $persistentLoginCollection->first();
            $persistentLogin->delete();
        }
        return $resVal;
    }

    public function userInfo(Request $request) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Success.';
        $currentUser = Auth::user();
        if ($currentUser != null) {
            $resVal['data'] = $currentUser;

            //For Last login time Update

            $user = $currentUser->first();
            $currentWorkingDir = getcwd();

            $currentDir = substr($currentWorkingDir, 0, strlen($currentWorkingDir) - 6);
            $envArray = parse_ini_file($currentDir . '.env');
            $sso_server = $envArray['DB_HOST'];
            $sso_db_username = $envArray['SSO_DB_USERNAME'];
            $sso_db_password = $envArray['SSO_DB_PASSWORD'];
            $sso_db_name = $envArray['SSO_DB_DATABASE'];
            $app_user_name = '';
            $userId = $currentUser->id;
           $userDetail = User::findOrFail($userId);
           $userName = $userDetail->username;


            try {
                $conn = new PDO("mysql:host=$sso_server;dbname=$sso_db_name", $sso_db_username, $sso_db_password);
            } catch (Exception $e) {
                die("Connection failed: " . $conn->connect_error);
            }
            $domain = $_SERVER['SERVER_NAME'];
           $u = "select u.* from tbl_user as u where u.username = '" . $userName . "'";
            $res = $conn->query($u);
            $new_pass = $request->input('newPassword');
            foreach ($res as $row) {
                $ssoUserId = $row['id'];
               // $ssoBUId = $row['buId'];
             }
            $bu = "select b.* from tbl_bu as b where b.name = '" . $domain . "'";
            $resp = $conn->query($bu);
            foreach ($resp as $row) {
                //$ssoUserId = $row['id'];
               $ssoBUId = $row['id'];
             }
            if (!empty($ssoUserId) && !empty($ssoBUId)) {
                $current_date = Carbon::now();
                $sql = "UPDATE `tbl_bu` SET last_bu_activate ='" . $current_date . "' where id = '" . $ssoBUId . "'";
                $res = $conn->query($sql);
                
                $sql = "UPDATE `tbl_user_bumapping` SET last_bu_activate ='" . $current_date . "' where bu_id = '" . $ssoBUId . "' and user_id = '".$ssoUserId."'";
                $res = $conn->query($sql);
                
            }
        } else {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Failed.';
        }
        return $resVal;
    }

    private function getMessage() {
        return new Swift_Message('Test subject', 'Test body.');
    }

}
