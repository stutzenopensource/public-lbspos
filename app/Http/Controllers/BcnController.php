<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use Illuminate\Http\Request;
use App\Transformer\UomTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\GeneralHelper;
use App\Bcn;
use App\Product;
use App\Purchaseinvoice;
use App\Purchaseinvoiceitem;
use Carbon\Carbon;
use App\InventoryAdjustment;
use App\Helper\LogHelper;
use App\Helper\NotificationHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UomController
 *
 * @author Deepa
 */
class BcnController extends Controller {

    //put your code here

    public function BcnLabeling(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Product Labeling Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $type = GeneralHelper::checkConfiqSetting();


        $bcn = new Bcn;
        $bcn->fill($request->all());
        $bcn->save();
        $bcn_previous = Bcn::where('id', "=", $request->input('bcn_id'))
                ->first();
        $qty = $request->input('sales_qty');
        DB::table('tbl_bcn')->where('id', '=', $request->input('bcn_id'))
                ->update(
                        array(
                            "qty_in_hand" => DB::raw('qty_in_hand -' . $qty),
                        )
        );
        if ($type == 'purchase based') {

            $purchaseinvoice = Purchaseinvoice::where('id', "=", $bcn_previous->purchase_invoice_id)
                    ->first();

            $customer_id = $purchaseinvoice->customer_id;
            $get_customer_code = DB::table('tbl_customer')->where('is_active', 1)
                            ->where('id', '=', $customer_id)->first();

            $bcn->save();
            $bcn->ref_type = 'purchase based';



            $code = $get_customer_code->customer_code . $request->input('product_sku') . $bcn_previous->ref_id . $bcn->id;
            $bcn->purchase_invoice_id = $bcn_previous->purchase_invoice_id;
        } else {
            DB::table('tbl_product')->where('id', '=', $bcn_previous->ref_id)
                    ->update(
                            array(
                                "sales_price" => $request->input('selling_price')
            ));

            DB::table('tbl_bcn')->where('id', $bcn_previous->ref_id)
                    ->update(['is_active' => 0]);

            $bcn->ref_type = 'product based';
            $code = $request->input('product_sku') . $bcn_previous->ref_id . $bcn->id;
        }
        $bcn->code = $code;
        $bcn->qty_in_hand = $qty;
        $bcn->qty = $qty;
      
        $bcn->sales_qty = 0;
        $bcn->ref_id = $bcn_previous->ref_id;
        $bcn->previous_bcn_id = $request->input('bcn_id');
        $bcn->created_by = $currentuser->id;
        $bcn->updated_by = $currentuser->id;
        $bcn->status = 'labeling';
        $bcn->is_active = $request->input('is_active', 1);



        $bcn->save();
        
        $inventory_adu = new InventoryAdjustment;
            $inventory_adu->product_id =$request->input('product_id');
//            $inventory_adu->product_name =$request->input('product_name');
            $inventory_adu->uom_id = $request->input('uom_id');
            $inventory_adu->type = "labelling";
            $inventory_adu->comments = "From Labelling";
            $inventory_adu->created_by = $currentuser->id;
            $inventory_adu->updated_by = $currentuser->id;
            $inventory_adu->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $inventory_adu->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $inventory_adu->total_stock = $request->input('sales_qty');
            $inventory_adu->quantity_credit = $request->input('sales_qty');
            $inventory_adu->quantity_debit = 0;
            $inventory_adu->sku = $request->input('product_sku');
            $inventory_adu->date = date('Y-m-d');
            $inventory_adu->bcn_id = $bcn->id;
            $inventory_adu->barcode = $bcn->code;
            $inventory_adu->is_active = 1;
            $inventory_adu->save();

        $resVal['id'] = $bcn->id;
        LogHelper::info1('Bcn Label Save ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Bcn Label', 'save', $screen_code, $bcn->id);

        return $resVal;
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Bcn Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        try {
            $bcn = Bcn::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Bcn Not Found';
            return $resVal;
        }

        $qty = $bcn->qty_in_hand;
        $previous_id = $bcn->previous_bcn_id;
        DB::table('tbl_bcn')->where('id', '=', $previous_id)
                ->update(
                        array(
                            "qty_in_hand" => DB::raw('qty_in_hand +' . $qty),
                            "sales_qty" => DB::raw('sales_qty -' . $qty)
                        )
        );

        $bcn->delete();
        LogHelper::info1('Bcn Delete ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Bcn', 'delete', $screen_code, $bcn->id);

        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $ref_id = $request->input('ref_id');
        $ref_type = $request->input('ref_type', '');
        $code = $request->input('code', '');
        $status = $request->input('status', '');


         $decimalFormat= GeneralHelper::decimalFormat();
       
         $builder = DB::table('tbl_bcn')
                ->select('*',DB::raw("cast(qty as DECIMAL(12,".$decimalFormat.")) as qty"));

        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }

        if (!empty($ref_id)) {
            $builder->where('ref_id', '=', $ref_id);
        }
        if (!empty($ref_type)) {
            $builder->where('ref_type', '=', $ref_type);
        }


        if (!empty($code)) {
            $builder->where('code', '=', $code);
        }
        if (!empty($status)) {
            $builder->where('status', '=', $status);
        }



        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $bcncollection = $builder->get();
        } else {

            $bcncollection = $builder->skip($start)->take($limit)->get();
        }

        $resVal['list'] = $bcncollection;
        LogHelper::info1('Bcn List All ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }

    public function generateBarCode(Request $request, $id) {
        $currentuser = Auth::user();
        $current_date = Carbon::now();
        $current_date = $current_date->format('Y-m-d');
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $settings = DB::table('tbl_app_config')
                ->select('value')->where('setting', '=', 'sales_product_list')
                ->first();

        $type = $settings->value;
        $labelingitem = new Bcn;
        if ($type == 'purchase based') {
            $resVal = BcnController::barcodeForPurchaseBased($id);
        } else {
            $resVal = BcnController::barcodeForProductBased($id);
        }
        LogHelper::info1('Bar Code Save ' . $request->fullurl(), $request->all());        

        return $resVal;
    }

    public static function  barcodeForProductBased($id) {


        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Bar Code Generated Successfully';

        try {
            $product = Product::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = ' Not Found';
            return $resVal;
        }

        $builder = DB::table('tbl_product')->where('id', $id)->where('is_active', '=', 1)->get();
        if (count($builder) > 0) {

            foreach ($builder as $item) {


                $current_date = Carbon::now();
                $year = date('y');
                $month = date('m');

                $code = $item->sku . $item->id;
                $product->code = $code;
                $product->save();

                DB::table('tbl_bcn')
                        ->where('ref_id', '=', $product->id)
                        ->update(['code' => $code]);               
            }
        }       
        return $resVal;
    }

    public static function barcodeForPurchaseBased($id) {

        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Bar Code Generated Successfully';

        try {
            $purchaseinvoice = Purchaseinvoice::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Purchase Invoice Not Found';
            return $resVal;
        }
        $purchaseinvoice->is_barcode_generated = 1;
        $purchaseinvoice->save();
        $builder = DB::table('tbl_purchase_invoice_item')->where('purchase_invoice_id', $id)->where('is_active', '=', 1)->get();
        if (count($builder) > 0) {

            foreach ($builder as $item) {
                if (empty($item->code)) {
                    $customer_id = $purchaseinvoice->customer_id;
                    $get_customer_code = DB::table('tbl_customer')->where('is_active', 1)
                                    ->where('id', '=', $customer_id)->first();

                    $purchaseinvoiceitems = PurchaseInvoiceItem::where('id', "=", $item->id)
                            ->where('is_active', "=", 1)
                            ->first();

                    $current_date = Carbon::now();
                    $year = date('y');
                    $month = date('m');

                    $code = $get_customer_code->customer_code . $item->product_sku . $item->id;
                    $purchaseinvoiceitems->code = $code;
                    $purchaseinvoiceitems->is_barcode_generated = 1;
                    $purchaseinvoiceitems->save();

                    DB::table('tbl_bcn')
                            ->where('ref_id', '=', $purchaseinvoiceitems->id)
                            ->update(['code' => $code]);              
                }
            }
        }
        return $resVal;
    }

    public function bcnDownload(Request $request) {

        $id = $request->input('id', '');
        $download_type = $request->input('type', '');
        $qty = $request->input('qty', '');


        $bcn_array = array();

        $type = GeneralHelper::checkConfiqSetting();

        if ($download_type != 'inventory') {
            if ($type == 'product based') {
                $bcn_list = BcnController::bcnDownloadforProductBased($id, $qty);

                if (empty($qty)) {
                    $qty = $bcn_list['qty'];
                }
                for ($i = 1; $i <= $qty; $i++) {
                    $code = $bcn_list['code'];
                    $name = $bcn_list['name'];
                    array_push($bcn_array, $bcn_list);
                }
            } else {
                // $purchase_item_id = $bcn['purchase_item_id'];

                $bcn_list = BcnController::bcnDownloadforPurchaseBased($id, $qty);
                array_push($bcn_array, $bcn_list);
            }
        } else {
            if ($type == 'product based') {
                $bcn_list = BcnController::bcnDownloadforProductBased($id, $qty);

                if (empty($qty)) {
                    $qty = $bcn_list['qty'];
                }
                for ($i = 1; $i <= $qty; $i++) {
                    $code = $bcn_list['code'];
                    $name = $bcn_list['name'];
                    array_push($bcn_array, $bcn_list);
                }
            } else {
                // $purchase_item_id = $bcn['purchase_item_id'];

                $bcn_list = BcnController::bcnDownloadforPurchaseBasedFromInventory($id, $qty);
                $bcn = Bcn::where('id', "=", $id) ->first();
                if(!empty($bcn))
                    $id=$bcn->purchase_invoice_id;
                
                array_push($bcn_array, $bcn_list);
            }
        }
        ini_set('max_execution_time', 600); //increase max_execution_time to 10 min if data set is very large
        //create a file
        //$filename = "bcncode" . "_" . time() . ".csv";
        //  $filename = "customer" . 
       // "_" . date("Y-m-d_H-i", time()) . ".csv";
	   
	   if($type=='product based')
	   {
		   $filename="product_".$id."_bcncode" . "_" . time() . ".csv";
	   }
	   else{
	    $pur_inv_no = DB::table('tbl_purchase_invoice')->where('id', $id)->first();

		if(!empty($pur_inv_no))
			$filename = "purchase_no_".$pur_inv_no->purchase_no."_bcncode" . "_" . time() . ".csv";
	   }	
        $serverName = $_SERVER['SERVER_NAME'];
        $filePath = "download/$serverName/bcn";

        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);
            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {

                if ($folderName == '')
                    $folderName = $folderList [$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index]; if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        $filePath = getcwd() . '/' . $filePath;
        $sr = 1;
        $output = fopen($filePath . "/" . $filename, 'w');
        
        if ($type == 'product based') {
            fputcsv($output, array('#', 'code', 'Product Name', 'Selling Price'));
            foreach ($bcn_array as $bcn) {
                fputcsv($output, array($sr, $bcn['code'], $bcn['name'], $bcn['selling_price']));
                $sr++;
            }
        } else {
            fputcsv($output, array('#', 'code', 'Product Name', 'Selling Price','Size','Color','Design','Mrp Price'));
            for ($index = 0; $index < count($bcn_array); $index++) {
                $bcn_list = $bcn_array[$index];
                foreach ($bcn_list as $bcn) {
                    fputcsv($output, array($sr, $bcn['code'], $bcn['product_name'], $bcn['selling_price'], $bcn['size'], $bcn['color'], $bcn['design'], $bcn['mrp_price']));
                    $sr++;
                }
            }
        }

        LogHelper::info1('Bcn Download ' . $request->fullurl(), $request->all());
        return response()->download($filePath . "/" . $filename, 'bcncode .csv');
    }

    public function bcnDownloadforProductBased($id, $qty) {

        $bcn = Bcn::where('ref_id', "=", $id)
                ->first();
        $array = array();

        $array['qty'] = $bcn->qty_in_hand;

        $array['code'] = $bcn->code;
        $array['name'] = $bcn->product_name;
        $array ['selling_price'] = $bcn->selling_price;
        $array['product_id'] = $bcn->product_id;

        return $array;
    }

    public function bcnDownloadforPurchaseBased($id, $qty1) {

        $bcn = Bcn::where('purchase_invoice_id', "=", $id)
                ->get();
         
        $bcn_array = array();
        foreach ($bcn as $bc) {

            $bcn_det = Bcn::where('id', "=", $bc->id)
                    ->first();
            
             $item_det1 = PurchaseinvoiceItem::where('id', "=", $bcn_det->ref_id)
                    ->first();
             $bc->size = $item_det1->custom_opt1;
            $bc->color = $item_det1->custom_opt2;
            $bc->design = $item_det1->custom_opt3;
            $bc->mrp_price = $item_det1->mrp_price;
            if (empty($qty1)) {
                $qty = $bcn_det->qty_in_hand;
            } else
                $qty = $qty1;
            $array['qty'] = $bcn_det->qty_in_hand;
            //  echo $qty;
            for ($i = 1; $i <= $qty; $i++) {
                $code = $bcn_det->code;
                $name = $bcn_det->product_name;
                array_push($bcn_array, $bc);
            }
        }

        return $bcn_array;
    }

    public function bcnDownloadforPurchaseBasedFromInventory($id, $qty1) {

        $bcn = Bcn::where('id', "=", $id)
                ->get();

        $bcn_array = array();
        foreach ($bcn as $bc) {

            $bcn_det = Bcn::where('id', "=", $bc->id)
                    ->first();
            
           $item_det1 = PurchaseinvoiceItem::where('id', "=", $bcn_det->ref_id)
                   ->first();
           
           $bc->size = $item_det1->custom_opt1;
           $bc->color = $item_det1->custom_opt2;
           $bc->design = $item_det1->custom_opt3;
           
            if (empty($qty1)) {
                $qty = $bcn_det->qty_in_hand;
            } else
                $qty = $qty1;
            $array['qty'] = $bcn_det->qty_in_hand;
            //  echo $qty;
            for ($i = 1; $i <= $qty; $i++) {
                $code = $bcn_det->code;
                $name = $bcn_det->product_name;
                array_push($bcn_array, $bc);
            }
        }

        return $bcn_array;
    }

}

?>
