<?php

namespace App\Http\Controllers\Catalog\CatalogAccounts;

use Validator;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Catalog\CatalogMedia\Helper\CatalogMediaHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CatalogAccountsController extends Controller {

    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Media Saved Successfully.';
        $resVal['success'] = TRUE;
        $currentuser = Auth::user();
        $request->merge(['createdBy' => $currentuser->id]);
        $request->merge(['updatedBy' => $currentuser->id]);
        $request->merge(['isActive' => 1]);
         
        $id = CatalogMediaHelper::ImageSave($request);

        $result['id'] = $id;
        $resVal['data'] = $result;
        return $resVal;
    }

    public function getList(Request $request) {
        $list = array();

        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        $list = CatalogMediaHelper::getList($start, $limit);
        $resVal['data'] = $list;
        $resVal['success'] = true;
        $resVal['message'] = "Success";
        return ($resVal);
    }

     
    public function getListById(Request $request, $id) {
        $list = array();

        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        $list = CatalogMediaHelper::getListById($start, $limit, $id);
        $resVal['data'] = $list;
        $resVal['success'] = true;
        $resVal['message'] = "Success";
        return ($resVal);
    }

}
