<?php

namespace App\Http\Controllers\Catalog\CatalogAccounts\Helper;

use DB;

class CatalogAccountsSqlProvider {

    public static function findAll() {
        $builder = DB::table('tbl_catelog_accounts')->select('accountId', 'accountName');
        return $builder;
    }

    public static function findByDate($filterDate) {
        $builder = DB::table('tbl_catelog_accounts')->select('accountId', 'accountName')
                ->whereDate('updated_at', '>=', $filterDate);
        return $builder;
    }

}
