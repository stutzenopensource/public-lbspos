<?php

namespace App\Http\Controllers\Catalog\CatalogMedia\Helper;

use Validator;
use App\Http\Controllers\Catalog\CatalogMedia\Helper\CatalogMediaSqlProvider;
use App\Catalog\Tag\Tag;
use App\Catalog\CatalogMedia\CatalogMedia;

class CatalogMediaHelper {

    
    public static function ImageSave($request) {
        $belogTo= $request->input('belongsTo');
        $belogToId= $request->input('belongsToId');
        
        CatalogMediaSqlProvider::deleteMediaByBelongTo($belogTo,$belogToId);
        
        $tag = new CatalogMedia;
        $tag->fill($request->all());
        $tag->save();
        
        return $tag->id;
     }

   public static function getList($start, $limit) {

        $result = array();
        $response = CatalogMediaSqlProvider::getTagList();
        if ($start == 0 && $limit == 0) {
            $response = $response->get();
        } else {

            $response = $response->skip($start)->take($limit)->get();
        }
        $result['total'] = $response->count();
        $result['list'] = $response;
        return $result;
    }
    public static function getListById($start, $limit,$id) {

        $result = array();
        $response = CatalogMediaSqlProvider::getListById( $id);
        if ($start == 0 && $limit == 0) {
            $response = $response->get();
        } else {

            $response = $response->skip($start)->take($limit)->get();
        }
        $result['total'] = $response->count();
        $result['list'] = $response;
        return $result;
    }

}
