<?php
namespace App\Http\Controllers\Catalog\CatalogMedia\Helper;
use DB;
 

class CatalogMediaSqlProvider {
    
     
    
    public static function deleteMediaByBelongTo($belogTo,$belogToId) {
        $builder = DB::table('tbl_catelog_media')
                ->where('belongsTo', $belogTo)
                ->where('belongsToId', $belogToId)->delete();
    }

    public static function getTagList()
    {
        $builder=DB::table('tbl_catelog_media')->select('mediaId','fileLoc','belongsTo','belongsToId') ;        
        return $builder;        
    }
    
    public static function findByDate($filterDate)  
    {
        $builder=DB::table('tbl_catelog_media')->select('mediaId','fileLoc','belongsTo','belongsToId') 
                ->whereDate('updated_at', '>=', $filterDate);
        return $builder;
    }
    
    
    public static function getListById ($id)
    {
              $builder=DB::table('tbl_catelog_media')->select('mediaId','fileLoc','belongsTo','belongsToId') 
                ->where('belongsToId',$id);        
        return $builder;        
    }

}
