<?php

namespace App\Http\Controllers\Catalog\Category;

use Illuminate\Http\Request;
use App\Http\Controllers\Catalog\Category\Helper\CategoryHelper;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Common\CommonConstant;
use App\Http\Controllers\Catalog\CatalogMedia\Helper\CatalogMediaHelper;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class CategoryController extends Controller {

    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Category Saved Successfully';
        $resVal['success'] = TRUE;
        $currentuser = Auth::user();

        $request->merge(['createdBy' => $currentuser->id]);
        $request->merge(['updatedBy' => $currentuser->id]);
        $request->merge(['description' => $request->input('desc', '')]);

        $request->merge(['isActive' => 1]);

        $request->merge(['accountId' => 11]);
        $request->merge(['imageId' => 11]);

        $id = CategoryHelper::save($request);



        $request->merge(['fileLoc' => $request->input('categoryPhoto', '')]);
        $request->merge(['belongsTo' => CommonConstant::$CATEGORY]);
        $request->merge(['belongsToId' => $id]);

        CatalogMediaHelper::ImageSave($request);


        $result['categoryId'] = $id;
        $resVal['data'] = $result;
        return $resVal;
    }

    public function listAll(Request $request) {
        $resVal = array();
        $resVal['success'] = true;
        $resVal['message'] = 'Success';

        $start = $request->input('start', 0);
        $limit = $request->input('limit', 0);

        $list = CategoryHelper::findAll($start, $limit);
        $resVal['data'] = $list;

        return $resVal;
    }

    public function listByAccount($accountId) {
        $resVal = array();
        $resVal['success'] = true;
        $resVal['message'] = 'Success';

        $list = CategoryHelper::findByAccount($accountId);

        $resVal['data'] = $list;

        return $resVal;
    }

    public function findByName(Request $request, $categorName) {

        $resVal = array();
        $resVal['success'] = true;
        $resVal['message'] = 'Success';

        $start = $request->input('start', 0);
        $limit = $request->input('limit', 0);

        $list = CategoryHelper::findByName($categorName, $start, $limit);

        $resVal['data'] = $list;

        return $resVal;
    }

    public static function findById($categoryId) {
        $resVal = array();
        $result = array();
        $resVal['success'] = true;
        $resVal['message'] = 'Success';

        $list = CategoryHelper::findById($categoryId);
        $result['category'] = $list;
        $resVal['data'] = $result;

        return $resVal;
    }

    public static function findByItemCategory(Request $request, $itemId) {
        $resVal = array();
        $resVal['success'] = true;
        $resVal['message'] = 'Success';

        $start = $request->input('start', 0);
        $limit = $request->input('limit', 0);

        $list = CategoryHelper::findByItemCategory($itemId, $start, $limit);

        $resVal['data'] = $list;


        return $resVal;
    }

    public static function detail(Request $request) {
        $resVal = array();
        $resVal['success'] = true;
        $category_id = $request->input('category_id', '');
        $category_accountid = $request->input('category_accountid', '');
        $category_photo = $request->input('category_photo', '');
        $category_name = $request->input('category_name', '');
        $category_icon = $request->input('category_icon', '');
        $category_desc = $request->input('category_desc', '');
        $is_active = $request->input('is_active', '');
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 0);

        $list = CategoryHelper::listAllCategory($category_id, $category_accountid, $category_photo, $category_name, $category_icon, $category_desc, $is_active, $start, $limit);
        $resVal['data'] = $list;
        return $resVal;
    }

}
