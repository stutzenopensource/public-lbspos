<?php

namespace App\Http\Controllers\Catalog\Category\Helper;

use App\Http\Controllers\Catalog\Category\Helper\CategorySqlProvider;
use App\Catalog\Category\Category;
use App\Common\CommonConstant;

class CategoryHelper {

    public static function save($request) {
        $id = $request->input('categoryId');
        if (!empty($id)) {
            try {
                $category = Category::findOrFail($id);
            } catch (Exception $ex) {
                return 0;
            }
        } else if (empty($id)) {
            $category = new Category;
        }
        $category->fill($request->all());
        $category->save();
        $id = $category->categoryId;
        return $id;
    }

    public static function findAll($start, $limit) {
        $resVal = array();
        $list = CategorySqlProvider::findAll();

        $resVal['total'] = $list->count();
        if ($start == 0 && $limit == 0) {
            $collection = $list->get();
        } else {
            $collection = $list->skip($start)->take($limit)->get();
        }

        $resVal['categoryList'] = $collection;
        return $resVal;
    }

    public static function findByAccount($accountId) {
        $resVal = array();
        $list = CategorySqlProvider::findByAccountId($accountId);

        $resVal['categoryList'] = $list->get();
        return $resVal;
    }

    public static function findByName($categorName, $start, $limit) {
        $resVal = array();
        $list = CategorySqlProvider::findByName($categorName);
        $resVal['total'] = $list->count();
        if ($start == 0 && $limit == 0) {
            $collection = $list->get();
        } else {
            $collection = $list->skip($start)->take($limit)->get();
        }

        $resVal['categoryList'] = $collection;
        return $resVal;
    }
    
    public static function findById($categoryId){
        $list= CategorySqlProvider::findById($categoryId);
        if($list != null && count($list) > 0){
            return $list->first();
        }else{
            return null;
        }
            
    }
    
    public static function findByItemCategory($itemId,$start,$limit) {
        $resVal = array();
        $list = CategorySqlProvider::findByItemCategory($itemId);
        $resVal['total'] = $list->count();
        if ($start == 0 && $limit == 0) {
            $collection = $list->get();
        } else {
            $collection = $list->skip($start)->take($limit)->get();
        }

        $resVal['catIds'] = $collection;
        return $resVal;
    }
    public static function listAllCategory($category_id,$category_accountid,$category_photo,$category_name,$category_icon,$category_desc,$is_active,$start,$limit)
    {
        $resVal=array();
        $list = CategorySqlProvider::listAllCategory($category_id,$category_accountid,$category_photo,$category_name,$category_icon,$category_desc,$is_active);
        $resVal['total'] = $list->count();
        if ($start==0 && $limit==0){
            $collection = $list->get();
        }else{
            $collection = $list->skip($start)->take($limit)->get();
        }
           $resVal['categoryList'] = $collection;
           return $resVal;
            
        }
    
}
