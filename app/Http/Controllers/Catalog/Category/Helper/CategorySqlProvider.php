<?php

namespace App\Http\Controllers\Catalog\Category\Helper;

use App\Catalog\Category\Category;
use App\Common\CommonConstant;
use DB;

class CategorySqlProvider {

    public static function findAll() {
        $builder = DB::table('tbl_catalog_category')
                ->select('categoryId', 'name', 'imageId', 'accountId', 'categoryPhoto', 'description as desc')
                ->where('isActive',1);
        return $builder;
    }
    
    public static function findByDate($filterDate) {
        $builder = DB::table('tbl_catalog_category')
                ->select('categoryId', 'name', 'imageId', 'accountId', 'categoryPhoto', 'description as desc')
                ->where('isActive',1)
                ->whereDate('updated_at','>=',$filterDate);
        return $builder;
    }

    public static function findByAccountId($accountId) {
        $builder = DB::table('tbl_catalog_category')
                ->select('categoryId', 'name', 'imageId', 'accountId', 'categoryPhoto', 'description as desc')
                ->where('accountId', $accountId);

        return $builder;
    }

    public static function findByName($categorName) {
        $builder = DB::table('tbl_catalog_category')
                ->select('categoryId', 'name', 'imageId', 'accountId', 'categoryPhoto', 'description as desc');
        if (!empty($categorName)) {
            $builder->where('name', 'like', '%' . $categorName . '%');
        }
        return $builder;
    }

    public static function findById($categoryId) {
        $builder = DB::table('tbl_catalog_category')
                ->select('categoryId', 'name', 'imageId', 'accountId', 'categoryPhoto', 'description as desc');

        if (!empty($categoryId)) {
            $builder->where('categoryId', '=', $categoryId);
        }
        return $builder;
    }

    public static function findByItemCategory($categoryId) {

        $builder = DB::table('tbl_catalog_category as t')
                ->leftjoin('tbl_categorymapping as tm', 't.categoryId', 'tm.categoryId')
                ->select('t.categoryId', 't.name', 't.imageId', 't.accountId', 't.categoryPhoto', 't.description as desc')
                ->where('tm.categoryId', '=', $categoryId);
         return $builder;
    }
    public static function listAllCategory($category_id,$category_accountid,$category_photo,$category_name,$category_icon,$category_desc,$is_active)
    {
        $builder = DB::table('tbl_catalog_category as c')
                ->leftjoin('tbl_catelog_media as ca', function($join) {
                    $join->on('ca.belongsToId', '=', 'c.categoryId');
                    $join->where('ca.belongsTo', 1);
                })
                ->select('c.categoryId', 'c.name', 'c.imageId', 'c.accountId', 'ca.fileLoc as categoryPhoto', 'c.description as desc');
        
        if(!empty($category_id)){
            $builder=$builder->where('c.categoryId','=',$category_id);
        }
        if(!empty($category_accountid)){
            $builder=$builder->where('c.accountId','=',$category_accountid);
        }
        if(!empty($category_photo)){
            $builder=$builder->where('c.categoryPhoto','like','%'.$category_photo.'%');
        }
        if(!empty($category_name)){
            $builder=$builder->where('c.name','like','%'.$category_name.'%');
        }
        if(!empty($category_icon)){
            $builder=$builder->where('c.imageId','=',$category_icon);
        }
        if(!empty($category_desc)){
            $builder=$builder->where('c.description','like','%'.$category_desc.'%');
        }
        
         if($is_active!=null){
            $builder=$builder->where('c.isActive','=',$is_active);
        }
        return $builder;
    }

}


