<?php

namespace App\Http\Controllers\Catalog\Item\Helper;

use App\Http\Controllers\Catalog\Item\Helper\ItemSqlProvider;
use App\Catalog\Item\Item;
use App\Catalog\Tag\TagMapping;
use App\Catalog\Category\CategoryMapping;
use App\Common\CommonConstant;

class ItemHelper {

    public static function save($request) {
        $id = $request->input('itemId');
        if (!empty($id)) {
            try {
                $item = Item::findOrFail($id);
            } catch (Exception $ex) {
                return 0;
            }
        } else if (empty($id)) {
            $item = new Item;
        }
        $item->fill($request->all());
        $item->save();
        $id = $item->itemId;
        return $id;
    }

    public static function tagMappingSave($request) {
        $tag_id = $request->input('tagIds');
        $tag = explode(',', $tag_id);
        $item_id = $request->input('itemId');
        ItemSqlProvider::deleteTagMapByItemId($item_id);
        foreach ($tag as $tId) {
            $item = new TagMapping;
            $item->fill($request->all());
            $item->tagId = $tId;
            $item->save();
        }
    }

    public static function categoryMappingSave($request) {
        $cat_id = $request->input('categoryIds');
        $cat = explode(',', $cat_id);
        $item_id = $request->input('itemId');
        ItemSqlProvider::deleteCategoryMapByItemId($item_id);
        foreach ($cat as $cId) {
            $item = new CategoryMapping;
            $item->fill($request->all());
            $item->categoryId = $cId;
            $item->save();
        }
    }

    public static function findAll($start, $limit) {
        $resVal = array();
        $list = ItemSqlProvider::findAll();

        $resVal['total'] = $list->count();
        if ($start == 0 && $limit == 0) {
            $collection = $list->get();
        } else {
            $collection = $list->skip($start)->take($limit)->get();
        }

        $resVal['itemList'] = $collection;
        return $resVal;
    }

    public static function findByAccount($accountId,$start, $limit) {
        $resVal = array();
        $list = ItemSqlProvider::findByAccountId($accountId);
        $resVal['total'] = $list->count();
        if ($start == 0 && $limit == 0) {
            $collection = $list->get();
        } else {
            $collection = $list->skip($start)->take($limit)->get();
        }
        $resVal['itemList'] = $list->get();
        return $resVal;
    }

    public static function findByCategory($categoryId,$start, $limit) {
        $resVal = array();
        $list = ItemSqlProvider::findByCategoryId($categoryId);
        $resVal['total'] = $list->count();
        if ($start == 0 && $limit == 0) {
            $collection = $list->get();
        } else {
            $collection = $list->skip($start)->take($limit)->get();
        }
        $resVal['itemList'] = $list->get();
        return $resVal;
    }
    
    public static function findByName($itemName, $start, $limit) {
        $resVal = array();
        $list = ItemSqlProvider::findByName($itemName);
        $resVal['total'] = $list->count();
        if ($start == 0 && $limit == 0) {
            $collection = $list->get();
        } else {
            $collection = $list->skip($start)->take($limit)->get();
        }

        $resVal['itemList'] = $collection;
        return $resVal;
    }

    public static function findById($Id) {
        $list = ItemSqlProvider::findById($Id);
        if ($list != null && count($list) > 0) {
            return $list->first();
        } else {
            return null;
        }
    }

    public static function findByCode($code, $start, $limit) {
        $resVal = array();
        $list = ItemSqlProvider::findByCode($code);
        $resVal['total'] = $list->count();
        if ($start == 0 && $limit == 0) {
            $collection = $list->get();
        } else {
            $collection = $list->skip($start)->take($limit)->get();
        }

        $resVal['itemList'] = $collection;
        return $resVal;
    }  
    
 public static function listAllItem($id,$account_id,$categoryid,$item_code,$item_name,$item_photo,$item_thumbnail_photo,$item_description,$item_weight,$is_active,$category_photo, $start, $limit){
       $resVal = array();
       $list = ItemSqlProvider::listAllItem($id,$account_id,$categoryid,$item_code,$item_name,$item_photo,$item_thumbnail_photo,$item_description,$item_weight,$is_active,$category_photo,$start, $limit);
       return $list;
       
   }    

    
}
