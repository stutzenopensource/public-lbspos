<?php

namespace App\Http\Controllers\Catalog\Item\Helper;

use App\Catalog\Item\Item;
use App\Common\CommonConstant;
use DB;

class ItemSqlProvider {

    public static function deleteCategoryMapByItemId($item) {
        $builder = DB::table('tbl_categorymapping')->where('itemId', $item)->delete();
    }

    public static function deleteTagMapByItemId($item) {
        $builder = DB::table('tbl_tagmapping')->where('itemId', $item)->delete();
    }

    public static function findAll() {
        $builder = DB::table('tbl_catelog_item as ci')
                 ->leftjoin('tbl_catelog_media as ca', function($join) {
                    $join->on('ca.belongsToId', '=', 'ci.itemId');
                    $join->where('ca.belongsTo', 2);
                })
                ->leftjoin('tbl_catelog_media as ca1', function($join) {
                    $join->on('ca1.belongsToId', '=', 'ci.itemId');
                    $join->where('ca1.belongsTo', 4);
                })
                ->leftjoin('tbl_categorymapping as tm', 'ci.itemId', 'tm.itemId')
                ->select('ci.itemId', 'ci.accountId', 'tm.categoryId', 'ci.code', 'ci.name', 'ci.description as desc', 'ci.weight', 'ca.fileLoc as itemPhoto',
                        'ca1.fileLoc as itemThumbnailPhoto', 'ci.isActive')
                ->where("ci.isActive",1);
        return $builder;
    }

    public static function findByDate($filterDate) {
        $builder = DB::table('tbl_catelog_item')
                ->select('itemId', 'accountId', 'categoryId', 'code', 'name', 'description as desc', 'weight', 'itemPhoto', 'itemThumbnailPhoto', 'isActive')
                ->where("isActive",1)
                ->whereDate('updated_at','>=',$filterDate);
        return $builder;
    }
    
    public static function findByAccountId($accountId) {
        $builder = $builder = DB::table('tbl_catelog_item')
                ->select('itemId', 'accountId', 'categoryId', 'code', 'name', 'description as desc', 'weight', 'itemPhoto', 'itemThumbnailPhoto', 'isActive')
                ->where('accountId', $accountId);

        return $builder;
    }

    public static function findByCategoryId($categoryId) {
        $builder = $builder = DB::table('tbl_catelog_item')
                ->select('itemId', 'accountId', 'categoryId', 'code', 'name', 'description as desc', 'weight', 'itemPhoto', 'itemThumbnailPhoto', 'isActive')
                ->where('categoryId', $categoryId);

        return $builder;
    }

    public static function findByName($itemName) {
        $builder = $builder = DB::table('tbl_catelog_item')
                ->select('itemId', 'accountId', 'categoryId', 'code', 'name', 'description as desc', 'weight', 'itemPhoto', 'itemThumbnailPhoto', 'isActive');

        if (!empty($itemName)) {
            $builder->where('name', 'like', '%' . $itemName . '%');
        }
        return $builder;
    }

    public static function findById($itemId) {
        $builder = $builder = DB::table('tbl_catelog_item')
                ->select('itemId', 'accountId', 'categoryId', 'code', 'name', 'description as desc', 'weight', 'itemPhoto', 'itemThumbnailPhoto', 'isActive');

        if (!empty($itemId)) {
            $builder->where('itemId', '=', $itemId);
        }
        return $builder;
    }

    public static function findByCode($code) {
        $builder = $builder = DB::table('tbl_catelog_item')
                ->select('itemId', 'accountId', 'categoryId', 'code', 'name', 'description as desc', 'weight', 'itemPhoto', 'itemThumbnailPhoto', 'isActive');

        if (!empty($code)) {
            $builder->where('code', 'like', '%' . $code . '%');
        }
        return $builder;
    } 
    public static function listAllItem($id,$account_id,$categoryid,$item_code,$item_name,$item_photo,$item_thumbnail_photo,$item_description,$item_weight,$is_active,$category_photo,$start, $limit){
        $builder = DB::table('tbl_catelog_item as ci')
                ->leftjoin('tbl_catelog_media as ca', function($join) {
                    $join->on('ca.belongsToId', '=', 'ci.itemId');
                    $join->where('ca.belongsTo', 2);
                })
                ->leftjoin('tbl_catelog_media as ca1', function($join) {
                    $join->on('ca1.belongsToId', '=', 'ci.itemId');
                    $join->where('ca1.belongsTo', 4);
                })
                ->select('ci.itemId', 'ci.accountId', 'ci.categoryId', 'ci.code', 'ci.name', 'ci.description', 'ci.weight', 'ca.fileLoc as itemPhoto',
                        'ca1.fileLoc as itemThumbnailPhoto', 'ci.isActive');
        
        if(!empty($id)){
            $builder->where('ci.itemId','=',$id);
        } 
        if(!empty($account_id)){
            $builder->where('ci.accountId','=',$account_id);
        }
        if(!empty($item_photo)){
            $builder->where('ci.itemPhoto','=',$item_photo);
        }
        if(!empty($item_thumbnail_photo)){
            $builder->where('ci.itemThumbnailPhoto','=',$item_thumbnail_photo);
        }
        if(!empty($categoryid)){
            $builder->where('ci.categoryId','=',$categoryid);
        }        
        if(!empty($item_name)){
            $builder->where('ci.name','like','%'.$item_name.'%');
        }
        if(!empty($item_weight)){
            $builder->where('ci.weight','=',$item_weight);
        }
        if(!empty($item_desc)){
            $builder->where('ci.description','like','%'.$item_desc.'%');
        }
        if($is_active!=null){
            $builder->where('ci.isActive','=',$is_active);
        } 
        $resVal['total']=$builder->count();
        if($start==0 && $limit==0){
           $collection=$builder->get();  
        }else
        {
           $collection=$builder->skip($start)->take($limit)->get();
        }
        foreach($collection as $build){
             $builder1 = DB::table('tbl_categorymapping as cm')
                     ->leftjoin('tbl_catalog_category as cc','cm.categoryId','=','cc.categoryId')
                     ->select('cc.categoryId','cc.name','cm.*')
                     ->where('cm.itemId','=',$build->itemId)
                     ->where('cc.isActive','=',1)
                     ->get();
             $build->categorymapping = $builder1;
        } 
        foreach($collection as $builds){
            $tag = DB::table('tbl_tagmapping as tm')
                    ->leftjoin('tbl_tag as tg','tm.tagId','=','tg.tagId')
                    ->select('tg.tagId','tg.Name as name','tm.*')
                    ->where('tm.itemId','=',$build->itemId)
                    ->where('tg.isActive','=',1)
                    ->get();
            $build->tagmapping = $tag;
        }
       
        $resVal['data']=$collection;
        return $resVal;
    }
    
    
}
