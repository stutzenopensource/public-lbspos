<?php

namespace App\Http\Controllers\Catalog\Item;

use Illuminate\Http\Request;
use App\Http\Controllers\Catalog\Item\Helper\ItemHelper;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Common\CommonConstant;
use App\Http\Controllers\Catalog\CatalogMedia\Helper\CatalogMediaHelper;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ItemController extends Controller {

    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Item Saved Successfully';
        $resVal['success'] = TRUE;
        $currentuser = Auth::user();
        $thumb_nail=$request->input('itemThumbnailPhoto', '');
        $item_photo=$request->input('itemPhoto', '');
        
//        $item_photo= substr($itemPhoto, strpos($itemPhoto, '/co.stutzen.service/')+20);
//        $thumb_nail= substr($thumbNail, strpos($thumbNail, '/co.stutzen.service/')+20);
        
        $request->merge(['itemPhoto' => $item_photo]);
        $request->merge(['itemThumbnailPhoto' => $thumb_nail]);
        $request->merge(['createdBy' => $currentuser->id]);
        $request->merge(['updatedBy' => $currentuser->id]);
        $request->merge(['isActive' => 1]);
        $request->merge(['description' => $request->input('desc', '')]);

        $request->merge(['accountId' => 11]);
        $request->merge(['categoryId' => 1]);

        $id = ItemHelper::save($request);

        $tagIds = $request->input('tagIds', '');
        $request->merge(['tagIds' => $request->input('tagIds', '')]);
        $request->merge(['itemId' => $id]);

        ItemHelper::tagMappingSave($request);

        $categoryIds = $request->input('categoryIds', '');
        $request->merge(['categoryIds' => $categoryIds]);
        $request->merge(['itemId' => $id]);

        ItemHelper::categoryMappingSave($request);

        $request->merge(['fileLoc' => $item_photo]);
        $request->merge(['belongsTo' => CommonConstant::$ITEM]);
        $request->merge(['belongsToId' => $id]);

        CatalogMediaHelper::ImageSave($request);


        $request->merge(['fileLoc' => $thumb_nail]);
        $request->merge(['belongsTo' => CommonConstant::$ITEMTHUMBNAIL]);
        $request->merge(['belongsToId' => $id]);

        CatalogMediaHelper::ImageSave($request);


        $result['itemId'] = $id;
        $resVal['data'] = $result;
        return $resVal;
    }

    public function listAll(Request $request) {
        $resVal = array();
        $resVal['success'] = True;
        $resVal['message'] = 'Success';

        $start = $request->input('start', 0);
        $limit = $request->input('limit', 0);

        $list = ItemHelper::findAll($start, $limit);
        $resVal['data'] = $list;

        return $resVal;
    }

    public function listByAccount(Request $request, $accountId) {
        $resVal = array();
        $resVal['success'] = True;
        $resVal['message'] = 'Success';
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 0);
        $list = ItemHelper::findByAccount($accountId, $start, $limit);

        $resVal['data'] = $list;

        return $resVal;
    }

    public function listByCategory(Request $request, $categoryId) {
        $resVal = array();
        $resVal['success'] = True;
        $resVal['message'] = 'Success';
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 0);
        $list = ItemHelper::findByCategory($categoryId, $start, $limit);

        $resVal['data'] = $list;

        return $resVal;
    }

    public function findByName(Request $request, $itemName) {

        $resVal = array();
        $resVal['success'] = True;
        $resVal['message'] = 'Success';

        $start = $request->input('start', 0);
        $limit = $request->input('limit', 0);

        $list = ItemHelper::findByName($itemName, $start, $limit);

        $resVal['data'] = $list;

        return $resVal;
    }

    public static function findById($Id) {
        $resVal = array();
        $result = array();
        $resVal['success'] = True;
        $resVal['message'] = 'Success';

        $list = ItemHelper::findById($Id);
        $result['item'] = $list;
        $resVal['data'] = $result;

        return $resVal;
    }

    public static function findByCode(Request $request, $code) {
        $resVal = array();
        $result = array();
        $resVal['success'] = True;
        $resVal['message'] = 'Success';
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 0);

        $list = ItemHelper::findByCode($code, $start, $limit);
        $result['item'] = $list;
        $resVal['data'] = $result;

        return $resVal;
    } 
    
    public function listAllitem(Request $request){
        
        $resVal = array();
        $resVal['success'] = True;
        $resVal['message'] = 'Success';
        $id=$request->input('item_id','');
        $account_id=$request->input('item_accountid','');
        $categoryid=$request->input('item_categoryid','');
        $item_code=$request->input('item_code','');
        $item_name=$request->input('item_name','');
        $item_photo=$request->input('item_photo','');
        $item_thumbnail_photo=$request->input('item_thumbnail_photo','');
        $item_description=$request->input('item_description','');
        $item_weight=$request->input('item_weight','');
        $category_photo=$request->input('category_photo','');
        $is_active=$request->input('is_active','');
        
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 0);
        
        $list = ItemHelper::listAllItem($id,$account_id,$categoryid,$item_code,$item_name,$item_photo,$item_thumbnail_photo,$item_description,$item_weight,$is_active,$category_photo, $start, $limit);

        $resVal['data'] = $list;

        return $resVal;
    
    }
    
    

}
