<?php

namespace App\Http\Controllers\Catalog\Price\Helper;
use App\Http\Controllers\Catalog\Price\Helper\PriceSqlProvider;


class PriceHelper{
    
    public static function deletePriceByItemId($itemId){
        $deletePrice = PriceSqlProvider::deletePrice($itemId);
        return $deletePrice;
    }
    public static function findAll(){
       $resVal=array();
       $data=PriceSqlProvider::listAll();
       $resVal['priceList']=$data;
       return $resVal;
   }
   
   public static function findByItem($itemId){
        $resVal=array();
       $data= PriceSqlProvider::findByItemId($itemId);
       $resVal['priceList']=$data;
       return $resVal;
   }
}
