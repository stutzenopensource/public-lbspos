<?php

namespace App\Http\Controllers\Catalog\Price\Helper;

use DB;

class PriceSqlProvider {

    public static function findAll() {
        $builder = DB::table('tbl_catelog_price')->select('priceId', 'itemId', 'pricevalue', 'priceisprimary',   'desc as desc');
        return $builder;
    }

      public static function findByDate($filterDate) {
        $builder = DB::table('tbl_catelog_price')->select('priceId', 'itemId', 'pricevalue', 'priceisprimary',   'desc as desc')
                ->whereDate('updated_at', '>=', $filterDate);
        return $builder;
    } 
    
    public static function deletePrice($itemId){
        $builder = DB::table('tbl_catelog_price')
                ->where('itemId','=',$itemId)
                ->delete();
        return $builder;
    }
    
    public static function listAll(){
        $builder = DB::table('tbl_catelog_price')
                ->select('priceId', 'itemId', 'pricevalue', 'priceisprimary',   'desc as desc')
                ->where('priceId','!=',0);
        return $builder->get();
    }
    
    public static function findByItemId($itemId){
        $builder = DB::table('tbl_catelog_price')
                ->select('priceId', 'itemId', 'pricevalue', 'priceisprimary',   'desc as desc')
                ->where('itemId','=',$itemId);
        return $builder->get();
    }
}


