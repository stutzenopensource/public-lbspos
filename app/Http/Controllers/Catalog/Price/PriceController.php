<?php

namespace App\Http\Controllers\Catalog\Price;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Common\CommonConstant;
use App\Catalog\Price\Price;
use App\Http\Controllers\Catalog\Price\Helper\PriceHelper;



class PriceController extends Controller {
    public static function save(Request $request) {
        $resVal = array();
        $itemId = $request->input('itemId', '');
        $deletePrice = PriceHelper::deletePriceByItemId($itemId);
        $price = $request->json()->all();
        $isPrimary = 0;
        foreach ($price as $pri) {
            $pricees = new Price;
           
             $pricees->fill($pri);
            if ($isPrimary == 0) {
                $pricees->priceisprimary = 1;
            } else {
                $pricees->priceisprimary = 0;
            }
           
            $pricees->save();
            $isPrimary = 1;
        }
        
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Price Saved Successfully';
		return $resVal;
    }
    
     public function listAll(){
        $resVal=array();
        $resVal['success']=true;
        $data=PriceHelper::findAll();
        $resVal['data']=$data;
        
        return $resVal;
    }
    
    public function listByAccount($itemId){
        $resVal=array();
        $resVal['success']=true;
        $data=PriceHelper::findByItem($itemId);
        $resVal['data']=$data;
        
        return $resVal;
    }

}
