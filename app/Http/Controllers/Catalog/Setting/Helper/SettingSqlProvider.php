<?php

namespace App\Http\Controllers\Catalog\Setting\Helper;

use App\Common\CommonConstant;
use DB;

class SettingSqlProvider {

    public static function findAll() {
        $builder = DB::table('tbl_catelog_setting')->select('settingId', 'type', 'setting_key as key', 'value');
        return $builder;
    }

    public static function findByDate($filterDate) {
        $builder = DB::table('tbl_catelog_setting')->select('settingId', 'type', 'setting_key as key', 'value')
                ->whereDate('updated_at', '>=', $filterDate);
        return $builder;
    }

}
