<?php

namespace App\Http\Controllers\Catalog\Sync\Helper;

use Validator;
use App\Http\Controllers\Catalog\Sync\Helper\SyncSqlProvider;
use App\Http\Controllers\Catalog\Tag\Helper\TagSqlProvider;
use App\Http\Controllers\Catalog\Category\Helper\CategorySqlProvider;
use App\Http\Controllers\Catalog\Item\Helper\ItemSqlProvider;
use App\Http\Controllers\Catalog\CatalogMedia\Helper\CatalogMediaSqlProvider;
use App\Media;
use App\Http\Controllers\Catalog\Price\Helper\PriceSqlProvider;
use App\Http\Controllers\Catalog\Setting\Helper\SettingSqlProvider;
use App\Http\Controllers\Catalog\CatalogAccounts\Helper\CatalogAccountsSqlProvider;

class SyncHelper {

    public static function getList() {

        $result = array();
        $accountId = 11;
        $tagList = TagSqlProvider::getTagList();
        $categoryList = CategorySqlProvider::findAll();
        $itemList = ItemSqlProvider::findAll();
        $mediaList = CatalogMediaSqlProvider::getTagList();
        $tagMappingList = TagSqlProvider::findTagMappingByAccount($accountId);

        $priceList = PriceSqlProvider::findAll();
        $settingList = SettingSqlProvider::findAll();
        $acountsList = CatalogAccountsSqlProvider::findAll();

        $result['accountList'] = $acountsList->get();
        $result['categoryList'] = $categoryList->get();
        $result['itemList'] = $itemList->get();
        $result['mediaList'] = $mediaList->get();
        $result['tagList'] = $tagList->get();
        $result['tagMappingList'] = $tagMappingList->get();
        $result['priceList'] = $priceList->get();
        $result['settingList'] = $settingList->get();

        return $result;
    }
    
    public static function getListByDate($filterDate) {

        $result = array();
        $accountId = 11;
        $tagList = TagSqlProvider::findByDate($filterDate);
        $categoryList = CategorySqlProvider::findByDate($filterDate);
        $itemList = ItemSqlProvider::findByDate($filterDate);
        $mediaList = CatalogMediaSqlProvider::findByDate($filterDate);
        $tagMappingList = TagSqlProvider::findTagMappingByDate($filterDate,$accountId);

        $priceList = PriceSqlProvider::findByDate($filterDate);
        $settingList = SettingSqlProvider::findByDate($filterDate);
        $acountsList = CatalogAccountsSqlProvider::findByDate($filterDate);

        $result['accountList'] = $acountsList->get();
        $result['categoryList'] = $categoryList->get();
        $result['itemList'] = $itemList->get();
        $result['mediaList'] = $mediaList->get();
        $result['tagList'] = $tagList->get();
        $result['tagMappingList'] = $tagMappingList->get();
        $result['priceList'] = $priceList->get();
        $result['settingList'] = $settingList->get();

        return $result;
    }

}
