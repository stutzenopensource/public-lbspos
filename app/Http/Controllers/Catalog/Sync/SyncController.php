<?php

namespace App\Http\Controllers\Catalog\Sync;

use Validator;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Catalog\Sync\Helper\SyncHelper;
use App\Http\Controllers\Catalog\CatalogMedia\Helper\CatalogMediaHelper;
use App\Common\CommonConstant;
use App\Catalog\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SyncController extends Controller {

    public function sync(Request $request) {
        $list = array();

        $filterDate = $request->input('filterDate', '');

        if ($filterDate == "")
            $list = SyncHelper::getList();
        else
            $list = SyncHelper::getListByDate($filterDate);
        $resVal['data'] = $list;
        $resVal['success'] = true;
        $resVal['message'] = "All Data Retrieved Successfully..";
        return ($resVal);
    }

}
