<?php

namespace App\Http\Controllers\Catalog\Tag\Helper;

use Validator;
use App\Http\Controllers\Catalog\Tag\Helper\TagSqlProvider;
use App\Catalog\Tag\Tag;
use App\Media;

class TagHelper {

    public static function save($request) {
        
        $id = $request->input('tagId');
        if (!empty($id)) {
            try {
                $tag = Tag::findOrFail($id);
            } catch (Exception $ex) {
                return 0;
            }
        } else if (empty($id)) {
        $tag = new Tag;
        }
        $tag->fill($request->all());
        $tag->save();
        $id = $tag->tagId;
        return $id;

        
    }

    public static function ImageSave($request) {
        $tag = new Media;
        $tag->fill($request->all());
        $tag->save();

     }

    public static function getList($start, $limit) {

        $result = array();
        $response = TagSqlProvider::getTagList();
        if ($start == 0 && $limit == 0) {
            $response = $response->get();
        } else {

            $response = $response->skip($start)->take($limit)->get();
        }
        $result['total'] = $response->count();
        $result['list'] = $response;
        return $result;
    }

    public static function getListByAccountId($start, $limit,$accountId) {

        $result = array();
        $response = TagSqlProvider::getListByAccountId( $accountId);
        if ($start == 0 && $limit == 0) {
            $response = $response->get();
        } else {

            $response = $response->skip($start)->take($limit)->get();
        }
        $result['total'] = $response->count();
        $result['tagList'] = $response;
        return $result;
    }
    
    public static function getListByName($start, $limit,$name) {

        $result = array();
        $response = TagSqlProvider::getListByName( $name);
        if ($start == 0 && $limit == 0) {
            $response = $response->get();
        } else {

            $response = $response->skip($start)->take($limit)->get();
        }
        $result['total'] = $response->count();
        $result['tagList'] = $response;
        return $result;
    }
     public static function getListByTagId($start, $limit,$tagId) {

        $result = array();
        $response = TagSqlProvider::getListByTagId( $tagId);
        if($response != null && $response->count()> 0){
            return $response->first();
        }else{
            return null;
        }
    }
    
    public static function findByItemTag($itemId,$start,$limit) {
        $resVal = array();
        $list = TagSqlProvider::findByItemTag($itemId);
        $resVal['total'] = $list->count();
        if ($start == 0 && $limit == 0) {
            $collection = $list->get();
        } else {
            $collection = $list->skip($start)->take($limit)->get();
        }

        $resVal['tagIds'] = $collection;
        return $resVal;
    }
    public static function tagdetail($tag_id,$tag_name,$tag_icon,$tag_accountid,$tag_photo,$tag_mediaid,$is_active,$start,$limit)
    {
        $result = array();
        $list = TagSqlProvider::tagdetail($tag_id,$tag_name,$tag_icon,$tag_accountid,$tag_photo,$tag_mediaid,$is_active);
        $result['total']=$list->count();
        if($start==0 && $limit==0){
            $collection = $list->get();
         }else{
             $collection=$list->skip($start)->take($limit)->get();
         }
         $result['list']=$collection;
         return $result;
    }
}
