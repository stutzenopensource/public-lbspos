<?php

namespace App\Http\Controllers\Catalog\Tag\Helper;

use DB;

class TagSqlProvider {

    public static function getListByTagId($tagId) {
        $builder = DB::table('tbl_tag')->select('tagId', 'name', 'imageId', 'accountId', 'tagPhoto')
                ->where('tag_id', $tagId);
        return $builder;
    }

    public static function getListByName($name) {
        $builder = DB::table('tbl_tag')->select('tagId', 'name', 'imageId', 'accountId', 'tagPhoto')
                ->where('name', 'like', '%' . $name . '%');
        return $builder;
    }

    public static function getListByAccountId($accountId) {
        $builder = DB::table('tbl_tag')->select('tagId', 'name', 'imageId', 'accountId', 'tagPhoto')
                ->where('accountId', $accountId);
        return $builder;
    }

    public static function getTagList() {
        $builder = DB::table('tbl_tag')->select('tagId', 'name', 'imageId', 'accountId', 'tagPhoto');
        return $builder;
    }

    public static function findByDate($filterDate) {
        $builder = DB::table('tbl_tag')->select('tagId', 'name', 'imageId', 'accountId', 'tagPhoto')
                ->whereDate('updated_at', '>=', $filterDate);
        return $builder;
    }

    public static function findByItemTag($itemId) {

        $builder = DB::table('tbl_tag as t')
                ->leftjoin('tbl_tagmapping as tm', 't.tagId', 'tm.tagId')
                ->select('t.tagId', 'name', 'imageId', 'accountId', 'tagPhoto')
                ->where('itemId', '=', $itemId);
        return $builder;
    }

    public static function tagdetail($tag_id, $tag_name, $tag_icon, $tag_accountid, $tag_photo, $tag_mediaid, $is_active) {

        $builder = DB::table('tbl_tag as t')
                 ->leftjoin('tbl_catelog_media as ca', function($join) {
                    $join->on('ca.belongsToId', '=', 't.tagId');
                    $join->where('ca.belongsTo', 3);
                })
                ->select('t.tagId', 't.name', 't.imageId', 't.accountId', 'ca.fileLoc as tagPhoto');
        if (!empty($tag_id)) {
            $builder = $builder->where('t.tagId', '=', $tag_id);
        }
        if (!empty($tag_name)) {
            $builder = $builder->where('t.name', 'like', '%' . $tag_name . '%');
        }
        if (!empty($tag_icon)) {
            $builder = $builder->where('t.tag_icon', '=', $tag_icon);
        }
        if (!empty($tag_accountid)) {
            $builder = $builder->where('t.accountId', '=', $tag_accountid);
        }
        if (!empty($tag_photo)) {
            $builder = $builder->where('t.tagPhoto', 'like', '%' . $tag_photo . '%');
        }
        if (!empty($tag_mediaid)) {
            $builder = $builder->where('t.imageId', '=', $tag_mediaid);
        }
        if ($is_active != null) {
            $builder = $builder->where('t.isActive', '=', $is_active);
        }

        return $builder;
    }

    public static function findTagMappingByAccount($accountId) {

        $builder = DB::table('tbl_tagmapping as tm')
                ->join('tbl_tag as t', 't.tagId', 'tm.tagId')
                ->select('tagmappingId', 'itemId', 't.tagId');
                //->where('t.accountId', '=', $accountId);
        return $builder;
    }
    
    public static function findTagMappingByDate($filterDate,$accountId){

        $builder = DB::table('tbl_tagmapping as tm')
                ->join('tbl_tag as t', 't.tagId', 'tm.tagId')
                ->select('tagmappingId', 'itemId', 't.tagId')
                ->where('t.accountId', '=', $accountId)
                ->whereDate('tm.updated_at', '>=', $filterDate);
        return $builder;
    }

}
