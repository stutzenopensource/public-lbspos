<?php

namespace App\Http\Controllers\Catalog\Tag;

use Validator;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Catalog\Tag\Helper\TagHelper;
use App\Http\Controllers\Catalog\CatalogMedia\Helper\CatalogMediaHelper;

use App\Common\CommonConstant;
use App\Catalog\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TagController extends Controller {

    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Tag Saved Successfully';
        $resVal['success'] = TRUE;
        $currentuser = Auth::user();

        $request->merge(['createdBy' => $currentuser->id]);
        $request->merge(['updatedBy' => $currentuser->id]);
        $request->merge(['isActive' => 1]);        
        $request->merge(['accountId' => 11]);
        $request->merge(['imageId' => 11]);

        $id = TagHelper::save($request);


        $request->merge(['createdBy' => $currentuser->id]);
        $request->merge(['updatedBy' => $currentuser->id]);
        $request->merge(['isActive' => 1]);
        $request->merge(['fileLoc' => $request->input('tagPhoto', '')]);
        $request->merge(['belongsTo' => CommonConstant::$TAG]);
        $request->merge(['belongsToId' => $id]);

        CatalogMediaHelper::ImageSave($request);


        $result['tagId'] = $id;
        $resVal['data'] = $result;
        return $resVal;
    }

    public function getList(Request $request) {
        $list = array();

        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        $list = TagHelper::getList($start, $limit);
        $resVal['data'] = $list;
        $resVal['success'] = True;
        $resVal['message'] = "Success";
        return ($resVal);
    }
    
    public function getListByAccountId(Request $request,$accountId) {
        $list = array();

        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
 
        $list = TagHelper::getListByAccountId($start, $limit,$accountId);
        $resVal['data'] = $list;
        $resVal['success'] = true;
        $resVal['message'] = "success";
        return ($resVal);
    }

    public function getListByname(Request $request,$name) {
        $list = array();
 
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
 
        $list = TagHelper::getListByname($start, $limit,$name);
        $resVal['data'] = $list;
        $resVal['success'] = True;
        $resVal['message'] = "Success";
        return ($resVal);
    }
     
    public function getListByTagId(Request $request,$tagId) {
        $list = array();

        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
 
        $list = TagHelper::getListByTagId($start, $limit,$tagId);
        $result['tag'] = $list;
        $resVal['data'] = $result;
        $resVal['success'] = true;
        $resVal['message'] = "success";
        return ($resVal);
    }

    
    public static function findByItemTag(Request $request,$itemId) {
         $resVal = array();
        $resVal['success'] = True;
        $resVal['message'] = 'Success';

        $start = $request->input('start', 0);
        $limit = $request->input('limit', 0);

        $list = TagHelper::findByItemTag($itemId,$start,$limit);

        $resVal['data'] = $list;
        
      
        return $resVal;
    }
    public function detailtag(Request $request){
        $resVal=array();
        $tag_id=$request->input('tag_id','');
        $tag_name=$request->input('tag_name','');
        $tag_icon=$request->input('tag_icon','');
        $tag_accountid=$request->input('tag_accountid','');
        $tag_photo=$request->input('tag_photo','');
        $tag_mediaid=$request->input('tag_mediaid','');
        $is_active=$request->input('isActive','');
        $start=$request->input('start',0);
        $limit=$request->input('limit',100);
        $list=TagHelper::tagdetail($tag_id,$tag_name,$tag_icon,$tag_accountid,$tag_photo,$tag_mediaid,$is_active,$start,$limit);
        $resVal['list']=$list;       
        $resVal['success']=True;
        $resVal['message']="Success";
        return $resVal;
    }  
    
    
}
