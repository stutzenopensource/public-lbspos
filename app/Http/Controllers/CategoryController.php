<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use DB;
use App\Category;
use Illuminate\Http\Request;
use App\Transformer\CategoryTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\LogHelper;
use App\Helper\NotificationHelper;

/**
 * Description of Category
 *
 * @author Stutzen Admin
 */
class CategoryController extends Controller {

    protected $table = 'tbl_category';

    //LIST ALL
    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        $category = new Category;
        $comp_id = $currentuser->comp_id;
        $id = $request->input('id');
        $name = $request->input('name', '');
        $is_active = $request->input('is_active', '');

        $builder = DB::table('tbl_category')
                ->select("*");

        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        if (!empty($comp_id)) {
            $builder->where('comp_id', '=', $comp_id);
        }
        if (!empty($id)) {
            $builder->where('id', '=', $request->input('id'));
        }
        if (!empty($name)) {
            $builder->where('name', 'like', '%' . $name . '%');
        }
        if ($is_active != '') {
            $builder->where('is_active', '=', $is_active);
        }

        $resVal['total'] = $builder->count();
        $resVal['list'] = $builder->skip($start)->take($limit)->get();
        LogHelper::info1('Category List All ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }

    //LIST BY ID
    public function listById(Request $request,$id) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        LogHelper::info1('Category ListById ' . $request->fullurl(), $request->all());
        return $this->item(Category::findOrFail($id), new CategoryTransformer());
    }

    //SAVE
    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Category Added Successfully';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        if ($request->input('name') != NULL) {
            $cate = Category::where('name', "=", $request->input('name'))->where('is_active', '=', 1)->get();

            if (count($cate) > 0) {
                $resVal['success'] = FALSE;
                $resVal['message'] = 'Category Name Is Already Exist';
                return $resVal;
            }
        }
        $currentuser = Auth::user();

        $category = new Category;
        $category->comp_id = $currentuser->comp_id;
        $category->created_by = $currentuser->id;
        $category->updated_by = $currentuser->id;
        $category->fill($request->all());
        $category->save();
        $resVal['id'] = $category->id;

        LogHelper::info1('Category Save ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Category', 'save', $screen_code, $category->id);


        return $resVal;
    }

    //UPDATE
    public function update(Request $request, $id) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        try {
            $category = Category::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                        'error' => [
                            'message' => 'Category Not Found'
                        ]
                            ], 404);
        }
        if ($request->input('name') != NULL) {
            $cate = Category::where('name', "=", $request->input('name'))->where('id', '!=', $id)->where('is_active', '=', 1)->get();

            if (count($cate) > 0) {
                $resVal['success'] = FALSE;
                $resVal['message'] = 'Category Name Is Already Exist';
                return $resVal;
            }
        }
        $resVal['message'] = 'Category Updated Successfully';
        $resVal['success'] = TRUE;
        $currentuser = Auth::user();

        $category->updated_by = $currentuser->id;
        $category->fill($request->all());
        $category->save();
        $resVal['id'] = $category->id;

        LogHelper::info1('Category Update ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Category', 'update', $screen_code, $category->id);


        return $resVal;
    }

    //DELETE
    public function delete(Request $request, $id) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $category = Category::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                        'error' => [
                            'message' => 'Category Not Found'
                        ]
                            ], 404);
        }

        $category->is_active = 0;
        $category->updated_by = $currentuser->id;
        $category->save();
        $resVal['message'] = 'Category Deleted Successfully';
        $resVal['success'] = TRUE;
        $resVal['id'] = $category->id;

        LogHelper::info1('Category Delete ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Category', 'delete', $screen_code, $category->id);


        return $resVal;
    }

    public function listWithLimit(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $limit = $request->input('limit', 100);
        LogHelper::info1('Category ListWithLimit ' . $request->fullurl(), $request->all());
        return Category::where('name', 'like', '%%')
                        ->where('id', '=', '23')->paginate($limit);
    }

    //LIST BY MANY
    public function listByIdNameDesc(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $limit = $request->input('limit', 100);
        $id = $request->input('id');
        $name = $request->input('name');
        $description = $request->input('description');

        $builder = Category::query();

        if (!empty($term['id'])) {
            $builder->where('id', '=', $id);
        }
        if (!empty($term['name'])) {
            $builder->where('name', 'like', $name . '%');
            $queries = DB::getQueryLog();
            $last_query = end($queries);
            DB::connection()->enableQueryLog();
            var_dump(DB::getQueryLog());
        }
        if (!empty($term['description'])) {
            $builder->where('description', '=', $description . '%');
        }
        LogHelper::info1('Category ListByIdNameDesc ' . $request->fullurl(), $request->all());
        return $result = $builder->orderBy('id')->get();
    }

    //listall

    public function listByIdName(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $id = $request->input('id');
        $name = $request->input('name');

        //$builder = Account::query();
        $builder = DB::table('tbl_category')->select('id as id', 'name as name', 'is_active as isactive', 'comp_id as companyId', 'comments as comments', 'description as description');

        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);



        if (!empty($id)) {

            $builder->where('id', '=', '$id');

//            DB::connection()->enableQueryLog();  
//            var_dump(DB::getQueryLog());
        }

        if (!empty($name)) {

            $builder->where('name', '=', '$name');
//            DB::connection()->enableQueryLog();  
//            var_dump(DB::getQueryLog());
        }


        DB::connection()->enableQueryLog();
        var_dump(DB::getQueryLog());
        $resVal['total'] = $builder->count();
        $resVal['list'] = $builder->skip($start)->take($limit)->get();
        LogHelper::info1('Category ListByIdName ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }

}
