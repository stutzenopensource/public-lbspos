<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\City;
use Illuminate\Http\Request;
use App\Transformer\CityTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\LogHelper;
use App\Helper\NotificationHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CityController
 *
 * @author Deepa
 */
class CityController extends Controller {

    //put your code here

    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'City Added Successfully';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
//        $validator = Validator::make($request->all(), [
//                    'code' => 'required|unique:tbl_city,code',
//                    'name' => 'required|unique:tbl_city,name'
//        ]);
//        if ($validator->fails()) {
//            $resVal['success'] = FALSE;
//            $resVal['message'] = 'Error in request format';
//            if (array_key_exists('code', $validator->failed())) {
//                $resVal['message'] = 'City Code is already exists';
//            }
//            if (array_key_exists('name', $validator->failed())) {
//                $resVal['message'] = 'City Name is already exists';
//            }
//
//            return $resVal;
//        }
        
        if ($request->input('code') != NULL) {

            $st = City::where('code', "=", $request->input('code'))->where('is_active', '=', 1)->get();

            if (count($st) > 0) {
                $resVal['success'] = FALSE;
                $resVal['message'] = 'City Code Is Already Exists';
                return $resVal;
            }
        }
        if ($request->input('name') != NULL) {

            $st = City::where('name', "=", $request->input('name'))->where('is_active', '=', 1)->get();

            if (count($st) > 0) {
                $resVal['success'] = FALSE;
                $resVal['message'] = 'City Name Is Already Exists';
                return $resVal;
            }
        }


        $city = new City;
        $city->created_by = $currentuser->id;
        $city->updated_by = $currentuser->id;
        $city->is_active = $request->input('is_active', 1);

        $city->fill($request->all());
        $city->save();

        LogHelper::info1('City Save ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'City', 'save', $screen_code, $city->id);

        $resVal['id'] = $city->id;

        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $name = $request->input('name', '');
        $state_id = $request->input('state_id', '');
        $country_name = $request->input('country_name', '');
        $state_name = $request->input('state_name', '');
        $country_id = $request->input('country_id', '');
         $is_active  =$request->input('is_active',1);
        //$status = $request->input('status', '');
        $builder = DB::table('tbl_city')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if ($state_id != '') {
            $builder->where('state_id', '=', $state_id);
        }
        if ($country_id != '') {
            $builder->where('country_id', '=', $country_id);
        }
        if (!empty($name)) {
            $builder->where('name', 'like', '%' . $name . '%');
        }

        if (!empty($country_name)) {
            $builder->where('country_name', '=', $country_name);
        }
        if (!empty($state_name)) {
            $builder->where('state_name', '=', $state_name);
        }
        if (($is_active) != '') {
            $builder->where('is_active', '=', $is_active);
        }
        
        $builder->orderBy('name', 'asc');

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $cityCollection = $builder->get();
        } else {
            $cityCollection = $builder->skip($start)->take($limit)->get();
        }

        $resVal['list'] = $cityCollection;

        LogHelper::info1('City List All ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));

        return ($resVal);
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'City Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        if ($request->input('code') != NULL) {

            $st = City::where('code', "=", $request->input('code'))->where('is_active', '=', 1)->where('id', '!=', $id)->get();

            if (count($st) > 0) {
                $resVal['success'] = FALSE;
                $resVal['message'] = 'City Code Is Already Exists';
                return $resVal;
            }
        }
        if ($request->input('name') != NULL) {

            $st = City::where('name', "=", $request->input('name'))->where('is_active', '=', 1)->where('id', '!=', $id)->get();

            if (count($st) > 0) {
                $resVal['success'] = FALSE;
                $resVal['message'] = 'City Name Is Already Exists';
                return $resVal;
            }
        }
        try {
            $city = City::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'City Not Found';
            return $resVal;
        }
        $city->updated_by = $currentuser->id;
        $city->fill($request->all());
        $city->save();

        LogHelper::info1('City Update ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'City', 'update', $screen_code, $city->id);

        return $resVal;
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'City Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        try {
            $city = City::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'City Not Found';
            return $resVal;
        }
        
        $city->is_active=0;
        $city->updated_by = $currentuser->id;
        $city->save();

        LogHelper::info1('City Delete ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'City', 'delete', $screen_code, $city->id);

        return $resVal;
    }

}

?>
