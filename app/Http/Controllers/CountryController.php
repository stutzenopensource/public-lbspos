<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\Country;
use Illuminate\Http\Request;
use App\Transformer\UomTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\LogHelper;
use App\Helper\NotificationHelper;

class CountryController extends Controller {

    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Country Added Successfully';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

//        $validator = Validator::make($request->all(), [
//                    'code' => 'required|unique:tbl_country,code',
//                    'name' => 'required|unique:tbl_country,name'
//        ]);
//        if ($validator->fails()) {
//            $resVal['success'] = FALSE;
//            $resVal['message'] = 'Error in request format';
//            if (array_key_exists('code', $validator->failed())) {
//                $resVal['message'] = 'Country Code is already exists';
//            }
//            if (array_key_exists('name', $validator->failed())) {
//                $resVal['message'] = 'Country Name is already exists';
//            }
//
//            return $resVal;
//        }
        
        if ($request->input('code') != NULL) {

            $st = Country::where('code', "=", $request->input('code'))->where('is_active', '=', 1)->get();

            if (count($st) > 0) {
                $resVal['success'] = FALSE;
                $resVal['message'] = 'Country Code Is Already Exists';
                return $resVal;
            }
        }
        if ($request->input('name') != NULL) {

            $st = Country::where('name', "=", $request->input('name'))->where('is_active', '=', 1)->get();

            if (count($st) > 0) {
                $resVal['success'] = FALSE;
                $resVal['message'] = 'Country Name Is Already Exists';
                return $resVal;
            }
        }
        $currentuser = Auth::user();
        $country = new Country;
        $country->updated_by = $currentuser->id;
        $country->created_by = $currentuser->id;
        $country->fill($request->all());
        $country->save();
        if ($country->is_default == 1) {
            $st = DB::table('tbl_country')->where('id', '!=', $country->id)->update(['is_default' => 0]);
        }
        $resVal['id'] = $country->id;

        LogHelper::info1('Country Save ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Country', 'save', $screen_code, $country->id);

        return $resVal;
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Country Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        if ($request->input('code') != NULL) {

            $st = Country::where('code', "=", $request->input('code'))->where('is_active', '=', 1)->where('id', '!=', $id)->get();

            if (count($st) > 0) {
                $resVal['success'] = FALSE;
                $resVal['message'] = 'Country Code Is Already Exists';
                return $resVal;
            }
        }
        if ($request->input('name') != NULL) {

            $st = Country::where('name', "=", $request->input('name'))->where('is_active', '=', 1)->where('id', '!=', $id)->get();

            if (count($st) > 0) {
                $resVal['success'] = FALSE;
                $resVal['message'] = 'Country Name Is Already Exists';
                return $resVal;
            }
        }
        try {
            $country = Country::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Country Not Found';
            return $resVal;
        }
        
        $country->updated_by = $currentuser->id;
        $country->fill($request->all());
        $country->save();
        if ($country->is_default == 1) {
            $st = DB::table('tbl_country')->where('id', '!=', $country->id)->update(['is_default' => 0]);
        }

        LogHelper::info1('Country Update ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Country', 'update', $screen_code, $country->id);

        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $name = $request->input('name', '');
        $is_active  =$request->input('is_active',1);
        $builder = DB::table('tbl_country')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }

        if (!empty($name)) {
            $builder->where('name', '=', $name);
        }
        if (($is_active) != '') {
            $builder->where('is_active', '=', $is_active);
        }
        $builder->orderBy('name', 'asc');

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $countryCollection = $builder->get();
        } else {

            $countryCollection = $builder->skip($start)->take($limit)->get();
        }

        $resVal['list'] = $countryCollection;

LogHelper::info1('Country List All ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));

        return ($resVal);
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Country Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        try {
            $country = Country::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Country Not Found';
            return $resVal;
        }
        
        $currentuser = Auth::user();
        
        $country->is_active=0;
        $country->updated_by = $currentuser->id;
        $country->save();
        

        LogHelper::info1('Country Delete ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Country', 'delete', $screen_code, $country->id);

        return $resVal;
    }

}

?>
