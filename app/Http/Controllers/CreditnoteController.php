<?php
namespace App\Http\Controllers;
use DB;
use App\Creditnote;
use Illuminate\Http\Request;
//use App\Transformer\ItemTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\LogHelper;
use App\Helper\NotificationHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CreditnoteController
 *
 * @author Deepa
 */
class CreditnoteController extends Controller {
    //put your code here
  public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Credit Note Added Successfully';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'save');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
      
        $creditnote = new Creditnote;
       
        $creditnote->created_by = $currentuser->id;
        $creditnote->updated_by = $currentuser->id;
        $creditnote->fill($request->all());
        $creditnote->save();

        $resVal['id'] = $creditnote->id;
        LogHelper::info1('Credit Note Save ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Credit Note', 'save', $screen_code, $creditnote->id);
        return $resVal;
    }
     public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Credit Note Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'update');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $creditnote = Creditnote::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Credit Note Not Found';
            return $resVal;
        }
        $creditnote->updated_by = $currentuser->id;
        $creditnote->fill($request->all());
        $creditnote->save();
        LogHelper::info1('Credit Note Update ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Credit Note', 'update', $screen_code, $creditnote->id);
        return $resVal;
    }
    public function delete(Request $request,$id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Credit Note Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'delete');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        try {
            $creditnote = Creditnote::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Credit Note Not Found';
            return $resVal;
        }

        $creditnote->delete();
        LogHelper::info1('Credit Note Delete ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Credit Note', 'delete', $screen_code, $creditnote->id);
        return $resVal;
    }
     public function listAll(Request $request) {
         $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'list');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
       
        $id = $request->input('id');
       
        $builder = DB::table('tbl_credit_note')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
       
        $builder->orderBy('id','desc');
        $resVal['total'] = $builder->count();
        $resVal['list'] = $builder->skip($start)->take($limit)->get();
        LogHelper::info1('Credit Note List All ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }
}

?>
