<?php

namespace App\Http\Controllers;

use DateTime;
use DateInterval;
use DatePeriod;
use DB;
use App\ActivityLog;
use App\PurchasePayment;
use Carbon\Carbon;
use App\Invoice;
use App\Invoiceitem;
use App\Attributevalue;
use App\Attribute;
use app\Customer;
use Illuminate\Http\Request;
use App\Transformer\InvoiceTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\LogHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of invoicesController
 *
 * @author Deepa
 */
class DashBoardController extends Controller {

    //put your code here
    public function InvoiceVsPayment(Request $request, $id) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        //Get Current and Previous Date
        $current_date = Carbon::now();
        $id = $id + 1;
        $previous_date = date('Y-m-d', strtotime("-" . $id . " days"));
        //Get 180 Date  and Store in an array
        $report_ending_date = date(($current_date));
        $report_starting_date = date($previous_date);
        $report_starting_date1 = date('Y-m-d', strtotime($report_starting_date . '+1 day'));
        while (strtotime($report_starting_date1) < strtotime($report_ending_date)) {

            $report_starting_date1 = date('Y-m-d', strtotime($report_starting_date1 . '+1 day'));
            $dates[] = $report_starting_date1;
        }
        //print_r($dates);
        //Get Invoice Date Report          
        $current_date1 = $current_date->format('Y-m-d');
        $builder = DB::table('tbl_invoice')
                ->select(DB::raw('date'), DB::raw('total_amount'), DB::raw('sum(total_amount) as total'))
                ->where('is_active', '=', 1)
                ->where('date', '<=', $current_date)
                ->where('date', '>=', $previous_date)
                ->groupBy('date')
                ->orderBy('date', 'asc')
                ->get();

        //Get Payment Date Report           
        $builder_payemnt = DB::table('tbl_payment')
                ->select(DB::raw('CAST(date AS DATE) as ddate, amount, sum(amount) as total'))
                ->where('is_active', '=', '1')
                ->where('date', '<=', $current_date1)
                ->where('date', '>=', $previous_date)
                ->groupBy('ddate')
                ->orderBy('date', 'asc')
                ->get();

        $invoice_payment_list = array();
        $start_i = 0;
        //Update Date Array       
        foreach ($builder as $gettot) {
            $invoice_list = array();
            $key = array_search($gettot->date, $dates);
            {
                for ($i = $start_i; $i < $key; $i++) {
                    $invoice_list['date'] = date('d-m-Y', strtotime($dates[$i]));
                    $invoice_list['invoice_amount'] = "00";
                    $invoice_list['payment_amount'] = "00";
                    array_push($invoice_payment_list, $invoice_list);
                }
            }
            $invoice_list['date'] = date('d-m-Y', strtotime($gettot->date));
            $invoice_list['invoice_amount'] = $gettot->total;
            $invoice_list['payment_amount'] = "00";
            array_push($invoice_payment_list, $invoice_list);
            $start_i = $i;
            $start_i ++;
        }
        for ($i = $start_i; $i < $id - 1; $i++) {
            $invoice_list['date'] = date('d-m-Y', strtotime($dates[$i]));
            $invoice_list['invoice_amount'] = "00";
            $invoice_list['payment_amount'] = "00";
            array_push($invoice_payment_list, $invoice_list);
        }
        foreach ($builder_payemnt as $gettot) {
            $key = array_search($gettot->ddate, $dates);
            $invoice_payment_list[$key]['payment_amount'] = $gettot->total;
        }
        //print_r($invoice_payment_list);                                    
        $resVal['list'] = $invoice_payment_list;
        LogHelper::info1('DashBoard InvoiceVsPayment List' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return $resVal;
    }

    public function InvoicePurchaseVsPayment(Request $request, $id) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        //Get Current and Previous Date
        $current_date = Carbon::now();
        $id = $id + 1;
        $previous_date = date('Y-m-d', strtotime("-" . $id . " days"));
        //Get 180 Date  and Store in an array
        $report_ending_date = date(($current_date));
        $report_starting_date = date($previous_date);
        $report_starting_date1 = date('Y-m-d', strtotime($report_starting_date . '+1 day'));
        while (strtotime($report_starting_date1) < strtotime($report_ending_date)) {

            $report_starting_date1 = date('Y-m-d', strtotime($report_starting_date1 . '+1 day'));
            $dates[] = $report_starting_date1;
        }
        //print_r($dates);
        //Get Invoice Payment Date Report          
        $current_date1 = $current_date->format('Y-m-d');
        $builder = DB::table('tbl_purchase_invoice')
                ->select(DB::raw('date'), DB::raw('total_amount'), DB::raw('sum(total_amount) as total'))
                ->where('is_active', '=', 1)
                ->where('date', '<=', $current_date)
                ->where('date', '>=', $previous_date)
                ->groupBy('date')
                ->orderBy('date', 'asc')
                ->get();

        //Get Payment Date Report           
        $builder_payemnt = DB::table('tbl_purchase_payment')
                ->select(DB::raw('CAST(date AS DATE) as ddate, amount, sum(amount) as total'))
                ->where('is_active', '=', '1')
                ->where('date', '<=', $current_date1)
                ->where('date', '>=', $previous_date)
                ->groupBy('ddate')
                ->orderBy('date', 'asc')
                ->get();

        $invoice_payment_list = array();
        $start_i = 0;
        //Update Date Array       
        foreach ($builder as $gettot) {
            $invoice_list = array();
            $key = array_search($gettot->date, $dates);
            {
                for ($i = $start_i; $i < $key; $i++) {
                    $invoice_list['date'] = date('d-m-Y', strtotime($dates[$i]));
                    $invoice_list['invoice_amount'] = "00";
                    $invoice_list['payment_amount'] = "00";
                    array_push($invoice_payment_list, $invoice_list);
                }
            }
            $invoice_list['date'] = date('d-m-Y', strtotime($gettot->date));
            $invoice_list['invoice_amount'] = $gettot->total;
            $invoice_list['payment_amount'] = "00";
            array_push($invoice_payment_list, $invoice_list);
            $start_i = $i;
            $start_i ++;
        }
        for ($i = $start_i; $i < $id - 1; $i++) {
            $invoice_list['date'] = date('d-m-Y', strtotime($dates[$i]));
            $invoice_list['invoice_amount'] = "00";
            $invoice_list['payment_amount'] = "00";
            array_push($invoice_payment_list, $invoice_list);
        }
        foreach ($builder_payemnt as $gettot) {
            $key = array_search($gettot->ddate, $dates);
            $invoice_payment_list[$key]['payment_amount'] = $gettot->total;
        }
        //print_r($invoice_payment_list);                                               
        $resVal['list'] = $invoice_payment_list;
        LogHelper::info1('DashBoard InvoicePurchaseVsPayment List' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return $resVal;
    }

    public function IncomeVsExpenses(Request $request, $id) {
        $resVal = array();
        $resVal['message'] = "Success";
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $current_date = Carbon::now();
        $carbon = $current_date->subMonths($id);

        $monthArray = array();
        //By Use For Loop
        $start = new DateTime(); // Today date
        $star = $start->modify('+1 month');
        $end = new DateTime($carbon->toDateTimeString()); // Create a datetime object from your Carbon object
        $interval = DateInterval::createFromDateString('1 month'); // 1 month interval
        $period = new DatePeriod($end, $interval, $start); // Get a set of date beetween the 2 period
        $months = array();
        foreach ($period as $dt) {
            $months[] = $dt->format("m Y");
        }
        $monthincome = array();
        for ($i = 0; $i <= $id; $i++) {
            $month_year = explode(" ", $months[$i]);
            $income_amount = DB::table('tbl_transaction')
                    ->whereMonth('transaction_date', "=", $month_year[0])->whereYear('transaction_date', '=', $month_year[1])
                    ->where('is_active', '=', 1)
                    ->where('voucher_type', '!=', 'transfer')
                    ->where('voucher_type', '!=', 'inital_balance')
                     ->where('is_cash_flow',1)
                    ->sum('credit');

            $expenses_amount = DB::table('tbl_transaction')
                    ->whereMonth('transaction_date', "=", $month_year[0])->whereYear('transaction_date', '=', $month_year[1])
                    ->where('is_active', '=', 1)
                    ->where('voucher_type', '!=', 'transfer')
                    ->where('voucher_type', '!=', 'inital_balance')
                     ->where('is_cash_flow',1)
                    ->sum('debit');

            $dateObj = DateTime::createFromFormat('!m', $month_year[0]);
            $monthName = $dateObj->format('M');

            $monthincome['month'] = $monthName . ", " . $month_year[1];
            $monthincome['Income'] = $income_amount;
            $monthincome['Expenses'] = $expenses_amount;
            array_push($monthArray, $monthincome);
        }
        //print_r($monthArray);             
        $resVal['list'] = $monthArray;
        LogHelper::info1('DashBoard IncomeVsExpenses List' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }

    public function categoryVsIncomeSummery(Request $request, $id) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $current_date = Carbon::now();
        $current_date1 = Carbon::now();
        $carbon = $current_date1->subDays($id);

        $previous_date = $carbon->format('Y-m-d');
        $current_date = $current_date->format('Y-m-d');


        $builder = DB::table("tbl_transaction")
                ->select(DB::raw("SUM(credit) as amount, account_category"))
                ->where('is_active', '=', 1)
                ->where('transaction_date', '<=', $current_date)
                ->where('transaction_date', '>=', $previous_date)
                 ->where('is_cash_flow',1)
                ->groupBy(DB::raw("account_category"))
                ->get();

        $resVal['success'] = TRUE;


        $resVal['list'] = $builder;
        LogHelper::info1('DashBoard CategoryVsIncomeSummary List' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }

    public function categoryVsExpenseSummery(Request $request, $id) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $current_date = Carbon::now();
        $current_date1 = Carbon::now();
        $carbon = $current_date1->subDays($id);

        $previous_date = $carbon->format('Y-m-d');
        $current_date = $current_date->format('Y-m-d');


        $builder = DB::table("tbl_transaction")
                ->select(DB::raw("SUM(debit) as amount, account_category"))
                ->where('is_active', '=', 1)
                ->where('transaction_date', '<=', $current_date)
                ->where('transaction_date', '>=', $previous_date)
                 ->where('is_cash_flow',1)
                ->groupBy(DB::raw("account_category"))
                ->get();

        $resVal['success'] = TRUE;


        $resVal['list'] = $builder;
        LogHelper::info1('DashBoard CategoryVsExpenseSummary List' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }

    public function incomeVsExpenseSummery(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        //print_r($yearcode);
        $builder = DB::table("tbl_transaction")
                ->select(DB::raw("SUM(debit) as amount, account as type"))
                ->where('is_active', '=', 1)
                ->where('account_id', '=', 2)
                ->groupBy("account_id")
                ->get();
        $builder1 = DB::table("tbl_transaction")
                ->select(DB::raw("SUM(debit) as amount, account as type"))
                ->where('is_active', '=', 1)
                ->where('account_id', '=', 1)
                ->groupBy("account_id")
                ->get();


        $resVal['success'] = TRUE;
        $resVal['expense'] = $builder->first();
        $resVal['income'] = $builder1->first();
        LogHelper::info1('DashBoard IncomeVsExpenseSummary  Expense List' . $request->fullurl(),json_decode(json_encode($resVal['expense']), true) );
        LogHelper::info1('DashBoard IncomeVsExpenseSummary  Income List' . $request->fullurl(),json_decode(json_encode($resVal['income']), true) );
        return ($resVal);
    }

    public function SalesPayment(Request $request, $id) {
        $resVal = array();
        $resVal['message'] = "Success";
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $current_date = Carbon::now();
        $carbon = $current_date->subMonths($id);

        $monthArray = array();
        //By Use For Loop
        $start = new DateTime(); // Today date
        $star = $start->modify('+1 month');
        $end = new DateTime($carbon->toDateTimeString()); // Create a datetime object from your Carbon object
        $interval = DateInterval::createFromDateString('1 month'); // 1 month interval
        $period = new DatePeriod($end, $interval, $start); // Get a set of date beetween the 2 period
        $months = array();
        foreach ($period as $dt) {
            $months[] = $dt->format("m Y");
        }
        $monthincome = array();
        for ($i = 0; $i <= $id; $i++) {
            $month_year = explode(" ", $months[$i]);
            $income_amount = DB::table('tbl_invoice')
                    ->whereMonth('date', "=", $month_year[0])->whereYear('date', '=', $month_year[1])
                    ->where('is_active', '=', 1)
                    ->sum('total_amount');

            $payment_amount = DB::table('tbl_payment')
                    ->whereMonth('date', "=", $month_year[0])->whereYear('date', '=', $month_year[1])
                    ->where('is_active', '=', 1)
                    ->sum('amount');

            $dateObj = DateTime::createFromFormat('!m', $month_year[0]);
            $monthName = $dateObj->format('M');

            $monthincome['month'] = $monthName . ", " . $month_year[1];
            $monthincome['invoice_amount'] = $income_amount;
            $monthincome['invoice_payment'] = $payment_amount;
            array_push($monthArray, $monthincome);
        }
        //print_r($monthArray);             
        $resVal['list'] = $monthArray;
        LogHelper::info1('DashBoard SalesPayment List' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }

    public function SalesPurchaseMonthlyReport(Request $request, $month) {
        $resVal = array();
        $resVal['message'] = "Success";
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $date = Carbon::now();

        $date = date_parse_from_format("Y-m-d", $date);
       // $month = $date["month"];
        $year = $date["year"];

        $dateObj = DateTime::createFromFormat('!m', $month);
        $mon = $dateObj->format('F'); // March     
        $builder = DB::table('tbl_invoice')
                ->whereMonth('date', "=", $month)
                ->whereYear('date', "=", $year)
                ->where('is_active', '=', 1);

        $salesArray1['month'] = $mon;
        $salesArray1['count'] = $builder->count();
        $total = $builder->select(DB::raw('sum(total_amount) as total'))->first();
        $salesArray1['total'] = $total->total;
        $resVal['sales'] = $salesArray1;

        $builder = DB::table('tbl_payment')
                ->whereMonth('date', "=", $month)
                ->whereYear('date', "=", $year)
                ->where('is_active', '=', 1);

        $paymentArray1['month'] = $mon;
        $paymentArray1['count'] = $builder->count();
        $total = $builder->select(DB::raw('sum(amount) as total'))->first();
        $paymentArray1['total'] = $total->total;
        $resVal['payment'] = $paymentArray1;

        $builder = DB::table('tbl_purchase_invoice')
                ->whereMonth('date', "=", $month)
                ->whereYear('date', "=", $year)
                ->where('is_active', '=', 1);

        $purchaseArray1['month'] = $mon;
        $purchaseArray1['count'] = $builder->count();
        $total = $builder->select(DB::raw('sum(total_amount) as total'))->first();
        $purchaseArray1['total'] = $total->total;
        $resVal['purchase'] = $purchaseArray1;

        $builder = DB::table('tbl_purchase_payment')
                ->whereMonth('date', "=", $month)
                ->whereYear('date', "=", $year)
                ->where('is_active', '=', 1);

        $purchasepaymentArray1['month'] = $mon;
        $purchasepaymentArray1['count'] = $builder->count();
        $total = $builder->select(DB::raw('sum(amount) as total'))->first();
        $purchasepaymentArray1['total'] = $total->total;
        $resVal['purchasepayment'] = $purchasepaymentArray1;
        
        //Receivable and payable from today to next 7 days 
            $fromDate = date("Y-m-d");
          $toDate = date('Y-m-d', strtotime("+" . 7 . " days"));
 
        $payable = DB::table('tbl_purchase_invoice as pi')
                -> where('duedate','>=',$fromDate)
                -> where('duedate','<=',$toDate)
                ->where('pi.is_active', '=', 1)
                ->sum('pi.total_amount');
        
        $paymentAmount = DB::table('tbl_purchase_invoice as pi')
                ->leftJoin('tbl_purchase_payment as p', 'p.purchase_invoice_id', '=', 'pi.id')
                -> where('duedate','>=',$fromDate)
                -> where('duedate','<=',$toDate)
                ->where('pi.is_active', '=', 1)
                ->where('p.is_active', '=', 1)
                ->sum('p.amount');
        
        $purAdvPayment= DB::table('tbl_adv_payment')
                ->where('voucher_type','advance_purchase')
//                -> where('date','>=',$fromDate)
//                -> where('date','<=',$toDate)
                ->where('is_active',1)
                ->sum('balance_amount');
        
        $payableAmount=$payable-$paymentAmount-$purAdvPayment;
        
        $payableCount = DB::table('tbl_purchase_invoice as pi') 
                -> where('duedate','>=',$fromDate)
                -> where('duedate','<=',$toDate)
                -> where('status','!=','paid')
                ->where('pi.is_active', '=', 1)
                ->count();
        
        
         $receivable = DB::table('tbl_invoice as so')
                -> where('duedate','>=',$fromDate)
                -> where('duedate','<=',$toDate)
                ->where('so.is_active', '=', 1)
                ->sum('so.total_amount');
        
        $paymentAmt = DB::table('tbl_invoice as so')
                ->leftJoin('tbl_payment as p', 'so.id', '=', 'p.invoice_id')
                -> where('so.duedate','>=',$fromDate)
                -> where('so.duedate','<=',$toDate)
                ->where('so.is_active', '=', 1)
                ->where('p.is_active', '=', 1)
                ->sum('p.amount');
        
        $advPayment= DB::table('tbl_adv_payment')
                ->where('voucher_type','advance_invoice')
//                -> where('date','>=',$fromDate)
//                -> where('date','<=',$toDate)
                ->where('is_active',1)
                ->sum('balance_amount');
        
        $receivableAmount=$receivable-$paymentAmt-$advPayment;
     
        $receivableCount = DB::table('tbl_invoice as so') 
                -> where('duedate','>=',$fromDate)
                -> where('duedate','<=',$toDate)
                -> where('status','!=','paid')
                ->where('so.is_active', '=', 1)
                ->count();
        
        $payableArray['count'] = $payableCount;
        $payableArray['total'] = $payableAmount;
        $receivableArray['count'] = $receivableCount;
        $receivableArray['total'] = $receivableAmount;
        $payableArray['fromDate'] = $fromDate;
        $payableArray['toDate'] = $toDate;
        $receivableArray['fromDate'] = $fromDate;
        $receivableArray['toDate'] = $toDate;
        $resVal['payable'] = $payableArray;
        $resVal['receivable'] = $receivableArray;
        LogHelper::info1('DashBoard SalesPurchaseMonthlyReport Payable List ' . $request->fullurl(),json_decode(json_encode($resVal['payable']), true) );
        LogHelper::info1('DashBoard SalesPurchaseMonthlyReport Receivable List ' . $request->fullurl(),json_decode(json_encode($resVal['receivable']), true) );
        return ($resVal);
    }

    public function salesMonthReport(Request $request) {
        $resVal = array();
        $resVal['message'] = "Success";
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $fromdate = $request->input('from_date', '');
        $todate = $request->input('to_date', '');
        $start = new DateTime($fromdate);
        $start->modify('first day of this month');
        $end = new DateTime($todate);
        $end->modify('first day of next month');
        $interval = DateInterval::createFromDateString('1 month');
        $period = new DatePeriod($start, $interval, $end);

        foreach ($period as $dt) {
            $months[] = $dt->format("Y m");
        }

        $monthincome = array();
        $monthArray = array();

        for ($i = 0; $i < count($months); $i++) {

            $month_year = explode(" ", $months[$i]);
            $amount = DB::table('tbl_invoice')
                    ->whereMonth('date', "=", $month_year[1])->whereYear('date', '=', $month_year[0])
                    ->where('is_active', '=', 1)
                    ->sum('total_amount');

            $dateObj = DateTime::createFromFormat('!m', $month_year[1]);
            $monthName = $dateObj->format('M');

            $monthincome['name'] = $month_year[0] . " " . $monthName;
            $monthincome['value'] = $amount;
            array_push($monthArray, $monthincome);
        }
        //print_r($monthArray);             
        $resVal['list'] = $monthArray;
        LogHelper::info1('DashBoard SalesMonth Report' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }

    public function salesMonthTotalReport(Request $request, $id) {
        $resVal = array();
        $resVal['message'] = "Success";
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $fromdate1 = Carbon::now();
        $fromdate = Carbon::now();
        $todate = $fromdate1->subMonths($id);

        $amount = DB::table('tbl_invoice')
                ->whereDate('date', "<=", $fromdate)
                ->whereDate('date', ">=", $todate)
                ->where('is_active', '=', 1)
                ->sum('total_amount');
        $builder = DB::table('tbl_invoice')
                ->whereDate('date', "<=", $fromdate)
                ->whereDate('date', ">=", $todate)
                ->where('is_active', '=', 1)
                ->get();

        //print_r($monthArray);             
        $resVal['count'] = $builder->count();
        $resVal['amount'] = $amount;
        LogHelper::info1('DashBoard SalesMonthTotal Report' . $request->fullurl(),$request->all());
        return ($resVal);
    }

    public function todayIncomeVsExpenses(Request $request) {
        $resVal = array();
        $resVal['message'] = "Success";
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
         if (!empty($request->input('date'))) {
             $current_date = $request->input('date');
         }
         else
             $current_date = Carbon::now();
        $d = date_parse_from_format("Y-m-d", $current_date);
        $current_date1 = date("Y-m-d", strtotime($current_date));
         $mon = $d["month"];
         $year=$d["year"];         
        $balance = DB::table('tbl_accounts')
                ->where('is_active', '=', 1)
                ->sum('balance');

        $income_amount = DB::table('tbl_transaction')
                ->whereDate('transaction_date', "=", $current_date1)
                ->where('is_active', '=', 1)
                ->where('voucher_type', '!=', 'transfer')
                ->where('voucher_type', '!=', 'inital_balance')
                 ->where('is_cash_flow',1)
                ->sum('credit');
        $expenses_amount = DB::table('tbl_transaction')
                ->whereDate('transaction_date', "=", $current_date1)
                ->where('voucher_type', '!=', 'transfer')
                ->where('voucher_type', '!=', 'inital_balance')
                ->where('is_active', '=', 1)
                 ->where('is_cash_flow',1)
                ->sum('debit');
        $income_month = DB::table('tbl_transaction')
                ->whereMonth('transaction_date', "=", $mon)
                ->whereYear('transaction_date', "=", $year)
                ->where('voucher_type', '!=', 'transfer')
                ->where('voucher_type', '!=', 'inital_balance')
                ->where('is_active', '=', 1)
                 ->where('is_cash_flow',1)
                ->sum('credit');
        $expenses_month = DB::table('tbl_transaction')
                ->whereMonth('transaction_date', "=", $mon)
                ->whereYear('transaction_date', "=", $year)
                ->where('voucher_type', '!=', 'transfer')
                ->where('voucher_type', '!=', 'inital_balance')
                ->where('is_active', '=', 1)
                 ->where('is_cash_flow',1)
                ->sum('debit');
        $resVal['balance'] = $balance;

        $resVal['today_income'] = $income_amount;
        $resVal['today_expense'] = $expenses_amount;
        $resVal['month_income'] = $income_month;
        $resVal['month_expense'] = $expenses_month;
        LogHelper::info1('DashBoard todayIncomeVsExpenses Report' . $request->fullurl(),$request->all());
        return ($resVal);
    }
    public function AgeingReport(Request $request) {
        $resVal = array();
        $resVal['message'] = "Success";
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        //  $current_date1 = Carbon::now();
        // $current_date = $current_date1->format('Y-m-d');

        $current_date = date('Y-m-d');
        $before_thirty = date('Y-m-d', strtotime('-30 days'));
        $current_date1 = date('Y-m-d');

        $builder = DB::table('tbl_invoice')
                        ->select(DB::raw('coalesce(sum(total_amount) - sum(paid_amount),0.0) as balance'))
                ->where('status', '!=', 'paid')
                ->where('is_active', '=', '1')
                ->where('duedate', '>', $current_date1)
                ->first();
        
        $invReturnAmount = DB::table('tbl_adv_payment')->select(DB::raw('coalesce(sum(balance_amount),0.0) as balance'))
//                ->where('payment_mode','invoice_return')
                ->where('voucher_type','advance_invoice')
                ->where('is_active',1)
                ->where('date','>',$current_date1)
                ->first();
        
        $receivable_payable = array();
        $inbetween_day = array();
        
        $inbetween_day['comming_due'] = $builder->balance - $invReturnAmount->balance;
        array_push($receivable_payable, $inbetween_day);
        $start = 1;
        $end = 30;
        // $before_thirty = $current_date1->subDays(30);
        for ($i = 1; $i <= 4; $i++) {
            $inbetween_day = array();
             if($i==1)
                $start1='first';
            if($i==2)
                $start1='second';
            if($i==3)
                $start1='thrid';
            if ($i == 4) {
                $builder = DB::table('tbl_invoice')
                        ->select(DB::raw('coalesce(sum(total_amount) - sum(paid_amount),0.0) as balance'))
                        ->where('status', '!=', 'paid')
                        ->where('is_active', '=', '1')
                        ->where('duedate', '<=', $before_thirty)
                        ->first();
                
                 $invReturnAmount = DB::table('tbl_adv_payment')->select(DB::raw('coalesce(sum(balance_amount),0.0) as balance'))
//                                ->where('payment_mode','invoice_return')
                                ->where('voucher_type','advance_invoice')
                                ->where('is_active',1)
                                ->where('date','<=',$before_thirty)
                                ->first();

                $inbetween_day['above_90_days'] = $builder->balance-$invReturnAmount->balance;
            } else {
                $builder = DB::table('tbl_invoice')
                        ->select(DB::raw('coalesce(sum(total_amount) - sum(paid_amount),0.0) as balance'))
                        ->where('status', '!=', 'paid')
                        ->where('is_active', '=', '1')
                        ->where('duedate', '>', $before_thirty)
                        ->where('duedate', '<=', $current_date)
                        ->first();
                
                 $invReturnAmount = DB::table('tbl_adv_payment')
                                ->select(DB::raw('coalesce(sum(balance_amount),0.0) as balance'))
//                                ->where('payment_mode','invoice_return')
                                ->where('voucher_type','advance_invoice')
                                ->where('is_active',1)
                                ->where('date','>',$before_thirty)
                                ->where('date','<=',$current_date)
                                ->first();

 
                $inbetween_day[$start1 . '_' . $end . 'days'] = $builder->balance-$invReturnAmount->balance;
            }
            $current_date = $before_thirty;
            $before_thirty = date('Y-m-d', strtotime("-30 days", strtotime($before_thirty)));
            $start = $start + 30;
            $end = $end + 30;

            array_push($receivable_payable, $inbetween_day);
        }

        $current_date = date('Y-m-d');
        $before_thirty = date('Y-m-d', strtotime('-30 days'));
        $current_date1 = date('Y-m-d');
        $builder = DB::table('tbl_purchase_invoice')
                        ->select(DB::raw('coalesce(sum(total_amount) - sum(paid_amount),0.0) as balance'))
                ->where('status', '!=', 'paid')
                ->where('is_active', '=', '1')
                ->where('duedate', '>', $current_date1)
                ->first();
        
        $purReturnAmount = DB::table('tbl_adv_payment')->select(DB::raw('coalesce(sum(balance_amount),0.0) as balance'))
//               ->where('payment_mode','=','purchase_return')
               ->where('voucher_type','=','advance_purchase')
                ->where('date', ">", $current_date1)
                ->where('is_active', '=', 1)->first();
        $payable = array();
        $inbetween_day = array();
     
        $inbetween_day['comming_due'] = $builder->balance-$purReturnAmount->balance;
        array_push($payable, $inbetween_day);
        $start = 1;
        $end = 30;
        // $before_thirty = $current_date1->subDays(30);
        for ($i = 1; $i <= 4; $i++) {
            $inbetween_day = array();   
             if($i==1)
                $start1='first';
            if($i==2)
                $start1='second';
            if($i==3)
                $start1='thrid';
            if ($i == 4) {
                $builder = DB::table('tbl_purchase_invoice')
                        ->select(DB::raw('coalesce(sum(total_amount) - sum(paid_amount),0.0) as balance'))
                        ->where('status', '!=', 'paid')
                        ->where('is_active', '=', '1')
                        ->where('duedate', '<=', $before_thirty)
                        ->first();
                
          $purReturnAmount=DB::table('tbl_adv_payment')
               ->select(DB::raw('coalesce(sum(balance_amount),0.0) as balance'))
//               ->where('payment_mode','=','purchase_return')
               ->where('voucher_type','=','advance_purchase')
               ->where('date',"<=",$before_thirty)
               ->where('is_active','=',1)->first();
          
                $inbetween_day['above_90_days'] = $builder->balance-$purReturnAmount->balance;
            } else {
                $builder = DB::table('tbl_purchase_invoice')
                        ->select(DB::raw('coalesce(sum(total_amount) - sum(paid_amount),0.0) as balance'))
                        ->where('status', '!=', 'paid')
                        ->where('is_active', '=', '1')
                        ->where('duedate', '>', $before_thirty)
                        ->where('duedate', '<=', $current_date)
                        ->first();
                
                 $purReturnAmount=DB::table('tbl_adv_payment') 
                ->select(DB::raw('coalesce(sum(balance_amount),0.0) as balance'))
//               ->where('payment_mode','=','purchase_return')
               ->where('voucher_type','=','advance_purchase')
                ->where('date', ">", $before_thirty)
                ->where('date', '<=', $current_date)
               ->where('is_active','=','1')
               ->first();

 
                $inbetween_day[$start1 . '_' . $end . 'days'] = $builder->balance-$purReturnAmount->balance;
            }
            $current_date = $before_thirty;
            $before_thirty = date('Y-m-d', strtotime("-30 days", strtotime($before_thirty)));
            $start = $start + 30;           
            $end = $end + 30;

            array_push($payable, $inbetween_day);
        }

        $resVal['receivable_list'] = $receivable_payable;
        $resVal['payable_list'] = $payable;
        LogHelper::info1('DashBoard Ageing Report Receivable List ' . $request->fullurl(), json_decode(json_encode($resVal['receivable_list']), true));
        LogHelper::info1('DashBoard Ageing Report Payable List ' . $request->fullurl(), json_decode(json_encode($resVal['payable_list']), true));
        
        return ($resVal);
    }

}

?>