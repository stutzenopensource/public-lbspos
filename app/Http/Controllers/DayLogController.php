<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DayLogController
 *
 * @author Stutzen Admin
 */

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\DayLog;
use App\DenominationUser;
use App\Denomination;
use App\ClosingStock;
use App\Invoice;
use App\Helper\SmsHelper;
use App\Helper\AuthorizationHelper;
use App\Helper\LogHelper;
use App\Helper\NotificationHelper;
use DateTime;

class DayLogController extends Controller {

    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Day Log Added Successfully';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        $current_date = date('Y-m-d');
        $timestamp = date('H:i:s');
        $timestamp = strtotime($timestamp);
        $day_code = 0;
        $dayLog = new DayLog;
        //Finding dayCOde

        $dl = DB::table('tbl_day_log')->select('*')
                        ->whereRaw('id = (select max(`id`) from tbl_day_log)')->first();
        if ($dl != "") {
            $day_code = $dl->id;
            $dayLog->day_code = $day_code;
        } else {
            $dayLog->updated_by = $currentuser->id;
            $dayLog->created_by = $currentuser->id;
            $dayLog->is_active = 1;
            $dayLog->start_on = $timestamp;
            $dayLog->save();
            $day_code = $dayLog->id;
        }
        DayLogController::denominationSave($day_code, $request, $currentuser->id);
        $invoice = DB::table('tbl_invoice')->select(DB::raw('count(id) as count'), DB::raw('max(date)as date'), DB::raw('coalesce(sum(total_amount),0) as total'))
                        ->where('day_code', $day_code)->groupby('day_code')->first();

        try {
            $d = DayLog::findOrFail($day_code);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = ' Day Log Not Found';
            return $resVal;
        }

        if (($invoice) != '') {
            $d->total_amount = $invoice->total;
            SmsHelper::dayCloseSms($request, 1, $invoice);
        } else {
            $d->total_amount = 0;
            SmsHelper::dayCloseSms($request, 0, $invoice);
        }
        $d->end_on = $timestamp;
        $d->save();
        DayLogController::updateSkuClosingStock($day_code, $currentuser->id);
        DayLogController::insertSkuOpeningStock($day_code, $currentuser->id);
        DayLogController::insertNextDayCode($dayLog, $currentuser->id, $request);
        DayLogController::updateClosingStockDayCode($currentuser->id);

        //$resVal['id'] = $dayLog->id;
        LogHelper::info1('Day Log Save ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Day Log', 'save', $screen_code, $dayLog->id);
        return $resVal;
    }

    public static function denominationSave($dayCode, $request, $createdBy) {
        $total = 0.0;
        if (!empty($request->cash))
            $total = $total + $request->cash;
        if (!empty($request->card))
            $total = $total + $request->card;
        if (!empty($request->credit))
            $total = $total + $request->credit;
        if (!empty($request->advance))
            $total = $total + $request->advance;
        if (!empty($request->others))
            $total = $total + $request->others;
        $userId = $request->user_id;
        // going to save Denomination user
        $du = new DenominationUser;
        $du->day_code = $dayCode;
        $du->user_id = $userId;
        $du->is_active = 1;
        $du->updated_by = $createdBy;
        $du->created_by = $createdBy;
        $du->total = $total;
        $du->save();
        $denomination = $request->input('denomination');
        foreach ($denomination as $d) {
            $de = new Denomination;
            $de->fill($d);

            //Finding DayCode
            $dayLog = DB::table('tbl_day_log')->select('*')
                            ->whereRaw('id = (select max(`id`) from tbl_day_log)')->first();
            if ($dayLog != "")
                $de->day_code = $dayLog->id;

            $de->is_active = 1;
            $de->denomination_user_id = $du->id;
            $de->updated_by = $userId;
            $de->created_by = $userId;
            $de->save();
            LogHelper::info1('denomination Save ' . $request->fullurl(), $request->all());
        }
    }

    public static function updateSkuClosingStock($dayCode, $createdBy) {
        $inventory = DB::table('tbl_inventory')->select('*')->where('is_active', 1)->get();
        $dl = DB::table('tbl_day_log')->select('*')
                        ->whereRaw('id = (select max(`id`) from tbl_day_log)')->first();
        if ($dl != "") {
            $day_code = $dl->id;
        }
        foreach ($inventory as $i) {
            $closingStock = DB::table('tbl_closing_stock')->select('*')->where('product_id', '=', $i->product_id)
                            ->where('day_code', $dayCode)->first();
            $cs = new ClosingStock();
            if (($closingStock) != '') {
                $clo = new ClosingStock();
                try {
                    $clo = ClosingStock::findOrFail($closingStock->id);
                } catch (ModelNotFoundException $e) {

                    $resVal['success'] = FALSE;
                    $resVal['message'] = 'Close Not Found';
                    return $resVal;
                }
                $clo->closing_stock_qty = $i->quantity;
                $clo->modified_by = $createdBy;
                $clo->save();
            } else {
                $cs->day_code = $day_code;
                $cs->product_id = $i->product_id;
                $cs->sku = $i->sku;
                $cs->opening_stock_qty = $i->quantity;
                $cs->closing_stock_qty = $i->quantity;
                $cs->uom_id = $i->uom_id;
                $cs->created_by = $createdBy;
                $cs->modified_by = $createdBy;
                $cs->is_active = 1;
                $cs->save();
            }
        }
    }

    public static function insertSkuOpeningStock($dayCode, $createdBy) {
        $closingStock = DB::table('tbl_closing_stock')->select('*')->where('day_code', $dayCode)->get();
        foreach ($closingStock as $c) {
            $cs = new ClosingStock();
            $cs->day_code = 0;
            $cs->product_id = $c->product_id;
            $cs->sku = $c->sku;
            $cs->opening_stock_qty = $c->closing_stock_qty;
            $cs->closing_stock_qty = 0.0;
            $cs->uom_id = $c->uom_id;
            $cs->created_by = $createdBy;
            $cs->modified_by = $createdBy;
            $cs->is_active = 1;
            $cs->save();
        }
    }

    public static function insertNextDayCode($dayLog, $createdBy, $request) {
        $day = new DayLog();
        if (($dayLog) != '') {
            $day->updated_by = $createdBy;
            $day->created_by = $createdBy;
            $day->is_active = 1;
            $day->fill($request->all());
            $current_date = date('Y-m-d');
            $timestamp = date('H:i:s');
            $timestamp = strtotime($timestamp);
            $day->start_on = $timestamp;
            $day->total_amount = 0;
            $day->save();
            LogHelper::info1('InsertNextDaycode Save ' . $request->fullurl(), $request->all());
            
        }
      
    }

    public static function updateClosingStockDayCode($createdBy) {
        $dl = DB::table('tbl_day_log')->select('*')
                        ->whereRaw('id = (select max(`id`) from tbl_day_log)')->first();
        $day = 0.0;
        if (($dl)!= '') {
            $day = $dl->id;
        }
        $prevCode = 0;
        $closingStock = DB::table('tbl_closing_stock')->select('*')->where('day_code', '=', $prevCode)->get();
        if (count($closingStock) > 0) {
            foreach ($closingStock as $cl) {
                $c = new ClosingStock();
                try {
                    $c = ClosingStock::findOrFail($cl->id);
                } catch (ModelNotFoundException $e) {

                    $resVal['success'] = FALSE;
                    $resVal['message'] = ' Close Not Found';
                    return $resVal;
                }

                $c->day_code = $day;
                $c->modified_by = $createdBy;
                $c->save();
            }
        }
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $id = $request->input('id');
        $fromDate = $request->input('fromDate', '');
        $toDate = $request->input('toDate', '');

        $builder = DB::table('tbl_day_log')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if ($fromDate != '') {
            $builder->whereDate('start_on', '>=', $fromDate);
        }
        if ($toDate != '') {
            $builder->whereDate('end_on', '<=', $toDate);
        }

        $builder->orderBy('id', 'desc');

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $collection = $builder->get();
        } else {
            $collection = $builder->skip($start)->take($limit)->get();
        }

        $resVal['list'] = $collection;
        LogHelper::info1('Day Log List All ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }

    //To get Max dayCode
    public function getMaxDayCode(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $resVal['success'] = TRUE;
        $collection = DB::table('tbl_day_log')->select('*')
                        ->whereRaw('id = (select max(`id`) from tbl_day_log)')->get();
        $resVal['list'] = $collection;
        LogHelper::info1('GetMaxDayCode List ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }

    public function dayCodeReport(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $from_date = $request->input('from_date', '');
        $to_date = $request->input('to_date', '');
        $day_code = $request->input('day_code', '');

        $builder = DB::table('tbl_day_log as d')
                ->leftJoin('tbl_denomination_user as de', 'de.day_code', '=', 'd.id')
                ->select('d.*', DB::raw("sum(d.total_amount) as total , SUM(d.credit) as credit , SUM(d.card) as card , SUM(d.others) as others , SUM(d.advance) as advance, SUM(d.cash) as cash"))
                ->groupby('de.day_code');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($day_code)) {
            $builder->where('d.id', '=', $day_code);
        }
        if ($from_date != '') {
            $builder->whereDate('d.start_on', '>=', $from_date);
        }
        if ($to_date != '') {
            $builder->whereDate('d.end_on', '<=', $to_date);
        }

        $builder->orderBy('d.id', 'desc');

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $collection = $builder->get();
        } else {
            $collection = $builder->skip($start)->take($limit)->get();
        }

        foreach ($collection as $col) {
            $dayCode = $col->id;

            $builder1 = DB::table('tbl_invoice as i')
                    ->leftJoin('tbl_customer as c', 'i.customer_id', '=', 'c.id')
                    ->select(DB::raw("'invoice' as ref_type  ,  i.id as ref_id ,  customer_id , date"), DB::raw("sum(total_amount) as total_amount"), "day_code", "c.fname", "c.lname")
                    ->where('i.is_active', '=', 1)
                    ->where('day_code', '=', $dayCode)
                    ->groupby('day_code');
          
            
            $builder2 = DB::table('tbl_adv_payment as i')
                    ->leftJoin('tbl_customer as c', 'i.customer_id', '=', 'c.id')
                    ->select(DB::raw("'advancePayment' as ref_type , i.id as ref_id ,customer_id,date ,  sum(amount) as total_amount,day_code"), "c.fname", "c.lname")
                    ->where("i.is_active", '=', 1)                    
                    ->where('day_code', '=', $dayCode)
                    ->groupby('day_code');

            $builder = DB::table('tbl_payment as i')
                            ->leftJoin('tbl_customer as c', 'i.customer_id', '=', 'c.id')
                            ->select(DB::raw("'payment' as ref_type  , i.id as ref_id, customer_id,date,sum(amount) as total_amount"), "day_code", "c.fname", "c.lname")
                            ->where('i.is_active', '=', 1)
                            ->where('day_code', '=', $dayCode)
                            ->groupby('day_code')
                            ->union($builder1)->union($builder2)->get();

            $col->data = $builder;
        }

        $resVal['list'] = $collection;
        LogHelper::info1('Day Log List All ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }
    
    public function dayCodeReport_csvexport(Request $request) {
        $discountLists = $this->dayCodeReport($request);
        ini_set('max_execution_time', 600); //increase max_execution_time to 10 min if data set is very large
        //create a file
        $filename = "dayLogReport" . "_" . time() . ".csv";
        //  $filename = "customer" . "_" . date("Y-m-d_H-i", time()) . ".csv";
        $serverName = $_SERVER['SERVER_NAME'];

        $filePath = "download/$serverName/csv";

        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);

            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {

                if ($folderName == '')
                    $folderName = $folderList[$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index];

                if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        $filePath = getcwd() . '/' . $filePath;
        $arrHead=array('S.NO','Start On','End On','Cash','Credit','Card','Advance','Others','Total Amount');
        $output = fopen($filePath . "/" . $filename, 'w');
        fputcsv($output, $arrHead);
        $count=0;
        foreach ($discountLists['list'] as $values) {
             $count=$count+1;
             $start_date=date_format(new DateTime($values->start_on),'Y-m-d');
             $end_date=date_format(new DateTime($values->end_on),'Y-m-d');
             
             if($values->end_on =='0000-00-00 00:00:00')
                 $end_date='-';
            $arrVal=array($count,$start_date, $end_date, $values->cash,$values->credit,$values->card,$values->advance,$values->others,$values->total);
            fputcsv($output,$arrVal);
        }
        LogHelper::info1('DayLog Report CSV Download' . $request->fullurl(), $request->all());
        return response()->download($filePath . "/" . $filename, 'dayLogReport.csv');
    }

}
