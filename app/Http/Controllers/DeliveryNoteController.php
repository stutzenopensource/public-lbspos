<?php

namespace App\Http\Controllers;

use DB;
use App\DeliveryNote;
use App\DeliveryNoteDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\LogHelper;
use App\Helper\NotificationHelper;
use App\Helper\GeneralHelper;
use App\Helper\InventoryHelper;
use App\DeliveryNoteHistory;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DeliverynoteController
 *
 * @author Deepa
 */
class DeliveryNoteController extends Controller {

    //put your code here
    public function save(Request $request) {
        $resVel = array();
        $resVel ['message'] = 'Delivery Note Added Sucessfully';
        $resVel['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        $deliveryNote = new DeliveryNote;
        $deliveryNote->created_by = $currentuser->id;
        $deliveryNote->updated_by = $currentuser->id;
        $deliveryNote->fill($request->all());
        $deliveryNote->is_active = 1;
        $deliveryNote->save();
        $resVel['id'] = $deliveryNote->id;

        $detail = $request->input('detail', '');

        DeliveryNoteController::detailSave($detail, $currentuser, $deliveryNote);
        if($deliveryNote->type=='outWard' )
        GeneralHelper::StockWastageSave($request->input('detail'), 'delivery_note', $deliveryNote->id,$deliveryNote->date);
        else
        GeneralHelper::StockAdjustmentSave($request->input('detail'), 'delivery_note', $deliveryNote->id,$deliveryNote->date);

        LogHelper::info1('Delivery  Note Save ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Delivery Note', 'save', $screen_code, $deliveryNote->id);
        return $resVel;
    }

    public static function detailSave($detail, $currentuser, $deliveryNote) {
        $overAllPurchase = 0;
        $overAllMrp = 0;
        $overAllSelling = 0;
        foreach ($detail as $det) {
            $deliverDet = new DeliveryNoteDetail;
            $deliverDet->fill($det);
            $deliverDet->is_active = 1;
            $deliverDet->created_by = $currentuser->id;
            $deliverDet->balance_qty = $det['given_qty'];
            $deliverDet->updated_by = $currentuser->id;
            $deliverDet->delivery_note_id = $deliveryNote->id;
            $deliverDet->customer_id = $deliveryNote->customer_id;
            $deliverDet->save();
            
            $overAllPurchase+=$deliverDet->purchase_price * $deliverDet->given_qty;
            $overAllMrp+=$deliverDet->mrp * $deliverDet->given_qty;
            $overAllSelling+=$deliverDet->selling_price * $deliverDet->given_qty;
            
            if($deliveryNote->type == 'inWard'){
            DeliveryNoteController::inWardUpdate($deliveryNote,$deliverDet,$currentuser);
            }
        }
        
        
        $deliveryNote->overall_selling_price = $overAllSelling;
        $deliveryNote->overall_mrp = $overAllMrp;
        $deliveryNote->overall_purchase_price = $overAllPurchase;
        $deliveryNote->save();
       
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Delivery Note Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $deliveryNote = DeliveryNote::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Delivery Note Not Found';
            return $resVal;
        }

        $builder = DB::table('tbl_delivery_note_detail')
                ->where('is_active','=',1)
                ->where('delivery_note_id', $id)
                ->where('received_qty', '!=', '0')->get();

        if (count($builder) > 0) {
            $resVal = array();
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Delivery Note Was Not Updated Because Qty Received For This Delivery';
            return $resVal;
        }

        if($deliveryNote->type=='outWard' )
        GeneralHelper::StockAdjustmentDelete($request->input('detail'), 'delivery_note_outWard_delete_forUpdate', $id);
        else if($deliveryNote->type=='inWard' )
        GeneralHelper::StockAdjustmentDelete($request->input('detail'), 'delivery_note_inWard_delete_forUpdate', $id);

        
         if ($deliveryNote->type == 'inWard') {
            DeliveryNoteController::inWardDecrease($id,$currentuser);
            DB::table('tbl_delivery_note_history')
                    ->where('in_delivery_note_id','=',$id)
                    ->update(["is_active"=>0,'updated_by'=>$currentuser->id]);
        }
        
        
        
        $deliveryNote->updated_by = $currentuser->id;
        $deliveryNote->fill($request->all());
        $deliveryNote->save();


        DB::table('tbl_delivery_note_detail')->where('delivery_note_id', $id)->delete();

        $detail = $request->input('detail', '');
        DeliveryNoteController::detailSave($detail, $currentuser, $deliveryNote);
        
       if($deliveryNote->type=='outWard' )
        GeneralHelper::StockWastageSave($request->input('detail'), 'delivery_note', $deliveryNote->id,$deliveryNote->date);
        else
        GeneralHelper::StockAdjustmentSave($request->input('detail'), 'delivery_note', $deliveryNote->id,$deliveryNote->date);

        LogHelper::info1('Delivery Note Update ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Delivery Note', 'update', $screen_code, $deliveryNote->id);
        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $customer_id = $request->input('customer_id', '');
        $is_active = $request->input('is_active', '');
        $from_date = $request->input('from_date', '');
        $to_date = $request->input('to_date', '');
        $date = $request->input('date', '');
        $type=$request->input('type','');
        $barcode=$request->input('barcode','');
        $sku=$request->input('sku','');
        $decimalFormat = GeneralHelper::decimalFormat();

        $builder = DB::table('tbl_delivery_note as d')
                ->select('d.*', 'c.fname as customer_name', DB::raw("cast(d.overall_qty as DECIMAL(12," . $decimalFormat . ")) as overall_qty"))
                ->leftjoin('tbl_customer as c', 'c.id', '=', 'd.customer_id');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        
        $deliverId=array();
        if(!empty($barcode) || !empty($sku)){
            $item=DB::table('tbl_delivery_note_detail')
                    ->select('delivery_note_id')
                    ->where('is_active','=',1);
            
            if(!empty($barcode)){
                $item->where('barcode','=',$barcode);
            }
            if(!empty($sku)){
                $item->where('product_sku','like','%'.$sku.'%');
            }
                 $collect=$item->get();  
            foreach($collect as $ite){
                array_push($deliverId,$ite->delivery_note_id);
            }
        }

        if (!empty($id)) {
            $builder->where('d.id', '=', $id);
        }
        if ($is_active != '') {
            $builder->where('d.is_active', '=', $is_active);
        }
         if ($type != '') {
            $builder->where('d.type', '=', $type);
        }
        if (!empty($customer_id)) {
            $builder->where('d.customer_id', '=', $customer_id);
        }

        if (!empty($from_date)) {
            $builder->whereDate('d.date', '>=', $from_date);
        }

        if (!empty($to_date)) {
            $builder->whereDate('d.date', '<=', $to_date);
        }

        if (!empty($date)) {
            $builder->whereDate('d.date', '=', $date);
        }
         if(!empty($barcode) || !empty($sku)){
             $builder->whereIn('d.id', $deliverId);
         }
        

        $builder->orderBy('d.id', 'desc');

        $resVal['total'] = $builder->count();
        $resVal['list'] = $builder->skip($start)->take($limit)->get();
        LogHelper::info1('Debit Note List All ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Delivery Note Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $currentuser = Auth::user();
        try {
            $deliveryNote = DeliveryNote::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Delivery note Not Found';
            return $resVal;
        }

        $builder = DB::table('tbl_delivery_note_detail')
                ->where('delivery_note_id', $id)
                ->where('is_active','=',1)
                ->where('received_qty', '!=', '0')->get();

        if (count($builder) > 0) {
            $resVal = array();
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Delivery Note Was Not Deleted Because Qty Received For This Delivery';
            return $resVal;
        }

        $deliveryNote->is_active = 0;
        $deliveryNote->updated_by = $currentuser->id;
        $deliveryNote->save();

       if($deliveryNote->type=='outWard' )
        GeneralHelper::StockAdjustmentDelete($request->input('detail'), 'delivery_note_outWard_delete', $id);
        else if($deliveryNote->type=='inWard' )
        GeneralHelper::StockAdjustmentDelete($request->input('detail'), 'delivery_note_inWard_delete', $id);

          if ($deliveryNote->type == 'inWard') {
            DeliveryNoteController::inWardDecrease($id,$currentuser);
            DB::table('tbl_delivery_note_history')
                    ->where('in_delivery_note_id','=',$id)
                    ->update(["is_active"=>0,'updated_by'=>$currentuser->id]);
        }
        
        DB::table('tbl_delivery_note_detail')->where('delivery_note_id', '=', $id)->update(['is_active' => 0, 'updated_by' => $currentuser->id]);

        LogHelper::info1('Delivery Note Delete' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Delivery Note', 'delete', $screen_code, $deliveryNote->id);
        return $resVal;
    }

    public function detail(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $id = $request->input('id');
        $resVal['success'] = TRUE;
        $decimalFormat = GeneralHelper::decimalFormat();

        $builder = DB::table('tbl_delivery_note as d')
                ->leftjoin(DB::raw("(select sum(received_qty) as received_qty,delivery_note_id from tbl_delivery_note_detail group by delivery_note_id) as dd on dd.delivery_note_id=d.id"),
                        function($join){
                        })
                ->select('d.*', 'c.fname as customer_name', DB::raw("cast(dd.received_qty as DECIMAL(12," . $decimalFormat . ")) as received_qty"), DB::raw("cast(d.overall_qty as DECIMAL(12," . $decimalFormat . ")) as overall_qty")
                        ,DB::raw("(Case when dd.received_qty = d.overall_qty then 1 Else 0 End) as is_received"))
                ->leftjoin('tbl_customer as c', 'c.id', '=', 'd.customer_id')
                ->where('d.id', '=', $id)
                ->where('d.is_active', '=', 1)
                ->get();
        $collection = $builder->first();

        if ($collection != '') {
            $detail = DB::table('tbl_delivery_note_detail')
                    ->select('*', DB::raw("cast(received_qty as DECIMAL(12," . $decimalFormat . ")) as received_qty"), DB::raw("cast(balance_qty as DECIMAL(12," . $decimalFormat . ")) as balance_qty"), DB::raw("cast(given_qty as DECIMAL(12," . $decimalFormat . ")) as given_qty"))
                    ->where('delivery_note_id', '=', $collection->id)
                    ->where('is_active', '=', 1)
                    ->get();

            $collection->detail = $detail;
        }

        $resVal['data'] = $collection;

        return $resVal;
    }

    public function qtyUpdate(Request $request) {

        $resVal = array();
        $detail = $request->input('detail', '');
        $id = $request->input('id');

        foreach ($detail as $det) {
            $data = array();
            DB::table('tbl_delivery_note_detail')
                    ->where('delivery_note_id', '=', $id)
                    ->where('id', '=', $det['detail_id'])
                    ->where('is_active', '=', 1)
                    ->update(['received_qty' => DB::raw('received_qty +' . $det['qty'])
                        , 'balance_qty' => DB::raw('balance_qty -' . $det['qty'])]);

            $item = DB::table('tbl_delivery_note_detail')
                    ->where('delivery_note_id', '=', $id)
                    ->where('id', '=', $det['detail_id'])
                    ->where('is_active', '=', 1)
                    ->first();

            if (($item) != '') {
                $data['product_id'] = $item->product_id;
                $data['product_name'] = $item->product_name;
                $data['uom_id'] = $item->uom_id;
                $data['uom_name'] = $item->uom_name;
                $data['qty'] = $det['qty'];
                $data['product_sku'] = $item->product_sku;
                $data['purchase_invoice_item_id'] = $item->ref_id;
                InventoryHelper::increase($data, $id, 'delivery_qty_update');
            }
        }
        $resVal['success'] = TRUE;
        $resVal['message'] = "Qty Updated SuccessFully";

        return $resVal;
    }

    public function productReport(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        //Get Current and Previous Date

         $decimalFormat = GeneralHelper::decimalFormat();
        $from_date = $request->input('from_date', '');
        $to_date = $request->input('to_date', '');
        $product_id = $request->input('product_id', '');
        $sku = $request->input('sku', '');

        $builder = DB::table('tbl_delivery_note_detail as dt')
                ->leftJoin('tbl_delivery_note as d', 'dt.delivery_note_id', '=', 'd.id')
                ->select(DB::raw("dt.product_name, dt.product_sku, dt.product_id,SUM(dt.purchase_price) as purchase_price, SUM(dt.selling_price ) as selling_price, dt.uom_name  , cast(SUM(dt.given_qty ) as DECIMAL(12," . $decimalFormat . ")) as tot_given_qty ,cast(SUM(dt.balance_qty ) as DECIMAL(12," . $decimalFormat . ")) as tot_balance_qty ,cast(SUM(dt.received_qty ) as DECIMAL(12," . $decimalFormat . ")) as tot_received_qty "))
                ->where('d.is_active', '=', 1)
                ->where('dt.is_active', '=', 1)
                ->where('d.type','=','outWard')
                ->whereColumn('dt.given_qty', '<>', 'dt.received_qty')
                ->groupBy(DB::raw("dt.product_id"));

        $resVal['success'] = TRUE;

        if (!empty($product_id)) {
            $builder = $builder->where("dt.product_id", $product_id);
        }

        if (!empty($from_date)) {
            $builder = $builder->where("d.date", '>=', $from_date);
        }

        if (!empty($to_date)) {
            $builder = $builder->where("d.date", '<=', $to_date);
        }
        $builder = $builder->get();

        foreach ($builder as $bu) {
            $id = $bu->product_id;
            $builder_detail = DB::table('tbl_delivery_note_detail as dt')
                    ->leftJoin('tbl_delivery_note as d', 'dt.delivery_note_id', '=', 'd.id')
                    ->leftJoin('tbl_customer as c', 'd.customer_id', '=', 'c.id')
                    ->select(DB::raw("dt.id, dt.product_id,dt.purchase_price,  dt.selling_price,    cast(dt.given_qty as DECIMAL(12," . $decimalFormat . ")) as given_qty , cast(dt.balance_qty as DECIMAL(12," . $decimalFormat . ")) as balance_qty , cast(dt.received_qty as DECIMAL(12," . $decimalFormat . ")) as received_qty  ,c.fname,c.lname "))
                    ->where('dt.is_active', '=', 1)
                    ->where('d.is_active', '=', 1)
                   ->where('d.type','=','outWard')
                   ->whereColumn('dt.given_qty', '<>', 'dt.received_qty')
                    ->where("dt.product_id", $id);

            if (!empty($product_id)) {
                $builder_detail = $builder_detail->where("dt.product_id", $product_id);
            }

            if (!empty($from_date)) {
                $builder_detail = $builder_detail->where("d.date", '>=', $from_date);
            }

            if (!empty($to_date)) {
                $builder_detail = $builder_detail->where("d.date", '<=', $to_date);
            }
            $bu->data = $builder_detail->get();
        }

        $resVal['list'] = $builder;
        return ($resVal);
    }
    
    public static function inWardUpdate($deliveryNote,$deliverDet,$currentuser){
        
           $data= DB::table('tbl_delivery_note_detail as dd')
                   ->select('dd.id as delivery_detail_id','d.id as delivery_note_id','dd.product_id','dd.balance_qty','dd.given_qty','dd.received_qty')
                    ->leftjoin('tbl_delivery_note as d','dd.delivery_note_id','=','d.id')
                    ->where('d.is_active','=',1)
                    ->where('d.type','=','outWard')
                    ->where('d.customer_id','=',$deliveryNote->customer_id)
                    ->where('dd.product_id','=',$deliverDet->product_id)
                    ->where('dd.ref_id','=',$deliverDet->ref_id)
                    ->where('dd.barcode','=',$deliverDet->barcode)
                    ->where('dd.balance_qty','!=',0.000)
                    ->get();
            $cQty=$deliverDet->given_qty;
            $historyQty=0;
           foreach($data as $dat){
               if($cQty >=$dat->balance_qty){
                   $cQty=$cQty-$dat->balance_qty;
                   $historyQty=$dat->balance_qty;
               }else if($cQty < $dat->balance_qty && $cQty != 0.000){
                   $historyQty=$cQty;
                   $cQty=0;
               }
               DB::table('tbl_delivery_note_detail')
                           ->where('id','=',$dat->delivery_detail_id)
                           ->where('product_id','=',$dat->product_id)
                           ->update(['received_qty' => DB::raw('received_qty +' . $historyQty)
                        , 'balance_qty' => DB::raw('balance_qty -' . $historyQty),"updated_by"=>$currentuser->id]);
               
                DeliveryNoteController::inWardHistorySave($dat,$deliverDet,$historyQty,$currentuser);
               
               if(empty($cQty))
                   break;
           }
    }
    
    public static function inWardHistorySave($deliverDet,$deliveryNote,$qty,$currentuser){
        $deliveryHistory=new DeliveryNoteHistory;
        $deliveryHistory->product_id=$deliverDet->product_id;
        $deliveryHistory->delivery_note_id=$deliverDet->delivery_note_id;
        $deliveryHistory->delivery_note_detail_id=$deliverDet->delivery_detail_id;
        $deliveryHistory->in_delivery_note_id=$deliveryNote->delivery_note_id;
        $deliveryHistory->in_delivery_note_detail_id=$deliveryNote->id;
        $deliveryHistory->qty=$qty;
        $deliveryHistory->is_active=1;
        $deliveryHistory->created_by=$currentuser->id;
        $deliveryHistory->updated_by=$currentuser->id;
        $deliveryHistory->save();
    }
    
    public static function inWardDecrease($deliveryNoteId,$currentuser){
        $collection=DB::table('tbl_delivery_note_history')
                ->where('is_active','=',1)
                ->where('in_delivery_note_id','=',$deliveryNoteId)
                ->get();
        
        foreach($collection as $collect){
            DB::table('tbl_delivery_note_detail')
                    ->where('delivery_note_id','=',$collect->delivery_note_id)
                    ->where('id','=',$collect->delivery_note_detail_id)
                    ->where('product_id','=',$collect->product_id)
                    ->where('is_active','=',1)
                     ->update(['received_qty' => DB::raw('received_qty -' . $collect->qty)
                        , 'balance_qty' => DB::raw('balance_qty +' . $collect->qty),"updated_by"=>$currentuser->id]);
                  
        }
            
    }
    
   
    public function balanceQty(Request $request){
         $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        
        $customerId=$request->input('customer_id',0);
        $decimalFormat = GeneralHelper::decimalFormat();
        
           $type = GeneralHelper::checkConfiqSetting();
            if ($type == 'purchase based'){
                
        $builder=DB::table('tbl_delivery_note_detail as dd')
                ->select('dd.barcode','d.date as outward_date','dd.product_id',DB::raw('dd.product_name'),DB::raw('dd.product_sku'),DB::raw("cast(sum(dd.balance_qty)  as DECIMAL(12," . $decimalFormat . ")) as balance_qty"),DB::raw("cast(sum(dd.given_qty)  as DECIMAL(12," . $decimalFormat . ")) as given_qty")
                        ,DB::raw("cast(sum(dd.received_qty) as DECIMAL(12," . $decimalFormat . ")) as received_qty"),'d.customer_id','c.fname as customer_name')
                ->leftjoin('tbl_delivery_note as d','d.id','=','dd.delivery_note_id')
                ->leftjoin('tbl_customer as c','c.id','=','d.customer_id')
                ->where('d.is_active','=',1)
                ->where('type','=','outWard')
                ->where('dd.is_active','=',1)
                ->where('d.customer_id','=',$customerId)
                ->groupBy('dd.barcode')
                ->get();
        
            }else{
                 $builder=DB::table('tbl_delivery_note_detail as dd')
                ->select('dd.barcode','d.date as outward_date','dd.product_id','dd.product_name','product_sku',DB::raw("cast(sum(dd.balance_qty)  as DECIMAL(12," . $decimalFormat . ")) as balance_qty"),DB::raw("cast(sum(dd.given_qty) as DECIMAL(12," . $decimalFormat . ")) as given_qty")
                        ,DB::raw("cast(sum(dd.received_qty)  as DECIMAL(12," . $decimalFormat . ")) as received_qty"),'d.customer_id','c.fname as customer_name')
                ->leftjoin('tbl_delivery_note as d','d.id','=','dd.delivery_note_id')
                ->leftjoin('tbl_customer as c','c.id','=','d.customer_id')
                ->where('d.is_active','=',1)
                ->where('type','=','outWard')
                ->where('dd.is_active','=',1)
                ->where('d.customer_id','=',$customerId)
                ->groupBy('dd.product_id')
                ->get();
            }
            
            $resVal['list']=$builder;
            
            return $resVal;
    }
    
     public function productReport_csvexport(Request $request) {
        $discountLists = $this->productReport($request);
        ini_set('max_execution_time', 600); //increase max_execution_time to 10 min if data set is very large
        //create a file
        $filename = "deliveryNoteReport" . "_" . time() . ".csv";
        //  $filename = "customer" . "_" . date("Y-m-d_H-i", time()) . ".csv";
        $serverName = $_SERVER['SERVER_NAME'];

        $filePath = "download/$serverName/csv";

        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);

            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {

                if ($folderName == '')
                    $folderName = $folderList[$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index];

                if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        $filePath = getcwd() . '/' . $filePath;
        $arrHead=array('S.NO','Product Name','Product Sku','Selling Price','Total Given Qty','Total Balance Qty','Total Received Qty');
        $output = fopen($filePath . "/" . $filename, 'w');
        
         $type = GeneralHelper::checkConfiqSetting();
            if ($type == 'purchase based')
                array_splice($arrHead,3,0,'Purchase Price');
            
        fputcsv($output, $arrHead);
        $count=0;
        foreach ($discountLists['list'] as $values) {
             $count=$count+1;
            $arrVal=array($count,$values->product_name, $values->product_sku, $values->selling_price,$values->tot_given_qty,$values->tot_balance_qty,$values->tot_received_qty);
            if ($type == 'purchase based')
                array_splice($arrVal,3,0,$values->purchase_price);
           
            fputcsv($output,$arrVal);
        }
        LogHelper::info1('Delivery Note Report CSV Download' . $request->fullurl(), $request->all());
        return response()->download($filePath . "/" . $filename, 'deliveryNoteReport.csv');
    }

}

?>
