<?php

namespace App\Http\Controllers;

use App\Designation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use DB;
use App\Helper\LogHelper;
use App\Helper\NotificationHelper;

Class DesignationController extends Controller {

    public function save(Request $request) {

        $resVal = array();
        $resVal['message'] = 'Designation Added Successfully';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $design = new Designation();
        $design->fill($request->all());

        $design->created_by = $currentuser->id;
        $design->updated_by = $currentuser->id;
        $design->is_active = 1;
        $design->save();

        if ($design->is_default == 1)
            DB::table('tbl_designation')->where('id', '!=', $design->id)->update(['is_default' => 0]);

        $resVal['id'] = $design->id;
        LogHelper::info1('Designation Save ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Designation', 'save', $screen_code, $design->id);
        return $resVal;
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['message'] = 'Designation Updated Successfully';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        try {
            $design = Designation::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Designation Not Found';
            return $resVal;
        }

        $design->fill($request->all());
        $design->updated_by = $currentuser->id;
        $design->is_active = 1;
        $design->save();

        if ($design->is_default == 1)
            DB::table('tbl_designation')->where('id', '!=', $id)->update(['is_default' => 0]);
        
        LogHelper::info1('Designation Update ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Designation', 'update', $screen_code, $design->id);
        return $resVal;
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Designation Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $design = Designation::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Designation Not Found';
            return $resVal;
        }

        $design->is_active = 0;
        $design->updated_at = $currentuser->id;
        $design->save();

        DB::table('tbl_designation')->where('id', '=', $id)->update(['is_default' => 0]);
        LogHelper::info1('Designation Delete ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Designation', 'delete', $screen_code, $design->id);
        return $resVal;
    }

    public function listAll(Request $request) {
        $resVal['success'] = True;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $name = $request->input('name', '');
        $is_active = $request->input('is_active', '');
        $is_default = $request->input('is_default', '');

        $builder = DB::table('tbl_designation')
                ->select('*');

        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }

        if (!empty($name)) {
            $builder->where('name', 'like', '%' . $name . '%');
        }
        if ($is_active != '') {
            $builder->where('is_active', '=', $is_active);
        }
        if ($is_default != '') {
            $builder->where('is_default', '=', $is_default);
        }

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $collection = $builder->get();
        } else {

            $collection = $builder->skip($start)->take($limit)->get();
        }

        $resVal['list'] = $collection;
        LogHelper::info1('Designation List All ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }

}
