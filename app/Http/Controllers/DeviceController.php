<?php

namespace App\Http\Controllers;

use App\Device;
use Illuminate\Http\Request;
use App\Transformer\DeviceTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\LogHelper;
use App\Helper\NotificationHelper;

class DeviceController extends controller {

    public function save(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $deviceCollection = Device::where('device_code', "=", $request->input('device_code'))->get();
        $sc = Device::where('short_code', "=", $request->input('short_code'))->get();
        if (count($deviceCollection) > 0 && count($sc) > 0) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Device Code & Short Code Is Already Exists.';
            return $resVal;
        } else if (count($deviceCollection) > 0) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Device Code Is Already Exists.';
            return $resVal;
        } else if (count($sc) > 0) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Short Code Is Already Exists.';
            return $resVal;
        }
        $resVal = array();
        $resVal['message'] = 'Device Added Successfully';
        $resVal['success'] = TRUE;
        $currentuser = Auth::user();
        $device = new Device;
        $device->created_by = $currentuser->id;
        $device->modified_by = $currentuser->id;
        $device->fill($request->all());
        $device->save();

        $resVal['id'] = $device->id;
        LogHelper::info1('Device Save' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Device', 'save', $screen_code, $device->id);
        return $resVal;
    }

//UPDATE
    public function update(Request $request, $id) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        try {
            $device = Device::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                        'error' => [
                            'message' => 'Device not Found'
                        ]
                            ], 404);
        }
        $resVal['message'] = 'Device Updated Successfully';
        $resVal['success'] = TRUE;
        $currentuser = Auth::user();

        $device->modified_by = $currentuser->id;
        $device->fill($request->all());
        $device->save();
        $resVal['id'] = $device->id;
        LogHelper::info1('Device Update' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Device', 'update', $screen_code, $device->id);
        return $resVal;
    }

    //DELETE
    public function delete(Request $request,$id) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        try {
            $device = Device::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                        'error' => [
                            'message' => 'Device Not Found'
                        ]
                            ], 404);
        }

        $device->delete();

        $resVal['message'] = 'Device Deleted Successfully.';
        $resVal['success'] = TRUE;
        $resVal['id'] = $device->id;
        LogHelper::info1('Device Delete' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Device', 'delete', $screen_code, $device->id);
        return $resVal;
    }

    //LIST ALL
    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $id = $request->input('id');
        $name = $request->input('name');
        $deviceCode = $request->input('deviceCode');
        $shortCode = $request->input('shortCode');
        $resVal{'total'} = Device::count();
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        $builder = Device::query();
        if (!empty($id)) {
            $builder->where('id', '=', $request->input('id'));
        }
        if (!empty($name)) {
            $builder->where('name', 'like', $name . '%');
        }
        if (!empty($deviceCode)) {
            $builder->where('device_code', 'like', $deviceCode . '%');
        }
        if (!empty($shortCode)) {
            $builder->where('short_code', 'like', $shortCode . '%');
        }
        $device = $builder->paginate($limit);
        $this->data = $this->collection($device, new DeviceTransformer());
        $resVal['total'] = $builder->count();
        $resCol = $this->collection($builder->skip($start)->take($limit)->get(), new DeviceTransformer());
        LogHelper::info1('Device List All ' . $request->fullurl(), json_decode(json_encode($resCol), true));
        return array_merge($resVal, $resCol);
    }

    //Device Validation
    public function validation(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $builder = Device::query();
        $deviceCode = $request->input('deviceCode');
        try {

            if (!empty($deviceCode)) {
                $builder->where('device_code', '!=', $deviceCode);
            }
        } catch (ModelNotFoundException $e) {
            $resVal['message'] = 'Unauthorized Device Code';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $device = new Device();
        // Device::where('device_code', '=', $deviceCode). get();
        $resVal['message'] = 'Authorized Device Code';
        $resVal['success'] = TRUE;
        $resVal['deviceCode'] = $device->device_code;
//          $resVal['deviceCode'] = $device->device_code;
        $resVal['shortCode'] = $device->short_code;
        LogHelper::info1('Device Validation ' . $request->fullurl(), $request->all());       
        return $resVal;
    }

}
