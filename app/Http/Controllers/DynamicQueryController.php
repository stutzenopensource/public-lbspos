<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

/**
 * Description of DynamicQueryController
 *
 * @author LENOVO
 */

use Illuminate\Http\Request;
use DB;
class DynamicQueryController {
                     
     
public function dynamicQuery(Request $request) {
        $resVal['success'] = TRUE;
        $filters = $request->input('filters');
        $limit = $request->input('limit');
        $columns = $request->input('columns');
        $order = $request->input('order');
        $orderBy = $request->input('orderBy');
        $builder = DB::table($request->input('tableName'));
        
        foreach ($filters as $filter) { // Filter
            if($filter['val'] >0 || $filter['val'] != '')
                $builder->where($filter['col'], $filter['op'], $filter['val']);
        }
      //  print_r($builder->toSql());
        $resVal['total'] = $builder->count();
        
        if (isset($limit)) {
            $builder->limit($limit);
        }
        if (isset($order)) {
            if (!isset($orderBy)) {
                $orderBy = 'asc';
            }
            $builder->orderBy(DB::raw($order), $orderBy);
        }

        if (isset($columns)) {
            $records = $builder->get($columns);
        } else {
            $records = $builder->get();
        }
         
         $resVal['list'] = $records;
        
        return $resVal;
    }

}
