<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;
use App\Attachment;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use DB;
use App\Helper\LogHelper;
use App\Helper\NotificationHelper;

Class EmployeeController extends Controller {

    public function save(Request $request) {

        $resVal = array();
        $resVal['message'] = 'Employee Added Successfully';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        
        $empCode = DB::table('tbl_employee')
                    ->select(DB::raw("MAX(employee_code) AS emp_code"))
                   // ->where('is_active', '=', 1)
                    ->first();
        
        if (isset($empCode)) {
                $emp_code = $empCode->emp_code + 1; 
            } else {
                $emp_code = 1;
            }
         
                
        if ($request->input('email') != NULL) {

            $employeeEmail = Employee::where('email', "=", $request->input('email'))
                            ->where('is_active', '=', 1)->get();

            if (count($employeeEmail) > 0) {
                $resVal['success'] = FALSE;
                $resVal['message'] = 'Employee Email Is Already Exists';
                return $resVal;
            }
        }
        $employee = new Employee;
        $employee->fill($request->all());

        $mobile = $request->input('mobile', '');
        $trimmed_array = '';
        if (!empty($mobile)) {
            $num = explode(',', $mobile);
            foreach ($num as $no) {
                $no = trim($no, " ");
                $data = preg_match('/^[0-9]{10}+$/', $no);
                $ph = DB::table('tbl_employee')->where('is_active', '=', 1)
                        ->Whereraw("FIND_IN_SET('" . $no . "',mobile)");
                if ($ph->count() > 0) {
                    $resVal['message'] = $no . ' Mobile Number Is Already Exists';
                    $resVal['success'] = False;
                    return $resVal;
                } else if ($data != 1) {
                    $resVal['message'] = $no . ' Given Mobile Number Only 10 Digit';
                    $resVal['success'] = False;
                    return $resVal;
                }
            }
            $num = array_unique($num);
            $trimmed_array = array_map('trim', $num);
            $trimmed_array = implode(',', $trimmed_array);
        }

        $employee->mobile = preg_replace('/\s+/', '', $request->input('mobile'));
        $employee->created_by = $currentuser->id;
        $employee->updated_by = $currentuser->id;
        $employee->employee_code=$emp_code;
        $employee->is_active = 1;
        $employee->save();
        
        
        $attachments = $request->input('attachment', '');
        if (!empty($attachments)) {
            foreach ($attachments as $attachment) {
                $attach = new Attachment;
                $attach->fill($attachment);
                $attach->is_active = 1;
                $attach->type = "employee";
                $attach->created_by = $currentuser->id;
                $attach->updated_by = $currentuser->id;
                $attach->ref_id = $employee->id;
                $attach->save();
            }
        }

        $resVal['id'] = $employee->id;
        LogHelper::info1('Employee Save ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Employee ', 'save', $screen_code, $employee->id);
        return $resVal;
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['message'] = 'Employee Updated Successfully';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        try {
            $employee = Employee::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Employee Not Found';
            return $resVal;
        }
        if ($request->input('employee_code') != NULL) {

            $employeeCollection = Employee::where('employee_code', "=", $request->input('employee_code'))
                            ->where('is_active', '=', 1)->where('id', '!=', $id)->get();

            if (count($employeeCollection) > 0) {
                $resVal['success'] = FALSE;
                $resVal['message'] = 'Employee Code Is Already Exists';
                return $resVal;
            }
        }
        if ($request->input('email') != NULL) {

            $employeeEmail = Employee::where('email', "=", $request->input('email'))
                            ->where('is_active', '=', 1)->where('id', '!=', $id)->get();

            if (count($employeeEmail) > 0) {
                $resVal['success'] = FALSE;
                $resVal['message'] = 'Employee email Is Already Exists';
                return $resVal;
            }
        }

        $employee->fill($request->all());

        $mobile = $request->input('mobile', '');
        $trimmed_array = '';
        if (!empty($mobile)) {
            $num = explode(',', $mobile);
            foreach ($num as $no) {
                $no = trim($no, " ");
                $data = preg_match('/^[0-9]{10}+$/', $no);
                $ph = DB::table('tbl_employee')->where('is_active', '=', 1)
                        ->Whereraw("FIND_IN_SET('" . $no . "',mobile)")
                        ->where('id', '!=', $id);
                if ($ph->count() > 0) {
                    $resVal['message'] = $no . ' Mobile Number Is Already Exists';
                    $resVal['success'] = False;
                    return $resVal;
                } else if ($data != 1) {
                    $resVal['message'] = $no . ' Given Mobile Number Only 10 Digit';
                    $resVal['success'] = False;
                    return $resVal;
                }
            }
            $num = array_unique($num);
            $trimmed_array = array_map('trim', $num);
            $trimmed_array = implode(',', $trimmed_array);
        }

        $employee->mobile = preg_replace('/\s+/', '', $request->input('mobile'));
        $employee->created_by = $currentuser->id;
        $employee->updated_by = $currentuser->id;
        $employee->is_active = 1;
        $employee->save();
        $atach = Attachment::where('ref_id', '=', $id)->where('type', '=', 'employee')->delete();
        $attachments = $request->input('attachment', '');
        if (!empty($attachments)) {
            foreach ($attachments as $attachment) {
                $attach = new Attachment;
                $attach->fill($attachment);
                $attach->type = "employee";
                $attach->is_active = 1;
                $attach->created_by = $currentuser->id;
                $attach->updated_by = $currentuser->id;
                $attach->ref_id = $employee->id;
                $attach->save();
            }
        }

        LogHelper::info1('Employee Update ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Employee', 'update', $screen_code, $employee->id);


        return $resVal;
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Employee Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $employee = Employee::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Employee Not Found';
            return $resVal;
        }

        $employee->is_active = 0;
        $employee->updated_at = $currentuser->id;
        $employee->save();
        Attachment::where('id', '=', $id)->where('type', '=', "employee")->delete();
        LogHelper::info1('Employee Delete ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Employee', 'delete', $screen_code, $employee->id);


        return $resVal;
    }

    public function listAll(Request $request) {
        $resVal['success'] = True;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $employee_code = $request->input('employee_code');
        $name = $request->input('name', '');
        $is_active = $request->input('is_active', '');
        $mobile = $request->input('mobile');
        $email = $request->input('email');
        $designation_id = $request->input('designation_id');
        $city_id = $request->input('city_id');
        $state_id = $request->input('state_id');
        $country_id = $request->input('country_id');
        $search=$request->input('search');


        $builder = DB::table('tbl_employee as e')
                ->leftjoin('tbl_country as c', 'c.id', '=', 'e.country_id')
                ->leftjoin('tbl_state as s', 's.id', '=', 'e.state_id')
                ->leftjoin('tbl_city as ct', 'ct.id', '=', 'e.city_id')
                ->leftjoin('tbl_designation as d', 'd.id', '=', 'e.designation_id')
                ->select('e.*', 'c.name as county_name', 's.name as state_name', 'ct.name as city_name', 'd.name as designation_name');

        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        if (!empty($employee_code)) {
            $builder->where('e.employee_code', '=', $employee_code);
        }
        if (!empty($id)) {
            $builder->where('e.id', '=', $id);
        }

        if (!empty($designation_id)) {
            $builder->where('e.designation_id', '=', $designation_id);
        }
        if (!empty($name)) {
            $builder->where('e.name', 'like', '%' . $name . '%');
        }
        if ($is_active != '') {
            $builder->where('e.is_active', '=', $is_active);
        }
        if (!empty($mobile)) {
            $builder->where('e.mobile', 'like', '%' . $mobile . '%');
        }
        if (!empty($email)) {
            $builder->where('e.email', 'like', '%' . $email . '%');
        }
        if (!empty($city_id)) {
            $builder->where('e.city_id', '=', $city_id);
        }

        if (!empty($state_id)) {
            $builder->where('e.state_id', '=', $state_id);
        }

        if (!empty($country_id)) {
            $builder->where('e.country_id', '=', $country_id);
        }
        if (!empty($search)) {
            $builder->whereRaw(DB::raw("(e.name  like '%" . $search . "%' or employee_code like '%" . $search . "%')"));
        }




        $builder->orderBy('e.id', 'desc');
        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $collection = $builder->get();
        } else {

            $collection = $builder->skip($start)->take($limit)->get();
        }

        foreach ($collection as $emp) {
            $attach = DB::table('tbl_attachment')
                    ->where('ref_id', '=', $emp->id)
                    ->where('type', '=', 'employee')
                    ->get();
            $emp->attachment = $attach;
        }

        $resVal['list'] = $collection;
        LogHelper::info1('Employee List All ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }

    public function detail(Request $request) {

        $resVal['success'] = True;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $id = $request->input('id', '');

        $builder = DB::table('tbl_employee as e')
                ->leftjoin('tbl_country as c', 'c.id', '=', 'e.country_id')
                ->leftjoin('tbl_state as s', 's.id', '=', 'e.state_id')
                ->leftjoin('tbl_city as ct', 'ct.id', '=', 'e.city_id')
                ->leftjoin('tbl_designation as d', 'd.id', '=', 'e.designation_id')
                ->select('e.*', 'c.name as county_name', 's.name as state_name', 'ct.name as city_name', 'd.name as designation_name');

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }

        $builder->orderBy('id', 'desc');
        $collection = $builder->get();
        if (count($collection) == 1) {
            foreach ($collection as $emp) {
                $attach = DB::table('tbl_attachment')
                        ->where('ref_id', '=', $id)
                        ->where('type', '=', 'employee')
                        ->get();
                $emp->attachment = $attach;
            }
            $resVal['list'] = $collection;
        }
        else 
        {
            $resVal['list'] = null;
        }
        LogHelper::info1('Employee Detail ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return $resVal;
    }

    public function employeeSalesReport(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $from_date = $request->input('from_date', '');
        $to_date = $request->input('to_date', '');
        $emp_id = $request->input('emp_id','');

        $builder = DB::table('tbl_employee as e')
                ->select('e.id', 'e.name as emp_name', 'e.employee_code as emp_code', DB::raw("Coalesce(cast(round(SUM(it.total_price)) as decimal(12,2)),0) as total_amount"), DB::raw("Coalesce(sum(it.qty),0) as total_qty"),DB::raw("Coalesce(cast(round(SUM(it.total_price)/Coalesce(sum(it.qty),0)) as decimal(12,2)),0) as bill_value"))
                ->leftjoin('tbl_invoice_item as it', 'e.id', '=', 'it.emp_id')
                ->leftjoin('tbl_invoice as i', 'i.id', '=', 'it.invoice_id')
                ->where('i.is_active','=',1)
                ->groupBy('it.emp_id');

        if (!empty($from_date)) {
            $builder->whereDate('i.date', '>=', $from_date);
        }
        if (!empty($to_date)) {
            $builder->whereDate('i.date', '<=', $to_date);
        }
        if (!empty($emp_id)) {
            $builder->where('it.emp_id', '=', $emp_id);
        }

        $collection = $builder->get();

        foreach ($collection as $collect) {
            $builderDet = DB::table('tbl_invoice as i')
                    ->select('i.invoice_code', DB::raw("Coalesce(cast(round(SUM(it.total_price)) as decimal(12,2)),0) as total_amount"), DB::raw("Coalesce(sum(it.qty),0) as total_qty"), 'it.product_name',DB::raw("Coalesce(cast(round(SUM(it.total_price)/Coalesce(sum(it.qty),0)) as decimal(12,2)),0) as bill_value"))
                    ->leftjoin('tbl_invoice_item as it', 'i.id', '=', 'it.invoice_id')
                    ->where('it.emp_id', '=', $collect->id)
                    ->where('i.is_active','=',1)
                    ->groupBy('it.product_id');

            if (!empty($from_date)) {
                $builderDet->whereDate('i.date', '>=', $from_date);
            }
            if (!empty($to_date)) {
                $builderDet->whereDate('i.date', '<=', $to_date);
            }
            
            $collect->detail=$builderDet->get();
        }
        
        $resVal['list']=$collection;
        LogHelper::info1('Employee Sales Report' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return $resVal;
    }
    
    public function EmployeeSalesReport_csvexport(Request $request) {
        $EmpWiseLists = $this->employeeSalesReport($request);
        ini_set('max_execution_time', 600); //increase max_execution_time to 10 min if data set is very large
        //create a file
        $filename = "EmployeeSalesReport" . "_" . time() . ".csv";
        //  $filename = "customer" . "_" . date("Y-m-d_H-i", time()) . ".csv";
        $serverName = $_SERVER['SERVER_NAME'];

        $filePath = "download/$serverName/csv";

        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);

            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {

                if ($folderName == '')
                    $folderName = $folderList[$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index];

                if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        $filePath = getcwd() . '/' . $filePath;
        $output = fopen($filePath . "/" . $filename, 'w');
         $sr=1;
        fputcsv($output, array('#','Employee Code','Employee Name','Sales Qty','Sales Value','Average Bill Value'));
        foreach ($EmpWiseLists['list'] as $values) {
            fputcsv($output, array($sr,$values->emp_code,$values->emp_name,$values->total_qty, $values->total_amount,$values->bill_value));
            $sr++;
        }
        LogHelper::info1('EmployeeSalesReport CSV Download ' . $request->fullurl(), $request->all());

        return response()->download($filePath . "/" . $filename, 'EmployeeSalesReport.csv');
    }


}
