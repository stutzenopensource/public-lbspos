<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\HsnTaxMapping;
use App\HsnCode;
use Illuminate\Http\Request;
use App\Transformer\UomTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\LogHelper;
use App\Helper\NotificationHelper;
use App\Helper\GeneralHelper;
use App\Tax;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of HsnCodeController
 *
 * @author Deepa
 */
class HsnCodeController extends Controller {

    //put your code here

    public function save(Request $request) {
        $resVal = array();
        if ($request->input('is_approved')==0)
        $resVal['message'] = 'Hsn Code Added Successfully';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        
        $hsnCode=DB::table('tbl_hsn_code')
                ->where('hsn_code','=',$request->input('hsn_code',''))
                ->where('is_active','=',1)
                ->get();
        
        $is_sso=$request->input('is_sso');
        if(count($hsnCode)> 0 && $request->input('is_approved')==1){
            
           if($is_sso==1)
            HsnCodeController::changeGroupTaxId($request, $currentuser,$hsnCode);
            return $resVal;
        }else if(count($hsnCode)> 0 ){
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Hsn Code Is Already Exist';
             return $resVal;
        }
        

        $hsn = new HsnCode;

        $hsn->created_by = $currentuser->id;
        $hsn->updated_by = $currentuser->id;
        $hsn->is_active = $request->input('is_active', 1);

        $hsn->fill($request->all());
        $hsn->save();
        
        $taxDetail = $request->input('taxDetail', '');
        if (!empty($taxDetail)) {
            HsnCodeController::taxDetailSave($taxDetail,$currentuser);
        }

        $mapping = $request->input('hsnTaxMapping');
        HsnCodeController::hsnTaxMappingSave($mapping, $currentuser, $hsn);

//        if ($hsn->is_approved == 0) {
//            HsnCodeController::isApprovedHsnCode($request->json()->all());
//        }

        $resVal['id'] = $hsn->id;
        LogHelper::info1('Hsn Code Save ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Hsn Code', 'save', $screen_code, $hsn->id);
        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $hsn_code = $request->input('hsn_code', '');
        $is_active = $request->input('is_active', '');

        $builder = DB::table('tbl_hsn_code')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if ($is_active != '') {
            $builder->where('is_active', '=', $is_active);
        }

        if (!empty($hsn_code)) {
            $builder->where('hsn_code', 'like', '%' . $hsn_code . '%');
        }
        $builder->orderBy('id', 'desc');
        if ($start == 0 && $limit == 0) {
            $taxCollection = $builder->get();
        } else {
            $taxCollection = $builder->skip($start)->take($limit)->get();
        }
        foreach ($taxCollection as $value) {
            $id = $value->id;
            $tax = DB::table('tbl_hsn_tax_mapping as ht')
                    ->leftJoin('tbl_tax as t', 't.id', '=', 'ht.group_tax_id')
                    ->select('ht.*', 't.tax_name', 't.tax_percentage')
                    ->where('is_active','=',1)
                    ->where('hsn_id', '=', $id)
                    ->get();
            $value->hsnTaxMapping = $tax;
        }
        $resVal['total'] = $builder->count();
        $resVal['list'] = $taxCollection;
        LogHelper::info1('Hsn Code List All ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Hsn Code Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $hsn = HsnCode::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Hsn Code Not Found';
            return $resVal;
        }
        $hsn->created_by = $currentuser->id;
        $hsn->fill($request->all());
        $hsn->save();
        DB::table('tbl_hsn_tax_mapping')->where('hsn_id', $id)->delete();
        
        $taxDetail = $request->input('taxDetail', '');
        if (!empty($taxDetail)) {
            HsnCodeController::taxDetailSave($taxDetail,$currentuser);
        }
        $mapping = $request->input('hsnTaxMapping');
        
         HsnCodeController::hsnTaxMappingSave($mapping, $currentuser, $hsn);
          
        
//        if ($hsn->is_approved == 0) {
//            HsnCodeController::isApprovedHsnCode($request->json()->all());
//        }
        LogHelper::info1('Hsn Code Update ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Hsn Code', 'update', $screen_code, $hsn->id);
        return $resVal;
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Hsn Code Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $hsn = HsnCode::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Hsn Code Not Found';
            return $resVal;
        }
        $hsn->is_active = 0;
        $hsn->updated_by = $currentuser->id;
        $hsn->update();
        $builder = DB::table('tbl_hsn_tax_mapping')->where('hsn_id', $id)->delete();
        LogHelper::info1('Hsn Code Delete ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Hsn Code', 'delete', $screen_code, $hsn->id);
        return $resVal;
    }

    public function detail(Request $request) {

        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $id = $request->input('id', '');
        $builder = DB::table('tbl_hsn_code')
                ->select('*')
                ->where('is_active', '=', 1)
                ->where('id', '=', $id);
        $taxCollection = $builder->orderBy('id', 'desc')->get();
        foreach ($taxCollection as $value) {
            $id = $value->hsn_code;
            $tax = DB::table('tbl_hsn_tax_mapping as ht')
                    ->leftJoin('tbl_tax as t', 't.id', '=', 'ht.group_tax_id')
                    ->select('ht.*', 't.tax_name', 't.tax_percentage')
                    ->where('is_active','=',1)
                    ->where('hsn_code', '=', $id)
                    ->get();
            $value->hsnTaxMapping = $tax;
        }
        $resVal['total'] = $builder->count();
        $resVal['data'] = $taxCollection;
        LogHelper::info1('Hsn Code detail ' . $request->fullurl(), json_decode(json_encode($resVal['data']), true));
        return ($resVal);
    }

    public function hsnTaxDetailSample(Request $request) {
        $resVal = array();
        $resVal['message'] = "success";
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $hsn_code = $request->input('hsn_code', '');
        $search = $request->input('search', '');
        $type = $request->input('type', '');
        $resVal['success'] = TRUE;

        $builder = DB::table('tbl_hsn_code')
                ->select('*')
                ->where('is_active', '=', 1);

        if (!empty($hsn_code)) {
            $builder->where('hsn_code', 'like', '%' . $hsn_code . '%');
        }
        if (!empty($search)) {
            $builder->where('description', 'like', '%' . $search . '%');
        }
        if (!empty($type)) {
            $builder->where('type', '=', $type);
        }

        $taxCollection = $builder->get();
        foreach ($taxCollection as $value) {
            $hsnCode = $value->hsn_code;
            $tax = DB::table('tbl_hsn_tax_mapping as ht')
                    ->leftJoin('tbl_tax as t', 't.id', '=', 'ht.group_tax_id')
                    ->select('ht.*', 't.tax_name', 't.tax_percentage', 't.group_tax_ids', 't.intra_group_tax_ids')
                    ->where('hsn_code', '=', $hsnCode)
                    ->where('hi.is_active', '=', 1)
                    ->get();

            foreach ($tax as $taxData) {

                $interTaxName = DB::select(DB::raw("SELECT  GROUP_CONCAT(c.tax_name) as inter_tax_name FROM tbl_tax i, tbl_tax c 
                                        WHERE FIND_IN_SET(c.id, i.group_tax_ids) and i.is_active=1 and i.id='" . $taxData->group_tax_id . "' 
                                        GROUP BY i.group_tax_ids"));

                $intraTaxName = DB::select(DB::raw("SELECT  GROUP_CONCAT(c.tax_name) as inter_tax_name FROM tbl_tax i, tbl_tax c 
                                        WHERE FIND_IN_SET(c.id, i.intra_group_tax_ids) and i.is_active=1 and i.id='" . $taxData->group_tax_id . "' 
                                        GROUP BY i.intra_group_tax_ids"));
                $taxData->inter_tax_name = $interTaxName;
                $taxData->intra_tax_name = $intraTaxName;
            }
            $value->hsnTaxMapping = $tax;
        }
        $resVal['list'] = $taxCollection;
        return $resVal;
    }

    public function gettingTaxId(Request $request) {
        $resval = array();
        $hsnCode = $request->input('hsn_code', '');
        $price = $request->input('purchase_price', '');
        $mrp_price = $request->input('mrp_price', '');

        if (!empty($hsnCode)) {
            $resval['hsn_code'] = $hsnCode;
            $resval['selling_tax_id'] = 0;
            $resval['purchase_tax_id'] = 0;
            if (!empty($price)) {

                $group_tax_id = GeneralHelper::hsnCodeTaxId($hsnCode, $price);
                $resval['purchase_tax_id'] = $group_tax_id;
            }
            if (!empty($mrp_price)) {

                $group_tax_id = GeneralHelper::hsnCodeTaxId($hsnCode, $mrp_price);
                $resval['selling_tax_id'] = $group_tax_id;
            }
        }
        return $resval;
    }

    public function ssoHsnTaxDetail(Request $request) {

        $hsn_code = $request->input('hsn_code', '');
        $search = $request->input('search', '');
        $type = $request->input('type', '');
        $domain = $_SERVER['SERVER_NAME'];

        $url = "http://lbssso/public/hsnCode/hsnTaxDetail?hsn_code=$hsn_code&search=$search&type=$type&domain_name=$domain";


        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        $result = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        return $result;
    }
    
     public function hsnTaxDetail(Request $request) {
        $resVal = array();
        $resVal['message'] = "success";

        $hsn_code = $request->input('hsn_code', '');
        $search = $request->input('search', '');
        $type = $request->input('type', '');
        $resVal['success'] = TRUE;

        
       $builder = DB::table('tbl_hsn_code')
                ->select('*')
                ->where('is_active', '=', 1);

        if (!empty($hsn_code)) {
            $builder->where('hsn_code', 'like', '%' . $hsn_code . '%');
        }
        if (!empty($search)) {
            $builder->where('description', 'like', '%' . $search . '%');
        }
        if (!empty($type)) {
            $builder->where('type', '=', $type);
        }

        $taxCollection = $builder->get();
        foreach ($taxCollection as $value) {
            $hsnCode = $value->hsn_code;
            $tax = DB::table('tbl_hsn_tax_mapping as ht')
                    ->leftJoin('tbl_tax as t', function($join){
                        $join->on('t.id', '=', 'ht.group_tax_id');
                        $join->where('t.is_active','=',1);
                    })
                    ->select('ht.*', 't.tax_name','t.tax_no','t.is_group','t.tax_type','t.hsn_flag','is_display', 't.tax_percentage', 't.group_tax_ids', 't.intra_group_tax_ids')
                    ->where('hsn_code', '=', $hsnCode)
//                    ->where('t.is_active','=',1)
                    ->where('ht.is_active', '=', 1)
                    ->get();

            foreach ($tax as $taxData) {

                $interTaxName = DB::select(DB::raw("SELECT  GROUP_CONCAT(c.tax_name) as inter_tax_name FROM tbl_tax i, tbl_tax c 
                                        WHERE FIND_IN_SET(c.id, i.group_tax_ids) and i.is_active=1 and i.id='" . $taxData->group_tax_id . "' 
                                        GROUP BY i.group_tax_ids"));

                $intraTaxName = DB::select(DB::raw("SELECT  GROUP_CONCAT(c.tax_name) as inter_tax_name FROM tbl_tax i, tbl_tax c 
                                        WHERE FIND_IN_SET(c.id, i.intra_group_tax_ids) and i.is_active=1 and i.id='" . $taxData->group_tax_id . "' 
                                        GROUP BY i.intra_group_tax_ids"));
                
                
                $inter_tax_name='';
                if(($interTaxName != '') && isset($interTaxName[0]))
                    $inter_tax_name = $interTaxName[0]->inter_tax_name;
                
                $intra_tax_name='';
                if(($intraTaxName != '')&&isset($intraTaxName[0]))
                    $intra_tax_name = $intraTaxName[0]->inter_tax_name;
                $taxData->inter_tax_name = $inter_tax_name;
                $taxData->intra_tax_name = $intra_tax_name;
                
                $associate_tax_id = explode(',', $taxData->group_tax_ids);
                $inter_tax_detail = DB::table('tbl_tax')
                        ->select('*')
                        ->whereIn('id', $associate_tax_id)
                        ->where('is_active', '=', 1)
                        ->get();

                $taxData->inter_tax_detail = $inter_tax_detail;
                $associate_tax_id = explode(',', $taxData->intra_group_tax_ids);

                $inter_tax_detail = DB::table('tbl_tax')
                        ->select('*')
                        ->whereIn('id', $associate_tax_id)
                        ->where('is_active', '=', 1)
                        ->get();

                $taxData->intra_tax_detail = $inter_tax_detail;
            }
            $value->hsnTaxMapping = $tax;
        }
        $resVal['list'] = $taxCollection;
        return $resVal;
    }

    public static function isApprovedHsnCode($data) {
        $url = "http://lbssso/public/hsnCode/save";

        $domain = $_SERVER['SERVER_NAME'];
        $data1 = array_add($data, 'domain_name', $domain);
        $jsonDataEncoded = json_encode($data1);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

        curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
    }

    public static function taxDetailSave($taxDetail, $currentuser) {
        foreach ($taxDetail as $tax) {
            if (!empty($tax['sso_tax_id'])) {
                $tax1 = DB::table('tbl_tax')
                        ->where('sso_tax_id','=',$tax['sso_tax_id'])
                        ->where('is_active', '=', 1)->get();
           
        $tax2=0;
        if(!empty($tax['tax_name']) && !empty($tax['tax_no']) && !empty($tax['tax_percentage'])){            
            $tax2=DB::table('tbl_tax')
                    ->whereRaw(DB::raw("(tax_name='".$tax['tax_name']."' and tax_no='".$tax['tax_no']."'and tax_percentage='".$tax['tax_percentage']."')"))
                    ->where('is_active','=',1)->get();
        }
        
        
         if($tax['is_group']==1){
             
             $groupTaxId=HsnCodeController::getGroupTaxId($tax['group_tax_ids']);
             $intraGroupTaxId=HsnCodeController::getGroupTaxId($tax['intra_group_tax_ids']);
             
             $tax2=DB::table('tbl_tax')
                    ->whereRaw(DB::raw("(tax_name='".$tax['tax_name']."' and tax_no='".$tax['tax_no']."'and tax_percentage='".$tax['tax_percentage']."')"))
                    ->where('is_active','=',1)
                     ->where('intra_group_tax_ids',$intraGroupTaxId)
                     ->where('group_tax_ids',$groupTaxId)
                     ->get();
         }
                
                if ((count($tax1) == 0 || count($tax2)==0)) {
                    $taxSa = new Tax;
                    $taxSa->created_by = $currentuser->id;
                    $taxSa->updated_by = $currentuser->id;
                    $taxSa->is_active = 1;
                    $taxSa->fill($tax);
                    
                    if($tax['is_group']==1){
                        $groupTaxId=HsnCodeController::getGroupTaxId($tax['group_tax_ids']);
                        $intraGroupTaxId=HsnCodeController::getGroupTaxId($tax['intra_group_tax_ids']);
                        $taxSa->group_tax_ids=$groupTaxId;
                        $taxSa->intra_group_tax_ids=$intraGroupTaxId;
                    }
                    $taxSa->save();
                }
            }
        }
    }
    
    public static function getTaxIdByTaxName($hsnTax){
        $taxId=0;
        $groupTaxId=HsnCodeController::getGroupTaxId($hsnTax['group_tax_ids']);
        $intraGroupTaxId=HsnCodeController::getGroupTaxId($hsnTax['intra_group_tax_ids']);
         $taxIds = DB::table('tbl_tax')
                 ->where('sso_tax_id','=',$hsnTax['group_tax_id'])
                 ->where('group_tax_ids','=',$groupTaxId)
                 ->where('intra_group_tax_ids','=',$intraGroupTaxId)
                 ->where('is_active', '=', 1)
                 ->first();
         
         if($taxIds != ''){
             $taxId=$taxIds->id;
         }
         return $taxId;
    }
    
    
    public static function getGroupTaxId($ssoTaxId){
        $tIds=explode(',',$ssoTaxId);
        $taxId=0;
         $taxIds = DB::table('tbl_tax')
                 ->select(DB::raw("(Group_ConCat(id)) as taxId"))
                 ->whereIn('sso_tax_id',$tIds)
                 ->where('is_active', '=', 1)
                 ->first();
         
         if($taxIds != ''){
             $taxId=$taxIds->taxId;
         }
         return $taxId;
    }
    
    public static function changeGroupTaxId($request,$currentuser,$hsnCode){
       $hsnCodes= $hsnCode->first();
       $taxDetail = $request->input('taxDetail', '');
       $hsnTaxMapping = $request->input('hsnTaxMapping');
        if (!empty($taxDetail)) {
            HsnCodeController::taxDetailSave($taxDetail,$currentuser);
        }
        
        foreach($hsnTaxMapping as $map){
            $hsnTaxMap=DB::table('tbl_hsn_tax_mapping')
                    ->where('hsn_code','=',$request->input('hsn_code'))
                     ->where('from_price','=',$map['from_price'])
                     ->where('to_price','=',$map['to_price'])
                     ->where('is_active','=',1)
                    ->get();
            if(count($hsnTaxMap)==0){
                 $mapping = $request->input('hsnTaxMapping');
                 HsnCodeController::hsnTaxMappingSave($mapping, $currentuser, $hsnCodes);
            }
            $groupTaxId=HsnCodeController::getGroupTaxId($map['group_tax_ids']);
            $intraGroupTaxId=HsnCodeController::getGroupTaxId($map['intra_group_tax_ids']);
            
            $groupTaxDetail=DB::table('tbl_tax')
                    ->where('sso_tax_id','=',$map['group_tax_id'])
                    ->where('group_tax_ids','=',$groupTaxId)
                    ->where('is_active','=',1)
                    ->where('intra_group_tax_ids','=',$intraGroupTaxId)
                    ->first();
            $groupTxId=0;
            
            if($groupTaxDetail != ''){
                $groupTxId=$groupTaxDetail->id;
            }
            if($groupTxId != 0){
               DB::table('tbl_hsn_tax_mapping')
                       ->where('hsn_code','=',$request->input('hsn_code'))
                       ->where('from_price','=',$map['from_price'])
                        ->where('to_price','=',$map['to_price'])
                       ->where('is_active','=',1)
                       ->update(['group_tax_id'=>$groupTxId]);
            }
        }
    }
    
    public static function hsnTaxMappingSave($mapping,$currentuser,$hsn){
        foreach ($mapping as $hsnTaxMapping) {
            $TaxMapping = new HsnTaxMapping;
            $TaxMapping->fill($hsnTaxMapping);
            $TaxMapping->hsn_id = $hsn->id;
            $TaxMapping->hsn_code = $hsn->hsn_code;
            $TaxMapping->created_by = $currentuser->id;
            $TaxMapping->updated_by = $currentuser->id;
            $TaxMapping->is_active = 1;
            
            
            if ($hsn->is_approved == 1) {
            $taxId=HsnCodeController::getTaxIdByTaxName($hsnTaxMapping);
            $TaxMapping->group_tax_id=$taxId;
             }
             
             $TaxMapping->save();
            }
    }
        
}

?>
