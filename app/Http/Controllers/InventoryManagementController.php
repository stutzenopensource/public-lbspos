<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Transformer\InvoiceTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\GeneralHelper;
use App\StockAdjustment;
use App\StockAdjustmentDetail;
use App\Attachment;
use App\InventoryAdjustment;
use App\Inventory;
use App\Helper\LogHelper;
use App\Helper\NotificationHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of invoicesController
 *
 * @author Deepa
 */
class InventoryManagementController extends Controller {

    //put your code here
    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Stock Adjustment Added Successfully';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentUser = Auth::user();

        $stock = new StockAdjustment;
        $stock->fill($request->all());
        $stock->is_active = 1;
        $stock->created_by = $currentUser->id;
        $stock->updated_by = $currentUser->id;
        $stock->save();
        
        if($stock->type=='stock_wastage') {
            $resVal['message'] = 'Stock Wastage Added Successfully';
        }

        $attachment = $request->input('attachment', '');
        InventoryManagementController::attachmentSave($attachment, $currentUser, $stock);

        InventoryManagementController::detailSave($request, $currentUser, $stock);
        $resVal['id'] = $stock->id;
        LogHelper::info1('StockAdjustment Save' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'StockAdjustment', 'save', $screen_code, $stock->id);
        return $resVal;
    }

    public static function detailSave($request, $currentUser, $stock) {
        $total_qty = 0;
        $total_amount = 0;
        $salesPrice = 0;
        $mrpPrice = 0;
        $purchasePrice=0;
        $salPrice=0;
        $details = $request->input('product', '');
        foreach ($details as $req) {
            $detail = new StockAdjustmentDetail;
            $detail->fill($req);
            $detail->created_by = $currentUser->id;
            $detail->updated_by = $currentUser->id;
            $detail->is_active = $req['is_active'];
            $detail->stock_adjustment_id = $stock->id;
            $detail->save();

            if ($req['is_active'] == 1) {
            $type = GeneralHelper::checkConfiqSetting();
            $salesPrice = $req['sales_price'] * $req['qty'];
            if ($type == 'product based') {
                $detail->total_price = $salesPrice;
            } else {
                 if ($stock->type == 'stock_adjust')
                $detail->total_price = $req['purchase_price'] * $req['qty'];
                 else
                $detail->total_price = $req['mrp_price'] * $req['qty'];
            }

            $detail->save();

            $total_qty+=$detail->qty;
            $total_amount+=$detail->total_price;
            $mrpPrice+=$req['mrp_price'] * $req['qty'];
            $purchasePrice+=$req['purchase_price'] * $req['qty'];
            $salPrice+=$salesPrice;
            }
        }

        if ($stock->type == 'stock_adjust')
            GeneralHelper::StockAdjustmentSave($request, 'adjust', $stock->id,$stock->date);
        else
            GeneralHelper::StockWastageSave($request, 'adjust', $stock->id,$stock->date);

        $stock->overall_qty = $total_qty;
        $stock->overall_purchase_price = $purchasePrice;
        $stock->overall_sales_price = $salPrice;
        $stock->overall_mrp_price = $mrpPrice;
        $stock->save();
    }

    public static function attachmentSave($image, $currentUser, $stock) {

        foreach ($image as $img) {
            $attach = new Attachment;
            $attach->fill($img);
            $attach->is_active = 1;
            $attach->created_by = $currentUser->id;
            $attach->updated_by = $currentUser->id;
            $attach->ref_id = $stock->id;
            $attach->type = $stock->type;
            $attach->save();
        }
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Stock Adjustment Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentUser = Auth::user();
        try {
            $stock = StockAdjustment::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Product Not Found';
            return $resVal;
        }


        $stock->fill($request->all());
        $stock->is_active = 1;
        $stock->updated_by = $currentUser->id;
        $stock->save();

        if($stock->type=='stock_wastage') {
            $resVal['message'] = 'Stock Wastage Updated Successfully';
        }

        DB::table('tbl_attachment')->where('ref_id', '=', $id)->where('type', '=', $stock->type)->delete();
        $attachment = $request->input('attachment', '');
        InventoryManagementController::attachmentSave($attachment, $currentUser, $stock);

        GeneralHelper::StockAdjustmentDelete($request, $stock->type, $id);
        DB::table('tbl_stock_adjustment_detail')->where('stock_adjustment_id', '=', $id)->delete();

        InventoryManagementController::detailSave($request, $currentUser, $stock);


//        $pro_inv_adj = $request->input("product", '');
//        InventoryManagementController::InventoryAdjustForIsActiveZero($pro_inv_adj, $stock->type,$stock->date);



        $resVal['id'] = $id;
        LogHelper::info1('StockAdjustment Update' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'StockAdjustment', 'update', $screen_code, $stock->id);
        return $resVal;
    }

    public static function InventoryAdjustForIsActiveZero($pro_inv_adj, $type,$date,$comments) {
        $currentuser = Auth::user();

        foreach ($pro_inv_adj as $pro) {
            
            if ( $pro['product_id'] != 0 || $pro['product_id'] != '') {
                $get_product_det = DB::table('tbl_product')
                                ->select('has_inventory', 'is_active')
                                ->where('has_inventory', '=', '1')
                                ->where('is_active', '=', '1')
                                ->where('id', '=', $pro['product_id'])->get();

                if ($get_product_det->count() >= 1) {
                    $data = array();
                    $data['qty'] = $pro['qty'];
                    $data['purchase_invoice_item_id'] = $pro['purchase_invoice_item_id'];
                    $data['bcn_id'] = 0;
                    // if ($pro['is_active'] == 0) {
                $inv_ad = new InventoryAdjustment;

                $inv_ad->fill($pro);
                $inv_ad->ref_id = $pro['stock_adjustment_id'];
                $inv_ad->comments = $comments;
                $inv_ad->date = $date;

                if ($type == 'stock_adjust') {
                    $inv_ad->quantity_debit = $pro['qty'];
                    $inv_ad->type = "stock_adjust_delete";
                    
                    DB::table('tbl_inventory')->where('product_id', '=', $pro['product_id'])
                            ->update(   array(
                                        "quantity" => DB::raw('quantity -' . $pro['qty'])
                                    ) );
                    
                } else {
                    $inv_ad->quantity_credit = $pro['qty'];
                    $inv_ad->type = "stock_wastage_delete";
                     DB::table('tbl_inventory')->where('product_id', '=', $pro['product_id'])
                            ->update(   array(
                                        "quantity" => DB::raw('quantity +' . $pro['qty'])
                                    ) );
                     
                }
                $inv_ad->sku = $pro['product_sku'];


                $get_uom_name = DB::table('tbl_uom')
                                ->select('name')
                                ->where('id', '=', $pro['uom_id'])->first();

                if (!empty($get_uom_name)) {
                    $inv_ad->uom_name = $get_uom_name->name;
                } else
                    $inv_ad->uom_name = "";

                 $bcn_id = DB::table('tbl_bcn')->select('id','code')->where('ref_id', '=', $pro['purchase_invoice_item_id'])->where('ref_type', '=', 'purchase based')->first();

                if (!empty($bcn_id)) {
                    $inv_ad->bcn_id = $bcn_id->id;
                    $inv_ad->barcode = $bcn_id->code;
                    $data['bcn_id'] =$bcn_id->id;
                 }
                 
                 if ($type == 'stock_adjust') {
                      GeneralHelper::StockInventoryDecrease($data);
                } else {
                      GeneralHelper::StockInventoryIncrease($data);
                }

                $inventotyCollection = Inventory::where('product_id', "=", $pro['product_id'])->first();
                $total_stock = $inventotyCollection->quantity;
                $inv_ad->total_stock = $total_stock;


                $inv_ad->created_by = $currentuser->id;
                $inv_ad->updated_by = $currentuser->id;
                $inv_ad->is_active = "1";
                $inv_ad->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
                $inv_ad->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;

                $inv_ad->save();
                
            }
           }
        }
    }

    public function listAll(Request $request) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $id = $request->input('id');
        $userId = $request->input('user_id');
        $isActive = $request->input('is_active');
        $date = $request->input('date');
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $autoNo = $request->input('auto_no');
        $type = $request->input('type');
        $reasonId = $request->input('reason_id');
        $reason = $request->input('reason');
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 0);

        $currentUser = Auth::user();
        
        $decimalFormat = GeneralHelper::decimalFormat();

        $builder = DB::table('tbl_stock_adjustment')
                ->select('*', DB::raw("cast(overall_qty as DECIMAL(12," . $decimalFormat . ")) as overall_qty"));

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if (!empty($reasonId)) {
            $builder->where('reason_id', '=', $reasonId);
        }
        if ($isActive != '') {
            $builder->where('is_active', '=', $isActive);
        }
        if (!empty($userId)) {
            $builder->where('user_id', '=', $userId);
        }
        if (!empty($type)) {
            $builder->where('type', '=', $type);
        }

        if (!empty($reason)) {
            $builder->where('reason', '=', $reason);
        }


        if (!empty($autoNo)) {
            $builder->where('auto_no', '=', $autoNo);
        }

        if (!empty($date)) {
            $builder->whereDate('date', '=', $date);
        }
        if (!empty($from_date)) {
            $builder->whereDate('date', '>=', $from_date);
        }
        if (!empty($to_date)) {
            $builder->whereDate('date', '<=', $to_date);
        }
        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $collection = $builder->get();
        } else {
            $collection = $builder->skip($start)->take($limit)->get();
        }

        foreach ($collection as $collect) {
            $detail = DB::table('tbl_stock_adjustment_detail')
                    ->select('*', DB::raw("cast(qty as DECIMAL(12," . $decimalFormat . ")) as qty"), DB::raw("(qty*purchase_price) as overall_purchase_price"), DB::raw("(qty*mrp_price) as overall_mrp_price"))
                    ->where('stock_adjustment_id', '=', $collect->id)
                    ->where('is_active', '=', 1)
                    ->get();

            $collect->detail = $detail;

            $attach = DB::table('tbl_attachment')
                    ->where('ref_id', '=', $collect->id)
                    ->where('type', '=', $collect->type)
                    ->get();

            $collect->attachment = $attach;
        }
        $resVal['list'] = $collection;
        LogHelper::info1('StockAdjustment List All ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return $resVal;
    }

    public function detail(Request $request) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $decimalFormat = GeneralHelper::decimalFormat();
        $id = $request->input('id');

        $builder = DB::table('tbl_stock_adjustment')
                ->select('*', DB::raw("cast(overall_qty as DECIMAL(12," . $decimalFormat . ")) as overall_qty"))
                ->where('is_active', '=', 1)
                ->where('id', '=', $id)
                ->get();

        foreach ($builder as $build) {
            $detail = DB::table('tbl_stock_adjustment_detail')
                    ->select('*', DB::raw("cast(qty as DECIMAL(12," . $decimalFormat . ")) as qty"))
                    ->where('stock_adjustment_id', '=', $build->id)
                    ->where('is_active', '=', 1)
                    ->get();

            $build->detail = $detail;

            $attach = DB::table('tbl_attachment')
                    ->where('ref_id', '=', $build->id)
                    ->where('type', '=', $build->type)
                    ->get();

            $build->attachment = $attach;
        }
        $resVal['total'] = $builder->count();
        $resVal['data'] = $builder;
        LogHelper::info1('Stock Adjustment Detail ' . $request->fullurl(), json_decode(json_encode($resVal['data']), true));
        return $resVal;
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Stock Adjustment Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentUser = Auth::user();
        try {
            $stock = StockAdjustment::findOrFail($id);
            if ($stock->is_active == 0) {
                return $resVal;
            }
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Stock Adjustment Not Found';
            return $resVal;
        }
        $stock->is_active = 0;
        $stock->updated_by = $currentUser->id;
        $stock->save();
        
        if($stock->type=='stock_wastage') {
            $resVal['message'] = 'Stock Wastage Deleted Successfully';
        }

        DB::table('tbl_attachment')->where('ref_id', '=', $id)->where('type', '=', $stock->type)->delete();

        if ($stock->type == 'stock_adjust')
            $code = 'stock_adjust_delete';
        else
            $code = 'stock_wastage_delete';
        GeneralHelper::StockAdjustmentDelete($request, $code, $id);
        DB::table('tbl_stock_adjustment_detail')->where('stock_adjustment_id', '=', $id)->delete();
        LogHelper::info1('StockAdjustment Delete' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'StockAdjustment', 'delete', $screen_code, $stock->id);
        return $resVal;
    }

    public function categoryWiseStockAdjustmentReport(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $fromDate = $request->input('from_date');
        $toDate = $request->input('to_date');
        $category_id = $request->input('category_id');
        $type = $request->input('type');


        $builder = DB::table('tbl_stock_adjustment as sa')
                ->select(DB::raw("coalesce(c.name,'UnCategorized') as category_name"), 'p.category_id', DB::raw("sum(sd.qty) as qty"), DB::raw("Cast(sum(sd.qty*sd.purchase_price) as Decimal(12,2) ) as purchase_price"), DB::raw("Cast(sum(sd.qty*sd.mrp_price) as Decimal(12,2) ) as mrp_price"))
                ->leftjoin('tbl_stock_adjustment_detail as sd', 'sa.id', '=', 'sd.stock_adjustment_id')
                ->leftjoin('tbl_product as p', 'p.id', '=', 'sd.product_id')
                ->leftjoin('tbl_category as c', 'c.id', '=', 'p.category_id')
                ->where('sa.type', '=', $type)
                ->where('sa.is_active', '=', 1)
                ->groupBy('p.category_id');

        if (!empty($fromDate)) {
            $builder->whereDate('sa.date', '>=', $fromDate);
        }
        if (!empty($toDate)) {
            $builder->whereDate('sa.date', '<=', $toDate);
        }
        if (!empty($category_id)) {
            $builder->where('c.id', '=', $category_id);
        }
        $collection = $builder->get();

        foreach ($collection as $collect) {
            $builderDet = DB::table('tbl_stock_adjustment as sa')
                    ->select('p.name as product_name', 'p.id as product_id', DB::raw("sum(sd.qty) as qty"), DB::raw("Cast(sum(sd.qty*sd.purchase_price) as Decimal(12,2)) as purchase_price"), DB::raw("Cast(sum(sd.qty*sd.mrp_price) as Decimal(12,2) ) as mrp_price"))
                    ->leftjoin('tbl_stock_adjustment_detail as sd', 'sa.id', '=', 'sd.stock_adjustment_id')
                    ->leftjoin('tbl_product as p', 'p.id', '=', 'sd.product_id')
                    ->where('sa.type', '=', $type)
                    ->where('p.category_id', '=', $collect->category_id)
                    ->where('sa.is_active', '=', 1)
                    ->groupBy('p.id');

            if (!empty($fromDate)) {
                $builderDet->whereDate('sa.date', '>=', $fromDate);
            }
            if (!empty($toDate)) {
                $builderDet->whereDate('sa.date', '<=', $toDate);
            }

            $collect->data = $builderDet->get();
        }

        $resVal['list'] = $collection;
        LogHelper::info1('CategoryWiseStockAdjustmentReport ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return $resVal;
    }

    public function CategoryStockAdjustmentReport_csvexport(Request $request) {
        $ItemWiseLists = $this->categoryWiseStockAdjustmentReport($request);
        ini_set('max_execution_time', 600); //increase max_execution_time to 10 min if data set is very large
        $type = $request->input('type', '');
        if ($type == 'stock_adjust') {
            $fName = "CategoryWiseStockAdjustmentReport";
        } elsE {
            $fName = "CategoryWiseStockWastageReport";
        }
        //create a file
        $filename = $fName . "_" . time() . ".csv";
        //  $filename = "customer" . "_" . date("Y-m-d_H-i", time()) . ".csv";
        $serverName = $_SERVER['SERVER_NAME'];

        $filePath = "download/$serverName/csv";

        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);

            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {

                if ($folderName == '')
                    $folderName = $folderList[$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index];

                if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        $filePath = getcwd() . '/' . $filePath;
        $output = fopen($filePath . "/" . $filename, 'w');
        $sr = 1;
        fputcsv($output, array('#', 'Category Name', 'Quantity', 'Overall Purchase Price', 'Overall Mrp Price'));
        foreach ($ItemWiseLists['list'] as $values) {
            fputcsv($output, array($sr, $values->category_name, $values->qty, $values->purchase_price, $values->mrp_price));
            $sr++;
        }
        LogHelper::info1('CategoryStockAdjustmentReport CSV Download ' . $request->fullurl(), $request->all());
        return response()->download($filePath . "/" . $filename, $fName . '.csv');
    }

    public function StockAdjustmentReport_csvexport(Request $request) {
        $ItemWiseLists = $this->listAll($request);
        ini_set('max_execution_time', 600); //increase max_execution_time to 10 min if data set is very large
        $type = $request->input('type', '');
        $arrHead = array('Date', 'Reason', 'Qty', 'Approved By');
        if ($type == 'stock_adjust') {
            $fName = "StockAdjustmentReport";
             array_splice($arrHead, 0, 0, 'Stock Adjustment No');
        } elsE {
            $fName = "StockWastageReport";
             array_splice($arrHead, 0, 0, 'Stock Wastage No');
        }
        //create a file
        $filename = $fName . "_" . time() . ".csv";
        //  $filename = "customer" . "_" . date("Y-m-d_H-i", time()) . ".csv";
        $serverName = $_SERVER['SERVER_NAME'];

        $filePath = "download/$serverName/csv";

        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);

            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {

                if ($folderName == '')
                    $folderName = $folderList[$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index];

                if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        $filePath = getcwd() . '/' . $filePath;
        $output = fopen($filePath . "/" . $filename, 'w');
        

        $projType = GeneralHelper::checkConfiqSetting();
        if ($projType == 'product based') 
            array_splice($arrHead, 4, 0, 'Sales Price');
        else {
            array_splice($arrHead, 4, 0, 'Purchase Value');
            array_splice($arrHead, 5, 0, 'MRP');
     }
         
     
        $sr = 1;
        fputcsv($output, $arrHead);
        $date_format=GeneralHelper::getDateFormat();
        foreach ($ItemWiseLists['list'] as $values) {
             $date=GeneralHelper::CsvDate( $values->date, $date_format);
            $arrVal = array($values->auto_no, $date, $values->reason, $values->overall_qty, $values->emp_name);
            if ($projType == 'product based')
                array_splice($arrVal, 4, 0, $values->overall_sales_price);
            else {
                array_splice($arrVal, 4, 0, $values->overall_purchase_price);
                array_splice($arrVal, 5, 0, $values->overall_mrp_price);
            }
            fputcsv($output, $arrVal);
            $sr++;
        }
        LogHelper::info1('Stock Adjustment Report CSV Download ' . $request->fullurl(), $request->all());
        return response()->download($filePath . "/" . $filename, $fName . '.csv');
    }
    
    public function StockItemDelete(Request $request,$id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Stock Item Deleted Successfully';

        try {
            $stock = StockAdjustmentDetail::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Stock Item  Not Found';
            return $resVal;
        }

        //This is for Maintain purchase inventory
        $builder = DB::table('tbl_stock_adjustment_detail')->where('id', $id)->get();
        
        $data=StockAdjustment::where('id','=',$stock->stock_adjustment_id)->first();
       if($data != ''){
           $date=$data->date;
       }
        $array = json_decode(json_encode($builder), True);

        InventoryManagementController::InventoryAdjustForIsActiveZero($array, $data->type,$date,$request->input('comments',''));

        $stock->comments=$request->input('comments','');
        $stock->is_active=0;
        $stock->save();
        InventoryManagementController::stockUpdate($data);
        return $resVal;
    }
    
    public static function stockUpdate($stock){
        $builder = DB::table('tbl_stock_adjustment_detail')
                ->where('stock_adjustment_id', $stock->id)
                ->where('is_active','=',1)
                ->get();
        
            $total_qty=0;
            $total_amount=0;
            $mrpPrice=0;
            $purchasePrice=0;
            $salPrice=0;
        
        foreach($builder as $detail){
            $total_qty+=$detail->qty;
            $total_amount+=$detail->total_price;
            $mrpPrice+=$detail->mrp_price * $detail->qty;
            $purchasePrice+=$detail->purchase_price * $detail->qty;
            $salPrice+= $detail->sales_price * $detail->qty;
        }
        $stock->overall_qty = $total_qty;
        $stock->overall_purchase_price = $purchasePrice;
        $stock->overall_sales_price = $salPrice;
        $stock->overall_mrp_price = $mrpPrice;
        $stock->save();
    }

}

?>