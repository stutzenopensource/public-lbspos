<?php

namespace App\Http\Controllers;

use DateTime;
use App\User;
use Carbon\Carbon;
use App\PersistentLogin;
use Faker\Provider\Uuid;
use Illuminate\Http\Request;
use App\Transformer\UserTransformer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use GuzzleHttp\Client as HttpClient;
use Illuminate\Mail\Message;
use Swift_Message;
use DB;
use App\ActivityLog;
use App\Bcn;
use App\PurchasePayment;
use App\Payment;
use App\Invoice;
use App\Invoiceitem;
use App\InvoiceTax;
use App\Attributevalue;
use App\Attribute;
use App\DealActivity;
use app\Customer;
use App\Templates;
use App\Transaction;
use App\Helper\SmsHelper;
use App\Helper\MailHelper;
use App\Helper\AuthorizationHelper;
use App\Helper\GeneralHelper;
use App\Helper\ActivityLogHelper;
use App\Helper\AccountHelper;
use App\Helper\PdfHelper;
use App\Helper\LogHelper;
use App\Helper\NotificationHelper;
use App\Helper\TransactionHelper;
use App\InvoiceReturn;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of invoicesController
 *
 * @author Deepa
 */
class InvoiceController extends Controller {

//put your code here
    public function Save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Invoice Added Successfully';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $invoice_no = $request->input('invoice_no', '');
        $prefix = $request->input('prefix', '');

        $invoice_code = '';
        if (!empty($invoice_no)) {

            $get_inv_no = DB::table('tbl_invoice')
                    ->select(DB::raw("MAX(invoice_no) AS invoice_no"))
                    ->where('prefix', '=', $prefix)
                    ->where('is_active', '=', 1)
                    ->first();
            $invoice_no = $get_inv_no->invoice_no + 1;
            $invoice_code = $prefix . $invoice_no;
        } else {
            $get_inv_no = DB::table('tbl_invoice')
                    ->select('invoice_no')
                    ->where('is_active', '=', 1)
                    ->where('prefix', '=', $prefix)
                    ->orderBy('invoice_no', 'desc')
                    ->first();
            if (isset($get_inv_no)) {
                $inv_no = $get_inv_no->invoice_no;
                $inv_no = $inv_no + 1;
            } else {
                $inv_no = 1;
            }
            $invoice_no = $inv_no;
            $invoice_code = $prefix . $inv_no;
        }

        $subtotal = 0;
        $discount_amount = 0;
        $roundoff = 0;
        $tax_id = 0;
        $tax_amount = 0;
        $taxable_amount = 0;
        $total_before_roundoff = 0;
        $total = 0;

        $invoiceItemCol = $request->input('item');
        foreach ($invoiceItemCol as $item) {
            $item['total_price'] = floatval($item['unit_price']) * floatval($item['qty']);

            $item['total_price'] = floatval(number_format($item['total_price'], 2, '.', ''));
            $subtotal = $subtotal + $item['total_price'];
        }
        $discountMode = $request->input('discount_mode', '');
        $discountValue = $request->input('discount_percentage', '');
        if (empty($discountMode) || empty($discountValue)) {
            $discount_amount = 0;
            $taxable_amount = $subtotal;
        } else {

            if ($discountMode == '%') {
                $discount_amount = $subtotal * $discountValue / 100;
                $discount_amount = floatval(number_format($discount_amount, 2, '.', ''));
            } else {
                $discount_amount = $discountValue;
            }

            $taxable_amount = $subtotal - $discount_amount;
        }

        $currentuser = Auth::user();
        $invoice = new Invoice;
        $invoice->fill($request->all());
        
        $hash_key = GeneralHelper::generateRandomString();
        
        $invoice->hash_key =  $hash_key  ;
 
        $invoice->created_by = $currentuser->id;
        $invoice->updated_by = $currentuser->id;
        $invoice->is_active = $request->input('is_active', 1);
        //Finding DayCode
        $dayLog = DB::table('tbl_day_log')->select('*')
                        ->whereRaw('id = (select max(`id`) from tbl_day_log)')->first();
        if ($dayLog != "")
            $invoice->day_code = $dayLog->id;
        $invoice->save();

        //This is for Calculate Tax for Particulat product

        $customerID=$request->input('customer_id');

        $settings = DB::table('tbl_app_config')
                        ->select('*')->where('setting', '=', 'sales_product_list')->where('value', 'product based')->get()->first();
        if ($settings == "") {
            $tax_amount = 0;
            $discount_amount = 0;

            $product_with_taxlist = GeneralHelper::getProductTaxforPurchase($request->input('item'),$customerID);
            $tax_amount = 0;
            $discount_amount = 0;

            foreach ($product_with_taxlist as $item) {
                $invoiceitems = new Invoiceitem;
                $invoiceitems->fill($item);
                $invoiceitems->created_by = $currentuser->id;
                $invoiceitems->updated_by = $currentuser->id;
                $invoiceitems->invoice_id = $invoice->id;
                $invoiceitems->is_active = $request->input('is_active', 1);
                $invoiceitems->customer_id = $request->input('customer_id');
                $invoiceitems->duedate = $invoice->duedate;
                $invoiceitems->total_price = $item['qty'] * $item['unit_price'];
                $invoiceitems->paymentmethod = $invoice->paymentmethod;
                $invoiceitems->save();

                $product_discount_amount = GeneralHelper::calculateDiscount($item);
                $invoiceitems->discount_amount = $product_discount_amount;

                $remain_value = ($item['qty'] * $item['unit_price']) - $product_discount_amount;
                $item['produt_rem_amount'] = $remain_value;

                $product_tax_amount = GeneralHelper::insertProductTax($item, $invoice->id, $invoiceitems->id,'invoice');
                $sales_value = (($item['qty'] * $item['unit_price']) - $product_discount_amount) - $product_tax_amount;

                $discount_amount = $discount_amount + $product_discount_amount;
                $tax_amount = $tax_amount + $product_tax_amount;

                $invoiceitems->total_price=(($item['qty'] * $item['unit_price'])-$product_discount_amount);
                $invoiceitems->tax_amount = $product_tax_amount;
                $invoiceitems->sales_value = $sales_value;
                $invoiceitems->save();
            }
        } else {

            $product_with_taxlist = GeneralHelper::getProductTax($request->input('item'),$customerID);
            $tax_amount = 0;
            $discount_amount = 0;
            foreach ($product_with_taxlist as $item) {
                $invoiceitems = new Invoiceitem;
                $invoiceitems->fill($item);
                $invoiceitems->created_by = $currentuser->id;
                $invoiceitems->updated_by = $currentuser->id;
                $invoiceitems->invoice_id = $invoice->id;
                $invoiceitems->is_active = $request->input('is_active', 1);
                $invoiceitems->customer_id = $request->input('customer_id');
                $invoiceitems->total_price = $item['qty'] * $item['unit_price'];
                $invoiceitems->duedate = $invoice->duedate;
                $invoiceitems->paymentmethod = $invoice->paymentmethod;
                $invoiceitems->save();

                $product_discount_amount = GeneralHelper::calculateDiscount($item);
                $invoiceitems->discount_amount = $product_discount_amount;

                $remain_value = ($item['qty'] * $item['unit_price']) - $product_discount_amount;
                $item['produt_rem_amount'] = $remain_value;
                //   print_r($product_with_taxlist);
                $product_tax_amount = GeneralHelper::insertProductTax($item, $invoice->id, $invoiceitems->id,'invoice');
                $sales_value = (($item['qty'] * $item['unit_price']) - $product_discount_amount) + $product_tax_amount;

                $discount_amount = $discount_amount + $product_discount_amount;
                $tax_amount = $tax_amount + $product_tax_amount;

                $invoiceitems->tax_amount = $product_tax_amount;
                $invoiceitems->sales_value = $sales_value;
                $invoiceitems->save();
            }
        }
        $packing_charge = $request->input('packing_charge', 0.0);
        $insurance_charge = $request->input('insurance_charge', 0.0);

        //finding roundoff value 
        $total_before_roundoff = $subtotal - $discount_amount + $packing_charge + $insurance_charge;
        $total_after_roundoff = round($total_before_roundoff);
        $roundoff = $total_after_roundoff - $total_before_roundoff;
        $roundoff = floatval(number_format($roundoff, 2, '.', ''));

        //finding grant total value 
        $total = $subtotal - $discount_amount + $roundoff + $packing_charge + $insurance_charge;
        ;
        $total = floatval(number_format($total, 2, '.', ''));



        $invoice->subtotal = $subtotal;
        $invoice->tax_amount = $tax_amount;
        $invoice->discount_amount = $discount_amount;
        $invoice->round_off = $roundoff;

        $invoice->total_amount = $total;
        $invoice->invoice_no = $invoice_no;
        $invoice->invoice_code = $invoice_code;

        $invoice->save();





        if ($request->input('quote_id') != NULL && !empty($request->input('quote_id'))) {
            DB::table('tbl_payment')->where('quote_id', '=', $request->input('quote_id'))->where('is_active', '=', 1)->update(['invoice_id' => $invoice->id]);
            DB::table('tbl_quote')->where('id', $request->input('quote_id'))->update(['status' => 'invoiced']);
        }
        if ($request->input('sales_order_id') != NULL && !empty($request->input('sales_order_id'))) {
            DB::table('tbl_payment')->where('sales_order_id', '=', $request->input('sales_order_id'))->where('is_active', '=', 1)->update(['invoice_id' => $invoice->id]);
            DB::table('tbl_sales_order')->where('id', $request->input('sales_order_id'))->update(['status' => 'invoiced']);
        }
        $totalamount = $invoice->total_amount;

        $payment = DB::table('tbl_payment')->where('invoice_id', $invoice->id)->where('is_active', '=', 1)->sum('amount');

        if (($totalamount <= $payment)) {
// DB::table('tbl_invoice') ->where('id',$invoice->id) ->update(['status' => 'paid' ]);
            $invoice->status = 'paid';
        } elseif ($payment == 0) {
            $invoice->status = 'unpaid';
        } elseif ($totalamount > $payment) {

            $invoice->status = 'partiallyPaid';
        }

        $customerItemCol = ($request->input('customattribute'));

        $attribute = array();
        foreach ($customerItemCol as $item) {
            $attribute[$item['attribute_code']] = $item['value'];
        }
        if ($request->input('customattribute') != null) {
            $json_att = json_encode($attribute);
            $invoice->custom_attribute_json = $json_att;
        }
        $invoice->paid_amount = $payment;
        $invoice->save();




        if ($request->input('customattribute') != null) {
            foreach ($customerItemCol as $attribute) {
//print_r($attribute);

                $customerattribute = new Attributevalue;

                $customerattribute->refernce_id = $invoice->id;
                $customerattribute->value = $attribute['value'];
                if (isset($attribute['sno'])) {
                    $customerattribute->sno = $attribute['sno'];
                }
                $customerattribute->attribute_code = $attribute['attribute_code'];

                $customerattribute->created_by = $currentuser->id;
                $customerattribute->updated_by = $currentuser->id;
                if ($attribute['attribute_code'] != NULL) {


                    $attributeCollection = Attribute::where('attribute_code', "=", $attribute['attribute_code'])->get();
                    if (count($attributeCollection) > 0) {
                        $attribute = $attributeCollection->first();
//print_r($attribute->id);
                        $customerattribute->attributes_id = $attribute->id;
                        $customerattribute->attributetype_id = $attribute->attributetype_id;
                    }
                }

//$customerattribute->fill($attribute);       

                $customerattribute->save();
            }
        }
        $builder = DB::table('tbl_customer')
                        ->select('*')
                        ->where('id', '=', $invoice->customer_id)->first();
        $cus_name = $builder->fname . " " . $builder->lname;


        $mydate = $invoice->date;



        $transactionData = array(
            "name" => $currentuser->f_name,
            "transaction_date" => $mydate,
            "voucher_type" => Transaction::$SALES,
            "voucher_number" => $invoice->id,
            "credit" => 0.00,
            "debit" => $invoice->total_amount,
            "pmtcode" => GeneralHelper::getPaymentAcode($request->input('account_id', '')),
            "pmtcoderef" => $request->input('account_id', ''),
            "is_cash_flow" => 0, //have to check  account in save and voucher type too
            "account" => $request->input('account', ''),
            "acode" => GeneralHelper::getCustomerAcode($invoice->customer_id),
            "acoderef" => $invoice->customer_id,
            "customer_id" => $invoice->customer_id,
            "particulars" => '#' . $invoice->id . 'Sales invoice Created for ' . $cus_name . ' by ' . $currentuser->f_name . ' ' . $currentuser->l_name,
            "created_by" => $currentuser->id,
            "updated_by" => $currentuser->id,
            "created_by_name" => $currentuser->f_name . ' ' . $currentuser->l_name,
            "updated_by_name" => $currentuser->f_name . ' ' . $currentuser->l_name,
            "is_active" => 1,
        );
        TransactionHelper::transactionSave($transactionData);


        ActivityLogHelper::invoiceSave($invoice);

        $resVal['id'] = $invoice->id;
        $code = "invoice";
        GeneralHelper::StockWastageSave($request->input('item'), $code, $invoice->id,$invoice->date);
        SmsHelper::invoiceCreatedNotification($invoice);

        // GeneralHelper::InvoiceTaxSave($invoice->id, $request->input('tax_id'), $taxable_amount, $subtotal, 'sales');
        $get_pdf_format = DB::table("tbl_app_config")
                ->where('setting', '=', 'invoice_print_template')
                ->first();



        $invoice_global = $invoice->id;
        PdfHelper::pdfGenerate($invoice->id);

        /* if ($get_pdf_format->value == 't1')

          else if ($get_pdf_format->value == 't2')
          PdfHelper::invoicePrintTemplate2($invoice->id);
          else if ($get_pdf_format->value == 't3')
          PdfHelper::invoicePrintTemplate3($invoice->id);
          else if ($get_pdf_format->value == 't4')
          PdfHelper::invoicePrintTemplate4($invoice->id); */

        //This is for Maintain purchase inventory

        GeneralHelper::updatePurchaseInventory($request->input('item'));

        $mail_obj = new MailHelper;
        $mail_obj->invoiceCreatedNotification($invoice);
        $deal_id = $request->input('deal_id', '');
        if ($deal_id != '') {
            $deal = new DealActivity;
            $get_contact_id = DB::table('tbl_customer')->where('is_active', 1)
                            ->where('id', '=', $request->customer_id)->first();
            $current_date = date('Y-m-d');
            $timestamp = date('H:i:s');
            $timestamp = strtotime($timestamp);
            $time = date('h:i:s');
            $time = strtotime($time);

            $deal->created_by = $currentuser->id;
            $deal->updated_by = $currentuser->id;
            $deal->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $deal->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $deal->time = $time;
            $deal->date = $current_date;
            $deal->type = 'invoice';
            $deal->description = "created an invoice";
            $deal->contact_id = $get_contact_id->contact_id;
            $deal->user_id = $currentuser->id;
            $deal->user_name = $currentuser->f_name;
            $deal->is_active = 1;
            $deal->ref_id = $invoice->id;

            $deal->deal_id = $deal_id;
            $deal->comments = "created an invoice";

            $deal->save();
        }
        LogHelper::info1('Invoice Save' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Invoice', 'save', $screen_code, $invoice->id);

        return $resVal;
    }

    public function invoice_csvexport(Request $request) {
        $studentLists = $this->listAll($request);
        ini_set('max_execution_time', 600); //increase max_execution_time to 10 min if data set is very large
//create a file

        $filename = "Invoice" . "_" . time() . ".csv";
//  $filename = "customer" . "_" . date("Y-m-d_H-i", time()) . ".csv";

        $serverName = $_SERVER['SERVER_NAME'];

        $filePath = "download/$serverName/csv";

        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);

            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {

                if ($folderName == '')
                    $folderName = $folderList[$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index];

                if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        $filePath = getcwd() . '/' . $filePath;
        $sr = 1;
        $output = fopen($filePath . "/" . $filename, 'w');
        fputcsv($output, array('#', 'Date', 'Invoice', 'Customer Name', 'City', 'Prefix', 'Amount', 'Paid Amount'));
        foreach ($studentLists['list'] as $values) {
            fputcsv($output, array($sr, $values->date, 'Invoice Code #' . $values->invoice_code, $values->customer_fname, $values->billing_city, $values->prefix, $values->total_amount, $values->paid_amount));
// id: 1, customer_id: 37, prefix: "test123r", date: "2017-05-02", duedate: "2017-05-02"
            $sr++;
        }
        LogHelper::info1('Invoice CSV Download' . $request->fullurl(), $request->all());
        return response()->download($filePath . "/" . $filename, 'Invoice.csv');
    }

    public function salesinvoicejson_csvexport(Request $request) {
        $salesinvoicejsonLists = $this->listAll($request);
        $sr = 1;
        $output = fopen('salesinvoicejson.csv', 'w');
        fputcsv($output, array('#', 'Date', 'Invoice', 'Customer Name', 'Paid Amount', 'Amount', 'detail', 'json'));
        foreach ($salesinvoicejsonLists['list'] as $values) {
            $sx = $values->custom_attribute_json;
            // echo $sx;
            fputcsv($output, array($sr, $values->date, 'Invoice No #' . $values->invoice_code,
                $values->customer_fname, $values->paid_amount, $values->total_amount, $values->custom_attribute_json));

            $sr++;
        }
        LogHelper::info1('SalesInvoiceJson CSV Download' . $request->fullurl(), $request->all());
        return response()->download(getcwd() . "/salesinvoicejson.csv");
    }

    public function invoicelist_csvexport(Request $request) {

        $salesinvoicejsonLists = $this->listAll($request);
        $sr = 1;
        $filename = "SalesInvoice" . "_" . time() . ".csv";
        $serverName = $_SERVER['SERVER_NAME'];
        $filePath = "download/$serverName/csv";

        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);
            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {
                if ($folderName == '')
                    $folderName = $folderList[$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index];
                if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        $filePath = getcwd() . '/' . $filePath;

        $attributeCollection = Attribute::where('attributetype_code', "=", 'invoice')
                        ->where('is_active', '=', 1)
                        ->where('is_show_in_list', '=', 1)
                        ->orderBy('sno', 'asc')->get();
        $output = fopen($filePath . "/" . $filename, 'w');
        $csvHeader = array('#', 'Customer', 'Total', 'Paid', 'Invoice Date', 'Due Date', 'Status');
        foreach ($attributeCollection as $attribute) {
            array_push($csvHeader, $attribute->attribute_label);
        }
        fputcsv($output, $csvHeader);
       $date_format=GeneralHelper::getDateFormat();
               
        foreach ($salesinvoicejsonLists['list'] as $invoice) {
            $json_att = $invoice->custom_attribute_json;
            $date=GeneralHelper::CsvDate($invoice->date, $date_format);
            $duedate=GeneralHelper::CsvDate($invoice->duedate, $date_format);
            $csv_row = array($invoice->invoice_code, $invoice->customer_fname, $invoice->total_amount, $invoice->paid_amount,$date
                ,$duedate, $invoice->status);
            foreach ($attributeCollection as $attribute) {
                $key = $attribute->attribute_code;
                if (isset($json_att->$key)) {
                    array_push($csv_row, $json_att->$key);
                } else
                    array_push($csv_row, '');
            }
            fputcsv($output, $csv_row);
        }
        LogHelper::info1('InvoiceList CSV Download' . $request->fullurl(), $request->all());
        return response()->download($filePath . "/" . $filename, 'SalesInvoice.csv');
    }

    public function listAll(Request $request) {
        $resVal = array();
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $id = $request->input('id');
        $customerid = $request->input('customer_id');
        $mobile = $request->input('mobile', '');
        $invoice_code = $request->input('invoice_code');
        $fromDate = $request->input('from_duedate', '');
        $toDate = $request->input('to_duedate', '');
        $isactive = $request->input('is_active', '');
        $status = $request->input('status');
        $fromdate = $request->input('from_date');
        $todate = $request->input('to_date');
        $mode = $request->input('mode', '');
        $notes = $request->input('notes');
        $invoiceDate = $request->input('invoice_date', '');
        $dueDate = $request->input('due_date', '');
        $noPrefix = $request->input('noPrefix');
        $attribute_value = $request->input('attribute_value');
        $discountpercentage = $request->input('discount_percentage');
        $deal_id = $request->input('deal_id', '');
        $prefix = $request->input('prefix', '');

//print_r($builder);
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
//$builder = DB::table('tbl_invoice')
// ->select('*');
        $builder = DB::table('tbl_invoice as i')
                ->leftJoin('tbl_customer as c', 'i.customer_id', '=', 'c.id')
                ->leftJoin('tbl_customer as d', 'i.payee', '=', 'd.id')
                ->leftJoin('tbl_customer as e', 'i.consignee', '=', 'e.id')
                ->leftJoin('tbl_tax as t', 'i.tax_id', '=', 't.id')
                ->select('i.*', 'c.fname as customer_fname', 'c.lname as customer_lname', 't.tax_name as taxname', 't.tax_percentage as taxpercentage', 'd.fname as payee_name', 'e.fname as consignee_name', 'c.shopping_city', 'c.billing_city');


        if (!empty($status)) {
            $builder->where('i.status', '=', $status);
        }
        if (!empty($mobile)) {
            $builder->where('c.phone', '=', $mobile);
        }
        if (!empty($invoice_code)) {
            $builder->where('i.invoice_code', 'like', '%' . $invoice_code . '%');
        }
        if (!empty($discountpercentage)) {
            $builder->where('i.discount_percentage', '=', $discountpercentage);
        }
        if (!empty($customerid)) {
            $builder->where('i.customer_id', '=', $customerid);
        }
        if (!empty($invoiceDate)) {
            $builder->whereDate('i.date', '=', $invoiceDate);
        }
        if (!empty($dueDate)) {
            $builder->whereDate('i.duedate', '=', $dueDate);
        }
        if (!empty($id)) {
            $builder->where('i.id', '=', $id);
        }
        if (!empty($prefix) || !empty($noPrefix)) {
            $prefix1 = explode(',', $prefix);
            $result = "'" . implode("', '", $prefix1) . "'";
            $builder->whereRaw(DB::raw("((Case When '" . $prefix . "' <> '' Then i.prefix in ($result) End) or (Case When '" . $noPrefix . "' = -1 Then i.prefix='' End)) "));
        }


        if (!empty($fromDate)) {

            $builder->whereDate('i.duedate', '>=', $fromDate);
        }
        if (!empty($toDate)) {

            $builder->whereDate('i.duedate', '<=', $toDate);
        }
        if (!empty($fromdate)) {

            $builder->whereDate('i.date', '>=', $fromdate);
        }
        if (!empty($todate)) {

            $builder->whereDate('i.date', '<=', $todate);
        }
        if (!empty($notes)) {
            $builder->where('i.notes', '=', $notes);
        }

        if ($isactive != '') {
            $builder->where('i.is_active', '=', $isactive);
        }
        if ($deal_id != '') {
            $builder->where('i.deal_id', '=', $deal_id);
        }

//        if ($attribute_value != '') {
//            $builder->groupBy('a.refernce_id');
//            $builder->where('a.value', '=', $attribute_value);
//        }

        $arrays[] = $request->toArray();
//     print_r($arrays);
        $arrays2 = $arrays[0];
        $array_key = array_keys($arrays2);
        $custom_array = array();
        for ($index = 0; $index < count($array_key); $index ++) {
            $arry_ind = $array_key[$index];
            $result = substr($arry_ind, 0, 8);
            if ($result == 'cus_attr') {
                array_push($custom_array, $arry_ind);
            }
        }
        for ($index = 0; $index < count($custom_array); $index ++) {
            $status1 = $custom_array[$index];
            $status = $arrays2[$status1];
            if (!empty($status)) {
                $status = explode("::", $status);
                $key = $status[0];
                $value = $status[1];
                $str = "'%" . '"' . $key . '":"' . $value . '"%' . "'";
                $builder->whereRaw('i.custom_attribute_json  like' . "'%" . '"' . $key . '":"' . $value . '"%' . "'");
//  $builder->Where ('custom_attribute_json', 'like',  $str);                        
            }
        }

        if ($mode == 0 || $mode == '') {
            $builder->orderBy('i.id', 'desc');
        } else if ($mode == 1) {
            $builder->orderBy('i.date', 'asc');
        } else if ($mode == 2) {
            $builder->orderBy('i.invoice_no', 'desc');
        }

        $resVal['total'] = $builder->count();

        if ($start == 0 && $limit == 0) {
            $invoiceCollection = $builder->get();
        } else {

            $invoiceCollection = $builder->skip($start)->take($limit)->get();
        }
        $show_all = $request->input('show_all', '');
        foreach ($invoiceCollection as $invoice) {
            $inv_id = $invoice->id;
            if ($show_all == 1) {
                $invoiceitems = DB::table('tbl_invoice_item')->where('invoice_id', $inv_id);
                $invoice->item = $invoiceitems->get();
            }

            $json_att = $invoice->custom_attribute_json;
            if (empty($json_att)) {
                $json_att = "{}";
            }
            $json_att = json_decode($json_att);
            $invoice->custom_attribute_json = $json_att;
        }

        $resVal['list'] = $invoiceCollection;
        LogHelper::info1('Invoice List All' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }

    public function detail(Request $request) {

        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $id = $request->input('id');
        $invoice_no = $request->input('invoice_no');


        $isactive = $request->input('isactive', '');
        $discountpercentage = $request->input('discount_percentage', '');

        $decimalFormat = GeneralHelper::decimalFormat();

//$builder = DB::table('tbl_invoice')->select('*');
        $builder = DB::table('tbl_invoice as i')
                ->leftJoin('tbl_user as cu', 'cu.id', '=', 'i.created_by')
                ->leftJoin('tbl_user as u', 'u.id', '=', 'i.updated_by')
                ->leftJoin('tbl_customer as c', 'i.customer_id', '=', 'c.id')
                ->leftJoin('tbl_customer as d', 'i.payee', '=', 'd.id')
                ->leftJoin('tbl_customer as e', 'i.consignee', '=', 'e.id')
                ->leftJoin('tbl_tax as t', 'i.tax_id', '=', 't.id')
                ->select('i.id', 'i.invoice_no', 'i.invoice_code','is_pos', 'i.customer_id', 'i.prefix', 'i.date', 'i.duedate', 'i.datepaid','i.gst_no','i.cust_gst_no'
                , 'i.subtotal', 'i.discount_mode', 'i.discount_amount', 'i.credit_amount', 'i.tax_amount', 'i.total_amount', 'i.status'
                , 'i.paymentmethod', 'i.notes', 'i.is_active', 'i.created_by', 'i.updated_by', 'i.updated_at', 'i.created_at'
                , 'i.tax_id', 'i.customer_address', 'i.quote_id', 'i.discount_percentage', 'i.paid_amount', 'i.round_off', 'i.deal_id', 'i.deal_name', 'i.payee', 'i.consignee'
                , 'c.fname as customer_fname', 'c.lname as customer_lname', 't.tax_name as taxname', 'c.phone', 'c.email'
                , 't.tax_percentage as taxpercentage', 'cu.f_name as created_byfname', 'cu.l_name as created_bylname'
                , 'u.f_name as updated_byfname', 'u.l_name as updated_bylame', 'd.fname as payee_name', 'e.fname as consignee_name', DB::raw("(i.total_amount-i.tax_amount-round_off) as taxable_amount")
                        , DB::raw("cast(i.total_qty as DECIMAL(12," . $decimalFormat . ")) as total_qty"));
        ;

        $invoiceitems = DB::table('tbl_invoice_item as it')
                ->leftjoin('tbl_bcn as b','b.id','=','it.purchase_invoice_item_id')
                ->leftjoin('tbl_invoice_return_item as irt','irt.invoice_item_id','=','it.id')
                ->select('it.*','b.code as barcode', DB::raw("cast(it.qty as DECIMAL(12," . $decimalFormat . ")) as qty"), DB::raw("e.name AS emp_name"), DB::raw("coalesce(cast(sum(irt.qty) as DECIMAL(12," . $decimalFormat . ")),0) as refund_qty"))
                ->leftjoin('tbl_employee as e', 'e.id', '=', 'it.emp_id')
                ->where('it.invoice_id', $id)
                ->where('it.is_active', '=', 1)
                ->groupby('it.id','it.product_id');
// $customerattribute = DB::table('tbl_attribute_value')->where('refernce_id', $id);

        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        if (!empty($id)) {
            $builder->where('i.id', '=', $id);
        }
        if (!empty($invoice_no)) {
            $builder->where('i.invoice_no', '=', $invoice_no);
        }
        if (!empty($discountpercentage)) {
            $builder->where('i.discount_percentage', '=', $discountpercentage);
        }

        if ($isactive != '') {
            $builder->where('i.is_active', '=', $isactive);
        }
        $builder->orderBy('i.id', 'desc');

        $invoiceCollection = $builder->skip($start)->take($limit)->get();
        if (count($invoiceCollection) == 1) {
            $invoice = $invoiceCollection->first();

            $invoice->item = $invoiceitems->get();
            
//            foreach($invoice->item as $it){
//                $returnQty = DB::table('tbl_invoice_return_item as it')
//                ->where('invoice_id', $id)
//                ->where('invoice_ret')        
//                ->select(DB::raw('sum(it.qty) as qty, it.product_id'),DB::raw("cast(sum(it.qty) as DECIMAL(12," . $decimalFormat . ")) as qty"))
//                ->groupBy('product_id')
//                ->get();
//                
//                $it
//            }

//$invoice->customattribute = $customerattribute->get();
            $customAttributeValue = DB::table('tbl_attribute_value as a')
                            ->leftJoin('tbl_attributes as abs', 'a.attributes_id', '=', 'abs.id')
                            ->select('a.*', 'abs.attribute_label as attribute_label')
                            ->where('a.refernce_id', '=', $id)
                            ->where('abs.attributetype_code', '=', 'invoice')->get();
            $customattribute = DB::table('tbl_attributes as abs')
                            ->select('abs.*')
                            ->where('abs.attributetype_code', '=', 'invoice')->get();
            if (count($customAttributeValue) > 0) {

                foreach ($customAttributeValue as $item) {
                    foreach ($customattribute as $customattributeItem) {

                        if ($item->attribute_code == $customattributeItem->attribute_code) {

                            $customattributeItem->value = $item->value;
                        }
                    }
                }
            }

            $invoice->customattribute = $customattribute;

            $invoice_tax_list_collection = DB::table('tbl_invoice_tax')
                            ->select('*')
                            ->where('invoice_id', '=', $id)
                            ->where('is_active', '=', 1)->get();

            // $invoice->tax_list = $invoice_tax_list_collection;
            //foreach ($invoice as $invoice_list) {
            $item = $invoice->item;
            $product_tax_det = array();
            foreach ($item as $product) {
                $item_id = $product->id;
                $builder = DB::table('tbl_invoice_item_tax')
                        ->select('*')
                        ->where('is_active', '=', 1)
                        ->where('invoice_id', '=', $id)
                        ->where('invoice_item_id', '=', $item_id)
                        ->get();
                $product->tax_list = array();
                if (count($builder) > 0) {
                    $product->tax_list = $builder;
                }
                array_push($product_tax_det, $product);
            }

            $builder = $builder = DB::table('tbl_tax as t')
                    ->leftJoin('tbl_invoice_item_tax as tm', 'tm.tax_id', '=', 't.id')
                    ->where('tm.is_active', '=', 1)
                    ->where('tm.invoice_id', '=', $id)
                    ->select(DB::raw('t.tax_name,SUM(tm.tax_amount) AS amount,tm.tax_id,tm.tax_percentage'))
                    ->groupBy('tm.tax_id')
                    ->get();
            $invoice->tax_summary = $builder;
            
            
            $builder = $builder = DB::table('tbl_tax as t')
                    ->leftJoin('tbl_invoice_item_tax as tm', 'tm.tax_id', '=', 't.id')
                    ->where('tm.is_active', '=', 1)
                    ->where('tm.invoice_id', '=', $id)
                    ->select(DB::raw("SUBSTRING_INDEX(tm.tax_name, ' ', 1) as name"))
                    ->groupBy('name')
                    ->get();
            
            foreach($builder as $bu)
            {
                 $builder1 =  DB::table('tbl_tax as t')
                    ->leftJoin('tbl_invoice_item_tax as tm', 'tm.tax_id', '=', 't.id')
                    ->where('tm.is_active', '=', 1)
                    ->where('tm.invoice_id', '=', $id)
                    ->select(DB::raw('t.tax_name as name ,tm.tax_amount AS taxAmount, tm.tax_percentage as taxPercentage '))
                    ->where('t.tax_name', 'like', '%' . $bu->name . '%')
                     ->get();
                 $bu->taxDetail = $builder1;
            
            }
            
            $invoice->tax_detail = $builder;
            
            $resVal['data'] = $invoice;
        }


        LogHelper::info1('Invoice Detail' . $request->fullurl(), $request->all());
        return ($resVal);
    }

    public function update(Request $request, $id) {

        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Invoice Updated Successfully';
        $currentuser = Auth::user();
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        try {
            $invoice = Invoice::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Invoice Not Found';
            return $resVal;
        }
        $invoice_no = $request->input('invoice_no', '');
        $prefix = $request->input('prefix', '');
        $invoice_code = '';
        if (!empty($invoice_no)) {

            $get_inv_no = DB::table('tbl_invoice')
                    ->select('is_active')
                    ->where('prefix', '=', $prefix)
                    ->where('is_active', '=', 1)
                    ->where('invoice_no', '=', $invoice_no)
                    ->where('id', '!=', $id)
                    ->get();
            if (count($get_inv_no) > 0) {
                $resVal['message'] = 'Invoice No Already Exists';
                $resVal['success'] = FALSE;
                return $resVal;
            } else {
                $invoice_no = $invoice_no;
                $invoice_code = $prefix . $invoice_no;
            }
        } else {
            $get_inv_no = DB::table('tbl_invoice')
                    ->select('invoice_no')
                    ->where('is_active', '=', 1)
                    ->where('prefix', '=', $prefix)
                    ->orderBy('invoice_no', 'desc')
                    ->where('id', '!=', $id)
                    ->first();
            if (isset($get_inv_no)) {
                $inv_no = $get_inv_no->invoice_no;
                $inv_no = $inv_no + 1;
            } else {
                $inv_no = 1;
            }
            $invoice_no = $inv_no;
            $invoice_code = $prefix . $inv_no;
        }

        $returnCollect = InvoiceReturn::where('invoice_id', '=', $id)->where('is_active', '=', 1)->get();
        if ($returnCollect->count() > 0) {
            $resVal['success'] = FALSE;
             $resVal['message'] = "Invoice Was Returned So Can't Able To Edit";
            return $resVal;
        }

        
        if($invoice->is_pos==1){
          $resVal['success'] = FALSE;
          $resVal['message'] = "This Is Pos Invoice So Can't Able To Edit";
          return $resVal;
        }
        

        $subtotal = 0;
        $discount_amount = 0;
        $roundoff = 0;
        $tax_id = 0;
        $tax_amount = 0;
        $taxable_amount = 0;
        $total_before_roundoff = 0;
        $total = 0;

        $code = "iupdate";
        GeneralHelper::StockAdjustmentDelete($request->input('item'), $code, $id);

        //This is for Maintain purchase inventory

        $builder = DB::table('tbl_invoice_item')->where('invoice_id', $id)->get();

        GeneralHelper::updatePurchaseInventoryForUpdate($builder);

        $builder = DB::table('tbl_invoice_item')->where('invoice_id', $id)->delete();

        GeneralHelper::updatePurchaseInventory($request->input('item'), 'save');

        //This is for Calculate Tax for Particulat product

        $taxt_item_delete = DB::table('tbl_invoice_item_tax')->where('invoice_id', '=', $id);

        $taxt_item_delete->delete();

         $customerID=$request->input('customer_id');
        $settings = DB::table('tbl_app_config')
                        ->select('*')->where('setting', '=', 'sales_product_list')->where('value', 'product based')->get()->first();
        if ($settings == "") {
            $tax_amount = 0;
            $discount_amount = 0;

            $product_with_taxlist = GeneralHelper::getProductTaxforPurchase($request->input('item'),$customerID);
            $tax_amount = 0;
            $discount_amount = 0;

            foreach ($product_with_taxlist as $item) {
                $invoiceitems = new Invoiceitem;
                $invoiceitems->fill($item);
                $invoiceitems->created_by = $currentuser->id;
                $invoiceitems->updated_by = $currentuser->id;
                $invoiceitems->invoice_id = $invoice->id;
                $invoiceitems->total_price = $item['qty'] * $item['unit_price'];
                $invoiceitems->is_active = $request->input('is_active', 1);
                $invoiceitems->customer_id = $request->input('customer_id');
                $invoiceitems->duedate = $invoice->duedate;
                $invoiceitems->paymentmethod = $invoice->paymentmethod;
                $invoiceitems->save();

                $product_discount_amount = GeneralHelper::calculateDiscount($item);
                $invoiceitems->discount_amount = $product_discount_amount;

                $remain_value = ($item['qty'] * $item['unit_price']) - $product_discount_amount;
                $item['produt_rem_amount'] = $remain_value;

                $product_tax_amount = GeneralHelper::insertProductTax($item, $invoice->id, $invoiceitems->id,'invoice');
                $sales_value = (($item['qty'] * $item['unit_price']) - $product_discount_amount) ;

                $discount_amount = $discount_amount + $product_discount_amount;
                $tax_amount = $tax_amount + $product_tax_amount;

                $invoiceitems->total_price=(($item['qty'] * $item['unit_price'])-$product_discount_amount);
                $invoiceitems->tax_amount = $product_tax_amount;
                $invoiceitems->sales_value = $sales_value;
                $invoiceitems->save();
            }
        } else {

            $product_with_taxlist = GeneralHelper::getProductTax($request->input('item'),$customerID);
            $tax_amount = 0;
            $discount_amount = 0;
            foreach ($product_with_taxlist as $item) {
                $invoiceitems = new Invoiceitem;
                $invoiceitems->fill($item);
                $invoiceitems->created_by = $currentuser->id;
                $invoiceitems->updated_by = $currentuser->id;
                $invoiceitems->invoice_id = $invoice->id;
                $invoiceitems->is_active = $request->input('is_active', 1);
                $invoiceitems->duedate = $invoice->duedate;
                $invoiceitems->paymentmethod = $invoice->paymentmethod;
                $invoiceitems->save();

                $product_discount_amount = GeneralHelper::calculateDiscount($item);
                $invoiceitems->discount_amount = $product_discount_amount;

                $remain_value = ($item['qty'] * $item['unit_price']) - $product_discount_amount;
                $item['produt_rem_amount'] = $remain_value;

                $product_tax_amount = GeneralHelper::insertProductTax($item, $invoice->id, $invoiceitems->id,'invoice');
                $sales_value = (($item['qty'] * $item['unit_price']) - $product_discount_amount) + $product_tax_amount;

                $discount_amount = $discount_amount + $product_discount_amount;
                $tax_amount = $tax_amount + $product_tax_amount;

                $invoiceitems->tax_amount = $product_tax_amount;
                $invoiceitems->sales_value = $sales_value;
                $invoiceitems->save();
            }
        }

        $invoiceItemCol = $request->input('item');
        foreach ($invoiceItemCol as $item) {
            $item['total_price'] = floatval($item['unit_price']) * floatval($item['qty']);

            $item['total_price'] = floatval(number_format($item['total_price'], 2, '.', ''));
            $subtotal = $subtotal + $item['total_price'];
        }


        $packing_charge = $request->input('packing_charge', 0.0);
        $insurance_charge = $request->input('insurance_charge', 0.0);

        //finding roundoff value 
        $total_before_roundoff = $subtotal - $discount_amount + $packing_charge + $insurance_charge;
        $total_after_roundoff = round($total_before_roundoff);
        $roundoff = $total_after_roundoff - $total_before_roundoff;
        $roundoff = floatval(number_format($roundoff, 2, '.', ''));

        //finding grant total value 
        $total = $subtotal - $discount_amount + $roundoff + $packing_charge + $insurance_charge;
        ;
        $total = floatval(number_format($total, 2, '.', ''));


        $invoice->updated_by = $currentuser->id;
        $input_request_data = $request->all();
        if ($input_request_data['tax_id'] == 'invoiced_tax_rate') {
            unset($input_request_data['tax_id']);
        }
        $invoice->fill($input_request_data);
        $invoice->subtotal = $subtotal;
        $invoice->tax_amount = $tax_amount;
        $invoice->discount_amount = $discount_amount;
        $invoice->round_off = $roundoff;
        $invoice->total_amount = $total;
        $invoice->invoice_no = $invoice_no;
        $invoice->invoice_code = $invoice_code;
        $invoice->save();



        $attributeCollection = DB::table('tbl_attribute_types')->where('code', 'invoice')->get();
        $attribute = $attributeCollection->first();


        if (count($attributeCollection) > 0) {


            $builder = DB::table('tbl_attribute_value')
                    ->where('attributetype_id', '=', $attribute->id)
                    ->where('refernce_id', '=', $id)
                    ->delete();
//print_r($builder);
        }
        if ($request->input('customattribute') != null) {
            $customerItemCol = ($request->input('customattribute'));

            $attribute = array();
            foreach ($customerItemCol as $item) {
                $attribute[$item['attribute_code']] = $item['value'];
            }
            $json_att = json_encode($attribute);
            $invoice->custom_attribute_json = $json_att;



            foreach ($customerItemCol as $attribute) {
//print_r($attribute);

                $customerattribute = new Attributevalue;

                $customerattribute->refernce_id = $invoice->id;
                $customerattribute->value = $attribute['value'];
                if (isset($attribute['sno'])) {
                    $customerattribute->sno = $attribute['sno'];
                }

                $customerattribute->attribute_code = $attribute['attribute_code'];

                if ($attribute['attribute_code'] != NULL) {


                    $attributeCollection = Attribute::where('attribute_code', "=", $attribute['attribute_code'])->get();

                    $attribute = $attributeCollection->first();
//print_r($attribute->id);
                    $customerattribute->attributes_id = $attribute->id;
                    $customerattribute->attributetype_id = $attribute->attributetype_id;
                }

                $customerattribute->updated_by = $currentuser->id;
                $customerattribute->save();
            }
        }

        $builder = DB::table('tbl_customer')
                        ->select('*')
                        ->where('id', '=', $invoice->customer_id)->first();
        $cus_name = $builder->fname . " " . $builder->lname;
        
        $voucher_number = $invoice->id;
                    $builder1 = DB::table('tbl_transaction')
                            ->select('*')
                            ->where('voucher_number', '=', $voucher_number)
                            ->where('voucher_type', '=', 'sales_invoice')
                            ->first();
                    
                    $id1 = $builder1->id;
 
        $mydate = $invoice->date;

        $transactionData = array(
            "id" => $id1,
            "name" => $currentuser->f_name,
            "transaction_date" => $mydate,
            "voucher_type" => Transaction::$SALES,
            "voucher_number" => $invoice->id,
            "credit" => 0.00,
            "debit" => $invoice->total_amount,
            "pmtcode" => GeneralHelper::getPaymentAcode($request->input('account_id', '')),
            "pmtcoderef" => $request->input('account_id', ''),
            "is_cash_flow" => 0, //have to check  account in save and voucher type too
            "account" => $request->input('account', ''),
            "acode" => GeneralHelper::getCustomerAcode($invoice->customer_id),
            "acoderef" => $invoice->customer_id,
            "customer_id" => $invoice->customer_id,
            "particulars" => 'Sales invoice ' . $invoice_code . ' to ' . $cus_name . ' by ' . $currentuser->f_name . ' ' . $currentuser->l_name,
            "created_by" => $currentuser->id,
            "updated_by" => $currentuser->id,
            "created_by_name" => $currentuser->f_name . ' ' . $currentuser->l_name,
            "updated_by_name" => $currentuser->f_name . ' ' . $currentuser->l_name,
            "is_active" => 1,
        );
        TransactionHelper::transactionSave($transactionData);

        $totalamount = $invoice->total_amount;

        $payment = DB::table('tbl_payment')->where('invoice_id', '=', $id)->where('is_active', '=', 1)->sum('amount');

        if ($payment == 0) {
            $invoice->status = 'unpaid';
        } else if ($totalamount <= $payment) {

            $invoice->status = 'paid';
        } else if ($totalamount >= $payment) {

            $invoice->status = 'partiallyPaid';
        }
        $invoice->paid_amount = $payment;
        $invoice->save();
        $deal_id = $request->input('deal_id', '');
        $deal_id = $request->input('deal_id', '');
        if ($deal_id != '') {
            $deal = new DealActivity;
            $get_contact_id = DB::table('tbl_customer')->where('is_active', 1)
                            ->where('id', '=', $request->customer_id)->first();
            $current_date = date('Y-m-d');
            $timestamp = date('H:i:s');
            $timestamp = strtotime($timestamp);
            $time = date('h:i:s');
            $time = strtotime($time);

            $deal->created_by = $currentuser->id;
            $deal->updated_by = $currentuser->id;
            $deal->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $deal->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $deal->time = $time;
            $deal->date = $current_date;
            $deal->type = 'invoice';
            $deal->description = "updated an invoice";
            $deal->contact_id = $get_contact_id->contact_id;
            $deal->user_id = $currentuser->id;
            $deal->user_name = $currentuser->f_name;
            $deal->is_active = 1;
            $deal->ref_id = $invoice->id;

            $deal->deal_id = $deal_id;
            $deal->comments = "updated an invoice";

            $deal->save();
        }
        ActivityLogHelper::invoiceModify($invoice);
        GeneralHelper::StockWastageSave($request->input('item'), "update", $id,$invoice->date);

//        GeneralHelper::InvoiceTaxUpdate($id, $request->input('tax_id'), $taxable_amount, $subtotal, 'sales');
        LogHelper::info1('Invoice Update' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Invoice', 'update', $screen_code, $invoice->id);

        return $resVal;
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $currentuser = Auth::user();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Invoice Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $invoice = Invoice::findOrFail($id);
            if ($invoice->is_active == 0) {
                return $resVal;
            }
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Invoice Not Found';
            return $resVal;
        }
        $paymentCollection = Payment::where('invoice_id', '=', $id)->where('is_active', '=', 1)->get();
        if ($paymentCollection->count() > 0) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Unable To Delete, Please Remove The Payment Transaction';
            return $resVal;
        }
        $returnCollect = InvoiceReturn::where('invoice_id', '=', $id)->where('is_active', '=', 1)->get();
        if ($returnCollect->count() > 0) {
            $resVal['success'] = FALSE;
             $resVal['message'] = "Invoice Was Returned So Can't Able To Delete";
            return $resVal;
        }
        
        //This is for Maintain purchase inventory

        $builder = DB::table('tbl_invoice_item')->where('invoice_id', $id)->get();

        GeneralHelper::updatePurchaseInventoryForUpdate($builder);


        DB::table('tbl_invoice_item')->where('invoice_id', '=', $id)->update(['is_active' => 0]);

        DB::table('tbl_invoice_item_tax')->where('invoice_id', '=', $id)->update(['is_active' => 0]);


        $invoice->is_active = 0;
        $invoice->updated_by = $currentuser->id;
//$invoice->fill($request->all());
        $invoice->update();
        /*
         * Code to adjust bank money and transaction table entry 
         */
//        $paymentCollection = Payment::where('invoice_id', '=', $id)->where('is_active', '=', 1)->get();
//        foreach ($paymentCollection as $payment) {
//
//            AccountHelper::withDraw($payment->account_id, $payment->amount);
//
////deavtivate the transaction
//            DB::table('tbl_transaction')
//                    ->where('voucher_type', '=', Transaction::$SALES_PAYMENT)
//                    ->where('voucher_number', '=', $payment->id)->update(['is_active' => 0]);
//        }
//
//
//        DB::table('tbl_payment')->where('invoice_id', '=', $id)->where('is_active', '=', 1)->update(['is_active' => 0]);
        $trans_id = DB::table('tbl_transaction')
                ->select('*')
                ->where('is_active','=',1)
                ->where('voucher_number', '=', $id)
                ->where('voucher_type', '=', Transaction::$SALES)
                ->first();
        $id2 = $trans_id->id;
//        $transaction = Transaction::findOrFail($id2);
//        $transaction->updated_by = $currentuser->id;
//        $transaction->is_active = 0;
//        $transaction->save();
        TransactionHelper::transactionDelete($id, Transaction::$SALES, 0);


        DB::table('tbl_payment')->where('invoice_id', '=', $id)->where('is_active', '=', 1)->update(['is_active' => 0]);
        $resVal['id'] = $invoice->id;
        ActivityLogHelper::invoiceDelete($invoice);
//$invoice->delete();
        $code = "idelete";
        GeneralHelper::StockAdjustmentDelete($request->input('item'), $code, $invoice->id);
        GeneralHelper::InvoiceTaxDelete($id, 'sales');

        $deal_id = $invoice->deal_id;
        if ($deal_id != 0) {
            $deal = new DealActivity;
            $get_contact_id = DB::table('tbl_customer')->where('is_active', 1)
                            ->where('id', '=', $request->customer_id)->first();
            $current_date = date('Y-m-d');
            $timestamp = date('H:i:s');
            $timestamp = strtotime($timestamp);
            $time = date('h:i:s');
            $time = strtotime($time);

            $deal->created_by = $currentuser->id;
            $deal->updated_by = $currentuser->id;
            $deal->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $deal->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $deal->time = $time;
            $deal->date = $current_date;
            $deal->type = 'invoice';
            $deal->description = "delete an invoice";
            $deal->contact_id = $get_contact_id->contact_id;
            $deal->user_id = $currentuser->id;
            $deal->user_name = $currentuser->f_name;
            $deal->is_active = 1;
            $deal->ref_id = $invoice->id;

            $deal->deal_id = $deal_id;
            $deal->comments = "delete an invoice";

            $deal->save();
        }
        $mail_obj = new MailHelper;
        $mail_obj->invoiceReminderNotification($id);
        LogHelper::info1('Invoice Delete' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Invoice', 'delete', $screen_code, $invoice->id);

        return $resVal;
    }

    public function invoiceReminder(Request $request, $id) {
        $get_pdf_format = DB::table("tbl_app_config")
                ->where('setting', '=', 'invoice_print_template')
                ->first();
        PdfHelper::pdfGenerate($id);
        SmsHelper::invoiceReminderNotification($id);


        $resVal['message'] = 'SMS Send Successfully';
        $resVal['success'] = TRUE;
        return $resVal;
    }

    public function salesTaxReport_csvexport(Request $request) {
        $salesTaxReportLists = $this->salesTaxReport($request);
        ini_set('max_execution_time', 600); //increase max_execution_time to 10 min if data set is very large
//create a file

        $filename = "salesTaxReport" . "_" . time() . ".csv";
//  $filename = "customer" . "_" . date("Y-m-d_H-i", time()) . ".csv";

        $serverName = $_SERVER['SERVER_NAME'];

        $filePath = "download/$serverName/csv";

        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);

            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {

                if ($folderName == '')
                    $folderName = $folderList[$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index];

                if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        $sr=1;
        $filePath = getcwd() . '/' . $filePath;
        $output = fopen($filePath . "/" . $filename, 'w');
        fputcsv($output, array('S No', 'Tax Name', 'Customer Name', 'City', 'Tax Percentage', 'Amount'));
        foreach ($salesTaxReportLists['list'] as $values) {
            fputcsv($output, array($sr, $values->tax_name, $values->fname, $values->billing_city, $values->tax_percentage, $values->tax_amount));
// id: 1, customer_id: 37, prefix: "test123r", date: "2017-05-02", duedate: "2017-05-02"
       $sr++;
            }
        LogHelper::info1('SalesTaxReport CSV Download' . $request->fullurl(), $request->all());
        return response()->download($filePath . "/" . $filename, 'salesTaxReport.csv');
    }

    public function salesTaxReport(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $from_Date = $request->input('from_Date');
        $to_Date = $request->input('to_Date');
        $tax_id = $request->input('tax_id', '');


        $builder = DB::table('tbl_invoice_item_tax as t')
                ->leftJoin('tbl_invoice as i', 'i.id', '=', 't.invoice_id')
                ->leftJoin('tbl_customer as c', 'i.customer_id', '=', 'c.id')
                ->select(DB::raw('i.id as invoice_id,i.invoice_code as invoice_code,t.tax_percentage,t.tax_name,t.tax_amount, i.date,c.fname,c.lname'),DB::raw("sum(t.tax_amount) as tax_amount"), DB::raw('CONCAT("Invoice", "#", i.invoice_code) AS particulare'), 'c.shopping_city', 'c.billing_city')
                ->whereDate('i.date', '>=', $from_Date)
                ->whereDate('i.date', '<=', $to_Date)
                ->where('i.is_active', '=', 1)
                ->groupBy('t.tax_id','i.customer_id')
                ->orderBy('i.id', 'asc')
                ->orderBy('t.tax_name', 'asc');
        if (!empty($tax_id)) {
            $builder->where('t.tax_id', '=', $tax_id);
        }

        $taxCollection = $builder->get();

        $resVal['list'] = $taxCollection;

        LogHelper::info1('SalesTaxReport' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }

    public function salesTaxReportSummery_csvexport(Request $request) {
        $salesTaxReportSummeryLists = $this->salesTaxReportSummery($request);
        $sr = 1;
        ini_set('max_execution_time', 600); //increase max_execution_time to 10 min if data set is very large
//create a file

        $filename = "salesTaxReportSummery" . "_" . time() . ".csv";
//  $filename = "customer" . "_" . date("Y-m-d_H-i", time()) . ".csv";

        $serverName = $_SERVER['SERVER_NAME'];

        $filePath = "download/$serverName/csv";

        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);

            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {

                if ($folderName == '')
                    $folderName = $folderList[$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index];

                if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        $filePath = getcwd() . '/' . $filePath;
        $output = fopen($filePath . "/" . $filename, 'w');
        fputcsv($output, array('S.No', 'Tax Name', 'Amount'));
        foreach ($salesTaxReportSummeryLists['list'] as $values) {
            fputcsv($output, array($sr, $values->tax_name, $values->amount));
// id: 1, customer_id: 37, prefix: "test123r", date: "2017-05-02", duedate: "2017-05-02"
            $sr++;
        }
        LogHelper::info1('SalesTaxReportSummary CSV Download' . $request->fullurl(), $request->all());
        return response()->download($filePath . "/" . $filename, 'salesTaxReportSummery.csv');
    }

    public function salesTaxReportSummery(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
//Get  Previous Date
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $from_Date = $request->input('from_Date');
        $to_Date = $request->input('to_Date');
        $tax_id = $request->input('tax_id');


        $builder = DB::table('tbl_invoice_item_tax as t ')
                ->leftJoin('tbl_invoice as i', 'i.id', '=', 't.invoice_id')
                ->select(DB::raw('t.tax_name,Coalesce(SUM(t.tax_amount),0) AS amount'))
                ->whereDate('i.date', '>=', $from_Date)
                ->whereDate('i.date', '<=', $to_Date)
                ->where('t.is_active', '=', 1)
                ->orderBy('i.date', 'asc')
                ->orderBy('t.tax_name', 'asc')
                ->groupBy('t.tax_id');


        if (!empty($tax_id)) {
            $builder->where('t.tax_id', '=', $tax_id);
        }
        $resVal['list'] = $builder->get();

        LogHelper::info1('SalesTaxReportSummary' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));

        return ($resVal);
    }

//Summary Display
    public function summary(Request $request) {


        $resVal = array();
        $resVal['message'] = "Success";
        $resVal['success'] = TRUE;
        
        
        $ldate = date('Y-m-d');
       
//Over due Date
        $bal_amount = DB::table('tbl_invoice')->where('duedate', "<", $ldate)
                ->whereColumn('paid_amount', "<", 'total_amount')
                ->where('is_active', "=", 1);
        $bal_amount1=InvoiceController::filters($bal_amount,$request);
        $count=$bal_amount1->get();        

        $overDueVal = array();
        $overDueVal['amount'] = $bal_amount1->sum(DB::raw('total_amount - paid_amount'));
        $overDueVal['invoice_count'] =count($count);
        $resVal['overdue'] = $overDueVal;

//Today Due Date                   

        $bal_amount = DB::table('tbl_invoice')->where('duedate', "=", $ldate)
                ->whereColumn('paid_amount', "<", 'total_amount')
                ->where('is_active', "=", 1);
        $bal_amount1=InvoiceController::filters($bal_amount,$request);
        $count=$bal_amount1->get();
        $todaydueval = array();
        $todaydueval['amount'] = $bal_amount1->sum(DB::raw('total_amount - paid_amount'));
        $todaydueval['invoice_count'] = count($count);
        $resVal['todate'] = $todaydueval;

//Next 30 Days Due Date
        $ndate = date('Y-m-d', strtotime("+30 days"));         
        $bal_amount = DB::table('tbl_invoice')->whereBetween('duedate', array($ldate, $ndate))
                ->whereColumn('paid_amount', "<", 'total_amount')
                ->where('is_active', "=", 1);
        $bal_amount1=InvoiceController::filters($bal_amount,$request);
        $count=$bal_amount1->get();          

        $nextDueVal = array();
        $nextDueVal['amount'] = $bal_amount1->sum(DB::raw('total_amount - paid_amount'));
        $nextDueVal['invoice_count'] = count($count);

        $resVal['nextdate'] = $nextDueVal;
        LogHelper::info1('Invoice Summary' . $request->fullurl(), $resVal['nextdate']);
        return $resVal;
    }
    public static function filters($bal_amount,$request) {
        
        $id = $request->input('id');
        $customerid = $request->input('customer_id');
        $invoice_code = $request->input('invoice_code');
        $fromDate = $request->input('from_duedate', '');
        $toDate = $request->input('to_duedate', '');
        $isactive = $request->input('is_active', '');
        $status = $request->input('status');
        $fromdate = $request->input('from_date');
        $todate = $request->input('to_date');
        $notes = $request->input('notes');
        $invoiceDate = $request->input('invoice_date', '');
        $dueDate = $request->input('due_date', '');
        $noPrefix = $request->input('noPrefix');
        $discountpercentage = $request->input('discount_percentage');
        $deal_id = $request->input('deal_id', '');
        $prefix = $request->input('prefix', '');
         if (!empty($status)) {
            $bal_amount->where('status', '=', $status);
        }
        if (!empty($invoice_code)) {
            $bal_amount->where('invoice_code', 'like', '%' . $invoice_code . '%');
        }
        if (!empty($discountpercentage)) {
            $bal_amount->where('discount_percentage', '=', $discountpercentage);
        }
        if (!empty($customerid)) {
            $bal_amount->where('customer_id', '=', $customerid);
        }
        if (!empty($invoiceDate)) {
            $bal_amount->whereDate('date', '=', $invoiceDate);
        }
        if (!empty($dueDate)) {
            $bal_amount->whereDate('duedate', '=', $dueDate);
        }
        if (!empty($id)) {
            $bal_amount->where('id', '=', $id);
        }
        if (!empty($prefix) || !empty($noPrefix)) {
            $prefix1 = explode(',', $prefix);
            $result = "'" . implode("', '", $prefix1) . "'";
            $bal_amount->whereRaw(DB::raw("((Case When '" . $prefix . "' <> '' Then prefix in ($result) End) or (Case When '" . $noPrefix . "' = -1 Then prefix='' End)) "));
        }
        if (!empty($fromDate)) {

            $bal_amount->whereDate('duedate', '>=', $fromDate);
        }
        if (!empty($toDate)) {

            $bal_amount->whereDate('duedate', '<=', $toDate);
        }
        if (!empty($fromdate)) {

            $bal_amount->whereDate('date', '>=', $fromdate);
        }
        if (!empty($todate)) {

            $bal_amount->whereDate('date', '<=', $todate);
        }
        if (!empty($notes)) {
            $bal_amount->where('notes', '=', $notes);
        }

        if ($isactive != '') {
            $bal_amount->where('is_active', '=', $isactive);
        }
        if ($deal_id != '') {
            $bal_amount->where('deal_id', '=', $deal_id);
        }
  
        return $bal_amount;
        
    }
    public function receivablereport_csvexport(Request $request) {
        $receivablereportLists = $this->receivable_list($request);
        ini_set('max_execution_time', 600); //increase max_execution_time to 10 min if data set is very large
//create a file
        $filename = "receivablereport" . "_" . time() . ".csv";
//  $filename = "customer" . "_" . date("Y-m-d_H-i", time()) . ".csv";
        $serverName = $_SERVER['SERVER_NAME'];

        $filePath = "download/$serverName/csv";

        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);

            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {

                if ($folderName == '')
                    $folderName = $folderList[$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index];

                if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        $filePath = getcwd() . '/' . $filePath;

        $sr = 1;
        $output = fopen($filePath . "/" . $filename, 'w');
        fputcsv($output, array('#', 'Customer Name', 'City', 'Mobile', 'Email', 'Qty', 'Amount'));
        foreach ($receivablereportLists['list'] as $values) {
             $ph=explode(',',$values->phone);
                 $phNo=implode(', ',$ph);
            fputcsv($output, array($sr, $values->customer_fname, $values->billing_city, $phNo, $values->email, $values->qty, $values->balance));
            $sr++;
        }
        LogHelper::info1('ReceivableReport CSV Download' . $request->fullurl(), $request->all());
        return response()->download($filePath . "/" . $filename, 'receivablereport.csv');
    }

    public function receivable_list(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $customerid = $request->input('customer_id');
        $fromdate = $request->input('from_date');
        $todate = $request->input('to_date');
        $currentDate = date('Y-m-d');

        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        $builder = DB::table('tbl_invoice as i')
                ->leftjoin(DB::raw("(select Coalesce(sum(it.qty),0) as qty,it.invoice_id  from tbl_invoice_item as it left join tbl_invoice as i on it.invoice_id =i.id and i.is_active=1 where date(`i`.`date`) >= '" . $fromdate . "' and date(`i`.`date`) <= '" . $todate . "' and i.status != 'paid' group by i.customer_id) as its on its.invoice_id=i.id"), function($join) {
                    
                })
                ->leftJoin('tbl_customer as c', 'i.customer_id', '=', 'c.id')
                ->leftJoin('tbl_tax as t', 'i.tax_id', '=', 't.id')
                ->select('c.id', 'c.fname as customer_fname', 'c.customer_code', 'c.lname as customer_lname', 'c.company', 'c.phone', 'c.email', 'c.billing_address', 'c.shopping_address', 'c.billing_city', 'c.shopping_city', 'c.shopping_state', 'c.billing_state', 'c.billing_country', 'c.shopping_pincode', 'c.billing_pincode', 'c.billing_country', 'c.is_sale', 'c.is_purchase', 'i.id as invoice_id', DB::raw('Coalesce(sum(i.total_amount) - sum(i.paid_amount),0) as balance'), 'its.qty')
                ->where('i.status', '!=', 'paid')
                ->where('i.is_active', '=', '1')
                ->groupBy('c.id')
                        ;


        if (!empty($customerid)) {
            $builder->where('i.customer_id', '=', $customerid);
        }

        if (!empty($fromdate)) {

            $builder->whereDate('i.date', '>=', $fromdate);
        }
        if (!empty($todate)) {

            $builder->whereDate('i.date', '<=', $todate);
        }

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {

            $invoiceCollection = $builder->get();
        } else {

            $invoiceCollection = $builder->skip($start)->take($limit)->get();
        }

        foreach ($invoiceCollection as $collect) {
            $cust_id = $collect->id;

            $detail = DB::table('tbl_invoice as i')
                    ->leftjoin(DB::raw("(select sum(it.qty) as qty,it.invoice_id  from tbl_invoice_item as it left join tbl_invoice as i on i.id=it.invoice_id and i.is_active=1 where date(`i`.`date`) >= '" . $fromdate . "' and date(`i`.`date`) <= '" . $todate . "' and i.status != 'paid' and i.customer_id='" . $cust_id . "' group by i.id) as it on it.invoice_id=i.id")
                            , function($join) {
                        
                    })
                    ->select('i.*', DB::raw('Coalesce(i.total_amount-i.paid_amount,0)  as balance'), DB::raw("(Case When datediff(i.duedate,'" . $currentDate . "') < 0 Then datediff(i.duedate,'" . $currentDate . "') else '' End) as diff_date"), 'it.qty')
                    ->where('i.status', '!=', 'paid')
                    ->where('i.is_active', '=', 1);

            if (!empty($cust_id)) {
                $detail->where('i.customer_id', '=', $cust_id);
            }

            if (!empty($fromdate)) {

                $detail->whereDate('i.date', '>=', $fromdate);
            }
            if (!empty($todate)) {

                $detail->whereDate('i.date', '<=', $todate);
            }


            $collect->details = $detail->get();
        }

        /* 9 foreach ($invoiceCollection as $invoice) {
          $json_att = $invoice->custom_attribute_json;

          if (empty($json_att)) {
          $json_att = "{}";
          }
          $json_att = json_decode($json_att);

          $invoice->custom_attribute_json = $json_att;
          } */

        $resVal['list'] = $invoiceCollection;
        LogHelper::info1('receivable_list ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }

    public function invoiceMailSend(Request $request) {
        $resVal['message'] = 'Mail Send Successfully';
        $resVal['success'] = TRUE;

        $invoice = $request->invoice_id;
        if ($request->attahed_pdf_copy == 1) {

            $get_pdf_format = DB::table("tbl_app_config")
                    ->where('setting', '=', 'invoice_print_template')
                    ->first();
            if ($get_pdf_format->value == 't1')
                PdfHelper::invoicePrintTemplate1($invoice);
            else if ($get_pdf_format->value == 't2')
                PdfHelper::invoicePrintTemplate2($invoice);
            else if ($get_pdf_format->value == 't3')
                PdfHelper::invoicePrintTemplate3($invoice);
            else if ($get_pdf_format->value == 't4')
                PdfHelper::invoicePrintTemplate4($invoice);
        }
        $mail_obj = new MailHelper;
        $mail_obj->invoiceMailSend($request);
        LogHelper::info1('Invoice Mail Send ' . $request->fullurl(), $request->all());
        return $resVal;
    }

    public function customerdetails(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $customer_id = $request->input('customer_id');



        $total_amount = DB::table('tbl_invoice')
                ->where('customer_id', '=', $customer_id)
                ->where('is_active', '=', 1)
                ->sum('total_amount');

        $count = DB::table('tbl_invoice')
                ->where('customer_id', '=', $customer_id)
                ->where('is_active', '=', 1)
                ->count('customer_id');

        $date = DB::table('tbl_invoice')
                ->where('customer_id', '=', $customer_id)
                ->where('is_active', '=', 1)
                ->max('date');

        $customer = array();
        $customer['total_amount'] = $total_amount;
        $customer['billing_count'] = $count;
        $customer['last_date_of_billing'] = $date;
        $resVal['list'] = $customer;
        LogHelper::info1('Invoice CustomerDetails ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return $resVal;
    }

    public function pdfPreview(Request $request, $id) {

        $get_pdf_format = DB::table("tbl_app_config")
                ->where('setting', '=', 'invoice_print_template')
                ->first();
        PdfHelper::pdfPreview($id);

        $resVal['message'] = 'Pdf Download Successfully';
        $resVal['success'] = TRUE;
        return $resVal;
    }

}

?>
