<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use DB;
use App\InvoiceReturn;
use App\InvoiceReturnItem;
use App\Helper\AuthorizationHelper;
use App\Helper\GeneralHelper;
use App\AdvancePayment;
use App\Transaction;
use App\Helper\AccountHelper;
use App\Helper\SalesInvoiceHelper;
use App\Helper\LogHelper;
use App\Helper\NotificationHelper;
use App\Helper\TransactionHelper;
use App\Helper\ReplaceHolderHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of invoicesController
 *
 * @author Deepa
 */
class InvoiceReturnController extends Controller {

//put your code here
    public function Save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'InvoiceReturn Added Successfully';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        
        $subtotal = 0;
        $discount_amount = 0;
        $roundoff = 0;
        $tax_id = 0;
        $tax_amount = 0;
        $taxable_amount = 0;
        $total_before_roundoff = 0;
        $total = 0;

        $invoiceItemCol = $request->input('item');
        $currentuser = Auth::user();
        $invoicereturn = new InvoiceReturn;
        $invoicereturn->fill($request->all());

        $invoicereturn->created_by = $currentuser->id;
        $invoicereturn->updated_by = $currentuser->id;
        $invoicereturn->is_active = $request->input('is_active', 1);
        //Finding DayCode
        $dayLog = DB::table('tbl_day_log')->select('*')
                        ->whereRaw('id = (select max(`id`) from tbl_day_log)')->first();
        if ($dayLog != "")
            $invoicereturn->day_code = $dayLog->id;
        $invoicereturn->save();

        //This is for Calculate Tax for Particulat product
        $product_with_taxlist = $request->input('item');

        foreach ($product_with_taxlist as $item) {

            $invoiceReturnItems = new InvoiceReturnItem;
            $invoiceReturnItems->fill($item);
            $invoiceReturnItems->created_by = $currentuser->id;
            $invoiceReturnItems->updated_by = $currentuser->id;
            $invoiceReturnItems->invoice_return_id = $invoicereturn->id;
            $invoiceReturnItems->invoice_id = $invoicereturn->invoice_id;
            $invoiceReturnItems->is_active = 1;
            $invoiceReturnItems->customer_id = $invoicereturn->customer_id;
            $invoiceReturnItems->discount_mode = $item['discount_mode'];
            if ($invoiceReturnItems->discount_mode == '%') {
                $invoiceReturnItems->discount_value = $item['discount_value'];
                $invoiceReturnItems->discount_amount = $item['qty'] * $item['unit_price'] * ($item['discount_value'] / 100);
            } else {
                $invoiceReturnItems->discount_value = 0;
                if(isset($item['discount_amount']))
                $invoiceReturnItems->discount_amount = $item['discount_amount'];
            }
            $invoiceReturnItems->tax_id = $item['tax_id'];
            $invoiceReturnItems->tax_percentage = $item['tax_percentage'];

            $rowTot=$item['qty'] * $item['unit_price'];
            $totalPrice=($rowTot-$invoiceReturnItems->discount_amount);
           $subtotal += $item['qty'] * $item['unit_price'];
            if (!empty($invoiceReturnItems->tax_percentage)) {
                $invoiceReturnItems->tax_amount = ($totalPrice) - ($totalPrice / (1 +$invoiceReturnItems->tax_percentage  / 100));
            } else {
                $invoiceReturnItems->tax_amount = 0;
            }
            $invoiceReturnItems->sales_value = $rowTot - $invoiceReturnItems->discount_amount;
            $invoiceReturnItems->save();
            $invoiceReturnItems->total_price=$totalPrice;
            $invoiceReturnItems->save();
            
//            $subtotal += $invoiceReturnItems->total_price;
            $discount_amount += $invoiceReturnItems->discount_amount;
            $tax_amount += $invoiceReturnItems->tax_amount;
            
        }


        //finding roundoff value 
        $total_before_roundoff = $subtotal - $discount_amount;
        $total_after_roundoff = round($total_before_roundoff);
        $roundoff = $total_after_roundoff - $total_before_roundoff;
        $roundoff = floatval(number_format($roundoff, 2, '.', ''));

        //finding grant total value 
        $total = $subtotal - $discount_amount  + $roundoff;
        $total = floatval(number_format($total, 2, '.', ''));

//        $invoicereturn->subtotal = $subtotal;
        $invoicereturn->tax_amount = $tax_amount;
        $invoicereturn->discount_amount = $discount_amount;
        $invoicereturn->round_off = $roundoff;
        $invoicereturn->total_amount = $total;
        $invoicereturn->save();

        $resVal['id'] = $invoicereturn->id;
        GeneralHelper::StockAdjustmentSave($request->input('item'), "invoice_return", $invoicereturn->id,$invoicereturn->date);
        //This is for Maintain purchase inventory
        GeneralHelper::updatePurchaseInventoryForUpdate($request->input('item'));
   
        //Create Payment Receipt invoice return
        $this->generatePaymentForInvoiceReturn($invoicereturn);
        LogHelper::info1('InvoiceReturn Save ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'InvoiceReturn', 'save', $screen_code, $invoicereturn->id);
        return $resVal;
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $currentuser = Auth::user();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'InvoiceReturn Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $invoicereturn = InvoiceReturn::findOrFail($id);
            if ($invoicereturn->is_active == 0) {
                return $resVal;
            }
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'InvoiceReturn Not Found';
            return $resVal;
        }
        
        $paymentCollection = AdvancePayment::where('voucher_no', '=', $id)
                ->where('voucher_type','=','advance_invoice')
                ->where('used_amount','!=',0)
                ->where('is_active', '=', 1)
                ->get();
        
        if ($paymentCollection->count() > 0) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Unable To Delete, Return Amount Used';
            return $resVal;
        }
        

        //This is for Maintain purchase inventory
        $invoiceReturnItemCollection = DB::table('tbl_invoice_return_item')->where('invoice_return_id', $id)->get();

        GeneralHelper::StockAdjustmentDelete($invoiceReturnItemCollection, "invoice_return_delete", $invoicereturn->id);

        GeneralHelper::updatePurchaseInventory($invoiceReturnItemCollection);

        DB::table('tbl_invoice_return_item')->where('invoice_return_id', '=', $id)->update(['is_active' => 0]);

        $invoicereturn->is_active = 0;
        $invoicereturn->updated_by = $currentuser->id;
        $invoicereturn->update();
        LogHelper::info1('InvoiceReturn Delete ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'InvoiceReturn', 'delete', $screen_code, $invoicereturn->id);
        return $resVal;
    }

    public function listAll(Request $request) {
        $resVal = array();
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $id = $request->input('id');
        $customerid = $request->input('customer_id');
        $mobile = $request->input('mobile', '');
        $invoice_id = $request->input('invoice_id');
        $isactive = $request->input('is_active', '');
        $fromdate = $request->input('from_date');
        $todate = $request->input('to_date');
        $mode = $request->input('mode', 0);
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        $builder = DB::table('tbl_invoice_return as i')
                ->leftJoin('tbl_customer as c', 'i.customer_id', '=', 'c.id')
                ->select('i.*', 'c.fname as customer_fname', 'c.lname as customer_lname');

        if (!empty($mobile)) {
            $builder->where('c.phone', '=', $mobile);
        }
        if (!empty($invoice_id)) {
            $builder->where('i.invoice_id', $invoice_id);
        }
        if (!empty($customerid)) {
            $builder->where('i.customer_id', '=', $customerid);
        }
        if (!empty($id)) {
            $builder->where('i.id', '=', $id);
        }
        if (!empty($fromdate)) {

            $builder->whereDate('i.date', '>=', $fromdate);
        }
        if (!empty($todate)) {

            $builder->whereDate('i.date', '<=', $todate);
        }

        if ($isactive != '') {
            $builder->where('i.is_active', '=', $isactive);
        }
        if ($mode == 0 || $mode == '') {
            $builder->orderBy('i.id', 'desc');
        } else if ($mode == 1) {
            $builder->orderBy('i.date', 'asc');
        }

        $resVal['total'] = $builder->count();

        if ($start == 0 && $limit == 0) {
            $invoiceCollection = $builder->get();
        } else {

            $invoiceCollection = $builder->skip($start)->take($limit)->get();
        }
        $resVal['list'] = $invoiceCollection;
        LogHelper::info1('InvoiceReturn List All ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }

    public function detail(Request $request) {

        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $id = $request->input('id');

        $isactive = $request->input('isactive', '');
        $decimalFormat= GeneralHelper::decimalFormat();

        $builder = DB::table('tbl_invoice_return as i')
                ->leftJoin('tbl_user as cu', 'cu.id', '=', 'i.created_by')
                ->leftJoin('tbl_user as u', 'u.id', '=', 'i.updated_by')
                ->leftJoin('tbl_customer as c', 'i.customer_id', '=', 'c.id')
                ->select('i.id', 'i.customer_id', 'i.date', 'i.subtotal', 'i.discount_amount', 'i.tax_amount', 'i.total_amount', 'i.status'
                , 'i.notes', 'i.is_active', 'i.created_by', 'i.updated_by', 'i.updated_at', 'i.created_at'
                , 'i.customer_address', 'i.round_off', 'c.fname as customer_fname', 'c.lname as customer_lname', 'c.phone', 'c.email'
                , 'cu.f_name as created_byfname', 'cu.l_name as created_bylname', 'u.f_name as updated_byfname', 'u.l_name as updated_bylame');

        $invoiceReturnItems = DB::table('tbl_invoice_return_item')
                ->select('*',DB::raw("cast(qty as DECIMAL(12,".$decimalFormat.")) as qty"))
                ->where('invoice_return_id', $id);

        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        if (!empty($id)) {
            $builder->where('i.id', '=', $id);
        }

        if ($isactive != '') {
            $builder->where('i.is_active', '=', $isactive);
        }
        $builder->orderBy('i.id', 'desc');

        $invoiceCollection = $builder->skip($start)->take($limit)->get();
        if (count($invoiceCollection) == 1) {

            $invoice = $invoiceCollection->first();

            $invoice->item = $invoiceReturnItems->get();

            $resVal['data'] = $invoice;
        }
        else 
        {
            $resVal['data'] = null;
        }
        LogHelper::info1('InvoiceReturn Detail ' . $request->fullurl(),$request->all());
        return ($resVal);
    }

    private function generatePaymentForInvoiceReturn($invoiceReturn) {

        $pos_account = DB::table('tbl_app_config')->where('setting', 'pos_bank_account_id')->first();
        $pos_account_name = '';
        if (!empty($pos_account)) {
            $pos_account_name = DB::table('tbl_accounts')->where('id', $pos_account->value)->first();
        }
        if (empty($pos_account_name) || empty($pos_account)) {
            return;
        }

        $mode=GeneralHelper::get_mode();
        $currentuser = Auth::user();
        $advance_payment = new AdvancePayment;
        $advance_payment->created_by = $currentuser->id;
        $advance_payment->updated_by = $currentuser->id;

        $advance_payment->is_active = 1;
        $advance_payment->customer_id = $invoiceReturn->customer_id;
        $advance_payment->comments = 'Payment Receipt created for invoice return';
        $advance_payment->date = $invoiceReturn->date;
        $advance_payment->amount = $invoiceReturn->total_amount;
        $advance_payment->used_amount = 0;
        $advance_payment->balance_amount = $invoiceReturn->total_amount;
        $advance_payment->payment_mode = $mode;
        $advance_payment->reference_no = '';
        $advance_payment->status = 'available';
        $advance_payment->tra_category = '';
        $advance_payment->voucher_type = 'advance_invoice';
        $advance_payment->voucher_no = $invoiceReturn->id;
//        $advance_payment->account = $pos_account_name->account;
        $advance_payment->account_id = 0;
        $advance_payment->save();

       
        $mydate = $advance_payment->date;
        
        $currency= ReplaceHolderHelper::getCurrencySymbol();
        $cusName= GeneralHelper::getCustName($invoiceReturn->customer_id);
        $incParticular=$currency.'. '.$invoiceReturn->total_amount." Added as an Advance Amount from ".$cusName." For the Invoice Return #".$invoiceReturn->id;
        $exParticular="Creating ".$currency.'. '.$invoiceReturn->total_amount." as an Advance Amount For the Customer ".$cusName;
        
        
//         $transactionData1 = array(
//            "name" => $currentuser->f_name,
//            "transaction_date" => $mydate,
//            "voucher_type" => 'invoice_return',
//            "voucher_number" => $invoiceReturn->id,
//            "credit" => 0.00,
//            "debit" => $advance_payment->amount,
//            "pmtcode" => NULL,
//            "pmtcoderef" => $advance_payment->account_id,
//            "is_cash_flow" => 1, //have to check  account in save and voucher type too
//            "account" => $advance_payment->account,
//            "acode" => GeneralHelper::getCustomerAcode($advance_payment->customer_id),
//            "acoderef" => $advance_payment->customer_id,
//            "customer_id" => $advance_payment->customer_id,
//            "account_category" => $advance_payment->tra_category,
//            "account_id" => $advance_payment->account_id,
//            "payment_mode" => $advance_payment->payment_mode,
//            "particulars" => $incParticular,
//            "created_by" => $currentuser->id,
//            "updated_by" => $currentuser->id,
//            "created_by_name" => $currentuser->f_name . ' ' . $currentuser->l_name,
//            "updated_by_name" => $currentuser->f_name . ' ' . $currentuser->l_name,
//            "is_active" => 1,
//        );
//        TransactionHelper::transactionSave($transactionData1);
//        
//          AccountHelper::withDraw($advance_payment->account_id, $advance_payment->amount); // Its For Invoice Return
//
//
//        $transactionData = array(
//            "name" => $currentuser->f_name,
//            "transaction_date" => $mydate,
//            "voucher_type" => $advance_payment->voucher_type,
//            "voucher_number" => $advance_payment->id,
//            "credit" => $advance_payment->amount,
//            "debit" => 0.00,
//            "pmtcode" => NULL,
//            "pmtcoderef" => $advance_payment->account_id,
//            "is_cash_flow" => 1, //have to check  account in save and voucher type too
//            "account" => $advance_payment->account,
//            "acode" => GeneralHelper::getCustomerAcode($advance_payment->customer_id),
//            "acoderef" => $advance_payment->customer_id,
//            "customer_id" => $advance_payment->customer_id,
//            "account_category" => $advance_payment->tra_category,
//            "account_id" => $advance_payment->account_id,
//            "payment_mode" => $advance_payment->payment_mode,
//            "particulars" => $exParticular,
//            "created_by" => $currentuser->id,
//            "updated_by" => $currentuser->id,
//            "created_by_name" => $currentuser->f_name . ' ' . $currentuser->l_name,
//            "updated_by_name" => $currentuser->f_name . ' ' . $currentuser->l_name,
//            "is_active" => 1,
//        );
//        TransactionHelper::transactionSave($transactionData);
//        
//
//        //Update Account Balance
//        AccountHelper::deposit($advance_payment->account_id, $advance_payment->amount); // Its For Invoice Advance
        //Update advance amount balance amount
        SalesInvoiceHelper::updateAdvPaymentStatusAndBalance($advance_payment->id);
    }

}
?>
