<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\Notification;
use Illuminate\Http\Request;
use App\Transformer\UomTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\LogHelper;
use App\Helper\NotificationHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UomController
 *
 * @author Deepa
 */
class NotificationController extends Controller {

    //put your code here


    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Notification Updated Successfully';

        $currentuser = Auth::user();
        try {
            $noti = Notification::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Notification Not Found';
            return $resVal;
        }
        $noti->read_or_not = 1;
        $userId = $noti->user_id;

        DB::table('tbl_notification')->where('read_or_not', 0)
                ->where('user_id', $userId)
                ->where('id', '<', $id)
                ->update(['read_or_not' => 1]);

        $noti->save();
        LogHelper::info1('Notification Update ' . $request->fullurl(), $request->all());
        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $user_id = $request->input('user_id', '');
        $read_or_not = $request->input('read_or_not', '');
        $type = $request->input('type', '');
        $is_active = $request->input('is_active', '');

        //$status = $request->input('status', '');
        $builder = DB::table('tbl_notification')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if ($is_active != '') {
            $builder->where('is_active', '=', $is_active);
        }

        if (!empty($user_id)) {
            $builder->where('user_id', '=', $user_id);
        }

        if (!empty($read_or_not)) {
            $builder->where('read_or_not', '=', $read_or_not);
        }

        if (!empty($type)) {
            $builder->where('type', '=', $type);
        }


        $builder->orderBy('id', 'desc');
        
        $resVal['total'] = $builder->count();
        
        if($start==0 && $limit==0){
             $collection=$builder->get();
        }else{
             $collection=$builder->skip($start)->take($limit)->get();
        }
        
        $resVal['list'] = $collection;

        LogHelper::info1('Notification List All ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));

        return ($resVal);
    }

    public function count(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        $resVal['message'] = "Success";
        $resVal['success'] = True;
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $user_id = $request->input('user_id', '');
        $read_or_not = $request->input('read_or_not', '');

        //$status = $request->input('status', '');
        $builder = DB::table('tbl_notification')
                ->select('*')
                ->where('is_active', '=', 1)
                ->where('read_or_not', '=', $read_or_not);
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);


        if (!empty($user_id)) {
            $builder->where('user_id', '=', $user_id);
        }

      


        $resVal['count'] = $builder->count();

        LogHelper::info1('Notification Count ' . $request->fullurl(), $request->all());

        return ($resVal);
    }

}

?>
