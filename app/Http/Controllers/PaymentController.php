<?php

namespace App\Http\Controllers;

use DB;
use App\Transaction;
use App\Invoice;
use App\PurchasePayment;
use App\Quote;
use App\SalesOrder;
use Carbon\Carbon;
use App\Helper\AccountHelper;
use App\ActivityLog;
use App\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\SmsHelper;
use App\Helper\MailHelper;
use App\Helper\AuthorizationHelper;
use App\Helper\ActivityLogHelper;
use App\Helper\SalesInvoiceHelper;
use App\Helper\LogHelper;
use App\Helper\NotificationHelper;
use App\Helper\GeneralHelper;
use App\Helper\TransactionHelper;
use App\AdvancePayment;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PaymentController
 *
 * @author Deepa
 */
class PaymentController extends Controller {

    //put your code here
    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Payment Added Successfully';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();


        $payment = new Payment;

        $payment->created_by = $currentuser->id;
        $payment->updated_by = $currentuser->id;

        $payment->fill($request->all());

        //Finding DayCode
        $dayLog = DB::table('tbl_day_log')->select('*')
                        ->whereRaw('id = (select max(`id`) from tbl_day_log)')->first();
        if ($dayLog != "")
            $payment->day_code = $dayLog->id;

        $payment->save();
        $cus_det = $payment->customer_id;
        $builder = DB::table('tbl_customer')
                        ->select('*')
                        ->where('id', '=', $cus_det)->first();
        $cus_name = $builder->fname . " " . $builder->lname;

        $builder = DB::table('tbl_user')
                        ->select('*')
                        ->where('id', '=', $currentuser->id)->first();
        $user_name = $builder->username;
        if ($request->input('invoice_id') != NULL && !empty($request->input('invoice_id'))) {
            $builder = DB::table('tbl_invoice')
                            ->select('*')
                            ->where('id', '=', $payment->invoice_id)->first();
            $invoice_no = $builder->invoice_no;
            $invoice_code = $builder->invoice_code;
        }
        if ($request->input('invoice_id') != NULL && !empty($request->input('invoice_id'))) {
            $paidamount = DB::table('tbl_payment')
                    ->where('invoice_id', '=', $request->input('invoice_id'))
                    ->where('is_active', '=', 1)
                    ->sum('amount');
            $invoiceCollection = Invoice::where('id', "=", $request->input('invoice_id'))->get();
            $collection = $invoiceCollection->first();
        }
        if (empty($payment->adv_payment_id)) {
            $account_id = $payment->account_id;
                AccountHelper::deposit($account_id, $payment->amount);
            $mydate = $payment->date;



            if ($request->input('quote_id') != NULL && !empty($request->input('quote_id')))
                $particulars = '#' . $request->input('quote_id') . 'Sales invoice Quote Payment has Received from ' . $cus_name . ' by ' . $user_name . ' Payment Mode - ' . $payment->payment_mode;
            else if ($request->input('sales_order_id') != NULL && !empty($request->input('sales_order_id')))
                $particulars = '#' . $request->input('Sales_order_id') . 'Sales invoice Sales Order Payment has Received from ' . $cus_name . ' by ' . $user_name . ' Payment Mode - ' . $payment->payment_mode;
            else
                $particulars = '#' . $invoice_code . 'Sales invoice Payment has Received from ' . $cus_name . ' by ' . $user_name . ' Payment Mode - ' . $payment->payment_mode;
            if (!empty($payment->cheque_no))
                $particulars = $particulars . ' Cheque No -' . $payment->cheque_no;

            $transactionData = array(
                "name" => $currentuser->f_name,
                "transaction_date" => $mydate,
                "voucher_type" => Transaction::$SALES_PAYMENT,
                "voucher_number" => $payment->id,
                "credit" => $payment->amount,
                "debit" => 0.00,
                "pmtcode" => GeneralHelper::getPaymentAcode($request->input('account_id', '')),
                "pmtcoderef" => $request->input('account_id', ''),
                "is_cash_flow" => 1, //have to check  account in save and voucher type too
                "account" => $request->input('account', ''),
                "acode" => GeneralHelper::getCustomerAcode($payment->customer_id),
                "account_id" => $payment->account_id,
                "acoderef" => $payment->customer_id,
                "customer_id" => $payment->customer_id,
                "account_type" => "",
                "particulars" => $particulars,
                "account_category" => $payment->tra_category,
                "payment_mode" => $payment->payment_mode,
                "cheque_no" => $payment->cheque_no,
                "created_by" => $currentuser->id,
                "updated_by" => $currentuser->id,
                "created_by_name" => $currentuser->f_name . ' ' . $currentuser->l_name,
                "updated_by_name" => $currentuser->f_name . ' ' . $currentuser->l_name,
                "is_active" => 1,
            );
            $transaction = TransactionHelper::transactionSave($transactionData);
            
            
        }else {
            SalesInvoiceHelper::updateAdvPaymentStatusAndBalance($payment->adv_payment_id);
        }
        SalesInvoiceHelper::updateInvoiceStatus($payment->invoice_id);
        //Activity log
        ActivityLogHelper::invoicePaymentSave($collection, $payment);
        
         if (empty($request->input('adv_payment_id','')) || $request->input('adv_payment_id','') == 0) {
                        $advance_payment = new AdvancePayment;
                        $advance_payment->created_by = $currentuser->id;
                        $advance_payment->updated_by = $currentuser->id;
                        $advance_payment->date = $request->date;
                        $advance_payment->amount = $request->amount;
                        $advance_payment->used_amount = $request->amount;
                        $advance_payment->voucher_type = 'direct_sales_payment';
                        $advance_payment->voucher_no = $payment->id;
                        $advance_payment->customer_id = $request->customer_id;
                        $advance_payment->comments = $request->input('comments');
                        $advance_payment->account = $request->account;
                        $advance_payment->account_id = $request->account_id;
                        $advance_payment->payment_mode = $request->payment_mode;
                        $advance_payment->tra_category = $request->tra_category;
                        $advance_payment->cheque_no = $payment->cheque_no;
                        $advance_payment->status = 'used';
                        $advance_payment->is_active = 1;
                        $advance_payment->save();
                    }

        $resVal['id'] = $payment->id;
        SmsHelper::invoicePaymentNotification($payment);
        $mail_obj = new MailHelper;
        $mail_obj->invoicePaymentNotification($payment);
        LogHelper::info1('Payment Save' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Payment', 'save', $screen_code, $payment->id);
        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $invoiceid = $request->input('invoice_id', '');
        $quoteid = $request->input('quote_id', '');
        $customerid = $request->input('customer_id');
        $isactive = $request->input('is_active', '');
        $fromDate = $request->input('from_date', '');
        $toDate = $request->input('to_date', '');
        $paymentmethod = $request->input('payment_mode', '');
        $cheque = $request->input('cheque_no', '');
        $status = $request->input('status', '');
        $mode = $request->input('mode');
        $recevied_by = $request->input('recevied_by');
        $tra_category = $request->input('tra_category');

        //$status = $request->input('status', '');


        $builder = DB::table('tbl_payment as i')
                ->leftJoin('tbl_customer as c', 'i.customer_id', '=', 'c.id')
                ->leftJoin('tbl_user as u', 'i.created_by', '=', 'u.id')
                ->select('i.*', 'c.fname as customer_fname', 'c.lname as customer_lname'
                , 'u.f_name as Receiver_fname', 'u.l_name as Receiver_lname');

        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('i.id', '=', $id);
        }
        if (!empty($invoiceid)) {
            $builder->where('i.invoice_id', '=', $invoiceid);
        }
        if (!empty($quoteid)) {
            $builder->where('i.quote_id', '=', $quoteid);
        }
        if (!empty($customerid)) {
            $builder->where('i.customer_id', '=', $customerid);
        }
        if ($isactive != '') {
            $builder->where('i.is_active', '=', $isactive);
        }
        if (!empty($fromDate)) {

            $builder->whereDate('i.date', '>=', $fromDate);
        }
        if (!empty($toDate)) {

            $builder->whereDate('i.date', '<=', $toDate);
        }
        if (!empty($paymentmethod)) {

            $builder->whereDate('i.payment_mode', '=', $paymentmethod);
        }
        if (!empty($cheque)) {

            $builder->whereDate('i.cheque_no', '=', $cheque);
        }
        if (!empty($status)) {

            $builder->whereDate('i.status', '=', $status);
        }
        if (!empty($recevied_by)) {
            $builder->where('i.created_by', '=', $recevied_by);
        }
        if (!empty($tra_category)) {
            $builder->where('i.tra_category', '=', $tra_category);
        }

        if ($mode == 0 || $mode == '') {
            $builder->orderBy('i.id', 'desc');
        } else {
            $builder->orderBy('i.date', 'asc');
        }


        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {

            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }
        
        LogHelper::info1('Payment List All' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Payment Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        try {
            $payment = Payment::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Payment Not Found';
            return $resVal;
        }
        $payment->fill($request->all());
        $payment->save();



        if ($request->input('invoice_id') != NULL && !empty($request->input('invoice_id'))) {
            $paidamount = DB::table('tbl_payment')
                            ->where('invoice_id', '=', $request->input('invoice_id'))
                            ->where('is_active', '=', 1)->sum('amount');

            //$invoiceamount = DB::table('tbl_invoice')->where('id', $request->input('invoice_id'))->update(['paid_amount' => $paidamount]);

            $invoiceCollection = Invoice::where('id', "=", $request->input('invoice_id'))->get();
            $collection = $invoiceCollection->first();
            $totalamount = $collection->total_amount;

            if ($paidamount == 0) {
                $collection->status = 'unpaid';
            } else if (($totalamount <= $paidamount)) {
                // DB::table('tbl_invoice') ->where('id',$invoice->id) ->update(['status' => 'paid' ]);
                $collection->status = 'paid';
            } elseif ($totalamount >= $paidamount) {

                $collection->status = 'partiallyPaid';
            }
            $collection->paid_amount = $paidamount;
            $collection->save();
        }



        if ($request->input('quote_id') != NULL && !empty($request->input('quote_id'))) {
            $advanceamount = DB::table('tbl_payment')
                    ->where('quote_id', $request->input('quote_id'))
                    ->where('is_active', '=', 1)
                    ->sum('amount');

            // $quoteamount = DB::table('tbl_quote')->where('id', $request->input('quote_id'))->update(['advance_amount' => $advanceamount]);

            $quoteCollection = Quote::where('id', "=", $request->input('quote_id'))
                    ->where('is_active','=',1)->get();
            $collection = $quoteCollection->first();
            $totalamount = $collection->total_amount;

            if ($advanceamount == 0) {
                $collection->status = 'unpaid';
            } else if (($totalamount <= $advanceamount)) {
                $collection->status = 'paid';
            } elseif ($totalamount >= $advanceamount) {

                $collection->status = 'partiallyPaid';
            }
            $collection->advance_amount = $advanceamount;
            $collection->save();
        }

        LogHelper::info1('Payment Update ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Payment', 'update', $screen_code, $payment->id);

        return $resVal;
    }

    public function delete(Request $request, $id) {
        $currentuser = Auth::user();
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Payment Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        try {
            $payment = Payment::findOrFail($id);
            if ($payment->is_active == 0) {
                return $resVal;
            }

            $paymentinvoiceid = $payment->invoice_id;
            $paymentquote = $payment->quote_id;
            $paymentamount = $payment->amount;
            $paymentisactive = $payment->is_active;

            if ($paymentisactive == 1) {
                if (!empty($paymentinvoiceid)) {
                    $payment->is_active = 0;
                    $payment->update();
                    $invoiceCollection = Invoice::where('id', "=", $paymentinvoiceid)->get();
                    $paymentinvoice = $invoiceCollection->first();
                    $paymentinvoice->save();
                    if ($payment->adv_payment_id != 0) {

                        SalesInvoiceHelper::updateAdvPaymentStatusAndBalance($payment->adv_payment_id);
                    }
                } else if (!empty($paymentquote)) {

                    $invoiceCollection = Quote::where('id', "=", $paymentquote)->get();
                    $paymentinvoice = $invoiceCollection->first();
                    $paid_amount = $paymentinvoice->advance_amount;

                    $deletePayment = $paid_amount - $paymentamount;
                    //print_r($deletePayment);
                    $paymentinvoice->advance_amount = $deletePayment;

                    $payment->is_active = 0;
                    $paymentinvoice->save();

                    $payment->update();


                    $paidamount = DB::table('tbl_payment')
                                    ->where('is_active', '=', 1)
                                    ->where('quote_id', '=', $paymentquote)->sum('amount');


                    $invoicetotalamount = $paymentinvoice->total_amount;

                    if ($paidamount == 0) {
                        $paymentinvoice->status = 'unpaid';
                    } else if (($invoicetotalamount <= $paidamount)) {
                        $paymentinvoice->status = 'paid';
                    } elseif ($invoicetotalamount > $paidamount) {

                        $paymentinvoice->status = 'partiallyPaid';
                    }
                    $paymentinvoice->advance_amount = $paidamount;
                    $paymentinvoice->save();
                }
                if ($payment->adv_payment_id == 0) {
                $transactionCollection = Transaction::where('voucher_number', "=", $payment->id)
                        ->where('voucher_type', "=", 'sales_invoice_payment')
                        ->get();
                $transaction = $transactionCollection->first();

                if (!empty($transaction->credit)) {

                    $account = AccountHelper::withDraw($transaction->account_id, $transaction->credit);
                }
                $transaction->is_active = 0;
                $transaction->update();
                }
                 SalesInvoiceHelper::updateInvoiceStatus($payment->invoice_id);
                //Activity log
                ActivityLogHelper::invoicePaymentDelete($paymentinvoice, $payment);
            }
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Payment Not Found';
            return $resVal;
        }

        $resVal['id'] = $payment->id;
        LogHelper::info1('Payment Delete ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Payment', 'delete', $screen_code, $payment->id);
        return $resVal;
    }

    public function findCardAmount(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $collection = DB::table('tbl_day_log')->select('*')
                        ->whereRaw('id = (select max(`id`) from tbl_day_log)')->first();

        if (($collection) != '') {
            $builder = DB::table('tbl_payment')
                            ->select(DB::raw('sum(amount) as cardAmount'))
                            ->where('day_code', '=', $collection->id)
                            ->where('payment_mode', 'like', '%' . 'card' . '%')->groupby('day_code');
            $resVal['success'] = TRUE;
            $resVal['list'] = $builder->get();
        } else {
            $resVal['success'] = FALSE;
              $resVal['list'] = array();
        }
        
        LogHelper::info1('Payment FindCardAmount List ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }

}

?>
