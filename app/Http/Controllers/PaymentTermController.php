<?php
namespace App\Http\Controllers;
use App\PaymentTerm;
use DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helper\AuthorizationHelper;
use App\Helper\LogHelper;
use App\Helper\NotificationHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserRoleController
 *
 * @author Deepa
 */
class PaymentTermController extends Controller {
    //put your code here
    public function save(Request $request){
        $resVal = array();
        $resVal['message'] = 'Payment Mode Added Successfully';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'save');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();


        $userrole = new PaymentTerm;
        
        if ($request->input('name') != NULL) {

            $userRoleCollection = PaymentTerm::where('name', "=", $request->input('name'))
                    ->where('is_active','=',1)->get();

            if (count($userRoleCollection) > 0) {
                $resVal['success'] = FALSE;
                $resVal['message'] = 'Name Is Already Exists';
                return $resVal;
            }
        }
        $userrole->created_by = $currentuser->id;
        $userrole->updated_by = $currentuser->id;
        $userrole->fill($request->all());
        $userrole->save();
        
        $payment = DB::table('tbl_payment_term')
                ->select('*')
                ->where('is_default', '=', 1)
                ->where('is_active','=',1)->get();
        if (count($payment) == 0) {
            $userrole->is_default = 1;
        }

        $userrole->save();
        
        if ($userrole->is_default == 1) {
             DB::table('tbl_payment_term')->where('id', '!=', $userrole->id)->update(['is_default' => 0]);
        }
        
        
        $resVal['id'] = $userrole->id;
        LogHelper::info1('Payment Term Save ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Payment Term', 'save', $screen_code, $userrole->id);
        return $resVal;
               
    }
     public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Payment Mode Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'update');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
         $currentuser = Auth::user();
         
          if ($request->input('name') != NULL) {
            $payTerm = PaymentTerm::where('name', "=", $request->input('name'))
                    ->where('is_active','=',1)
                    ->where('id', '!=', $id)
                    ->get();
            
            if (count($payTerm) > 0) {
                $resVal['success'] = FALSE;
                $resVal['message'] = 'Name Is Already Exists';
                return $resVal;
            }
        }

        try {
            $userrole = PaymentTerm::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Payment Mode Not Found';
            return $resVal;
        }
        
        if ($request->is_default == 0) {
            $payment = DB::table('tbl_payment_term')->select('*')
                    ->where('is_active','=',1)
                    ->where('is_default', '=', 1)
                    ->where('id', '!=', $id)
                    ->get();
            
            if (count($payment) == 0) {
                $resVal['success'] = FALSE;
                $resVal['message'] = 'Atleast One Payment Mode Must Be Marked As Default';
                return $resVal;
            }
        }

        $userrole->updated_by = $currentuser->id;
        $userrole->fill($request->all());
        $userrole->save();
        
        if ($userrole->is_default == 1) {
            DB::table('tbl_payment_term')->where('id', '!=', $id)->update(['is_default' => 0]);
        }
        LogHelper::info1('Payment Term Update ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Payment Term', 'update', $screen_code, $userrole->id);
        return $resVal;
    }
    
    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'list');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $name = $request->input('name', '');
        
        $is_active = $request->input('is_active', '');

        //$status = $request->input('status', '');
        $builder = DB::table('tbl_payment_term')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if ($is_active != '') {
            $builder->where('is_active', '=', $is_active);
        }

        if (!empty($name)) {
            $builder->where('name', 'like', '%' . $name . '%');

        }
        
        $builder->orderBy('id', 'desc');

        $resVal['total'] = $builder->count();
        $resVal['list'] = $builder->skip($start)->take($limit)->get();
         LogHelper::info1('Payment Term List All ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }
     public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = ' Payment Mode Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'delete');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        try {
            $userrole = PaymentTerm::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Payment Mode Not Found';
            return $resVal;
        }
        
        if ($userrole->is_default == 1) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Atleast One Payment Mode Must Be Marked As Default';
            return $resVal;
        }

        $userrole->delete();
        LogHelper::info1('Payment Term Delete ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Payment Term', 'delete', $screen_code, $userrole->id);
        return $resVal;
    }
    

    
}

?>
