<?php

namespace App\Http\Controllers;

use DB;
use App\Product;
use App\Inventory;
use App\InventoryAdjustment;
use App\Uom;
use App\Bcn;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\GeneralHelper;
use Validator;
use App\Attachment;
use App\Helper\LogHelper;
use App\Helper\NotificationHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ItemController
 *
 * @author Deepa
 */
class ProductController extends Controller {

    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Product Added Successfully';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        if ($request->input('name') != NULL) {
            $pro = Product::where('name', "=", $request->input('name'))->where('is_active', '=', 1)->get();

            if (count($pro) > 0) {
                $resVal['success'] = FALSE;
                $resVal['message'] = 'Name Is Already Exist';
                return $resVal;
            }
        }
        if ($request->input('sku') != NULL) {
            $product = Product::where('sku', "=", $request->input('sku'))->where('is_active', '=', 1)->get();

            if (count($product) > 0) {
                $resVal['success'] = FALSE;
                $resVal['message'] = 'Sku Name Is Already Exist';
                return $resVal;
            }
        }
        
        if(strcasecmp($request->type, 'Service') == 0)
            $resVal['message'] = 'Service Added Successfully';
        
        $currentuser = Auth::user();

        $item = new Product;
        if (!empty($request->input('uom_id', ''))) {
            $get_uom_name = DB::table('tbl_uom')
                            ->select('name')
                            ->where('id', '=', $request->uom_id)->first();
            $item->uom = $get_uom_name->name;
        } else {
            $item->uom = '';
        }
        $item->min_stock_qty = $request->min_stock_qty;
        $item->has_inventory = $request->has_inventory;
        $item->created_by = $currentuser->id;
        $item->updated_by = $currentuser->id;

        $item->fill($request->all());
        $item->save();


        if ($request->has_inventory == 1) {

            $inventory = new Inventory;
            $inventory->uom_name = $item->uom;
            $inventory->product_id = $item->id;
            $inventory->created_by = $currentuser->id;
            $inventory->updated_by = $currentuser->id;
            $inventory->quantity = $request->opening_stock;
            $inventory->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $inventory->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $inventory->fill($request->all());
            $inventory->save();




            $inventory_adu = new InventoryAdjustment;
            $inventory_adu->product_id = $item->id;
            $inventory_adu->uom_name = $item->uom;
            $inventory_adu->uom_id = $item->uom_id;
            $inventory_adu->type = "opening_stock";
            $inventory_adu->comments = "From Opening Stock";
            $inventory_adu->created_by = $currentuser->id;
            $inventory_adu->updated_by = $currentuser->id;
            $inventory_adu->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $inventory_adu->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $inventory_adu->total_stock = $request->opening_stock;
            $inventory_adu->date = date('Y-m-d');
            $inventory_adu->quantity_credit = $request->opening_stock;
            $inventory_adu->quantity_debit = 0;
            $inventory_adu->is_active = 1;
            $inventory_adu->sku = $item->sku;
            $inventory_adu->save();
        }

        $attachment = $request->input('attachment', '');
        if (!empty($attachment)) {
            foreach ($attachment as $attachments) {
                $attach = new Attachment;
                $attach->created_by = $currentuser->id;
                $attach->updated_by = $currentuser->id;
                $attach->ref_id = $item->id;
                $attach->is_active = 1;
                $attach->fill($attachments);
                $attach->save();
            }
        }


        // For Labeling 


        $type = GeneralHelper::checkConfiqSetting();


        if ($type == 'product based') {
            $labelingitem = new Bcn;
            $labelingitem->ref_id = $item->id;
            //  $labelingitem->purchase_invoice_id = $items->purchase_invoice_id;
            $labelingitem->ref_type = 'product based';
            $labelingitem->product_id = $item->id;
            $labelingitem->product_name = $item->name;
            $labelingitem->product_sku = $item->sku;
            $labelingitem->code = $item->code;
            $labelingitem->qty = $request->opening_stock;
            $labelingitem->qty_in_hand = $request->opening_stock;
            $labelingitem->sales_qty = 0;
            $labelingitem->purchase_qty = 0;
            $labelingitem->selling_price = $item->sales_price;
            //  $labelingitem->purchase_price = $items->purchase_price;
            //$labelingitem->mrp_price = $items->mrp_price;
            $labelingitem->status = 'product';
            //  $labelingitem->discount_mode = $items->discount_mode;
            //  $labelingitem->discount_value = $items->discount_value;
            //   $labelingitem->discount_amount = $items->discount_amount;
            $labelingitem->code = $item->code;
            $labelingitem->uom_id = $item->uom_id;
            $labelingitem->is_active = $item->is_active;
            $labelingitem->uom_name = $item->uom;

            $currentuser = Auth::user();
            $labelingitem->created_by = $currentuser->id;
            $labelingitem->updated_by = $currentuser->id;

            $labelingitem->save();
            BcnController::barcodeForProductBased($item->id);
        }
        if ($request->has_inventory == 1 && $type == 'product based') {
            DB::table('tbl_inventory_adjustment')->where('id', '=', $inventory_adu->id)->update(['bcn_id' => $labelingitem->id]);
        }

        LogHelper::info1('Product Save ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Product', 'save', $screen_code, $item->id);


        $resVal['id'] = $item->id;

        return $resVal;
    }

    public function product_csvexport(Request $request) {
        $productLists = $this->listAll($request);
        ini_set('max_execution_time', 600); //increase max_execution_time to 10 min if data set is very large
        //create a file
        $filename = " product" . "_" . time() . ".csv";
        //  $filename = "customer" . "_" . date("Y-m-d_H-i", time()) . ".csv";
        $serverName = $_SERVER['SERVER_NAME'];

        $filePath = "download/$serverName/csv";

        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);

            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {

                if ($folderName == '')
                    $folderName = $folderList[$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index];

                if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        
        $arrHead=array('#', 'Name', 'SKU','HSN/Legal Code', 'Description');
        $type1 = $request-> type ;
        
        $type = GeneralHelper::checkConfiqSetting();
        if ($type == 'product based' || $type1 == 'Service') 
         array_splice($arrHead,5,0, 'Sales Price'); 
         
        $filePath = getcwd() . '/' . $filePath;
        $sr = 1;
        $output = fopen($filePath . "/" . $filename, 'w');
        fputcsv($output, $arrHead);
        foreach ($productLists['list'] as $values) {
             $arrVal=array($values->id, $values->name, $values->sku,$values->hsn_code, $values->description);
             if ($type == 'product based'|| $type1 == 'Service') 
                 array_splice($arrVal,5,0, $values->sales_price); 
             
            fputcsv($output, $arrVal);
            // id: 1, customer_id: 37, prefix: "test123r", date: "2017-05-02", duedate: "2017-05-02"
            $sr++;
        }

        LogHelper::info1('Product CSV Download ' . $request->fullurl(), $request->all());


        return response()->download($filePath . "/" . $filename);
    }

    public function listAll(Request $request) {
        $current_date = Carbon::now();
        $current_date = $current_date->format('Y-m-d');
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        $id = $request->input('id');

        $isactive = $request->input('is_active', '');
        $name = $request->input('name', '');
        $sku = $request->input('sku', '');
        $type = $request->input('type', '');
        $issale = $request->input('is_sale', '');
        $has_inventory = $request->input('has_inventory', '');
        $ispurchase = $request->input('is_purchase', '');
        $category_id = $request->input('category_id', '');
        $local_data = $request->input('localData');
        $hsn_id = $request->input('hsn_id', '');
         $decimalFormat= GeneralHelper::decimalFormat();

        $builder = DB::table('tbl_product as p')
                ->leftJoin('tbl_category as c', 'p.category_id', '=', 'c.id')
                ->select('p.*',DB::raw("cast(min_stock_qty as DECIMAL(12,".$decimalFormat.")) as min_stock_qty"), 'c.name as category');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('p.id', '=', $id);
        }
        if (!empty($issale)) {
            $builder->where('p.is_sale', '=', $issale);
        }
        if (!empty($ispurchase)) {
            $builder->where('p.is_purchase', '=', $ispurchase);
        }
        if (!empty($has_inventory)) {
            $builder->where('p.has_inventory', '=', $has_inventory);
        }
        if (!empty($category_id)) {
            $builder->where('p.category_id', '=', $category_id);
        }
        if (!empty($name)) {
            $builder->where('p.name', 'like', '%' . $name . '%');
//            DB::connection()->enableQueryLog();
//            var_dump(DB::getQueryLog());
        }

        if ($hsn_id != '') {
            $builder->where('p.hsn_id', '=', $hsn_id);
        }

        if (!empty($sku)) {
            $builder->where('p.sku', 'like', '%' . $sku . '%');
        }

        if ($isactive != '') {
            $builder->where('p.is_active', '=', $isactive);
        }
        if (!empty($type)) {
            $builder->where('p.type', 'like', '%' . $type . '%');
//            DB::connection()->enableQueryLog();
//            var_dump(DB::getQueryLog());
        }
        $builder->orderBy('p.id', 'desc');
        $resVal['total'] = $builder->count();
        if ($local_data == 1) {
            if ($start == 0 && $limit == 100) {
                $products = $builder->get();
            } else {

                $products = $builder->skip($start)->take($limit)->get();
            }
        } else {
            if ($start == 0 && $limit == 0) {
                $products = $builder->get();
            } else {

                $products = $builder->skip($start)->take($limit)->get();
            }
        }

        foreach ($products as $prod) {
            $attach = DB::table('tbl_attachment')
                    ->where('ref_id', '=', $prod->id)
                    ->where('type', '=', 'product')
                    ->get();
            $prod->attachment = $attach;
        }
        $resVal['list'] = $products;

        LogHelper::info1('Product List All ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));


        return ($resVal);
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Product Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        try {
            $item = Product::findOrFail($id);
            if ($item->is_active == 0) {
                return $resVal;
            }
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Product Not found';
            return $resVal;
        }
        $currentuser = Auth::user();
        if(strcasecmp($item->type, 'Service') == 0)
            $resVal['message'] = 'Service Deleted Successfully';
        $item->is_active = 0;
        $item->updated_by = $currentuser->id;
        $item->save();
        $type = GeneralHelper::checkConfiqSetting();
        Attachment::where('ref_id', '=', $id)->where('type', '=', 'product')->delete();

        if ($type == 'product based') {

            DB::table('tbl_bcn')->where('ref_id', '=', $id)->update(['is_active' => 0]);
        }

        LogHelper::info1('Product Delete ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Product', 'delete', $screen_code, $item->id);


        return $resVal;
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Product Updated Successfully';
        if(strcasecmp($request->type, 'Service')==0)
            $resVal['message'] = 'Service Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $item = Product::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Product Not Found';
            return $resVal;
        }
        if ($request->input('name') != NULL) {
            $pro = Product::where('name', "=", $request->input('name'))->where('id', '!=', $id)->where('is_active', '=', 1)->get();

            if (count($pro) > 0) {
                $resVal['success'] = FALSE;
                $resVal['message'] = ' Name Is Already Exist';
                return $resVal;
            }
        }
        if ($request->input('sku') != NULL) {
            $product = Product::where('sku', "=", $request->input('sku'))->where('is_active', '=', 1)->where('id', '!=', $id)->get();

            if (count($product) > 0) {
                $resVal['success'] = FALSE;
                $resVal['message'] = 'Sku Name Is Already Exist';
                return $resVal;
            }
        }
        if (!empty($request->input('uom_id', ''))) {
            $get_uom_name = DB::table('tbl_uom')
                            ->select('name')
                            ->where('id', '=', $request->uom_id)->first();
            $item->uom = $get_uom_name->name;
        } else {
            $item->uom = '';
        }
        $item->created_by = $currentuser->id;
        $item->fill($request->all());
        $item->save();

        $type = GeneralHelper::checkConfiqSetting();


        if ($type == 'product based') {

            $labelingitem = Bcn::Where('ref_id', '=', $id)->first();
            $labelingitem->ref_id = $item->id;
            //  $labelingitem->purchase_invoice_id = $items->purchase_invoice_id;
            $labelingitem->ref_type = $type;
            $labelingitem->product_id = $item->id;
            $labelingitem->product_name = $item->name;
            $labelingitem->product_sku = $item->sku;
            $labelingitem->code = $item->code;
            if ($request->has_inventory == 1) {
                $labelingitem->qty = $request->min_stock_qty;
                $labelingitem->qty_in_hand = $request->min_stock_qty;
            }
            $labelingitem->sales_qty = 0;
            $labelingitem->selling_price = $item->sales_price;
            //  $labelingitem->purchase_price = $items->purchase_price;
            //$labelingitem->mrp_price = $items->mrp_price;
            $labelingitem->status = 'product';
            //  $labelingitem->discount_mode = $items->discount_mode;
            //  $labelingitem->discount_value = $items->discount_value;
            //   $labelingitem->discount_amount = $items->discount_amount;
            $labelingitem->code = $item->code;
            $labelingitem->uom_id = $item->uom_id;
            $labelingitem->is_active = $item->is_active;
            $labelingitem->uom_name = $item->uom;

            $currentuser = Auth::user();
            $labelingitem->created_by = $currentuser->id;
            $labelingitem->updated_by = $currentuser->id;

            $labelingitem->save();
        }
//print_r($request->all());
        Attachment::where('ref_id', '=', $id)->where('type', '=', 'product')->delete();
        $attachment = $request->input('attachment', '');
        if (!empty($attachment)) {
            foreach ($attachment as $attachments) {
                $attach = new Attachment;
                $attach->created_by = $currentuser->id;
                $attach->updated_by = $currentuser->id;
                $attach->ref_id = $item->id;
                $attach->is_active = 1;
                $attach->fill($attachments);
                $attach->save();
            }
        }


        if ($request->has_inventory == 1) {

            $inventory = Inventory::where('product_id', '=', $id)->where('is_active', '=', 1)->first();

            if (empty($inventory)) {
                $inventory = new Inventory;
                $inventory->uom_name = $item->uom;
                $inventory->sku = $item->sku;
                $inventory->product_id = $item->id;
                $inventory->created_by = $currentuser->id;
                $inventory->updated_by = $currentuser->id;
                // $inventory->quantity = $request->opening_stock;
                $inventory->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
                $inventory->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
                $inventory->fill($request->all());
                $inventory->quantity = 0;
                $inventory->is_active = 1;
                $inventory->save();

                $inventory_adu = new InventoryAdjustment;
                $inventory_adu->product_id = $item->id;
                $inventory_adu->uom_name = $item->uom;
                $inventory_adu->uom_id = $item->uom_id;
                $inventory_adu->sku = $item->sku;
                $inventory_adu->type = "opening_stock";
                $inventory_adu->comments = "From Opening Stock";
                $inventory_adu->created_by = $currentuser->id;
                $inventory_adu->updated_by = $currentuser->id;
                $inventory_adu->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
                $inventory_adu->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;

                $inventory_adu->quantity_credit = 0;
                $inventory_adu->quantity_debit = 0;
                $inventory_adu->date = date('Y-m-d');

                $inventory_adu->total_stock = 0;
                $inventory_adu->is_active = 1;
                $inventory_adu->save();
            } else {
                $inventory->sku = $item->sku;
                $inventory->uom_id = $item->uom_id;
                $inventory->uom_name = $item->uom;
                $inventory->product_id = $item->id;
                $inventory->created_by = $currentuser->id;
                $inventory->updated_by = $currentuser->id;
                $inventory->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
                $inventory->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
                $inventory->save();

                DB::table('tbl_inventory_adjustment')->where('product_id', '=', $id)
                        ->update(
                                array(
                                    "sku" => $item->sku,
                                    "uom_id" => $item->uom_id,
                                    "uom_name" => $item->uom
                                )
                );
            }
        }

        LogHelper::info1('Product Update ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Product', 'update', $screen_code, $item->id);


        return $resVal;
    }

    public function listWithTax(Request $request) {
        $current_date = Carbon::now();
        $current_date = $current_date->format('Y-m-d');
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        $id = $request->input('id');
        $isactive = $request->input('is_active', '');
        $name = $request->input('name', '');
        $type = $request->input('type');
        $customer_id=$request->input('customer_id',0);
        $is_group=$request->input('is_group','');
        $mode=$request->input('mode',0);
            
        $sku = $request->input('sku', '');
        $builder = DB::table('tbl_product as p')
                ->leftJoin('tbl_category as c', 'p.category_id', '=', 'c.id')
                ->select('p.*', 'c.name as category');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('p.id', '=', $id);
        }
        if ($isactive != '') {
            $builder->where('p.is_active', '=', $isactive);
        }
        if (!empty($type)) {
            $builder->where('p.type', 'like', '%' . $type . '%');
        }
        if(!empty($sku)){
             $builder->where('p.sku', '=', $sku);
        }
        
        $builder->whereRaw(" (p.name like '%" . $name . "%' or p.sku like '%" . $name . "%')");
        $builder->orderBy('p.id', 'desc');
        $resVal['total'] = $builder->count();
        $products = $builder->skip($start)->take($limit)->get();
        foreach ($products as $p) {
            $pdt_id = $p->id;
            
            $taxMapping = DB::table('tbl_tax as t')
                    ->leftJoin('tbl_tax_mapping as tm', function($join) use ($pdt_id) {
                        $join->on('t.id', '=', 'tm.tax_id');
                        $join->on('product_id', '=', DB::raw("'" . $pdt_id . "'"));
                        $join->on('tm.is_active', '=', DB::raw("'1'"));
                    })
                    ->select(DB::raw('coalesce(tm.id, 0) as id'),DB::raw('coalesce(tm.product_id, 0) as product_id'), DB::raw('ifnull(t.id, 0) as tax_id'), DB::raw('coalesce(t.tax_name,"") as tax_name'), DB::raw('coalesce(t.tax_percentage,0) as tax_percentage'), DB::raw('coalesce(t.tax_applicable_amt,0) as tax_applicable_amt'), DB::raw('coalesce(t.start_date) as start_date'), DB::raw('coalesce(t.end_date,0) as end_date'))
                    ->where(DB::raw("((t.start_date <= $current_date and t.end_date >= $current_date) or (t.start_date = null or t.end_date = null))"))
                    ->where('t.is_display', '=', 1)
                    ->where('t.is_active', '=', 1);
                    
        $type = GeneralHelper::checkConfiqSetting();

        if ($type == 'product based' && $mode==1) {
            $taxMapping->where('tm.product_id','=',$pdt_id);
        }
                    
            if($is_group != '') {
                $taxMapping->where('t.is_group', '=', $is_group);
            }
            
            $p->taxMapping = $taxMapping->get();
        }
        $resVal['list'] = $products;

        LogHelper::info1('Product With Tax List All ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));

        return ($resVal);
    }

    public function listMappedTax(Request $request) {
        $current_date = Carbon::now();
        $current_date = $current_date->format('Y-m-d');
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        $id = $request->input('id');
        $isactive = $request->input('is_active', '');
        $search = $request->input('name', '');

        $builder = DB::table('tbl_product as p')
                ->leftJoin('tbl_category as c', 'p.category_id', '=', 'c.id')
                ->select('p.*', 'c.name as category');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('p.id', '=', $id);
        }
        if ($isactive != '') {
            $builder->where('p.is_active', '=', $isactive);
        }
        if (!empty($search)) {
            $builder->whereRaw(" (p.name like '%" . $search . "%' or p.sku like '%" . $search . "%') ");
        }
        $builder->orderBy('p.id', 'desc');
        $resVal['total'] = $builder->count();
        $products = $builder->skip($start)->take($limit)->get();
        foreach ($products as $p) {
            $pdt_id = $p->id;
            $taxMapping = DB::table('tbl_tax_mapping as tm')
                    ->leftJoin('tbl_tax as t', 't.id', '=', 'tm.tax_id')
                    ->select('tm.id as id', 't.id as tax_id', 't.tax_name as tax_name', 't.tax_percentage', 't.tax_applicable_amt', 't.start_date', 't.end_date')
                    ->where('product_id', $pdt_id)->where('t.is_active', '=', 1)->where('tm.is_active', '=', 1)
                    ->where('t.start_date', '<=', $current_date)
                    ->where('t.end_date', '>=', $current_date);
            $p->taxMapping = $taxMapping->get();
        }
        $resVal['list'] = $products;

        LogHelper::info1('Product With Mapped Tax List ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));

        return ($resVal);
    }

    public function pdtListForBill(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        $current_date = Carbon::now();
        $current_date = $current_date->format('Y-m-d');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $id = $request->input('id');
        $isactive = $request->input('is_active', '');
        $search = $request->input('search', '');
        $category = $request->input('category', '');
        $is_sale=$request->input('is_sale','');
        $is_purchase=$request->input('is_purchase','');
        $customer_id=$request->input('customer_id',0);
        $decimalFormat= GeneralHelper::decimalFormat();
        
       
        $settings = DB::table('tbl_app_config')
                        ->select('*')->where('setting', '=', 'sales_product_list')->where('value', 'product based')->get()->first();

        //If Setting is empty then uni_price from  tbl_purchase_invoice_item else tierPricing                  
        if ($settings == "") {
            // print_r("Purchase");
            $builder = DB::table('tbl_bcn as pi')
                    ->leftJoin('tbl_product as p', function($join) {
                        $join->on('p.id', '=', 'pi.product_id')
                        ->on('pi.ref_type', '=', DB::Raw('"purchase based"'));
                    })
                    //     ->leftJoin('tbl_product as p', 'p.id', '=', 'pi.product_id')
                    ->leftJoin('tbl_category as ca', 'p.category_id', '=', 'ca.id')
                    ->leftJoin('tbl_inventory as i', 'pi.product_id', '=', 'i.product_id')
                    ->leftjoin('tbl_purchase_invoice_item as pt', 'pt.id', '=', 'pi.ref_id')
                    ->select('p.*', 'p.id as product_id','pt.purchase_price_w_tax','pi.purchase_invoice_id', 'p.name as product_name', 'pt.customer_id', 'pt.purchase_price', 'p.sku',DB::raw("cast(i.quantity as DECIMAL(12,".$decimalFormat.")) as inventory_qty"),DB::raw("cast(p.min_stock_qty as DECIMAL(12,".$decimalFormat.")) as min_stock_qty"),DB::raw("cast(pi.qty_in_hand as DECIMAL(12,".$decimalFormat.")) as qty_in_hand"), 'pi.mrp_price', 'pi.selling_price as selling_price', 'ca.name as category_name','pi.id as bcn_id', 'pt.id as purchase_invoice_item_id', 'pi.code as code', 'pi.ref_id', DB::raw("'Purchase Based' as refType"),
                            'pt.custom_opt1', 'pt.custom_opt2', 'pt.custom_opt3', 'pt.custom_opt4', 'pt.custom_opt5','pt.custom_opt1_key', 'pt.custom_opt2_key', 'pt.custom_opt3_key', 'pt.custom_opt4_key', 'pt.custom_opt5_key')
                    ->where('pi.code', '=', $search);
            $resVal['success'] = TRUE;
            $start = $request->input('start', 0);
            $limit = $request->input('limit', 0);

            if (!empty($id)) {
                $builder->where('pi.id', '=', $id);
            }
            if (!empty($category)) {
                $builder->where('ca.id', '=', $category);
            }
            if ($isactive != '') {
                $builder->where('pi.is_active', '=', $isactive);
            }
            if (!empty($customer_id)) {
                $builder->where('pt.customer_id', '=', $customer_id);
            }
            if (!empty($is_sale)) {
                $builder->where('p.is_sale', '=', $is_sale);
            }
            if (!empty($is_purchase)) {
                $builder->where('p.is_purchase', '=', $is_purchase);
            }

//            if (!empty($search)) {
//                $builder->whereRaw(" (pi.code = '" . $search . "' or p.name like '%" . $search . "%' or p.sku like '%" . $search . "%' OR pi.id = '$search' ) ");
//            }
            $builder->orderBy('pi.id', 'desc');
            $resVal['total'] = $builder->count();
            if ($start == 0 && $limit == 0) {
                $products = $builder->get();
            } else {

                $products = $builder->skip($start)->take($limit)->get();
            }
            //Tax Mapping
            foreach ($products as $pdt) {
                $pdt_id = $pdt->product_id;
            
            
                $taxMapping = DB::table('tbl_purchase_invoice_item as tm')
                        ->leftJoin('tbl_tax as t', 't.id', '=', 'tm.selling_tax_id')
                        ->select('tm.id as id', 'tm.selling_tax_id as tax_id', 't.tax_name as tax_name', 'tm.selling_tax_percentage as tax_percentage', 't.tax_applicable_amt', 't.start_date', 't.end_date')
                        ->where('tm.id', $pdt->ref_id)->where('t.is_active', '=', 1)->where('tm.is_active', '=', 1)
//                        ->where('t.start_date', '<=', $current_date)
//                        ->where('t.end_date', '>=', $current_date);
                        ->whereRaw(DB::raw("(start_date <= '$current_date' or  `start_date` is null) and (end_date >= '$current_date' or end_date is null) "));
                $pdt->taxMapping = $taxMapping->get();
                
                $purchaseTaxMapping = DB::table('tbl_purchase_invoice_item as tm')
                        ->leftJoin('tbl_tax as t', 't.id', '=', 'tm.tax_id')
                        ->select('tm.id as id', 'tm.tax_id as tax_id', 't.tax_name as tax_name', 'tm.tax_percentage as tax_percentage', 't.tax_applicable_amt', 't.start_date', 't.end_date')
                        ->where('tm.id', $pdt->ref_id)->where('t.is_active', '=', 1)->where('tm.is_active', '=', 1)
                        ->whereRaw(DB::raw("(start_date <= '$current_date' or  `start_date` is null) and (end_date >= '$current_date' or end_date is null) "));
                $pdt->purchaseTaxMapping = $purchaseTaxMapping->get();
            
            }

            //Tier Pricing
            foreach ($products as $pdt) {
                $pdt->tierPricing = [];
            }

            $resVal['list'] = $products;
        } else {
            // print_r("Product");
            $builder = DB::table('tbl_product as p')
                    ->leftJoin('tbl_category as ca', 'p.category_id', '=', 'ca.id')
                    ->leftJoin('tbl_inventory as i', 'p.id', '=', 'i.product_id')
                    ->select('p.*', 'p.id as product_id', 'p.name as product_name',DB::raw("cast(p.min_stock_qty as DECIMAL(12,".$decimalFormat.")) as min_stock_qty"), 'p.sku',DB::raw("cast(i.quantity as DECIMAL(12,".$decimalFormat.")) as inventory_qty"),DB::raw("cast(i.quantity as DECIMAL(12,".$decimalFormat.")) as qty_in_hand"), 'ca.name as category_name', 'p.sales_price as selling_price', DB::Raw("0 as mrp_price, 0 as purchase_invoice_item_id,0 as purchase_price_w_tax,0 as purchase_invoice_id,0 as customer_id,0 as purchase_price,0 as bcn_id,0 as ref_id,"
                            . " '' as custom_opt1,'' as custom_opt2,'' as custom_opt3,'' as custom_opt4,'' as custom_opt5,'' as custom_opt1_key,'' as custom_opt2_key,'' as custom_opt3_key,'' as custom_opt4_key,'' as custom_opt5_key"), DB::raw("'Product Based' as refType"))
                    ->where('p.is_sale', '=', 1);
            
            $resVal['success'] = TRUE;
            $start = $request->input('start', 0);
            $limit = $request->input('limit', 0);

            if (!empty($id)) {
                $builder->where('p.id', '=', $id);
            }
            if (!empty($category)) {
                $builder->where('ca.id', '=', $category);
            }
            if ($isactive != '') {
                $builder->where('p.is_active', '=', $isactive);
            }

            if (!empty($search)) {
                $builder->whereRaw(" (p.code = '" . $search . "' or p.name like '%" . $search . "%' or p.sku like '%" . $search . "%' OR p.id =' $search') ");
            }
            $builder->orderBy('p.id', 'desc');
            $resVal['total'] = $builder->count();
            if ($start == 0 && $limit == 0) {
                $products = $builder->get();
            } else {

                $products = $builder->skip($start)->take($limit)->get();
            }

            //Tax Mapping
            foreach ($products as $pdt) {
                $pdt_id = $pdt->product_id;
                
               
                $taxMapping = DB::table('tbl_tax_mapping as tm')
                        ->leftJoin('tbl_tax as t', 't.id', '=', 'tm.tax_id')
                        ->select('tm.id as id', 't.id as tax_id', 't.tax_name as tax_name', 't.tax_percentage', 't.tax_applicable_amt', 't.start_date', 't.end_date')
                        ->where('product_id', $pdt_id)->where('t.is_active', '=', 1)->where('tm.is_active', '=', 1)
//                        ->where('t.start_date', '<=', $current_date)
//                        ->where('t.end_date', '>=', $current_date);
                        ->whereRaw(DB::raw("(start_date <= '$current_date' or  `start_date` is null) and (end_date >= '$current_date' or end_date is null) "));
                $pdt->taxMapping = $taxMapping->get();
                $pdt->purchaseTaxMapping =[];
            
            }
            //Tier Pricing wer
            foreach ($products as $pdt) {
                $pdt_id = $pdt->product_id;
                $tierPricing = DB::table('tbl_product_tier_pricing as tm')
                                ->select('tm.*')
                                ->where('product_id', $pdt_id)->where('tm.is_active', '=', 1);
                $pdt->tierPricing = $tierPricing->get();
            }
            $resVal['list'] = $products;
        }

        LogHelper::info1('BCN Product List ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));

        return ($resVal);
    }

}
?>

