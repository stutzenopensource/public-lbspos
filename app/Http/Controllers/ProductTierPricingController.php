<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductTierPricingController
 *
 * @author Stutzen Admin
 */

namespace App\Http\Controllers;

use DB;
use App\ProductTierPricing;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\LogHelper;
use App\Helper\NotificationHelper;

class ProductTierPricingController extends Controller {

    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Tier Pricing Saved Successfully';
        $resVal['success'] = TRUE;
        $currentuser = Auth::user();
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $tps = $request->all();
        foreach ($tps as $tierPricing) {
            if ($tierPricing['id'] == 0) {
                $tp = new ProductTierPricing;
                $pdt = DB::table('tbl_product')
                      ->select('*')->where('id', $tierPricing['product_id'])->get()->first();
                $tp->fill($tierPricing);
                if($pdt!=""){
                $tp->product_name = $pdt->name;
                $tp->category_id = $pdt->category_id;
                $tp->uom_id = $pdt->uom_id;
                $tp->uom = $pdt->uom;
                }
                $tp->created_by = $currentuser->id;
                $tp->updated_by = $currentuser->id;
                $tp->is_active = 1;
                $tp->save();
            } else {
                try {
                    $tp = ProductTierPricing::findOrFail($tierPricing['id']);
                } catch (ModelNotFoundException $e) {

                    $resVal['success'] = FALSE;
                    $resVal['message'] = ' Details  Not Found';
                    return $resVal;
                }

                $tp->updated_by = $currentuser->id;
                $tp->is_active = 1;
                $tp->fill($tierPricing);
                $tp->save();
            }
        }
        LogHelper::info1('Product Tier Pricing Save' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Product Tier Pricing', 'save', $screen_code, $tp->id);
        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $id = $request->input('id');
        $category_id = $request->input('category_id', '');
        $product_id = $request->input('product_id', '');
        $uom_id = $request->input('uom_id', '');
        $is_active = $request->input('is_active');
        $builder = DB::table('tbl_product_tier_pricing')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if ($is_active != '') {
            $builder->where('is_active', '=', $is_active);
        }
        if (!empty($category_id)) {
            $builder->where('category_id', '=', $category_id);
        }
        if (!empty($product_id)) {
            $builder->where('product_id', '=', $product_id);
        }

        if (!empty($uom_id)) {
            $builder->where('uom_id', '=', $uom_id);
        }

        $builder->orderBy('id', 'desc');
        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {
            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }
        LogHelper::info1('Product Tier Pricing List All ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return $resVal;
    }

    public function delete(Request $request) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Tier Pricing  Deleted Successfully';
        $currentuser = Auth::user();
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $tps = $request->all();
        foreach ($tps as $aa) {
            if ($aa['id'] != NULL) {
                $collection = ProductTierPricing::where('id', "=", $aa['id'])->get();
                $tierPricing = $collection->first();
                $tierPricing->updated_by = $currentuser->id;
                $tierPricing->is_active = 0;
                $tierPricing->save();
                NotificationHelper::saveNotification($request, 'Product Tier Pricing', 'delete', $screen_code, $tierPricing->id);
            }
        }
        LogHelper::info1('Product Tier Pricing Delete' . $request->fullurl(), $request->all());
      
        return $resVal;
    }

}
