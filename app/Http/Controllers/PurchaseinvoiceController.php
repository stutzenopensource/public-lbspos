<?php

namespace App\Http\Controllers;

use DB;
use Carbon\Carbon;
use App\ActivityLog;
use App\Transaction;
use App\PurchasePayment;
use App\Purchaseinvoice;
use App\Purchaseinvoiceitem;
use App\Bcn;
use Illuminate\Http\Request;
use App\Attributevalue;
use App\Attribute;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\GeneralHelper;
use App\Helper\ActivityLogHelper;
use App\Helper\AccountHelper;
use App\Helper\LogHelper;
use App\Helper\NotificationHelper;
use App\Http\Controllers\BcnController;
use App\Helper\TransactionHelper;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PurchaseinvoiceController
 *
 * @author Deepa
 */
class PurchaseinvoiceController extends Controller {

    //put your code here
    public function save(Request $request) {
        $current_date = Carbon::now();
        $current_date = $current_date->format('Y-m-d');
        $resVal = array();
        $resVal['message'] = ' Purchase Invoice Added Successfully';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $purchase_no = $request->input('purchase_no', '');
        $prefix = $request->input('prefix', '');
        $purchase_code = '';
        if (!empty($purchase_no)) {

            $get_inv_no = DB::table('tbl_purchase_invoice')
                    ->select('is_active')
                    ->where('prefix', '=', $prefix)
                    ->where('is_active', '=', 1)
                    ->where('purchase_no', '=', $purchase_no)
                    ->get();
            if (count($get_inv_no) > 0) {
                $resVal['message'] = 'Purchase Invoice No Already Exists';
                $resVal['success'] = FALSE;
                return $resVal;
            } else {
                $purchase_no = $purchase_no;
                $purchase_code = $prefix . $purchase_no;
            }
        } else {
            $get_inv_no = DB::table('tbl_purchase_invoice')
                    ->select('purchase_no')
                    ->where('is_active', '=', 1)
                    ->where('prefix', '=', $prefix)
                    ->orderBy('purchase_no', 'desc')
                    ->first();
            if (isset($get_inv_no)) {
                $inv_no = $get_inv_no->purchase_no;
                $inv_no = $inv_no + 1;
            } else {
                $inv_no = 1;
            }
            $purchase_no = $inv_no;
            $purchase_code = $prefix . $inv_no;
        }
        $subtotal = 0;
        $discount_amount = 0;
        $roundoff = 0;
        $tax_id = 0;
        $tax_amount = 0;
        $taxable_amount = 0;
        $total_before_roundoff = 0;
        $total = 0;

        $invoiceItemCol = $request->input('item');
        /* foreach ($invoiceItemCol as $item) {
          $item['total_price'] = floatval($item['purchase_price']) * floatval($item['qty']);

          $item['total_price'] = floatval(number_format($item['total_price'], 2, '.', ''));
          $subtotal = $subtotal + $item['total_price'];
          } */

        $currentuser = Auth::user();
        $purchaseinvoice = new Purchaseinvoice;
        $purchaseinvoice->fill($request->all());
        $purchaseinvoice->created_by = $currentuser->id;
        $purchaseinvoice->updated_by = $currentuser->id;
        $purchaseinvoice->is_active = $request->input('is_active', 1);
        // $purchaseinvoice->subtotal = $subtotal;
        // $purchaseinvoice->tax_amount = $tax_amount;
        // $purchaseinvoice->discount_amount = $discount_amount;
        // $purchaseinvoice->round_off = $roundoff;
        // $purchaseinvoice->total_amount = $total;
        $purchaseinvoice->purchase_no = $purchase_no;
        $purchaseinvoice->purchase_code = $purchase_code;
        $purchaseinvoice->save();

        if ($request->input('purchase_quote_id') != NULL && $request->input('purchase_quote_id') != 0 && !empty($request->input('purchase_quote_id'))) {

            DB::table('tbl_purchase_payment')->where('purchase_quote_id', $request->input('purchase_quote_id'))
                    ->where('is_active', "=", 1)
                    ->update(['purchase_invoice_id' => $purchaseinvoice->id]);
            DB::table('tbl_purchase_quote')->where('id', $request->input('purchase_quote_id'))->update(['status' => 'invoiced']);
        }


        $totalamount = $purchaseinvoice->total_amount;


        $payment = 0;
        if ($request->input('purchase_quote_id') != NULL && $request->input('purchase_quote_id') != 0 && !empty($request->input('purchase_quote_id'))) {

            $payment = DB::table('tbl_purchase_payment')->where('purchase_quote_id', $request->input('purchase_quote_id'))
                    ->where('is_active', "=", 1)
                    ->sum('amount');
        }

        if (($totalamount <= $payment)) {
            // DB::table('tbl_invoice') ->where('id',$invoice->id) ->update(['status' => 'paid' ]);
            $purchaseinvoice->status = 'paid';
        } elseif ($payment == 0) {
            $purchaseinvoice->status = 'unpaid';
        } elseif ($totalamount > $payment) {

            $purchaseinvoice->status = 'partiallyPaid';
        }

        $customerItemCol = ($request->input('customattribute'));
        $purchaseinvoice->paid_amount = $payment;
        $purchaseinvoice->is_barcode_generated = 0;
        $attribute = array();
        $purchaseinvoice->save();
        $invoiceItemCol = $request->input('item');
        PurchaseinvoiceController::calculateTotal($purchaseinvoice->id, $invoiceItemCol, 'update');


//         //finding roundoff value
//            $total_before_roundoff = $subtotal - $discount_amount + $overAllTax;
//            $total_after_roundoff = round($total_before_roundoff);
//            $roundoff = $total_after_roundoff - $total_before_roundoff;
//            $roundoff = floatval(number_format($roundoff, 2, '.', ''));
//            $purchaseinvoice->tax_amount = $overAllTax;
//            $total = $subtotal - $discount_amount + $overAllTax + $roundoff;
//            $total = floatval(number_format($total, 2, '.', ''));
//            $purchaseinvoice->total_amount = $total;
//            $purchaseinvoice->save();
        if ($request->input('customattribute') != null) {

            $attribute = array();
            foreach ($customerItemCol as $item) {
                $attribute[$item['attribute_code']] = $item['value'];
            }
            $json_att = json_encode($attribute);
            $purchaseinvoice->custom_attribute_json = $json_att;
            $purchaseinvoice->save();

            foreach ($customerItemCol as $attribute) {
                //print_r($attribute);

                $customerattribute = new Attributevalue;

                $customerattribute->refernce_id = $purchaseinvoice->id;
                $customerattribute->value = $attribute['value'];

                $customerattribute->attribute_code = $attribute['attribute_code'];

                $customerattribute->created_by = $currentuser->id;
                $customerattribute->updated_by = $currentuser->id;
                if ($attribute['attribute_code'] != NULL) {


                    $attributeCollection = Attribute::where('attribute_code', "=", $attribute['attribute_code'])->get();
                    if (count($attributeCollection) > 0) {
                        $attribute = $attributeCollection->first();
                        //print_r($attribute->id);
                        $customerattribute->attributes_id = $attribute->id;
                        $customerattribute->attributetype_id = $attribute->attributetype_id;
                    }
                }

                //$customerattribute->fill($attribute);

                $customerattribute->save();
            }
        }

        $builder = DB::table('tbl_customer')
                        ->select('*')
                        ->where('id', '=', $purchaseinvoice->customer_id)->first();
        $cus_name = $builder->fname . " " . $builder->lname;
        $mydate = $purchaseinvoice->date;
         $transactionData = array(
             "name" => $currentuser->f_name,
             "transaction_date" => $purchaseinvoice->date,
             "voucher_type" => Transaction::$PURCHASE,
             "voucher_number" => $purchaseinvoice->id,
             "credit" => $purchaseinvoice->total_amount,
             "debit" => 0.00,
             "pmtcode" => GeneralHelper::getPaymentAcode($request->input('account_id', '')),
             "account_category" => '',
             "acode" => GeneralHelper::getCustomerAcode($purchaseinvoice->customer_id),
             "acoderef" => $purchaseinvoice->customer_id,
             "account" => $request->input('account', ''),
             "pmtcoderef" => $request->input('account_id', ''),
             "is_cash_flow" => 0,
             "customer_id" => $purchaseinvoice->customer_id,
             "payment_mode" => '',
             "particulars" => 'Purchase invoice '.$purchase_code.' from ' . $cus_name . ' by ' . $currentuser->f_name . ' ' . $currentuser->l_name,
             "is_active" => 1,
             "created_by" => $currentuser->id,
             "updated_by" => $currentuser->id,
             "created_by_name" => $currentuser->f_name . ' ' . $currentuser->l_name,
             "updated_by_name" => $currentuser->f_name . ' ' . $currentuser->l_name,
             "prefix" => $purchaseinvoice->prefix,

        );
        TransactionHelper::transactionSave($transactionData);

        $resVal['id'] = $purchaseinvoice->id;
        ActivityLogHelper::purchaseinvoiceSave($purchaseinvoice);
        GeneralHelper::StockAdjustmentSave($request->input('item'), 'purchase', $purchaseinvoice->id,$purchaseinvoice->date);
        // GeneralHelper::InvoiceTaxSave($purchaseinvoice->id, $request->input('tax_id'), $taxable_amount, $subtotal, 'purchase');
        LogHelper::info1('Purchase Invoice Save' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Purchase Invoice', 'save', $screen_code, $purchaseinvoice->id);
        return $resVal;
    }

    public function update(Request $request, $id) {
        $currentuser = Auth::user();
        $current_date = Carbon::now();
        $current_date = $current_date->format('Y-m-d');
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Purchase Invoice Updated Successfully';

        try {
            $purchaseinvoice = Purchaseinvoice::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Purchase Invoice Not Found';
            return $resVal;
        }
        $type = GeneralHelper::checkConfiqSetting();
        if($type == 'purchase based')
        {

         $query=DB::table('tbl_purchase_invoice_item')->select('id')
                                 ->Where('purchase_invoice_id','=',$id)
                                 ->where('is_active',1);
         $pinvoice_return =DB::table('tbl_purchase_invoice_item_return')->select('id')
                        ->where('is_active',1)
        		->whereIn('purchase_invoice_item_id',$query)
                        ->get();

        $pinvoice_stockadj=DB::table('tbl_stock_adjustment_detail')->select('*')
                                ->where('purchase_invoice_id',$id)->where('is_active',1)->get();

        $pinvoice_deliverynote=DB::table('tbl_delivery_note_detail')->select('*')
                                ->where('is_active',1)
                                ->whereIn('ref_id',$query)
                                ->get();
         if(count($pinvoice_return) > 0)
         {
             $resVal['success'] = FALSE;
             $resVal['message'] = "Purchase Was Returned So Can't Able To Edit";
             return $resVal;
         }
         if(count($pinvoice_stockadj) > 0)
         {
             $resVal['success'] = FALSE;
             $resVal['message'] = "Stock Adjustment Done For This Purchase So Can't Able To Edit";
              return $resVal;
         }
         if(count($pinvoice_deliverynote) > 0)
         {
             $resVal['success'] = FALSE;
              $resVal['message'] = "Delivery Note Done For This Purchase So Can't Able To Edit";
               return $resVal;
         }

        }
        $purchase_no = $request->input('purchase_no', '');
        $prefix = $request->input('prefix', '');
        $purchase_code = '';
        if (!empty($purchase_no)) {

            $get_inv_no = DB::table('tbl_purchase_invoice')
                    ->select('is_active')
                    ->where('prefix', '=', $prefix)
                    ->where('is_active', '=', 1)
                    ->where('purchase_no', '=', $purchase_no)
                    ->where('id', '!=', $id)
                    ->get();
            if (count($get_inv_no) > 0) {
                $resVal['message'] = 'Purchase Invoice No Already Exists';
                $resVal['success'] = FALSE;
                return $resVal;
            } else {
                $purchase_no = $purchase_no;
                $purchase_code = $prefix . $purchase_no;
            }
        } else {
            $get_inv_no = DB::table('tbl_purchase_invoice')
                    ->select('purchase_no')
                    ->where('is_active', '=', 1)
                    ->where('prefix', '=', $prefix)
                    ->where('id', '!=', $id)
                    ->orderBy('purchase_no', 'desc')
                    ->first();
            if (isset($get_inv_no)) {
                $inv_no = $get_inv_no->purchase_no;
                $inv_no = $inv_no + 1;
            } else {
                $inv_no = 1;
            }
            $purchase_no = $inv_no;
            $purchase_code = $prefix . $inv_no;
        }


        $builder = DB::table('tbl_purchase_invoice_item')->where('purchase_invoice_id', $id)->where('sales_qty', '!=', '0')->get();

        if (count($builder) > 0) {
            $resVal = array();
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Purchase Invoice Was Not Updated Because Sales Was Done For This Purchase';
            return $resVal;
        } else {
            $type_check = GeneralHelper::checkConfiqSetting();
            if (count($builder) == 0) {
                GeneralHelper::StockAdjustmentDelete($request->input('item'), 'pupdate', $id);
                $itemDelete = DB::table('tbl_purchase_invoice_item')->where('purchase_invoice_id', $id)->delete();

                if ($type_check == 'purchase based') {
                    $builder = DB::table('tbl_bcn')
                            ->where('purchase_invoice_id', '=', $id)
                            ->delete();
                }
            } else {
                if ($type_check == 'purchase based') {
                    $itemDelete = DB::table('tbl_purchase_invoice_item')
                                    ->select('id')
                                    ->where('purchase_invoice_id', $id)
                                    ->whereRaw(" (is_barcode_generated = 0 or is_barcode_generated IS NULL)")
                                ->get();

                    foreach ($itemDelete as $del) {
                        $builder = DB::table('tbl_bcn')
                                ->where('purchase_invoice_id', '=', $id)
                                ->where('ref_id', '=', $del->id)
                                ->delete();
                    }
                    $itemDelete = DB::table('tbl_purchase_invoice_item')->where('purchase_invoice_id', $id)
                                    ->where('purchase_invoice_id', $id)
                                    ->whereRaw(" (is_barcode_generated = 0 or is_barcode_generated IS NULL)")->delete();
                }
            }

            $taxDelete = DB::table('tbl_purchase_invoice_tax')->where('invoice_id', $id)->delete();



            $invoiceItemCol = $request->input('item');
            $purchaseinvoice->fill($request->all());
            $purchaseinvoice->purchase_code=$purchase_code;
            $purchaseinvoice->purchase_no=$purchase_no;
            $purchaseinvoice->save();
            PurchaseinvoiceController::calculateTotal($id, $invoiceItemCol, 'update');

            $purchaseItemArray = Purchaseinvoiceitem::where('purchase_invoice_id', "=", $purchaseinvoice->id)->get();

            PurchaseinvoiceController::calculateTotal($purchaseinvoice->id, $purchaseItemArray, 'delete');

            $attributeCollection = DB::table('tbl_attribute_types')->where('code', 'purchaseinvoice')->get();
            $attribute = $attributeCollection->first();

            if (count($attributeCollection) > 0) {


                $builder = DB::table('tbl_attribute_value')
                        ->where('attributetype_id', '=', $attribute->id)
                        ->where('refernce_id', '=', $id)
                        ->delete();
                //print_r($builder);
            }
            if ($request->input('customattribute') != null) {
                $customerItemCol = ($request->input('customattribute'));
                $attribute = array();
                foreach ($customerItemCol as $item) {
                    $attribute[$item['attribute_code']] = $item['value'];
                }
                $json_att = json_encode($attribute);
                $purchaseinvoice->custom_attribute_json = $json_att;



                foreach ($customerItemCol as $attribute) {
                    //print_r($attribute);

                    $customerattribute = new Attributevalue;

                    $customerattribute->refernce_id = $purchaseinvoice->id;
                    $customerattribute->value = $attribute['value'];

                    $customerattribute->attribute_code = $attribute['attribute_code'];

                    if ($attribute['attribute_code'] != NULL) {


                        $attributeCollection = Attribute::where('attribute_code', "=", $attribute['attribute_code'])->get();

                        $attribute = $attributeCollection->first();
                        //print_r($attribute->id);
                        $customerattribute->attributes_id = $attribute->id;
                        $customerattribute->attributetype_id = $attribute->attributetype_id;
                    }


                    $customerattribute->save();
                }
            }
            $builder = DB::table('tbl_customer')
                            ->select('*')
                            ->where('id', '=', $purchaseinvoice->customer_id)->first();
            $cus_name = $builder->fname . " " . $builder->lname;

            $voucher_number = $purchaseinvoice->id;

                $builder1 = DB::table('tbl_transaction')
                            ->select('*')
                            ->where('voucher_number', '=', $voucher_number)
                            ->where('voucher_type', '=', 'purchase_invoice')
                            ->first();

                $id1 = $builder1->id;

                $mydate = $purchaseinvoice->date;
           $transactionData = array(
             "id"=>$id1,
             "name" => $currentuser->f_name,
             "transaction_date" => $purchaseinvoice->date,
             "voucher_type" => Transaction::$PURCHASE,
             "voucher_number" => $purchaseinvoice->id,
             "credit" => $purchaseinvoice->total_amount,
             "debit" => 0.00,
             "pmtcode" => GeneralHelper::getPaymentAcode($request->input('account_id', '')),
             "account_category" => '',
             "acode" => GeneralHelper::getCustomerAcode($purchaseinvoice->customer_id),
             "acoderef" => $purchaseinvoice->customer_id,
             "account" => $request->input('account', ''),
             "pmtcoderef" => $request->input('account_id', ''),
             "is_cash_flow" => 0,
             "customer_id" => $purchaseinvoice->customer_id,
             "payment_mode" => '',
             "particulars" => 'Purchase invoice '.$purchase_code.' from ' . $cus_name . ' by ' . $currentuser->f_name . ' ' . $currentuser->l_name,
             "is_active" => 1,
             "created_by" => $currentuser->id,
             "updated_by" => $currentuser->id,
             "created_by_name" => $currentuser->f_name . ' ' . $currentuser->l_name,
             "updated_by_name" => $currentuser->f_name . ' ' . $currentuser->l_name,
             "prefix" => $purchaseinvoice->prefix,

        );
        TransactionHelper::transactionSave($transactionData);
            // Transaction entry for invoice
//            $transaction = new Transaction;
//            $mydate = $purchaseinvoice->date;
//            $month = date("m", strtotime($mydate));
//            $transaction->fiscal_month_code = $month;
//
//            $fiscalMonthCollection = DB::table('tbl_fiscal_month')->where('code', "=", $month)->get();
//            if ($fiscalMonthCollection->count() > 0) {
//                $fiscalMonth = $fiscalMonthCollection->first();
//                $transaction->fiscal_month_code = $fiscalMonth->id;
//            }
//
//            $year = date("Y", strtotime($mydate));
//            $fiscalYearCollection = DB::table('tbl_fiscal_year')->where('is_active', 1)->get();
//
//            $transaction->fiscal_year_id = 0;
//            $transaction->fiscal_year_code = $year;
//            if ($fiscalYearCollection->count() > 0) {
//                $fiscalYear = $fiscalYearCollection->first();
//                $transaction->fiscal_year_id = $fiscalYear->id;
//                $transaction->fiscal_year_code = $fiscalYear->code;
//            }
//
//            $transaction->transaction_date = $purchaseinvoice->date;
//            $transaction->voucher_type = Transaction::$PURCHASE;
//            $transaction->voucher_number = $purchaseinvoice->id;
//            $transaction->credit = $purchaseinvoice->total_amount;
//            $transaction->debit = 0.00;
//            $transaction->account_category = '';
//
//            $transaction->acode = GeneralHelper::getCustomerAcode($purchaseinvoice->customer_id);
//            $transaction->acoderef = $purchaseinvoice->customer_id;
//            $transaction->account = $request->input('account', '');
//            $transaction->pmtcode = GeneralHelper::getPaymentAcode($request->input('account_id', ''));
//            $transaction->pmtcoderef = $request->input('account_id', '');
//            $transaction->is_cash_flow = 0;
//
//
//            $transaction->payment_mode = '';
//            $transaction->customer_id = $purchaseinvoice->customer_id;
//            //$transaction->particulars = '#' . $purchaseinvoice->id . 'Purchase invoice Created for ' . $cus_name . ' by ' . $currentuser->f_name . ' ' . $currentuser->l_name;
//            $transaction->particulars = 'Purchase invoice '.$purchase_code.' from ' . $cus_name . ' by ' . $currentuser->f_name . ' ' . $currentuser->l_name;
//            $transaction->is_active = 1;
//            $transaction->created_by = $currentuser->id;
//            $transaction->updated_by = $currentuser->id;
//            $transaction->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
//            $transaction->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
//            $transaction->prefix = $purchaseinvoice->prefix;
//            $transaction->save();
            $totalamount = $purchaseinvoice->total_amount;


            $payment = DB::table('tbl_purchase_payment')
                    ->where('purchase_invoice_id', '=', $id)
                    ->where('is_active', '=', 1)
                    ->sum('amount');

            if ($payment == 0) {
                $purchaseinvoice->status = 'unpaid';
            } else if ($totalamount <= $payment) {

                $purchaseinvoice->status = 'paid';
            } else if ($totalamount >= $payment) {

                $purchaseinvoice->status = 'partiallyPaid';
            }

            $purchaseinvoice->paid_amount = $payment;
            $purchaseinvoice->save();

            ActivityLogHelper::purchaseinvoiceModify($purchaseinvoice);
            GeneralHelper::StockAdjustmentSave($request->input('item'), "update", $id,$purchaseinvoice->date);
            //GeneralHelper::InvoiceTaxUpdate($id, $request->input('tax_id'), $taxable_amount, $subtotal, 'purchase');
        }
        LogHelper::info1('Purchase Invoice Update' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Purchase Invoice', 'update', $screen_code, $purchaseinvoice->id);
        return $resVal;
    }

    public function payablereport_csvexport(Request $request) {
        $payableLists = $this->payable_list($request);
        ini_set('max_execution_time', 600); //increase max_execution_time to 10 min if data set is very large
        //create a file
        $filename = "payablereport" . "_" . time() . ".csv";
        //  $filename = "customer" . "_" . date("Y-m-d_H-i", time()) . ".csv";
        $serverName = $_SERVER['SERVER_NAME'];

        $filePath = "download/$serverName/csv";

        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);

            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {

                if ($folderName == '')
                    $folderName = $folderList[$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index];

                if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        $filePath = getcwd() . '/' . $filePath;
        $sr = 1;
        $output = fopen($filePath . "/" . $filename, 'w');
        fputcsv($output, array('#', 'Customer Name','City', 'Phone', 'Email','Qty', 'Amount'));
        foreach ($payableLists['list'] as $values) {
            $ph=explode(',',$values->phone);
                 $phNo=implode(', ',$ph);
            fputcsv($output, array($sr, $values->customer_fname, $values->billing_city, $phNo, $values->email,$values->qty, $values->balance));
            $sr++;
        }
        LogHelper::info1('Payable Report CSV Download' . $request->fullurl(), $request->all());
        return response()->download($filePath . "/" . $filename, 'payablereport.csv');
    }

    public function purchaseInvoiceItemDelete(Request $request,$id) {
        $resVal = array();
        $screen_code = $request->header('screen-code');
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Purchase Invoice Item Deleted Successfully';

        try {
            $purchaseinvoiceitem = Purchaseinvoiceitem::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Purchase Invoice Item  Not Found';
            return $resVal;
        }

        //This is for Maintain purchase inventory
        $new_array = array();
        $builder = DB::table('tbl_purchase_invoice_item')->where('id', $id)->get();
        $purchase_invoice_id = $purchaseinvoiceitem->purchase_invoice_id;
        $array = json_decode(json_encode($builder), True);

        //array_push($new_array,$builder);
        GeneralHelper::StockAdjustmentDelete($array, 'item_delete', $id);

        $type = GeneralHelper::checkConfiqSetting();

        if ($type == 'purchase based') {
            $labelingitems = Bcn::where('ref_id', "=", $id)
                    ->where('purchase_invoice_id', "=", $purchase_invoice_id)
                    ->first();
            $labelingitems->delete();
        }
        $purchaseinvoiceitem->delete();

        $purchaseItemArray = Purchaseinvoiceitem::where('purchase_invoice_id', "=", $purchase_invoice_id)->get();

        PurchaseinvoiceController::calculateTotal($purchase_invoice_id, $purchaseItemArray, 'delete');

        //Delete item tax 
         DB::table('tbl_purchase_invoice_tax')
                ->where('purchase_invoice_item_id', "=", $id)
                ->delete();
            
        $currentuser = Auth::user();

        $purchaseinvoice =  DB::table('tbl_purchase_invoice')
                        ->select('*')
                        ->where('id', '=', $purchaseinvoiceitem->purchase_invoice_id)
                        ->first();
        
        $voucher_number = $purchaseinvoice->id;

        // Update Transaction
        
        $builder1 = DB::table('tbl_transaction')
                    ->select('*')
                    ->where('voucher_number', '=', $voucher_number)
                    ->where('voucher_type', '=', 'purchase_invoice')
                    ->first();

        $id1 = $builder1->id;
        $purchase_code =$purchaseinvoice->prefix.$purchaseinvoice->purchase_no;
        
         $builder = DB::table('tbl_customer')
                        ->select('*')
                        ->where('id', '=', $purchaseinvoice->customer_id)->first();
        $cus_name = $builder->fname . " " . $builder->lname;
            
        $mydate = $purchaseinvoice->date;
           $transactionData = array(
             "id"=>$id1,
             "name" => $currentuser->f_name,
             "transaction_date" => $purchaseinvoice->date,
             "voucher_type" => Transaction::$PURCHASE,
             "voucher_number" => $purchaseinvoice->id,
             "credit" => $purchaseinvoice->total_amount,
             "debit" => 0.00,
             "account_category" => '',
             "acode" => GeneralHelper::getCustomerAcode($purchaseinvoice->customer_id),
             "acoderef" => $purchaseinvoice->customer_id,
             "is_cash_flow" => 0,
             "customer_id" => $purchaseinvoice->customer_id,
             "payment_mode" => '',
             "particulars" => 'Purchase invoice '.$purchase_code.' from ' . $cus_name . ' by ' . $currentuser->f_name . ' ' . $currentuser->l_name,
             "is_active" => 1,
             "created_by" => $currentuser->id,
             "updated_by" => $currentuser->id,
             "created_by_name" => $currentuser->f_name . ' ' . $currentuser->l_name,
             "updated_by_name" => $currentuser->f_name . ' ' . $currentuser->l_name,
             "prefix" => $purchaseinvoice->prefix,

        );
        TransactionHelper::transactionSave($transactionData);

        LogHelper::info1('Purchase Invoice Item Delete ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Purchase Invoice Item ', 'delete', $screen_code, $purchaseinvoiceitem->id);
        return $resVal;
    }

    public function calculateTotal($id, $request, $code) {
        $subtotal = 0;
        $discount_amount1 = 0;
        $roundoff = 0;
        $tax_id = 0;
        $tax_amount = 0;
        $taxable_amount = 0;
        $total_before_roundoff = 0;
        $total = 0;
        $overAllTax = 0.00;


        $purchaseinvoice = Purchaseinvoice::findOrFail($id);
//        $overAllDiscountAmount=$purchaseinvoice->discount_amount/$purchaseinvoice->overall_qty;

        foreach ($request as $item) {

            if ($code == 'update') {
                $purchaseinvoiceitems = new Purchaseinvoiceitem;
                $purchaseinvoiceitems->purchase_invoice_id = $id;
                $purchaseinvoiceitems->fill($item);
                $purchaseinvoiceitems->is_active = 1;
                $purchaseinvoiceitems->save();

                $type = GeneralHelper::checkConfiqSetting();
                if ($type == 'purchase based')
                    PurchaseinvoiceController::insertLabelingItem($purchaseinvoiceitems);


            } else {
                $purchaseinvoiceitems = Purchaseinvoiceitem::where('id', "=", $item['id'])
                        ->where('is_active', "=", 1)
                        ->first();
            }
            $purchaseinvoiceitems->customer_id = $purchaseinvoice->customer_id;
            $purchaseinvoiceitems->purchase_value = $purchaseinvoiceitems->qty * $purchaseinvoiceitems->purchase_price;
            $sub_total = $purchaseinvoiceitems->qty * $purchaseinvoiceitems->purchase_price;
            ;
            $subtotal+= $sub_total;

            //Discount calcualtion start
            if (empty($purchaseinvoiceitems->discount_mode) || empty($purchaseinvoiceitems->discount_value)) {
                $purchaseinvoiceitems->discount_amount = 0;
                $purchaseinvoiceitems->discount_mode = "";
                $purchaseinvoiceitems->discount_value = 0;
                $discount_amount = 0;
            } else {

                if ($purchaseinvoiceitems->discount_mode == 'amount') {
                    $purchaseinvoiceitems->discount_amount = $purchaseinvoiceitems->discount_value;
                    $discount_amount = $purchaseinvoiceitems->discount_value;
                     $discount_amount1+=$discount_amount;
                } else {

                    $discount_amount = $purchaseinvoiceitems->purchase_value * ($purchaseinvoiceitems->discount_value / 100);
                    $discount_amount = floatval(number_format($discount_amount, 2, '.', ''));
                    $purchaseinvoiceitems->discount_amount = $discount_amount;
                     $discount_amount1+=$discount_amount;
                }
            }


            $purchaseinvoiceitems->duedate = $purchaseinvoice->duedate;
            $purchaseinvoiceitems->paymentmethod = $purchaseinvoice->paymentmethod;
            $purchaseinvoiceitems->qty_in_hand = $purchaseinvoiceitems->qty;
            $purchaseinvoiceitems->save();



            $purchasePrice=$purchaseinvoiceitems->purchase_price-$purchaseinvoiceitems->discount_amount;
            $mrpPrice=$purchaseinvoiceitems->mrp_price-$purchaseinvoiceitems->discount_amount;
             //HscCode Based TaxIds
            $purchaseTaxId= GeneralHelper::hsnCodeTaxId($purchaseinvoiceitems->hsn_code,$purchasePrice);
            $sellingTaxId= GeneralHelper::hsnCodeTaxId($purchaseinvoiceitems->hsn_code,$mrpPrice);
           
          $purchaseinvoiceitems->tax_id=$purchaseTaxId;
          $purchaseinvoiceitems->selling_tax_id=$sellingTaxId;
            //Tax Calcualtion Starts
            if ($code == 'update')
            GeneralHelper::purchaseinvoiceItemTax($purchaseinvoiceitems);
            $tax_amount+= $purchaseinvoiceitems->tax_amount;

            $purchaseinvoiceitems->total_price = ($purchaseinvoiceitems->purchase_value - $discount_amount) + $purchaseinvoiceitems->tax_amount;
            $purchaseinvoiceitems->save();
        }

        $packing_charge = $purchaseinvoice->packing_charge;
        $insurance_charge = $purchaseinvoice->insurance_charge;

        //finding roundoff value
        $total_before_roundoff = $subtotal - $discount_amount1 + $tax_amount + $packing_charge + $insurance_charge;
        $total_after_roundoff = round($total_before_roundoff);
        $roundoff = $total_after_roundoff - $total_before_roundoff;
        $roundoff = floatval(number_format($roundoff, 2, '.', ''));

        //finding grant total value
        $total = $subtotal - $discount_amount1 + $tax_amount + $roundoff + $packing_charge + $insurance_charge;
        $total = floatval(number_format($total, 2, '.', ''));



        $purchaseinvoice->subtotal = $subtotal;
        $purchaseinvoice->tax_amount = $tax_amount;
        $purchaseinvoice->discount_amount = $discount_amount1;
        $purchaseinvoice->round_off = $roundoff;

        $purchaseinvoice->total_amount = $total;

        $purchaseinvoice->save();
    }

    public function insertLabelingItem($item) {

        $labelingitem = new Bcn;
        $labelingitem->ref_id = $item->id;
        $labelingitem->purchase_invoice_id = $item->purchase_invoice_id;
        $labelingitem->ref_type = 'purchase based';
        $labelingitem->product_id = $item->product_id;
        $labelingitem->product_name = $item->product_name;
        $labelingitem->product_sku = $item->product_sku;
        $labelingitem->code = $item->code;
        $labelingitem->qty = $item->qty;
        $labelingitem->qty_in_hand = $item->qty;
        $labelingitem->sales_qty = 0;
        $labelingitem->purchase_qty = 0;
        $labelingitem->selling_price = $item->selling_price;
        $labelingitem->purchase_price = $item->purchase_price;
        $labelingitem->mrp_price = $item->mrp_price;
        $labelingitem->status = 'product';
        $labelingitem->discount_mode = $item->discount_mode;
        $labelingitem->discount_value = $item->discount_value;
        $labelingitem->discount_amount = $item->discount_amount;
        $labelingitem->code = $item->code;
        $labelingitem->uom_id = $item->uom_id;
        $labelingitem->is_active = $item->is_active;
        $labelingitem->uom_name = $item->uom_name;

        $currentuser = Auth::user();
        $labelingitem->created_by = $currentuser->id;
        $labelingitem->updated_by = $currentuser->id;

        $labelingitem->save();
        $purchaseinvoiceid = $labelingitem->purchase_invoice_id;
        PurchaseinvoiceController::generateBarCode($purchaseinvoiceid);

    }

   public static function generateBarCode($id) {

        $settings = DB::table('tbl_app_config')
                ->select('value')->where('setting', '=', 'sales_product_list')
                ->first();

        $type = $settings->value;

        if ($type == 'purchase based') {
            $resVal = BcnController::barcodeForPurchaseBased($id);
        } else {
            $resVal = BcnController::barcodeForProductBased($id);
        }
        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $purchase_code = $request->input('purchase_code');
        $customerid = $request->input('customer_id', '');
        $mobile = $request->input('mobile', '');
        $fromDate = $request->input('from_duedate', '');
        $toDate = $request->input('to_duedate', '');
        $status = $request->input('status');
        $fromdate = $request->input('from_date');
        $todate = $request->input('to_date');
        $invoice_date = $request->input('invoice_date');
        $due_date = $request->input('due_date');
        $discountpercentage = $request->input('discount_percentage');
        $is_active = $request->input('is_active', '');
        $mode = $request->input('mode');
        //$status = $request->input('status', '');
//        $builder = DB::table('tbl_purchase_invoice')
//                ->select('*');

        $builder = DB::table('tbl_purchase_invoice as i')
                  ->leftJoin(DB::raw("(select sum(qty) as qty,(CASE WHEN sales_qty > '0' THEN 1 ELSE 0 END) AS is_sale,purchase_invoice_id from tbl_purchase_invoice_item group by purchase_invoice_id) as pi on pi.purchase_invoice_id=i.id")
                  , function($join) {
                })
                ->leftJoin('tbl_customer as c', 'i.customer_id', '=', 'c.id')
                ->leftJoin('tbl_tax as t', 'i.tax_id', '=', 't.id')
                ->select('i.*', 'c.fname as customer_fname', 'c.lname as customer_lname', 't.tax_name as taxname', 't.tax_percentage as taxpercentage','pi.qty','pi.is_sale');

        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('i.id', '=', $id);
        }
        if (!empty($purchase_code)) {
            $builder->where('i.purchase_code', 'like', '%' . $purchase_code . '%');
        }
        if (!empty($customerid)) {
            $builder->where('i.customer_id', '=', $customerid);
        }
        if (!empty($mobile)) {
            $builder->where('c.phone', '=', $mobile);
        }

        if (!empty($fromDate)) {

            $builder->whereDate('i.duedate', '>=', $fromDate);
        }
        if (!empty($toDate)) {

            $builder->whereDate('i.duedate', '<=', $toDate);
        }
        if (!empty($fromdate)) {

            $builder->whereDate('i.date', '>=', $fromdate);
        }
        if (!empty($todate)) {

            $builder->whereDate('i.date', '<=', $todate);
        }
        if (!empty($invoice_date)) {

            $builder->whereDate('i.date', '=', $invoice_date);
        }
        if (!empty($due_date)) {

            $builder->whereDate('i.duedate', '=', $due_date);
        }
        if (!empty($status)) {
            $builder->where('i.status', '=', $status);
        }
        if (!empty($discountpercentage)) {
            $builder->where('i.discount_percentage', '=', $discountpercentage);
        }

        if ($is_active != '') {
            $builder->where('i.is_active', '=', $is_active);
        }

        $arrays[] = $request->toArray();
        //     print_r($arrays);
        $arrays2 = $arrays[0];
        $array_key = array_keys($arrays2);
        $custom_array = array();
        for ($index = 0; $index < count($array_key); $index ++) {
            $arry_ind = $array_key[$index];
            $result = substr($arry_ind, 0, 8);
            if ($result == 'cus_attr') {
                array_push($custom_array, $arry_ind);
            }
        }
        for ($index = 0; $index < count($custom_array); $index ++) {
            $status1 = $custom_array[$index];
            $status = $arrays2[$status1];
            if (!empty($status)) {
                $status = explode("::", $status);
                $key = $status[0];
                $value = $status[1];
                $str = "'%" . '"' . $key . '":"' . $value . '"%' . "'";
                $builder->whereRaw('i.custom_attribute_json  like' . "'%" . '"' . $key . '":"' . $value . '"%' . "'");
                //  $builder->Where ('custom_attribute_json', 'like',  $str);
            }
        }


        if ($mode == 0 || $mode == '') {
            $builder->orderBy('i.id', 'desc');
        } else {
            $builder->orderBy('i.date', 'asc');
        }

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $purchaseinvoiceCollection = $builder->get();
        } else {

            $purchaseinvoiceCollection = $builder->skip($start)->take($limit)->get();
        }
        foreach ($purchaseinvoiceCollection as $invoice) {
            $json_att = $invoice->custom_attribute_json;

            if (empty($json_att)) {
                $json_att = "{}";
            }
            $json_att = json_decode($json_att);
            $invoice->custom_attribute_json = $json_att;
        }
        $resVal['list'] = $purchaseinvoiceCollection;
        LogHelper::info1('Purchase Invoice List All' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }

    public function payable_list(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $customerid = $request->input('customer_id', '');
        $fromdate = $request->input('from_date');
        $todate = $request->input('to_date');
        $currentDate=date('Y-m-d');

        $builder = DB::table('tbl_purchase_invoice as i')
                ->leftjoin(DB::raw("(select Coalesce(sum(it.qty),0) as qty,it.purchase_invoice_id  from tbl_purchase_invoice_item as it left join tbl_purchase_invoice as i on i.id=it.purchase_invoice_id and i.is_active=1 where date(`i`.`date`) >= '".$fromdate."' and date(`i`.`date`) <= '".$todate."' and i.status != 'paid' group by i.customer_id) as it on it.purchase_invoice_id=i.id")
                        ,function($join){
                })
                ->leftJoin('tbl_customer as c', 'i.customer_id', '=', 'c.id')
                ->leftJoin('tbl_tax as t', 'i.tax_id', '=', 't.id')
                ->select('c.id','it.qty', 'c.fname as customer_fname','c.customer_code', 'c.lname as customer_lname', 'c.company', 'c.phone', 'c.email', 'c.billing_address', 'c.shopping_address', 'c.billing_city', 'c.shopping_city', 'c.shopping_state', 'c.billing_state', 'c.billing_country', 'c.shopping_pincode', 'c.billing_pincode', 'c.billing_country', 'c.is_sale', 'c.is_purchase', DB::raw('Coalesce(sum(i.total_amount) - sum(i.paid_amount),0) as balance'))
                ->where('i.status', '!=', 'paid')
                ->where('i.is_active', '=', '1')
                ->groupBy('c.id');

        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);


        if (!empty($customerid)) {
            $builder->where('i.customer_id', '=', $customerid);
        }

        if (!empty($fromdate)) {

            $builder->whereDate('i.date', '>=', $fromdate);
        }
        if (!empty($todate)) {

            $builder->whereDate('i.date', '<=', $todate);
        }

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $collection = $builder->get();
        } else {
            $collection = $builder->skip($start)->take($limit)->get();
        }
        foreach ($collection as $purchase) {
            $cust_id = $purchase->id;

            $detail = DB::table('tbl_purchase_invoice as i')
                    ->leftjoin(DB::raw("(select Coalesce(sum(it.qty),0) as qty,it.purchase_invoice_id from tbl_purchase_invoice_item as it left join tbl_purchase_invoice as i on i.id=it.purchase_invoice_id and i.is_active=1 where date(`i`.`date`) >= '".$fromdate."' and date(`i`.`date`) <= '".$todate."' and i.status != 'paid' group by it.purchase_invoice_id) as it on it.purchase_invoice_id=i.id")
                            ,function($join){
                })
                    ->select('i.*', DB::raw('Coalesce(i.total_amount-i.paid_amount,0)  as balance'),DB::raw("(Case When datediff(i.duedate,'".$currentDate."') < 0 Then datediff(i.duedate,'".$currentDate."') else '' End) as diff_date"),'it.qty')
                    ->where('i.status', '!=', 'paid')
                    ->where('i.is_active','=',1);

            if (!empty($cust_id)) {
                $detail->where('i.customer_id', '=', $cust_id);
            }

            if (!empty($fromdate)) {

                $detail->whereDate('i.date', '>=', $fromdate);
            }
            if (!empty($todate)) {

                $detail->whereDate('i.date', '<=', $todate);
            }

            $purchase->purchase_details = $detail->get();
        }
        $resVal['list'] = $collection;
        LogHelper::info1('Payable List' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $currentuser = Auth::user();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Purchase Invoice Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        try {
            $purchaseinvoice = Purchaseinvoice::findOrFail($id);
            if ($purchaseinvoice->is_active == 0) {
                return $resVal;
            }
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Purchase Invoice Not Found';
            return $resVal;
        }
        $type = GeneralHelper::checkConfiqSetting();
        if ($type == 'purchase based') {


            $query = DB::table('tbl_purchase_invoice_item')->select('id')
                    ->Where('purchase_invoice_id', '=', $id)
                    ->where('is_active', 1);

            $pinvoice_return = DB::table('tbl_purchase_invoice_item_return')->select('id')
                    ->where('is_active', 1)
                    ->whereIn('purchase_invoice_item_id', $query)
                    ->get();

            $pinvoice_stockadj = DB::table('tbl_stock_adjustment_detail')->select('*')
                            ->where('purchase_invoice_id', $id)->where('is_active', 1)->get();

            $pinvoice_deliverynote = DB::table('tbl_delivery_note_detail')->select('*')
                    ->where('is_active', 1)
                    ->whereIn('ref_id', $query)
                    ->get();

            if (count($pinvoice_return) > 0) {
                 $resVal['success'] = FALSE;
                $resVal['message'] = "Purchase Was Returned So Can't Able To Delete";
                return $resVal;
            }
            if (count($pinvoice_stockadj) > 0) {
                 $resVal['success'] = FALSE;
                $resVal['message'] = "Stock Adjustment Done For This Purchase So Can't Able To Delete";
                return $resVal;
            }
            if (count($pinvoice_deliverynote) > 0) {
                 $resVal['success'] = FALSE;
                $resVal['message'] = "Delivery Note Done For This Purchase So Can't Able To Delete";
                return $resVal;
            }
        }
        $builder = DB::table('tbl_purchase_invoice_item')->where('purchase_invoice_id', $id)->where('sales_qty', '!=', '0')->get();
        if (count($builder) > 0) {
            $resVal = array();
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Purchase Invoice Was Not Updated Because Sales Was Done For this Purchase';
            return $resVal;
        }
//
//        $builderBarcode = DB::table('tbl_purchase_invoice_item')->where('purchase_invoice_id', $id)->where('is_barcode_generated', '=', 1)->get();
//        if (count($builderBarcode) > 0) {
//            $resVal = array();
//            $resVal['success'] = FALSE;
//            $resVal['message'] = 'Purchase invoice was not updated because barcode generated for this purchase';
//            return $resVal;
//        }
        $paymentCollection = PurchasePayment::where('purchase_invoice_id', '=', $id)->where('is_active', '=', 1)->get();
        if ($paymentCollection->count() > 0) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Unable To Delete, Please Remove The Payment Transaction';
            return $resVal;
        }
        $trans_id = DB::table('tbl_transaction')
                ->select('*')
                ->where('voucher_number', '=', $id)
                ->where('voucher_type', '=', 'purchase_invoice')
                ->where('is_active','=',1)
                ->first();
        $id2 = $trans_id->id;
        $transaction = Transaction::findOrFail($id2);
        $transaction->updated_by = $currentuser->id;
        $transaction->is_active = 0;
        $transaction->save();

        $type_check = GeneralHelper::checkConfiqSetting();

        if ($type_check == 'purchase based') {

            DB::table('tbl_bcn')->where('purchase_invoice_id', $id)->update(['is_active' => 0]);


        }


        $purchaseinvoiceitem = DB::table('tbl_purchase_invoice_item')->where('purchase_invoice_id', $id)->update(['is_active' => 0]);
        $taxDelete = DB::table('tbl_purchase_invoice_tax')->where('invoice_id', $id)->update(['is_active' => 0]);
        $purchaseinvoice->is_active = $request->input('is_active', 0);
        //$purchaseinvoice->fill($request->all());
        $purchaseinvoice->update();
        $currentuser = Auth::user();

        $resVal['id'] = $purchaseinvoice->id;
        ActivityLogHelper::purchaseinvoiceDelete($purchaseinvoice);
        GeneralHelper::StockAdjustmentDelete($request->input('item'), 'pdelete', $purchaseinvoice->id);
        GeneralHelper::InvoiceTaxDelete($id, 'purchase');
        LogHelper::info1('Purchase Invoice Delete ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Purchase Invoice', 'delete', $screen_code, $purchaseinvoice->id);
        return $resVal;
    }

    public function detail(Request $request) {
        $current_date = Carbon::now();
        $current_date = $current_date->format('Y-m-d');
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $id = $request->input('id', '');


        $isactive = $request->input('isactive', '');
        $discountpercentage = $request->input('discount_percentage', '');

        $decimalFormat= GeneralHelper::decimalFormat();
        //$builder = DB::table('tbl_invoice')->select('*');
        $builder = DB::table('tbl_purchase_invoice as i')
                ->leftJoin('tbl_customer as c', 'i.customer_id', '=', 'c.id')
                ->leftJoin('tbl_tax as t', 'i.tax_id', '=', 't.id')
                ->select('i.*', 'c.fname as customer_fname', 'c.lname as customer_lname', 't.tax_name as taxname', 'c.phone', 'c.email'
                        , 't.tax_percentage as taxpercentage')
                ->where('i.id', '=', $id);


        $purchaseinvoiceitems = DB::table('tbl_purchase_invoice_item')
                ->select('*',DB::raw("cast(qty as DECIMAL(12,".$decimalFormat.")) as qty"),DB::raw("cast(qty_in_hand as DECIMAL(12,".$decimalFormat.")) as qty_in_hand")
                        ,DB::raw("cast(sales_qty as DECIMAL(12,".$decimalFormat.")) as sales_qty"))
                ->where('purchase_invoice_id', $id)
                ->where('is_active', '=', 1);

        $customerattribute = DB::table('tbl_attribute_value')->where('refernce_id', $id);

        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        if (!empty($id)) {
            $builder->where('i.id', '=', $id);
        }
        if (!empty($discountpercentage)) {
            $builder->where('i.discount_percentage', '=', $discountpercentage);
        }

        if ($isactive != '') {
            $builder->where('i.is_active', '=', $isactive);
        }
        $builder->orderBy('i.id', 'desc');

        $invoiceCollection = $builder->skip($start)->take($limit)->get();
        if (count($invoiceCollection) == 1) {

            $purchaseinvoice = $invoiceCollection->first();

            $purchaseinvoice->item = $purchaseinvoiceitems->get();

            $is_sale=0;
            foreach ($purchaseinvoice->item as $p) {
                //   print_r($purchaseinvoiceitems);
                $p_id = $p->id;
                $taxMapping = DB::table('tbl_purchase_invoice_tax')
                                ->select('*')
                                ->where('purchase_invoice_item_id', $p_id)->where('is_active', '=', 1);
                $p->taxMapping = $taxMapping->get();

                if($p->sales_qty >0 && $is_sale==0){
                   $is_sale++;
            }
                 $purchaseinvoice->is_sale=$is_sale;
            }
            $over_all_tax = DB::table('tbl_purchase_invoice_tax')->where('invoice_id', $id)->where('is_active', '=', 1)
                            ->select('id', 'invoice_id', 'tax_id', 'tax_name', 'tax_percentage', 'tax_amount', DB::raw('SUM(tax_amount) as overAllTaxAmt'))
                            // ->sum('tax_amount')
                            ->groupBy('tax_id')->get();
            $purchaseinvoice->overAllTax = $over_all_tax;
            //$invoice->customattribute = $customerattribute->get();
            $customAttributeValue = DB::table('tbl_attribute_value as a')
                            ->leftJoin('tbl_attributes as abs', 'a.attributes_id', '=', 'abs.id')
                            ->select('a.*', 'abs.attribute_label as attribute_label')
                            ->where('a.refernce_id', '=', $id)
                            ->where('abs.attributetype_code', '=', 'purchaseinvoice')->get();

            $customattribute = DB::table('tbl_attributes as abs')
                            ->select('abs.*')
                            ->where('abs.attributetype_code', '=', 'purchaseinvoice')->get();
            if (count($customAttributeValue) > 0) {

                foreach ($customAttributeValue as $item) {

                    foreach ($customattribute as $customattributeItem) {

                        if ($item->attribute_code == $customattributeItem->attribute_code) {

                            $customattributeItem->value = $item->value;
                        }
                    }
                }
            }
            $purchaseinvoice->customattribute = $customattribute;

//            $invoice_tax_list_collection = DB::table('tbl_purchase_invoice_tax')
//                            ->select('*')
//                            ->where('invoice_id', '=', $id)
//                            ->where('is_active', '=', 1)->get();
//
//            $purchaseinvoice->tax_list = $invoice_tax_list_collection;

             $builder = $builder = DB::table('tbl_tax as t')
                    ->leftJoin('tbl_purchase_invoice_tax as tm', 'tm.tax_id', '=', 't.id')
                    ->where('tm.is_active', '=', 1)
                    ->where('tm.invoice_id', '=', $id)
                    ->select(DB::raw("SUBSTRING_INDEX(tm.tax_name, ' ', 1) as name"))
                    ->groupBy('name')
                    ->get();
            
            foreach($builder as $bu)
            {
                 $builder1 =  DB::table('tbl_tax as t')
                    ->leftJoin('tbl_purchase_invoice_tax as tm', 'tm.tax_id', '=', 't.id')
                    ->where('tm.is_active', '=', 1)
                    ->where('tm.invoice_id', '=', $id)
                    ->select(DB::raw('t.tax_name as name ,tm.tax_amount AS taxAmount, tm.tax_percentage as taxPercentage '))
                    ->where('t.tax_name', 'like', '%' . $bu->name . '%')
                     ->get();
                 $bu->taxDetail = $builder1;
            
            }
            
            $purchaseinvoice->tax_detail = $builder;
            
            $resVal['data'] = $purchaseinvoice;
        }
        LogHelper::info1('Purchase Invoice Detail ' . $request->fullurl(), $request->all());
        return ($resVal);
    }

    public function purchasereceipt_csvexport(Request $request) {
        ini_set('max_execution_time', 600); //increase max_execution_time to 10 min if data set is very large
        //create a file
        $filename = "purchasereceiptreport" . "_" . time() . ".csv";
        //  $filename = "customer" . "_" . date("Y-m-d_H-i", time()) . ".csv";
        $serverName = $_SERVER['SERVER_NAME'];

        $filePath = "download/$serverName/csv";

        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);

            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {

                if ($folderName == '')
                    $folderName = $folderList[$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index];

                if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        $filePath = getcwd() . '/' . $filePath;
        $sr = 1;
        $output = fopen($filePath . "/" . $filename, 'w');
        fputcsv($output, array('Purchase Receipt Report'));
        fputcsv($output, array('#', 'BarCode', 'Product Name', 'Quantity', 'Sales Rate', 'Design', 'Color', 'Size'));
        $id = $request->input('id');


        $purchaseinvoiceitems = DB::table('tbl_purchase_invoice_item')->where('purchase_invoice_id', $id)->get();
        foreach ($purchaseinvoiceitems as $values) {

            fputcsv($output, array($sr, $values->product_id, $values->product_name, $values->qty, $values->selling_price, $values->custom_opt2, $values->custom_opt3, $values->custom_opt4));
            $sr++;
        }
        LogHelper::info1('Purchase Receipt CSV Download' . $request->fullurl(), $request->all());
        return response()->download($filePath . "/" . $filename, 'purchaseReceipt.csv');
    }

    public function partywiseStockReport(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }


        $customer_id = $request->input('customer_id');
        $product_id = $request->input('product_id');
        $show_all = $request->input('show_all');
        $category_id=$request->input('category_id','');
        
        $builder = DB::table('tbl_purchase_invoice_item as pin')
                ->leftjoin('tbl_purchase_invoice as pi', 'pi.id', '=', 'pin.purchase_invoice_id')
                ->leftjoin('tbl_product as pr','pr.id','=','pin.product_id')
                ->leftjoin('tbl_category as c','c.id','=','pr.category_id')
                ->select('pin.product_id', 'pin.product_name','pr.category_id','c.name as category_name',DB::raw('Coalesce(sum(pin.qty_in_hand),0) as qty'))
                ->where('pin.is_active', '=', 1)
                ->where('pin.qty_in_hand', '!=', 0)
                ->groupby('pin.product_id');

        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        
        if (!empty($category_id)) {
            $builder->where('pr.category_id', '=', $category_id);
        }
        if (!empty($customer_id)) {
            $builder->where('pi.customer_id', '=', $customer_id);
        }
        if (!empty($product_id)) {
            $builder->where('pin.product_id', '=', $product_id);
        }
        if ($start == 0 && $limit == 0) {
            $collection = $builder->get();
        } else {

            $collection = $builder->skip($start)->take($limit)->get();
        }
        if ($show_all == 1) {
            foreach ($collection as $product) {
                $prod_id = $product->product_id;
                $custom = DB::table('tbl_purchase_invoice_item as pin')
                        ->leftjoin('tbl_purchase_invoice as pi', 'pi.id', '=', 'pin.purchase_invoice_id')
                        ->leftjoin('tbl_customer as c', 'pi.customer_id', '=', 'c.id')
                        ->select('c.id as customer_id', DB::raw('concat(c.fname," ",coalesce(c.lname,"")) as customer_name'), DB::raw('Coalesce(sum(pin.qty_in_hand),0) as overall_qty'), 'c.billing_city', 'c.shopping_city')
                        ->where('pin.is_active', '=', 1)
                        ->where('product_id', '=', $prod_id)
                        ->where('pin.qty_in_hand', '!=', 0)
                        ->groupby('pi.customer_id');

                 if (!empty($customer_id)) {
                 $custom->where('pi.customer_id', '=', $customer_id);
                    }

                $product->customer_detail = $custom->get();
            }
        }

        $resVal['list'] = $collection;
        LogHelper::info1('PartyWiseStock Report ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return $resVal;
    }
    public function partywisestock_csvexport(Request $request) {
        $partywisestockLists = $this->partywiseStockReport($request);
        ini_set('max_execution_time', 600); //increase max_execution_time to 10 min if data set is very large
        //create a file
        $filename = "partywisestockreport" . "_" . time() . ".csv";
        //  $filename = "customer" . "_" . date("Y-m-d_H-i", time()) . ".csv";
        $serverName = $_SERVER['SERVER_NAME'];

        $filePath = "download/$serverName/csv";

        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);

            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {

                if ($folderName == '')
                    $folderName = $folderList[$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index];

                if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        $filePath = getcwd() . '/' . $filePath;
        $output = fopen($filePath . "/" . $filename, 'w');
        fputcsv($output, array('Product id','Product Name','Category Name','Product Quantity'));
        foreach ($partywisestockLists['list'] as $values) {
            fputcsv($output, array($values->product_id,$values->product_name,$values->category_name,$values->qty));
        }
        LogHelper::info1('PartyWiseStock CSV Download' . $request->fullurl(),  $request->all());
        return response()->download($filePath . "/" . $filename, 'partywisestockreport.csv');
    }
    public function partywiseSalesReport(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $customer_id = $request->input('customer_id');
        $product_id = $request->input('product_id');
        $show_all = $request->input('show_all');


        $builder = DB::table('tbl_purchase_invoice_item as pin')
                ->leftjoin('tbl_purchase_invoice as pi', 'pi.id', '=', 'pin.purchase_invoice_id')
                ->leftjoin('tbl_customer as c', 'pi.customer_id', '=', 'c.id')
                ->select('c.id as customer_id', DB::raw('concat(c.fname," ",coalesce(c.lname,"")) as customer_name'), DB::raw('Coalesce(sum(pin.sales_qty),0) as overall_sales_qty'), 'c.billing_city', 'c.shopping_city')
                ->where('pin.is_active', '=', 1)
                ->where('pin.sales_qty', '!=', 0)
                ->groupby('pi.customer_id');


        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($customer_id)) {
            $builder->where('pi.customer_id', '=', $customer_id);
        }
        if (!empty($product_id)) {
            $builder->where('pin.product_id', '=', $product_id);
        }

        if ($start == 0 && $limit == 0) {
            $collection = $builder->get();
        } else {

            $collection = $builder->skip($start)->take($limit)->get();
        }
        if ($show_all == 1) {
            foreach ($collection as $customer) {
                $cust_id = $customer->customer_id;
                $product = DB::table('tbl_purchase_invoice_item as pin')
                        ->leftjoin('tbl_purchase_invoice as pi', 'pi.id', '=', 'pin.purchase_invoice_id')
                        ->select('pin.product_id', 'pin.product_name', DB::raw('Coalesce(sum(pin.sales_qty),0) as sales_qty'))
                        ->where('pin.is_active', '=', 1)
                        ->where('pi.customer_id', '=', $cust_id)
                        ->where('pin.sales_qty', '!=', 0)
                        ->groupby('pin.product_id');

                if (!empty($product_id)) {
                    $product->where('pin.product_id', '=', $product_id);
                }

                $customer->product_detail = $product->get();
            }
        }

        $resVal['list'] = $collection;
        LogHelper::info1('PartyWiseSalesReport ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return $resVal;
    }
     public function partywisesales_csvexport(Request $request) {
        $partywisesalesLists = $this->partywiseSalesReport($request);
        ini_set('max_execution_time', 600); //increase max_execution_time to 10 min if data set is very large
        //create a file
        $filename = "partywisesalesreport" . "_" . time() . ".csv";
        //  $filename = "customer" . "_" . date("Y-m-d_H-i", time()) . ".csv";
        $serverName = $_SERVER['SERVER_NAME'];

        $filePath = "download/$serverName/csv";

        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);

            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {

                if ($folderName == '')
                    $folderName = $folderList[$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index];

                if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        $filePath = getcwd() . '/' . $filePath;
        $output = fopen($filePath . "/" . $filename, 'w');
        fputcsv($output, array('Customer Id','Customer Name', 'City', 'Qty'));
        foreach ($partywisesalesLists['list'] as $values) {
            fputcsv($output, array($values->customer_id,$values->customer_name, $values->billing_city, $values->overall_sales_qty));
        }
        LogHelper::info1('PartyWiseSales CSV Download' . $request->fullurl(),  $request->all());
        return response()->download($filePath . "/" . $filename, 'partywisesalesreport.csv');
    }
    public function invoiceDetail(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $customer_id = $request->input('customer_id');
        $reference_no = $request->input('reference_no');
        $purchase_invoice_id = $request->input('purchase_invoice_id');

        $builder = DB::table('tbl_purchase_invoice')
                        ->where('customer_id', '=', $customer_id)
                        ->where('reference_no', '=', $reference_no)
                        ->where('is_active', '=', 1);

        if(!empty($purchase_invoice_id)){
            $builder->where('id','!=',$purchase_invoice_id);
        }

        $builder=$builder->get();


        if ((count($builder) > 0) && ($reference_no != '') ) {
//                         $resVal['message'] = 'The Reference no  already Exists';
            $resVal['success'] = FALSE;
            return $resVal;
        } else {
//              $resVal['message'] = 'The Reference no  does not  Exists';
            $resVal['success'] = TRUE;
            return $resVal;
        }
        LogHelper::info1('Purchase Invoice Detail ' . $request->fullurl(), $request->all());
    }

    public static function filters($bal_amount, $request) {
        $purchase_code = $request->input('purchase_code', '');
        $customer_id = $request->input('customer_id', '');
        $invoice_date = $request->input('invoice_date', '');
        $duedate = $request->input('due_date', '');
        $status = $request->input('status', '');
        if (!empty($purchase_code)) {
            $bal_amount->where('purchase_code', "=", $purchase_code);
        }
        if (!empty($customer_id)) {
            $bal_amount->where('customer_id', "=", $customer_id);
        }
        if (!empty($invoice_date)) {
            $bal_amount->where('date', "=", $invoice_date);
        }
        if (!empty($duedate)) {
            $bal_amount->where('duedate', "=", $duedate);
        }
        if (!empty($status)) {
            $bal_amount->where('status', "=", $status);
        }
        return $bal_amount;
    }

    public function summary(Request $request) {


        $resVal = array();
        $resVal['message'] = "Success";
        $resVal['success'] = TRUE;
        $ldate = date('Y-m-d');

        $overall = array();
        $bal_amount = DB::table('tbl_purchase_invoice')
                ->whereColumn('paid_amount', "<=", 'total_amount')
                ->where('is_active', "=", 1)
                ->select(DB::raw('sum(total_amount) as overall_amount'));
        $bal_amount1 = PurchaseinvoiceController::filters($bal_amount, $request);
        $overall['invoice_count'] = $bal_amount1->count();
        $bal_amount1 = $bal_amount1->first();
        $overall['amount'] = $bal_amount1->overall_amount;
        $resVal['overall_amount'] = $overall;



        $paid = array();
        $bal_amount = DB::table('tbl_purchase_invoice')
                ->whereColumn('paid_amount', "<=", 'total_amount')
                ->where('is_active', "=", 1)
                ->where('status', "!=", "unpaid")
                ->select(DB::raw('sum(paid_amount) as paid_amount'));
        $bal_amount1 = PurchaseinvoiceController::filters($bal_amount, $request);
        $paid['invoice_count'] = $bal_amount1->count();
        $bal_amount1 = $bal_amount1->first();
        $paid['amount'] = $bal_amount1->paid_amount;
        $resVal['paid_amount'] = $paid;

        $unpaid = array();
        $bal_amount = DB::table('tbl_purchase_invoice')
                ->whereColumn('paid_amount', "<=", 'total_amount')
                ->where('is_active', "=", 1)
                ->where('status', '!=', "paid")
                ->select(DB::raw('sum(total_amount-paid_amount) as unpaid_amount'));
        $bal_amount1 = PurchaseinvoiceController::filters($bal_amount, $request);
        $unpaid['invoice_count'] = $bal_amount1->count();
        $bal_amount1 = $bal_amount1->first();
        $unpaid['amount'] = $bal_amount1->unpaid_amount;
        $resVal['unpaid_amount'] = $unpaid;


        LogHelper::info1('Purchase Invoice Summary' . $request->fullurl(), $resVal['unpaid_amount']);
        return $resVal;
    }

}

?>
