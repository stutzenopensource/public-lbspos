<?php

namespace App\Http\Controllers;

use DB;
use Carbon\Carbon;
use App\ActivityLog;
use App\Transaction;
use App\PurchasePayment;
use App\PurchaseinvoiceReturn;
use App\PurchaseinvoiceitemReturn;
use Illuminate\Http\Request;
use App\Attributevalue;
use App\Attribute;
use App\AdvancePayment;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\GeneralHelper;
use App\Helper\ActivityLogHelper;
use App\Helper\AccountHelper;
//use App\Helper\LogHelper;
use App\Helper\SalesInvoiceHelper;
use App\Helper\LogHelper;
use App\Helper\NotificationHelper;
use App\Helper\TransactionHelper;
use App\PlaceHolders\ReplacePlaceHolderForPurchaseInvoiceReturn;
use App\Helper\PdfHelper;
use App\Helper\ReplaceHolderHelper;


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PurchaseinvoiceController
 *
 * @author Deepa
 */
class PurchaseinvoiceReturnController extends Controller {

    //put your code here
    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = ' Purchase Invoice Return Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

//        $pos_account = DB::table('tbl_app_config')->where('setting', 'pos_bank_account_id')->first();
//        $pos_account_name = '';
//        if (!empty($pos_account)) {
//            $pos_account_name = DB::table('tbl_accounts')->where('id', $pos_account->value)->first();
//        }
//        if (empty($pos_account_name) || empty($pos_account)) {
//          $resVal['message'] = 'Please Create POS Bank Account';
//          $resVal['success'] = FALSE;
//          return $resVal;
//        }

        $purchase_no = $request->input('purchase_no', '');
        $prefix = $request->input('prefix', '');
        $purchase_code = '';
        if (!empty($purchase_no)) {

            $get_inv_no = DB::table('tbl_purchase_invoice_return')
                    ->select('is_active')
                    ->where('prefix', '=', $prefix)
                    ->where('is_active', '=', 1)
                    ->where('purchase_no', '=', $purchase_no)
                    ->get();
            if (count($get_inv_no) > 0) {
                $resVal['message'] = 'Purchase Invoice No Already Exists';
                $resVal['success'] = FALSE;
                return $resVal;
            } else {
                $purchase_no = $purchase_no;
                $purchase_code = $prefix . $purchase_no;
            }
        } else {
            $get_inv_no = DB::table('tbl_purchase_invoice_return')
                    ->select('purchase_no')
                    ->where('is_active', '=', 1)
                    ->where('prefix', '=', $prefix)
                    ->orderBy('purchase_no', 'desc')
                    ->first();
            if (isset($get_inv_no)) {
                $inv_no = $get_inv_no->purchase_no;
                $inv_no = $inv_no + 1;
            } else {
                $inv_no = 1;
            }
            $purchase_no = $inv_no;
            $purchase_code = $prefix . $inv_no;
        }

        $purchaseinvoicereturn = new PurchaseinvoiceReturn;
        $purchaseinvoicereturn->fill($request->all());
        $purchaseinvoicereturn->save();

        $subtotal = 0;
        $discount_amount = 0;
         $discount_amount1 = 0;
        $roundoff = 0;
        $tax_id = 0;
        $tax_amount = 0;
        $taxable_amount = 0;
        $total_before_roundoff = 0;
        $total = 0;

        $invoiceItemCol = $request->input('item');

        $currentuser = Auth::user();


        $purchaseinvoicereturn->created_by = $currentuser->id;
        $purchaseinvoicereturn->updated_by = $currentuser->id;
        $purchaseinvoicereturn->is_active = $request->input('is_active', 1);
        $purchaseinvoicereturn->subtotal = $subtotal;
        $purchaseinvoicereturn->tax_amount = $tax_amount;
        $purchaseinvoicereturn->discount_amount = $discount_amount;
        $purchaseinvoicereturn->round_off = $roundoff;
        $purchaseinvoicereturn->total_amount = $total;

        $purchaseinvoicereturn->purchase_no = $purchase_no;
        $purchaseinvoicereturn->purchase_code = $purchase_code;
        $purchaseinvoicereturn->save();



        foreach ($invoiceItemCol as $item) {

            $purchaseinvoicereturnitems = new PurchaseinvoiceitemReturn;
            $purchaseinvoicereturnitems->fill($item);

            $purchaseinvoicereturnitems->created_by = $currentuser->id;
            $purchaseinvoicereturnitems->updated_by = $currentuser->id;

                $purchaseinvoicereturnitems->purchase_invoice_return_id = $purchaseinvoicereturn->id;
                $purchaseinvoicereturnitems->is_active = 1;
                $purchaseinvoicereturnitems->save();


            $purchaseinvoicereturnitems->customer_id = $purchaseinvoicereturn->customer_id;
            $purchaseinvoicereturnitems->purchase_value = $purchaseinvoicereturnitems->qty * $purchaseinvoicereturnitems->purchase_price;
            $sub_total = $purchaseinvoicereturnitems->qty * $purchaseinvoicereturnitems->purchase_price;
            ;
            $subtotal+= $sub_total;

            //Discount calcualtion start
            if (empty($purchaseinvoicereturnitems->discount_mode) || empty($purchaseinvoicereturnitems->discount_value)) {
                $purchaseinvoicereturnitems->discount_amount = 0;
                $purchaseinvoicereturnitems->discount_mode = "";
                $purchaseinvoicereturnitems->discount_value = 0;
                $discount_amount = 0;
            } else {

                if ($purchaseinvoicereturnitems->discount_mode == 'amount') {
                    $purchaseinvoicereturnitems->discount_amount = $purchaseinvoicereturnitems->discount_value;
                    $discount_amount = $purchaseinvoicereturnitems->discount_value;
                } else {

                    $discount_amount = $purchaseinvoicereturnitems->purchase_value * ($purchaseinvoicereturnitems->discount_value / 100);
                    $discount_amount = floatval(number_format($discount_amount, 2, '.', ''));
                    $purchaseinvoicereturnitems->discount_amount = $discount_amount;
                }
            }
            $discount_amount1+=$discount_amount;

            $purchaseinvoicereturnitems->duedate = $purchaseinvoicereturn->duedate;
            $purchaseinvoicereturnitems->paymentmethod = $purchaseinvoicereturn->paymentmethod;
            $purchaseinvoicereturnitems->qty_in_hand = $purchaseinvoicereturnitems->qty;
            $purchaseinvoicereturnitems->save();

            $tax_amount+= $purchaseinvoicereturnitems->tax_amount;

            $purchaseinvoicereturnitems->total_price = ($purchaseinvoicereturnitems->purchase_value - $discount_amount) + $purchaseinvoicereturnitems->tax_amount;
            $purchaseinvoicereturnitems->save();
        }

        $packing_charge = $purchaseinvoicereturn->packing_charge;
        $insurance_charge = $purchaseinvoicereturn->insurance_charge;

        //finding roundoff value
        $total_before_roundoff = $subtotal - $discount_amount1 + $tax_amount + $packing_charge + $insurance_charge;
        $total_after_roundoff = round($total_before_roundoff);
        $roundoff = $total_after_roundoff - $total_before_roundoff;
        $roundoff = floatval(number_format($roundoff, 2, '.', ''));

        //finding grant total value
        $total = $subtotal - $discount_amount1 + $tax_amount + $roundoff + $packing_charge + $insurance_charge;
        ;
        $total = floatval(number_format($total, 2, '.', ''));



        $purchaseinvoicereturn->subtotal = $subtotal;
        $purchaseinvoicereturn->tax_amount = $tax_amount;
        $purchaseinvoicereturn->discount_amount = $discount_amount1;
        $purchaseinvoicereturn->round_off = $roundoff;

        $purchaseinvoicereturn->total_amount = $total;


        $purchaseinvoicereturn->save();

        $customerItemCol = ($request->input('customattribute'));

        if ($request->input('customattribute') != null) {

            $attribute = array();
            foreach ($customerItemCol as $item) {
                $attribute[$item['attribute_code']] = $item['value'];
            }
            $json_att = json_encode($attribute);
            $purchaseinvoicereturn->custom_attribute_json = $json_att;
            $purchaseinvoicereturn->save();

            foreach ($customerItemCol as $attribute) {
                //print_r($attribute);

                $customerattribute = new Attributevalue;

                $customerattribute->refernce_id = $purchaseinvoicereturn->id;
                $customerattribute->value = $attribute['value'];
                $customerattribute->attribute_code = $attribute['attribute_code'];

                $customerattribute->created_by = $currentuser->id;
                $customerattribute->updated_by = $currentuser->id;
                if ($attribute['attribute_code'] != NULL) {


                    $attributeCollection = Attribute::where('attribute_code', "=", $attribute['attribute_code'])->get();
                    if (count($attributeCollection) > 0) {
                        $attribute = $attributeCollection->first();
                        //print_r($attribute);
                        $customerattribute->attributes_id = $attribute->id;
                        $customerattribute->attributetype_id = $attribute->attributetype_id;
                    }
                }

                //$customerattribute->fill($attribute);

                $customerattribute->save();
            }
        }


        $resVal['id'] = $purchaseinvoicereturn->id;
        //ActivityLogHelper::purchaseinvoiceSave($purchaseinvoicereturn);
        GeneralHelper::StockWastageSave($request->input('item'), 'purchase_return', $purchaseinvoicereturn->id, $purchaseinvoicereturn->date);

//         GeneralHelper::updatePurchaseReturnInventory($request->input('item'));
        //Create Payment Receipt invoice return
        $this->generatePaymentForPurchaseInvoiceReturn($purchaseinvoicereturn);

        // GeneralHelper::InvoiceTaxSave($purchaseinvoicereturn->id, $request->input('tax_id'), $taxable_amount, $subtotal, 'purchase');

        //LogHelper::info('Purchase Invoice Return Save' . $request->fullurl());
        LogHelper::info1('Purchase Return Save' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Purchase Return', 'save', $screen_code, $purchaseinvoicereturn->id);
        return $resVal;
    }


    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $purchase_code = $request->input('purchase_code');
        $purchase_no = $request->input('purchase_no');
        $customerid = $request->input('customer_id', '');
        $mobile = $request->input('mobile', '');
        $fromDate = $request->input('from_duedate', '');
        $toDate = $request->input('to_duedate', '');
        $status = $request->input('status');
        $fromdate = $request->input('from_date');
        $todate = $request->input('to_date');
        $discountpercentage = $request->input('discount_percentage');
        $is_active = $request->input('is_active', '');
        $mode = $request->input('mode');


        $builder = DB::table('tbl_purchase_invoice_return as i')
                ->leftJoin('tbl_customer as c', 'i.customer_id', '=', 'c.id')
                 ->select('i.*', 'c.fname as customer_fname', 'c.lname as customer_lname');

        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('i.id', '=', $id);
        }
        if (!empty($mobile)) {
            $builder->where('c.phone', '=', $mobile);
        }
        if (!empty($purchase_code)) {
            $builder->where('i.purchase_code', 'like', '%' . $purchase_code . '%');
        }
         if (!empty($purchase_no)) {
            $builder->where('i.purchase_no','=', $purchase_no );
        }
        if (!empty($customerid)) {
            $builder->where('i.customer_id', '=', $customerid);
        }

        if (!empty($fromDate)) {

            $builder->whereDate('i.duedate', '>=', $fromDate);
        }
        if (!empty($toDate)) {

            $builder->whereDate('i.duedate', '<=', $toDate);
        }
        if (!empty($fromdate)) {

            $builder->whereDate('i.date', '>=', $fromdate);
        }
        if (!empty($todate)) {

            $builder->whereDate('i.date', '<=', $todate);
        }
        if (!empty($status)) {
            $builder->where('i.status', '=', $status);
        }
        if (!empty($discountpercentage)) {
            $builder->where('i.discount_percentage', '=', $discountpercentage);
        }

        if ($is_active != '') {
            $builder->where('i.is_active', '=', $is_active);
        }

        if ($mode == 0 || $mode == '') {
            $builder->orderBy('i.id', 'desc');
        } else {
            $builder->orderBy('i.date', 'asc');
        }

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $purchaseinvoicereturnCollection = $builder->get();
        } else {

            $purchaseinvoicereturnCollection = $builder->skip($start)->take($limit)->get();
        }

        $resVal['list'] = $purchaseinvoicereturnCollection;

        LogHelper::info1('Purchase Return List All ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }


    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Purchase Invoice Return Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

         $currentuser = Auth::user();
        try {
            $purchaseinvoicereturn = PurchaseinvoiceReturn::findOrFail($id);
            if ($purchaseinvoicereturn->is_active == 0) {
                return $resVal;
            }
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Purchase Invoice Not Found';
            return $resVal;
        }
        
                
        $paymentCollection = AdvancePayment::where('voucher_no', '=', $id)
                ->where('voucher_type','=','advance_purchase')
                ->where('used_amount','!=',0)
                ->where('is_active', '=', 1)
                ->get();
        
        if ($paymentCollection->count() > 0) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Unable To Delete, Return Amount Used';
            return $resVal;
        }


        $invoiceReturnItemCollection = DB::table('tbl_purchase_invoice_item_return')->where('purchase_invoice_return_id', $id)->where('is_active','=',1)->get();

        $purchaseinvoicereturnitem = DB::table('tbl_purchase_invoice_item_return')->where('purchase_invoice_return_id', $id)->update(['is_active' => 0]);

        $purchaseinvoicereturn->is_active = 0;
        $purchaseinvoicereturn->updated_by = $currentuser->id;
        $purchaseinvoicereturn->save();

        $advanceDetail= DB::table('tbl_adv_payment')
                ->where('voucher_no', $id)
//                ->where('payment_mode', '=','purchase_return')
                ->where('voucher_type' ,'=','advance_purchase')
                ->first();



        DB::table('tbl_adv_payment')->where('voucher_no', $id)
//                ->where('payment_mode', '=','purchase_return')
                ->where('voucher_type' ,'=','advance_purchase')
                ->update(['is_active' => 0]);

        $resVal['id'] = $purchaseinvoicereturn->id;
        $det = json_decode(json_encode($invoiceReturnItemCollection), true);
        GeneralHelper::StockAdjustmentSave($det, 'purchase_return_delete', $purchaseinvoicereturn->id,$purchaseinvoicereturn->date);

        GeneralHelper::PurchaseReturnInventory($det);

//        if($advanceDetail != ''){
//        AccountHelper::deposit($advanceDetail->account_id, $advanceDetail->amount);
//
//         DB::table('tbl_transaction')
//                 ->where('voucher_number', $advanceDetail->id)
//                ->where('voucher_type' ,'=','advance_purchase')
//                ->update(['is_active' => 0]);
//         
//         DB::table('tbl_transaction')
//                 ->where('voucher_number', $purchaseinvoicereturn->id)
//                ->where('voucher_type' ,'=','purchase_return')
//                ->update(['is_active' => 0]);
//         
//         AccountHelper::withDraw($advanceDetail->account_id, $advanceDetail->amount);
//
//
//        }
        //$purchaseinvoicereturn->delete();
       // LogHelper::info('Purchase Invoice Return Delete' . $request->fullurl());
        LogHelper::info1('Purchase Return Delete ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Purchase Return', 'delete', $screen_code, $purchaseinvoicereturn->id);
        return $resVal;
    }

    public function detail(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $id = $request->input('id', '');


        $isactive = $request->input('isactive', '');
        $discountpercentage = $request->input('discount_percentage', '');
        $decimalFormat= GeneralHelper::decimalFormat();

        //$builder = DB::table('tbl_invoice')->select('*');
        $builder = DB::table('tbl_purchase_invoice_return as i')
                ->leftJoin('tbl_customer as c', 'i.customer_id', '=', 'c.id')
                 ->select('i.*', 'c.fname as customer_fname', 'c.lname as customer_lname' , 'c.phone', 'c.email'
                        )
                ->where('i.id', '=', $id);


        $purchaseinvoicereturnitems = DB::table('tbl_purchase_invoice_item_return')
                ->select('*',DB::raw("cast(qty as DECIMAL(12,".$decimalFormat.")) as qty"))
                ->where('purchase_invoice_return_id', $id)
                ->where("is_active", '=', 1);
//        $customerattribute = DB::table('tbl_attribute_value')->where('refernce_id', $id);

        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        if (!empty($id)) {
            $builder->where('i.id', '=', $id);
        }
        if (!empty($discountpercentage)) {
            $builder->where('i.discount_percentage', '=', $discountpercentage);
        }

        if ($isactive != '') {
            $builder->where('i.is_active', '=', $isactive);
        }
        $builder->orderBy('i.id', 'desc');

        $invoiceCollection = $builder->skip($start)->take($limit)->get();
        if (count($invoiceCollection) == 1) {

            $purchaseinvoicereturn = $invoiceCollection->first();

            $purchaseinvoicereturn->item = $purchaseinvoicereturnitems->get();

             $customAttributeValue = DB::table('tbl_attribute_value as a')
                            ->leftJoin('tbl_attributes as abs', 'a.attributes_id', '=', 'abs.id')
                            ->select('a.*', 'abs.attribute_label as attribute_label')
                            ->where('a.refernce_id', '=', $id)
                            ->where('abs.attributetype_code', '=', 'purchasereturn')->get();

            $customattribute = DB::table('tbl_attributes as abs')
                            ->select('abs.*')
                            ->where('abs.attributetype_code', '=', 'purchasereturn')->get();
            if (count($customAttributeValue) > 0) {

                foreach ($customAttributeValue as $item) {

                    foreach ($customattribute as $customattributeItem) {

                        if ($item->attribute_code == $customattributeItem->attribute_code) {

                            $customattributeItem->value = $item->value;
                        }
                    }
                }
            }
            $purchaseinvoicereturn->customattribute = $customattribute;

            //$invoice->customattribute = $customerattribute->get();


            $resVal['data'] = $purchaseinvoicereturn;
        }
        else
        {
            $resVal['data'] = null;
        }
//        LogHelper::info('Purchase Invoice Detail Filters' . $request->fullurl());
//        LogHelper::info('Purchase Invoice Detail List' . $invoiceCollection);
        LogHelper::info1('Purchase Return Detail ' . $request->fullurl(), $request->all());
        return ($resVal);
    }

    public function invoiceDetail(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $customer_id = $request->input('customer_id');
        $reference_no = $request->input('reference_no');
        $purchase_invoice_id = $request->input('purchase_invoice_id');

        $builder = DB::table('tbl_purchase_invoice')
                        ->where('customer_id', '=', $customer_id)
                        ->where('reference_no', '=', $reference_no)->first();

//        LogHelper::info('Purchase Invoice Refenerece no Filters' . $request->fullurl());


        if ((count($builder) > 0) && ($reference_no != '') && ($purchase_invoice_id == 0)) {
            $resVal['message'] = 'The Reference No  Already Exists';
            $resVal['success'] = FALSE;
            return $resVal;
        } else {
            $resVal['message'] = 'The Reference No  Does Not  Exists';
            $resVal['success'] = TRUE;
            return $resVal;
        }
        LogHelper::info1('Purchase InvoiceDetail ' . $request->fullurl(), $request->all());
    }

    private function generatePaymentForPurchaseInvoiceReturn($purchaseReturn) {

        $pos_account = DB::table('tbl_app_config')->where('setting', 'pos_bank_account_id')->first();
        $pos_account_name = '';
        if (!empty($pos_account)) {
            $pos_account_name = DB::table('tbl_accounts')->where('id', $pos_account->value)->first();
        }
        if (empty($pos_account_name) || empty($pos_account)) {
            return;
        }
        $mode=GeneralHelper::get_mode();
        $currentuser = Auth::user();
        $advance_payment = new AdvancePayment;
        $advance_payment->created_by = $currentuser->id;
        $advance_payment->updated_by = $currentuser->id;

        $advance_payment->is_active = 1;
        $advance_payment->customer_id = $purchaseReturn->customer_id;
        $advance_payment->comments = 'Payment Receipt created for purchase invoice return';
        $advance_payment->date = $purchaseReturn->date;
        $advance_payment->amount = $purchaseReturn->total_amount;
        $advance_payment->used_amount = 0;
        $advance_payment->balance_amount = $purchaseReturn->total_amount;
        $advance_payment->payment_mode = $mode;
        $advance_payment->reference_no = '';
        $advance_payment->status = 'available';
        $advance_payment->tra_category = '';
        $advance_payment->voucher_type = 'advance_purchase';
        $advance_payment->voucher_no = $purchaseReturn->id;
//        $advance_payment->account = $pos_account_name->account;
        $advance_payment->account_id = 0;
        $advance_payment->save();
        
        $currency= ReplaceHolderHelper::getCurrencySymbol();
        $cusName= GeneralHelper::getCustName($purchaseReturn->customer_id);
        $incParticular=$currency.'. '.$purchaseReturn->total_amount." Received as an Advance Amount from ".$cusName." For the Purchase Return #".$purchaseReturn->id;
        $exParticular="Creating ".$currency.'. '.$purchaseReturn->total_amount." as an Advance Amount For the Vendor ".$cusName;
        
//        $mydate = $advance_payment->date;
//        $transactionData1 = array(
//            "name" => $currentuser->f_name,
//            "transaction_date" => $mydate,
//            "voucher_type" => "purchase_return",
//            "voucher_number" => $purchaseReturn->id, //Purchase Return Id
//            "credit" =>$advance_payment->amount,
//            "debit" => 0.00,
//            "pmtcoderef" => $advance_payment->account_id,
//            "is_cash_flow" => 1, //have to check  account in save and voucher type too
//            "account" => $advance_payment->account,
//            "account_category" => $advance_payment->tra_category,
//            "payment_mode" => $advance_payment->payment_mode,
//            "acoderef" => $advance_payment->customer_id,
//            "customer_id" => $advance_payment->customer_id,
//            "account" => $advance_payment->account,
//            "account_id" => $advance_payment->account_id,
//            "particulars" => $incParticular,
//            "created_by" => $currentuser->id,
//            "updated_by" => $currentuser->id,
//            "created_by_name" => $currentuser->f_name . ' ' . $currentuser->l_name,
//            "updated_by_name" => $currentuser->f_name . ' ' . $currentuser->l_name,
//            "is_active" => 1,
//        );
//        TransactionHelper::transactionSave($transactionData1);
//        
//        AccountHelper::deposit($advance_payment->account_id, $advance_payment->amount); //Its For Purchase Return Credit
//
//        
//         $transactionData = array(
//            "name" => $currentuser->f_name,
//            "transaction_date" => $mydate,
//            "voucher_type" => $advance_payment->voucher_type,
//            "voucher_number" => $advance_payment->id,
//            "credit" => 0.00,
//            "debit" => $advance_payment->amount,
//            "pmtcode" => NULL,
//            "pmtcoderef" => $advance_payment->account_id,
//            "is_cash_flow" => 1, //have to check  account in save and voucher type too
//            "account" => $advance_payment->account,
//            "account_category" => $advance_payment->tra_category,
//            "payment_mode" => $advance_payment->payment_mode,
//            "acode" => GeneralHelper::getCustomerAcode($advance_payment->customer_id),
//            "acoderef" => $advance_payment->customer_id,
//            "customer_id" => $advance_payment->customer_id,
//            "account" => $advance_payment->account,
//            "account_id" => $advance_payment->account_id,
//            "particulars" => $exParticular,
//            "created_by" => $currentuser->id,
//            "updated_by" => $currentuser->id,
//            "created_by_name" => $currentuser->f_name . ' ' . $currentuser->l_name,
//            "updated_by_name" => $currentuser->f_name . ' ' . $currentuser->l_name,
//            "is_active" => 1,
//        );
//        TransactionHelper::transactionSave($transactionData);
//        
//        //Update Account Balance
//        AccountHelper::withDraw($advance_payment->account_id, $advance_payment->amount); // Its For Advance Amout Debit
        //Update advance amount balance amount
        SalesInvoiceHelper::updateAdvPaymentStatusAndBalance($advance_payment->id);
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['message'] = ' Purchase Invoice Return Updated Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        try {
            $purchaseinvoicereturn = PurchaseinvoiceReturn::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Purchase invoice Not Found';
            return $resVal;
        }
        
//        $pos_account = DB::table('tbl_app_config')->where('setting', 'pos_bank_account_id')->first();
//        $pos_account_name = '';
//        if (!empty($pos_account)) {
//            $pos_account_name = DB::table('tbl_accounts')->where('id', $pos_account->value)->first();
//        }
//        if (empty($pos_account_name) || empty($pos_account)) {
//          $resVal['message'] = 'Please Create POS Bank Account';
//          $resVal['success'] = FALSE;
//          return $resVal;
//        }

        $purchase_no = $request->input('purchase_no', '');
        $prefix = $request->input('prefix', '');
        $purchase_code = '';
        if (!empty($purchase_no)) {

            $get_inv_no = DB::table('tbl_purchase_invoice_return')
                    ->select('is_active')
                    ->where('prefix', '=', $prefix)
                    ->where('is_active', '=', 1)
                    ->where('purchase_no', '=', $purchase_no)
                    ->where('id', '!=', $id)
                    ->get();
            if (count($get_inv_no) > 0) {
                $resVal['message'] = 'Purchase Invoice No Already Exists';
                $resVal['success'] = FALSE;
                return $resVal;
            } else {
                $purchase_no = $purchase_no;
                $purchase_code = $prefix . $purchase_no;
            }
        } else {
            $get_inv_no = DB::table('tbl_purchase_invoice_return')
                    ->select('purchase_no')
                    ->where('is_active', '=', 1)
                    ->where('prefix', '=', $prefix)
                    ->where('id', '!=', $id)
                    ->orderBy('purchase_no', 'desc')
                    ->first();
            if (isset($get_inv_no)) {
                $inv_no = $get_inv_no->purchase_no;
                $inv_no = $inv_no + 1;
            } else {
                $inv_no = 1;
            }
            $purchase_no = $inv_no;
            $purchase_code = $prefix . $inv_no;
        }
        
        $paymentCollection = AdvancePayment::where('voucher_no', '=', $id)
                ->where('voucher_type','=','advance_purchase')
                ->where('used_amount','!=',0)
                ->where('is_active', '=', 1)->get();
        
        if ($paymentCollection->count() > 0) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Unable To Edit, Return Amount Used';
            return $resVal;
        }

        $purchaseinvoicereturn->fill($request->all());
        $purchaseinvoicereturn->save();

        $subtotal = 0;
        $discount_amount = 0;
        $discount_amount1 = 0;
        $roundoff = 0;
        $tax_id = 0;
        $tax_amount = 0;
        $taxable_amount = 0;
        $total_before_roundoff = 0;
        $total = 0;

        $invoiceItemCol = $request->input('item');

        $currentuser = Auth::user();


        $purchaseinvoicereturn->updated_by = $currentuser->id;
        $purchaseinvoicereturn->is_active = $request->input('is_active', 1);
        $purchaseinvoicereturn->subtotal = $subtotal;
        $purchaseinvoicereturn->tax_amount = $tax_amount;
        $purchaseinvoicereturn->discount_amount = $discount_amount;
        $purchaseinvoicereturn->round_off = $roundoff;
        $purchaseinvoicereturn->total_amount = $total;

        $purchaseinvoicereturn->purchase_no = $purchase_no;
        $purchaseinvoicereturn->purchase_code = $purchase_code;
        $purchaseinvoicereturn->save();


        $invoiceReturnItemCollection = DB::table('tbl_purchase_invoice_item_return')->where('purchase_invoice_return_id', $id)->where('is_active', '=', 1)->get();

        $det = json_decode(json_encode($invoiceReturnItemCollection), true);

        GeneralHelper::InventoryCredit($det);

        GeneralHelper::InsertInventoryAdjustment($det, 'purchase_return_delete', $purchaseinvoicereturn->id,$purchaseinvoicereturn->date);

        GeneralHelper::PurchaseReturnInventory($invoiceReturnItemCollection);


        $itemDelete = DB::table('tbl_purchase_invoice_item_return')->where('purchase_invoice_return_id', $id)->delete();

        $advanceDetail= DB::table('tbl_adv_payment')
                ->where('voucher_no', $id)
//                ->where('payment_mode', '=','purchase_return')
                ->where('voucher_type' ,'=','advance_purchase')
                ->first();
        
        
//        if($advanceDetail != ''){
//        AccountHelper::deposit($advanceDetail->account_id, $advanceDetail->amount);
//
//         DB::table('tbl_transaction')
//                 ->where('voucher_number', $advanceDetail->id)
//                ->where('voucher_type' ,'=','advance_purchase')
//                ->update(['is_active' => 0]);
//         
//         DB::table('tbl_transaction')
//                 ->where('voucher_number', $purchaseinvoicereturn->id)
//                ->where('voucher_type' ,'=','purchase_return')
//                ->update(['is_active' => 0]);
//         
//         AccountHelper::withDraw($advanceDetail->account_id, $advanceDetail->amount);
//
//
//        }
        
         DB::table('tbl_adv_payment')
                        ->where('voucher_no', $id)
                       // ->where('payment_mode', '=', 'purchase_return')
                        ->where('voucher_type', '=', 'advance_purchase')->delete();


        foreach ($invoiceItemCol as $item) {

            $purchaseinvoicereturnitems = new PurchaseinvoiceitemReturn;
            $purchaseinvoicereturnitems->fill($item);

            $purchaseinvoicereturnitems->created_by = $currentuser->id;
            $purchaseinvoicereturnitems->updated_by = $currentuser->id;

            $purchaseinvoicereturnitems->purchase_invoice_return_id = $purchaseinvoicereturn->id;
            $purchaseinvoicereturnitems->is_active = 1;
            $purchaseinvoicereturnitems->save();


            $purchaseinvoicereturnitems->customer_id = $purchaseinvoicereturn->customer_id;
            $purchaseinvoicereturnitems->purchase_value = $purchaseinvoicereturnitems->qty * $purchaseinvoicereturnitems->purchase_price;
            $sub_total = $purchaseinvoicereturnitems->qty * $purchaseinvoicereturnitems->purchase_price;
            ;
            $subtotal+= $sub_total;

            //Discount calcualtion start
            if (empty($purchaseinvoicereturnitems->discount_mode) || empty($purchaseinvoicereturnitems->discount_value)) {
                $purchaseinvoicereturnitems->discount_amount = 0;
                $purchaseinvoicereturnitems->discount_mode = "";
                $purchaseinvoicereturnitems->discount_value = 0;
                $discount_amount = 0;
            } else {

                if ($purchaseinvoicereturnitems->discount_mode == 'amount') {
                    $purchaseinvoicereturnitems->discount_amount = $purchaseinvoicereturnitems->discount_value;
                    $discount_amount = $purchaseinvoicereturnitems->discount_value;
                } else {

                    $discount_amount = $purchaseinvoicereturnitems->purchase_value * ($purchaseinvoicereturnitems->discount_value / 100);
                    $discount_amount = floatval(number_format($discount_amount, 2, '.', ''));
                    $purchaseinvoicereturnitems->discount_amount = $discount_amount;
                }
            }
            $discount_amount1+=$discount_amount;

            $purchaseinvoicereturnitems->duedate = $purchaseinvoicereturn->duedate;
            $purchaseinvoicereturnitems->paymentmethod = $purchaseinvoicereturn->paymentmethod;
            $purchaseinvoicereturnitems->qty_in_hand = $purchaseinvoicereturnitems->qty;
            $purchaseinvoicereturnitems->save();

            $tax_amount+= $purchaseinvoicereturnitems->tax_amount;

            $purchaseinvoicereturnitems->total_price = ($purchaseinvoicereturnitems->purchase_value - $discount_amount) + $purchaseinvoicereturnitems->tax_amount;
            $purchaseinvoicereturnitems->save();
        }

        $packing_charge = $purchaseinvoicereturn->packing_charge;
        $insurance_charge = $purchaseinvoicereturn->insurance_charge;

        //finding roundoff value
        $total_before_roundoff = $subtotal - $discount_amount1 + $tax_amount + $packing_charge + $insurance_charge;
        $total_after_roundoff = round($total_before_roundoff);
        $roundoff = $total_after_roundoff - $total_before_roundoff;
        $roundoff = floatval(number_format($roundoff, 2, '.', ''));

        //finding grant total value
        $total = $subtotal - $discount_amount1 + $tax_amount + $roundoff + $packing_charge + $insurance_charge;
        ;
        $total = floatval(number_format($total, 2, '.', ''));



        $purchaseinvoicereturn->subtotal = $subtotal;
        $purchaseinvoicereturn->tax_amount = $tax_amount;
        $purchaseinvoicereturn->discount_amount = $discount_amount1;
        $purchaseinvoicereturn->round_off = $roundoff;

        $purchaseinvoicereturn->total_amount = $total;
        $purchaseinvoicereturn->save();

        $customerItemCol = ($request->input('customattribute'));

        if ($request->input('customattribute') != null) {

            $attribute = array();
            foreach ($customerItemCol as $item) {
                $attribute[$item['attribute_code']] = $item['value'];
            }
            $json_att = json_encode($attribute);
            $purchaseinvoicereturn->custom_attribute_json = $json_att;
            $purchaseinvoicereturn->save();
            $attributeValue=AttributeValue::where("refernce_id",'=',$purchaseinvoicereturn->id)->where("attribute_code",'=',"purchasereturn")->delete();
            foreach ($customerItemCol as $attribute) {
                //print_r($attribute);

                $customerattribute = new Attributevalue;

                $customerattribute->refernce_id = $purchaseinvoicereturn->id;
                $customerattribute->value = $attribute['value'];
                $customerattribute->attribute_code = $attribute['attribute_code'];

                $customerattribute->created_by = $currentuser->id;
                $customerattribute->updated_by = $currentuser->id;
                if ($attribute['attribute_code'] != NULL) {


                    $attributeCollection = Attribute::where('attribute_code', "=", $attribute['attribute_code'])->get();
                    if (count($attributeCollection) > 0) {
                        $attribute = $attributeCollection->first();
                        //print_r($attribute);
                        $customerattribute->attributes_id = $attribute->id;
                        $customerattribute->attributetype_id = $attribute->attributetype_id;
                    }
                }

                //$customerattribute->fill($attribute);

                $customerattribute->save();
            }
        }
        $resVal['id'] = $purchaseinvoicereturn->id;
        

        GeneralHelper::InventoryDebit($request->input('item'));

        GeneralHelper::InsertInventoryAdjustment($request->input('item'), 'purchase_return_update', $purchaseinvoicereturn->id,$purchaseinvoicereturn->date);

        GeneralHelper::updatePurchaseReturnInventory($request->input('item'));

        $this->generatePaymentForPurchaseInvoiceReturn($purchaseinvoicereturn);




        // GeneralHelper::InvoiceTaxSave($purchaseinvoicereturn->id, $request->input('tax_id'), $taxable_amount, $subtotal, 'purchase');
        //LogHelper::info('Purchase Invoice Return Save' . $request->fullurl());
        LogHelper::info1('Purchase Return Update ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Purchase Return', 'update', $screen_code, $purchaseinvoicereturn->id);
        return $resVal;
    }


    public function purchaseReturnPrint(Request $request,$id){

        $replace = new ReplacePlaceHolderForPurchaseInvoiceReturn;
        $replace_msg = $replace->placeHolderFun($id,'for_print','','','');
        LogHelper::info1('Purhase invoice Return Print ' . $request->fullurl(), $request->all());
        return $replace_msg;
}

     public function pdfPreview(Request $request, $id) {


        PdfHelper::purchaseReturnpdfPreview($id);

        $resVal['message'] = 'Pdf Download Successfully';
        $resVal['success'] = TRUE;
        return $resVal;
    }

}

?>
