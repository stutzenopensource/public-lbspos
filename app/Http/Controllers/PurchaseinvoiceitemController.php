<?php
namespace App\Http\Controllers;
use DB;
use Carbon\Carbon;
use App\Purchaseinvoiceitem;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use App\Helper\AuthorizationHelper;
use App\Helper\LogHelper;
use App\Helper\NotificationHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PurchaseinvoiceitemController
 *
 * @author Deepa
 */
class PurchaseinvoiceitemController extends Controller {
    
    //put your code here
    public function save (Request $request){
       $resVal = array();
        $resVal['message'] = 'Purchase Invoice Item Added Successfully';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'save');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        
        
        $purchaseinvoiceitem = new Purchaseinvoiceitem;
       
        $purchaseinvoiceitem->created_by = $currentuser->id;
        $purchaseinvoiceitem->updated_by = $currentuser->id;
        $purchaseinvoiceitem->fill($request->all());
        $purchaseinvoiceitem->save();

        $resVal['id'] = $purchaseinvoiceitem->id;
        LogHelper::info1('Purchase Invoice Item Save ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Purchase Invoice Item', 'save', $screen_code, $purchaseinvoiceitem->id);

        return $resVal;
    
    }
    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Perchase Invoice Item Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'update');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        try {
            $purchaseinvoiceitem = Purchaseinvoiceitem::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Perchase invoice Item Not Found';
            return $resVal;
        }
        $purchaseinvoiceitem->created_by = $currentuser->id;
        $purchaseinvoiceitem->fill($request->all());
        $purchaseinvoiceitem->save();
        LogHelper::info1('Purchase Invoice Item Update ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Purchase Invoice Item', 'update', $screen_code, $purchaseinvoiceitem->id);
        return $resVal;
    }
    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'list');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
       
        $id = $request->input('id');
       
        $isactive= $request->input('is_active','');
       
        $builder = DB::table('tbl_purchase_invoice_item')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
         
      
        if ($isactive != '') {
            $builder->where('is_active', '=', $isactive);
        }
        
        $builder->orderBy('id','desc');
        $resVal['total'] = $builder->count();
        $resVal['list'] = $builder->skip($start)->take($limit)->get();
         LogHelper::info1('Purchase Invoice Item List All ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }
    
    public function delete(Request $request,$id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Purchase Invoice Item Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'delete');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        try {
            $purchaseinvoiceitem = Purchaseinvoiceitem::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Purchase Invoice Item  Not Found';
            return $resVal;
        }

        $purchaseinvoiceitem->delete();
        LogHelper::info1('Purchase Invoice Item Delete' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Purchase Invoice Item', 'delete', $screen_code, $purchaseinvoiceitem->id);
        return $resVal;
    }
    
      public function posProductDetail(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'list');     
         $current_date = Carbon::now();
        $current_date = $current_date->format('Y-m-d');
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        $id = $request->input('id');
        $isactive= $request->input('is_active','');
        $search= $request->input('search','');
        $category= $request->input('category','');
       
        $builder = DB::table('tbl_purchase_invoice_item as pi')
                ->leftJoin('tbl_product as p','p.id','=','pi.product_id')
                ->leftJoin('tbl_category as ca','p.category_id','=','ca.id')
                 ->leftJoin('tbl_inventory as i','pi.product_id','=','i.product_id')
                ->select('p.*','p.id as product_id','p.name as product_name','p.sku','i.quantity as inventory_qty','pi.qty_in_hand',
                        'pi.mrp_price','pi.selling_price','ca.name as category_name','pi.id as purchase_invoice_item_id');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('pi.id', '=', $id);
        }
        if (!empty($category)) {
            $builder->where('ca.name', 'like', '%' . $category . '%');
         }
        if ($isactive != '') {
            $builder->where('pi.is_active', '=', $isactive);
        }
        
        if (!empty($search)) {
            $builder->whereRaw(" (p.name like '%" . $search . "%' or p.sku like '%" . $search . "%' or pi.id like '%" . $search . "%') ");
        }           

        
        $builder->orderBy('pi.id','desc');
        $resVal['total'] = $builder->count();
         $products = $builder->skip($start)->take($limit)->get();
        foreach ($products as $pdt) {
            //print_r($pdt);
            $pdt_id = $pdt->product_id;
            $taxMapping = DB::table('tbl_tax_mapping as tm')
            ->leftJoin('tbl_tax as t', 't.id', '=', 'tm.tax_id')
            ->select('tm.id as id','t.id as tax_id','t.tax_name as tax_name','t.tax_percentage','t.tax_applicable_amt','t.start_date','t.end_date')
            ->where('product_id', $pdt_id)->where('t.is_active','=',1)->where('tm.is_active','=',1)
            ->where('t.start_date', '<=', $current_date)
            ->where('t.end_date', '>=', $current_date);
                $pdt->taxMapping = $taxMapping->get();
        }
        $resVal['list'] = $products;
        LogHelper::info1('PosProductDetail ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }
    
}

?>
