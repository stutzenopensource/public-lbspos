<?php
namespace App\Http\Controllers;
use DB;
Use App\Purchasequoteitem;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helper\AuthorizationHelper;
use App\Helper\LogHelper;
use App\Helper\NotificationHelper;


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PerchasequoteitemController
 *
 * @author Deepa
 */
class PurchasequoteitemController extends Controller {
    //put your code here
    
    public function save (Request $request){
         $resVal = array();
        $resVal['message'] = 'Purchase Quote Item Added Successfully';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'save');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $perchasequotesitem = new Purchasequoteitem;
       
        $perchasequotesitem->created_by = $currentuser->id;
        $perchasequotesitem->updated_by = $currentuser->id;
        $perchasequotesitem->fill($request->all());
        $perchasequotesitem->save();

        $resVal['id'] = $perchasequotesitem->id;

        LogHelper::info1('Purchase Quote Item Save ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Purchase Quote Item', 'save', $screen_code, $perchasequotesitem->id);
        return $resVal;

    }
     public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Purchase Quote Item Updated Successfully';
        $currentuser = Auth::user();
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'update');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        try {
            $purchasequoteitem = Purchasequoteitem::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Perchase Quote Item Not Found';
            return $resVal;
        }
        $purchasequoteitem->created_by = $currentuser->id;
        $purchasequoteitem->fill($request->all());
        $purchasequoteitem->save();
        LogHelper::info1('Purchase Quote Item Update ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Purchsae Quote Item','update', $screen_code, $perchasequotesitem->id);
        return $resVal;
    }
    
    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'list');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
       
        $id = $request->input('id');
       
        $isactive= $request->input('is_active','');
       
        $builder = DB::table('tbl_purchase_quote_item')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
         
      
        if ($isactive != '') {
            $builder->where('is_active', '=', $isactive);
        }
        
        $builder->orderBy('id','desc');
        $resVal['total'] = $builder->count();
        $resVal['list'] = $builder->skip($start)->take($limit)->get();
        LogHelper::info1('Purchase Quote Item List All ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }
    
     public function delete(Request $request,$id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Purchase Quote Item Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'delete');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        try {
            $purchasequoteitem = Purchasequoteitem::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Purchase Quote Item  Not Found';
            return $resVal;
        }

        $purchasequoteitem->delete();
        LogHelper::info1('Purchase Quote Item Delete ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, ' Purchase Quote Item', 'delete', $screen_code, $perchasequotesitem->id);

        return $resVal;
    }
}

?>
