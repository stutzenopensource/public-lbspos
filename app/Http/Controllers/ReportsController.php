<?php

//

namespace App\Http\Controllers;

use DateTime;
use DateInterval;
use DatePeriod;
use DB;
use App\ActivityLog;
use App\PurchasePayment;
use Carbon\Carbon;
use App\Attributevalue;
use App\Attribute;
use app\Customer;
use Illuminate\Http\Request;
use App\Transformer\InvoiceTransformer;
use Illuminate\Support\Facades\Auth;
use App\Helper\GeneralHelper;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\LogHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of invoicesController
 *
 * @author Deepa
 */
class ReportsController {

    //put your code here
    public function ItemWiseReport(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        //Get Current and Previous Date
        $current_date = Carbon::now();

        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $show_all = $request->input('show_all', '');
        
        $cat_id=$request->input('cat_id','');
//        $builder = DB::table('tbl_product as p')
//                ->leftJoin('tbl_category as c', 'p.category_id', '=', 'c.id')
//                ->leftJoin('tbl_invoice_item as a', 'a.product_id', '=', 'p.id')
//                ->leftJoin('tbl_invoice as at', 'a.invoice_id', '=', 'at.id')
//                ->select(DB::raw("coalesce(c.id, 0) as cat_id, coalesce(c.name, 'UnCategorized') as  name,Coalesce(SUM(a.qty),0) as qty,Coalesce(cast(Round(sum(a.discount_amount)) as decimal(12,2)),0) as discount_amount,cast(round(SUM(a.total_price-a.discount_amount)) as decimal(12,2)) as total_price"))
//                ->whereDate('at.date', '>=', $from_date)
//                ->whereDate('at.date', '<=', $to_date)
//                ->where('a.is_active', '=', 1)
//                ->groupBy(DB::raw("c.id"))
//                ->get();
        
        $builder=DB::table('tbl_invoice_item as a')
                ->leftjoin('tbl_product as p','a.product_id','=','p.id')
                ->leftJoin('tbl_category as c', 'p.category_id', '=', 'c.id')
                ->leftJoin('tbl_invoice as at', 'a.invoice_id', '=', 'at.id')
                ->select(DB::raw("coalesce(c.id, 0) as cat_id, coalesce(c.name, 'UnCategorized') as  name,Coalesce(SUM(a.qty),0) as qty,Coalesce(cast(Round(sum(a.discount_amount)) as decimal(12,2)),0) as discount_amount,cast(round(SUM(a.total_price)) as decimal(12,2)) as total_price"))
                ->whereDate('at.date', '>=', $from_date)
                ->whereDate('at.date', '<=', $to_date)
                ->where('a.is_active', '=', 1)
                ->where('at.is_active','=',1)
                ->groupBy(DB::raw("c.id"));
              
        if (!empty($cat_id)) {
            $builder->where('p.category_id','=',$cat_id);
        }
        
       $builder= $builder->get();
        $prouct_det = array();
        if ($show_all == 1) {
            foreach ($builder as $cat) {
                $id = $cat->cat_id;
                $builder_detail = DB::table('tbl_invoice_item as a')
                        ->leftJoin('tbl_product as p', 'a.product_id', '=', 'p.id')
                        ->leftJoin('tbl_category as c', 'p.category_id', '=', 'c.id')
                        ->leftJoin('tbl_invoice as at', 'a.invoice_id', '=', 'at.id')
                        ->select(DB::raw("coalesce(p.id, 0) as product_id, a.product_name as  name,Coalesce(a.unit_price-(a.discount_amount/a.qty),0) as selling_price,Coalesce(cast(round(sum(a.discount_amount)) as decimal(12,2)),0) as discount_amount,a.mrp_price,Coalesce(SUM(a.qty),0) as qty, Coalesce(cast(round(SUM(a.total_price)) as decimal(12,2)),0) as total_price"))
                        ->whereDate('at.date', '>=', $from_date)
                        ->whereDate('at.date', '<=', $to_date)
                        ->where('a.is_active', '=', 1)
                        ->where('at.is_active','=',1)
                        ->where('p.category_id', '=', $id)
                        ->groupBy(DB::raw("p.id"));
                if($id==0)
                    $builder_detail->orwhere('a.product_id', '=', 0)
                         ->whereDate('at.date', '>=', $from_date)
                         ->whereDate('at.date', '<=', $to_date)
                         ->where('at.is_active','=',1)
                         ->where('a.is_active', '=', 1);
                 
                $cat->product = $builder_detail->get();
            }
          
            foreach ($builder as $cat) {
                $product = $cat->product;
                $id = $cat->cat_id;

                    foreach ($product as $pro) {
                        $product_id = $pro->product_id;
                        $builder_detail = DB::table('tbl_invoice_item as a')
                                ->leftJoin('tbl_invoice as at', 'a.invoice_id', '=', 'at.id')
                                ->leftJoin('tbl_customer as c', 'at.customer_id', '=', 'c.id')
                                ->select(DB::raw("at.id as invoice_id,at.invoice_code,at.date,c.fname,c.billing_city,c.shopping_city, a.qty,Coalesce(cast(round(a.discount_amount) as decimal(12,2)),0) as discount_amount ,Coalesce(a.total_price,0) as sales_value"))
                                ->whereDate('at.date', '>=', $from_date)
                                ->whereDate('at.date', '<=', $to_date)
                                ->where('a.is_active', '=', 1)
                                 ->where('at.is_active','=',1)
                                ->where('a.product_id', '=', $product_id);

                        $pro->data = $builder_detail->get();
                    }

                }
            }
           
//        foreach ($builder as $cat) {
//            $id = $cat->cat_id;
//            $product_array = array();
//            if ($id == 0) {
//                $qty = $cat->qty;
//                $total_price = $cat->total_price;
//                $builder1 = DB::table('tbl_invoice_item as a')
//                        ->leftJoin('tbl_invoice as at', 'a.invoice_id', '=', 'at.id')
//                        ->whereDate('at.date', '>=', $from_date)
//                        ->whereDate('at.date', '<=', $to_date)
//                        ->where('a.is_active', '=', 1)
//                        ->where('a.product_id', '=', 0)
//                        ->sum('a.qty','a.total_price-a.discount_amount');
//                $cat->qty = number_format((float) $builder1 + $qty, 3, '.', '');
//                $cat->total_price = number_format((float) $builder1 + $total_price, 2, '.', '');
//
//
//                //$cat->product = $builder5;
//            }
//        }
        if ($show_all == '') {



          $builder = DB::table('tbl_invoice_item as a')
          ->leftJoin('tbl_invoice as at', 'a.invoice_id', '=', 'at.id')
          ->leftjoin('tbl_product as p','p.id','=','a.product_id')
          ->leftjoin('tbl_category as ca','ca.id','=','p.category_id')     
          ->select(DB::raw("a.product_name,ca.name as category_name, a.product_id,Coalesce(SUM(a.qty),0) as qty,Coalesce(cast(round(sum(a.discount_amount)) as decimal(12,2)),0)  as discount_amount, Coalesce(cast(round(SUM(a.total_price)) as decimal(12,2)),0) as total_price, a.uom_name,Coalesce(sum(a.unit_price-((a.discount_amount))),0) as selling_price,Coalesce(sum(a.mrp_price),0) as mrp_price"))
          ->whereDate('at.date', '>=', $from_date)
          ->whereDate('at.date', '<=', $to_date)
          ->where('a.is_active', '=', 1)
          ->where('at.is_active', '=', 1)
          ->groupBy(DB::raw("a.product_name"));
          
            if (!empty($cat_id)) {
                $builder->where('p.category_id', '=', $cat_id);
            }
            $builder = $builder->get();


            $resVal['success'] = TRUE;

          foreach ($builder as $tax) {
          $id = $tax->product_name;
          $builder_detail = DB::table('tbl_invoice_item as a')
          ->leftJoin('tbl_invoice as at', 'a.invoice_id', '=', 'at.id')
          ->leftJoin('tbl_customer as c', 'at.customer_id', '=', 'c.id')
          ->select(DB::raw("at.id as invoice_id,at.invoice_code,at.date,c.fname,c.billing_city,c.shopping_city, a.qty ,a.mrp_price,Coalesce(cast(round(a.discount_amount) as decimal(12,2)),0) as discount_amount  ,Coalesce(a.unit_price-((a.discount_amount)/a.qty),0) as selling_price,Coalesce(cast(round(a.total_price) as decimal(12,2)),0) as total_price"))
          ->whereDate('at.date', '>=', $from_date)
          ->whereDate('at.date', '<=', $to_date)
          ->where('a.is_active', '=', 1)
          ->where('at.is_active', '=', 1)
          ->where('a.product_name', '=', $id);
 
          $tax->data = $builder_detail->get();
          }
        }

        $resVal['list'] = $builder;
        LogHelper::info1('ItemWise Report' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }

    public function ItemReport(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        //Get Current and Previous Date
        $current_date = Carbon::now();
        $resVal['success'] = TRUE;
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $product_id = $request->input('product_id');
        $builder = DB::table('tbl_invoice_item as a')
                ->leftJoin('tbl_invoice as at', 'a.invoice_id', '=', 'at.id')
                ->leftJoin('tbl_customer as c', 'at.customer_id', '=', 'c.id')
                ->select(DB::raw("at.invoice_code,at.date,c.fname,c.billing_city,c.shopping_city, a.qty ,  a.sales_value "))
                ->whereDate('at.date', '>=', $from_date)
                ->whereDate('at.date', '<=', $to_date)
                ->where('a.is_active', '=', 1)
                ->where('at.is_active','=',1)
                ->where('a.product_id', '=', $product_id)
                ->get();
        $resVal['list'] = $builder;
        LogHelper::info1('ItemReport' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }

    public function itemizedreport_csvexport(Request $request) {
        $itemizedLists = $this->ItemWiseReport($request);
           $show_all=$request->input('show_all');
        ini_set('max_execution_time', 600); //increase max_execution_time to 10 min if data set is very large
        //create a file
        $filename = "itemizedreport" . "_" . time() . ".csv";
        //  $filename = "customer" . "_" . date("Y-m-d_H-i", time()) . ".csv";
        $serverName = $_SERVER['SERVER_NAME'];

        $filePath = "download/$serverName/csv";

        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);

            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {

                if ($folderName == '')
                    $folderName = $folderList[$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index];

                if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        $filePath = getcwd() . '/' . $filePath;
        $output = fopen($filePath . "/" . $filename, 'w');

        if($show_all==1){
            fputcsv($output,array('Category Name','Qty','Amount'));
            foreach($itemizedLists['list'] as $values){
              fputcsv($output, array($values->name,$values->qty, $values->total_price));
            }
        }else{
        fputcsv($output, array('Product Name','Category Name', 'UOM', 'Qty', 'Amount'));
        foreach ($itemizedLists['list'] as $values) {
            fputcsv($output, array($values->product_name,$values->category_name,$values->uom_name, $values->qty, $values->total_price));

        }
        }
        LogHelper::info1('Itemized Report CSV Download' . $request->fullurl(), $request->all());
        return response()->download($filePath . "/" . $filename, 'itemizedreport.csv');
    }

    public function PartyWiseSalesReport(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        //Get Current and Previous Date
        $id = $request->input('id');
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');


        $amount = DB::table('tbl_invoice')
                ->where('date', "<", $from_date)
                ->where('customer_id', '=', $id)
                ->where('is_active', '=', 1)
                ->sum('total_amount');

        $returnAmount = DB::table('tbl_invoice_return')
                ->where('date', "<", $from_date)
                ->where('customer_id', '=', $id)
                ->where('is_active', '=', 1)
                ->sum('total_amount');


        $paid_amount = DB::table('tbl_payment')
                ->where('date', "<", $from_date)
                ->where('customer_id', '=', $id)
                ->where('is_active', '=', 1)
                ->sum('amount');


        $bal_amount = $amount - ($paid_amount+$returnAmount);

        $invoice_list = array();
        $invoice_list['date'] = date('d-m-Y', strtotime($from_date . '-1 day'));
        $invoice_list['description  '] = 'Opening Balance';
        $invoice_list['credit'] = number_format((float) $amount, 2, '.', '');
        $invoice_list['debit'] = number_format((float) $paid_amount+$returnAmount, 2, '.', '');
        $resVal['openBalance'] = $invoice_list;

        $builder1 = DB::table('tbl_invoice')
                ->select(DB::raw('id'), DB::raw('"0" as mode'), DB::raw('"Invoice" as type'), DB::raw('date'), DB::raw('CONCAT( "Invoice #",id) AS description'), DB::raw('total_amount as credit'), DB::raw('total_amount*0 as debit'),'id as invoice_id','created_at')
                ->where('customer_id', '=', $id)
                ->where('date', '>=', $from_date)
                ->where('date', '<=', $to_date)
                ->where('is_active', '=', 1);

        $builder2 = DB::table('tbl_invoice_return')
                ->select(DB::raw('id'), DB::raw('"1" as mode'), DB::raw('"Invoice Return" as type'), DB::raw('date'), DB::raw('CONCAT( "Invoice Return #",id) AS description'), DB::raw('total_amount*0 as credit'), DB::raw('total_amount as debit'),'id as invoice_id','created_at')
                ->where('customer_id', '=', $id)
                ->whereDate('date', '>=', $from_date)
                ->whereDate('date', '<=', $to_date)
                ->where('is_active', '=', 1);
        
//        $builder3 = DB::table('tbl_payment as p')
//                ->join('tbl_adv_payment as a','p.adv_payment_id','=','a.id')
//                ->select(DB::raw('p.id'), DB::raw('"3" as mode'), DB::raw('"Invoice Return Advance" as type'), DB::raw('p.date'), DB::raw('CONCAT("Amount From the Return #", " ", a.voucher_no , " For Invoice #"," ",p.invoice_id , " Advance Payment") AS description'), DB::raw('p.amount as credit'), DB::raw('p.amount*0 as debit'),'p.invoice_id','p.created_at')
//                ->where('p.customer_id', '=', $id)
//                ->whereDate('p.date', '>=', $from_date)
//                ->whereDate('p.date', '<=', $to_date)
//                ->whereRaw(DB::raw("(a.voucher_no is null or a.voucher_no != 0)"))
//                ->where('a.voucher_type','=','advance_invoice')
//                ->where('p.is_active', '=', 1);

         $builder = DB::table('tbl_payment as p')
                ->leftjoin('tbl_accounts as a','p.account_id','=','a.id')
                 ->leftjoin('tbl_adv_payment as ap',function($join){
                     $join->on('ap.id','=','p.adv_payment_id');
                     $join->where('ap.voucher_type','=',DB::raw("'advance_invoice'"));
                 })
                ->select(DB::raw('p.id'), DB::raw('"3" as mode'), DB::raw('"Payment" as type'), DB::raw('p.date'), DB::raw('CONCAT("Invoice Payment #", " ", p.invoice_id , " in " ,a.account) AS description'), DB::raw('p.amount*0 as credit'), DB::raw('p.amount as debit'),'p.invoice_id','p.created_at')
                ->where('p.customer_id', '=', $id)
                ->whereDate('p.date', '>=', $from_date)
                ->whereDate('p.date', '<=', $to_date)
                ->where('p.is_active', '=', 1)
                ->whereRaw("(ap.voucher_no=0 or ap.voucher_no is Null)")
                ->union($builder1)
                ->union($builder2)
//                ->union($builder3)
                ->orderBy('date', 'asc')
                ->orderBy('created_at', 'asc')
                ->get();

        $resVal['list'] = $builder;
        LogHelper::info1('PartyWiseSalesReport' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }

    public function PartyWiseSalesReport_csvexport(Request $request) {
        $PartyWiseLists = $this->PartyWiseSalesReport($request);
        ini_set('max_execution_time', 600); //increase max_execution_time to 10 min if data set is very large
        //create a file
        $filename = "PartyWiseSalesReport" . "_" . time() . ".csv";
        //  $filename = "customer" . "_" . date("Y-m-d_H-i", time()) . ".csv";
        $serverName = $_SERVER['SERVER_NAME'];

        $filePath = "download/$serverName/csv";

        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);

            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {

                if ($folderName == '')
                    $folderName = $folderList[$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index];

                if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        $filePath = getcwd() . '/' . $filePath;
        $bal = 0;
        $openblnc = $PartyWiseLists['openBalance'];
        $output = fopen($filePath . "/" . $filename, 'w');
        fputcsv($output, array('Transaction Date', 'Narration', 'Source Type', 'Debit', 'Credit', 'Balance'));
        $bal = $openblnc['credit'] - $openblnc['debit'];
        $bal=number_format((float) $bal, 2, '.', '');
        fputcsv($output, array('', $openblnc['description  '], '', $openblnc['debit'], $openblnc['credit'], $bal));
        $date_format=GeneralHelper::getDateFormat();
        foreach ($PartyWiseLists['list'] as $values) {
            $bal = $bal + $values->credit;
            $bal = $bal - $values->debit;
            $date=GeneralHelper::CsvDate($values->date, $date_format);
            $bal=number_format((float) $bal, 2, '.', '');
            fputcsv($output, array($date,$values->description, $values->type, $values->debit, $values->credit, $bal));
            // id: 1, customer_id: 37, prefix: "test123r", date: "2017-05-02", duedate: "2017-05-02"
        }
        LogHelper::info1('PartyWiseSalesReport CSV Download' . $request->fullurl(), $request->all());
        return response()->download($filePath . "/" . $filename, 'PartyWiseSalesReport.csv');
    }

    public function PartyWisePurchaseReport_csvexport(Request $request) {
        $PartyWisePurchaseLists = $this->PartyWisePurchaseReport($request);
        ini_set('max_execution_time', 600); //increase max_execution_time to 10 min if data set is very large
        //create a file
        $filename = "PartyWisePurchaseReport" . "_" . time() . ".csv";
        //  $filename = "customer" . "_" . date("Y-m-d_H-i", time()) . ".csv";
        $serverName = $_SERVER['SERVER_NAME'];

        $filePath = "download/$serverName/csv";

        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);

            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {

                if ($folderName == '')
                    $folderName = $folderList[$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index];

                if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        $filePath = getcwd() . '/' . $filePath;
        $bal = 0;
        $openblnc = $PartyWisePurchaseLists['openBalance'];
        $output = fopen($filePath . "/" . $filename, 'w');
        fputcsv($output, array('Transaction_date', 'ID', 'Narration', 'Source_Type', 'Depit', 'Credit', 'Balance'));
        $bal = $openblnc['credit'] - $openblnc['debit'];
        fputcsv($output, array('', $openblnc['description  '], '', $openblnc['debit'], $openblnc['credit'], $bal));
        foreach ($PartyWisePurchaseLists['list'] as $values) {
            $bal = $bal + $values->credit;
            $bal = $bal - $values->debit;
            fputcsv($output, array($values->date, $values->id, $values->description, $values->type, $values->debit, $values->credit, $bal));
            // id: 1, customer_id: 37, prefix: "test123r", date: "2017-05-02", duedate: "2017-05-02"
        }
        LogHelper::info1('PartyWisePurchaseReport CSV Download' . $request->fullurl(), $request->all());
        return response()->download($filePath . "/" . $filename, 'PartyWisePurchaseReport.csv');
    }

    public function PartyWisePurchaseReport(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        //Get Current and Previous Date
        $id = $request->input('id');
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');


        $amount = DB::table('tbl_purchase_invoice')
                ->where('date', "<", $from_date)
                ->where('customer_id', '=', $id)
                ->where('is_active', '=', 1)
                ->sum('total_amount');

        $returnAmount = DB::table('tbl_purchase_invoice_return')
                ->where('date', "<", $from_date)
                ->where('customer_id', '=', $id)
                ->where('is_active', '=', 1)
                ->sum('total_amount');

       // $amount=$amount-$returnAmount;

        $paid_amount = DB::table('tbl_purchase_payment')
                ->where('date', "<", $from_date)
                ->where('customer_id', '=', $id)
                ->where('is_active', '=', 1)
                ->sum('amount');


        $bal_amount = $amount - ($paid_amount+$returnAmount);

        $invoice_list = array();
        $invoice_list['date'] = date('d-m-Y', strtotime($from_date . '-1 day'));
        $invoice_list['description  '] = 'Opening Balance';
        $invoice_list['credit'] = $amount;
        $invoice_list['debit'] = $paid_amount+$returnAmount;
        $resVal['openBalance'] = $invoice_list;

        $builder1 = DB::table('tbl_purchase_invoice')
                ->select(DB::raw('id'), DB::raw('"0" as mode'), DB::raw('"Invoice" as type'), DB::raw('date'), DB::raw('CONCAT( "Invoice #",id) AS description'), DB::raw('total_amount*0 as debit'), DB::raw('total_amount as credit'),'id as purchase_invoice_id','created_at')
                ->where('customer_id', '=', $id)
                ->where('date', '>=', $from_date)
                ->where('date', '<=', $to_date)
                ->where('is_active', '=', 1);

        $builder2 = DB::table('tbl_purchase_invoice_return')
                ->select(DB::raw('id'), DB::raw('"1" as mode'), DB::raw('"Invoice Return" as type'), DB::raw('date'), DB::raw('CONCAT( "Invoice Return #",id) AS description'), DB::raw('total_amount as debit'), DB::raw('total_amount*0 as credit'),'id as purchase_invoice_id','created_at')
                ->where('customer_id', '=', $id)
                ->where('date', '>=', $from_date)
                ->where('date', '<=', $to_date)
                ->where('is_active', '=', 1);
        
//         $builder3 = DB::table('tbl_purchase_payment as p')
//                ->join('tbl_adv_payment as a','p.adv_payment_id','=','a.id')
//                ->select(DB::raw('p.id'), DB::raw('"3" as mode'), DB::raw('"Invoice Return Advance" as type'), DB::raw('p.date'), DB::raw('CONCAT("Amount From the Return #", " ", a.voucher_no , " For Invoice #"," ",p.purchase_invoice_id , " Advance Payment") AS description'), DB::raw('p.amount*0 as debit'), DB::raw('p.amount as credit'),'p.purchase_invoice_id','p.created_at')
//                ->where('p.customer_id', '=', $id)
//                ->whereDate('p.date', '>=', $from_date)
//                ->whereDate('p.date', '<=', $to_date)
//                ->whereRaw(DB::raw("(a.voucher_no is null or a.voucher_no != 0)"))
//                ->where('a.voucher_type','=','advance_Purchase')
//                ->where('p.is_active', '=', 1);


        $builder = DB::table('tbl_purchase_payment as p')
                 ->leftjoin('tbl_accounts as a','p.account_id','=','a.id')
                 ->leftjoin('tbl_adv_payment as ap',function($join){
                     $join->on('ap.id','=','p.adv_payment_id');
                     $join->where('ap.voucher_type','=',DB::raw("'advance_purchase'"));
                 })
                ->select(DB::raw('p.id'), DB::raw('"3" as mode'), DB::raw('"Payment" as type'), DB::raw('p.date'), DB::raw('CONCAT("Invoice Payment #", " ", p.purchase_invoice_id , " in " ,a.account)  AS description'), DB::raw('p.amount as debit'), DB::raw('p.amount*0 as credit'),'p.purchase_invoice_id','p.created_at')
                ->where('p.customer_id', '=', $id)
                ->where('p.date', '>=', $from_date)
                ->where('p.date', '<=', $to_date)
                ->where('p.is_active', '=', 1)
                ->whereRaw("(ap.voucher_no=0 or ap.voucher_no is Null)")
                ->union($builder1)
                ->union($builder2)
//                ->union($builder3)
                ->orderBy('date', 'asc')
                ->orderBy('created_at', 'asc')
                ->get();
        $resVal['list'] = $builder;
        LogHelper::info1('PartyWisePurchaseReport' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }

    public function discountReport(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        //Get Current and Previous Date
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $customer_id = $request->input('customer_id');

        $builder = DB::table('tbl_invoice as i ')
                ->leftJoin('tbl_customer as c', 'i.customer_id', '=', 'c.id')
                ->select('i.id as invoice_id','i.invoice_code', 'i.date', DB::raw("CONCAT(c.fname , ' ' , COALESCE(c.lname,'')) AS name"), 'c.billing_city', 'i.total_amount', 'i.discount_amount', 'i.subtotal')
                ->where('i.date', ">=", $from_date)
                ->where('i.discount_amount', ">", 0)
                ->where('i.date', "<=", $to_date)
                ->where('i.is_active', '=', 1);

         if (!empty($customer_id)) {
            $builder->where('i.customer_id', '=', $customer_id);
        }
        $collection= $builder->get();
        foreach($collection as $list)
        {
            $dicount_amount= $list->discount_amount;
            $subtotal= $list->subtotal;
            $discount_perc=$dicount_amount*100/$subtotal;
            $discount_perc = floatval(number_format($discount_perc, 2, '.', ''));
            $list->discount_percentage=$discount_perc;
        }
        $resVal['list'] = $collection;
        LogHelper::info1('Discount Report' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }
    public function discountreport_csvexport(Request $request) {
        $discountLists = $this->discountReport($request);
        ini_set('max_execution_time', 600); //increase max_execution_time to 10 min if data set is very large
        //create a file
        $filename = "discountreport" . "_" . time() . ".csv";
        //  $filename = "customer" . "_" . date("Y-m-d_H-i", time()) . ".csv";
        $serverName = $_SERVER['SERVER_NAME'];

        $filePath = "download/$serverName/csv";

        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);

            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {

                if ($folderName == '')
                    $folderName = $folderList[$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index];

                if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        $filePath = getcwd() . '/' . $filePath;
        $output = fopen($filePath . "/" . $filename, 'w');
        fputcsv($output, array('S.NO', 'Invoice No', 'Invoice Date', 'Customer Name','City','Net Total','Discount %','Discount Amount','Invoice Amount'));
        $count=0;
         $date_format=GeneralHelper::getDateFormat();
        foreach ($discountLists['list'] as $values) {
             $date=GeneralHelper::CsvDate($values->date,$date_format);
            $count=$count+1;
            fputcsv($output, array($count,$values->invoice_code, $date, $values->name,$values->billing_city,$values->subtotal,$values->discount_percentage,$values->discount_amount,$values->total_amount));
            // id: 1, customer_id: 37, prefix: "test123r", date: "2017-05-02", duedate: "2017-05-02"
        }
        LogHelper::info1('DiscountReport CSV Download' . $request->fullurl(), $request->all());
        return response()->download($filePath . "/" . $filename, 'discountreport.csv');
    }
    public function PartyWisePurchaseReportSummary(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        //Get Current and Previous Date
        $id = $request->input('id');
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $summary = $request->input('summary');


        $builder_for_total_purchase = DB::table('tbl_purchase_invoice')
                ->select(DB::raw("SUM(total_amount) as total_amount"))
                ->where('date', ">=", $from_date)
                ->where('date', "<=", $to_date)
                ->where('is_active', '=', 1)
                ->first();


        $builder = DB::table('tbl_purchase_invoice as i ')
                ->leftJoin('tbl_customer as c', 'i.customer_id', '=', 'c.id')
                ->leftJoin('tbl_purchase_invoice_item as p', 'p.purchase_invoice_id', '=', 'i.id')
                ->where('i.date', ">=", $from_date)
                ->where('i.date', "<=", $to_date)
                ->where('i.is_active', '=', 1);

        if ($summary == 1) {
            $builder->select('i.id as purchase_invoice_id','i.customer_id', DB::raw("CONCAT(c.fname , ' ' , coalesce(c.lname,'')) AS name"), 'c.billing_city', DB::raw("SUM(p.qty) as Purchase_Qty"));
            $builder->groupby('i.customer_id');
            $purchaseinvoiceCollection = $builder->get();
            foreach ($purchaseinvoiceCollection as $purchase) {
                $customer_id = $purchase->customer_id;
                $purchase_amount = DB::table('tbl_purchase_invoice')
                        ->select(DB::raw("SUM(total_amount) as total_amount"))
                        ->where('date', ">=", $from_date)
                        ->where('date', "<=", $to_date)
                        ->where('is_active', '=', 1)
                        ->where('customer_id', '=', $customer_id)
                        ->first();
                $purchase->total_amount = $purchase_amount->total_amount;
            }
        } else {
            $builder->select('i.id as purchase_invoice_id','i.purchase_code', DB::raw("CONCAT(c.fname , ' ' , coalesce(c.lname,'')) AS name"), 'c.billing_city', 'i.total_amount', DB::raw("SUM(p.qty) as Purchase_Qty"));
            $builder->groupby('i.id');
            $purchaseinvoiceCollection = $builder->get();
        }

        $resVal['list'] = $purchaseinvoiceCollection;
        LogHelper::info1('PartyWisePurchaseReportSummary' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        $resVal['total_purchase'] = $builder_for_total_purchase->total_amount;
        return ($resVal);
    }
    public function partywisepurchasesummary_csvexport(Request $request) {
        $partywisepurchasesummary = $this->PartyWisePurchaseReportSummary($request);
        $summary=$request->input('summary');
        ini_set('max_execution_time', 600); //increase max_execution_time to 10 min if data set is very large
        //create a file
        $filename = "partywisepurchasesummary" . "_" . time() . ".csv";
        //  $filename = "customer" . "_" . date("Y-m-d_H-i", time()) . ".csv";
        $serverName = $_SERVER['SERVER_NAME'];

        $filePath = "download/$serverName/csv";

        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);

            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {

                if ($folderName == '')
                    $folderName = $folderList[$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index];

                if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        $filePath = getcwd() . '/' . $filePath;
        $output = fopen($filePath . "/" . $filename, 'w');
        $arrHead=array('S.NO','Supplier Name','City','Purchase Quantity','Purchase Amount');
        if(empty($summary))
        array_splice($arrHead,3,0,'Purchase Code');
        fputcsv($output, $arrHead);
        $count=0;
        foreach ($partywisepurchasesummary['list'] as $values) {
            $count++;
            $arrVal=array($count,$values->name,$values->billing_city,$values->Purchase_Qty,$values->total_amount);
             if(empty($summary))
                 array_splice($arrVal,3,0,$values->purchase_code);
            fputcsv($output, $arrVal);
        }
        LogHelper::info1('PartyWisePurchaseSummary CSV Download' . $request->fullurl(), $request->all());
        return response()->download($filePath . "/" . $filename, 'partywisepurchasesummary.csv');
    }

    public function GSTSales(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        //Get Current and Previous Date
        $id = $request->input('id');
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $summary = $request->input('summary');
        $invoice_code=$request->input('invoice_code');
        $customer_type=$request->input('customer_type');

        $taxCollection = DB::table('tbl_tax as t ')
                ->leftJoin('tbl_invoice_item_tax as i', 'i.tax_id', '=', 't.id')
                ->select('t.id', 't.tax_name')
                ->where('t.is_active', '=', 1)
                ->where('t.is_group','!=',1)
                ->groupby('t.id')
                ->orderBy('id', 'asc')
                ->get();

        $gstCollection = DB::table('tbl_invoice as i ')
                ->leftJoin(DB::raw("(select Coalesce(sum(it.qty),0) as qty,it.invoice_id from tbl_invoice_item as it left join tbl_invoice as i on it.invoice_id =i.id and i.is_active=1 where date(i.date) >= '".$from_date."' and date(i.date) <= '".$to_date."' group by i.id) as its on its.invoice_id=i.id"),function($join){
                })
                ->leftJoin('tbl_customer as c', 'i.customer_id', '=', 'c.id')
                ->select(DB::raw("CONCAT(c.fname , ' ' ,coalesce(c.lname,'')) AS name"),'c.gst_no', 'i.id as invoice_id', 'c.billing_city','c.customer_type', 'c.id as customer_id', 'c.phone', 'invoice_code','invoice_no', 'i.date', 'i.subtotal', 'i.discount_amount', 'i.total_amount', DB::raw('Coalesce((i.total_amount-i.tax_amount-i.round_off),0) AS valauable_amount'), 'i.tax_amount','its.qty as overall_qty')
                ->where('i.date', ">=", $from_date)
                ->where('i.date', "<=", $to_date)
                ->where('i.is_active', '=', 1)
                ->orderBy('i.id', 'desc');
//                ->get();
        if(!empty($invoice_code)){
            $gstCollection->where('i.invoice_code','=',$invoice_code);
        }
        if(!empty($customer_type)){
            $gstCollection->where('c.customer_type','=',$customer_type);
        }
        $gstCollection=$gstCollection->get();

        foreach ($gstCollection as $gst) {
            $customer_id = $gst->customer_id;
            $invoice_id = $gst->invoice_id;
//            $get_gst = DB::table('tbl_attribute_value as t ')
//                    ->leftJoin('tbl_attributes as i', 't.attributes_id', '=', 'i.id')
//                    ->where('t.attributetype_id', "=", 2)
//                    ->where('t.refernce_id', "=", $customer_id)
//                    ->where('i.attribute_code', "=", 'gst_no')
//                    ->first();
//
//            if ($get_gst != NULL) {
//                $gst->gst_no = $get_gst->value;
//            } else {
//                $gst->gst_no = '';
//            }



            $get_tax = DB::table('tbl_tax as t')
                    ->leftJoin('tbl_invoice_item_tax as i ', function($join) use ($invoice_id) {
                        $join->on('t.id', '=', 'i.tax_id');
                        $join->on('i.invoice_id', '=', DB::raw("'" . $invoice_id . "'"));
                        $join->on('i.is_active', '=', DB::raw("'1'"));
                    })
                    ->leftJoin(DB::raw('(select t.id,coalesce(sum((i.tax_amount)),0) as tax_amount  from  tbl_tax as t
left join tbl_invoice_item_tax as i on i.tax_id=t.id and t.is_active=1 and t.is_group != 1  AND i.invoice_id=' . $invoice_id . '  group by i.tax_id)
as amt on amt.id=t.id'), function($join1) {

                    })
                    ->select(DB::raw('coalesce(t.id, 0) as id'), 't.tax_name', DB::raw(" coalesce(amt.tax_amount, 0) as tax_amount"))
                    ->where('t.is_active', '=', 1)
                    ->where('t.is_group', '!=', 1)
                    ->distinct('t.id')
                    ->orderBy('t.id', 'asc')
                    ->get();
            $gst->tax_detail = $get_tax;
        }


        $resVal['tax_list'] = $taxCollection;
        $resVal['list'] = $gstCollection;
        LogHelper::info1('GSTSales' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }
     public function gstsales_csvexport(Request $request) {
        $gstsales = $this->GSTSales($request);
        ini_set('max_execution_time', 600); //increase max_execution_time to 10 min if data set is very large
        //create a file
        $filename = "gstsalesreport" . "_" . time() . ".csv";
        //  $filename = "customer" . "_" . date("Y-m-d_H-i", time()) . ".csv";
        $serverName = $_SERVER['SERVER_NAME'];

        $filePath = "download/$serverName/csv";

        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);

            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {

                if ($folderName == '')
                    $folderName = $folderList[$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index];

                if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        $filePath = getcwd() . '/' . $filePath;
        $output = fopen($filePath . "/" . $filename, 'w');
        $sr=1;
        $head=array('S.No','Name','Customer Type','City','Mobile','Gst No','Invoice No','Qty','Invoice Amount','Discount Amount','Taxable Amount','Tax Amount');
        foreach($gstsales['tax_list'] as $value){
            $values=$value->tax_name;
            array_push($head,$values);
        }
        fputcsv($output,$head);
        foreach ($gstsales['list'] as $values) {
            $tax_detail = $values->tax_detail;
             $ph=explode(',',$values->phone);
             $phNo=implode(', ',$ph);
            $csv_row = array($sr,$values->name,$values->customer_type,$values->billing_city,$phNo,$values->gst_no, $values->invoice_code,$values->overall_qty,$values->total_amount,$values->discount_amount,$values->valauable_amount,$values->tax_amount);
            foreach ($tax_detail as $value) {
                $key = $value->tax_amount;
                if (isset($key)) {
                    array_push($csv_row, $key);
                } else
                    array_push($csv_row, '');
            }
            $sr++;
            fputcsv($output, $csv_row);

        }
        LogHelper::info1('GstSales CSV Download' . $request->fullurl(), $request->all());
        return response()->download($filePath . "/" . $filename, 'gstsalesreport.csv');
    }

    public function GSTPurchase(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        //Get Current and Previous Date
        $id = $request->input('id');
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $summary = $request->input('summary');
        $purchase_code=$request->input('purchase_code');


        $taxCollection = DB::table('tbl_tax as t ')
                ->leftJoin('tbl_purchase_invoice_tax as i', 'i.tax_id', '=', 't.id')
                ->select('t.id', 't.tax_name')
                ->where('t.is_active', '=', 1)
                ->where('t.is_group', '!=', 1)
                ->groupby('t.id')
                ->orderBy('id', 'asc')
                ->get();

        $gstCollection = DB::table('tbl_purchase_invoice as i ')
                ->leftJoin('tbl_customer as c', 'i.customer_id', '=', 'c.id')
                ->select(DB::raw("CONCAT(c.fname , ' ' , coalesce(c.lname,'')) AS name"),'c.gst_no', 'i.id as purchase_id', 'c.billing_city', 'c.id as customer_id', 'c.phone',DB::raw("CONCAT(i.prefix, '' ,reference_no) AS purchase_code"), 'i.date', 'i.subtotal', 'i.discount_amount', 'i.total_amount', DB::raw('Coalesce((i.subtotal - i.discount_amount),0) AS valauable_amount'), 'i.tax_amount')
                ->where('i.date', ">=", $from_date)
                ->where('i.date', "<=", $to_date)
                ->where('i.is_active', '=', 1);
//                ->get();

        if(!empty($purchase_code)){
            $gstCollection->having("purchase_code",'=',$purchase_code);
        }
        $gstCollection=$gstCollection->get();

        foreach ($gstCollection as $gst) {
            $customer_id = $gst->customer_id;
            $purchase_id = $gst->purchase_id;

//            $get_gst = DB::table('tbl_attribute_value as t ')
//                    ->leftJoin('tbl_attributes as i', 't.attributes_id', '=', 'i.id')
//                    ->where('t.attributetype_id', "=", 2)
//                    ->where('t.refernce_id', "=", $customer_id)
//                    ->where('i.attribute_code', "=", 'gst_no')
//                    ->first();
//
//            if ($get_gst != NULL) {
//                $gst->gst_no = $get_gst->value;
//            } else {
//                $gst->gst_no = '';
//            }
            $get_tax = DB::table('tbl_tax as t')
                    ->leftJoin('tbl_purchase_invoice_tax as i ', function($join) use ($purchase_id) {
                        $join->on('t.id', '=', 'i.tax_id');
                        $join->on('i.invoice_id', '=', DB::raw("'" . $purchase_id . "'"));
                        $join->on('i.is_active', '=', DB::raw("'1'"));
                    })
                    ->leftJoin(DB::raw('(select t.id,coalesce(sum((i.tax_amount)),0) as tax_amount  from  tbl_tax as t
left join tbl_purchase_invoice_tax as i on i.tax_id=t.id and t.is_active=1 and t.is_group != 1 AND i.invoice_id=' . $purchase_id . '  group by i.tax_id)
as amt on amt.id=t.id'), function($join1) {

                    })
                    ->select(DB::raw('coalesce(t.id, 0) as id'), 't.tax_name', DB::raw(" coalesce(amt.tax_amount, 0) as tax_amount"))
                    ->where('t.is_active', '=', 1)
                       ->where('t.is_group', '!=', 1)
                    ->distinct('t.id')
                    ->orderBy('t.id', 'asc')
                    ->get();
            $gst->tax_detail = $get_tax;
        }
        $resVal['tax_list'] = $taxCollection;

        $resVal['list'] = $gstCollection;

        LogHelper::info1('GSTPurchase' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }
    public function gstpurchase_csvexport(Request $request) {
        $gstpurchase = $this->GSTPurchase($request);
        ini_set('max_execution_time', 600); //increase max_execution_time to 10 min if data set is very large
        //create a file
        $filename = "gstpurchasesreport" . "_" . time() . ".csv";
        //  $filename = "customer" . "_" . date("Y-m-d_H-i", time()) . ".csv";
        $serverName = $_SERVER['SERVER_NAME'];

        $filePath = "download/$serverName/csv";

        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);

            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {

                if ($folderName == '')
                    $folderName = $folderList[$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index];

                if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        $filePath = getcwd() . '/' . $filePath;
        $output = fopen($filePath . "/" . $filename, 'w');
         $date_format=GeneralHelper::getDateFormat();
        $head=array('Vendor Name','Purchase Code','City','Mobile','Gst No','Invoice Date','Invoice Amount','Discount Amount','Taxable Amount','Tax Amount');
        foreach($gstpurchase['tax_list'] as $value){
            $values=$value->tax_name;
            array_push($head,$values);
        }
        fputcsv($output,$head);
        foreach ($gstpurchase['list'] as $values) {
            $tax_detail = $values->tax_detail;
             $ph=explode(',',$values->phone);
             $phNo=implode(', ',$ph);
             $date=GeneralHelper::CsvDate($values->date, $date_format);
               $csv_row = array($values->name,$values->purchase_code,$values->billing_city,$phNo,$values->gst_no,
                $date,$values->total_amount,$values->discount_amount,$values->valauable_amount,$values->tax_amount);
            foreach ($tax_detail as $value) {
                $key = $value->tax_amount;
                if (isset($key)) {
                    array_push($csv_row, $key);
                } else
                    array_push($csv_row, '');
            }
            fputcsv($output, $csv_row);

        }
        LogHelper::info1('GstPurchase CSV Download' . $request->fullurl(), $request->all());
        return response()->download($filePath . "/" . $filename, 'gstpurchasesreport.csv');
    }
   public function CategoryWiseStockReport(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        //Get Current and Previous Date
        $current_date = Carbon::now();

        $product_id = $request->input('product_id');
        $mode=$request->input('mode','');
        $category_id=$request->input('category_id','');
        $type = GeneralHelper::checkConfiqSetting();

        if ($type == 'product based') {

            $builder = DB::table('tbl_product as p')
                    ->leftJoin('tbl_category as c', 'p.category_id', '=', 'c.id')
                    ->leftJoin('tbl_inventory as a', 'a.product_id', '=', 'p.id')
                    ->select(DB::raw("coalesce(p.id, 0) as product_id,p.name,c.name as category_name ,  a.quantity,p.sales_price , Coalesce(cast((a.quantity*p.sales_price) as decimal(12,2)),0) as cost ")
                    )
                    ->where('a.quantity', '!=', 0)
                    ->where('a.is_active', '=', 1);

            if (!empty($product_id)) {
                $builder->where('p.id', '=', $product_id);
            }
            if($mode != ''){
              if ($mode==0) {
              $builder->where('p.has_inventory','=',0);
            }else{
              $builder->where('p.has_inventory','=',1);
            }
            }
            if (!empty($category_id)) {
                $builder->where('p.category_id', '=', $category_id);
            }
        } else {
            $builder = DB::table('tbl_bcn as a')
                    ->leftjoin('tbl_purchase_invoice_item as pi', 'pi.id', '=', 'a.ref_id')
                    ->leftJoin('tbl_product as p', 'a.product_id', '=', 'p.id')
                    ->leftJoin('tbl_category as c', 'p.category_id', '=', 'c.id')
                    ->select(DB::raw("coalesce(p.id, 0) as product_id,p.name ,c.name as category_name,Coalesce(cast(sum(a.qty_in_hand) as decimal(12,3)),0) as quantity,Coalesce(cast(sum((a.qty_in_hand*pi.mrp_price)) as decimal(12,2)),0) as overall_sales_price ,Coalesce(cast(sum((a.qty_in_hand*pi.purchase_price_w_tax)) as decimal(12,2)),0) as overall_purchase_price")       )
                    ->where('a.is_active', '=', 1)
                    ->where('a.qty_in_hand', '!=', 0)
                    ->where('p.is_active', '=', 1)
                    ->orderBy('p.name')
                    ->groupby('p.id');

            if (!empty($product_id)) {
                $builder->where('p.id', '=', $product_id);
            }
            if (!empty($category_id)) {
                $builder->where('p.category_id', '=', $category_id);
            }
            if($mode != ''){
            if ($mode==0) {
                $builder->where('p.has_inventory','=',0);
            }else{
              $builder->where('p.has_inventory','=',1);
            }

        }
        }
        $resVal['list'] = $builder->get();
        LogHelper::info1('CategoryWiseStockReport' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }
    public function categorywisestock_csvexport(Request $request) {
        $categoryWiseLists = $this->CategoryWiseStockReport($request);
        ini_set('max_execution_time', 600); //increase max_execution_time to 10 min if data set is very large
        //create a file
        $filename = "categorywisestockreport" . "_" . time() . ".csv";
        //  $filename = "customer" . "_" . date("Y-m-d_H-i", time()) . ".csv";
        $serverName = $_SERVER['SERVER_NAME'];

        $filePath = "download/$serverName/csv";

        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);

            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {

                if ($folderName == '')
                    $folderName = $folderList[$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index];

                if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        $filePath = getcwd() . '/' . $filePath;
        $output = fopen($filePath . "/" . $filename, 'w');
        fputcsv($output, array('Product Name','Category Name','Qty','OverAll Purchase Price','OverAll Sales Price'));
        foreach ($categoryWiseLists['list'] as $values) {
            fputcsv($output, array($values->name,$values->category_name,$values->quantity,$values->overall_purchase_price, $values->overall_sales_price));
        }
        LogHelper::info1('CategoryWiseStockReport CSV Download' . $request->fullurl(), $request->all());
        return response()->download($filePath . "/" . $filename, 'categorywisestockreport.csv');
    }
     public function BarcodeWiseReport(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        //Get Current and Previous Date
        $current_date = Carbon::now();


        $code =  $request->input('code', '');
        $product_id =  $request->input('product_id', '');
        $product_name =  $request->input('product_name', '');
        $cat_name =  $request->input('cat_name', '');
        $cat_id =  $request->input('cat_id', '');
        $mode=$request->input('mode','');

        $builder = DB::table('tbl_bcn as bc')
                ->leftJoin('tbl_purchase_invoice_item as pur', 'bc.ref_id', '=', 'pur.id')
                ->leftJoin('tbl_product as p','bc.product_id','=','p.id')
                ->leftJoin('tbl_category as c','p.category_id','=','c.id')
                ->select(DB::raw("bc.id,bc.product_id,bc.product_name,bc.code,bc.mrp_price as mrp_price,SUM(bc.qty_in_hand) as qty"), DB::raw("(SELECT   c.name  FROM tbl_category as c
                                 inner join tbl_product as v on v.category_id=c.id
                                WHERE bc.product_id = v.id
                                   ) as cat_name "),'c.id as cat_id')
                ->where("bc.code",'!=',NULL)
                 ->where('bc.is_active', '=', 1)
                 ->where('p.is_active', '=', 1)
                ->groupBy(DB::raw("bc.code"))
                 ->orderBy(DB::raw("bc.qty_in_hand"),'desc');


        $show_price = $request->input('show_price', '');
        $show_all = $request->input('show_all', '');

        if (!empty($show_price)) {
            $builder = $builder->addselect(DB::raw("bc.selling_price,pur.purchase_price as unit_price"));
}

         if (!empty($code)) {
            $builder = $builder->where("bc.code",'=',$code);
        }

         if (!empty($product_id)) {
            $builder = $builder->where("bc.product_id",'=',$product_id);
        }

        if (!empty($product_name)) {
            $builder = $builder->where("bc.product_name",'like','%'.$product_name.'%');
        }
        if (!empty($cat_name)) {
            $builder = $builder->where("c.name",'like','%'.$cat_name.'%');
        }
        if (!empty($cat_id)) {
            $builder = $builder->where("c.id",'=',$cat_id);
        }
        if($mode != ''){
        if ($mode==0) {
          $builder->where('p.has_inventory','=',0);
        }else{
          $builder->where('p.has_inventory','=',1);
        }
        }


        $builder = $builder->get();
        if ($show_all==1) {

            foreach($builder as $bu)
            {
                $id = $bu->id;
                $builder1 = DB::table('tbl_bcn')
                ->select(DB::raw("id,product_id,product_name,code,qty_in_hand as qty, code"))
                ->where("code",'!=',NULL)
                        ->where("id",'=',$id)
                         ->where('is_active', '=', 1)
                         ->orderBy(DB::raw("qty_in_hand"),'desc')
                ->get();
                $bu->detail = $builder1;
            }
        }

        $resVal['list'] = $builder;
        LogHelper::info1('BarcodeWise Report' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }
    public function barcodewise_csvexport(Request $request) {

        $barcodeWiseLists = $this->BarcodeWiseReport($request);
        $show_price=$request->input('show_price');
        ini_set('max_execution_time', 600); //increase max_execution_time to 10 min if data set is very large
        //create a file
        $filename = "barcodewisereport" . "_" . time() . ".csv";
        //  $filename = "customer" . "_" . date("Y-m-d_H-i", time()) . ".csv";
        $serverName = $_SERVER['SERVER_NAME'];

        $filePath = "download/$serverName/csv";

        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);

            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {

                if ($folderName == '')
                    $folderName = $folderList[$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index];

                if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        $filePath = getcwd() . '/' . $filePath;
        $output = fopen($filePath . "/" . $filename, 'w');
         $sr=1;
        $head= array('#','Barcode','Product Name','Category Name','Qty','Mrp Price','Total');
        if($show_price==1){
            array_push($head,'Unit Price','Selling Price');
            fputcsv($output,$head);
            foreach ($barcodeWiseLists['list'] as $values) {
            fputcsv($output, array($sr,$values->code,$values->product_name,$values->cat_name,$values->qty,$values->mrp_price, $values->qty*$values->mrp_price,$values->unit_price,$values->selling_price));  
            $sr++;
        }
        }
        else{
          fputcsv($output,$head);
          foreach ($barcodeWiseLists['list'] as $values) {
            fputcsv($output, array($sr,$values->code,$values->product_name,$values->cat_name,$values->qty,$values->mrp_price, $values->qty*$values->mrp_price));
            $sr++;
        }
        }
        LogHelper::info1('BarcodeWiseReport CSV Download' . $request->fullurl(), $request->all());
        return response()->download($filePath . "/" . $filename, 'barcodewisereport.csv');
    }
     public function CategoryWiseReport(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        //Get Current and Previous Date
        $current_date = Carbon::now();

        $show_all = $request->input('show_all', '');
        $cat_id = $request->input('category_id', '');
        $mode=$request->input('mode','');
        $type = GeneralHelper::checkConfiqSetting();

        if($type=='product based'){

            $builder = DB::table('tbl_inventory as a')
                    ->select(DB::raw("coalesce(p.category_id, 0) as cat_id, coalesce(c.name, 'Uncategorized') as  name, Coalesce(SUM(a.quantity),0) as qty "), DB::raw("cast(SUM(coalesce(a.quantity,0)*coalesce(p.sales_price,0)) as decimal(12,2)) as cost"))
                    ->leftJoin('tbl_product as p', 'a.product_id', '=', 'p.id')
                    ->leftjoin('tbl_category as c', 'c.id', '=', 'p.category_id')
                    ->where('a.is_active', '=', 1)
                    ->where('p.is_active', '=', 1)
                    ->groupBy("p.category_id");
            if (!empty($cat_id)) {
                $builder->where('c.id', '=', $cat_id);
            }
            if($mode != ''){
              if($mode==0){
                $builder->where('p.has_inventory','=',0);
        }else{
                $builder->where('p.has_inventory','=',1);
              }
            }
        }else{
                 $builder = DB::table('tbl_product as p')
                ->leftJoin('tbl_bcn as a', 'a.product_id', '=', 'p.id')
                ->leftJoin('tbl_category as c', 'p.category_id', '=', 'c.id')
                ->leftjoin('tbl_purchase_invoice_item as pi','pi.id','=','a.ref_id')
                ->select(DB::raw("coalesce(c.id, 0) as cat_id, coalesce(c.name, 'Uncategorized') as  name,Coalesce(SUM(a.qty_in_hand),0) as qty "),
                        DB::raw("Coalesce(cast((sum(a.qty_in_hand*pi.mrp_price)) as decimal(12,2)),0) as cost"),DB::raw("Coalesce(cast((sum(a.qty_in_hand*pi.purchase_price_w_tax)) as decimal(12,2)) ,0) as tot_mrp"))
                ->where('a.is_active', '=', 1)
                ->where('p.is_active', '=', 1)
                ->groupBy(DB::raw("c.id"));
                if(!empty($cat_id))
                {
                    $builder->where('c.id','=',$cat_id);
                }
                if ($mode != '') {
                  if($mode==0){
                    $builder->where('p.has_inventory','=',0);
                  }else{
                    $builder->where('p.has_inventory','=',1);
            }
                }
            }
           $builder = $builder->get();

        if ($show_all == 1) {
             if($type=='product based'){
            foreach ($builder as $bu) {
                $id = $bu->cat_id;

                $builder_detail = DB::table('tbl_product as p')
                        ->leftJoin('tbl_category as c', 'p.category_id', '=', 'c.id')
                        ->leftJoin('tbl_inventory as a', 'a.product_id', '=', 'p.id')
                    ->select(DB::raw("coalesce(p.id, 0) as product_id,p.name ,  a.quantity,p.sales_price , Coalesce(cast(a.quantity*p.sales_price as decimal(12,2)),0) as overall_sales_price ")
                    )
                        ->where('a.is_active', '=', 1)
                        ->where('p.category_id', '=', $id)
                        ->groupby('p.id');

          if($mode != ''){
            if($mode==0){
                  $builder_detail->where('p.has_inventory','=',0);
              }else{
                  $builder_detail->where('p.has_inventory','=',1);
            }
            }
            $bu->product = $builder_detail->get();
            }
        } else{
            foreach ($builder as $bu) {
                $id = $bu->cat_id;
                    $builder_detail = DB::table('tbl_bcn as a')
                            ->leftjoin('tbl_purchase_invoice_item as pi','pi.id','=','a.ref_id')
                    ->leftJoin('tbl_product as p', 'a.product_id', '=', 'p.id')
                    ->leftJoin('tbl_category as c', 'p.category_id', '=', 'c.id')
                    ->select(DB::raw("coalesce(p.id, 0) as product_id,p.name , Coalesce(sum(a.qty_in_hand),0) as quantity,Coalesce(cast(sum((a.qty_in_hand*pi.mrp_price)) as decimal(12,2)) ,0) as overall_sales_price ,Coalesce(cast(sum((a.qty_in_hand*pi.purchase_price_w_tax)) as decimal(12,2)) ,0) as overall_purchase_price")
                    )
                    ->where('a.is_active', '=', 1)
                        ->where('a.qty_in_hand','!=',0)
                        ->where('p.category_id', '=', $id)
                    ->where('p.is_active', '=', 1)
                    ->orderBy('p.name')
                        ->groupby('p.id');

          if($mode != ''){
            if($mode==0){
                  $builder_detail->where('p.has_inventory','=',0);
              }else{
                  $builder_detail->where('p.has_inventory','=',1);
            }
        }
            $bu->product = $builder_detail->get();
        }
        }
        }
        $resVal['list'] = $builder;
        LogHelper::info1('CategoryWiseReport' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }


    public function categorywisereport_csvexport(Request $request) {
        $categoryWiseLists = $this->CategoryWiseReport($request);
        ini_set('max_execution_time', 600); //increase max_execution_time to 10 min if data set is very large
        //create a file
        $filename = "categorywisereport" . "_" . time() . ".csv";
        //  $filename = "customer" . "_" . date("Y-m-d_H-i", time()) . ".csv";
        $serverName = $_SERVER['SERVER_NAME'];

        $filePath = "download/$serverName/csv";

        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);

            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {

                if ($folderName == '')
                    $folderName = $folderList[$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index];

                if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        $filePath = getcwd() . '/' . $filePath;
        $output = fopen($filePath . "/" . $filename, 'w');
        $arrHead=array('Category','Quantity','Overall Sales Price');
         $type = GeneralHelper::checkConfiqSetting();
        if($type=='purchase based')
           array_splice($arrHead,2,0, 'OverAll Purchase price');

        fputcsv($output, $arrHead);
        foreach ($categoryWiseLists['list'] as $values) {
            $arrVal=array($values->name,$values->qty,$values->cost);
            if($type=='purchase based')
                array_splice($arrVal,2,0, $values->tot_mrp);

            fputcsv($output, $arrVal);
        }
        LogHelper::info1('CategoryWiseReport CSV Download' . $request->fullurl(), $request->all());
        return response()->download($filePath . "/" . $filename, 'categorywisereport.csv');
    }

    public function SalesPaymentAccountReport(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $from_date = $request->input('from_date', '');
        $to_date = $request->input('to_date', '');
        $account_id = $request->input('account_id');
        $show_all = $request->input('show_all', '');
        $customer_id=$request->input('customer_id','');

        $builder = DB::table('tbl_accounts as a')
                ->select("a.account as accountName", DB::raw("Coalesce(sum(p.amount),0) as amount"))
                ->leftjoin('tbl_payment as p', 'p.account_id', '=', 'a.id')
                ->where('p.is_active', '=', 1)
                ->groupBy('a.id');

        if (!empty($from_date)) {
            $builder->whereDate('p.date', '>=', $from_date);
        }
        if (!empty($to_date)) {
            $builder->whereDate('p.date', '<=', $to_date);
        }
        if (!empty($account_id)) {
            $builder->where('a.id', '=', $account_id);
        }


        if ($show_all == 1) {

            $builder = DB::table('tbl_accounts as a')
                    ->select("a.account as accountName",DB::raw("date(p.date) as date"), DB::raw("Coalesce(sum(p.amount),0) as amount"), "c.fname", 'c.phone as customer_phone', "p.payment_mode", 'i.invoice_no', 'i.total_amount')
                    ->leftjoin('tbl_payment as p', 'p.account_id', '=', 'a.id')
                    ->leftjoin('tbl_invoice as i', 'i.id', 'p.invoice_id')
                    ->leftjoin('tbl_customer as c', 'c.id', '=', 'i.customer_id')
                    ->where('p.is_active', '=', 1)
                    ->where('i.is_active', '=', 1)
                    ->groupBy('i.id', 'p.date');


            if (!empty($from_date)) {
                $builder->whereDate('p.date', '>=', $from_date);
            }
            if (!empty($to_date)) {
                $builder->whereDate('p.date', '<=', $to_date);
            }
            if (!empty($account_id)) {
                $builder->where('a.id', '=', $account_id);
            }
            if (!empty($customer_id)) {
                $builder->where('c.id', '=', $customer_id);
            }
        }

        $resVal['list'] = $builder->get();
        LogHelper::info1('SalesPaymentAccountReport' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return $resVal;
    }

    public function SalesPurchaseAccountReport(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $from_date = $request->input('from_date', '');
        $to_date = $request->input('to_date', '');
        $account_id = $request->input('account_id');
        $show_all = $request->input('show_all', '');
        $customer_id = $request->input('customer_id', '');
        $type = $request->input('type', '');

        $builder1 = DB::table('tbl_accounts as a')
                ->select("a.account as accountName", DB::raw("Coalesce(sum(p.amount),0) as amount"), DB::raw(' "invoice_payment"   AS type'))
                ->leftjoin('tbl_payment as p', 'p.account_id', '=', 'a.id')
                ->where('p.is_active', '=', 1)
                ->groupBy('a.id')
        ;

        if (!empty($from_date)) {
            $builder1->whereDate('p.date', '>=', $from_date);
        }
        if (!empty($to_date)) {
            $builder1->whereDate('p.date', '<=', $to_date);
        }
        if (!empty($account_id)) {
            $builder1->where('a.id', '=', $account_id);
        }

        $builder = DB::table('tbl_accounts as a')
                ->select("a.account as accountName", DB::raw("Coalesce(sum(pp.amount),0) as amount"), DB::raw(' "purchase_payment"   AS type'))
                ->leftjoin('tbl_purchase_payment as pp', 'pp.account_id', '=', 'a.id')
                ->where('pp.is_active', '=', 1)
                ->groupBy('a.id')
                ->unionAll($builder1);

        if (!empty($from_date)) {
            $builder->whereDate('pp.date', '>=', $from_date);
        }
        if (!empty($to_date)) {
            $builder->whereDate('pp.date', '<=', $to_date);
        }
        if (!empty($account_id)) {
            $builder->where('a.id', '=', $account_id);
        }

        if ($show_all == 1) {
            if ($type == 'invoice_payment') {
                $builder = DB::table('tbl_accounts as a')
                        ->select("a.account as accountName", DB::raw("date(p.date) as date"), DB::raw("Coalesce(sum(p.amount),0) as amount"), "c.fname", 'c.phone as customer_phone', "p.payment_mode", 'i.invoice_no', 'i.total_amount')
                        ->leftjoin('tbl_payment as p', 'p.account_id', '=', 'a.id')
                        ->leftjoin('tbl_invoice as i', 'i.id', 'p.invoice_id')
                        ->leftjoin('tbl_customer as c', 'c.id', '=', 'i.customer_id')
                        ->where('p.is_active', '=', 1)
                        ->where('i.is_active', '=', 1)
                        ->groupBy('i.id', 'p.date');


                if (!empty($from_date)) {
                    $builder->whereDate('p.date', '>=', $from_date);
                }
                if (!empty($to_date)) {
                    $builder->whereDate('p.date', '<=', $to_date);
                }
                if (!empty($account_id)) {
                    $builder->where('a.id', '=', $account_id);
                }
                if (!empty($customer_id)) {
                    $builder->where('c.id', '=', $customer_id);
                }
            } else {
                $builder = DB::table('tbl_accounts as a')
                        ->select("a.account as accountName", DB::raw("date(pp.date) as date"), DB::raw("Coalesce(sum(pp.amount),0) as amount"), "c.fname", 'c.phone as customer_phone', "pp.payment_mode", 'pi.purchase_no as invoice_no', 'pi.total_amount')
                        ->leftjoin('tbl_purchase_payment as pp', 'pp.account_id', '=', 'a.id')
                        ->leftjoin('tbl_purchase_invoice as pi', 'pi.id', 'pp.purchase_invoice_id')
                        ->leftjoin('tbl_customer as c', 'c.id', '=', 'pi.customer_id')
                        ->where('pp.is_active', '=', 1)
                        ->where('pi.is_active', '=', 1)
                        ->groupBy('pi.id', 'pp.date') ;
                ;


                if (!empty($from_date)) {
                    $builder->whereDate('pp.date', '>=', $from_date);
                }
                if (!empty($to_date)) {
                    $builder->whereDate('pp.date', '<=', $to_date);
                }
                if (!empty($account_id)) {
                    $builder->where('a.id', '=', $account_id);
                }
                if (!empty($customer_id)) {
                    $builder->where('c.id', '=', $customer_id);
                }
            }
        }


        $resVal['list'] = $builder->get();
        LogHelper::info1('SalesPaymentAccountReport' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return $resVal;
    }
    
    public function ItemWiseAgeingReport(Request $request) {  //purchaseBased Report
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $from_date = $request->input('from_date', '');
        $to_date = $request->input('to_date', '');
        $show_all = $request->input('show_all', '');
        $product_id = $request->input('product_id', '');
        $has_inventory = $request->input('has_inventory','');
        $category_id=$request->input('category_id','');

        $builder = DB::table('tbl_product as p')
                ->leftjoin('tbl_category as c','c.id','=','p.category_id')
                ->leftjoin('tbl_purchase_invoice_item as pt','p.id','=','pt.product_id')
                ->leftjoin('tbl_purchase_invoice as pi','pi.id','=','pt.purchase_invoice_id')
                ->leftjoin(DB::Raw("(select Coalesce(sum(qty_in_hand),0) as inventory_qty,Coalesce(sum(qty_in_hand*purchase_price_w_tax),0) as purchase_value,Coalesce(sum(qty_in_hand*mrp_price),0) as mrp,product_name,p.id as product_id from tbl_product as p left join  tbl_purchase_invoice_item as pt on p.id=pt.product_id "
                                . "left join tbl_purchase_invoice as pi on pt.purchase_invoice_id=pi.id where pi.is_active=1 group by product_id) as pi on pi.product_id=p.id"), function($join) {

                })
                ->select('p.name as product_name','c.name as category_name','p.category_id','p.sku', 'p.id as product_id','p.has_inventory'
                        , DB::raw("coalesce(pi.inventory_qty,0) as remaining_qty"), DB::raw("Coalesce(cast(round(pi.purchase_value) as decimal(12,2)),0) as purchase_value"), DB::raw("coalesce(cast(round(pi.mrp) as decimal(12,2)),0) as mrp_price"))
                ->having('remaining_qty','!=',0)
                 ->where('p.is_active', '=', 1)
              //  ->where('p.has_inventory','=',1)
                 ->groupBy('p.id');

        if (!empty($product_id)) {
            $builder->where('p.id', '=', $product_id);
        }
        
        if (!empty($category_id)) {
            $builder->where('p.category_id', '=', $category_id);
        }
        if(!empty($from_date)){
            $builder->whereDate('pi.created_at','>=',$from_date);
        }
        if(!empty($to_date)){
            $builder->whereDate('pi.created_at','<=',$to_date);
        }

        if($has_inventory != '')
         {
            $builder->where('p.has_inventory','=',$has_inventory);
         }

        $collection = $builder->get();

        foreach ($collection as $collect) {
            $pdt_id = $collect->product_id;

            $data = DB::table('tbl_bcn as b')
                    ->leftjoin('tbl_purchase_invoice_item as pt','pt.id','=','b.ref_id')
                    ->leftjoin('tbl_purchase_invoice as pi','pi.id','=','pt.purchase_invoice_id')
                    ->leftjoin(DB::raw("(select pt.code,sum(pt.qty_in_hand) as inventory_qty,sum(pt.qty_in_hand*pt.purchase_price_w_tax) as purchase_value,sum(pt.qty_in_hand*pt.mrp_price) as mrp ,pt.product_name,pt.id,b.id as bcn_id from tbl_purchase_invoice as pi "
                                    . "left join tbl_purchase_invoice_item as pt on pi.id=pt.purchase_invoice_id "
                                    . " left join tbl_bcn as b on b.ref_id=pt.id where pt.product_id=$pdt_id and pi.is_active=1 group by pt.code) as pi on pi.bcn_id=b.id"), function($join) {

                    })
                    ->select(DB::raw("coalesce(b.code,'') as barcode")
                        , DB::raw("coalesce(pi.inventory_qty,0) as remaining_qty"), DB::raw("coalesce(cast(round(pi.purchase_value) as decimal(12,2)),0) as purchase_value"), DB::raw("coalesce(cast(round(pi.mrp) as decimal(12,2)),0) as mrp_price"))
                     ->having('remaining_qty','!=',0)
                     ->where('b.is_active','=',1);


         if(!empty($from_date)){
            $builder->whereDate('pi.created_at','>=',$from_date);
        }
        if(!empty($to_date)){
            $builder->whereDate('pi.created_at','<=',$to_date);
        }

            $collect->detail = $data ->get();
        }

        $resVal['list']=$collection;
        LogHelper::info1('ItemWiseAgeingReport' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return $resVal;
    }


    public function CategoryWiseAgeingReport(Request $request){
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $from_date = $request->input('from_date', '');
        $to_date = $request->input('to_date', '');
        $show_all = $request->input('show_all', '');
        $category_id = $request->input('category_id', '');
        $has_inventory=$request->input('has_inventory','-1');

        $builder = DB::table('tbl_product as c')
                ->leftjoin('tbl_category as k','c.category_id','=','k.id')
                ->leftjoin('tbl_purchase_invoice_item as pt','c.id','=','pt.product_id')
                ->leftjoin('tbl_purchase_invoice as pi','pi.id','=','pt.purchase_invoice_id')
                ->leftjoin(DB::Raw("(select sum(qty_in_hand) as stock_qty,sum(qty_in_hand*purchase_price_w_tax) as purchase_value,sum(qty_in_hand*mrp_price) as mrp,c.name as category_name,coalesce(c.id,0) as category_id from tbl_purchase_invoice as pi left join  tbl_purchase_invoice_item as pt on pi.id=pt.purchase_invoice_id "
                                . "left join tbl_product as p on pt.product_id=p.id left join tbl_category as c on c.id=p.category_id where pi.is_active=1 and (Case when '$has_inventory' >= 0 Then p.has_inventory='$has_inventory' Else true End)  group by c.id) as pi on pi.category_id=c.category_id"), function($join) {
                })

                ->select(DB::raw("coalesce(k.name,'Uncategorized') as category_name"), DB::raw("coalesce(k.id,0) as category_id")
                        , DB::raw("coalesce(pi.stock_qty,0) as remaining_qty"), DB::raw("coalesce(cast(round(pi.purchase_value) as decimal(12,2)),0) as purchase_value"), DB::raw("coalesce(cast(round(pi.mrp) as decimal(12,2)),0) as mrp_price"))
                         ->having('remaining_qty','!=',0)
                       // ->where('c.has_inventory','=',1)
                        ->groupby('k.id');

                if(($has_inventory)==1)
                {
                    $builder->where('c.has_inventory','=',$has_inventory);
                }
                elseif(($has_inventory)==0)
                {
                    $builder->where('c.has_inventory','=',$has_inventory);
                }

        if (!empty($category_id)) {
            $builder->where('c.id', '=', $category_id);
        }

        if(!empty($from_date)){
            $builder->whereDate('pi.created_at','>=',$from_date);
        }
        if(!empty($to_date)){
            $builder->whereDate('pi.created_at','<=',$to_date);
        }


        $collection = $builder->get();

        foreach($collection as $collect){
            $catId=$collect->category_id;

        $detail = DB::table('tbl_product as p')
                ->leftjoin('tbl_purchase_invoice_item as pt','p.id','=','pt.product_id')
                ->leftjoin('tbl_purchase_invoice as pi','pi.id','=','pt.purchase_invoice_id')
                ->leftjoin(DB::Raw("(select sum(qty_in_hand) as stock_qty,sum(qty_in_hand*purchase_price_w_tax) as purchase_value,sum(qty_in_hand*mrp_price) as mrp,product_name,p.id as product_id from tbl_purchase_invoice as pi left join  tbl_purchase_invoice_item as pt on pi.id=pt.purchase_invoice_id "
                                . "left join tbl_product as p on pt.product_id=p.id where pi.is_active=1 and p.category_id='".$catId."' group by product_id) as pi on pi.product_id=p.id"), function($join) {

                })

                ->select('p.name as product_name','p.sku', 'p.id as product_id'
                        , DB::raw("coalesce(pi.stock_qty,0) as remaining_qty"), DB::raw("coalesce(cast(round(pi.purchase_value) as decimal(12,2)),0) as purchase_value"),
                        DB::raw("coalesce(cast(round(pi.mrp) as decimal(12,2)),0) as mrp_price,p.has_inventory")
                        )
                 ->having('remaining_qty','!=',0)
                        //->where('p.has_inventory','=',1)
                  ->groupBy('p.id');

                if(($has_inventory)==1)
                {
                    $detail->where('p.has_inventory','=',$has_inventory);
                }
                elseif(($has_inventory)==0)
                {
                    $detail->where('p.has_inventory','=',$has_inventory);
                }
                if(!empty($from_date)){
            $builder->whereDate('pi.created_at','>=',$from_date);
        }
        if(!empty($to_date)){
            $builder->whereDate('pi.created_at','<=',$to_date);
        }
                $collect->detail=$detail->get();
        }

        $resVal['list']=$collection;
        LogHelper::info1('CategoryWiseAgeingReport' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return $resVal;
    }

    public function ItemWiseAgeingReport_csvexport(Request $request) {
        $ItemWiseLists = $this->ItemWiseAgeingReport($request);
        ini_set('max_execution_time', 600); //increase max_execution_time to 10 min if data set is very large
        //create a file
        $filename = "ItemWiseAgeingReport" . "_" . time() . ".csv";
        //  $filename = "customer" . "_" . date("Y-m-d_H-i", time()) . ".csv";
        $serverName = $_SERVER['SERVER_NAME'];

        $filePath = "download/$serverName/csv";

        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);

            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {

                if ($folderName == '')
                    $folderName = $folderList[$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index];

                if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        $filePath = getcwd() . '/' . $filePath;
        $output = fopen($filePath . "/" . $filename, 'w');
        fputcsv($output, array('Product Name','Category Name','Qty','Purchase Value','Mrp Price'));
        foreach ($ItemWiseLists['list'] as $values) {
            fputcsv($output, array($values->product_name,$values->category_name,$values->remaining_qty,$values->purchase_value, $values->mrp_price));
        }
        LogHelper::info1('ItemWiseAgeingReport CSV Download' . $request->fullurl(), $request->all());
        return response()->download($filePath . "/" . $filename, 'ItemWiseAgeingReport.csv');
    }

     public function CategoryWiseAgeingReport_csvexport(Request $request) {
        $ItemWiseLists = $this->CategoryWiseAgeingReport($request);
        ini_set('max_execution_time', 600); //increase max_execution_time to 10 min if data set is very large
        //create a file
        $filename = "CategoryWiseAgeingReport" . "_" . time() . ".csv";
        //  $filename = "customer" . "_" . date("Y-m-d_H-i", time()) . ".csv";
        $serverName = $_SERVER['SERVER_NAME'];

        $filePath = "download/$serverName/csv";

        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);

            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {

                if ($folderName == '')
                    $folderName = $folderList[$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index];

                if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        $filePath = getcwd() . '/' . $filePath;
        $output = fopen($filePath . "/" . $filename, 'w');
        fputcsv($output, array('Category Name','Qty','Purchase Value','Mrp Price'));
        foreach ($ItemWiseLists['list'] as $values) {
            fputcsv($output, array($values->category_name,$values->remaining_qty,$values->purchase_value, $values->mrp_price));
        }
        LogHelper::info1('CategoryWiseAgeingReport CSV Download' . $request->fullurl(), $request->all());
        return response()->download($filePath . "/" . $filename, 'CategoryWiseAgeingReport.csv');
    }
    
}

?>
