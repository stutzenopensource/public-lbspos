<?php
namespace App\Http\Controllers;
use DB;
use App\RoleAccess;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use App\Helper\AuthorizationHelper;
use App\Helper\LogHelper;
use App\Helper\NotificationHelper;


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserScreenAccess
 *
 * @author Deepa
 */
class RoleAccessController extends Controller{
    //put your code here
    public function update(Request $request){
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Role Access Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'update');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
       
        $accesslist = $request->all();
        $noti_role_id ="";
        foreach ($accesslist as $std) 
        {
            $noti_role_id = $std['role_id'];
            if ($std['role_id'] != NULL) {
                $useracessCollection = RoleAccess::where('role_id', "=", $std['role_id'])
                        ->where('screen_id', "=", $std['screen_id'])
                        ->get();
            }

            if (count($useracessCollection) > 0) {
                $useraccess = $useracessCollection->first();
            } else {
                $useraccess = new RoleAccess;
            }
            
            $builder = DB::table('tbl_screen')
                ->select('code')
                ->where('id', "=", $std['screen_id'])->first();            
            $useraccess->fill($std);
            $useraccess->screen_code = $builder->code;
            $useraccess->created_by = $currentuser->id;
            $useraccess->is_active = 1;
            $useraccess->updated_by = $currentuser->id;
            $useraccess->save();
        }
         
        LogHelper::info1('Role Access Update ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Role Access', 'update', $screen_code, $noti_role_id);

        return $resVal;
    }
     public function listAll(Request $request) {
         $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'list');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
       
        $id = $request->input('id');
        $roleid = $request->input('role_id', '');
        $isactive =$request->input('is_active','');
        
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        $builder = DB::table('tbl_role_access as us')
                ->leftJoin('tbl_screen as s', 'us.screen_id', '=', 's.id')
                ->select('us.*', 's.code as code','s.sno as sno','s.section_sqno');

        if (!empty($id)) {
            $builder->where('us.id', '=', $id);
        }
        if ($roleid != '') {
            $builder->where('us.role_id', '=', $roleid);
        }
        if ($isactive != '') {
            $builder->where('us.is_active', '=', $isactive);
        }
        
        $builder->orderBy('s.section_sqno','asc')
                ->orderBy('s.sno','asc');
        
        $resVal['total'] = $builder->count();
        $resVal['list'] = $builder->skip($start)->take($limit)->get();
        LogHelper::info1('Role Access List All ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }
}

?>
