<?php

namespace App\Http\Controllers;

use App\Role;
use DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helper\AuthorizationHelper;
use App\Helper\LogHelper;
use App\Helper\NotificationHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserRoleController
 *
 * @author Deepa
 */
class RoleController extends Controller {

    //put your code here
    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Role Added Successfully';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();


        $userrole = new Role;

        if ($request->input('name') != NULL) {

            $userRoleCollection = Role::where('name', "=", $request->input('name'))->get();

            if (count($userRoleCollection) > 0) {
                $resVal['success'] = FALSE;
                $resVal['message'] = 'Name Is Already Exists';
                return $resVal;
            }
        }
        $userrole->created_by = $currentuser->id;
        $userrole->updated_by = $currentuser->id;
        $userrole->fill($request->all());
        $userrole->save();
        $resVal['id'] = $userrole->id;

        LogHelper::info1('Role Save ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Role', 'save', $screen_code, $userrole->id);


        return $resVal;
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Role Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        try {
            $userrole = Role::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'User Role Not Found';
            return $resVal;
        }
        
        if ($request->input('name') != NULL) {

            $userRoleCollection = Role::where('name', "=", $request->input('name'))
                    ->where('id','!=',$id)->get();

            if (count($userRoleCollection) > 0) {
                $resVal['success'] = FALSE;
                $resVal['message'] = 'Name Is Already Exists';
                return $resVal;
            }
        }

        $userrole->updated_by = $currentuser->id;
        $userrole->fill($request->all());
        $userrole->save();

        LogHelper::info1('Role Update ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Role', 'update', $screen_code, $userrole->id);


        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $name = $request->input('name', '');

        $is_active = $request->input('is_active', '');

        //$status = $request->input('status', '');
        $builder = DB::table('tbl_role')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if ($is_active != '') {
            $builder->where('is_active', '=', $is_active);
        }

        if (!empty($name)) {
            $builder->where('name', 'like', '%' . $name . '%');
        }

        $builder->orderBy('id', 'desc');

        $resVal['total'] = $builder->count();
        $resVal['list'] = $builder->skip($start)->take($limit)->get();

        LogHelper::info1('Role List All ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));


        return ($resVal);
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = ' Role Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        try {
            $userrole = Role::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Role Not Found';
            return $resVal;
        }
        
        $userCount=DB::table('tbl_user')
                ->where('role_id','=',$id)
                ->where('is_active','=',1)
                ->orWhere('rolename','=',$userrole->name)
                ->get();
        
        if(count($userCount)>0){
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Unable To Delete.Role Mapped To User';
            return $resVal;
        }

        $userrole->delete();

        LogHelper::info1('Role Delete ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Role', 'delete', $screen_code, $userrole->id);


        return $resVal;
    }

}

?>
