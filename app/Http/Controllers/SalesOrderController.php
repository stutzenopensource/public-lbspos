<?php

namespace App\Http\Controllers;

use DB;
use App\SalesOrder;
use App\SalesOrderItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Transaction;
use App\Helper\AccountHelper;
use App\Payment;
use App\DealActivity;
use App\Helper\ActivityLogHelper;
use Carbon\Carbon;
use App\Helper\GeneralHelper;
use App\Helper\LogHelper;
use App\Helper\NotificationHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of QuotesController
 *
 * @author Deepa
 */
class SalesOrderController extends Controller {

    //put your code here
    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Sales Order Added Successfully';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        
        $subtotal = 0;
        $discount_amount = 0;
        $roundoff = 0;
        $tax_id = 0;
        $tax_amount = 0;
        $taxable_amount = 0;
        $total_before_roundoff = 0;
        $total = 0;

        $invoiceItemCol = $request->input('item');
        foreach ($invoiceItemCol as $item) {
            $item['total_price'] = floatval($item['unit_price']) * floatval($item['qty']);

            $item['total_price'] = floatval(number_format($item['total_price'], 2, '.', ''));
            $subtotal = $subtotal + $item['total_price'];
        }
        $discountMode = $request->input('discount_mode', '');
        $discountValue = $request->input('discount_percentage', '');
        if (empty($discountMode) || empty($discountValue)) {
            $discount_amount = 0;
            $taxable_amount = $subtotal;
        } else {

            if ($discountMode == '%') {
                $discount_amount = $subtotal * $discountValue / 100;
                $discount_amount = floatval(number_format($discount_amount, 2, '.', ''));
            } else {
                $discount_amount = $discountValue;
            }

            $taxable_amount = $subtotal - $discount_amount;
        }

        $taxId = $request->input('tax_id', '');
        if (!empty($taxId)) {
            $tax_list = GeneralHelper::calculatedInvoiceTax(0, $taxId, $taxable_amount, 'sales');

            foreach ($tax_list as $tax) {

                $tax_amount += $tax->tax_amount;
            }
        }
        //finding roundoff value 
        $total_before_roundoff = $subtotal - $discount_amount + $tax_amount;
        $total_after_roundoff = round($total_before_roundoff);
        $roundoff = $total_after_roundoff - $total_before_roundoff;
        $roundoff = floatval(number_format($roundoff, 2, '.', ''));

        //finding grant total value 
        $total = $subtotal - $discount_amount + $tax_amount + $roundoff;
        $total = floatval(number_format($total, 2, '.', ''));
        
        $sales = new SalesOrder;
        $sales->created_by = $currentuser->id;
        $sales->updated_by = $currentuser->id;
        $sales->is_active = $request->input('is_active', 1);
        $sales->fill($request->all());
        $sales->subtotal = $subtotal;
        $sales->tax_amount = $tax_amount;
        $sales->discount_amount = $discount_amount;
        $sales->round_off = $roundoff;
        $packing_charge = $request->input('packing_charge', 0);
        $insurance_charge = $request->input('insurance_charge', 0);
        $total = $total + $packing_charge + $insurance_charge;
        $sales->total_amount = $total;
        
        $customerID = $request->input('customer_id');
        
        $sales->status = 'new';
        $sales->save();
        $quoteItemCol = ($request->input('item'));

        $settings = DB::table('tbl_app_config')
                        ->select('*')->where('setting', '=', 'sales_product_list')->where('value', 'product based')->get()->first();
        if (!empty($settings)) {
            $product_with_taxlist = GeneralHelper::getProductTax($request->input('item'), $customerID);
            $tax_amount = 0;
            $discount_amount = 0;
            foreach ($product_with_taxlist as $item) {
                $salesitems = new SalesOrderItem;
                $salesitems->fill($item);
                $salesitems->sales_order_id = $sales->id;
                $salesitems->is_active = $request->input('is_active', 1);
                $salesitems->customer_id = $request->input('customer_id');
                $salesitems->duedate = $sales->duedate;
                $salesitems->paymentmethod = $sales->paymentmethod;
                $salesitems->tax_amount = $sales->tax_amount;
                $salesitems->created_by = $currentuser->id;
                $salesitems->updated_by = $currentuser->id;
                $salesitems->save();
                $product_discount_amount = GeneralHelper::calculateDiscount($item);
                $salesitems->discount_amount = $product_discount_amount;

                $remain_value = ($item['qty'] * $item['unit_price']) - $product_discount_amount;
                $item['produt_rem_amount'] = $remain_value;

                $product_tax_amount = GeneralHelper::insertProductTax($item, $sales->id, $salesitems->id, 'sales_order');
                $sales_value = (($item['qty'] * $item['unit_price']) - $product_discount_amount) - $product_tax_amount;

                $discount_amount = $discount_amount + $product_discount_amount;
                $tax_amount = $tax_amount + $product_tax_amount;

                $salesitems->tax_amount = $product_tax_amount;
                $salesitems->sales_value = $sales_value;
                $salesitems->save();
            }

            $total_before_roundoff = $subtotal - $discount_amount + $packing_charge + $insurance_charge;
            $total_after_roundoff = round($total_before_roundoff);
            $roundoff = $total_after_roundoff - $total_before_roundoff;
            $roundoff = floatval(number_format($roundoff, 2, '.', ''));

            //finding grant total value 
            $total = $subtotal - $discount_amount + $roundoff + $packing_charge + $insurance_charge;
            ;
            $total = floatval(number_format($total, 2, '.', ''));



            $sales->subtotal = $subtotal;
            $sales->tax_amount = $tax_amount;
            $sales->discount_amount = $discount_amount;
            $sales->round_off = $roundoff;

            $sales->total_amount = $total;

            $sales->save();
        } else {
        foreach ($quoteItemCol as $item) {

            $salesitems = new SalesOrderItem;
            $salesitems->fill($item);
            $salesitems->sales_order_id = $sales->id;
            $salesitems->is_active = $request->input('is_active', 1);
            $salesitems->customer_id = $request->input('customer_id');
            $salesitems->duedate = $sales->duedate;
            $salesitems->paymentmethod = $sales->paymentmethod;
            $salesitems->tax_amount = $sales->tax_amount;
            $salesitems->created_by = $currentuser->id;
            $salesitems->updated_by = $currentuser->id;
            $salesitems->save();
        }
        }
        $resVal['id'] = $sales->id;
        $deal_id = $request->input('deal_id', '');
        if ($deal_id != '') {
            $deal = new DealActivity;     
            $get_contact_id = DB::table('tbl_customer')->where('is_active', 1)
                            ->where('id', '=', $request->customer_id)->first();
            $current_date = date('Y-m-d');
            $timestamp = date('H:i:s');
            $timestamp = strtotime($timestamp);
            $time = date('h:i:s');
            $time = strtotime($time);

            $deal->created_by = $currentuser->id;
            $deal->updated_by = $currentuser->id;
            $deal->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $deal->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $deal->time = $time;
            $deal->date = $current_date;
            $deal->type = 'salesorder';
            $deal->description = "created an Sales Order";
            $deal->contact_id = $get_contact_id->contact_id;
            $deal->user_id = $currentuser->id;
            $deal->user_name = $currentuser->f_name;
            $deal->is_active = 1;
            $deal->deal_id = $deal_id;
            $deal->ref_id = $sales->id;
            $deal->comments = "created an Sales Order";

            $deal->save();
        }
        ActivityLogHelper::salesOrderSave($sales);
        
        LogHelper::info1('Sales Order Save ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Sales Order', 'save', $screen_code, $sales->id);
        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $id = $request->input('id');
        $mobile = $request->input('mobile', '');
        $sales_order_code = $request->input('sales_order_code', '');
        $customerid = $request->input('customerid');
        $fromValidDate = $request->input('from_validdate', '');
        $toValidDate = $request->input('to_validdate', '');
        $fromDate = $request->input('from_date', '');
        $toDate = $request->input('to_date', '');
        $orderDate = $request->input('order_date', '');
        $vaildDate = $request->input('valid_date', '');
        $deal_id = $request->input('deal_id');
        $status = $request->input('status');


        $isactive = $request->input('is_active', '');
        //$builder = DB::table('tbl_quote')->select('*');
        
        $builder = DB::table('tbl_sales_order as q')
                ->leftJoin('tbl_customer as c', 'q.customer_id', '=', 'c.id')
                ->leftJoin('tbl_customer as d', 'q.payee', '=', 'd.id')
                ->leftJoin('tbl_customer as e', 'q.consignee', '=', 'e.id')
                ->leftJoin('tbl_tax as t', 'q.tax_id', '=', 't.id')
                ->select('q.*', 'c.fname as customer_fname', 'c.lname as customer_lname', 't.tax_name as taxname', 'c.phone', 'c.email', 'd.fname as payee_name', 'e.fname as consignee_name');
                
        /* $builder = DB::table('tbl_sales_order as q')
                ->leftJoin('tbl_customer as c', 'q.customer_id', '=', 'c.id')
                ->leftJoin('tbl_tax as t', 'q.tax_id', '=', 't.id')
          ->select('q.*', 'c.fname as customer_fname', 'c.lname as customer_lname', 't.tax_name as taxname'); */

        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        if (!empty($id)) {
            $builder->where('q.id', '=', $id);
        }

        if ($isactive != '') {
            $builder->where('q.is_active', '=', $isactive);
        }
         if (!empty($sales_order_code)) {
            $builder->where('q.sales_order_code', 'like', '%' . $sales_order_code . '%');
        }
        if (!empty($fromValidDate)) {

            $builder->whereDate('q.validuntil', '>=', $fromValidDate);
        }
         if (!empty($mobile)) {
            $builder->where('c.phone', '=', $mobile);
        }
        if (!empty($toValidDate)) {

            $builder->whereDate('q.validuntil', '<=', $toValidDate);
        }
        if (!empty($fromDate)) {

            $builder->whereDate('q.date', '>=', $fromDate);
        }
        if (!empty($orderDate)) {
            $builder->whereDate('q.date', '=', $orderDate);
        }
        if (!empty($vaildDate)) {
            $builder->whereDate('q.validuntil', '=', $vaildDate);
        }
        if (!empty($toDate)) {

            $builder->whereDate('q.date', '<=', $toDate);
        }
        if (!empty($status)) {
            $builder->where('q.status', '=', $status);
        }
        if (!empty($customerid)) {
            $builder->where('q.customer_id', '=', $customerid);
        }
        if (!empty($deal_id)) {
            $builder->where('q.deal_id', '=', $deal_id);
        }

        $builder->orderBy('q.id', 'desc');

        $resVal['total'] = $builder->count();
        $resVal['list'] = $builder->skip($start)->take($limit)->get();
        LogHelper::info1('Sales Order List All ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Sales Order Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $sales = SalesOrder::findOrFail($id);

            if ($sales->is_active == 0) {
                return $resVal;
            }
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Sales Order Not Found';
            return $resVal;
        }
        $sales->created_by = $currentuser->id;
        $sales->updated_by = $currentuser->id;
        $sales->is_active = 0;
        //$sales->fill($request->all());
        $sales->update();


        DB::table('tbl_sales_order_item')->where('sales_order_id', '=', $id)->update(['is_active' => 0]);

        DB::table('tbl_sales_order_item_tax')->where('sales_order_id', '=', $id)->update(['is_active' => 0]);


        /*
         * Code to adjust bank money and transaction table entry 
         */
        $paymentCollection = Payment::where('invoice_id', '=', 0)
                        ->where('quote_id', '=', 0)
                        ->where('sales_order_id', '=', $id)
                        ->where('is_active', '=', 1)->get();
        foreach ($paymentCollection as $payment) {

            AccountHelper::withDraw($payment->account_id, $payment->amount);

            //deavtivate the transaction
            DB::table('tbl_transaction')
                    ->where('voucher_type', '=', Transaction::$SALES_PAYMENT)
                    ->where('voucher_number', '=', $payment->id)->update(['is_active' => 0]);
        }

        $resVal['id'] = $sales->id;
        $deal_id = $sales->deal_id;
        if ($deal_id != 0) {
            $deal = new DealActivity;
            $get_contact_id = DB::table('tbl_customer')->where('is_active', 1)
                            ->where('id', '=', $request->customer_id)->first();
            $current_date = date('Y-m-d');
            $timestamp = date('H:i:s');
            $timestamp = strtotime($timestamp);
            $time = date('h:i:s');
            $time = strtotime($time);

            $deal->created_by = $currentuser->id;
            $deal->updated_by = $currentuser->id;
            $deal->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $deal->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $deal->time = $time;
            $deal->date = $current_date;
            $deal->type = 'salesorder';
            $deal->description = "delete an sales order";
            $deal->contact_id = $get_contact_id->contact_id;
            $deal->user_id = $currentuser->id;
            $deal->user_name = $currentuser->f_name;
            $deal->is_active = 1;
            $deal->deal_id = $deal_id;
            $deal->ref_id = $sales->id;
            $deal->comments = "delete an sales order";

            $deal->save();
        }
        ActivityLogHelper::salesOrderDelete($sales);
        LogHelper::info1('Sales Order Delete ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Sales Order', 'delete', $screen_code, $sales->id);

        return $resVal;
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Sales Order Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $sales = SalesOrder::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Sales Order Not Found';
            return $resVal;
        }
        $subtotal = 0;
        $discount_amount = 0;
        $roundoff = 0;
        $tax_id = 0;
        $tax_amount = 0;
        $taxable_amount = 0;
        $total_before_roundoff = 0;
        $total = 0;

        $invoiceItemCol = $request->input('item');
        foreach ($invoiceItemCol as $item) {
            $item['total_price'] = floatval($item['unit_price']) * floatval($item['qty']);

            $item['total_price'] = floatval(number_format($item['total_price'], 2, '.', ''));
            $subtotal = $subtotal + $item['total_price'];
        }
        $discountMode = $request->input('discount_mode', '');
        $discountValue = $request->input('discount_percentage', '');
        if (empty($discountMode) || empty($discountValue)) {
            $discount_amount = 0;
            $taxable_amount = $subtotal;
        } else {

            if ($discountMode == '%') {
                $discount_amount = $subtotal * $discountValue / 100;
                $discount_amount = floatval(number_format($discount_amount, 2, '.', ''));
            } else {
                $discount_amount = $discountValue;
            }

            $taxable_amount = $subtotal - $discount_amount;
        }

        $taxId = $request->input('tax_id', '');
        if (!empty($taxId)) {
            $tax_list = GeneralHelper::calculatedInvoiceTax($id, $taxId, $taxable_amount, 'sales');

            foreach ($tax_list as $tax) {

                $tax_amount += $tax->tax_amount;
            }
        }
        $packing_charge = $request->input('packing_charge', 0.0);
        $insurance_charge = $request->input('insurance_charge', 0.0);

        //finding roundoff value 
        $total_before_roundoff = $subtotal - $discount_amount + $tax_amount + $packing_charge + $insurance_charge;
        $total_after_roundoff = round($total_before_roundoff);
        $roundoff = $total_after_roundoff - $total_before_roundoff;
        $roundoff = floatval(number_format($roundoff, 2, '.', ''));

        //finding grant total value 
        $total = $subtotal - $discount_amount + $tax_amount + $roundoff + $packing_charge + $insurance_charge;
        ;
        $total = floatval(number_format($total, 2, '.', ''));
        
        $builder = DB::table('tbl_sales_order_item')->where('sales_order_id', $id)->delete();

        $builder = DB::table('tbl_sales_order_item_tax')->where('sales_order_id', $id)->delete();

        $quoteItemCol = ($request->input('item'));

        $customerID = $request->input('customer_id');

        $settings = DB::table('tbl_app_config')
                        ->select('*')->where('setting', '=', 'sales_product_list')->where('value', 'product based')->get()->first();
        if (!empty($settings)) {
            $product_with_taxlist = GeneralHelper::getProductTax($request->input('item'), $customerID);
            $tax_amount = 0;
            $discount_amount = 0;
            foreach ($product_with_taxlist as $item) {
                $salesitems = new SalesOrderItem;
                $salesitems->fill($item);
                $salesitems->sales_order_id = $sales->id;
                $salesitems->is_active = $request->input('is_active', 1);
                $salesitems->customer_id = $request->input('customer_id');
                $salesitems->duedate = $sales->duedate;
                $salesitems->paymentmethod = $sales->paymentmethod;
                $salesitems->tax_amount = $sales->tax_amount;
                $salesitems->created_by = $currentuser->id;
                $salesitems->updated_by = $currentuser->id;
                $salesitems->save();
                $product_discount_amount = GeneralHelper::calculateDiscount($item);
                $salesitems->discount_amount = $product_discount_amount;

                $remain_value = ($item['qty'] * $item['unit_price']) - $product_discount_amount;
                $item['produt_rem_amount'] = $remain_value;

                $product_tax_amount = GeneralHelper::insertProductTax($item, $sales->id, $salesitems->id, 'sales_order');
                $sales_value = (($item['qty'] * $item['unit_price']) - $product_discount_amount) - $product_tax_amount;

                $discount_amount = $discount_amount + $product_discount_amount;
                $tax_amount = $tax_amount + $product_tax_amount;

                $salesitems->tax_amount = $product_tax_amount;
                $salesitems->sales_value = $sales_value;
                $salesitems->save();
            }

            $total_before_roundoff = $subtotal - $discount_amount + $packing_charge + $insurance_charge;
            $total_after_roundoff = round($total_before_roundoff);
            $roundoff = $total_after_roundoff - $total_before_roundoff;
            $roundoff = floatval(number_format($roundoff, 2, '.', ''));

            //finding grant total value 
            $total = $subtotal - $discount_amount + $roundoff + $packing_charge + $insurance_charge;
            ;
            $total = floatval(number_format($total, 2, '.', ''));



            $sales->subtotal = $subtotal;
            $sales->tax_amount = $tax_amount;
            $sales->discount_amount = $discount_amount;
            $sales->round_off = $roundoff;

            $sales->total_amount = $total;

            $sales->save();
        } else {
        foreach ($quoteItemCol as $item) {

            $salesitems = new SalesOrderItem;
            $salesitems->fill($item);
            $salesitems->sales_order_id = $sales->id;
            $salesitems->is_active = $request->input('is_active', 1);
            $salesitems->customer_id = $request->input('customer_id');
            $salesitems->duedate = $sales->duedate;
            $salesitems->paymentmethod = $sales->paymentmethod;
            $salesitems->tax_amount = $sales->tax_amount;
            $salesitems->created_by = $currentuser->id;
            $salesitems->updated_by = $currentuser->id;
            $salesitems->save();
        }
        $sales->fill($request->all());
        $sales->subtotal = $subtotal;
        $sales->tax_amount = $tax_amount;
        $sales->discount_amount = $discount_amount;
        $sales->round_off = $roundoff;  
        $sales->total_amount = $total;
        $sales->created_by = $currentuser->id;
        $sales->updated_by = $currentuser->id;
        $sales->save();
        }
        $totalamount = $request->input('total_amount');

        $payment = DB::table('tbl_payment')->where('sales_order_id', '=', $id)->where('is_active', '=', 1)->sum('amount');

        if (!empty($request->status)) {
            $sales->status = $request->status;
        } else {
            if ($payment == 0) {
                $sales->status = 'unpaid';
            } else if ($totalamount <= $payment) {

                $sales->status = 'paid';
            } else if ($totalamount >= $payment) {

                $sales->status = 'partiallyPaid';
            }
            $sales->advance_amount = $payment;
        }
        $sales->save();

        $resVal['id'] = $sales->id;

        $deal_id = $request->input('deal_id', '');
        if ($deal_id != '') {
            $deal = new DealActivity;     
            $get_contact_id = DB::table('tbl_customer')->where('is_active', 1)
                            ->where('id', '=', $request->customer_id)->first();
            $current_date = date('Y-m-d');
            $timestamp = date('H:i:s');
            $timestamp = strtotime($timestamp);
            $time = date('h:i:s');
            $time = strtotime($time);

            $deal->created_by = $currentuser->id;
            $deal->updated_by = $currentuser->id;
            $deal->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $deal->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $deal->time = $time;
            $deal->date = $current_date;
            $deal->type = 'salesorder';
            $deal->description = "updated an Sales Order";
            $deal->contact_id = $get_contact_id->contact_id;
            $deal->user_id = $currentuser->id;
            $deal->user_name = $currentuser->f_name;
            $deal->is_active = 1;
            $deal->deal_id = $deal_id;
            $deal->ref_id = $sales->id;
            $deal->comments = "updated an Sales Order";

            $deal->save();
        }
        ActivityLogHelper::salesOrderModify($sales);
        LogHelper::info1('Sales Order Update ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Sales Order', 'update', $screen_code, $sales->id);

        return $resVal;
    }

    public function detail(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $id = $request->input('id');
        $decimalFormat = GeneralHelper::decimalFormat();

        $is_active = $request->input('is_active', '');
        //$builder = DB::table('tbl_quotes')->select('*');
        $builder = DB::table('tbl_sales_order as q')
                ->leftJoin('tbl_customer as c', 'q.customer_id', '=', 'c.id')
                ->leftJoin('tbl_customer as d', 'q.payee', '=', 'd.id')
                ->leftJoin('tbl_customer as e', 'q.consignee', '=', 'e.id')
                ->leftJoin('tbl_tax as t', 'q.tax_id', '=', 't.id')
                ->select('q.*', 'c.fname as customer_fname', 'c.lname as customer_lname', 't.tax_name as taxname', 'c.phone', 'c.email', 'd.fname as payee_name', 'e.fname as consignee_name')
                ->where('q.id', '=', $id);

        $salestitems = DB::table('tbl_sales_order_item')->where('sales_order_id', $id)
                ->select('*', DB::raw("cast(qty as DECIMAL(12," . $decimalFormat . ")) as qty"))
                ->where('is_active', '=', 1);
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        if (!empty($id)) {
            $builder->where('q.id', '=', $id);
        }

        if ($is_active != '') {
            $builder->where('q.is_active', '=', $is_active);
        }
        $builder->orderBy('q.id', 'desc');

        $resVal['total'] = $builder->count();
        $salestCollection = $builder->skip($start)->take($limit)->get();
        if (count($salestCollection) == 1) {

            $salest = $salestCollection->first();

            $salest->item = $salestitems->get();
            $resVal['data'] = $salest;
        } else {
            $resVal['data'] = null;
        }
        // $resVal['list'] = $builder->skip($start)->take($limit)->get();
         LogHelper::info1('Sales Order Detail ' . $request->fullurl(), $request->all());
        return ($resVal);
    }

}

?>
