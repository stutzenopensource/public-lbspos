<?php
namespace App\Http\Controllers;
use DB;
Use App\Quotesitem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException; 
use App\Helper\AuthorizationHelper;
use App\Helper\LogHelper;
use App\Helper\NotificationHelper;
//use App\Transformer\InvoiceTransformer;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of QuotesitemsController
 *
 * @author Deepa
 */
class QuotesitemController extends Controller {
    //put your code here
    public function save (Request $request){
         $resVal = array();
        $resVal['message'] = 'QuotesItems Added Successfully';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'save');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $quotesitem = new Quotesitem;
       
        $quotesitem->created_by = $currentuser->id;
        $quotesitem->updated_by = $currentuser->id;
        $quotesitem->fill($request->all());
        $quotesitem->save();

        $resVal['id'] = $quotesitem->id;
        
        LogHelper::info1('Sales Order Item Save ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Sales Order Item', 'save', $screen_code, $quotesitem->id);

        return $resVal;

    }
     public function listAll(Request $request) {
         $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'list');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
       
        $id = $request->input('id');
       
        $isactive= $request->input('is_active','');
       
        
      
       
        //$status = $request->input('status', '');
        $builder = DB::table('tbl_quote_item')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
         
      
        if ($isactive != '') {
            $builder->where('is_active', '=', $isactive);
        }
        
        $builder->orderBy('id','desc');
        $resVal['total'] = $builder->count();
        $resVal['list'] = $builder->skip($start)->take($limit)->get();
        LogHelper::info1('Sales Order Item List All ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }
     public function delete(Request $request,$id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'QuotesItem Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'delete');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        try {
            $quotstitem = Quotesitem::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'QuotesItem Not Found';
            return $resVal;
        }

        $quotstitem->delete();
        LogHelper::info1('Sales Order Item Delete ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Sales Order Item', 'delete', $screen_code, $quotstitem->id);

        return $resVal;
    }
     public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'QuotesItems Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'update');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $quotstitem = Quotesitem::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'QuotesItems Not Found';
            return $resVal;
        }
        $quotstitem->created_by = $currentuser->id;
        $quotstitem->fill($request->all());
        $quotstitem->save();
        LogHelper::info1('Sales Order Item Update ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Sales Order Item', 'update', $screen_code, $quotstitem->id);

        return $resVal;
    }
    
}

?>
