<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\State;
use Illuminate\Http\Request;
use App\Transformer\UomTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\LogHelper;
use App\Helper\NotificationHelper;

class StateController extends Controller {

    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'State Added Successfully';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

//        $validator = Validator::make($request->all(), [
//                    'code' => 'required|unique:tbl_state,code',
//                    'name' => 'required|unique:tbl_state,name'
//        ]);
//        if ($validator->fails()) {
//            $resVal['success'] = FALSE;
//            $resVal['message'] = 'Error in request format';
//            if (array_key_exists('code', $validator->failed())) {
//                $resVal['message'] = 'State Code is already exists';
//            }
//            if (array_key_exists('name', $validator->failed())) {
//                $resVal['message'] = 'State Name is already exists';
//            }
//            return $resVal;
//        }
        
        if ($request->input('code') != NULL) {
            $st = State::where('code', "=", $request->input('code'))->where('is_active', '=', 1)->get();

            if (count($st) > 0) {
                $resVal['success'] = FALSE;
                $resVal['message'] = 'State Code Is Already Exists';
                return $resVal;
            }
        }
        if ($request->input('name') != NULL) {
            $st = State::where('name', "=", $request->input('name'))->where('is_active', '=', 1)->get();

            if (count($st) > 0) {
                $resVal['success'] = FALSE;
                $resVal['message'] = 'State Name Is Already Exists';
                return $resVal;
            }
        }
        $currentuser = Auth::user();
        $state = new State;
        $state->created_by = $currentuser->id;
        $state->updated_by = $currentuser->id;
        $state->fill($request->all());
        $state->save();
        if ($state->is_default == 1) {
            $st = DB::table('tbl_state')->where('id', '!=', $state->id)->update(['is_default' => 0]);
        }
        $resVal['id'] = $state->id;

        LogHelper::info1('State Save ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'State', 'save', $screen_code, $state->id);

        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $id = $request->input('id');
        $name = $request->input('name');
        $country_name = $request->input('country_name');
        $country_id = $request->input('country_id');
        $is_active  =$request->input('is_active',1);
        $builder = DB::table('tbl_state')
                // ->leftJoin('tbl_state as s', 'c.id', '=', 's.country_id')
                ->select('*');

        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if (!empty($name)) {
            $builder->where('name', 'like', '%' . $name . '%');
        }
        if (!empty($country_name)) {
            $builder->where('country_name', '=', $country_name);
        }

        if (!empty($country_id)) {
            $builder->where('country_id', '=', $country_id);
        }
        if (($is_active) != '') {
              $builder->where('is_active', '=', $is_active);
         }
        $builder->orderBy('name', 'asc');

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $stateCollection = $builder->get();
        } else {

            $stateCollection = $builder->skip($start)->take($limit)->get();
        }
        $resVal['list'] = $stateCollection;

        LogHelper::info1('State List All ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));

        return ($resVal);
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'State Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        
        $app=DB::table('tbl_app_config')->select('value')->where('setting','=','gst_state')->first();
        
        if($app != '' && $app->value == $id){
           $resVal['message'] = 'State Is Preserved';
           $resVal['success'] = FALSE; 
           return $resVal;
        }
        
        $currentuser = Auth::user();

        try {
            $state = State::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'State Not Found';
            return $resVal;
        }
        
        $state->is_active=0;
        $state->updated_by = $currentuser->id;
        $state->save();
       

        LogHelper::info1('State Delete ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'State', 'delete', $screen_code, $state->id);

        return $resVal;
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'State Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        if ($request->input('code') != NULL) {
            $st = State::where('code', "=", $request->input('code'))->where('is_active', '=', 1)->where('id', '!=', $id)->get();

            if (count($st) > 0) {
                $resVal['success'] = FALSE;
                $resVal['message'] = 'State Code Is Already Exists';
                return $resVal;
            }
        }
        if ($request->input('name') != NULL) {
            $st = State::where('name', "=", $request->input('name'))->where('is_active', '=', 1)->where('id', '!=', $id)->get();

            if (count($st) > 0) {
                $resVal['success'] = FALSE;
                $resVal['message'] = 'State Name Is Already Exists';
                return $resVal;
            }
        }
        try {
            $state = State::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'State Not Found';
            return $resVal;
        }
        
        $state->updated_by = $currentuser->id;
        $state->fill($request->all());
        $state->save();
        if ($state->is_default == 1) {
            $st = DB::table('tbl_state')->where('id', '!=', $state->id)->update(['is_default' => 0]);
        }

        LogHelper::info1('State Update ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'State', 'update', $screen_code, $state->id);

        return $resVal;
    }

}

?>
