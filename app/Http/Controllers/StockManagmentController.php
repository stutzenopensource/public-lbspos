<?php

//

namespace App\Http\Controllers;

use DateTime;
use DateInterval;
use DatePeriod;
use DB;
use App\ActivityLog;
use App\PurchasePayment;
use Carbon\Carbon;
use App\Invoice;
use App\Invoiceitem;
use App\Inventory;
use App\InventoryAdjustment;
use App\Attributevalue;
use App\Attribute;
use app\Customer;
use Illuminate\Http\Request;
use App\Transformer\InvoiceTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\GeneralHelper;
use App\Helper\LogHelper;
use App\Helper\NotificationHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of invoicesController
 *
 * @author Deepa
 */
class StockManagmentController {

    //put your code here
    public function StockAdjustment(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
//        $id=GeneralHelper::StockAdjustmentSave($request, 'adjust', '','');
        LogHelper::info1('Stock Adjustment Save ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Stock Adjustment', 'save', $screen_code, 0);
        return $resVal;
    }

    public function StockWastage(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
//        $id=GeneralHelper::StockWastageSave($request, 'adjust', '','');

        LogHelper::info1('Stock Wastage Save ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Stock Wastage', 'save', $screen_code, 0);

        return $resVal;
    }

    public function InventoryAdjustment_list(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        $decimalFormat= GeneralHelper::decimalFormat();

        $id = $request->input('id');
        $product_id = $request->input('product_id');
        $type = $request->input('type');
        $fromDate = $request->input('from_date', '');
        $toDate = $request->input('to_date', '');
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        $barcode=$request->input('barcode');

        $builder = DB::table('tbl_inventory_adjustment as ia')
                ->leftJoin('tbl_product as p', 'p.id', '=', 'ia.product_id')
                ->leftjoin('tbl_bcn as b','b.id','=','ia.bcn_id')
                ->where('p.is_active', '=', 1)
                ->where('ia.is_active','=',1)
                ->where('p.has_inventory', '=', 1)
                ->select('ia.*',DB::raw("cast(ia.quantity_debit as DECIMAL(12,".$decimalFormat.")) as quantity_debit")
                        ,DB::raw("cast(ia.quantity_credit as DECIMAL(12,".$decimalFormat.")) as quantity_credit"),DB::raw("cast(ia.total_stock as DECIMAL(12,".$decimalFormat.")) as total_stock"), 'p.name')
                ->orderBy('ia.id', 'asc');
        
        $credit_qty = DB::table('tbl_inventory_adjustment as ia')
                ->leftjoin('tbl_product as p', 'p.id', '=', 'ia.product_id')
                ->leftjoin('tbl_bcn as b', 'b.id', '=', 'ia.bcn_id')
                ->select(DB::raw("Coalesce(SUM(cast(ia.quantity_credit as DECIMAL(12," . $decimalFormat . "))),0)as creditqty"))
                ->where('p.is_active', '=', 1)
                ->where('ia.is_active', '=', 1)
                ->where('p.has_inventory', '=', 1)
                ->where('date', '<', $fromDate);

        if (!empty($product_id)) {
            $credit_qty->where('ia.product_id', '=', $product_id);
        }
        $creditQty = $credit_qty->first();

        $debit_qty = DB::table('tbl_inventory_adjustment as ia')
                ->leftjoin('tbl_product as p', 'p.id', '=', 'ia.product_id')
                ->leftjoin('tbl_bcn as b', 'b.id', '=', 'ia.bcn_id')
                ->select(DB::raw("Coalesce(SUM(cast(ia.quantity_debit as DECIMAL(12," . $decimalFormat . "))),0)as debitqty"))
                ->where('p.is_active', '=', 1)
                ->where('ia.is_active', '=', 1)
                ->where('p.has_inventory', '=', 1)
                ->where('date', '<', $fromDate);

        if (!empty($product_id)) {
            $debit_qty->where('ia.product_id', '=', $product_id);
        }

        $debitQty = $debit_qty->first();
        
        $qty_list = array();
        $qty_list['date'] = date('d-m-Y', strtotime($fromDate));
        $qty_list['description'] = 'Opening Stock';
        $qty_list['credit_qty'] = $creditQty->creditqty;
        $qty_list['debit_qty'] = $debitQty->debitqty;
        $resVal['OpeningStock'] = $qty_list;

        if (!empty($id)) {
            $builder->where('ia.id', '=', $id);
        }
        if (!empty($barcode)) {
            $builder->where('ia.barcode','like','%'.$barcode .'%');
        }
        if (!empty($type)) {
            if ($type == 1)
                $builder->where('ia.type', '=', 'stock_adjust');
            else if ($type == 2)
                $builder->where('ia.type', '=', 'stock_wastage');
            else if ($type == 3)
                $builder->where('ia.type', '=', 'invoice_create');
            else if ($type == 4)
                $builder->where('ia.type', '=', 'invoice_update');
            else if ($type == 5)
                $builder->where('ia.type', '=', 'invoice_delete');
        }
        if (!empty($product_id)) {

            $builder->where('ia.product_id', '=', $product_id);
        }
        if (!empty($toDate)) {

            $builder->whereDate('ia.date', '<=', $toDate);
        }
        if (!empty($fromDate)) {
            $builder->whereDate('ia.date', '>=', $fromDate);
        }

        if ($start == 0 && $limit == 0) {
            $inventoryCollection = $builder->get();
        } else {

            $inventoryCollection = $builder->skip($start)->take($limit)->get();
        }

        $resVal['list'] = $inventoryCollection;

        LogHelper::info1('InventoryAdjustment_list ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }

    public function listAll(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $id = $request->input('id');
        $name = $request->input('product_name');
        $type = $request->input('type', '');
        $mode=$request->input('mode');
        $category_id=$request->input('category_id');
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 0);
        $show_all = $request->input('show_all', '');
        $has_inventory=$request->input('has_inventory',1);
        $cat_array = array();


        if ($show_all == '') {
            $type1 = GeneralHelper::checkConfiqSetting();
            if ($type1 == 'product based') {

                $builder = DB::table('tbl_product as p')
                        ->leftJoin('tbl_inventory as i', 'p.id', '=', 'i.product_id')
                        ->leftJoin('tbl_bcn as bcn', 'p.id', '=', 'bcn.ref_id')
                        ->leftJoin('tbl_category as c', 'p.category_id', '=', 'c.id')
                        ->where('p.is_active', '=', 1)
                        ->where('bcn.is_active', '=', 1)
                        // ->where('p.has_inventory', '=', 1)
                        ->select(DB::raw('p.*,i.quantity, c.name as category_name,bcn.id as bcn_id,bcn.code as barcode'))
                        ->orderBy('c.name', 'asc')
                        ->orderBy('p.name', 'asc');
            } else {
                $builder = DB::table('tbl_product as p')
                        ->leftJoin('tbl_inventory as i', 'p.id', '=', 'i.product_id')
                        ->leftJoin('tbl_category as c', 'p.category_id', '=', 'c.id')
                        ->where('p.is_active', '=', 1)
                        // ->where('p.has_inventory', '=', 1)
                        ->select(DB::raw('p.*,i.quantity, c.name as category_name'))
                        ->orderBy('c.name', 'asc')
                        ->orderBy('p.name', 'asc');
            }
             if ($mode == 1) {
                $builder->whereColumn('p.min_stock_qty', '<', 'i.quantity');
            } elseif ($mode == 2) {
                $builder->whereColumn('p.min_stock_qty', '>=', 'i.quantity');
            }
            if ($has_inventory != '') {
              $builder->where('p.has_inventory','=',$has_inventory);
            }
        } else {
            $type1 = GeneralHelper::checkConfiqSetting();
            if ($type1 == 'product based') {
                $builder = DB::table('tbl_product as p')
                        ->leftJoin('tbl_category as c', 'p.category_id', '=', 'c.id')
                        ->leftJoin('tbl_bcn as bcn', 'p.id', '=', 'bcn.ref_id')
                        ->leftJoin('tbl_inventory as i', 'p.id', '=', 'i.product_id')
                        ->where('p.is_active', '=', 1)
			->where('bcn.is_active', '=', 1)
                        // ->where('p.has_inventory', '=', 1)
                        ->select(DB::raw("coalesce(c.id, 0) as cat_id, coalesce(c.name, '') as  name, coalesce(SUM(i.quantity),0)  as quantity,bcn.id as bcn_id,bcn.code as barcode,coalesce(SUM(p.min_stock_qty),0)  as min_stock_qty"))
                        ->orderBy('c.name', 'asc')
                        ->groupBy('c.id');
            } else {
                $builder = DB::table('tbl_product as p')
                        ->leftJoin('tbl_category as c', 'p.category_id', '=', 'c.id')
                        ->leftJoin('tbl_inventory as i', 'p.id', '=', 'i.product_id')
                        ->where('p.is_active', '=', 1)
//                         ->where('p.has_inventory', '=', 1)
                        ->select(DB::raw("coalesce(c.id, 0) as cat_id, coalesce(c.name, '') as  name, coalesce(SUM(i.quantity),0)  as quantity,coalesce(SUM(p.min_stock_qty),0)  as min_stock_qty"))
                        ->orderBy('c.name', 'asc')
                        ->groupBy('c.id');
            }
            if ($mode == 1) {
                $builder->havingRaw(DB::raw("(coalesce(SUM(p.min_stock_qty),0) < coalesce(SUM(i.quantity),0))"));
            } elseif ($mode == 2) {
                $builder->havingRaw(DB::raw("(coalesce(SUM(p.min_stock_qty),0) >= coalesce(SUM(i.quantity),0))"));
            }
          if ($has_inventory != '') {
              $builder->where('p.has_inventory','=',$has_inventory);
            }
        }
        if (!empty($id)) {
            $builder->where('p.id', '=', $id);
        }
        if (!empty($name)) {
            $builder->where('p.name', 'like','%'.$name .'%');
        }

        if(!empty($category_id)){
            $builder->where('c.id','=',$category_id);
        }
        
        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $cat_array = $builder->get();
        } else {

            $cat_array = $builder->skip($start)->take($limit)->get();
        }

        if ($show_all == '') {
            if ($type == 1) {
                if ($type1 == 'product based') {

                    foreach ($cat_array as $cat) {
                        $cat_id = $cat->id;
                        $builder = DB::table('tbl_purchase_invoice_item as pin')
                                ->leftjoin('tbl_product as p', 'pin.product_id', '=', 'p.id')
                                ->select('pin.*')
                                ->where('pin.product_id', '=', $cat_id)
                                ->where('p.is_active', '=', 1)
                                ->orderBy('p.name', 'asc');
                        $cat->data = $builder->get();
                    }
                } ELSE {

                  foreach ($cat_array as $cat) {
                        $cat_id = $cat->id;
                        $builder = DB::table('tbl_purchase_invoice_item as pin')
                                ->leftjoin('tbl_product as p', 'pin.product_id', '=', 'p.id')
//                            ->leftjoin('tbl_bcn as b', 'b.ref_id', '=', 'pin.id')
                                ->leftJoin('tbl_bcn as b', function($join) {
                                    $join->on('b.ref_id', '=', 'pin.id');
                                    $join->on('b.ref_type', '=', DB::raw("'purchase based'"));
                                    $join->where('b.is_active','=',1);
                                })
                                ->select('pin.*', 'b.id as bcnId', 'b.code as bcnBarCode', 'b.code as bcnBarCode', 'b.selling_price as bcn_selling_price','b.qty_in_hand')
                                ->where('pin.product_id', '=', $cat_id)
                                ->where('p.is_active', '=', 1)
                                ->where('pin.is_active', '=', 1)
                                ->orderBy('p.name', 'asc');
                        $cat->data = $builder->get();
                    }
                }
            }
        }
        if ($show_all == 1) {
            foreach ($cat_array as $cat) {
                $cat_id = $cat->cat_id;
                $builder = DB::table('tbl_inventory as i')
                        ->leftJoin('tbl_product as p', 'p.id', '=', 'i.product_id')
                        ->select(DB::raw('p.*,i.quantity as qty'))
                        ->where('p.category_id', '=', $cat_id)
                        ->where('p.is_active', '=', 1)
                         ->where('p.has_inventory', '=', 1)
                        ->orderBy('p.name', 'asc');
                
               if ($has_inventory != '') {
              $builder->where('p.has_inventory','=',$has_inventory);
                }
                $cat->data = $builder->get();
            }
        }
        $resVal['list'] = $cat_array;
        LogHelper::info1('Stock List All ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return $resVal;
    }

    public function MinStocklist(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $id = $request->input('id');

        $builder = DB::table('tbl_product as p')
                ->leftJoin('tbl_inventory as i', 'p.id', '=', 'i.product_id')
                ->whereColumn('i.quantity', '<=', 'p.min_stock_qty')
                ->where('p.is_active', '=', 1)
                // ->where('p.has_inventory', '=', 1)
                ->select(DB::raw('p.*,i.quantity'));

        $id    = $request->input('id');
        $name  = $request->input('product_name');
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        $mode  =$request->input('mode','');
        if (!empty($id)) {
            $builder->where('p.id', '=', $id);
        }
        if($mode != ''){
        if ($mode==0) {
          $builder->where('p.has_inventory','=',0);
        }else{
          $builder->where('p.has_inventory','=',1);
        }
        }
        $resVal['total'] = $builder->count();

        if ($start == 0 && $limit == 0) {
            $productCollection = $builder->get();
        } else {

            $productCollection = $builder->skip($start)->take($limit)->get();
        }
        $resVal['list'] = $productCollection;
        LogHelper::info1('MinStock List ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return $resVal;
    }

}

?>
