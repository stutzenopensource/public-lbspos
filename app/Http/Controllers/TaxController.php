<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\Tax;
use Carbon\Carbon;
use Illuminate\Http\Request;
//use App\Transformer\TaxTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\GeneralHelper;
use App\Helper\LogHelper;
use App\Helper\NotificationHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TaxController
 *
 * @author Deepa
 */
class TaxController extends Controller {

    //put your code here
    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Tax Added Successfully';
        $resVal['success'] = TRUE;
        $currentuser = Auth::user();
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $tax_name=$request->input('tax_name','');
        $tax_no=$request->input('tax_no','');
        $tax_percentage=$request->input('tax_percentage','');       
        if(!empty($tax_name) && !empty($tax_no) && !empty($tax_percentage)){            
            $tax1=DB::table('tbl_tax')->whereRaw(DB::raw("(tax_name='".$tax_name."' and tax_no='".$tax_no."'and tax_percentage='".$tax_percentage."')"))->where('is_active','=',1)->get();
           if(count($tax1)> 0){               
              $resVal['success']=False;
              $resVal['message']='Tax Already Exists';
              return $resVal;  
            }         
        }
        $tax = new Tax;


        $tax->created_by = $currentuser->id;
        $tax->updated_by = $currentuser->id;
        $tax->is_active = $request->input('is_active', 1);
        $tax->fill($request->all());
        $tax->save();

        $resVal['id'] = $tax->id;
        LogHelper::info1('Tax Save ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Tax', 'save', $screen_code, $tax->id);
        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $is_group = $request->input('is_group', '');

        $name = $request->input('tax_name', '');
        $tax_percentage = $request->input('tax_percentage', '');
        $is_display = $request->input('is_display', '');
        $is_active = $request->input('is_active', 1);
        $hsn_flag = $request->input('hsn_flag', '');


        $builder = DB::table('tbl_tax')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if ($is_active != '') {
            $builder->where('is_active', '=', $is_active);
        }
        if (!empty($tax_percentage)) {
            $builder->where('tax_percentage', '=', $tax_percentage);
        }

        if (!empty($name)) {
            $builder->where('tax_name', 'like', '%' . $name . '%');
        }
        if ($is_group != null) {
            $builder->where('is_group', '=', $is_group);
        }

        if ($is_display != '') {
            $builder->where('is_display', '=', $is_display);
        }
        if ($hsn_flag != null) {
            $builder->where('hsn_flag', '=', $hsn_flag);
        }

        $builder->orderBy('id', 'desc');
        $resVal['total'] = $builder->count();

        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {

            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }
       LogHelper::info1('Tax List All ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return $resVal;
    }

    public function expiredListAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $current_date = Carbon::now();
        $current_date = $current_date->format('Y-m-d');

        $id = $request->input('id');

        $name = $request->input('tax_name', '');
        $is_group = $request->input('is_group', '');
        $is_active = $request->input('is_active', '');
        $date = $request->input('date', '');
        $is_display = $request->input('is_display', '');
        $tax_no = $request->input('tax_no', '');
        $tax_percentage=$request->input('tax_percentage','');
        
       
            $builder = DB::table('tbl_tax')
                    ->select('*');
            
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if ($is_active != '') {
            $builder->where('is_active', '=', $is_active);
        }
        if (!empty($tax_no)) {
            $builder->where('tax_no', '=', $tax_no);
        }
        if (!empty($tax_percentage)) {
            $builder->where('tax_percentage', '=', $tax_percentage);
        }
        if ($is_display != '') {
            $builder->where('is_display', '=', $is_display);
        }
        
        if($is_group != ''){
            $builder->where('is_group', '=', $is_group);
        }

        if (!empty($date)) {

            $builder->whereRaw(DB::raw("(start_date <= '$date' or  `start_date` is null) and (end_date >= '$date' or end_date is null) "));
        } else {

            $builder->whereRaw(DB::raw("(start_date <= '$current_date' or  `start_date` is null) and (end_date >= '$current_date' or end_date is null) "));
        }

        $builder->orderBy('id', 'desc');
        $resVal['total'] = $builder->count();


        DB::connection()->enableQueryLog();



        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {

            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }
        $queries = DB::getQueryLog();
        $last_query = end($queries);

        // echo 'Query<pre>';
        //print_r($last_query);
       LogHelper::info1('Tax Expired List All ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return $resVal;
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Tax Deleted Successfully';
        $currentuser = Auth::user();
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
         $builder=DB::table('tbl_invoice as i')
                ->leftjoin('tbl_invoice_item as it','it.invoice_id','=','i.id')
                ->where('i.is_active','=',1)
                ->leftjoin('tbl_invoice_item_tax as t','i.id','=','t.invoice_id')
                ->whereRaw(DB::raw("(i.tax_id in (".$id.") or t.tax_id in (".$id.") or it.tax_id in (".$id.") )"))
                ->get();
        
         $builder1=DB::table('tbl_purchase_invoice as i')
                 ->leftjoin('tbl_purchase_invoice_item as pt','pt.purchase_invoice_id','=','i.id')
                 ->where('i.is_active','=',1)
                ->leftjoin('tbl_purchase_invoice_tax as t','i.id','=','t.invoice_id')
                ->whereRaw(DB::raw("(i.tax_id in (".$id.") or t.tax_id in (".$id.")  or pt.tax_id in (".$id.") or pt.selling_tax_id in (".$id.") )"))
                ->get();
         
        if(count($builder)>0 || count($builder1)>0){
            $resVal['message']="Transaction Done With This Tax Can't Be Deleted";
            $resVal['success']=False;
            return $resVal;
        }
        try {
            $tax = Tax::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Tax Not Found';
            return $resVal;
        }
        
        if ($tax->is_group == 1) {
            $product = DB::table('tbl_product')->select('id')
                            ->where('is_active', 1)
                            ->whereIn('hsn_code', function($query) use ($id) {
                                $query->select('hsn_code')->from('tbl_hsn_tax_mapping')->where('group_tax_id', $id)->where('is_active', 1);
                            })->get();
        }

        if ($tax->is_group == 0) {
            $product = DB::table('tbl_tax')->where('is_active', 1)->where('is_group', 1)->select('id')
                            ->Whereraw("FIND_IN_SET('" . $id . "',group_tax_ids) OR FIND_IN_SET('" . $id . "',intra_group_tax_ids)")->get();
        }

        if (count($product) > 0) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Unable To Delete. Tax Mapped To Product';
            return $resVal;
        }

        $tax->created_by = $currentuser->id;
        $tax->is_active = 0;
        $tax->save();
        DB::table('tbl_tax_mapping')->where('tax_id', '=', $id)->update(['is_active' => 0]);
        
        LogHelper::info1('Tax Delete ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Tax', 'delete', $screen_code, $tax->id);

        return $resVal;
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Tax Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        $tax_name=$request->input('tax_name','');
        $tax_no=$request->input('tax_no','');
        $tax_percentage=$request->input('tax_percentage','');       
        if(!empty($tax_name) && !empty($tax_no) && !empty($tax_percentage)){            
           $tax1=DB::table('tbl_tax')->whereRaw(DB::raw("(tax_name='".$tax_name."' and tax_no='".$tax_no."'and tax_percentage='".$tax_percentage."')"))->where('is_active','=',1)->where('id','!=',$id)->get();
           if(count($tax1)>0){               
              $resVal['success']=False;
              $resVal['message']='Tax Already Exists';
              return $resVal;  
            }         
        }
        
        try {
            $tax = Tax::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Tax Not Found';
            return $resVal;
        }
        
        if($tax->is_group==0){
            $groupIds=GeneralHelper::getGroupTaxId($id);
            if(!empty($groupIds))
                $id=$groupIds;
        }
        
        $builder=DB::table('tbl_invoice as i')
                ->leftjoin('tbl_invoice_item as it','it.invoice_id','=','i.id')
                ->where('i.is_active','=',1)
                ->leftjoin('tbl_invoice_item_tax as t','i.id','=','t.invoice_id')
                ->whereRaw(DB::raw("(i.tax_id in (".$id.") or t.tax_id in (".$id.") or it.tax_id in (".$id.") )"))
                ->get();
        
         $builder1=DB::table('tbl_purchase_invoice as i')
                 ->leftjoin('tbl_purchase_invoice_item as pt','pt.purchase_invoice_id','=','i.id')
                 ->where('i.is_active','=',1)
                ->leftjoin('tbl_purchase_invoice_tax as t','i.id','=','t.invoice_id')
                ->whereRaw(DB::raw("(i.tax_id in (".$id.") or t.tax_id in (".$id.")  or pt.tax_id in (".$id.") or pt.selling_tax_id in (".$id.") )"))
                ->get();
             
        if(count($builder)>0 || count($builder1)>0){
            $resVal['message']="Transaction Done With This Tax Can't Be Updated";
            $resVal['success']=False;
            return $resVal;
        }
        
        $tax->created_by = $currentuser->id;
        $tax->fill($request->all());
        $tax->save();
        
        LogHelper::info1('Tax Update ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Tax', 'update', $screen_code, $tax->id);
        return $resVal;
    }

    public function calculate(Request $request) {

        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Tax Value Calculated Successfully';

        $validator = Validator::make($request->all(), [
                    'amount' => 'required'
        ]);
        if ($validator->fails()) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Error In Request Format';
            return $resVal;
        }

        $tax_id = $request->input('tax_id', '0');
        $amount = $request->input('amount');
        $invoice_id = $request->input('invoice_id', '');
        $invoice_type = $request->input('invoice_type', '');

        $tax_collection = DB::table('tbl_tax')->select('*')
                        ->where('id', '=', $tax_id)
                        ->where('is_active', '=', 1)->get();
        $total_tax_amount = 0;


        $tax_list = GeneralHelper::calculatedInvoiceTax($invoice_id, $tax_id, $amount, $invoice_type);


        foreach ($tax_list as $tax) {

            $total_tax_amount += $tax->tax_amount;
        }

        $resVal['tax_amount'] = $total_tax_amount;
        $resVal['list'] = $tax_list;
        LogHelper::info1('Tax Calculate ' . $request->fullurl(), $request->all());
        return ($resVal);
    }

    public function groupDetail(Request $request) {

        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Tax Group Detail Success';

        $validator = Validator::make($request->all(), [
                    'id' => 'required',
        ]);
        if ($validator->fails()) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Error In Request Format';
            return $resVal;
        }

        $tax_id = $request->input('id');

        $tax_collection = DB::table('tbl_tax')->select('*')
                        ->where('id', '=', $tax_id)
                        ->where('is_group', '=', 1)
                        ->where('is_active', '=', 1)->get();
        $tax_list = array();
        $intra_tax_list = array();

        if (count($tax_collection) > 0) {

            $tax = $tax_collection->first();
            $tax->tax_list = array();
            $tax->intra_tax_list = array();
            if (!empty($tax->is_group)) {
                $associate_tax_id = explode(',', $tax->group_tax_ids);
                $intra_associate_tax_id = explode(',', $tax->intra_group_tax_ids);

                $associate_tax_collection = DB::table('tbl_tax')->select('*')
                                ->where('is_active', '=', 1)
                                ->whereIn('id', $associate_tax_id)->get();
                
                $intra_associate_tax_collection = DB::table('tbl_tax')->select('*')
                                ->where('is_active', '=', 1)
                                ->whereIn('id', $intra_associate_tax_id)->get();

                //sorting the tax in creation order
                foreach ($intra_associate_tax_id as $asso_tax_ids) {

                    foreach ($intra_associate_tax_collection as $intra_associate_tax) {
                        if ($asso_tax_ids == $intra_associate_tax->id) {
                            array_push($intra_tax_list, $intra_associate_tax);
                            break;
                        }
                    }
                }
                $tax->intra_tax_list = $intra_tax_list;
                
                 //sorting the tax in creation order
                foreach ($associate_tax_id as $asso_tax_id) {

                    foreach ($associate_tax_collection as $associate_tax) {
                        if ($asso_tax_id == $associate_tax->id) {
                            array_push($tax_list, $associate_tax);
                            break;
                        }
                    }
                }
                $tax->tax_list = $tax_list;
            }
            $resVal['data'] = $tax;
        } else {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Is Not Valid Taxgroup';
            $resVal['data'] = null;
        }
         LogHelper::info1('Tax Group Detail ' . $request->fullurl(), json_decode(json_encode($resVal['data']), true));
        return ($resVal);
    }
    
}

?>
