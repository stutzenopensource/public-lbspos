<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\TaxMapping;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helper\AuthorizationHelper;
use App\Helper\LogHelper;
use App\Helper\NotificationHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TaxController
 *
 * @author Stutzen Admin
 */
class TaxMappingController extends Controller {


    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Tax Mapping Saved Successfully';
        $resVal['success'] = TRUE;
        $currentuser = Auth::user();
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $taxMappings = $request->all();
        foreach ($taxMappings as $tm) {
            $validator = Validator::make($tm, [
                    'tax_id' => 'required',
                    'product_id' => 'required',
        ]);
        if ($validator->fails()) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Error In Request Format';
            return $resVal;
        }
          else{  
        $taxMapping = new TaxMapping;
        $taxMapping->fill($tm);
        $taxMapping->created_by = $currentuser->id;
        $taxMapping->updated_by = $currentuser->id;
        $taxMapping->is_active = $request->input('is_active', 1);
        $taxMapping->save();
          }
        }
        LogHelper::info1('Tax Mapping Save ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Tax Mapping', 'save', $screen_code, $taxMapping->id);
        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $tax_id = $request->input('tax_id', '');
        $product_id = $request->input('product_id', '');
        $is_active = $request->input('is_active');
        $builder = DB::table('tbl_tax_mapping')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if ($is_active != '') {
            $builder->where('is_active', '=', $is_active);
        }
        if (!empty($tax_id)) {
            $builder->where('tax_id', '=', $tax_id);
        }
        if (!empty($product_id)) {
            $builder->where('product_id', '=', $product_id);
        }
        $builder->orderBy('id', 'desc');
        $resVal['total'] = $builder->count();

        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {

            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }
        LogHelper::info1('Tax Mapping List All ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return $resVal;
    }


    public function delete(Request $request) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Tax Mapping Deleted Successfully';
        $currentuser = Auth::user();
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
         $taxMappings = $request->all();
        foreach ($taxMappings as $aa) {
           if ($aa['id'] != NULL) {
                $collection = TaxMapping::where('id', "=", $aa['id']) ->get();
                $taxMapping = $collection->first();
                 $taxMapping->updated_by = $currentuser->id;
                $taxMapping->is_active = 0;
                $taxMapping->save();
            }
        }
        LogHelper::info1('Tax Mapping Delete ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Tax Mapping', 'delete', $screen_code, $taxMapping->id);
        return $resVal;
    }

}

?>
