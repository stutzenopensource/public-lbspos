<?php

namespace App\Http\Controllers;

use DB;
use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\PurchasePayment;
use App\Helper\AccountHelper;
use App\Helper\AuthorizationHelper;
use Carbon\Carbon;
use App\Helper\ActivityLogHelper;
use App\Helper\LogHelper;
use App\Helper\NotificationHelper;
use App\Helper\TransactionHelper;
use App\Helper\GeneralHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TransactionController
 *
 * @author Deepa
 */
class TransactionController extends Controller {

//put your code here

    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Transaction Added Successfully';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

 
        $mydate = $request->transaction_date;

 
        $id = $request->input('account_id', '');
        
        if ($request->input('credit', 0) == 0 && $request->input('debit', 0) == 0) {
            $resVal['message'] = 'Please Give The Amount Above Zero';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        if (!empty($request->input('credit', 0))) {
            $amount = $request->input('credit');
            $account = AccountHelper::deposit($id, $amount);
            $voucher_type = Transaction::$DIRECT_INCOME;
            $credit = $request->input('credit');
            $debit = 0.00;
        }
        if (!empty($request->input('debit', 0))) {
            $amount = $request->input('debit');
            $account = AccountHelper::withDraw($id, $amount);

            $debit = $request->input('debit');
            $credit = 0.00;
            $voucher_type = Transaction::$DIRECT_EXPENSE;
        }
        
        if( $voucher_type == Transaction::$DIRECT_INCOME)
                $resVal['message']='Deposit Added Successfully';
        if( $voucher_type == Transaction::$DIRECT_EXPENSE)
                 $resVal['message']='Expense Added Successfully';
    
 

        $customer_id = GeneralHelper::getCustomerByAcode($request->input('acode'));

        $transactionData = array(
            "name" => $currentuser->f_name,
            "transaction_date" => $mydate,
            "voucher_type" => $voucher_type,
            "voucher_number" => 0,
            "credit" => $credit,
            "debit" => $debit,
            "pmtcode" => GeneralHelper::getPaymentAcode($id),
            "pmtcoderef" => $id,
            "is_cash_flow" => 1, //have to check  account in save and voucher type too
            "account" => $request->input('account', ''),
            "account_id" => $request->input('account_id', ''),
            "paid_to_customer_id" => $request->input('paid_to_customer_id', ''),
            "account_category" => $request->input('account_category', ''),
            "payment_mode" => $request->input('payment_mode', ''),
            "acode" => $request->input('acode'),
            "acoderef" => $customer_id,
            "particulars" => $request->input('particulars', ''),
            "created_by" => $currentuser->id,
            "updated_by" => $currentuser->id,
            "created_by_name" => $currentuser->f_name . ' ' . $currentuser->l_name,
            "updated_by_name" => $currentuser->f_name . ' ' . $currentuser->l_name,
            "is_active" => 1,
        );
        $transaction = TransactionHelper::transactionSave($transactionData);

//         
        $resVal['id'] = $transaction->id;

        LogHelper::info1('Transaction Save ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Transaction', 'save', $screen_code, $transaction->id);


        return $resVal;
    }

    public function accountstatement_csvexport(Request $request) {
        $accountstmt = $this->accountStatement($request);
        $bal = 0;
        $openblnc = $accountstmt['openBalance'];
        $output = fopen('accountstatement.csv', 'w');
        fputcsv($output, array('Transaction Date', 'Narration', 'Debit', 'Credit', 'Balance',));
        $bal = $openblnc['credit'] - $openblnc['debit'];
       $bal= number_format((float) $bal, 2, '.', '');
        fputcsv($output, array('', $openblnc['description  '], $openblnc['debit'], $openblnc['credit'], $bal));
 $date_format=GeneralHelper::getDateFormat();
        foreach ($accountstmt['list'] as $values) {
            $bal = $bal + $values->credit;
            $bal = $bal - $values->debit;
            $bal= number_format((float) $bal, 2, '.', '');
            $date=GeneralHelper::CsvDate( $values->transaction_date, $date_format);
            fputcsv($output, array($date, $values->particulars, $values->debit, $values->credit, $bal));
        }
        LogHelper::info1('AccountStatement CSV Download ' . $request->fullurl(), $request->all());
        return response()->download(getcwd() . "/accountstatement.csv");
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $name = $request->input('name', '');


//$status = $request->input('status', '');
        $builder = DB::table('tbl_transaction')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }


        if (!empty($name)) {
            $builder->where('name', 'like', '%' . $name . '%');
        }
        $builder->orderBy('id', 'desc');

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {

            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }
        LogHelper::info1('Transaction List All ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }

    public function listAllByDate(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $cus_id = $request->input('cus_id');
        $acc_id = $request->input('acc_id');
        $created_by = $request->input('created_by');
        $acc_cat = $request->input('acc_cat');
        $name = $request->input('created_by_name', '');
        $fromDate = $request->input('from_date', '');
        $toDate = $request->input('to_date', '');

        $builder = DB::table('tbl_transaction')
                ->select('*');

        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($cus_id)) {
            $builder->where('customer_id', '=', $cus_id);
        }
        if (!empty($acc_id)) {
            $builder->where('account_id', '=', $acc_id);
        }
        if (!empty($created_by)) {
            $builder->where('created_by', '=', $created_by);
        }
        if (!empty($acc_cat)) {
            $builder->where('account_category', '=', $acc_cat);
        }
        if (!empty($fromDate)) {

            $builder->whereDate('transaction_date', '>=', $fromDate);
        }
        if (!empty($toDate)) {

            $builder->whereDate('transaction_date', '<=', $toDate);
        }
        if (!empty($name)) {
            $builder->where('created_by_name', 'like', '%' . $name . '%');
        }
        $builder->orderBy('id', 'desc');

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {

            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }
        LogHelper::info1('Transaction List All By Date ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Transaction Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $transaction = Transaction::findOrFail($id);
            if ($transaction->is_active == 0) {
                return $resVal;
            }
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Transaction Not Found';
            return $resVal;
        }

        $transaction->updated_by = $currentuser->id;
        $transaction->is_active = 0;
        $transaction->save();
        LogHelper::info1('Transaction Delete ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Transaction', 'delete', $screen_code, $transaction->id);

        return $resVal;
    }

    public function listAllByIncome(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        $id = $request->input('id');
        $cus_id = $request->input('customer_id', '');

        $acc_id = $request->input('account_id', '');

        $fromDate = $request->input('from_Date', '');

        $toDate = $request->input('to_Date', '');
        $is_active = $request->input('is_active', 1);
        $acc_cat = $request->input('account_category', '');
        $created_by = $request->input('created_by');
        $name = $request->input('created_by_name', '');

        $builder = DB::table('tbl_transaction')
                ->select('id', 'name', 'fiscal_year_id', 'fiscal_year_code', 'transaction_date', 'voucher_type', 'voucher_number', 'credit As amount', 'customer_id', 'account', 'account_id', 'particulars', 'created_by', 'updated_by', 'updated_at', 'created_at', 'created_by_name', 'updated_by_name', 'account_category', 'payment_mode', 'fiscal_month_code', 'is_active')
                ->where('voucher_type', '!=', 'transfer')
                ->where('voucher_type', '!=', 'inital_balance')
                ->where('is_cash_flow', 1)
                ->where('credit', '!=', 0);
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if (!empty($cus_id)) {
            $builder->where('customer_id', '=', $cus_id);
        }
        if (!empty($acc_id)) {
            $builder->where('account_id', '=', $acc_id);
        }
        if (!empty($acc_cat)) {
            $builder->where('account_category', '=', $acc_cat);
        }
        if (!empty($is_active)) {
            $builder->where('is_active', '=', $is_active);
        }
        if (!empty($created_by)) {
            $builder->where('created_by', '=', $created_by);
        }
        if (!empty($fromDate)) {

            $builder->whereDate('transaction_date', '>=', $fromDate);
        }
        if (!empty($toDate)) {

            $builder->whereDate('transaction_date', '<=', $toDate);
        }
        if (!empty($name)) {
            $builder->where('created_by_name', 'like', '%' . $name . '%');
        }
        $builder->orderBy('id', 'desc');

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {

            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }
        LogHelper::info1('Transaction List All By Income ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }

    public function ExpenseReport_csvexport(Request $request) {
        $listAllBYExpenseLists = $this->listAllBYExpanse($request);

        $output = fopen('ExpenseReport.csv', 'w');
        fputcsv($output, array('Date', 'Particular', 'Category', 'Created By', 'Amount'));
        $date_format=GeneralHelper::getDateFormat();
        foreach ($listAllBYExpenseLists['list'] as $values) {
            $transaction_date=GeneralHelper::CsvDate($values->transaction_date, $date_format);
            fputcsv($output, array($transaction_date,$values->particulars, $values->account_category, $values->created_by_name, $values->amount));
            // id: 1, customer_id: 37, prefix: "test123r", date: "2017-05-02", duedate: "2017-05-02"
        }
        LogHelper::info1('ExpenseReport CSV Download ' . $request->fullurl(), $request->all());
        return response()->download(getcwd() . "/ExpenseReport.csv");
    }

    public function listAllBYExpanse(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        $id = $request->input('id');
        $cus_id = $request->input('customer_id', '');
        $is_active = $request->input('is_active', 1);
        $acc_cat = $request->input('account_id', '');

        $fromDate = $request->input('from_Date', '');
        $toDate = $request->input('to_Date', '');
        $acc_cat1 = $request->input('account_category', '');
        $created_by = $request->input('created_by');
        $name = $request->input('created_by_name', '');
        $mode = $request->input('mode', '');

        $builder = DB::table('tbl_transaction')
                ->select('id', 'name', 'fiscal_year_id', 'fiscal_year_code', 'transaction_date', 'voucher_type', 'voucher_number', 'debit As amount', 'customer_id', 'account', 'account_id', 'particulars', 'created_by', 'updated_by', 'updated_at', 'created_at', 'created_by_name', 'updated_by_name', 'account_category', 'payment_mode', 'fiscal_month_code', 'is_active')
                ->where('voucher_type', '!=', 'transfer')
                ->where('voucher_type', '!=', 'inital_balance')
                ->where('is_cash_flow', 1)
                ->where('debit', '!=', 0);

        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if (!empty($cus_id)) {
            $builder->where('customer_id', '=', $cus_id);
        }
        if (!empty($acc_cat)) {
            $builder->where('account_id', '=', $acc_cat);
        }
        if (!empty($acc_cat1)) {
            $builder->where('account_category', '=', $acc_cat1);
        }
        if (!empty($created_by)) {
            $builder->where('created_by', '=', $created_by);
        }
        if (!empty($is_active)) {
            $builder->where('is_active', '=', $is_active);
        }
        if (!empty($fromDate)) {

            $builder->whereDate('transaction_date', '>=', $fromDate);
        }
        if (!empty($toDate)) {

            $builder->whereDate('transaction_date', '<=', $toDate);
        }
        if (!empty($name)) {
            $builder->where('created_by_name', 'like', '%' . $name . '%');
        }

        if ($mode == 1) {
            $builder->orderBy('transaction_date', 'asc');
        } else {
            $builder->orderBy('id', 'desc');
        }


        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {

            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }
        LogHelper::info1('Transaction List All By Expense ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }

    public function transactionIncomeDelete(Request $request, $id) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Transaction Income Deleted Successfully';
        $currentuser = Auth::user();
        try {

            $transaction = Transaction::findOrFail($id);

            if ($transaction->is_active == 0) {
                return $resVal;
            }


            $transaction->updated_by = $currentuser->id;
            $transaction->is_active = 0;

            $account_id = $transaction->account_id;


            if (!empty($transaction->credit)) {

                AccountHelper::withDraw($account_id, $transaction->credit);
            }

            $transaction->update();
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Transaction Income Not Found';
            return $resVal;
        }


        LogHelper::info1('Transaction Income Delete ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Transaction Income', 'delete', $screen_code, $transaction->id);
        return $resVal;
    }

    public function transactionExpenseDelete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Transaction Expense Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {

            $transaction = Transaction::findOrFail($id);
            if ($transaction->is_active == 0) {
                return $resVal;
            }


            $transaction->updated_by = $currentuser->id;
            $transaction->is_active = 0;

            $account_id = $transaction->account_id;


            if (!empty($transaction->debit)) {

                AccountHelper::deposit($account_id, $transaction->debit);
            }

            $transaction->update();
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Transaction Expense Not Found';
            return $resVal;
        }

        LogHelper::info1('Transaction Expense Delete ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Transaction Expense', 'delete', $screen_code, $transaction->id);

        return $resVal;
    }

    public function accountStatement(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
//Get Current and Previous Date
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $account_id = $request->input('account_id');
        $from_Date = $request->input('from_Date');
        $to_Date = $request->input('to_Date');

        $amountBuilder = DB::table('tbl_transaction')
                ->select(DB::raw('id'))
                ->whereDate('transaction_date', '<', $from_Date)
                ->where('is_cash_flow', 1)
                ->where('is_active', '=', 1);
        if (!empty($account_id)) {
            $amountBuilder->where('account_id', '=', $account_id);
        }
        $amount = $amountBuilder->sum('credit');

        $debitamountBuilder = DB::table('tbl_transaction')
                ->select(DB::raw('id'))
                ->where('transaction_date', '<', $from_Date)
                ->where('is_cash_flow', 1)
                ->where('is_active', '=', 1);
        if (!empty($account_id)) {
            $debitamountBuilder->where('account_id', '=', $account_id);
        }
        $debitamount = $debitamountBuilder->sum('debit');
        $account_list = array();
// $account_list['date'] = date('d-m-Y', strtotime($from_Date.'-1 day'));   
        $account_list['description  '] = 'Opening Balance';
        $account_list['credit'] = number_format((float) $amount, 2, '.', '');
        $account_list['debit'] = number_format((float) $debitamount, 2, '.', '');



        $resVal['openBalance'] = $account_list;


        $builder = DB::table('tbl_transaction')
                ->select(DB::raw('*'))
                ->whereDate('transaction_date', '>=', $from_Date)
                ->whereDate('transaction_date', '<=', $to_Date)
                ->where('is_active', '=', 1)
                ->where('is_cash_flow', 1)
                ->orderBy('transaction_date','created_at');

        if (!empty($account_id)) {
            $builder->where('account_id', '=', $account_id);
        }
        $resVal['list'] = $builder->get();
//        
//         $builder = DB::table('tbl_payment')     
//                    ->select( DB::raw('id'),
//                            DB::raw('"Payment" as type'),
//                            DB::raw('date'),
//                            DB::raw('CONCAT("Sales Invoice1", " ", invoice_id) AS description'),
//                            DB::raw('amount*0 as debit'),
//                            DB::raw('amount as credit'))
//                     ->where('customer_id', '=', $id)
//                     ->where('date','>=',$from_date)
//                     ->where('date','<=',$to_date)    
//                     ->union($builder1)
//                    ->orderBy('date','asc')
//                    ->get();           
//         $resVal['list'] = $builder;
        LogHelper::info1('Account Statement List ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }

    public function transferSave(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Transfer Added Successfully';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $sourse_account = $request->source_account;
        $desig_account = $request->desig_account;
        $amount = $request->amount;
        $mydate = $request->date;
        $comments = $request->comments;
        $currentuser = Auth::user();
        $tran_id=0;

        $account = AccountHelper::deposit($desig_account, $amount);
        $account = AccountHelper::withDraw($sourse_account, $amount);
        for ($i = 0; $i <= 1; $i++) {
//            

            $mydate = date(($mydate));


            if ($i == 0) {
                $voucher_number = $desig_account;
                $pmtcode = GeneralHelper::getPaymentAcode($desig_account);
                $pmtcoderef = $desig_account;
            } else {
                $voucher_number = $tran_id;
                $pmtcode = GeneralHelper::getPaymentAcode($sourse_account);
                $pmtcoderef = $sourse_account;
            }
            if ($i == 0) {
                $debit = 0;
                $credit = $amount;
                $account_id = $desig_account;
            } else {
                $debit = $amount;
                $credit = 0;
                $account_id = $sourse_account;
            }




            $transactionData = array(
                "name" => $currentuser->f_name,
                "transaction_date" => $mydate,
                "voucher_type" => "transfer",
                "voucher_number" => $voucher_number,
                "credit" => $credit,
                "debit" => $debit,
                "pmtcode" => $pmtcode,
                "pmtcoderef" => $pmtcoderef,
                "is_cash_flow" => 1, //have to check  account in save and voucher type too
                "account" => $request->input('account', ''),
                "account_id" => $account_id,
                "customer_id" => $request->input('account_id', ''),
                "account_category" => $request->input('customer_id', ''),
                "payment_mode" => $request->input('comments', ''),
                "acode" => "",
                "acoderef" => "",
                "particulars" => $comments,
                "created_by" => $currentuser->id,
                "updated_by" => $currentuser->id,
                "created_by_name" => $currentuser->f_name . ' ' . $currentuser->l_name,
                "updated_by_name" => $currentuser->f_name . ' ' . $currentuser->l_name,
                "is_active" => 1,
            );
            
             $vouchertype= $transactionData['voucher_type'];
             
            if($vouchertype == "transfer")
               $resVal['message'] = 'Journal Saved Successfully'; 
            
            $transaction = TransactionHelper::transactionSave($transactionData);
            


            $resVal['id'] = $transaction->id;
            $tran_id = $transaction->id;
        }
        ActivityLogHelper::transferSave($transaction);
        LogHelper::info1('Transfer Save ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Transfer', 'save', $screen_code, $transaction->id);

        return $resVal;
    }

    public function transferDelete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Transfer Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $transaction = Transaction::findOrFail($id);
            if ($transaction->is_active == 0) {
                return $resVal;
            }
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Transaction Not Found';
            return $resVal;
        }
        $transaction->updated_by = $currentuser->id;
        $desig_account = $transaction->account_id;
        $builder = DB::table('tbl_transaction')
                ->select(DB::raw('account_id'))
                ->where('is_active','=',1)
                ->where('voucher_number', '=', $id)
                ->where('voucher_type', '=', 'transfer')
                ->first();
        $source_account = $builder->account_id;

        $amount = $transaction->credit;
        $account = AccountHelper::deposit($source_account, $amount);
        $account = AccountHelper::withDraw($desig_account, $amount);
//        $transaction->is_active = 0;
//        $transaction->save();
        TransactionHelper::transactionDeleteByPrimary($id);
//        DB::table('tbl_transaction')->where('voucher_number', '=', $id)->where('voucher_type', '=', 'transfer')
//                ->update(['is_active' => 0]);
        
        TransactionHelper::transactionDelete($id, 'transfer', 0);

        ActivityLogHelper::transferDelete($transaction);
        LogHelper::info1('Transfer Delete ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Transfer', 'delete', $screen_code, $transaction->id);

        return $resVal;
    }

    public function transferList(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        $fromDate = $request->input('from_date', '');
        $toDate = $request->input('to_date', '');
        $id = $request->input('id');
        $created_by = $request->input('created_by');
        $builder = DB::table('tbl_transaction as t')
                ->leftJoin('tbl_accounts as c', 't.account_id', '=', 'c.id')
                ->select('t.*', 'c.account')
                ->where('t.voucher_type', '=', 'transfer')
                ->where('t.credit', '!=', 0)
                ->where('t.is_active', '=', 1)
                ->orderBy('t.id', 'desc');
        if (!empty($id)) {
            $builder->where('t.id', '=', $id);
        }
        if (!empty($created_by)) {
            $builder->where('t.created_by', '=', $created_by);
        }
        if (!empty($fromDate)) {

            $builder->whereDate('t.transaction_date', '>=', $fromDate);
        }
        if (!empty($toDate)) {

            $builder->whereDate('t.transaction_date', '<=', $toDate);
        }
        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $collection = $builder->get();
        } else {

           $collection = $builder->skip($start)->take($limit)->get();
        }
        
        foreach($collection as $collect){
            $account = DB::table('tbl_transaction as t')
                ->leftJoin('tbl_accounts as c', 't.account_id', '=', 'c.id')
                ->select('t.account_id', 'c.account')
                ->where('t.voucher_type', '=', 'transfer')
                ->where('t.debit', '!=', 0)
                ->where('t.is_active', '=', 1)
                ->where('voucher_number','=',$collect->id)
                ->first();
            
            if($account != null && $account != ''){
                $collect->source_account=$account->account;
                $collect->source_account_id=$account->account_id;
            }else{
                $collect->source_account='';
                $collect->source_account_id=0;
            }
            
            
        }
        
        $resVal['list'] = $collection;
        LogHelper::info1('Transfer List ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return $resVal;
    }

  public function payableAndReceivableReport(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $customer_id = $request->input('customer_id');
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');

        $payable = array();
        $receivable = array();
        $data = array();
        $resVal['success'] = true;
        $result = array("receivable", "payable");
        
        
  $builder2=DB::table('tbl_invoice as i')
          ->leftJoin('tbl_customer as c','i.customer_id','=','c.id')
          ->leftJoin(DB::raw("(select sum(balance_amount) as balance_amount,customer_id,date from tbl_adv_payment where is_active = 1 and voucher_type='advance_invoice' and (date >= '".$from_date."' or ''='".$from_date."')  and (date <= '".$to_date."' or ''='".$to_date."') group by customer_id) as ap on ap.customer_id = c.id")
          ,function($join){
          })
          ->select(DB::raw("(Concat(c.fname,' ',Coalesce(c.lname,''))) as customer_name"),'c.customer_code','c.email', 'c.phone', 'c.billing_city','i.customer_id',DB::raw("coalesce(sum(i.total_amount-i.paid_amount),0)-COALESCE(ap.balance_amount,0) as amount"),DB::raw("'receivable' as type"))
          ->where('i.is_active','=',1)
          ->groupBy('i.customer_id');
          
          if (!empty($from_date)) {
            $builder2->whereDate('i.date', '>=', $from_date);
        }
        if (!empty($to_date)) {
            $builder2->whereDate('i.date', '<=', $to_date);
        }
        if (!empty($customer_id)) {
            $builder2->where('i.customer_id', '=', $customer_id);
        }
        
        
          $builder=DB::table('tbl_purchase_invoice as pi')
          ->leftJoin('tbl_customer as c','pi.customer_id','=','c.id')
          ->leftJoin(DB::raw("(select sum(balance_amount) as balance_amount,customer_id,date from tbl_adv_payment where is_active = 1 and voucher_type='advance_purchase' and (date >= '".$from_date."' or ''='".$from_date."')  and (date <= '".$to_date."' or ''='".$to_date."') group by customer_id) as ap on ap.customer_id = c.id")
          ,function($join){
          })
          ->select(DB::raw("(Concat(c.fname,' ',Coalesce(c.lname,''))) as customer_name"),'c.customer_code','c.email', 'c.phone', 'c.billing_city','pi.customer_id',DB::raw("coalesce(sum(pi.total_amount-pi.paid_amount),0)-COALESCE(ap.balance_amount,0) as amount"),DB::raw("'payable' as type"))
          ->where('pi.is_active','=',1)
          ->groupBy('pi.customer_id');
          
          if (!empty($from_date)) {
            $builder->whereDate('pi.date', '>=', $from_date);
        }
        if (!empty($to_date)) {
            $builder->whereDate('pi.date', '<=', $to_date);
        }
        if (!empty($customer_id)) {
            $builder->where('pi.customer_id', '=', $customer_id);
        }
        $builder->unionAll($builder2);
          


//        $builder = DB::table('tbl_transaction as t')
//                ->select(DB::raw("(Concat(c.fname,' ',Coalesce(c.lname,''))) as customer_name"), 'c.customer_code', 'c.email', 'c.phone', 'c.billing_city', DB::raw("coalesce(sum(credit)-sum(debit),0) as amount"), 't.customer_id')
//                ->leftjoin('tbl_customer as c', 'c.id', '=', 't.customer_id')
//                ->WhereRaw(DB::raw("(t.voucher_type='advance_invoice' or t.voucher_type='advance_purchase' or t.voucher_type='purchase_invoice' or t.voucher_type='sales_invoice' or t.voucher_type='sales_invoice_payment' or t.voucher_type='purchase_invoice_payment')"))
//                ->where('t.is_active', '=', 1)
//                ->groupBy('t.customer_id');
//
//        if (!empty($from_date)) {
//            $builder->whereDate('t.transaction_date', '>=', $from_date);
//        }
//        if (!empty($to_date)) {
//            $builder->whereDate('t.transaction_date', '<=', $to_date);
//        }
//
//        if (!empty($customer_id)) {
//            $builder->where('c.id', '=', $customer_id);
//        }
        $collection = $builder->get();
        Foreach ($collection as $build) {
            if ($build->type == 'receivable') {
                array_push($receivable, $build);
            } else  {
                array_push($payable, $build);
            }
        }

        array_push($data, $receivable);
        array_push($data, $payable);
        $resVal['list'] = array_combine($result, $data);
     LogHelper::info1('payableAndReceivableReport' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return $resVal;
    }
    
    public function payableReceivaleReport_csvexport(Request $request) {
        $payableLists = $this->payableAndReceivableReport($request);
        ini_set('max_execution_time', 600); //increase max_execution_time to 10 min if data set is very large
        //create a file
        $type=$request->input('type');
        if($type=="payable"){
        $filename = "payablereport" . "_" . time() . ".csv";
        $typeOf="payablereport";
        }else{
            $typeOf="receivablesreport";
        $filename = "receivablesreport" . "_" . time() . ".csv";
        }
        //  $filename = "customer" . "_" . date("Y-m-d_H-i", time()) . ".csv";
        $serverName = $_SERVER['SERVER_NAME'];

        $filePath = "download/$serverName/csv";

        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);

            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {

                if ($folderName == '')
                    $folderName = $folderList[$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index];

                if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        $filePath = getcwd() . '/' . $filePath;
        $sr = 1;
        $output = fopen($filePath . "/" . $filename, 'w');
        $arrHead=array('#', 'Customer Name','City', 'Mobile', 'Email', 'Amount');
        if($type=="payable"){
            $repArray=array("1"=> 'Vendor Name');
            $arrHead= array_replace($arrHead,$repArray);
        }
        fputcsv($output, $arrHead);
        
        foreach ($payableLists['list'][$type] as $values) {
            $ph=explode(',',$values->phone);
            $phNo=implode(', ',$ph);
            fputcsv($output, array($sr, $values->customer_name, $values->billing_city, $phNo, $values->email, $values->amount));
            $sr++;
        }
        LogHelper::info1('Payable And Receivable Report CSV Download' . $request->fullurl(), $request->all());
        return response()->download($filePath . "/" . $filename, $typeOf.'.csv');
    }

}

?>
