<?php

namespace App\Http\Controllers;

use DB;
//use App\SysCategory;
use App\TransationCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\LogHelper;
use App\Helper\NotificationHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SysCategoryController
 *
 * @author Deepa
 */
class TransationCategoryController extends Controller {

    //put your code here 
    public function save(Request $request) {
        $resVal = array();
        $resVal['success'] = TRUE;
        if(strcasecmp($request->type, 'income') == 0)
            $resVal['message'] = 'Income Category Added Successfully';
        elseif(strcasecmp($request->type, 'expense')==0)
            $resVal['message'] = 'Expense Category Added Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();


        $syscategory = new TransationCategory;

        $transactionCollection = TransationCategory::where('name', "=", $request->input('name'))
                        ->where('type', '=', $request->input('type'))
                        ->where('is_active', '=', 1)->get();
        if (count($transactionCollection) > 0) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Name Is Already Exists';
            return $resVal;
        }
        if ($request->input('name') && $request->input('type')) {
            $syscategory->created_by = $currentuser->id;
            $syscategory->updated_by = $currentuser->id;
            $syscategory->is_active = $request->input('is_active', 1);

            $syscategory->fill($request->all());
            $syscategory->save();
        } else {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Name Is Invalid';
            return $resVal;
        }


        $resVal['id'] = $syscategory->id;
        LogHelper::info1('Transaction Category Save ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Transaction Category', 'save', $screen_code, $syscategory->id);
        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id', '');
        $type = $request->input('type', '');
        $name = $request->input('name', '');
        $isactive = $request->input('is_active', '');


        //$status = $request->input('status', '');
        $builder = DB::table('tbl_tran_category')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if (!empty($type)) {
            $builder->where('type', '=', $type);
        }
        if (!empty($name)) {
            $builder->where('name', 'like', '%' . $name . '%');
        }
        if ($isactive != '') {
            $builder->where('is_active', '=', $isactive);
        }


        $builder->orderBy('seq_no', 'asc');

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {

            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }
        LogHelper::info1('Transaction Category List All ' . $request->fullurl(), json_decode(json_encode($resVal['list']), true));
        return ($resVal);
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        if(strcasecmp($request->type, 'income') == 0)
            $resVal['message'] = 'Income Category Updated Successfully';
        elseif(strcasecmp($request->type, 'expense') == 0)
            $resVal['message'] = 'Expense Category Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $syscategory = TransationCategory::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Category Not Found';
            return $resVal;
        }
        
        $transactionCollection = TransationCategory::where('name', "=", $request->input('name'))
                        ->where('type', '=', $request->input('type'))
                        ->where('id', '!=', $id)
                        ->where('is_active', '=', 1)->get();
        
        if (count($transactionCollection) > 0) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Name Is Already Exists';
            return $resVal;
        }
        
        $syscategory->created_by = $currentuser->id;
        $syscategory->fill($request->all());
        $syscategory->save();
        LogHelper::info1('Transaction Category Update ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Transaction Category', 'update', $screen_code, $syscategory->id);
        return $resVal;
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        try {
            $syscategory = TransationCategory::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Category Not Found';
            return $resVal;
        }
        $resVal['success'] = TRUE;
        if(strcasecmp($syscategory->type, 'income') == 0)
            $resVal['message'] = 'Income Category Deleted Successfully';
        elseif(strcasecmp($syscategory->type, 'expense') == 0)
            $resVal['message'] = 'Expense Category Deleted Successfully';
        $syscategory->is_active = 0;
        $syscategory->update();
        LogHelper::info1('Transaction Category Delete ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Transaction Category', 'delete', $screen_code, $syscategory->id);
        return $resVal;
    }

}

?>
