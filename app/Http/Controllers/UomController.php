<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\Uom;
use Illuminate\Http\Request;
use App\Transformer\UomTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\LogHelper;
use App\Helper\NotificationHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UomController
 *
 * @author Deepa
 */
class UomController extends Controller {

    //put your code here

    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Uom Added Successfully';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
//        $validator = Validator::make($request->all(), [
//                    'name' => 'required|unique:tbl_uom,name'
//        ]);
//        if ($validator->fails()) {
//            $resVal['success'] = FALSE;
//            $resVal['message'] = 'Error In Request Format';
//            if (array_key_exists('name', $validator->failed())) {
//                $resVal['message'] = 'UOM Name Is Already Exist';
//            }
//
//            return $resVal;
//        }
        
         if ($request->input('name') != NULL) {

            $uom_name = Uom::where('name', "=", $request->input('name'))
                            ->where('is_active', '=', 1)->get();

            if (count($uom_name) > 0) {
                $resVal['success'] = FALSE;
                $resVal['message'] = 'Uom Name Is Already Exists';
                return $resVal;
            }
        }


        $unit = new Uom;

        $unit->created_by = $currentuser->id;
        $unit->updated_by = $currentuser->id;
        $unit->is_active = $request->input('is_active', 1);

        $unit->fill($request->all());
        $unit->save();

        $resVal['id'] = $unit->id;
        LogHelper::info1('Uom Save ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Uom', 'save', $screen_code, $unit->id);

        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $name = $request->input('name', '');
        $is_active = $request->input('is_active', '');

        //$status = $request->input('status', '');
        $builder = DB::table('tbl_uom')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if ($is_active != '') {
            $builder->where('is_active', '=', $is_active);
        }

        if (!empty($name)) {
            $builder->where('name', 'like', '%' . $name . '%');
//            DB::connection()->enableQueryLog();
//            var_dump(DB::getQueryLog());
        }
        $builder->orderBy('id', 'desc');

        $resVal['total'] = $builder->count();
        $resVal['list'] = $builder->skip($start)->take($limit)->get();

        LogHelper::info1('Uom List All ' . $request->fullurl(),  json_decode(json_encode($resVal['list']), true));

        return ($resVal);
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Uom Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        
        if ($request->input('name') != NULL) {

            $uom_name = Uom::where('name', "=", $request->input('name'))
                            ->where('is_active', '=', 1)->where('id', '!=', $id)->get();

            if (count($uom_name) > 0) {
                $resVal['success'] = FALSE;
                $resVal['message'] = 'Uom Name Is Already Exists';
                return $resVal;
            }
        }

        try {
            $unit = Uom::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Uom Not Found';
            return $resVal;
        }
        $unit->created_by = $currentuser->id;
        $unit->fill($request->all());
        $unit->save();
        LogHelper::info1('Uom Update ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Uom', 'update', $screen_code, $unit->id);
        return $resVal;
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Uom Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        try {
            $unit = Uom::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Uom Not Found';
            return $resVal;
        }

        $unit->delete();
        
        LogHelper::info1('Uom Delete ' . $request->fullurl(), $request->all());
        NotificationHelper::saveNotification($request, 'Uom', 'delete', $screen_code, $unit->id);

        return $resVal;
    }

}

?>
