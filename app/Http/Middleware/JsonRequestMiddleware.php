<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class JsonRequestMiddleWare {

    public function handle(Request $request, Closure $next) {
        $api_token = $request->header('api_token');
        
        if (in_array($request->method(), ['POST', 'PUT', 'PATCH']) && $request->isJson()
        ) {
            $data = $request->json()->all();
            $request->request->replace(is_array($data) ? $data : []);
        }
        return $next($request);
    }

}
