<?php

use Psr\Http\Message\ServerRequestInterface;
use Illuminate\Support\Facades\Session;

//use Illuminate\Http\Request;
/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It is a breeze. Simply tell Lumen the URIs it should respond to
  | and give it the Closure to call when that URI is requested.
  |
 */

$app->get('/', function () use ($app) {
    return $app->version();
});


$app->group(['prefix' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('login', 'AuthController@login');
    $app->post('ssologin', 'AuthController@ssologin');
    $app->post('convertSSOTokenToAPIToken', 'AuthController@convertSSOTokenToAPIToken');
});

$app->group(['prefix' => 'auth', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->get('logout', 'AuthController@logout');
    $app->get('userInfo', 'AuthController@userInfo');
});

$app->group(['prefix' => 'company'], function (\Laravel\Lumen\Application $app) {

    $app->post('/save', 'CompanyController@save');
});

//product
$app->group(['prefix' => 'product', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('/save', 'ProductController@save');
    $app->GET('/listAll', 'ProductController@listAll');
    $app->GET('/listWithTax', 'ProductController@listWithTax');
    $app->GET('/listMappedTax', 'ProductController@listMappedTax');
    $app->PUT('/modify/{id:[\d]+}', 'ProductController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'ProductController@delete');
    $app->GET('/pdtListForBill', 'ProductController@pdtListForBill');
});
//Uom
$app->group(['prefix' => 'uom', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('/save', 'UomController@save');
    $app->GET('/listAll', 'UomController@listAll');
    $app->PUT('/modify/{id:[\d]+}', 'UomController@update');
    $app->get('/listByIdDesc', 'UomController@listByIdDesc');
    $app->DELETE('/delete/{id:[\d]+}', 'UomController@delete');
});
//country
$app->group(['prefix' => 'city', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('/save', 'CityController@save');
    $app->GET('/listAll', 'CityController@listAll');
    $app->PUT('/update/{id:[\d]+}', 'CityController@update');
    $app->get('/listByIdDesc', 'CityController@listByIdDesc');
    $app->DELETE('/delete/{id:[\d]+}', 'CityController@delete');
});
$app->group(['prefix' => 'country', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('/save', 'CountryController@save');
    $app->PUT('/update/{id:[\d]+}', 'CountryController@update');
    $app->GET('/listAll', 'CountryController@listAll');
    $app->DELETE('/delete/{id:[\d]+}', 'CountryController@delete');
});
$app->group(['prefix' => 'state', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('/save', 'StateController@save');
    $app->GET('/listAll', 'StateController@listAll');
    $app->DELETE('/delete/{id:[\d]+}', 'StateController@delete');
    $app->PUT('/update/{id:[\d]+}', 'StateController@update');
});
//Album
$app->group(['prefix' => 'urlsign'], function (\Laravel\Lumen\Application $app) {

    $app->GET('/url', 'UrlSignController@url');
});
//Album
$app->group(['prefix' => 'album', 'middleware' => 'auth', 'permissions' => 'access'], function (\Laravel\Lumen\Application $app) {

    $app->post('/permissionCheck', 'AlbumController@permissionCheck');
    $app->post('/save', 'AlbumController@save');
    $app->GET('/listAll', 'AlbumController@listAll');
    $app->PUT('/modify/{id:[\d]+}', 'AlbumController@update');

    $app->DELETE('/delete/{id:[\d]+}', 'AlbumController@delete');
});
$app->group(['prefix' => 'album'], function (\Laravel\Lumen\Application $app) {
    $app->GET('/listAll', 'AlbumController@listAll');
    $app->get('/detail', 'AlbumController@detail');
});


//AlbumPermission
$app->group(['prefix' => 'albumpermission', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->POST('/save', 'AlbumPermissionController@save');
    $app->Get('/listAll', 'AlbumPermissionController@listAll');
    $app->DELETE('/delete/{id:[\d]+}', 'AlbumPermissionController@delete');
});
//Albumasset       
$app->group(['prefix' => 'albumasset', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->Post('/save', 'AlbumassetController@save');
    $app->Get('/listAll', 'AlbumassetController@listAll');
//    $app->Get('/detail', 'QuoteController@detail');
    $app->PUT('/modify/{id:[\d]+}', 'AlbumassetController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'AlbumassetController@delete');
});
$app->group(['prefix' => 'albumasset'], function (\Laravel\Lumen\Application $app) {
    $app->Post('/save', 'AlbumassetController@save');
    $app->Get('/listAll', 'AlbumassetController@listAll');
});
//Albumassetcomments       
$app->group(['prefix' => 'albumassetcomments', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->Post('/save', 'AlbumassetcommentsController@save');
    $app->Get('/listAll', 'AlbumassetcommentsController@listAll');
//    $app->Get('/detail', 'QuoteController@detail');
    $app->PUT('/modify/{id:[\d]+}', 'AlbumassetcommentsController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'AlbumassetcommentsController@delete');
});
$app->group(['prefix' => 'albumassetcomments'], function (\Laravel\Lumen\Application $app) {
    $app->Post('/save', 'AlbumassetcommentsController@save');
    $app->Get('/listAll', 'AlbumassetcommentsController@listAll');
});
$app->group(['prefix' => 'uom', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('/save', 'UomController@save');
    $app->GET('/listAll', 'UomController@listAll');
    $app->PUT('/modify/{id:[\d]+}', 'UomController@update');
    $app->get('/listByIdDesc', 'UomController@listByIdDesc');
    $app->DELETE('/delete/{id:[\d]+}', 'UomController@delete');
});
//UserRole
$app->group(['prefix' => 'role', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('/save', 'RoleController@save');
    $app->GET('/listAll', 'RoleController@listAll');
    $app->PUT('/modify/{id:[\d]+}', 'RoleController@update');
    $app->get('/listByIdDesc', 'RoleController@listByIdDesc');
    $app->DELETE('/delete/{id:[\d]+}', 'RoleController@delete');
});
//UserRole
$app->group(['prefix' => 'paymentTerm', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('/save', 'PaymentTermController@save');
    $app->GET('/listAll', 'PaymentTermController@listAll');
    $app->PUT('/modify/{id:[\d]+}', 'PaymentTermController@update');
    $app->get('/listByIdDesc', 'PaymentTermController@listByIdDesc');
    $app->DELETE('/delete/{id:[\d]+}', 'PaymentTermController@delete');
});
//Customer
$app->group(['prefix' => 'customer', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->Post('/save', 'CustomerController@save');
    $app->Get('/listAll', 'CustomerController@listAll');
    $app->Get('/detail', 'CustomerController@detail');
    $app->PUT('/modify/{id:[\d]+}', 'CustomerController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'CustomerController@delete');
    $app->Get('/search', 'CustomerController@search');
});
//Customer Dual Info
$app->group(['prefix' => 'customerdualinfo', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->Post('/save', 'CustomerDualController@save');
    $app->Get('/listAll', 'CustomerDualController@listAll');
    $app->Get('/detail', 'CustomerDualController@detail');
    $app->PUT('/modify/{id:[\d]+}', 'CustomerDualController@update');
    $app->Get('/search', 'CustomerDualCustomerController@search');
});
//Invoices
$app->group(['prefix' => 'invoice', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->Post('/save', 'InvoiceController@save');
    $app->Get('/listAll', 'InvoiceController@listAll');
    $app->Get('/list_All', 'InvoiceController@list_All');
    $app->GET('/customerdetails', 'InvoiceController@customerdetails');

    $app->GET('/salesinvoicejson_csvexport', 'InvoiceController@salesinvoicejson_csvexport');
    $app->Get('/detail', 'InvoiceController@detail');
    $app->get('/invoicecreate', 'InvoiceController@invoicecreate');
    $app->PUT('/modify/{id:[\d]+}', 'InvoiceController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'InvoiceController@delete');
    $app->Get('/invoiceReminder/{id:[\d]+}', 'InvoiceController@invoiceReminder');

    $app->Get('/salesTaxReport', 'InvoiceController@salesTaxReport');

    $app->Get('/salesTaxReportSummery', 'InvoiceController@salesTaxReportSummery');
    $app->Get('/summary', 'InvoiceController@summary');

    $app->Get('/receivable_list', 'InvoiceController@receivable_list');
    $app->POST('/invoiceMailSend', 'InvoiceController@invoiceMailSend');
    $app->Get('/pdfPreview/{id:[\d]+}', 'InvoiceController@pdfPreview');
});
//Invoices Return
$app->group(['prefix' => 'invoicereturn', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->Get('/listAll', 'InvoiceReturnController@listAll');
    $app->Post('/save', 'InvoiceReturnController@save');
    $app->DELETE('/delete/{id:[\d]+}', 'InvoiceReturnController@delete');
    $app->GET('/detail', 'InvoiceReturnController@detail');
    $app->PUT('/modify/{id:[\d]+}', 'InvoiceReturnController@update');
});

//Albumasset       
$app->group(['prefix' => 'albumasset', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->Post('/save', 'AlbumassetController@save');
    $app->Get('/listAll', 'AlbumassetController@listAll');
    $app->PUT('/modify/{id:[\d]+}', 'AlbumassetController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'AlbumassetController@delete');
});
//Albumassetcomments       
$app->group(['prefix' => 'albumassetcomments', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->Post('/save', 'AlbumassetcommentsController@save');
    $app->Get('/listAll', 'AlbumassetcommentsController@listAll');
//    $app->Get('/detail', 'QuoteController@detail');
    $app->PUT('/modify/{id:[\d]+}', 'AlbumassetcommentsController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'AlbumassetcommentsController@delete');
});
//Quotes        
$app->group(['prefix' => 'quote', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->Post('/save', 'QuoteController@save');
    $app->Get('/listAll', 'QuoteController@listAll');
    $app->Get('/detail', 'QuoteController@detail');
    $app->PUT('/modify/{id:[\d]+}', 'QuoteController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'QuoteController@delete');
});

//Sales Order       
$app->group(['prefix' => 'salesorder', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->Post('/save', 'SalesOrderController@save');
    $app->Get('/listAll', 'SalesOrderController@listAll');
    $app->Get('/detail', 'SalesOrderController@detail');
    $app->PUT('/modify/{id:[\d]+}', 'SalesOrderController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'SalesOrderController@delete');
});

//QuotePurchaseOrder
$app->group(['prefix' => 'purchasequote', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->Post('/save', 'PurchasequoteController@save');
    $app->Get('/listAll', 'PurchasequoteController@listAll');
    $app->Get('/detail', 'PurchasequoteController@detail');
    $app->PUT('/modify/{id:[\d]+}', 'PurchasequoteController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'PurchasequoteController@delete');
});


//QuotePurchaseOrder
$app->group(['prefix' => 'purchaseinvoice', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->Post('/save', 'PurchaseinvoiceController@save');

    $app->Get('/listAll', 'PurchaseinvoiceController@listAll');
    $app->Get('/detail', 'PurchaseinvoiceController@detail');
    $app->PUT('/modify/{id:[\d]+}', 'PurchaseinvoiceController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'PurchaseinvoiceController@delete');
    $app->Get('/payable_list', 'PurchaseinvoiceController@payable_list');
    $app->Get('/purchase_receipt', 'PurchaseinvoiceController@purchasereceipt_csvexport');
    $app->Get('/partywiseStockReport', 'PurchaseinvoiceController@partywiseStockReport');
    $app->Get('/partywiseSalesReport', 'PurchaseinvoiceController@partywiseSalesReport');
    $app->DELETE('/purchaseInvoiceItemDelete/{id:[\d]+}', 'PurchaseinvoiceController@purchaseInvoiceItemDelete');
    $app->Get('/invoiceDetail', 'PurchaseinvoiceController@invoiceDetail');
    $app->Get('/partywisestock_csvexport','PurchaseinvoiceController@partywisestock_csvexport');
    $app->Get('/summary','PurchaseinvoiceController@summary');
});

//Quotesitems        
$app->group(['prefix' => 'quotesitem', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->Post('/save', 'QuotesitemController@save');
    $app->Get('/listAll', 'QuotesitemController@listAll');

    $app->PUT('/modify/{id:[\d]+}', 'QuotesitemController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'QuotesitemController@delete');
});

//Purchasequotesitems        
$app->group(['prefix' => 'purchasequoteitem', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->Post('/save', 'PurchasequoteitemController@save');
    $app->Get('/listAll', 'PurchasequoteitemController@listAll');

    $app->PUT('/modify/{id:[\d]+}', 'PurchasequoteitemController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'PurchasequoteitemController@delete');
});

//Purchaseinvoiceitems        
$app->group(['prefix' => 'purchaseinvoiceitem', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->Post('/save', 'PurchaseinvoiceitemController@save');
    $app->Get('/listAll', 'PurchaseinvoiceitemController@listAll');
    $app->Get('/posProductDetail', 'PurchaseinvoiceitemController@posProductDetail');
    $app->PUT('/modify/{id:[\d]+}', 'PurchaseinvoiceitemController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'PurchaseinvoiceitemController@delete');
});


//Attributestypes
$app->group(['prefix' => 'attributestype', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->Get('/listAll', 'AttributestypeController@listAll');
});

//Screen
$app->group(['prefix' => 'screen', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->Get('/listAll', 'ScreenController@listAll');
});
//Appconfig
$app->group(['prefix' => 'appconfig', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->POST('/modify', 'ApconfigController@update');
    $app->Get('/listAll', 'ApconfigController@listAll');
});
//userscreenacess
$app->group(['prefix' => 'roleacess', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->POST('/modify', 'RoleAccessController@update');
    $app->Get('/listAll', 'RoleAccessController@listAll');
});

//Attribute
$app->group(['prefix' => 'attribute', 'middleWare' => 'auth'], function (Laravel\Lumen\Application $app) {

    $app->Post('/save', 'AttributeController@save');
    $app->Get('/listAll', 'AttributeController@listAll');
    $app->PUT('/modify/{id:[\d]+}', 'AttributeController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'AttributeController@delete');
});
//Tax
$app->group(['prefix' => 'tax', 'middleWare' => 'auth'], function (Laravel\Lumen\Application $app) {

    $app->Post('/save', 'TaxController@save');
    $app->Get('/listAll', 'TaxController@listAll');
    $app->Get('/activeListAll', 'TaxController@expiredListAll');
    $app->PUT('/modify/{id:[\d]+}', 'TaxController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'TaxController@delete');
    $app->get('/calculate', 'TaxController@calculate');
    $app->Get('/groupdetail', 'TaxController@groupDetail');
});
//Debitnote
$app->group(['prefix' => 'debitnote', 'middleWare' => 'auth'], function (Laravel\Lumen\Application $app) {

    $app->Post('/save', 'DebitnoteController@save');
    $app->Get('/listAll', 'DebitnoteController@listAll');
    $app->PUT('/modify/{id:[\d]+}', 'DebitnoteController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'DebitnoteController@delete');
});
//Credit Node
$app->group(['prefix' => 'creditnote', 'middleWare' => 'auth'], function (Laravel\Lumen\Application $app) {

    $app->Post('/save', 'CreditnoteController@save');
    $app->Get('/listAll', 'CreditnoteController@listAll');
    $app->PUT('/modify/{id:[\d]+}', 'CreditnoteController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'CreditnoteController@delete');
});


$app->group(['prefix' => 'user', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->get('list', 'UserController@index');
    $app->post('save', 'UserController@save');
    $app->post('userInvite', 'UserController@userInvite');
    $app->put('update/{id}', 'UserController@update');
    $app->delete('delete/{id}', 'UserController@delete');
    $app->GET('/listingAll', 'UserController@listAll');
    $app->post('/changePwd', 'UserController@changePwd');
    $app->GET('/businessList', 'UserController@businessList');
    $app->POST('/userVerification', 'UserController@userVerification');
    $app->post('/newUserUpdate', 'UserController@newUserUpdate');
});


//CLIENT
$app->group(['prefix' => 'client', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->POST('/save', 'ClientController@save');
    $app->PUT('/modify/{id:[\d]+}', 'ClientController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'ClientController@delete');
    $app->GET('/listAll', 'ClientController@listAll');
    $app->GET('/listWithLimit', 'ClientController@listWithLimit');
    $app->GET('/listById/{id:[\d]+}', 'ClientController@listById');
    $app->GET('/index', 'UserController@index1');
});

//CATEGORY
$app->group(['prefix' => 'category', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->post('/save', 'CategoryController@save');
    $app->PUT('/modify/{id:[\d]+}', 'CategoryController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'CategoryController@delete');
    $app->GET('/listWithLimit', 'CategoryController@listWithLimit');
    $app->get('/listByIdNameDesc', 'CategoryController@listByIdNameDesc');
    $app->GET('/listAll', 'CategoryController@listAll');
    $app->GET('/listBYIdName', 'CategoryController@listBYIdName');
    $app->GET('/{id:[\d]+}', [
        'as' => 'tbl_category.listById',
        'uses' => 'CategoryController@listById'
    ]);
    $app->post('listAll', 'CategoryController@listAll');
});

//DEVICE
$app->group(['prefix' => 'device', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->post('/save', 'DeviceController@save');
    $app->PUT('/modify/{id:[\d]+}', 'DeviceController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'DeviceController@delete');
    $app->GET('/listAll', 'DeviceController@listAll');
    $app->GET('/deviceValidation', 'DeviceController@validation');
});



$app->put('/company', 'CompanyController@save');
$app->get('/company', 'CompanyController@listAll');
$app->get('/company/listwithlimit', 'CompanyController@listWithLimit');
$app->get('/company/customlist', 'CompanyController@customlist');
$app->get('/company/listAll', 'CompanyController@listAll');


//payment
$app->group(['prefix' => 'payment', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->get('listAll', 'PaymentController@listAll');
    $app->post('save', 'PaymentController@save');
    $app->put('modify/{id}', 'PaymentController@update');
    $app->delete('delete/{id}', 'PaymentController@delete');
    $app->post('updateSettledamt', 'PaymentController@updateSettledamt');
    $app->GET('findCardAmount', 'PaymentController@findCardAmount');
});

//payment
$app->group(['prefix' => 'purchasepayment', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->get('listAll', 'PurchasePaymentController@listAll');
    $app->post('save', 'PurchasePaymentController@save');
    $app->put('modify/{id}', 'PurchasePaymentController@update');
    $app->delete('delete/{id}', 'PurchasePaymentController@delete');
    $app->post('updateSettledamt', 'PurchasePaymentController@updateSettledamt');
});


$app->group(['prefix' => 'system', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->get('save', 'SystemController@index');
});
////Account
//$app->group(['prefix' => 'account', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {
//
//    $app->get('list', 'PaymentController@index');
//    $app->post('withdraw', 'AccountController@withdrawSave');
//    $app->put('update/{id}', 'PaymentController@update');
//    $app->delete('delete/{id}', 'PaymentController@delete');
//    $app->post('updateSettledamt', 'PaymentController@updateSettledamt');
//    $app->GET('listAll', 'AccountController@listAll');
//});
//Expense

$app->group(['prefix' => 'expense', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->POST('save', 'ExpenseController@save');
    $app->GET('listAll', 'ExpenseController@listAll');
    $app->put('update/{id}', 'ExpenseController@update');

    $app->Get('/detail', 'ExpenseController@detail');
});
$app->group(['prefix' => 'advancepayment', 'middleware' => 'auth'], function(\Laravel\Lumen\Application $app) {
    $app->POST('save', 'AdvancePaymentController@save');
    $app->GET('listAll', 'AdvancePaymentController@listAll');
    $app->put('update/{id}', 'AdvancePaymentController@update');
    $app->DELETE('delete/{id}', 'AdvancePaymentController@delete');
    $app->put('creditDebtUpdate/{id}', 'AdvancePaymentController@creditDebtUpdate');
});
$app->group(['prefix' => 'paymentallotment', 'middleware' => 'auth'], function(\Laravel\Lumen\Application $app) {
    $app->POST('save', 'AdvPaymentAllotmentController@save');
    $app->GET('listAll', 'AdvPaymentAllotmentController@listAll');
    $app->put('update/{id}', 'AdvPaymentAllotmentController@update');
    $app->DELETE('delete/{id}', 'AdvPaymentAllotmentController@delete');
});
//TransationCategory

$app->group(['prefix' => 'transationcategory', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->POST('save', 'TransationCategoryController@save');
    $app->GET('listAll', 'TransationCategoryController@listAll');
    $app->put('update/{id}', 'TransationCategoryController@update');
    $app->DELETE('delete/{id}', 'TransationCategoryController@delete');
    $app->Get('/detail', 'TransationCategoryController@detail');
});
//Transation

$app->group(['prefix' => 'transaction', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->POST('save', 'TransactionController@save');
    $app->POST('transferSave', 'TransactionController@transferSave');
    $app->GET('transferList', 'TransactionController@transferList');
    $app->DELETE('transferDelete/{id}', 'TransactionController@transferDelete');

    $app->GET('listAll', 'TransactionController@listAll');
    $app->POST('listAllByDate', 'TransactionController@listAllByDate');
    $app->DELETE('delete/{id}', 'TransactionController@delete');
    $app->Get('/detail', 'TransactionController@detail');
    $app->GET('listByIncome', 'TransactionController@listAllByIncome');
    $app->GET('listBYExpense', 'TransactionController@listAllBYExpanse');
    $app->DELETE('transactionIncomeDelete/{id}', 'TransactionController@transactionIncomeDelete');
    $app->DELETE('transactionExpenseDelete/{id}', 'TransactionController@transactionExpenseDelete');

    $app->GET('accountStatement', 'TransactionController@accountStatement');
    $app->GET('payableAndReceivableReport', 'TransactionController@payableAndReceivableReport');
   
});

//EmailTemplates

$app->group(['prefix' => 'emailTemplates', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->put('update', 'EmailTemplatesController@update');
    $app->GET('listAll', 'EmailTemplatesController@listAll');
});



//Transation

$app->group(['prefix' => 'fiscalyear', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {


    $app->GET('listAll', 'FiscalYearController@listAll');
});

//ActivityLogController
$app->group(['prefix' => 'activitylog', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {


    $app->GET('listAll', 'ActivityLogController@listAll');
});
//FiscalMonth
$app->group(['prefix' => 'fiscalmonth', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {


    $app->GET('listAll', 'FiscalMonthController@listAll');
});

//Dashboard
$app->group(['prefix' => 'dashboard', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->Get('/InvoiceVsPayment/{id:[\d]+}', 'DashBoardController@InvoiceVsPayment');
    $app->Get('/InvoicePurchaseVsPayment/{id:[\d]+}', 'DashBoardController@InvoicePurchaseVsPayment');
    $app->Get('/IncomeVsExpenses/{id:[\d]+}', 'DashBoardController@IncomeVsExpenses');
    $app->GET('categoryVsIncomeSummery/{id:[\d]+}', 'DashboardController@categoryVsIncomeSummery');
    $app->GET('categoryVsExpenseSummery/{id:[\d]+}', 'DashboardController@categoryVsExpenseSummery');
    $app->GET('incomeVsExpenseSummery', 'DashboardController@incomeVsExpenseSummery');
    $app->Get('/SalesPayment/{id:[\d]+}', 'DashBoardController@SalesPayment');
    $app->Get('/SalesPurchaseMonthlyReport/{id:[\d]+}', 'DashBoardController@SalesPurchaseMonthlyReport');
    $app->Get('/salesMonthReport', 'DashBoardController@salesMonthReport');
    $app->Get('/salesMonthTotalReport/{id:[\d]+}', 'DashBoardController@salesMonthTotalReport');
    $app->Get('/todayIncomeVsExpenses', 'DashBoardController@todayIncomeVsExpenses');
    $app->Get('/AgeingReport', 'DashBoardController@AgeingReport');
});
//StockManagement
$app->group(['prefix' => 'stock', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->Post('/StockAdjustment', 'StockManagmentController@StockAdjustment');
    $app->Post('/StockWastage', 'StockManagmentController@StockWastage');
    $app->GET('/InventoryAdjustment_list', 'StockManagmentController@InventoryAdjustment_list');
    $app->GET('/listAll', 'StockManagmentController@listAll');
    $app->GET('/MinStocklist', 'StockManagmentController@MinStocklist');
});
//Reports
$app->group(['prefix' => 'reports', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->GET('ItemWiseReport', 'ReportsController@ItemWiseReport');
    $app->GET('ItemReport', 'ReportsController@ItemReport');
    $app->GET('PartyWiseSalesReport', 'ReportsController@PartyWiseSalesReport');
    $app->GET('PartyWisePurchaseReport', 'ReportsController@PartyWisePurchaseReport');
    $app->GET('discountReport', 'ReportsController@discountReport');
    $app->GET('PartyWisePurchaseReportSummary', 'ReportsController@PartyWisePurchaseReportSummary');
    $app->GET('GSTSales', 'ReportsController@GSTSales');
    $app->GET('GSTPurchase', 'ReportsController@GSTPurchase');
    $app->GET('CategoryWiseStockReport', 'ReportsController@CategoryWiseStockReport');
    $app->GET('CategoryWiseReport', 'ReportsController@CategoryWiseReport');
    $app->GET('BarcodeWiseReport', 'ReportsController@BarcodeWiseReport');
    $app->GET('SalesPaymentAccountReport', 'ReportsController@SalesPaymentAccountReport');
    $app->GET('ItemWiseAgeingReport', 'ReportsController@ItemWiseAgeingReport');
    $app->GET('CategoryWiseAgeingReport', 'ReportsController@CategoryWiseAgeingReport');
});

//Deal Activty
$app->group(['prefix' => 'dealactivity', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('/save', 'DealActivityController@save');
    $app->GET('/listAll', 'DealActivityController@listAll');
    $app->PUT('/update/{id:[\d]+}', 'DealActivityController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'DealActivityController@delete');
});

//Deal Stage
$app->group(['prefix' => 'dealstage', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->post('/save', 'DealStageController@save');
    $app->GET('/listAll', 'DealStageController@listAll');
    $app->PUT('/update/{id:[\d]+}', 'DealStageController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'DealStageController@delete');
});

//Deal 
$app->group(['prefix' => 'deal', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('/save', 'DealController@save');
    $app->GET('/listAll', 'DealController@listAll');
    $app->PUT('/update/{id:[\d]+}', 'DealController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'DealController@delete');
});

//Deal 
$app->group(['prefix' => 'general', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->post('/nextInvoiceNo', 'GeneralController@findCode');
    $app->get('/nextAutoNo', 'GeneralController@findAutoNo');
});

//Accounts
$app->group(['prefix' => 'accounts', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('/save', 'AccountController@save');
    $app->GET('/listAll', 'AccountController@listAll');
    $app->PUT('/update/{id:[\d]+}', 'AccountController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'AccountController@delete');
    $app->PUT('/accounthelper', 'AccountController@accounthelper');
    $app->GET('/checkAccountTransaction','AccountController@checkAccountTransaction');
});

//csv download api
$app->GET('/product/product_csvexport', 'ProductController@product_csvexport');
$app->Get('/customer/customerdetails_exportcsv', 'CustomerController@customerdetails_exportcsv');
$app->GET('/invoice/invoice_csvexport', 'InvoiceController@invoice_csvexport');
$app->GET('/invoice/invoicelist_csvexport', 'InvoiceController@invoicelist_csvexport');
$app->GET('/invoice/salesTaxReport_csvexport', 'InvoiceController@salesTaxReport_csvexport');
$app->Get('/invoice/salesTaxReportSummery_csvexport', 'InvoiceController@salesTaxReportSummery_csvexport');
$app->GET('/invoice/receivablereport_csvexport', 'InvoiceController@receivablereport_csvexport');
$app->GET('/transaction/accountstatement_csvexport', 'TransactionController@accountstatement_csvexport');
$app->GET('/transaction/ExpenseReport_csvexport', 'TransactionController@ExpenseReport_csvexport');
$app->GET('/reports/itemizedreport_csvexport', 'ReportsController@itemizedreport_csvexport');
$app->GET('/reports/PartyWiseSalesReport_csvexport', 'ReportsController@PartyWiseSalesReport_csvexport');
$app->GET('/reports/PartyWisePurchaseReport_csvexport', 'ReportsController@PartyWisePurchaseReport_csvexport');
$app->Get('/payablereport_csvexport', 'PurchaseinvoiceController@payablereport_csvexport');
$app->Get('/partywise_sales_csvexport', 'PurchaseinvoiceController@partywise_sales_csvexport');
$app->get('/bcnDownload', 'BcnController@bcnDownload');
$app->get('/reports/discountreport_csvexport','ReportsController@discountreport_csvexport');
$app->get('/purchaseinvoice/partywisesales_csvexport','PurchaseinvoiceController@partywisesales_csvexport');
$app->get('/reports/categorywisereport_csvexport','ReportsController@categorywisereport_csvexport');
$app->get('/reports/partywisepurchasesummary_csvexport','ReportsController@partywisepurchasesummary_csvexport');
$app->get('/reports/barcodewise_csvexport','ReportsController@barcodewise_csvexport');
$app->get('/reports/gstsales_csvexport','ReportsController@gstsales_csvexport');
$app->get('/reports/gstpurchase_csvexport','ReportsController@gstpurchase_csvexport');
$app->get('/reports/categorywisestock_csvexport','ReportsController@categorywisestock_csvexport');
$app->get('/reports/ItemWiseAgeingReport_csvexport','ReportsController@ItemWiseAgeingReport_csvexport');
$app->get('/reports/CategoryWiseAgeingReport_csvexport','ReportsController@CategoryWiseAgeingReport_csvexport');
$app->Get('/transaction/payableReceivablereport_csvexport', 'TransactionController@payableReceivaleReport_csvexport');
$app->Get('/deliveryNote/deliveryNoteReport_csvexport', 'DeliveryNoteController@productReport_csvexport');
$app->Get('/dayLog/dayLogReport_csvexport', 'DayLogController@dayCodeReport_csvexport');
 
//CLIENT

$app->post('/client/save', 'ClientController@save');
$app->put('client/update/{id:[\d]+}', 'ClientController@update');
$app->delete('/client/delete/{id:[\d]+}', 'ClientController@delete');
$app->get('/client/listWithLimit', 'ClientController@listWithLimit');
$app->get('/client/listAll', 'ClientController@listAll');
$app->get('/client/listByName', 'ClientController@listByName');
$app->get('/client/listById/{id:[\d]+}', 'ClientController@listById');

$app->post('uploadfile', 'FileuploadController@saveFile');
$app->post('uploadlogo', 'FileuploadController@saveLogo');
$app->post('uploadbackground', 'FileuploadController@saveBackground');

//TaxMapping
$app->group(['prefix' => 'taxMapping', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->POST('/save', 'TaxMappingController@save');
    $app->GET('/listAll', 'TaxMappingController@listAll');
    $app->POST('/delete', 'TaxMappingController@delete');
});

//ProductTierPricing
$app->group(['prefix' => 'productTierPricing', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->POST('/save', 'ProductTierPricingController@save');
    $app->GET('/listAll', 'ProductTierPricingController@listAll');
    $app->POST('/delete', 'ProductTierPricingController@delete');
});

//DayLog
$app->group(['prefix' => 'dayLog', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->POST('/save', 'DayLogController@save');
    $app->GET('/listAll', 'DayLogController@listAll');
    $app->GET('/getMaxDayCode', 'DayLogController@getMaxDayCode');
    $app->GET('/dayCodeReport', 'DayLogController@dayCodeReport');
});

//Acode
$app->group(['prefix' => 'acode', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('/save', 'AcodeController@save');
    $app->GET('/listAll', 'AcodeController@listAll');
    $app->PUT('/update/{id:[\d]+}', 'AcodeController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'AcodeController@delete');
    $app->GET('/search', 'AcodeController@search');
    $app->GET('/acodeReport', 'AcodeController@acodeReport');
});

//BCN
$app->group(['prefix' => 'bcn', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('/BcnLabeling', 'BcnController@BcnLabeling');
    $app->DELETE('/delete/{id:[\d]+}', 'BcnController@delete');
    $app->GET('/listAll', 'BcnController@listAll');
    $app->PUT('/generateBarCode/{id:[\d]+}', 'BcnController@generateBarCode');
});


//hsnCOde
$app->group(['prefix' => 'hsnCode', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('/save', 'HsnCodeController@save');
    $app->PUT('/update/{id:[\d]+}', 'HsnCodeController@update');
    $app->GET('/listAll', 'HsnCodeController@listAll');
    $app->DELETE('/delete/{id:[\d]+}', 'HsnCodeController@delete');
});

$app->group(['prefix' => 'journal', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('/save', 'JournalController@save');
    $app->GET('/listAll', 'JournalController@listAll');
    $app->PUT('/modify/{id:[\d]+}', 'JournalController@update');
    $app->get('/detail', 'JournalController@detail');
    $app->DELETE('/delete/{id:[\d]+}', 'JournalController@delete');
});

$app->group(['prefix' => 'tag', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('/save', 'Catalog\Tag\TagController@save');
    $app->GET('/list', 'Catalog\Tag\TagController@getList');
    $app->GET('/listAll', 'Catalog\Tag\TagController@getList');
    $app->GET('/detail', 'Catalog\Tag\TagController@detailtag');
    $app->GET('/list/account/{id:[\d]+}', 'Catalog\Tag\TagController@getListByAccountId');
    $app->GET('/find/name/{id}', 'Catalog\Tag\TagController@getListByname');
    $app->GET('/find/id/{id:[\d]+}', 'Catalog\Tag\TagController@getListByTagId');
    $app->GET('/list/tag/{id:[\d]+}', 'Catalog\Tag\TagController@findByItemTag');
    
});


$app->group(['prefix' => 'media', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('/save', 'Catalog\CatalogMedia\CatalogMediaController@save');
});

$app->group(['prefix' => 'media'], function (\Laravel\Lumen\Application $app) {

    $app->GET('/list', 'Catalog\CatalogMedia\CatalogMediaController@getList');
    $app->GET('/list/item/{id:[\d]+}', 'Catalog\CatalogMedia\CatalogMediaController@getListById');
});


$app->group(['prefix' => 'cataloguecategory', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('/save', 'Catalog\Category\CategoryController@save');
    $app->GET('/list', 'Catalog\Category\CategoryController@listAll');
    $app->GET('/list/account/{id:[\d]+}', 'Catalog\Category\CategoryController@listByAccount');
    $app->GET('/find/name/{id}', 'Catalog\Category\CategoryController@findByName');
    $app->GET('/find/id/{id:[\d]+}', 'Catalog\Category\CategoryController@findById');
    $app->GET('/list/map/{id:[\d]+}', 'Catalog\Category\CategoryController@findByItemCategory');
    $app->GET('/detail','Catalog\Category\CategoryController@detail');
    
});


$app->group(['prefix' => 'price', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

     $app->GET('/list', 'Catalog\Price\PriceController@listAll');
     $app->GET('/list/item/{itemId:[\d]+}', 'Catalog\Price\PriceController@listByAccount');
    $app->post('/save', 'Catalog\Price\PriceController@save');
    
});


$app->group(['prefix' => 'item', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('/save', 'Catalog\Item\ItemController@save');
    $app->GET('/list', 'Catalog\Item\ItemController@listAll');
    $app->GET('/list/account/{id:[\d]+}', 'Catalog\Item\ItemController@listByAccount');
    $app->GET('/list/category/{id:[\d]+}', 'Catalog\Item\ItemController@listByCategory');
    
    $app->GET('/find/name/{id}', 'Catalog\Item\ItemController@findByName');
    $app->GET('/find/id/{id:[\d]+}', 'Catalog\Item\ItemController@findById');
    $app->GET('/find/code/{id}', 'Catalog\Item\ItemController@findByCode');
    $app->GET('/listAllItem', 'Catalog\Item\ItemController@listAllitem');
    
});

$app->group([ 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->get('/sync', 'Catalog\Sync\SyncController@sync');
     
});

$app->group(['prefix' => 'purchaseReturn', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->Post('/save', 'PurchaseinvoiceReturnController@save');
    $app->get('/listAll', 'PurchaseinvoiceReturnController@listAll');
    $app->delete('/delete/{id}', 'PurchaseinvoiceReturnController@delete');
    $app->put('/update/{id}', 'PurchaseinvoiceReturnController@update');
    $app->get('/detail', 'PurchaseinvoiceReturnController@detail');
    $app->GET('/purchaseReturnPrint/{id:[\d]+}', 'PurchaseinvoiceReturnController@purchaseReturnPrint');
    $app->GET('/pdfPreview/{id:[\d]+}', 'PurchaseinvoiceReturnController@pdfPreview');
    
});


//InventoryManagement
$app->group(['prefix' => 'inventoryAdjust', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->Post('/save', 'InventoryManagementController@save');
    $app->Put('/update/{id}', 'InventoryManagementController@update');
    $app->Delete('/delete/{id}', 'InventoryManagementController@delete');
    $app->get('/detail', 'InventoryManagementController@detail');
    $app->GET('/listAll', 'InventoryManagementController@listAll');
    $app->Delete('/stockItemDelete/{id}', 'InventoryManagementController@StockItemDelete');
    $app->GET('/categoryWiseAdjustmentReport', 'InventoryManagementController@categoryWiseStockAdjustmentReport');
    $app->GET('/StockAdjustmentReport_csvexport', 'InventoryManagementController@StockAdjustmentReport_csvexport');
    $app->GET('/CategoryStockAdjustmentReport_csvexport', 'InventoryManagementController@CategoryStockAdjustmentReport_csvexport');
});

//Employee
$app->group(['prefix' => 'employee', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->Post('/save', 'EmployeeController@save');
    $app->Get('/listAll', 'EmployeeController@listAll');
    $app->Get('/detail', 'EmployeeController@detail');
    $app->PUT('/modify/{id:[\d]+}', 'EmployeeController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'EmployeeController@delete');
    $app->Get('/employeeSalesReport', 'EmployeeController@employeeSalesReport');
    $app->GET('/EmployeeSalesReport_csvexport', 'EmployeeController@EmployeeSalesReport_csvexport');
});

//Designation
$app->group(['prefix' => 'designation', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->Post('/save', 'DesignationController@save');
    $app->Get('/listAll', 'DesignationController@listAll');
    $app->PUT('/modify/{id:[\d]+}', 'DesignationController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'DesignationController@delete');
    
});

//Designation
$app->group(['prefix' => 'notification', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {   
    $app->PUT('/update/{id:[\d]+}', 'NotificationController@update');
    $app->GET('/listAll', 'NotificationController@listAll');
    $app->GET('/count', 'NotificationController@count');
});

//DeliveryNote
$app->group(['prefix' => 'deliveryNote', 'middleWare' => 'auth'], function (Laravel\Lumen\Application $app) {

    $app->Post('/save', 'DeliveryNoteController@save');
    $app->Get('/listAll', 'DeliveryNoteController@listAll');
    $app->PUT('/update/{id:[\d]+}', 'DeliveryNoteController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'DeliveryNoteController@delete');
    $app->get('/detail', 'DeliveryNoteController@detail');
    $app->post('/qtyUpdate', 'DeliveryNoteController@qtyUpdate');
    $app->get('/productReport', 'DeliveryNoteController@productReport');
    $app->get('/balanceQty', 'DeliveryNoteController@balanceQty');
});

$app->group(['prefix' => 'hsnCode', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('/save', 'HsnCodeController@save');
    $app->PUT('/update/{id:[\d]+}', 'HsnCodeController@update');
    $app->GET('/listAll', 'HsnCodeController@listAll');
    $app->DELETE('/delete/{id:[\d]+}', 'HsnCodeController@delete');
    $app->GET('/detail', 'HsnCodeController@detail');
    $app->GET('/hsnTaxDetail', 'HsnCodeController@hsnTaxDetail');
    $app->GET('gettingTaxId','HsnCodeController@gettingTaxId');
});

$app->Post('/dynamicQuery', 'DynamicQueryController@dynamicQuery');