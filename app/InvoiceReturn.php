<?php
namespace app;
use Illuminate\Database\Eloquent\Model;


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of invoice
 *
 * @author Deepa
 */
class InvoiceReturn extends Model{
    //put your code here
    protected $table ='tbl_invoice_return';
   
    protected $fillable = ['invoice_id','customer_id','date','subtotal','discount_amount',
        'tax_id','tax_amount','total_amount','status','notes','is_active','customer_address','round_off','created_at','updated_at','created_by','updated_by',
        'day_code'];
    protected $dates = [
        'created_at',
        'updated_at',
        'date',
     ];
}

?>
