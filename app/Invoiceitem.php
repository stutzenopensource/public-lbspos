<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Invoiceitems
 *
 * @author Deepa
 */
class Invoiceitem extends Model {
    //put your code here
    protected  $table ='tbl_invoice_item';
    protected  $fillable =['hsn_code','invoice_id','customer_id','product_id','product_name','product_sku','qty','unit_price',
                           'tax_amount','total_price', 'created_at','gross_price','duedate','paymentmethod','notes','emp_id','emp_code',
                            'uom_id','uom_name','is_active','created_by','updated_at','updated_by','custom_opt1'
                            ,'custom_opt2','custom_opt3','custom_opt4','custom_opt5','purchase_invoice_item_id'
                            ,'tax_id','tax_percentage','discount_mode','discount_value','discount_amount','sales_value','comments','mrp_price'
        ,'custom_opt1_key','custom_opt2_key','custom_opt3_key','custom_opt4_key','custom_opt5_key'];
                            
}

?>
