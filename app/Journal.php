<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Journal extends Model {

    protected $table = 'tbl_journal';

    protected $fillable = ['date','particular', 'created_by', 'updated_by', 'is_active', 'comments'];
    
    protected $dates = ['created_at','updated_at'];


}
