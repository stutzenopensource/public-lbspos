<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JournalDetail extends Model {

    protected $table = 'tbl_journal_detail';

    protected $fillable = ['journal_id','ref_id','ref_type','acode','amount_type','debit','credit',
        'created_by', 'updated_by', 'is_active', 'comments'];
    
    protected $dates = ['created_at','updated_at'];


}
