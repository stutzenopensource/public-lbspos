<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Purchaseinvoiceitem
 *
 * @author Deepa
 */
class Media extends Model  {
    //put your code here
     protected  $table ='tbl_media';
    protected  $fillable =['path','media_type','media_type_id' 
                            ,'is_active','created_by','updated_at','updated_by'  ];
    protected $dates = ['updated_at','created_at'];
}

?>
