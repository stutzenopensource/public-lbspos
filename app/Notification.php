<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model {

    protected $table = 'tbl_notification';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description', 'name', 'type', 'ip_address','notification_type','to_display','method','url',
        'read_or_not', 'redirect_url', 'ref_id', 'ref_name', 'screen_code','user_id','user_agent','request','created_by_name',
        'created_by', 'updated_by', 'is_active'   
    ];
    protected $dates = [
        'created_at',
        'updated_at'
        ];

//    /**
//     * The attributes excluded from the model's JSON form.
//     *
//     * @var array
//     */
//    protected $hidden = [
//        
//    ];
}
