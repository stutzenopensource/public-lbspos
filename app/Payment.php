<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Payment
 *
 * @author Deepa
 */
class Payment extends Model {
    //put your code here
    protected $table = 'tbl_payment';
    protected $fillable = ['created_by','updated_by','created_at','updated_at','is_active','customer_id','comments'
                            ,'is_settled','date','amount','invoice_id','quote_id','payment_mode','cheque_no','status',
            'tra_category','account','account_id','sales_order_id','day_code','adv_payment_id'];
    protected $dates = ['created_at', 'updated_at','date'];
        
       
        
    
}

?>
