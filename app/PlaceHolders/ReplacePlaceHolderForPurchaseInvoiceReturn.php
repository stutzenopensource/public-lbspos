<?php

namespace App\PlaceHolders;

use DB;
use App\Helper\ReplaceHolderHelper;

class ReplacePlaceHolderForPurchaseInvoiceReturn extends ReplacePlaceHolder {

    function getPlaceHolderValue($no, $check_print_pdf, $thous_sep, $from_date, $to_date) {
        $replace_array = array();
        $result = DB::table('tbl_app_config')
                        ->where('setting', 'app_title')->first();

        $companyName = DB::table('tbl_app_config')
                        ->where('setting', 'companyname_print')->first();
        $state_code = DB::table('tbl_app_config')->where('setting','=','gst_state_name')->first();
        if ($state_code != null ) {
          $replace_array['company_state']=$state_code->value;
        }
        if ($result != null)
           $replace_array['title'] = $result->value;

        if ($companyName != null)
         $replace_array['companyname'] = $companyName->value;
        $bank = '';
        $addr = '';
        $sign = '';
        $cust_gst_no='';
        $gst_no='';
        $result = DB::table('tbl_app_config')
                        ->where('setting', 'customer_gst_no')->first();
        if ($result != null)
            $cust_gst_no = $result->value;
        $replace_array['cust_gst_no'] = $cust_gst_no;

        $result = DB::table('tbl_app_config')
                        ->where('setting', 'pay_to_address')->first();
        if ($result != null)
            $addr = $result->value;

        $result = DB::table('tbl_app_config')
                        ->where('setting', 'bank_detail')->first();
        if ($result != null)
            $bank = $result->value;

        $replace_array['bank'] = $bank;

        $result = DB::table('tbl_app_config')
                        ->where('setting', 'sign')->first();
        if ($result != null)
            $sign = $result->value;

        $replace_array['sign'] = $sign;

        $array = explode("\n", $addr);
        $pay_det = "";
        for ($i = 0; $i < count($array); $i++) {
            $pay_det = $pay_det . "<p> " . $array[$i] . "</p>";
        }

        $replace_array['address'] = $pay_det;
        $configureDateFormat = ReplaceHolderHelper::getDateFormat();

        $builder = DB::table('tbl_purchase_invoice_return as mi')
                ->select('mi.*', 't.tax_name', 't.tax_percentage')
                ->leftjoin('tbl_tax as t', 'mi.tax_id', '=', 't.id')
                ->where('mi.is_active', '=', 1)
                ->where('mi.id', '=', $no)
                ->get();

        foreach ($builder as $row) {
            $customer_id = $row->customer_id;
            $invoice_no = $row->purchase_no;
            $subtotal = $row->subtotal;
            $discount = $row->discount_amount;
            $over_tax = $row->tax_amount;
            $round = $row->round_off;
            $total_amount = $row->total_amount;
            $invoice_code = $row->purchase_code;
            $taxName = $row->tax_name;
            $taxPercen = $row->tax_percentage;
            $duedate = $row->duedate;
            $paid = $row->paid_amount;
            $date = ReplaceHolderHelper::formatDate($row->date, $configureDateFormat);
            $duedate = ReplaceHolderHelper::formatDate($row->duedate, $configureDateFormat);
            $balance = $total_amount - $paid;
             $balance = sprintf('%0.2f', $balance); // 520 -> 520.00

            $word = ReplaceHolderHelper::numberTowords($total_amount);
        }

        $customer = DB::table('tbl_customer')
                        ->where('is_active', '=', 1)
                        ->where('id', '=', $customer_id)->get();

        foreach ($customer as $cus) {
            $cust_name = $cus->fname . ' ' . $cus->lname;
            $cust_email = $cus->email;
            $cust_billing_addr = $cus->billing_address;
            $gst_no = $cus->gst_no;
            $cust_phone = $cus->phone;
            // $state = $cus->billing_state;
            $state = $cus->state;
            $replace_array['customer_state'] = $state;
            $replace_array['gst_no']= $gst_no;

            $cust_state_code=DB::table('tbl_state')->where('id',$cus->billing_state_id)->first();
            if($cust_state_code != null)
              $replace_array['customer_state_code']=$cust_state_code->code;
            }



        $product = DB::table('tbl_purchase_invoice_item_return as pr')
                ->select('pr.*','p.hsn_code')
                ->leftjoin('tbl_product as p','p.id','=','pr.product_id')
                ->where('pr.is_active', '=', 1)
                ->where('pr.purchase_invoice_return_id', '=', $no)->get();

        $replace_array['product_detail'] = array();
        $product_det = array();
        $count = 1;
        $total_qty = 0;
        foreach ($product as $value) {
            $prod_name = $value->product_name;
            $qty = $value->qty;
            $unit_price = $value->mrp_price;
            $uom = $value->uom_name;
            $total = $value->purchase_price;
            $hsn_code=$value->hsn_code;
            $total_qty = $total_qty + $qty;

            $product_det['product'] = $prod_name;
            $product_det['slno'] = $count;
            $product_det['qty'] = $qty;
            $product_det['price'] = $unit_price;
            $product_det['hsn_code'] = $hsn_code;
            $product_det['uom'] = $uom;
            $product_det['row_total'] = $total;
            array_push($replace_array['product_detail'], $product_det);
            $count = $count + 1;
        }
        $currency = ReplaceHolderHelper::getCurrencySymbol();

        $replace_array['tax_detail'] = array();


        $replace_array['date'] = $date;
        $replace_array['invoice_no'] = $invoice_no;
        $replace_array['invoice_code'] = $invoice_code;
        $replace_array['cus_name'] = $cust_name;
        $replace_array['email'] = $cust_name;
        $replace_array['billing'] = $cust_billing_addr;
        $replace_array['phone'] = $cust_phone;
        $replace_array['shipping'] = $cust_billing_addr;
        $replace_array['subtotal'] = $subtotal;
        $replace_array['discount'] = $discount;
        $replace_array['taxable_value'] = $over_tax;
        $replace_array['duedate'] = $duedate;
        $replace_array['round_off'] = $round;
        $replace_array['total_qty'] = $total_qty;
        $replace_array['grand'] = $total_amount;
        $replace_array['amount_word'] = $word;
        $replace_array['currency'] = $currency;
        $replace_array['paid'] = $paid;
        $replace_array['balance'] = $balance;
        $replace_array['tax_percen_design'] = '';
        $replace_array['tax_percen_detail'] = '';
        return $replace_array;
    }

    function getDesign($check_print_pdf) {
        $result = DB::table('tbl_email_templates')
                        ->where('tplname', 'invoice_print_template4')->first();
        if ($result != null) {
            $msg = $result->message;
        }

        $msg = preg_replace("/<!--pdf-hidden-start-->.+?<!--pdf-hidden-end-->/i", "", $msg);



        return $msg;
    }

    function replacePlaceHolderValue($replace_array, $msg) {
        $count = count($replace_array['product_detail']);
        $count1 = count($replace_array['tax_detail']);
        foreach ($replace_array as $key => $value) {
            if (gettype($replace_array[$key]) == 'array') {

                if ($key == 'product_detail') {
                    $product_det = "";
                    foreach ($replace_array[$key] as $prod_key => $prod_value) {
                        $extract_string = ReplaceHolderHelper::extractString($msg, '<!--product-row-start-->', '<!--product-row-end-->');

                        $copy_string = $extract_string;
                        foreach ($prod_value as $prod1_key => $prod1_value) {
                            $extract_string = str_replace("{{" . $prod1_key . "}}", $prod1_value, $extract_string);
                        }
                        $product_det .= $extract_string;
                        $extract_string = $copy_string;
                    }

                    $msg = str_replace("<!--product-row-start-->$extract_string<!--product-row-end-->", $product_det, $msg);
                } else if ($key == 'tax_detail') {
                    $tax_det = "";
                    $extract_string = ReplaceHolderHelper::extractString($msg, '<!--tax-row-start-->', '<!--tax-row-end-->');
                    if ($replace_array[$key] != '') {
                        foreach ($replace_array[$key] as $prod_key => $prod_value) {
                            $copy_string = $extract_string;
                            foreach ($prod_value as $prod1_key => $prod1_value) {
                                $extract_string = str_replace("{{" . $prod1_key . "}}", $prod1_value, $extract_string);
                            }
                            $tax_det .= $extract_string;
                            $extract_string = $copy_string;
                        }
                        $msg = str_replace("<!--tax-row-start-->$extract_string<!--tax-row-end-->", $tax_det, $msg);
                    } else {
                        $msg = str_replace("<!--tax-row-start-->$extract_string<!--tax-row-end-->", '', $msg);
                    }
                }
            } else {
                $msg = str_replace("{{" . $key . "}}", $value, $msg);
            }
        }
        return $msg;
    }

}

?>
