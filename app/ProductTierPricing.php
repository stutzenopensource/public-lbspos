<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductTierPricing
 *
 * @author Stutzen Admin
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
class ProductTierPricing extends Model {

    protected $table = 'tbl_product_tier_pricing';

  
    protected $fillable = [
        'product_id', 'product_name', 'category_id', 'uom_id', 'uom', 'from_qty', 'to_qty', 
        'unit_price',  'created_by', 'updated_by', 'is_active', 'comments'];
    
    protected $dates = [ 'created_at', 'updated_at' ];


}

