<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AccountTransformer
 *
 * @author Stutzen Admin
 */
namespace App\Transformer;
use App\Account;
use League\Fractal\TransformerAbstract;
class AccountTransformer extends TransformerAbstract{
    
     public function transform(Account $account){
       return[
          'id'=>$account->id,
           'particulars'=>$account->particulars,
           'accCategory'=>$account->acc_category,
           'debit'=>$account->debit,
           'credit'=>$account->credit,
           'balance'=>$account->balance,
           'sourceType'=>$account->source_type,
           'sourceId'=>$account->source_id,
           'createdBy'=>$account->created_by,
           'updatedBy'=>$account->updated_by,
           'createdOn'=>$account->created_at->toIso8601String(),
           'lastmodifiedon'=>$account->updated_at->toIso8601String(),
           'isActive'=>$account->is_active,
           'date'=>$account->date->toIso8601String(),
           'companyId'=>$account->comp_id,
           'clientId'=>$account->client_id,
           'subCategory'=>$account->sub_category,
       
       ];
       
   }
}
