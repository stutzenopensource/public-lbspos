<?php

namespace App\Transformer;

use App\Client;
use League\Fractal\TransformerAbstract;
class ClientTransformer extends TransformerAbstract{
    
   
    
   public function transform(Client $client){
       return[
          'id'=>$client->id,
           'name'=>$client->name,
           'createdBy'=>$client->created_by,
           'updatedBy'=>$client->updated_by,
           'createdOn'=>$client->created_at->toIso8601String(),
           'lastmodifiedon'=>$client->updated_at->toIso8601String(),
           'email'=>$client->email,
           'phoneNumber'=>$client->ph_no,
           'city'=>$client->city,
           'state'=>$client->state,
           'country'=>$client->country,
           'postCode'=>$client->post_code,
           'isactive'=>$client->isactive,
           'companyId'=>$client->comp_id,
           'balanceAmount'=>$client->balance_amount,
           'contactPersonName'=>$client->contactpersonname,
           'contactPersonRole'=>$client->contactpersonrole,
           'categoryId'=>$client->category_id,
           'categoryName'=>$client->category_name,
           'imagepath'=>$client->imagepath,
           'familyCode'=>$client->family_code,
           'customerOf'=>$client->customer_of,
           'commission'=>$client->commission,
           'familyCode'=>$client->family_code,
           'accountNumber'=>$client->id,
            'address'=>$client->address,
            'companyName'=>$client->Comp->name,
           'com'=>$client->Comp->id,
       ];
       
   }
}
