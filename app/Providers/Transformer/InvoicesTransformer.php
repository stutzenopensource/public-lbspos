<?php
namespace App\Transformer;
use App\Invoices;
use League\Fractal\TransformerAbstract;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of InvoicesTransformer
 *
 * @author Deepa
 */
class InvoicesTransformer extends TransformerAbstract {
    //put your code here
    public function tranform(Invoice $invoice){
         return [
            'Userid' => $invoice->userid,
            'Account' => $invoice->account,
            'cn' => $invoice->cn,
            'invoicenum' => $invoice->invoicenum,
            'date' => $invoice->date,
            'duedate' => $invoice->duedate,
            'datepaid' => $invoice->datepaid,
            'subtotal' => $invoice->subtotal,
            'discount_type' => $invoice->discount_type,
            'discount_value' => $invoice->discount_value,
            'discount' => $invoice->discount,
            'credit' => $invoice->credit,
            'tax' => $invoice->tax,
            'taxname' => $invoice->taxname,
            'tax2' => $invoice->tax2,
            'total' => $invoice->total,
            'taxrate' => $invoice->taxrate,
            'taxrate2' => $invoice->taxrate2,
            'status' => $invoice->status,
            'paymentmethod' => $invoice->paymentmethod,
            'notes' => $invoice->notes,
            'vtoken' => $invoice->vtoken,
            'ptoken' => $invoice->ptoken,
            'r' => $invoice->r,
            'nd' => $invoice->nd,
            'eid' => $invoice->eid,
            'ename' => $invoice->ename,
            'created_at' => strtotime($invoice->created_at->toIso8601String()),
            'updated_at' => strtotime($invoice->updated_at->toIso8601String()),
           
            
        ];
        
    }
}

?>
