<?php

namespace App\Transformer;

use App\Sponsor;
use League\Fractal\TransformerAbstract;

class SponsorTransformer extends TransformerAbstract
{
    /**
     * Transform a Book model into an array
     *
     * @param Book $book
     * @return array
     */
    public function transform(Sponsor $sponsor)
    {
        return [
            'id'          => $sponsor->id,
            'name'       => $sponsor->name,
            'description' => $sponsor->description,
            'ph_no'      => $sponsor->ph_no
        ];
    }
}
