<?php
namespace App\Transformer;
use App\Uom;
use League\Fractal\TransformerAbstract;


class UomTransformer extends TransformerAbstract {
    //put your code here

    public function transform(Item $item){
          return[
          'id'=>$client->id,
           'name'=>$client->name,
           'createdBy'=>$client->created_by,
           'updatedBy'=>$client->updated_by,
           'Description'=>$client->description,
           
           'isactive'=>$client->is_active,
          
       ];
    }
    public function listAll(Request $request) {
        $currentuser = Auth::user();
       
        $id = $request->input('id');
        $name = $request->input('name', '');
        //$status = $request->input('status', '');
        $builder = DB::table('sys_uom')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $request->input('id'));
        }
         
        if (!empty($name)) {
            $builder->where('name', 'like','%'.$name .'%');
            DB::connection()->enableQueryLog();
            var_dump(DB::getQueryLog());
        }
        $resVal['total'] = $builder->count();
        $resVal['list'] = $builder->skip($start)->take($limit)->get();
        return ($resVal);
    }
}

?>
