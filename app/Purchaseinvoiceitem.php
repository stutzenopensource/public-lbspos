<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Purchaseinvoiceitem
 *
 * @author Deepa
 */
class Purchaseinvoiceitem extends Model  {
    //put your code here
     protected  $table ='tbl_purchase_invoice_item';
    protected  $fillable =['code','hsn_code','purchase_invoice_id','customer_id','product_id','product_name','product_sku','qty','selling_price',
                           'tax_amount','total_price', 'created_at','gross_price','duedate','paymentmethod','notes',
                            'uom_id','uom_name','is_active','created_by','updated_at','updated_by','custom_opt1'
                            ,'custom_opt2','custom_opt3','custom_opt4','custom_opt5','qty_in_hand','sales_qty','purchase_price',
                            'mrp_price','purchase_price_w_tax','tax_id','tax_percentage','selling_tax_id','selling_tax_percentage','discount_mode','discount_value','discount_amount'
                            ,'purchase_value','is_barcode_generated','custom_opt1_key'
                            ,'custom_opt2_key','custom_opt3_key','custom_opt4_key','custom_opt5_key'];
    protected $dates = ['updated_at','created_at','duedate'];
}

?>
