<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tax
 *
 * @author Deepa
 */
class SalesOrderItemTax extends Model {

    //put your code here
    protected $table = 'tbl_sales_order_item_tax';
    protected $fillable = ['sales_order_id','sales_order_item_id', 'tax_id', 'tax_name',  'tax_percentage',
        'product_amt', 'tax_applicable_amt', 'tax_amount',  'is_active'];
    protected $dates = ['created_at', 'updated_at'];

}

?>
