<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Screen
 *
 * @author Deepa
 */
class Screen extends Model {

    //put your code here
    protected $table ='tbl_screen';
    protected  $fillable =['name','comments','sno','module_name','section_name','section_sqno','is_active','created_by','updated_by'];
    protected $dates = ['created_at','updated_at'];
}

?>
