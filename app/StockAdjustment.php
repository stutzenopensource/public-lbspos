<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class StockAdjustment extends Model {
     protected $table = 'tbl_stock_adjustment';
     protected $fillable = ['user_id','reason','date','type','auto_no','reason_id','user_name','overall_qty','overall_mrp_price','overall_purchase_price','overall_sales_price','is_active','created_by','updated_by','comments','emp_id','emp_name'];
     protected $dates = ['created_at','updated_at'];
    //put your code here
}

?>
