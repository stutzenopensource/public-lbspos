<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class StockAdjustmentDetail extends Model {
     protected $table = 'tbl_stock_adjustment_detail';
     protected $fillable = ['product_id','product_sku','uom_id','uom_name','product_name','sales_price','barcode','purchase_price','stock_adjustment_id','purchase_invoice_id','purchase_invoice_item_id','mrp_price','qty','total_price','is_active','created_by','updated_by','comments'];
     protected $dates = ['created_at','updated_at'];
    //put your code here
}

?>
