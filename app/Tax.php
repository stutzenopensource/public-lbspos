<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tax
 *
 * @author Deepa
 */
class Tax extends Model {
    //put your code here
    protected $table = 'tbl_tax';
    protected $fillable = ['tax_no' , 'tax_name','tax_percentage', 'tax_applicable_amt','is_group', 'group_tax_ids','intra_group_tax_ids','tax_type', 'start_date', 
                            'end_date','created_on','lastmodifiedon','sso_tax_id','is_active','comments','created_by','updated_by','is_display','hsn_flag'];
    protected $dates = ['start_date','end_date','created_at','updated_at' ];
    
}

?>
