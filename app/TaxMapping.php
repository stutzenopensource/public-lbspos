<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tax
 *
 * @author Stutzen Admin
 */
class TaxMapping extends Model {
    protected $table = 'tbl_tax_mapping';
    protected $fillable = ['tax_id','product_id','is_active','comments','created_by','updated_by'];
    protected $dates = ['created_at','updated_at'];
    
}

?>
