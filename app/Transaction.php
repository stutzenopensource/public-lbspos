<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Transaction
 *
 * @author Deepa
 */
class Transaction extends Model {
    //put your code here
     protected $table = 'tbl_transaction' ;
     protected $fillable = ['name', 'fiscal_year_id', 'fiscal_year','transaction_date','voucher_type','voucher_number','credit'
         ,'debit','customer_id','account','account_id','particulars','created_by','updated_by','account_category','created_by_name','updated_by_name'
         ,'fiscal_year_code','payment_mode','fiscal_month_code','is_active','pmtcode','pmtcoderef','is_cash_flow','acode','acoderef'];
     protected $dates = ['created_at', 'updated_at'];
    public static $SALES = 'sales_invoice'; 
    public static $PURCHASE = 'purchase_invoice';
    public static $SALES_PAYMENT = 'sales_invoice_payment';
    public static $PURCHASE_PAYMENT = 'purchase_invoice_payment';
    
    public static $DIRECT_INCOME = 'direct_income';
    public static $DIRECT_EXPENSE = 'direct_expense';
  
     
}

?>
