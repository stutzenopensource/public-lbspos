<?php

class ReplacePlaceHolderForSales extends ReplacePlaceHolder {
    function getPlaceHolderValue($no, $con, $check_print_pdf, $thous_sep) {
        $replace_array = array();

        $bank = '';
        $addr = '';
        $result = mysqli_query($con, "select * from tbl_app_config where setting = 'bank_detail' ");
        while ($row = mysqli_fetch_array($result))
            $bank = $row['value'];

        $result = mysqli_query($con, "select * from tbl_app_config where setting = 'pay_to_address'");
        while ($row = mysqli_fetch_array($result))
            $addr = $row['value'];
        $array = explode("\n", $addr);
        
        $result = mysqli_query($con, "select * from tbl_app_config where setting = 'app_title' ");
        while ($row = mysqli_fetch_array($result))
            $companyname = $row['value'];
        
        $result = mysqli_query($con, "select * from tbl_app_config where setting = 'app_title' ");
        while ($row = mysqli_fetch_array($result))
            $title = $row['value'];
        
        $pay_det = "";
        for ($i = 0; $i < count($array); $i++) {
            $pay_det = $pay_det . "<p> " . $array[$i] . "</p>";
        }
        $array = explode("\n", $bank);
        $pank_det = "";
        for ($i = 0; $i < count($array); $i++) {
            $pank_det = $pank_det . "<p> " . $array[$i] . "</p>";
        }

        $result = mysqli_query($con, "select * from tbl_sales_order where id = '" . $no . "'");
        $configureDateFormat = $this->getDateFormat($con);
        $thous_sep = $this->getThosandSeperatorFormat($con);
        $currency = $this->getCurrencySymbol($con);

        while ($row = mysqli_fetch_array($result)) {
            $customer_id = $row['customer_id'];
            $replace_array['customer_id'] = $customer_id;
            $date = $this->formatDate($row['date'], $configureDateFormat);
            $duedate = $this->formatDate($row['date_accepted'], $configureDateFormat);
            $total_amount = $row['total_amount'];
            $new_tota1 = $row['total_amount'];
            $subtotal = $row['subtotal'];
            $cus_addr = $row['customer_address'];
            $tax = $row['tax_amount'];
            $packing = $row['packing_charge'];
            $insurance = $row['insurance_charge'];
            $discount = $row['discount_amount'];
            $new_total_amount = ( $subtotal + $tax + $packing + $insurance) - $discount;
            $discount = $this->convertNumberFormat($discount, $thous_sep);
            //$paid_amount = $row['credit_amount'];

            $result1 = mysqli_query($con, "SELECT SUM(amount) AS value_sum FROM tbl_adv_payment where voucher_no='" . $no . "' and voucher_type='advance_payment'");
            $row1 = mysqli_fetch_array($result1);
            $paid_amount = $row1['value_sum'];

            $invoice_no = $row['sales_order_no'];
            $no1 = $row['sales_order_code'];
            $status = $row['status'];
            $balance = $total_amount - $paid_amount;
            $paid_amount = $this->convertNumberFormat($paid_amount, $thous_sep);
            $tax = $this->convertNumberFormat($tax, $thous_sep);
            $payee_id = $row['payee'];
            $consignee_id = $row['consignee'];
        }
        $payee = '-';
        $consignee = '-';
        $result = mysqli_query($con, "select * from tbl_customer where id = " . $payee_id);
        while ($row = mysqli_fetch_array($result)) {
            $payee = $row['fname'] . "  " . $row['lname'];
        }
        $result = mysqli_query($con, "select * from tbl_customer where id = " . $consignee_id);
        while ($row = mysqli_fetch_array($result)) {
            $consignee = $row['fname'] . "  " . $row['lname'];
        }
        $result = mysqli_query($con, "select * from tbl_customer where id = " . $customer_id);
        while ($row = mysqli_fetch_array($result)) {
            $cus_name = $row['fname'] . "  " . $row['lname'] . " " . " ,<p> " . $cus_addr;
            $cus_phone = $row['phone'];
            $cus_email = $row['email'];
        }
        // $result = mysqli_query($con, "select * from tbl_tax as t left Join tbl_invoice_tax as i on  t.id = i.tax_id where i.invoice_id  = " . $no);
        $tax_detail = "";
        $replace_array['tax_detail'] = array();
        /* while ($inv_tax = mysqli_fetch_array($result)) {
          $tax_name = $inv_tax['tax_name'];
          $tax_amount = $inv_tax['tax_amount'];
          $tax_percentage = $inv_tax['tax_percentage'];
          $tax_amount = $this->convertNumberFormat($tax_amount, $thous_sep);
          $tax_detail = $tax_name . "&&" . $tax_amount . "&&" . $tax_percentage;
          array_push($replace_array['tax_detail'], $tax_detail);
          } */
        $replace_array['total_tax'] = $tax;

        $domain = $_SERVER['SERVER_NAME'];
        $img_loc = "http:\\\\" . $domain . "\public\img\logo\\" . $domain . '.png';
        //echo $img_loc; 
        $img = "<img src=" . $img_loc . "   style=width:100px;height:50px;>";

        $replace_array['invoice_no'] = $no1;
        $replace_array['no'] = $no;
        $replace_array['payee'] = $payee;
        $replace_array['consignee'] = $consignee;
        $replace_array['packing'] = $packing;
        $replace_array['insurance'] = $insurance;
        $replace_array['currency'] = $currency;
        $replace_array['date'] = $date;
        $replace_array['duedate'] = $duedate;
        $replace_array['address'] = $pay_det;
        $replace_array['bank'] = $pank_det;
        $replace_array['cus_name'] = $cus_name;
        $replace_array['phone'] = $cus_phone;
        $replace_array['email'] = $cus_email;
        $replace_array['product_detail_for_hsn'] ="";
        $replace_array['companyname'] = $companyname;
        $replace_array['title'] = $title;

        $result = mysqli_query($con, "select * from tbl_sales_order as i left Join tbl_sales_order_item as c on  i.id = c.sales_order_id where i.id  = " . $no." and c.is_active=1");
        $product_det = "";
        $totalQty = 0;
        $replace_array['product_detail'] = array();
        $balance = number_format((float) $balance, 2, '.', '');
        while ($row = mysqli_fetch_array($result)) {
            $pro_id = $row['product_id'];
            $qty = $row['qty'];
            $prod = $row['product_name'];
            $price = $row['unit_price'];
            $pro_amt = $price * $qty;
            $totalQty += $qty;
            $price = number_format((float) $price, 2, '.', '');
            $pro_amt = number_format((float) $pro_amt, 2, '.', '');
            $price = number_format((float) $price, 2, '.', '');
            $price = $this->convertNumberFormat($price, $thous_sep);
            $pro_amt = $this->convertNumberFormat($pro_amt, $thous_sep);

            $product_det['product'] = $prod;
            $product_det['qty'] = $qty;
            $product_det['price'] = $price;
            $product_det['amount'] = $pro_amt;
            array_push($replace_array['product_detail'], $product_det);
        }
        $subtotal = $this->convertNumberFormat($subtotal, $thous_sep);
        $replace_array['subtotal'] = $subtotal;
        $round1 = ceil($total_amount);

        $round = number_format((float) $round1, 2, '.', '');
        $reminder = $round - $new_total_amount;
        $reminder = number_format((float) $reminder, 2, '.', '');
        $replace_array['round_off'] = $reminder;
        $total_amount = $round;
        $subtotal = $this->convertNumberFormat($subtotal, $thous_sep);
        $word = $this->convertNumberToWord($total_amount);
        $balance = $this->convertNumberFormat($balance, $thous_sep);
        $total_amount = $this->convertNumberFormat($total_amount, $thous_sep);


        if ($status == 'paid')
            $status = "Paid";
        else if ($status == 'partiallyPaid')
            $status = "Partially Paid";
        else if ($status == 'unpaid')
            $status = "Un Paid";

        $replace_array['discount'] = $discount;
        $replace_array['paid'] = $paid_amount;
        $replace_array['grand'] = $total_amount;
        $replace_array['balance'] = $balance;
        $replace_array['amount_word'] = $word;



        $replace_array['amount'] = $total_amount;
        $sign = $this->setSignature($con);
        $replace_array['sign'] = $sign;
        $replace_array['logo'] = $img;
        $pay_det = "";
        $replace_array['payment_det'] = array();
        $result2 = mysqli_query($con, "select * from tbl_adv_payment where voucher_no ='" . $no . "' and voucher_type='advance_payment'");
        while ($inv = mysqli_fetch_array($result2)) {
            $pay_date = $inv['date'];
            $pay_date = date("Y-m-d", strtotime($pay_date));
            $pay_amount = $inv['amount'];
            $pay_mode = $inv['account'];
            $pay_date = $this->formatDate($inv['date'], $configureDateFormat);
            $pay_amount = $inv['amount'];
            $pay_amount = $this->convertNumberFormat($pay_amount, $thous_sep);
            $pay_type = $inv['payment_mode'];

            $pay_det['pay_type'] = $pay_type;
            $pay_det['pay_amount'] = $pay_amount;
            $pay_det['pay_date'] = $pay_date;
            $pay_det['pay_mode'] = $pay_mode;

            array_push($replace_array['payment_det'], $pay_det);
        }


        $replace_array['total_qty'] = $totalQty;

        $replace_array['payment'] = $pay_det;
        $replace_array['status'] = $status;

        //This is Custom Attribute Changes

        $replace_array['invoice_custom_detail'] = array();
        $result = mysqli_query($con, "select * from  tbl_attribute_value as t left Join tbl_attributes as i on  t.attributes_id = i.id where t.attributetype_id =  '1' and t.refernce_id = '" . $no . "' and   i.is_show_in_invoice =  1");
        $cust_attribute = array();
        while ($attr = mysqli_fetch_array($result)) {
            $key = '{{inv_cust_attr_' . $attr['attribute_code'] . '}}';
            $value = $attr['value'];
            $cust_attribute[$key] = $value;
            if (empty($value)) {
                $value = ' - ';
            }
        }

        $result = mysqli_query($con, "select * from    tbl_attributes  where is_show_in_invoice =  1");
        $replace_cust_attribute = array();
        while ($attr = mysqli_fetch_array($result)) {
            $key = '{{inv_cust_attr_' . $attr['attribute_code'] . '}}';
            $search_key = array_search($key, $cust_attribute);
            $key_det['key'] = $key;

            if (array_key_exists($key, $cust_attribute)) {
                $key_det['value'] = $cust_attribute[$key];
            } else {
                $value = '-';
                $cust_attribute[$key] = $value;
                $key_det['value'] = $value;
            }
            array_push($replace_array['invoice_custom_detail'], $key_det);
        }

        $replace_array['customer_custom_detail'] = array();
        $result = mysqli_query($con, "select * from  tbl_attribute_value as t left Join tbl_attributes as i on  t.attributes_id = i.id where t.attributetype_id =  '2' and t.refernce_id = '" . $customer_id . "' and   i.is_show_in_invoice =  1");
        $cust_attribute_crm = array();
        while ($attr = mysqli_fetch_array($result)) {
            $key = '{{crm_cust_attr_' . $attr['attribute_code'] . '}}';
            $value = $attr['value'];
            $cust_attribute_crm[$key] = $value;
            if (empty($value)) {
                $value = ' - ';
            }
        }
        $result = mysqli_query($con, "select * from    tbl_attributes  where is_show_in_invoice =  1");
        $replace_cust_attribute = array();
        while ($attr = mysqli_fetch_array($result)) {
            $key = '{{crm_cust_attr_' . $attr['attribute_code'] . '}}';
            $search_key = array_search($key, $cust_attribute);
            $key_det['key'] = $key;

            if (array_key_exists($key, $cust_attribute_crm)) {
                $key_det['value'] = $cust_attribute_crm[$key];
            } else {
                $value = '-';
                $cust_attribute_crm[$key] = $value;
                $key_det['value'] = $value;
            }
            array_push($replace_array['customer_custom_detail'], $key_det);
        }

        return $replace_array;
    }

    function getDesign($con, $no, $replace_array, $check_print_pdf, $thous_sep) {

        $result = mysqli_query($con, "select * from  tbl_email_templates where tplname = 'sales_order_template'");
        while ($row = mysqli_fetch_array($result))
            $msg = $row['message'];
        $temp = 't2';
        if ($check_print_pdf == 'for_pdf') {
            $new_msg = $msg;
            $msg = preg_replace("/<!--pdf-hidden-start-->.+?<!--pdf-hidden-end-->/i", "", $new_msg);
        }
        $replace_array = $this->getDesignStyles($no, $replace_array, $temp, $thous_sep);

        $replace_msg = $this->replacePlaceHolderValue($replace_array, $msg,$thous_sep);
        return $replace_msg;
    }

}

?>
