'use strict';


angular.module('app', [
    'ngAnimate',
    'ngCookies',
    'ngSanitize',
    'ngTouch',
    'ngStorage',
    'ui.router',
    'ui.bootstrap',
    'oc.lazyLoad',
    'pascalprecht.translate',
    'angularValidator',
    'hSweetAlert',
    'highcharts-ng',
    'dndLists',
    'textAngular',
    'chieffancypants.loadingBar',
    'ui.select',
    'angularLocalstorage',
    'angulartics', 
    'angulartics.google.analytics',
    'ng-fusioncharts',
    'angular-confirm'
]);
