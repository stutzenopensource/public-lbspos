'use strict';
/**
 * Config for the router
 */
angular.module('app')
        .run(
                ['$rootScope', '$state', '$stateParams', '$location', 'Auth',
                    function ($rootScope, $state, $stateParams, $location, Auth) {
                        $rootScope.$state = $state;
                        $rootScope.$stateParams = $stateParams;
                        $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {

                            if (typeof $location.search().refer != 'undefined')
                            {
                                $rootScope.refer = $location.search().refer;
                            } else
                            {
                                $rootScope.refer = "";
                            }
                            //
                            if (!$rootScope.isUserModelUpdated) {

                                event.preventDefault();
                                if (!$rootScope.isUserModelUpdatedProcessing)
                                {
                                    $rootScope.isUserModelUpdatedProcessing = true;
                                    Auth.getUserInfo().then(function (response) {
                                        $state.go(toState, toParams);
                                    });
                                }
                                return;
                            }

                            if ($rootScope.getNavigationBlockMsg != null && $rootScope.getNavigationBlockMsg() != "")
                            {
                                if (!confirm($rootScope.getNavigationBlockMsg(false))) {
                                    event.preventDefault(); // user clicks cancel, wants to stay on page
                                } else {
                                    //No Script need to redirect another page
                                }

                            }


                        });
                        $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
                            Auth.checkCookie();
                            if ($rootScope.isLogined)
                            {
                                if (typeof toState.accessRole != 'undefined')
                                {
                                    var avaAccessRole = toState.accessRole;
                                    if (avaAccessRole.indexOf($rootScope.app.selectedRole) == -1)
                                    {
                                        Auth.validatePageAccess();
                                    }
                                }
                            }

                            if (typeof toState.containerClass != "undefined")
                            {
                                $rootScope.containerClass = toState.containerClass;
                            } else
                            {
                                $rootScope.containerClass = '';
                            }
                            //Current Page Update
                            if (typeof toState.pageName != "undefined" && $rootScope.isLogined)
                            {
                                $rootScope.currentPage = toState.pageName;
                            } else
                            {
                                $rootScope.currentPage = '';
                                $rootScope.actionMsgError = "Unauthorized";
                            }
                        });
                    }
                ]
                )
        .config(
                ['$stateProvider', '$urlRouterProvider', 'JQ_CONFIG', 'MODULE_CONFIG', '$provide', 'COMPONENT_CONFIG',
                    function ($stateProvider, $urlRouterProvider, JQ_CONFIG, MODULE_CONFIG, $provide, COMPONENT_CONFIG) {

                        $urlRouterProvider.otherwise(function ($injector, $location) {
                            var $state = $injector.get("$state");
                            $state.go('app.dashboard');
                        });
                        $stateProvider
                                .state('app', {
                                    url: '',
                                    pageName: 'dashboard',
                                    containerClass: '',
                                    templateUrl: 'tpl/dashboard.html',
                                    resolve: load(['service/adminService.js', 'moment'])
                                })
                                .state('app.dashboard', {
                                    url: '/dashboard',
                                    pageName: 'dashboard-list',
                                    containerClass: 'czn-dashboard',
                                    templateUrl: 'tpl/dashboard-list.html',
                                    resolve: load(['controllor/dashboardListCtrl.js', 'sparklines', 'hc-area-chart'])
                                })
                                .state('app.userlist', {
                                    url: '/user-list',
                                    pageName: 'user',
                                    containerClass: '',
                                    templateUrl: 'tpl/user-list.html',
                                    resolve: load(['controllor/userListCtrl.js'])
                                })
                                .state('app.userrole', {
                                    url: '/role-list',
                                    pageName: 'userrole',
                                    containerClass: '',
                                    templateUrl: 'tpl/user-role.html',
                                    resolve: load(['controllor/userRoleCtrl.js'])
                                })
                                .state('app.userroleadd', {
                                    url: '/role-add',
                                    pageName: 'userrole-add',
                                    containerClass: '',
                                    templateUrl: 'tpl/user-role-add.html',
                                    resolve: load(['controllor/userRoleAddCtrl.js'])
                                })
                                .state('app.userroleedit', {
                                    url: '/user-role-edit/:id',
                                    pageName: 'userrole-edit',
                                    containerClass: '',
                                    templateUrl: 'tpl/user-role-edit.html',
                                    resolve: load(['controllor/userRoleEditCtrl.js'])
                                })
                                .state('app.categorylist', {
                                    url: '/category-list',
                                    pageName: 'category',
                                    containerClass: '',
                                    templateUrl: 'tpl/category-list.html',
                                    resolve: load(['controllor/categoryListCtrl.js'])
                                })
                                .state('app.clientlist', {
                                    url: '/client-list',
                                    pageName: 'client',
                                    containerClass: '',
                                    templateUrl: 'tpl/client-list.html',
                                    resolve: load(['controllor/clientListCtrl.js'])
                                })
                                .state('app.paymentlist', {
                                    url: '/payment-list',
                                    pageName: 'payment',
                                    containerClass: 'payment-list-print',
                                    templateUrl: 'tpl/payment-list.html',
                                    resolve: load(['controllor/paymentListCtrl.js'])
                                })
                                .state('app.useradd', {
                                    url: '/user-save',
                                    pageName: 'user',
                                    containerClass: '',
                                    templateUrl: 'tpl/user-add.html',
                                    resolve: load(['controllor/userAddCtrl.js'])
                                })
                                .state('app.paymentadd', {
                                    url: '/payment-save',
                                    pageName: 'payment',
                                    containerClass: '',
                                    templateUrl: 'tpl/paymentadd.html',
                                    resolve: load(['controllor/paymentAddCtrl.js'])
                                })
                                .state('app.clientadd', {
                                    url: '/client-save',
                                    pageName: 'client',
                                    templateUrl: 'tpl/clientadd.html',
                                    resolve: load(['controllor/clientAddCtrl.js'])
                                })
                                .state('app.categoryadd', {
                                    url: '/category-save',
                                    pageName: 'category',
                                    containerClass: '',
                                    templateUrl: 'tpl/category-add.html',
                                    resolve: load(['controllor/categoryAddCtrl.js'])
                                })
                                .state('app.userEdit', {
                                    url: '/user-edit/:userid',
                                    pageName: 'user',
                                    containerClass: '',
                                    templateUrl: 'tpl/user-edit.html',
                                    resolve: load(['controllor/userEditCtrl.js'])
                                })
                                .state('app.clientEdit', {
                                    url: '/client-edit/:id',
                                    pageName: 'client',
                                    containerClass: '',
                                    templateUrl: 'tpl/client-edit.html',
                                    resolve: load(['controllor/clientEditCtrl.js'])

                                })
                                .state('app.categoryEdit', {
                                    url: '/category-edit/:id',
                                    pageName: 'category',
                                    containerClass: '',
                                    templateUrl: 'tpl/category-edit.html',
                                    resolve: load(['controllor/categoryEditCtrl.js'])

                                })
                                .state('app.paymentEdit', {
                                    url: '/payment-edit/:id',
                                    pageName: 'payment',
                                    containerClass: '',
                                    templateUrl: 'tpl/payment-edit.html',
                                    resolve: load(['controllor/paymentEditCtrl.js'])
                                })
                                .state('app.payment-settlement', {
                                    url: '/payment-settlement',
                                    pageName: 'paymentsettle',
                                    containerClass: '',
                                    templateUrl: 'tpl/payment-settlement.html',
                                    resolve: load(['controllor/paymentSettlementCtrl.js'])
                                })
                                .state('settlement-print', {
                                    url: '/settlement-print/:id',
                                    pageName: 'settlement-print',
                                    containerClass: '',
                                    templateUrl: 'tpl/payment-settlement-print.html',
                                    resolve: load(['service/adminService.js', 'controllor/paymentSettlementPrintCtrl.js'])
                                })
                                .state('app.changepassword', {
                                    url: '/changepassword',
                                    pageName: 'changepassword',
                                    containerClass: '',
                                    templateUrl: 'tpl/changePassword.html',
                                    resolve: load(['controllor/changePasswordCtrl.js', 'controllor/userEditCtrl.js'])
                                })
                                .state('app.deal', {
                                    url: '/deal-list',
                                    pageName: 'deal',
                                    containerClass: '',
                                    templateUrl: 'tpl/deal.html',
                                    resolve: load(['controllor/dealCtrl.js'])
                                })
                                .state('app.dealadd', {
                                    url: '/deal-save',
                                    pageName: 'deal',
                                    containerClass: '',
                                    templateUrl: 'tpl/dealadd.html',
                                    resolve: load(['controllor/dealAddCtrl.js', 'controllor/newCustomerAddCtrl.js'])
                                })
                                .state('app.dealedit', {
                                    url: '/deal-modify/:id',
                                    pageName: 'deal',
                                    containerClass: '',
                                    templateUrl: 'tpl/dealedit.html',
                                    resolve: load(['controllor/dealEditCtrl.js', 'controllor/newCustomerAddCtrl.js', 'fileupload', 'angularFileUpload', 'photo-gallery', 'cloud-image-loader'])
                                })
                                .state('app.stage', {
                                    url: '/stage-list',
                                    pageName: 'stage',
                                    containerClass: '',
                                    templateUrl: 'tpl/stage.html',
                                    resolve: load(['controllor/stageCtrl.js'])
                                })
                                .state('app.stageadd', {
                                    url: '/stage-save',
                                    pageName: 'stage',
                                    containerClass: '',
                                    templateUrl: 'tpl/stageadd.html',
                                    resolve: load(['controllor/stageAddCtrl.js'])
                                })
                                .state('app.stageedit', {
                                    url: '/stage-modify/:id',
                                    pageName: 'stage',
                                    containerClass: '',
                                    templateUrl: 'tpl/stageedit.html',
                                    resolve: load(['controllor/stageEditCtrl.js'])
                                })
                                .state('app.screenAccess', {
                                    url: '/role-access/:id',
                                    pageName: 'screenaccess',
                                    containerClass: '',
                                    templateUrl: 'tpl/screen-access-edit.html',
                                    resolve: load(['controllor/screenAccessEditCtrl.js'])
                                })
                                .state('app.appSettings', {
                                    url: '/appSettings',
                                    pageName: 'appSettings',
                                    containerClass: '',
                                    templateUrl: 'tpl/appSettings.html',
                                    resolve: load(['controllor/appSettingsEditCtrl.js', 'ls-fileupload', 'angularLsFileUpload', 'ls-photo-gallery'])
                                })
                                .state('app.businessSettings', {
                                    url: '/businesssettings',
                                    pageName: 'businesssettings',
                                    containerClass: '',
                                    templateUrl: 'tpl/businessSettings.html',
                                    resolve: load(['controllor/appSettingsEditCtrl.js'])
                                })
                                .state('app.invoicesettings', {
                                    url: '/invoicesettings',
                                    pageName: 'invoicesettings',
                                    containerClass: '',
                                    templateUrl: 'tpl/invoicesettings.html',
                                    resolve: load(['controllor/appSettingsEditCtrl.js'])
                                })
                                .state('app.prefixsettings', {
                                    url: '/prefixsettings',
                                    pageName: 'prefixsettings',
                                    containerClass: '',
                                    templateUrl: 'tpl/prefixSettings.html',
                                    resolve: load(['controllor/appSettingsEditCtrl.js'])
                                })
                                .state('app.smssettings', {
                                    url: '/smssettings',
                                    pageName: 'smssettings',
                                    containerClass: '',
                                    templateUrl: 'tpl/smssettingsList.html',
                                    resolve: load(['controllor/smsSettingsListCtrl.js'])
                                })
                                .state('app.smssettingsedit', {
                                    url: '/smssettings/:id',
                                    pageName: 'smssettings',
                                    containerClass: '',
                                    templateUrl: 'tpl/smssettings.html',
                                    resolve: load(['controllor/smsSettingsCtrl.js'])
                                })
                                .state('app.emailsettingsedit', {
                                    url: '/emailsettings/:id',
                                    pageName: 'emailsettings',
                                    containerClass: '',
                                    templateUrl: 'tpl/emailsettings.html',
                                    resolve: load(['controllor/emailSettingsCtrl.js'])
                                })
                                .state('app.clientdetail', {
                                    url: '/client-detail/:id',
                                    containerClass: '',
                                    templateUrl: 'tpl/clientdetail.html',
                                    resolve: load(['controllor/clientDetailCtrl.js'])

                                })
                                .state('app.accountlist', {
                                    url: '/account-list',
                                    pageName: 'account',
                                    containerClass: '',
                                    templateUrl: 'tpl/account-list.html',
                                    resolve: load(['controllor/accountListCtrl.js'])
                                })
                                .state('app.accountaddd', {
                                    url: '/account-add',
                                    pageName: 'account',
                                    containerClass: '',
                                    templateUrl: 'tpl/account-add.html',
                                    resolve: load(['controllor/accountAddCtrl.js'])
                                })
                                .state('app.device', {
                                    url: '/device-list',
                                    pageName: 'device',
                                    containerClass: '',
                                    templateUrl: 'tpl/device.html',
                                    resolve: load(['controllor/deviceCtrl.js'])
                                })
                                .state('app.deviceadd', {
                                    url: '/device-save',
                                    pageName: 'device',
                                    containerClass: '',
                                    templateUrl: 'tpl/deviceadd.html',
                                    resolve: load(['controllor/deviceAddCtrl.js'])
                                })
                                .state('app.deviceedit', {
                                    url: '/device-modify/:id',
                                    pageName: 'device',
                                    containerClass: '',
                                    templateUrl: 'tpl/deviceedit.html',
                                    resolve: load(['controllor/deviceEditCtrl.js'])
                                })
                                .state('app.commission', {
                                    url: '/commission-report',
                                    pageName: 'commission',
                                    containerClass: 'payment-list-print',
                                    templateUrl: 'tpl/commissionReport.html',
                                    resolve: load(['controllor/commissionReportCtrl.js'])
                                })
                                .state('app.attribute', {
                                    url: '/attribute-list',
                                    pageName: 'attribute',
                                    containerClass: '',
                                    templateUrl: 'tpl/attribute-list.html',
                                    resolve: load(['controllor/attributeListCtrl.js'])
                                })
                                .state('app.attributeadd', {
                                    url: '/attribute-add',
                                    pageName: 'attributeadd',
                                    containerClass: '',
                                    templateUrl: 'tpl/attribute-add.html',
                                    resolve: load(['controllor/attributeAddCtrl.js'])
                                })
                                .state('app.attributeedit', {
                                    url: '/attribute-Edit/:id',
                                    pageName: 'attributeedit',
                                    containerClass: '',
                                    templateUrl: 'tpl/attribute-edit.html',
                                    resolve: load(['controllor/attributeEditCtrl.js'])
                                })
                                .state('app.serviceAdd', {
                                    url: '/serviceAdd',
                                    pageName: 'serviceAdd',
                                    containerClass: '',
                                    templateUrl: 'tpl/serviceAdd.html',
                                    resolve: load(['controllor/serviceAddCtrl.js'])
                                })
                                .state('app.serviceList', {
                                    url: '/serviceList',
                                    pageName: 'serviceList',
                                    containerClass: '',
                                    templateUrl: 'tpl/serviceList.html',
                                    resolve: load(['controllor/serviceListCtrl.js'])
                                })
                                .state('app.serviceEdit', {
                                    url: '/serviceEdit/:id',
                                    pageName: 'serviceEdit',
                                    containerClass: '',
                                    templateUrl: 'tpl/serviceEdit.html',
                                    resolve: load(['controllor/serviceEditCtrl.js'])
                                })
                                .state('app.productAdd', {
                                    url: '/productAdd',
                                    pageName: 'productAdd',
                                    containerClass: '',
                                    templateUrl: 'tpl/productAdd.html',
                                    resolve: load(['controllor/productAddCtrl.js'])
                                })
                                .state('app.productList', {
                                    url: '/productList',
                                    pageName: 'productList',
                                    containerClass: '',
                                    templateUrl: 'tpl/productList.html',
                                    resolve: load(['controllor/productListCtrl.js'])
                                })
                                .state('app.productEdit', {
                                    url: '/productEdit/:id',
                                    pageName: 'productEdit',
                                    containerClass: '',
                                    templateUrl: 'tpl/productEdit.html',
                                    resolve: load(['controllor/productEditCtrl.js', 'fileupload', 'angularFileUpload', 'photo-gallery', 'cloud-image-loader'])
                                })
                                .state('app.customers', {
                                    url: '/customersList',
                                    pageName: 'customers',
                                    containerClass: '',
                                    templateUrl: 'tpl/customer-list.html',
                                    resolve: load(['controllor/customerListCtrl.js'])
                                })
                                .state('app.customeradd', {
                                    url: '/customeradd',
                                    pageName: 'customeradd',
                                    containerClass: '',
                                    templateUrl: 'tpl/customeradd.html',
                                    resolve: load(['controllor/customerAddCtrl.js', 'custom-textinput', 'custom-selectoption', 'custom-multiselect', 'custom-checkbox', 'custom-yes-or-no', 'custom-date'])
                                })
                                .state('app.customerEdit', {
                                    url: '/customerEdit/:id',
                                    pageName: 'customerEdit',
                                    containerClass: '',
                                    templateUrl: 'tpl/customer-edit.html',
                                    resolve: load(['controllor/customerEditCtrl.js', 'custom-textinput', 'custom-selectoption', 'custom-multiselect', 'custom-checkbox', 'custom-yes-or-no', 'custom-date', 'fileupload', 'angularFileUpload', 'photo-gallery', 'cloud-image-loader'])
                                })
                                .state('app.uom', {
                                    url: '/uom',
                                    templateUrl: 'tpl/uom.html',
                                    resolve: load(['controllor/uomCtrl.js'])
                                })
                                .state('app.uomAdd', {
                                    url: '/uom-add',
                                    templateUrl: 'tpl/uomAdd.html',
                                    resolve: load(['controllor/uomAddCtrl.js'])
                                })
                                .state('app.uomEdit', {
                                    url: '/uom-edit/:id',
                                    templateUrl: 'tpl/uomEdit.html',
                                    resolve: load(['controllor/uomEditCtrl.js'])
                                })
                                .state('app.salesquote', {
                                    url: '/salesquote',
                                    pageName: 'salesquote',
                                    containerClass: '',
                                    templateUrl: 'tpl/salesquote.html',
                                    resolve: load(['controllor/salesquoteCtrl.js'])
                                })
                                .state('app.sampleadd', {
                                    url: '/sampleadd',
                                    pageName: 'sampleadd',
                                    containerClass: '',
                                    templateUrl: 'tpl/sampleadd.html',
                                    resolve: load(['controllor/sampleAddCtrl.js'])
                                })
                                .state('app.neworder', {
                                    url: '/new-order',
                                    pageName: "new-order",
                                    templateUrl: 'tpl/orders-new.html',
                                    resolve: load(['controllor/ordernewCtrl.js', 'controllor/productselectCtrl.js'])
                                })
                                .state('app.purchaseInvoice', {
                                    url: '/purchase-invoice',
                                    pageName: "purchase-invoice",
                                    templateUrl: 'tpl/purchase-invoice-list.html',
                                    resolve: load(['controllor/purchaseInvoiceListCtrl.js', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.purchaseInvoiceAdd', {
                                    url: '/purchase-invoice-add/:id',
                                    pageName: "purchase-invoiceAdd",
                                    templateUrl: 'tpl/purchase-invoice-add.html',
                                    resolve: load(['controllor/purchaseInvoiceAddCtrl.js', 'controllor/newCustomerAddCtrl.js', 'controllor/purchaseProductListCtrl.js', 'controllor/sendMailCtrl.js', 'custom-textinput', 'custom-selectoption', 'custom-multiselect', 'custom-checkbox', 'custom-yes-or-no', 'custom-date'])
                                })
                                .state('app.purchaseInvoiceEdit', {
                                    url: '/purchase-invoice-edit/:id',
                                    pageName: "purchase-invoiceEdit",
                                    templateUrl: 'tpl/purchase-invoice-edit.html',
                                    resolve: load(['controllor/purchaseInvoiceEditCtrl.js', 'controllor/purchaseProductListCtrl.js', 'controllor/sendMailCtrl.js', 'custom-textinput', 'custom-selectoption', 'custom-multiselect', 'custom-checkbox', 'custom-yes-or-no', 'custom-date'])
                                })
                                .state('app.purchaseInvoiceView', {
                                    url: '/purchase-invoice-view/:id/pop/:pop',
                                    pageName: "purchase-invoiceView",
                                    templateUrl: 'tpl/purchase-invoice-view.html',
                                    resolve: load(['controllor/purchaseInvoiceViewCtrl.js', 'controllor/purchaseProductListCtrl.js', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.invoice', {
                                    url: '/sales-invoice',
                                    pageName: "invoice",
                                    templateUrl: 'tpl/invoiceList.html',
                                    resolve: load(['controllor/invoiceListCtrl.js', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.invoiceAdd', {
                                    url: '/sales-invoice-add/:id',
                                    pageName: "invoiceAdd",
                                    templateUrl: 'tpl/invoiceAdd.html',
                                    resolve: load(['controllor/invoiceAddCtrl.js', 'controllor/productselectCtrl.js', 'controllor/newCustomerAddCtrl.js', 'custom-textinput', 'custom-selectoption', 'custom-multiselect', 'custom-checkbox', 'custom-yes-or-no', 'custom-date', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.dealInvoiceAdd', {
                                    url: '/deal-invoice-add/:id/:deal_id',
                                    pageName: "dealInvoiceAdd",
                                    templateUrl: 'tpl/invoiceAdd.html',
                                    resolve: load(['controllor/invoiceAddCtrl.js', 'controllor/productselectCtrl.js', 'controllor/newCustomerAddCtrl.js', 'custom-textinput', 'custom-selectoption', 'custom-multiselect', 'custom-checkbox', 'custom-yes-or-no', 'custom-date', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.invoiceEdit', {
                                    url: '/sales-invoice-edit/:id',
                                    pageName: "invoiceEdit",
                                    templateUrl: 'tpl/invoiceEdit.html',
                                    resolve: load(['controllor/invoiceEditCtrl.js', 'controllor/productselectCtrl.js', 'controllor/sendMailCtrl.js', 'custom-textinput', 'custom-selectoption', 'custom-multiselect', 'custom-date', 'custom-checkbox', 'custom-yes-or-no'])
                                })
                                .state('app.invoiceReturnAdd', {
                                    url: '/sales-return/:id',
                                    pageName: "invoiceReturnAdd",
                                    templateUrl: 'tpl/invoiceReturnAdd.html',
                                    resolve: load(['controllor/invoiceReturnAddCtrl.js', 'controllor/productselectCtrl.js', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.purchaseQuote', {
                                    url: '/purchase-order',
                                    pageName: "purchase-order",
                                    templateUrl: 'tpl/purchase-quote-list.html',
                                    resolve: load(['controllor/purchaseQuoteListCtrl.js', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.purchaseQuoteAdd', {
                                    url: '/purchase-order-add/:id',
                                    pageName: "purchase-orderAdd",
                                    templateUrl: 'tpl/purchase-quote-add.html',
                                    resolve: load(['controllor/purchaseQuoteAddCtrl.js', 'controllor/newCustomerAddCtrl.js', 'controllor/purchaseProductListCtrl.js', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.purchaseQuoteEdit', {
                                    url: '/purchase-order-edit/:id',
                                    pageName: "purchase-orderEdit",
                                    templateUrl: 'tpl/purchase-quote-edit.html',
                                    resolve: load(['controllor/purchaseQuoteEditCtrl.js', 'controllor/purchaseProductListCtrl.js', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.purchaseQuoteView', {
                                    url: '/purchase-order-view/:id',
                                    pageName: "purchase-orderView",
                                    templateUrl: 'tpl/purchase-quote-view.html',
                                    resolve: load(['controllor/purchaseQuoteViewCtrl.js', 'controllor/purchaseProductListCtrl.js', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.salesorder', {
                                    url: '/sales-order',
                                    pageName: "order",
                                    templateUrl: 'tpl/salesorderList.html',
                                    resolve: load(['controllor/salesorderListCtrl.js', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.salesorderAdd', {
                                    url: '/sales-order-add/:id',
                                    pageName: "orderAdd",
                                    templateUrl: 'tpl/salesorderAdd.html',
                                    resolve: load(['controllor/salesorderAddCtrl.js', 'controllor/newCustomerAddCtrl.js', 'controllor/productselectCtrl.js', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.salesorderEdit', {
                                    url: '/sales-order-edit/:id',
                                    pageName: "orderEdit",
                                    templateUrl: 'tpl/salesorderEdit.html',
                                    resolve: load(['controllor/salesorderEditCtrl.js', 'controllor/productselectCtrl.js', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.salesorderView', {
                                    url: '/sales-order-view/:id',
                                    pageName: "orderView",
                                    templateUrl: 'tpl/salesorderView.html',
                                    resolve: load(['controllor/salesorderViewCtrl.js', 'controllor/productselectCtrl.js', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.quote', {
                                    url: '/sales-estimate',
                                    pageName: "estimate",
                                    templateUrl: 'tpl/quoteList.html',
                                    resolve: load(['controllor/quoteListCtrl.js', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.quoteAdd', {
                                    url: '/sales-estimate-add/:id',
                                    pageName: "estimateAdd",
                                    templateUrl: 'tpl/quoteAdd.html',
                                    resolve: load(['controllor/quoteAddCtrl.js', 'controllor/newCustomerAddCtrl.js', 'controllor/productselectCtrl.js', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.dealQuoteAdd', {
                                    url: '/sales-estimate-add/:id',
                                    pageName: "orderAdd",
                                    templateUrl: 'tpl/quoteAdd.html',
                                    resolve: load(['controllor/quoteAddCtrl.js', 'controllor/newCustomerAddCtrl.js', 'controllor/productselectCtrl.js', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.quoteEdit', {
                                    url: '/sales-estimate-edit/:id',
                                    pageName: "estimateEdit",
                                    templateUrl: 'tpl/quoteEdit.html',
                                    resolve: load(['controllor/quoteEditCtrl.js', 'controllor/productselectCtrl.js', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.invoicePayList', {
                                    url: '/invoice-pay-list',
                                    pageName: "invoicePayList",
                                    templateUrl: 'tpl/invoice-pay-list.html',
                                    resolve: load(['controllor/invoicepayListCtrl.js'])
                                })
                                .state('app.invoicePayAdd', {
                                    url: '/invoice-pay-add',
                                    pageName: "invoicePayAdd",
                                    templateUrl: 'tpl/invoice-pay-add.html',
                                    resolve: load(['controllor/invoicepayAddCtrl.js'])
                                })
                                .state('app.invoiceView1', {
                                    url: '/sales-invoice-view-t1/id/:id/pop/:pop',
                                    pageName: "invoiceView1",
                                    templateUrl: 'tpl/invoiceView1.html',
                                    resolve: load(['controllor/invoiceViewCtrl.js', 'controllor/productselectCtrl.js', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.invoiceView2', {
                                    url: '/sales-invoice-view-t2/id/:id/pop/:pop',
                                    pageName: "invoiceView2",
                                    templateUrl: 'tpl/invoiceView2.html',
                                    resolve: load(['controllor/invoiceViewCtrl.js', 'controllor/productselectCtrl.js', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.quoteView', {
                                    url: '/sales-estimate-view/:id',
                                    pageName: "estimateView",
                                    templateUrl: 'tpl/quoteView.html',
                                    resolve: load(['controllor/quoteViewCtrl.js', 'controllor/productselectCtrl.js', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.trial', {
                                    url: '/trial-report',
                                    pageName: 'trial',
                                    containerClass: 'payment-list-print',
                                    templateUrl: 'tpl/trialReport.html',
                                    resolve: load(['controllor/trialReportCtrl.js'])
                                })
                                .state('app.expense', {
                                    url: '/expense-list',
                                    pageName: "expense",
                                    templateUrl: 'tpl/expenseList.html',
                                    resolve: load(['controllor/expenseListCtrl.js'])
                                })
                                .state('app.expenseadd', {
                                    url: '/expense-add',
                                    pageName: "expense",
                                    templateUrl: 'tpl/expenseAdd.html',
                                    resolve: load(['controllor/expenseAddCtrl.js'])
                                })
                                .state('app.expenseedit', {
                                    url: '/expense-edit/:id',
                                    pageName: "expense",
                                    templateUrl: 'tpl/expenseEdit.html',
                                    resolve: load(['controllor/expenseEditCtrl.js'])
                                })
                                .state('app.incomecategory', {
                                    url: '/incomecategory',
                                    pageName: "incomecategory",
                                    templateUrl: 'tpl/incomecategoryList.html',
                                    resolve: load(['controllor/incomecategoryListCtrl.js'])
                                })
                                .state('app.incomecategoryadd', {
                                    url: '/incomecategory-add',
                                    pageName: "incomecategory",
                                    templateUrl: 'tpl/incomecategoryAdd.html',
                                    resolve: load(['controllor/incomecategoryAddCtrl.js'])
                                })
                                .state('app.incomecategoryedit', {
                                    url: '/incomecategory-edit/:id',
                                    pageName: "incomecategory",
                                    templateUrl: 'tpl/incomecategoryEdit.html',
                                    resolve: load(['controllor/incomecategoryEditCtrl.js'])
                                })
                                .state('app.expensecategory', {
                                    url: '/expensecategory',
                                    pageName: "expensecategory",
                                    templateUrl: 'tpl/expensecategoryList.html',
                                    resolve: load(['controllor/expensecategoryListCtrl.js'])
                                })
                                .state('app.expensecategoryadd', {
                                    url: '/expensecategory-add',
                                    pageName: "expensecategory",
                                    templateUrl: 'tpl/expensecategoryAdd.html',
                                    resolve: load(['controllor/expensecategoryAddCtrl.js'])
                                })
                                .state('app.expensecategoryedit', {
                                    url: '/expensecategory-edit/:id',
                                    pageName: "expensecategory",
                                    templateUrl: 'tpl/expensecategoryEdit.html',
                                    resolve: load(['controllor/expensecategoryEditCtrl.js'])
                                })
                                .state('app.paymentprint', {
                                    url: '/payment-print/:id',
                                    pageName: "paymentprint",
                                    templateUrl: 'tpl/paymentprint.html',
                                    resolve: load(['controllor/paymentPrintCtrl.js'])
                                })
                                .state('app.transaction', {
                                    url: '/transaction-list',
                                    pageName: 'transaction',
                                    containerClass: '',
                                    templateUrl: 'tpl/transaction.html',
                                    resolve: load(['controllor/transactionCtrl.js'])
                                })
                                .state('app.sales', {
                                    url: '/sales-report',
                                    pageName: 'sales',
                                    containerClass: '',
                                    templateUrl: 'tpl/sales.html',
                                    resolve: load(['controllor/salesCtrl.js'])
                                })
                                .state('app.itemizedsales', {
                                    url: '/itemizedsales-report',
                                    pageName: 'itemizedsales',
                                    containerClass: '',
                                    templateUrl: 'tpl/itemizedsales.html',
                                    resolve: load(['controllor/itemizedsalesCtrl.js'])
                                })
                                .state('app.paymentreport', {
                                    url: '/payment-report',
                                    pageName: "paymentreport",
                                    templateUrl: 'tpl/paymentreport.html',
                                    resolve: load(['controllor/paymentReportCtrl.js'])
                                })
                                .state('app.receivablereport', {
                                    url: '/receivable-report',
                                    pageName: "receivablereport",
                                    templateUrl: 'tpl/receivablereport.html',
                                    resolve: load(['controllor/receivableReportCtrl.js'])
                                })
                                .state('app.payablereport', {
                                    url: '/payable-report',
                                    pageName: "payablereport",
                                    templateUrl: 'tpl/payablereport.html',
                                    resolve: load(['controllor/payableReportCtrl.js'])
                                })
                                .state('app.partysalesstatement', {
                                    url: '/partywise-sales-statement',
                                    pageName: "partysalesstatement",
                                    templateUrl: 'tpl/partysalesstatement.html',
                                    resolve: load(['controllor/partySalesStatementCtrl.js'])
                                })
                                .state('app.partypurchasestatement', {
                                    url: '/party-purchase-statement',
                                    pageName: "partypurchasestatement",
                                    templateUrl: 'tpl/partypurchasestatement.html',
                                    resolve: load(['controllor/partyPurchasesStatementCtrl.js'])
                                })
                                .state('app.partysalespurchasebased', {
                                    url: '/party-sales-purchase-based',
                                    pageName: "partysalespurchasebased",
                                    templateUrl: 'tpl/partysalespurchasebased.html',
                                    resolve: load(['controllor/partysalespurchasebasedCtrl.js'])
                                })

                                .state('app.itemwisesalespurchase', {
                                    url: '/item-stock-partywise-purchase',
                                    pageName: "itemwisesalespurchase",
                                    templateUrl: 'tpl/itemwisesalespurchase.html',
                                    resolve: load(['controllor/itemwisesalespurchaseCtrl.js'])
                                })
                                .state('app.account', {
                                    url: '/account_list',
                                    pageName: 'account',
                                    containerClass: '',
                                    templateUrl: 'tpl/account.html',
                                    resolve: load(['controllor/accountCtrl.js'])
                                })
                                .state('app.accountbalance', {
                                    url: '/accountbalance_list',
                                    pageName: 'accountbalance',
                                    containerClass: '',
                                    templateUrl: 'tpl/accountbalance.html',
                                    resolve: load(['controllor/accountbalanceCtrl.js'])
                                })
                                .state('app.accountAdd', {
                                    url: '/account_add',
                                    pageName: 'account',
                                    containerClass: '',
                                    templateUrl: 'tpl/accountAdd.html',
                                    resolve: load(['controllor/accountaddCtrl.js'])
                                })
                                .state('app.accountedit', {
                                    url: '/account-modify/:id',
                                    pageName: 'account',
                                    containerClass: '',
                                    templateUrl: 'tpl/accountedit.html',
                                    resolve: load(['controllor/accountEditCtrl.js'])
                                })
                                .state('app.tax', {
                                    url: '/tax-list',
                                    pageName: 'tax',
                                    containerClass: '',
                                    templateUrl: 'tpl/tax.html',
                                    resolve: load(['controllor/taxCtrl.js'])
                                })
                                .state('app.taxsummary', {
                                    url: '/taxsummary-list',
                                    pageName: 'taxsummary',
                                    containerClass: '',
                                    templateUrl: 'tpl/taxsummary.html',
                                    resolve: load(['controllor/taxsummaryCtrl.js'])
                                })
                                .state('app.deposit', {
                                    url: '/deposit-list',
                                    pageName: 'deposit',
                                    containerClass: '',
                                    templateUrl: 'tpl/deposit.html',
                                    resolve: load(['controllor/depositCtrl.js'])
                                })
                                .state('app.depositAdd', {
                                    url: '/deposit-save',
                                    pageName: 'deposit',
                                    templateUrl: 'tpl/depositAdd.html',
                                    resolve: load(['controllor/depositAddCtrl.js'])
                                })
                                .state('app.depositEdit', {
                                    url: '/deposit-modify/:id',
                                    pageName: 'deposit',
                                    containerClass: '',
                                    templateUrl: 'tpl/depositEdit.html',
                                    resolve: load(['controllor/depositEditCtrl.js'])
                                })
                                .state('app.transexpense', {
                                    url: '/transexpense_list',
                                    pageName: 'transexpense',
                                    containerClass: '',
                                    templateUrl: 'tpl/transexpense.html',
                                    resolve: load(['controllor/transexpenseCtrl.js'])
                                })
                                .state('app.transexpenseAdd', {
                                    url: '/transeexpense-save',
                                    pageName: 'transexpense',
                                    templateUrl: 'tpl/transexpenseAdd.html',
                                    resolve: load(['controllor/transexpenseAddCtrl.js'])
                                })
                                .state('app.transexpenseEdit', {
                                    url: '/transeexpense-modify/:id',
                                    pageName: 'transeexpense',
                                    containerClass: '',
                                    templateUrl: 'tpl/transexpenseEdit.html',
                                    resolve: load(['controllor/transexpenseEditCtrl.js'])
                                })
                                .state('app.accountstatement', {
                                    url: '/account-statement',
                                    pageName: 'accountstatement',
                                    containerClass: '',
                                    templateUrl: 'tpl/accountstatement.html',
                                    resolve: load(['controllor/accountStatementCtrl.js'])
                                })
                                .state('app.activitylist', {
                                    url: '/activity-list',
                                    pageName: 'activitylist',
                                    containerClass: '',
                                    templateUrl: 'tpl/activity.html',
                                    resolve: load(['controllor/activityLogCtrl.js'])
                                })
                                .state('app.inventorystock', {
                                    url: '/inventorystock-list',
                                    pageName: 'inventorystock',
                                    containerClass: '',
                                    templateUrl: 'tpl/inventorystock.html',
                                    resolve: load(['controllor/inventoryStockCtrl.js'])
                                })
                                .state('app.stocktracelist', {
                                    url: '/stocktracelist',
                                    pageName: 'stocktrace',
                                    containerClass: '',
                                    templateUrl: 'tpl/stocktracelist.html',
                                    resolve: load(['controllor/stocktracelistCtrl.js'])
                                })
                                .state('app.stockadjustment', {
                                    url: '/stockadjustment-list',
                                    pageName: 'stockadjustment',
                                    containerClass: '',
                                    templateUrl: 'tpl/stockadjustment.html',
                                    resolve: load(['controllor/stockadjustmentCtrl.js'])
                                })
                                .state('app.stockadjustmentAdd', {
                                    url: '/stockadjustment-save',
                                    pageName: 'stockadjustment',
                                    containerClass: '',
                                    templateUrl: 'tpl/stockadjustmentAdd.html',
                                    resolve: load(['controllor/stockadjustmentAddCtrl.js', 'cloud-image-loader', 'fileupload', 'angularFileUpload', 'photo-gallery'])
                                })
                                .state('app.stockadjustmentEdit', {
                                    url: '/stockadjustment-edit/:id',
                                    pageName: 'stockadjustmentEdit',
                                    containerClass: '',
                                    templateUrl: 'tpl/stockadjustmentEdit.html',
                                    resolve: load(['controllor/stockadjustmentEditCtrl.js', 'cloud-image-loader', 'fileupload', 'angularFileUpload', 'photo-gallery'])
                                })
                                .state('app.stockadjustmentView', {
                                    url: '/stockadjustment-view/:id',
                                    pageName: 'stockadjustmentView',
                                    containerClass: '',
                                    templateUrl: 'tpl/stockAdjustmentView.html',
                                    resolve: load(['controllor/stockAdjustmentViewCtrl.js', 'cloud-image-loader', 'fileupload', 'angularFileUpload', 'photo-gallery'])
                                })
                                .state('app.stockwastage', {
                                    url: '/stockwastage-list',
                                    pageName: 'stockwastage',
                                    containerClass: '',
                                    templateUrl: 'tpl/stockwastage.html',
                                    resolve: load(['controllor/stockwastageCtrl.js'])
                                })
                                .state('app.stockwastageAdd', {
                                    url: '/stockwastage-save',
                                    pageName: 'stockwastage',
                                    containerClass: '',
                                    templateUrl: 'tpl/stockwastageAdd.html',
                                    resolve: load(['controllor/stockwastageAddCtrl.js', 'cloud-image-loader', 'fileupload', 'angularFileUpload', 'photo-gallery'])
                                })
                                .state('app.stockwastageEdit', {
                                    url: '/stockwastage-edit/:id',
                                    pageName: 'stockwastage-edit',
                                    containerClass: '',
                                    templateUrl: 'tpl/stockwastageEdit.html',
                                    resolve: load(['controllor/stockwastageEditCtrl.js', 'cloud-image-loader', 'fileupload', 'angularFileUpload', 'photo-gallery'])
                                })
                                .state('app.stockwastageView', {
                                    url: '/stockwastage-view/:id',
                                    pageName: 'stockwastage-view',
                                    containerClass: '',
                                    templateUrl: 'tpl/stockWastageView.html',
                                    resolve: load(['controllor/stockWastageViewCtrl.js', 'cloud-image-loader', 'fileupload', 'angularFileUpload', 'photo-gallery'])
                                })
                                .state('app.minstock', {
                                    url: '/minstock-list',
                                    pageName: 'minstock',
                                    containerClass: '',
                                    templateUrl: 'tpl/minstock.html',
                                    resolve: load(['controllor/minstockCtrl.js'])
                                })
                                .state('app.taxlist', {
                                    url: '/taxlist',
                                    pageName: 'taxlist',
                                    containerClass: '',
                                    templateUrl: 'tpl/taxlist.html',
                                    resolve: load(['controllor/taxlistCtrl.js'])
                                })
                                .state('app.taxlistAdd', {
                                    url: '/tax-add',
                                    pageName: "taxlist",
                                    templateUrl: 'tpl/taxlistAdd.html',
                                    resolve: load(['controllor/taxlistAddCtrl.js'])
                                })
                                .state('app.taxlistEdit', {
                                    url: '/tax-modify/:id',
                                    pageName: 'taxlist',
                                    containerClass: '',
                                    templateUrl: 'tpl/taxlistEdit.html',
                                    resolve: load(['controllor/taxlistEditCtrl.js'])
                                })
                                .state('app.transferlist', {
                                    url: '/journallist',
                                    pageName: 'journallist',
                                    containerClass: '',
                                    templateUrl: 'tpl/transferlist.html',
                                    resolve: load(['controllor/transferlistCtrl.js'])
                                })
                                .state('app.transferlistAdd', {
                                    url: '/journal-add',
                                    pageName: "journalAdd",
                                    templateUrl: 'tpl/transferlistAdd.html',
                                    resolve: load(['controllor/transferlistAddCtrl.js'])
                                })
                                .state('app.transferlistEdit', {
                                    url: '/journal-modify/:id',
                                    pageName: 'journalView',
                                    containerClass: '',
                                    templateUrl: 'tpl/transferlistEdit.html',
                                    resolve: load(['controllor/transferlistEditCtrl.js'])
                                })
                                .state('app.paymentterms', {
                                    url: '/paymentterms',
                                    pageName: 'paymentterms',
                                    containerClass: '',
                                    templateUrl: 'tpl/paymentterms.html',
                                    resolve: load(['controllor/paymenttermsCtrl.js'])
                                })
                                .state('app.paymenttermsAdd', {
                                    url: '/paymentterms-add',
                                    pageName: "paymentterms",
                                    templateUrl: 'tpl/paymenttermsAdd.html',
                                    resolve: load(['controllor/paymenttermsAddCtrl.js'])
                                })
                                .state('app.paymenttermsEdit', {
                                    url: '/paymentterms-modify/:id',
                                    pageName: 'paymentterms',
                                    containerClass: '',
                                    templateUrl: 'tpl/paymenttermsEdit.html',
                                    resolve: load(['controllor/paymenttermsEditCtrl.js'])
                                })
                                .state('app.activitylog', {
                                    url: '/activitylog',
                                    pageName: 'activitylog',
                                    containerClass: '',
                                    templateUrl: 'tpl/activitylog.html',
                                    resolve: load(['controllor/activityLogCtrl.js'])
                                })
                                .state('app.databasebackup', {
                                    url: '/databasebackup',
                                    pageName: 'databasebackup',
                                    containerClass: '',
                                    templateUrl: 'tpl/databasebackup.html'
                                })
                                .state('app.taxgroup', {
                                    url: '/taxgroup',
                                    pageName: 'taxgroup',
                                    containerClass: '',
                                    templateUrl: 'tpl/taxgroup.html',
                                    resolve: load(['controllor/taxGroupCtrl.js'])
                                })
                                .state('app.taxgroupadd', {
                                    url: '/taxgroup-add',
                                    pageName: 'taxgroup',
                                    containerClass: '',
                                    templateUrl: 'tpl/taxGroupAdd.html',
                                    resolve: load(['controllor/taxGroupAddCtrl.js'])
                                })
                                .state('app.taxgroupedit', {
                                    url: '/taxgroup-edit/:id',
                                    pageName: 'taxgroup',
                                    containerClass: '',
                                    templateUrl: 'tpl/taxGroupEdit.html',
                                    resolve: load(['controllor/taxGroupEditCtrl.js'])
                                })
                                .state('app.expensereport', {
                                    url: '/expensereport',
                                    pageName: 'expensereport',
                                    containerClass: '',
                                    templateUrl: 'tpl/expensereport.html',
                                    resolve: load(['controllor/expensereportCtrl.js'])
                                })
                                .state('app.emailsettingstemp', {
                                    url: '/emailsettings-template/:tplname',
                                    pageName: 'emailsettings',
                                    containerClass: '',
                                    templateUrl: 'tpl/emailsettings-template.html',
                                    resolve: load(['controllor/emailSettingsTemplateCtrl.js'])
                                })
                                .state('app.emailsettings', {
                                    url: '/emailsettings-list',
                                    pageName: 'emailsettings',
                                    containerClass: '',
                                    templateUrl: 'tpl/emailSettingsList.html',
                                    resolve: load(['controllor/emailSettingsListCtrl.js'])
                                })
                                .state('app.albumlist', {
                                    url: '/albumlist',
                                    pageName: 'albumlist',
                                    containerClass: '',
                                    templateUrl: 'tpl/albumlist.html',
                                    resolve: load(['controllor/albumlistCtrl.js', 'cloud-image-loader', 'photo-gallery'])
                                })
                                .state('app.albumAdd', {
                                    url: '/album-add',
                                    pageName: "albumlist",
                                    templateUrl: 'tpl/albumAdd.html',
                                    resolve: load(['controllor/albumAddCtrl.js'])
                                })
                                .state('app.albumEdit', {
                                    url: '/album-modify/:id',
                                    pageName: 'albumlist',
                                    containerClass: '',
                                    templateUrl: 'tpl/albumEdit.html',
                                    resolve: load(['controllor/albumEditCtrl.js'])
                                })
                                .state('app.albumView', {
                                    url: '/album-view/:id',
                                    pageName: 'albumlist',
                                    containerClass: '',
                                    templateUrl: 'tpl/albumView.html',
                                    resolve: load(['controllor/albumViewCtrl.js', 'cloud-image-loader', 'fileupload', 'angularFileUpload', 'photo-gallery'])
                                })
                                .state('app.albumViewNew', {
                                    url: '/albumNew-view-new/:id',
                                    pageName: 'albumlist',
                                    containerClass: '',
                                    templateUrl: 'tpl/albumViewNew.html',
                                    resolve: load(['controllor/albumViewNewCtrl.js', 'photo-gallery', 'cloud-image-loader'])
                                })
                                .state('app.country', {
                                    url: '/country-list',
                                    pageName: 'country',
                                    containerClass: '',
                                    templateUrl: 'tpl/countryList.html',
                                    resolve: load(['controllor/countryListCtrl.js'])
                                })
                                .state('app.countryAdd', {
                                    url: '/country-add',
                                    pageName: 'country',
                                    containerClass: '',
                                    templateUrl: 'tpl/countryAdd.html',
                                    resolve: load(['controllor/countryAddCtrl.js'])
                                })
                                .state('app.countryEdit', {
                                    url: '/country-edit/:id',
                                    pageName: 'country',
                                    containerClass: '',
                                    templateUrl: 'tpl/countryEdit.html',
                                    resolve: load(['controllor/countryEditCtrl.js'])
                                })
                                .state('app.state', {
                                    url: '/state-list',
                                    pageName: 'state',
                                    containerClass: '',
                                    templateUrl: 'tpl/stateList.html',
                                    resolve: load(['controllor/stateListCtrl.js'])
                                })
                                .state('app.stateAdd', {
                                    url: '/state-add',
                                    pageName: 'state',
                                    containerClass: '',
                                    templateUrl: 'tpl/stateAdd.html',
                                    resolve: load(['controllor/stateAddCtrl.js'])
                                })
                                .state('app.stateEdit', {
                                    url: '/state-edit/:id',
                                    pageName: 'state',
                                    containerClass: '',
                                    templateUrl: 'tpl/stateEdit.html',
                                    resolve: load(['controllor/stateEditCtrl.js'])
                                })
                                .state('app.city', {
                                    url: '/city-list',
                                    pageName: 'city',
                                    containerClass: '',
                                    templateUrl: 'tpl/cityList.html',
                                    resolve: load(['controllor/cityListCtrl.js'])
                                })
                                .state('app.cityAdd', {
                                    url: '/city-add',
                                    pageName: 'city',
                                    containerClass: '',
                                    templateUrl: 'tpl/cityAdd.html',
                                    resolve: load(['controllor/cityAddCtrl.js'])
                                })
                                .state('app.cityEdit', {
                                    url: '/city-edit/:id',
                                    pageName: 'city',
                                    containerClass: '',
                                    templateUrl: 'tpl/cityEdit.html',
                                    resolve: load(['controllor/cityEditCtrl.js'])
                                })
                                .state('app.contact', {
                                    url: '/contact-list',
                                    pageName: 'contact',
                                    containerClass: '',
                                    templateUrl: 'tpl/contactList.html',
                                    resolve: load(['controllor/contactListCtrl.js'])
                                })
                                .state('app.contactAdd', {
                                    url: '/contact-add',
                                    pageName: 'contact',
                                    containerClass: '',
                                    templateUrl: 'tpl/contactAdd.html',
                                    resolve: load(['controllor/contactAddCtrl.js'])
                                })
                                .state('app.contactEdit', {
                                    url: '/contact-edit/:id',
                                    pageName: 'contact',
                                    containerClass: '',
                                    templateUrl: 'tpl/contactEdit.html',
                                    resolve: load(['controllor/contactEditCtrl.js'])
                                })
                                .state('app.pos', {
                                    url: '/pos',
                                    pageName: 'pos',
                                    containerClass: '',
                                    templateUrl: 'tpl/pos.html',
                                    resolve: load(['controllor/posCtrl.js', 'controllor/newCustomerAddCtrl.js'])
                                })
                                .state('app.discountedreport', {
                                    url: '/discountedreport',
                                    pageName: 'discountedreport',
                                    containerClass: '',
                                    templateUrl: 'tpl/discountedreport.html',
                                    resolve: load(['controllor/discountedreportCtrl.js'])
                                })
                                .state('app.partywisepurchasereport', {
                                    url: '/partywisepurchasereport',
                                    pageName: 'partywisepurchasereport',
                                    containerClass: '',
                                    templateUrl: 'tpl/partywisepurchasereport.html',
                                    resolve: load(['controllor/partywisepurchasereportCtrl.js'])
                                })
                                .state('app.salestocus', {
                                    url: '/Sales-To-Customers',
                                    pageName: 'salestocus',
                                    containerClass: '',
                                    templateUrl: 'tpl/salestocus.html',
                                    resolve: load(['controllor/salesToCusCtrl.js'])
                                })
                                .state('app.purfromcus', {
                                    url: '/Purchase-From-Customers',
                                    pageName: 'purfromcus',
                                    containerClass: '',
                                    templateUrl: 'tpl/purfromcus.html',
                                    resolve: load(['controllor/purFromCusCtrl.js'])
                                })
                                .state('app.advancepayment', {
                                    url: '/advancepayment-list',
                                    pageName: 'advancepayment',
                                    containerClass: '',
                                    templateUrl: 'tpl/advancepayment.html',
                                    resolve: load(['controllor/advancepaymentCtrl.js'])
                                })
                                .state('app.purchasepayment', {
                                    url: '/purchasepayment-list',
                                    pageName: 'purchasepayment',
                                    containerClass: '',
                                    templateUrl: 'tpl/purchasepayment.html',
                                    resolve: load(['controllor/purchasepaymentCtrl.js'])
                                })
                                .state('app.advancepaymenAdd', {
                                    url: '/advancepayment-save',
                                    pageName: 'advancepayment',
                                    containerClass: '',
                                    templateUrl: 'tpl/advancepaymentAdd.html',
                                    resolve: load(['controllor/advancepaymentAddCtrl.js'])
                                })
                                .state('app.advancepaymentEdit', {
                                    url: '/advancepayment-modify/:id',
                                    pageName: 'advancepayment',
                                    containerClass: '',
                                    templateUrl: 'tpl/advancepaymentEdit.html',
                                    resolve: load(['controllor/advancepaymentEditCtrl.js'])
                                })
                                .state('app.advancepaymentView', {
                                    url: '/advancepayment-view/:id',
                                    pageName: 'advancepayment',
                                    containerClass: '',
                                    templateUrl: 'tpl/advancepaymentView.html',
                                    resolve: load(['controllor/advancepaymentViewCtrl.js'])
                                })
                                .state('app.purchasepaymentView', {
                                    url: '/purchasepayment-view/:id',
                                    pageName: 'purchasepayment',
                                    containerClass: '',
                                    templateUrl: 'tpl/advancepaymentView.html',
                                    resolve: load(['controllor/advancepaymentViewCtrl.js'])
                                })
                                .state('app.customersettings', {
                                    url: '/customersettings',
                                    pageName: 'customersettings',
                                    containerClass: '',
                                    templateUrl: 'tpl/customersettings.html',
                                    resolve: load(['controllor/appSettingsEditCtrl.js'])
                                })
                                .state('app.posinvoiceEdit', {
                                    url: '/invoice-edit/:id',
                                    pageName: 'invoice-edit',
                                    containerClass: '',
                                    templateUrl: 'tpl/posinvoiceEdit.html',
                                    resolve: load(['controllor/posinvoiceEditCtrl.js', 'controllor/newCustomerAddCtrl.js', ])
                                })
                                .state('app.categorystockreport', {
                                    url: '/categorystock-report',
                                    pageName: 'category-stock-report',
                                    containerClass: '',
                                    templateUrl: 'tpl/categorystockreport.html',
                                    resolve: load(['controllor/categorystockreportCtrl.js'])
                                })
                                .state('app.categorywisereport', {
                                    url: '/categorywise-report',
                                    pageName: 'category-wise-report',
                                    containerClass: '',
                                    templateUrl: 'tpl/categoryWiseReport.html',
                                    resolve: load(['controllor/categoryWiseReportCtrl.js'])
                                })
                                .state('app.barcodewisestockreport', {
                                    url: '/barcodewise-stock-report',
                                    pageName: 'barcodewise-stock-report',
                                    containerClass: '',
                                    templateUrl: 'tpl/barcodereport.html',
                                    resolve: load(['controllor/barcodereportCtrl.js'])
                                })
                                .state('app.creditnote',
                                        {
                                            url: '/creditnote-list',
                                            pageName: "creditnote",
                                            templateUrl: 'tpl/creditnoteList.html',
                                            resolve: load(['controllor/creditnoteListCtrl.js'])
                                        })
                                .state('app.creditnoteadd', {
                                    url: '/creditnote-add',
                                    pageName: "creditnote-add",
                                    templateUrl: 'tpl/creditnoteAdd.html',
                                    resolve: load(['controllor/creditnoteAddCtrl.js'])
                                })
                                .state('app.creditnoteview', {
                                    url: '/creditnote-view/:id',
                                    pageName: "creditnote-view",
                                    templateUrl: 'tpl/creditnoteView.html',
                                    resolve: load(['controllor/creditnoteViewCtrl.js'])
                                })
                                .state('app.debitnote', {
                                    url: '/debitnote-list',
                                    pageName: "debitnote",
                                    templateUrl: 'tpl/debitnoteList.html',
                                    resolve: load(['controllor/debitnoteCtrl.js'])
                                })
                                .state('app.debitnoteadd', {
                                    url: '/debitnote-add',
                                    pageName: "debitnote-add",
                                    templateUrl: 'tpl/debitnoteAdd.html',
                                    resolve: load(['controllor/debitnoteAddCtrl.js'])
                                })
                                .state('app.debitnoteview', {
                                    url: '/debitnote-view/:id',
                                    pageName: "debitnote-view",
                                    templateUrl: 'tpl/debitnoteView.html',
                                    resolve: load(['controllor/debitnoteViewCtrl.js'])
                                })
                                .state('app.acodestatement', {
                                    url: '/acode-statement',
                                    pageName: "acode-statement",
                                    templateUrl: 'tpl/acodestatement.html',
                                    resolve: load(['controllor/acodeStatementCtrl.js'])
                                })
                                .state('app.tag', {
                                    url: '/tag',
                                    templateUrl: 'tpl/tag.html',
                                    resolve: load(['controllor/tagCtrl.js', ])
                                })
                                .state('app.tagAdd', {
                                    url: '/tag-add',
                                    templateUrl: 'tpl/tagAdd.html',
                                    resolve: load(['controllor/tagAddCtrl.js', 'fileupload', 'angularFileUpload', 'photo-gallery', 'cloud-image-loader'])
                                })
                                .state('app.tagEdit', {
                                    url: '/tag-edit/:tagId',
                                    templateUrl: 'tpl/tagEdit.html',
                                    resolve: load(['controllor/tagEditCtrl.js', 'fileupload', 'angularFileUpload', 'photo-gallery', 'cloud-image-loader'])
                                })
                                .state('app.catalogproduct', {
                                    url: '/catalogproduct',
                                    templateUrl: 'tpl/catalogproduct.html',
                                    resolve: load(['controllor/catalogproductCtrl.js'])
                                })
                                .state('app.catalogproductAdd', {
                                    url: '/catalogproduct-add',
                                    templateUrl: 'tpl/catalogproductAdd.html',
                                    resolve: load(['controllor/catalogproductAddCtrl.js', 'fileupload', 'angularFileUpload', 'photo-gallery', 'cloud-image-loader'])
                                })
                                .state('app.catalogproductEdit', {
                                    url: '/catalogproduct-edit/:itemId',
                                    templateUrl: 'tpl/catalogproductEdit.html',
                                    resolve: load(['controllor/catalogproductEditCtrl.js', 'fileupload', 'angularFileUpload', 'photo-gallery', 'cloud-image-loader'])
                                })
                                .state('app.catelogcategory', {
                                    url: '/catalog-category-list',
                                    pageName: 'catalogCategory',
                                    containerClass: '',
                                    templateUrl: 'tpl/catelogcategoryList.html',
                                    resolve: load(['controllor/catelogcategoryListCtrl.js'])
                                })
                                .state('app.catalogcategoryadd', {
                                    url: '/catalog-category-save',
                                    pageName: 'catalogCategory-save',
                                    containerClass: '',
                                    templateUrl: 'tpl/catelogcategoryAdd.html',
                                    resolve: load(['controllor/catelogcategoryAddCtrl.js', 'fileupload', 'angularFileUpload', 'photo-gallery', 'cloud-image-loader'])
                                })
                                .state('app.catalogcategoryedit', {
                                    url: '/catalog-category-edit/:id',
                                    pageName: 'catalog-categoryEdit',
                                    containerClass: '',
                                    templateUrl: 'tpl/catelogcategoryEdit.html',
                                    resolve: load(['controllor/catelogcategoryEditCtrl.js', 'fileupload', 'angularFileUpload', 'photo-gallery', 'cloud-image-loader'])
                                })
                                .state('app.purchaseInvoiceReturn', {
                                    url: '/purchaseInvoiceReturn',
                                    pageName: 'purchaseInvoiceReturn',
                                    containerClass: '',
                                    templateUrl: 'tpl/purchaseInvoiceReturnList.html',
                                    resolve: load(['controllor/purchaseInvoiceReturnCtrl.js'])
                                })
                                .state('app.purchaseInvoiceReturnAdd', {
                                    url: '/purchaseInvoiceReturn-add',
                                    pageName: 'purchaseInvoiceReturnAdd',
                                    containerClass: '',
                                    templateUrl: 'tpl/purchaseInvoiceReturnAdd.html',
                                    resolve: load(['controllor/purchaseInvoiceReturnAddCtrl.js', 'custom-textinput', 'custom-selectoption', 'custom-multiselect', 'custom-checkbox', 'custom-yes-or-no', 'custom-date'])
                                })
                                .state('app.purchaseInvoiceReturnEdit', {
                                    url: '/purchaseInvoiceReturn-edit/:id',
                                    pageName: 'purchaseInvoiceReturnEdit',
                                    containerClass: '',
                                    templateUrl: 'tpl/purchaseInvoiceReturnEdit.html',
                                    resolve: load(['controllor/purchaseInvoiceReturnEditCtrl.js', 'custom-textinput', 'custom-selectoption', 'custom-multiselect', 'custom-checkbox', 'custom-yes-or-no', 'custom-date'])
                                })
                                .state('app.purchaseInvoiceReturnView', {
                                    url: '/purchaseInvoiceReturn-view/:id',
                                    pageName: 'purchaseInvoiceReturnView',
                                    containerClass: '',
                                    templateUrl: 'tpl/purchase-return-view.html',
                                    resolve: load(['controllor/purchaseReturnViewCtrl.js', 'custom-textinput', 'custom-selectoption', 'custom-multiselect', 'custom-checkbox', 'custom-yes-or-no', 'custom-date'])
                                })
                                .state('app.accountsummary', {
                                    url: '/account-summary-report',
                                    pageName: 'account-summary-report',
                                    containerClass: '',
                                    templateUrl: 'tpl/accountsummaryreport.html',
                                    resolve: load(['controllor/accountSummaryreportCtrl.js'])
                                })
                                .state('app.accountdetail', {
                                    url: '/account-detail-report',
                                    pageName: 'account-detail-report',
                                    containerClass: '',
                                    templateUrl: 'tpl/accountDetailreport.html',
                                    resolve: load(['controllor/accountDetailreportCtrl.js'])
                                })
                                .state('app.itemwiseageingreport', {
                                    url: '/itemwise-ageing-report',
                                    pageName: 'itemwise-ageing-report',
                                    containerClass: '',
                                    templateUrl: 'tpl/itemwiseAgeingReport.html',
                                    resolve: load(['controllor/itemwiseAgeingReportCtrl.js'])
                                })
                                .state('app.categorywiseageingstockreport', {
                                    url: '/categorywise-ageing-report',
                                    pageName: 'categorywise-ageing-report',
                                    containerClass: '',
                                    templateUrl: 'tpl/categorywiseAgeing.html',
                                    resolve: load(['controllor/categorywiseAgeingCtrl.js'])
                                })
                                .state('app.stockadjustmentreport', {
                                    url: '/stock-adjustment-report',
                                    pageName: 'stock-adjustment-report',
                                    containerClass: '',
                                    templateUrl: 'tpl/stockAdjustmentReport.html',
                                    resolve: load(['controllor/stockAdjusmentReportCtrl.js'])
                                })
                                .state('app.stockwastagereport', {
                                    url: '/stock-wastage-report',
                                    pageName: 'stock-wastage-report',
                                    containerClass: '',
                                    templateUrl: 'tpl/stockWastageReport.html',
                                    resolve: load(['controllor/stockWastageReportCtrl.js'])
                                })
                                .state('app.categorystockadjustreport', {
                                    url: '/category-stock-adjust-report',
                                    pageName: 'category-stock-adjust-report',
                                    containerClass: '',
                                    templateUrl: 'tpl/categoryWiseStockAdjustmentReport.html',
                                    resolve: load(['controllor/categoryWiseStockAdjustReportCtrl.js'])
                                })
                                .state('app.categorystockwastagereport', {
                                    url: '/category-stock-wastage-report',
                                    pageName: 'category-stock-wastage-report',
                                    containerClass: '',
                                    templateUrl: 'tpl/categoryWiseStockWastageReport.html',
                                    resolve: load(['controllor/categoryWiseStockWastageReportCtrl.js'])
                                })
                                .state('app.employee', {
                                    url: '/employee-list',
                                    pageName: 'employee',
                                    containerClass: '',
                                    templateUrl: 'tpl/employeeList.html',
                                    resolve: load(['controllor/employeeListCtrl.js'])
                                })
                                .state('app.employeeEdit', {
                                    url: '/employee-edit/:id',
                                    pageName: 'employee-edit',
                                    containerClass: '',
                                    templateUrl: 'tpl/employeeEdit.html',
                                    resolve: load(['controllor/employeeEditCtrl.js', 'fileupload', 'angularFileUpload', 'photo-gallery', 'cloud-image-loader'])
                                })
                                .state('app.employeeAdd', {
                                    url: '/employee-add',
                                    pageName: 'employee-add',
                                    containerClass: '',
                                    templateUrl: 'tpl/employeeAdd.html',
                                    resolve: load(['controllor/employeeAddCtrl.js'])
                                })
                                .state('app.designation', {
                                    url: '/designation-list',
                                    pageName: 'designation',
                                    containerClass: '',
                                    templateUrl: 'tpl/designationList.html',
                                    resolve: load(['controllor/designationListCtrl.js'])
                                })
                                .state('app.designationEdit', {
                                    url: '/designation-edit/:id',
                                    pageName: 'designation-edit',
                                    containerClass: '',
                                    templateUrl: 'tpl/designationEdit.html',
                                    resolve: load(['controllor/designationEditCtrl.js'])
                                })
                                .state('app.designationAdd', {
                                    url: '/designation-add',
                                    pageName: 'designation-add',
                                    containerClass: '',
                                    templateUrl: 'tpl/designationAdd.html',
                                    resolve: load(['controllor/designationAddCtrl.js'])
                                })
                                .state('app.empsalesreport', {
                                    url: '/employee-sales-report',
                                    pageName: 'employee-sales-report',
                                    containerClass: '',
                                    templateUrl: 'tpl/employeeSalesReport.html',
                                    resolve: load(['controllor/employeeSalesReportCtrl.js'])
                                })
                                .state('app.notification', {
                                    url: '/notification-list',
                                    pageName: 'notification-list',
                                    containerClass: '',
                                    templateUrl: 'tpl/notification.html',

                                })
                                .state('app.deliverynote', {
                                    url: '/delivery-note',
                                    pageName: "deliverynote",
                                    templateUrl: 'tpl/deliverynoteList.html',
                                    resolve: load(['controllor/deliverynoteListCtrl.js'])
                                })
                                .state('app.deliverynoteAdd', {
                                    url: '/delivery-note-add/:type',
                                    pageName: "deliveryNoteAdd",
                                    templateUrl: 'tpl/deliverynoteAdd.html',
                                    resolve: load(['controllor/deliverynoteAddCtrl.js', 'controllor/newCustomerAddCtrl.js'])
                                })
                                .state('app.deliverynoteEdit', {
                                    url: '/delivery-note-edit/:id',
                                    pageName: "deliverynoteEdit",
                                    templateUrl: 'tpl/deliverynoteEdit.html',
                                    resolve: load(['controllor/deliverynoteEditCtrl.js'])
                                })
                                .state('app.deliverynoteView', {
                                    url: '/designation-view/:id',
                                    pageName: 'designation-view',
                                    containerClass: '',
                                    templateUrl: 'tpl/deliverynoteView.html',
                                    resolve: load(['controllor/deliverynoteViewCtrl.js'])
                                })
                                .state('app.deliverynotereport', {
                                    url: '/delivery-noteReport',
                                    pageName: "deliverynotereport",
                                    templateUrl: 'tpl/deliverynoteReport.html',
                                    resolve: load(['controllor/deliverynoteReportCtrl.js'])
                                }).state('app.daylogreport', {
                                    url: '/day-log-report',
                                    pageName: "daylogreport",
                                    templateUrl: 'tpl/daylogReport.html',
                                    resolve: load(['controllor/daylogReportCtrl.js'])
                                })
                                .state('app.vendor', {
                                    url: '/vendorList',
                                    pageName: 'vendor',
                                    containerClass: '',
                                    templateUrl: 'tpl/vendor-list.html',
                                    resolve: load(['controllor/vendorListCtrl.js'])
                                })
                                .state('app.vendoradd', {
                                    url: '/vendoradd',
                                    pageName: 'vendoradd',
                                    containerClass: '',
                                    templateUrl: 'tpl/vendoradd.html',
                                    resolve: load(['controllor/vendorAddCtrl.js', 'custom-textinput', 'custom-selectoption', 'custom-multiselect', 'custom-checkbox', 'custom-yes-or-no', 'custom-date'])
                                })
                                .state('app.vendorEdit', {
                                    url: '/vendorEdit/:id',
                                    pageName: 'vendorEdit',
                                    containerClass: '',
                                    templateUrl: 'tpl/vendor-edit.html',
                                    resolve: load(['controllor/vendorEditCtrl.js', 'custom-textinput', 'custom-selectoption', 'custom-multiselect', 'custom-checkbox', 'custom-yes-or-no', 'custom-date', 'fileupload', 'angularFileUpload', 'photo-gallery', 'cloud-image-loader'])
                                })
                                .state('app.hsncode', {
                                    url: '/hsncode',
                                    pageName: 'hsncode',
                                    containerClass: '',
                                    templateUrl: 'tpl/hsncode.html',
                                    resolve: load(['controllor/hsncodeCtrl.js'])
                                })
                                .state('app.hsncodeadd', {
                                    url: '/hsncode-add',
                                    pageName: 'hsncode-add',
                                    containerClass: '',
                                    templateUrl: 'tpl/hsncodeAdd.html',
                                    resolve: load(['controllor/hsncodeAddCtrl.js'])
                                })
                                .state('app.hsncodeedit', {
                                    url: '/hsncode-edit/:id',
                                    pageName: 'hsncode-edit',
                                    containerClass: '',
                                    templateUrl: 'tpl/hsncodeEdit.html',
                                    resolve: load(['controllor/hsncodeEditCtrl.js'])
                                })
                        function load(srcs, callback) {
                            return {
                                deps: ['$ocLazyLoad', '$q',
                                    function ($ocLazyLoad, $q) {
                                        var deferred = $q.defer();
                                        var promise = false;
                                        srcs = angular.isArray(srcs) ? srcs : srcs.split(/\s+/);
                                        if (!promise) {
                                            promise = deferred.promise;
                                        }
                                        angular.forEach(srcs, function (src) {
                                            promise = promise.then(function () {
                                                if (JQ_CONFIG[src]) {
                                                    return $ocLazyLoad.load(JQ_CONFIG[src]);
                                                }
                                                //lazy load the application component
                                                if (COMPONENT_CONFIG[src]) {
                                                    return $ocLazyLoad.load(COMPONENT_CONFIG[src]);
                                                }
                                                angular.forEach(MODULE_CONFIG, function (module) {
                                                    if (module.name == src) {
                                                        name = module.name;
                                                    } else {
                                                        name = src;
                                                    }
                                                });
                                                return $ocLazyLoad.load(name);
                                            });
                                        });
                                        deferred.resolve();
                                        return callback ? promise.then(function () {
                                            return callback();
                                        }) : promise;
                                    }]
                            }
                        }

                        $provide.decorator('$uiViewScroll', function ($delegate) {
                            return function (uiViewElement) {
                                $('html,body').animate({
                                    scrollTop: uiViewElement.offset().top
                                }, 500);
                            };
                        });
                    }
                ]
                );
