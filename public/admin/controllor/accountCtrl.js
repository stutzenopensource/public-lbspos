app.controller('accountCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$filter', 'Auth', '$timeout', '$httpService', '$localStorage', 'APP_CONST', 'ValidationFactory','$window', function ($scope, $rootScope, adminService, utilityService, $filter, Auth, $timeout, $httpService, $localStorage, APP_CONST, ValidationFactory,$window) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.accountModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
            id: '',
            pos_visibility: '',
            account: '',
            description: '',
            balance: '',
            account_no: '',
            status: '',
//             sku: '',

            serverList: null,
            isLoadingProgress: false
        };

        $scope.pagePerCount = [50, 100];
        $scope.accountModel.limit = $scope.pagePerCount[0];

        $scope.searchFilter = {
            id: '',
            start: '',
            account: '',
            bank_name: '',
            account_number: '',
            customerInfo: '',
            userInfo: '',
            pos_visibility: ''

        };

        $scope.searchFilterValue = "";
        {

        }
        ;

        $scope.refreshScreen = function ()
        {
            $scope.searchFilter.account = '';
            $scope.searchFilter.pos_visibility = '';
            $scope.getList();
        }

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.selectAccountId = '';
        $scope.showdeletePopup = false;
        $scope.isdeleteLoadingProgress = false;
        $scope.showPopup = function (id)
        {
            $scope.selectAccountId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function ()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteLoadingProgress = false;
        };

        $scope.deleteAccountInfo = function ( )
        {
            if ($scope.isdeleteLoadingProgress == false)
            {
                $scope.isdeleteLoadingProgress = true;
                var getListParam = {};
                getListParam.id = $scope.selectAccountId;
                var headers = {};
                headers['screen-code'] = 'account';
                adminService.deleteAccount(getListParam, getListParam.id, headers).then(function (response)
                {
                    var data = response.data;
                    if (data.success === true)
                    {
                        $scope.closePopup();
                        $scope.getList();
                    }
                    $scope.isdeleteLoadingProgress = false;
                });
            }
        };
        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'account_form';


        $scope.$on('onLocalStorageFormRenterCompleteEvent', function (event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });

        $scope.$on('onLocalStorageFieldRenterCompleteEvent', function (event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRenterCount++;
                }
                if ($scope.localStorageRenterCount >= $scope.localStorageCount)
                {
                    console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                    $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
                }

            }
        });
        $scope.$on('updateTypeaheadFieldValue', function (event, data) {

            if (data.fieldName == "account_form-account_dropdown")
            {
                $scope.searchFilter.account = data.value;
            }
        });



        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function (event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRetrieveCount++;
                }
                if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
                {
                    $scope.initTableFilter();
                }
                console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);
            }
        });

        $scope.init = function ()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');

        };

        $scope.$on('onLocalStorageReadyEvent', function () {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                $timeout(function () {
                    $scope.init();
                }, 300);

            }
        });
        $scope.getList = function () {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'account';
            getListParam.id = '';
            getListParam.account = $scope.searchFilter.account;
            getListParam.pos_visibility = $scope.searchFilter.pos_visibility;
            getListParam.bank_name = $scope.searchFilter.bank_name;
            getListParam.account_number = $scope.searchFilter.account_number;
            getListParam.start = ($scope.accountModel.currentPage - 1) * $scope.accountModel.limit;
            getListParam.limit = $scope.accountModel.limit;
            $scope.accountModel.isLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getAccountlist(getListParam, configOption, headers).then(function (response) {
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.accountModel.list = data;
                    $scope.accountModel.total = data.total;
                     if ($scope.accountModel.list.length > 0)
                    {
                        $rootScope.accountAdded = true;
                        $window.localStorage.setItem("accountAdded", "true");
                    }
                    if ($rootScope.userModel.new_user == 1 && $rootScope.accountNext != false && $rootScope.accountNext != undefined)
                    {
                        $window.localStorage.setItem("demo_guide", "true");
                        $rootScope.closeDemoPopup(6);
                    }
                }
                $scope.accountModel.isLoadingProgress = false;
            });

        };

//        $scope.getList();
    }]);




