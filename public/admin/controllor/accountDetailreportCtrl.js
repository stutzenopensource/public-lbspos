app.controller('accountDetailreportCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', 'accountFactory', function ($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout, accountFactory) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.accountDetailModel = {
            currentPage: 1,
            total: 0,
            Amount: 0,
            limit: 10,
            list: [],
            Accountlist: [],
            isLoadingProgress: false,
            isSearchLoadingProgress: false
        };
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.accountDetailModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
        $scope.validationFactory = ValidationFactory;
        $scope.openDate = function (index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            } else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };

        $scope.searchFilter = {
            fromdate: '',
            todate: '',
            account: ''
        };

        $scope.accountFactory = accountFactory.get();

        if ($scope.accountFactory != null && $scope.accountFactory != '' && $scope.accountFactory != undefined)
        {
            if ($scope.accountFactory.fromdate != '' && $scope.accountFactory.fromdate != null && $scope.accountFactory.fromdate != undefined)
            {
//                var fromdate = utilityService.parseDateToStr($scope.accountFactory.fromdate, $scope.dateFormat);
                $scope.searchFilter.fromdate = $scope.accountFactory.fromdate;
            }
            if ($scope.accountFactory.todate != '' && $scope.accountFactory.todate != null && $scope.accountFactory.todate != undefined)
            {
//                var todate = utilityService.parseDateToStr($scope.accountFactory.todate, $scope.dateFormat);
                $scope.searchFilter.todate = $scope.accountFactory.todate;;
            }
            if ($scope.accountFactory.account != '' && $scope.accountFactory.account != null && $scope.accountFactory.account != undefined)
            {
                $scope.searchFilter.account = $scope.accountFactory.account + '';
            }

        }
        accountFactory.set('');

//        $scope.searchFilter.fromdate = $scope.currentDate;
        $scope.clearFilters = function ()
        {
            $scope.searchFilter = {
                fromdate: '',
                todate: '',
                account: ''
            };
            $scope.accountDetailModel.list = [];
            $scope.searchFilter.fromdate = $scope.currentDate;
        }

        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.validateDateFilterData = function ()
        {
            var retVal = false;
            if ($scope.searchFilter.todate !== '' || $scope.searchFilter.fromdate !== '')
            {
                retVal = true;
            }
            return retVal;
        };

        $scope.getAccountlist = function ()
        {
            var getListParam = {};
            getListParam.id = "";
            getListParam.is_active = 1;
            getListParam.start = ($scope.accountDetailModel.currentPage - 1) * $scope.accountDetailModel.limit;
            getListParam.limit = $scope.accountDetailModel.limit;
            $scope.isDataSavingProcess = true;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getAccountlist(getListParam, configOption).then(function (response)
            {
                var data = response.data;
                $scope.accountDetailModel.Accountlist = data.list;

                for (var i = 0; i < $scope.accountDetailModel.Accountlist.length; i++)
                {
                    if ($scope.searchFilter.account == $scope.accountDetailModel.Accountlist[i].id)
                    {
                        $scope.accountName = $scope.accountDetailModel.Accountlist[i].account;
                    }
                }
                $scope.isDataSavingProcess = false;
            });
        };

        $scope.getList = function () {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'accountdetailreport';
            getListParam.from_date = $scope.searchFilter.fromdate;
            getListParam.to_date = $scope.searchFilter.todate;
            if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
            {
                getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
            }
            if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
            {
                getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
            }

            if ($scope.searchFilter.fromdate != null && $scope.searchFilter.fromdate != '')
            {
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);

            }
            if ($scope.searchFilter.todate != null && $scope.searchFilter.todate != '')
            {
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
            }
            getListParam.start = 0;
            getListParam.limit = 0;
            getListParam.show_all = 1;
            getListParam.account_id = $scope.searchFilter.account;
            $scope.accountDetailModel.isLoadingProgress = true;
            $scope.accountDetailModel.isSearchLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getAccountSummaryDetailReport(getListParam, configOption, headers).then(function (response)
            {
                var totalPayment = 0.00;
                var totalInvoiceAmt = 0.00;
                $scope.accountDetailModel.total_invoiceAmt = 0.00;
                $scope.accountDetailModel.total_paymentAmt = 0.00;
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.accountDetailModel.list = data;

                    for (var i = 0; i < $scope.accountDetailModel.list.length; i++)
                    {
                        $scope.accountDetailModel.list[i].newdate = utilityService.parseStrToDate($scope.accountDetailModel.list[i].date);
                        $scope.accountDetailModel.list[i].date = utilityService.parseDateToStr($scope.accountDetailModel.list[i].newdate, $scope.adminService.appConfig.date_format);

                        totalInvoiceAmt = totalInvoiceAmt + parseFloat($scope.accountDetailModel.list[i].total_amount);
                        totalPayment = totalPayment + parseFloat($scope.accountDetailModel.list[i].amount);
                    }
                    $scope.accountDetailModel.total_invoiceAmt = totalInvoiceAmt;
                    $scope.accountDetailModel.total_invoiceAmt = parseFloat($scope.accountDetailModel.total_invoiceAmt).toFixed(2);
                    $scope.accountDetailModel.total_invoiceAmt = utilityService.changeCurrency($scope.accountDetailModel.total_invoiceAmt, $rootScope.appConfig.thousand_seperator);

                    $scope.accountDetailModel.total_paymentAmt = totalPayment;
                    $scope.accountDetailModel.total_paymentAmt = parseFloat($scope.accountDetailModel.total_paymentAmt).toFixed(2);
                    $scope.accountDetailModel.total_paymentAmt = utilityService.changeCurrency($scope.accountDetailModel.total_paymentAmt, $rootScope.appConfig.thousand_seperator);

                }
                $scope.accountDetailModel.isLoadingProgress = false;
                $scope.accountDetailModel.isSearchLoadingProgress = false;
            });

        };

        $scope.print = function ()
        {
            $window.print();
        };

        $scope.getList();
        $scope.getAccountlist();
    }]);






