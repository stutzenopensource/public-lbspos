app.controller('accountStatementCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', function ($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.accountStatementModel = {
            currentPage: 1,
            total: 0,
            total_Amount: 0,
            limit: 10,
            list: [],
            length: [],
            date: '',
            invoice_id: '',
            customer_id: '',
            amount: '',
            status: '',
            serverList: null,
            isLoadingProgress: false,
            totaldebit: 0,
            totalcredit: 0,
            totalamt: 0,
            totalbalance: 0,
            openingBalance: null,
            accountList: [],
            accountName: '',
            isSearchLoadingProgress: false,
            openingCredit: '',
            openingDebit: '',
            openingAmount: ''
        };
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.accountStatementModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
        $scope.validationFactory = ValidationFactory;
        $scope.openDate = function (index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            } else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };

        $scope.searchFilter = {
            fromdate: '',
            todate: '',
            account: ''

        };
        $scope.clearFilters = function ()
        {
            $scope.searchFilter = {
                fromdate: '',
                todate: '',
                account: ''
            };
            $scope.accountStatementModel.list = [];
            $scope.accountStatementModel.openingBalance = null;
        }
        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.validateDateFilterData = function ()
        {
            var retVal = false;
            if ($scope.searchFilter.todate !== '' || $scope.searchFilter.fromdate !== '')
            {
                retVal = true;
            }
            return retVal;
        };

        $scope.calculateTotal = function ()
        {
            var totDebit = 0;
            var totCredit = 0;
            var totamt = 0;
            for (var i = 0; i < $scope.accountStatementModel.list.length; i++)
            {
                totDebit += parseFloat($scope.accountStatementModel.list[i].debit);
                $scope.accountStatementModel.totaldebit = totDebit;

                totCredit += parseFloat($scope.accountStatementModel.list[i].credit);
                $scope.accountStatementModel.totalcredit = totCredit;

                totamt += parseFloat($scope.accountStatementModel.list[i].amount);
                $scope.accountStatementModel.totalamt = totamt;

            }
            var openingcred = parseFloat($scope.accountStatementModel.openingBalance.credit);
            var openingdeb = parseFloat($scope.accountStatementModel.openingBalance.debit);
            $scope.accountStatementModel.totaldebit += parseFloat(openingdeb);

            $scope.accountStatementModel.totalcredit += parseFloat(openingcred);

            $scope.accountStatementModel.totaldebit = parseFloat($scope.accountStatementModel.totaldebit).toFixed(2);
            $scope.accountStatementModel.totaldebit = utilityService.changeCurrency($scope.accountStatementModel.totaldebit, $rootScope.appConfig.thousand_seperator);
            $scope.accountStatementModel.totalcredit = parseFloat($scope.accountStatementModel.totalcredit).toFixed(2);
            $scope.accountStatementModel.totalcredit = utilityService.changeCurrency($scope.accountStatementModel.totalcredit, $rootScope.appConfig.thousand_seperator);
        }

        $scope.updateBalanceAmount = function ()
        {
            if ($scope.accountStatementModel.list.length >= 1)
            {
                var openingBalance = parseFloat($scope.accountStatementModel.openingBalance.credit) - parseFloat($scope.accountStatementModel.openingBalance.debit);
                //  $scope.accountStatementModel.list[0].balance = openingBalance;
                for (var i = 0; i < $scope.accountStatementModel.list.length; i++)
                {
                    var credit = $scope.accountStatementModel.list[i].credit == null ? 0 : parseFloat($scope.accountStatementModel.list[i].credit);
                    var debit = $scope.accountStatementModel.list[i].debit == null ? 0 : parseFloat($scope.accountStatementModel.list[i].debit);
                    if (credit != 0)
                    {
                        openingBalance = openingBalance + credit;
                    } else if (debit != 0)
                    {
                        openingBalance = openingBalance - debit;
                    }
                    $scope.accountStatementModel.list[i].balance = openingBalance;
                }

                $scope.accountStatementModel.totalbalance = openingBalance;
                $scope.accountStatementModel.totalbalance = parseFloat($scope.accountStatementModel.totalbalance).toFixed(2);
                $scope.accountStatementModel.totalbalance = utilityService.changeCurrency($scope.accountStatementModel.totalbalance, $rootScope.appConfig.thousand_seperator);



            } else
            {
                $scope.accountStatementModel.totalbalance = 0.00;
            }
        }

        $scope.getAccountlist = function ()
        {
            var getListParam = {};
            getListParam.id = "";
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getAccountlist(getListParam, configOption).then(function (response)
            {
                var data = response.data;
                $scope.accountStatementModel.accountList = data.list;
            });
        };

        $scope.findAccountName = function ()
        {
            if ($scope.searchFilter.account != null && $scope.searchFilter.account != '')
            {
                for (var i = 0; i < $scope.accountStatementModel.accountList.length; i++)
                {
                    if ($scope.searchFilter.account == $scope.accountStatementModel.accountList[i].id)
                    {
                        $scope.accountStatementModel.accountName = $scope.accountStatementModel.accountList[i].account;
                    }
                }
            }
        }

        $scope.getList = function () {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'accountstatement';
            if ($scope.searchFilter.todate != '' && ($scope.searchFilter.fromdate == null || $scope.searchFilter.fromdate == ''))
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_Date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                getListParam.to_Date = utilityService.changeDateToSqlFormat(getListParam.to_Date, $scope.dateFormat);
                getListParam.from_Date = getListParam.to_Date;
            } else if ($scope.searchFilter.fromdate != '' && ($scope.searchFilter.todate == null || $scope.searchFilter.todate == ''))
            {
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_Date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                getListParam.from_Date = utilityService.changeDateToSqlFormat(getListParam.from_Date, $scope.dateFormat);
                getListParam.to_Date = getListParam.from_Date;
            } else
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_Date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_Date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                getListParam.to_Date = utilityService.changeDateToSqlFormat(getListParam.to_Date, $scope.dateFormat);
                getListParam.from_Date = utilityService.changeDateToSqlFormat(getListParam.from_Date, $scope.dateFormat);
            }
            //            if ($scope.searchFilter.todate !== '' && $scope.searchFilter.fromdate === '')
            //            {
            //                getListParam.to_Date = $filter('date')($scope.searchFilter.todate, $scope.dateFormat);
            //                getListParam.from_Date = $filter('date')($scope.searchFilter.todate, $scope.dateFormat);
            //            }
            //            else if ($scope.searchFilter.fromdate !== '' && $scope.searchFilter.todate === '')
            //            {
            //                getListParam.to_Date = $filter('date')($scope.searchFilter.fromdate, $scope.dateFormat);
            //                getListParam.from_Date = $filter('date')($scope.searchFilter.fromdate, $scope.dateFormat);
            //            }
            //            else
            //            {
            //                getListParam.to_Date = $filter('date')($scope.searchFilter.todate, $scope.dateFormat);
            //                getListParam.from_Date = $filter('date')($scope.searchFilter.fromdate, $scope.dateFormat);
            //            }
            getListParam.account_id = $scope.searchFilter.account;
            getListParam.start = 0;
            getListParam.limit = 0;
            $scope.accountStatementModel.isLoadingProgress = true;
            $scope.accountStatementModel.isSearchLoadingProgress = true;
            if ($scope.searchFilter.todate != '' && $scope.searchFilter.todate != null && $scope.searchFilter.todate != 'undefined' && $scope.searchFilter.fromdate != '' && $scope.searchFilter.fromdate != null && $scope.searchFilter.account !='')
            {
                adminService.getAccountStatementList(getListParam, headers).then(function (response)
                {
                    if (response.data.success === true)
                    {
                        var data = response.data.list;
                        $scope.accountStatementModel.list = data;
                        $scope.accountStatementModel.openingBalance = response.data.openBalance;
                        $scope.accountStatementModel.openingBalance.credit = parseFloat($scope.accountStatementModel.openingBalance.credit).toFixed(2);
                        $scope.accountStatementModel.openingBalance.debit = parseFloat($scope.accountStatementModel.openingBalance.debit).toFixed(2);
                        var openingBalance = parseFloat($scope.accountStatementModel.openingBalance.credit) - parseFloat($scope.accountStatementModel.openingBalance.debit);
                        $scope.accountStatementModel.openingBalance.balance = openingBalance;
                        $scope.accountStatementModel.openingBalance.balance = parseFloat($scope.accountStatementModel.openingBalance.balance).toFixed(2);
                        $scope.accountStatementModel.openingCredit = utilityService.changeCurrency($scope.accountStatementModel.openingBalance.credit, $rootScope.appConfig.thousand_seperator);
                        $scope.accountStatementModel.openingDebit = utilityService.changeCurrency($scope.accountStatementModel.openingBalance.debit, $rootScope.appConfig.thousand_seperator);

                        $scope.accountStatementModel.openingAmount = utilityService.changeCurrency($scope.accountStatementModel.openingBalance.balance, $rootScope.appConfig.thousand_seperator);

                        if ($scope.accountStatementModel.list.length != 0)
                        {
                            $scope.updateBalanceAmount();
                            $scope.calculateTotal();
                            for (var i = 0; i < $scope.accountStatementModel.list.length; i++)
                            {
                                $scope.accountStatementModel.list[i].newdate = utilityService.parseStrToDate($scope.accountStatementModel.list[i].transaction_date);
                                $scope.accountStatementModel.list[i].transaction_date = utilityService.parseDateToStr($scope.accountStatementModel.list[i].newdate, $scope.adminService.appConfig.date_format);
                                $scope.accountStatementModel.list[i].debit = utilityService.changeCurrency($scope.accountStatementModel.list[i].debit, $rootScope.appConfig.thousand_seperator);
                                $scope.accountStatementModel.list[i].credit = utilityService.changeCurrency($scope.accountStatementModel.list[i].credit, $rootScope.appConfig.thousand_seperator);
//                            $scope.accountStatementModel.list[i].balance = Math.abs($scope.accountStatementModel.list[i].balance);
                                $scope.accountStatementModel.list[i].balance = parseFloat($scope.accountStatementModel.list[i].balance).toFixed(2);
                                $scope.accountStatementModel.list[i].balance = utilityService.changeCurrency($scope.accountStatementModel.list[i].balance, $rootScope.appConfig.thousand_seperator);

                            }
                        } else
                        {
                            $scope.accountStatementModel.totaldebit = $scope.accountStatementModel.openingDebit;
                            $scope.accountStatementModel.totalcredit = $scope.accountStatementModel.openingCredit;
                            $scope.accountStatementModel.totalbalance = $scope.accountStatementModel.openingAmount;
                        }
                        $scope.accountStatementModel.total = data.total;
                    }
                    $scope.accountStatementModel.isLoadingProgress = false;
                    $scope.accountStatementModel.isSearchLoadingProgress = false;
                });
            } else {
                $scope.accountStatementModel.isLoadingProgress = false;
                $scope.accountStatementModel.isSearchLoadingProgress = false;
            }

        };

        $scope.print = function (div)
        {
            var docHead = document.head.outerHTML;
            var printContents = document.getElementById(div).outerHTML;
            var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=865, height=600, resizable=yes, screenX=200, screenY=200, personalbar=no, scrollbars=yes";

            var newWin = window.open("", "_blank", winAttr);
            var writeDoc = newWin.document;
            writeDoc.open();
            writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
            writeDoc.close();
            newWin.focus();
        };
        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'partysales_form';


        $scope.$on('onLocalStorageFormRenterCompleteEvent', function (event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });

        $scope.$on('onLocalStorageFieldRenterCompleteEvent', function (event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRenterCount++;
                }
                if ($scope.localStorageRenterCount >= $scope.localStorageCount)
                {
                    console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                    $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
                }

            }
        });

        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function (event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRetrieveCount++;
                }
                if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
                {
                    $scope.initTableFilter();
                }
                console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);
            }
        });

        $scope.init = function ()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');

        };

        $scope.$on('onLocalStorageReadyEvent', function () {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                $timeout(function () {
                    $scope.init();
                }, 300);

            }
        });

        //$scope.getList();
        $scope.getAccountlist();
    }]);




