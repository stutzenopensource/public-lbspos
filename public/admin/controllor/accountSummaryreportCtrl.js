app.controller('accountSummaryreportCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', 'accountFactory', function ($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout, accountFactory) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.accountsummaryModel = {
            currentPage: 1,
            limit: 10,
            list:[],
            isLoadingProgress: false,
            isSearchLoadingProgress: false,
            Accountlist: []
        };
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.accountsummaryModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
        $scope.validationFactory = ValidationFactory;
        $scope.openDate = function (index) {
            if (index === 0)
            {
                $scope.fromDateOpen = true;
            } else if (index === 1)
            {
                $scope.toDateOpen = true;
            }
        };

        $scope.searchFilter = {
            fromdate: '',
            todate: '',
            account: ''
        };
//        $scope.searchFilter.fromdate = $scope.currentDate;
        $scope.clearFilters = function ()
        {
            $scope.searchFilter = {
                fromdate: '',
                todate: '',
                account: ''
            };
            $scope.accountsummaryModel.list = [];
//            $scope.searchFilter.fromdate = $scope.currentDate;
        }

        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.validateDateFilterData = function ()
        {
            var retVal = false;
            if ($scope.searchFilter.todate !== '' || $scope.searchFilter.fromdate !== '')
            {
                retVal = true;
            }
            return retVal;
        };
        $scope.redirectToAccountDetail = function (account)
        {
            var accountInfo = {};
            accountInfo.fromdate = $scope.searchFilter.fromdate;
            accountInfo.todate = $scope.searchFilter.todate;
            for (var i = 0; i < $scope.accountsummaryModel.Accountlist.length; i++)
            {
                if (account.toLowerCase() == $scope.accountsummaryModel.Accountlist[i].account.toLowerCase())
                {
                    accountInfo.account = $scope.accountsummaryModel.Accountlist[i].id;
                }
            }
            accountFactory.set(accountInfo);
            $state.go("app.accountdetail");
        };

        $scope.getAccountlist = function ()
        {
            var getListParam = {};
            getListParam.id = "";
            getListParam.is_active = 1;
            getListParam.start = ($scope.accountsummaryModel.currentPage - 1) * $scope.accountsummaryModel.limit;
            getListParam.limit = $scope.accountsummaryModel.limit;
            $scope.isDataSavingProcess = true;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getAccountlist(getListParam, configOption).then(function (response)
            {
                var data = response.data;
                $scope.accountsummaryModel.Accountlist = data.list;
                $scope.isDataSavingProcess = false;
            });
        };

        $scope.getList = function () {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'accountsummaryreport';
            getListParam.from_date = $scope.searchFilter.fromdate;
            getListParam.to_date = $scope.searchFilter.todate;
            if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
            {
                getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
            }
            if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
            {
                getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
            }

            if ($scope.searchFilter.fromdate != null && $scope.searchFilter.fromdate != '')
            {
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
            }
            if ($scope.searchFilter.todate != null && $scope.searchFilter.todate != '')
            {
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
            }
            getListParam.account_id = $scope.searchFilter.account;
            getListParam.start = 0;
            getListParam.limit = 0;
            $scope.accountsummaryModel.isLoadingProgress = true;
            $scope.accountsummaryModel.isSearchLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getAccountSummaryDetailReport(getListParam, configOption, headers).then(function (response)
            {
                var totalAmt = 0.00;
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.accountsummaryModel.list = data;
                    for (var i = 0; i < $scope.accountsummaryModel.list.length; i++)
                    {
                        totalAmt = totalAmt + parseFloat($scope.accountsummaryModel.list[i].amount);
                    }
                    for (var i = 0; i < $scope.accountsummaryModel.Accountlist.length; i++)
                    {
                        if ($scope.searchFilter.account == $scope.accountsummaryModel.Accountlist[i].id)
                        {
                            $scope.accountName = $scope.accountsummaryModel.Accountlist[i].account;
                        }
                    }
                    $scope.accountsummaryModel.totalAmount = totalAmt.toFixed(2);
                    $scope.accountsummaryModel.isLoadingProgress = false;
                    $scope.accountsummaryModel.isSearchLoadingProgress = false;
                }
            });
        };

        $scope.print = function ()
        {
            $window.print();
        };

//        $scope.getList();
        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'account_summary_form';
        $scope.$on('onLocalStorageFormRenterCompleteEvent', function (event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });

        $scope.$on('onLocalStorageFieldRenterCompleteEvent', function (event, formName) {

            if ($scope.localStorageFormName == formName)
            {
                $scope.localStorageRenterCount++;
            }
            if ($scope.localStorageRenterCount >= $scope.localStorageCount)
            {
                console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
            }


        });

        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function (event, formName) {

            if ($scope.localStorageFormName == formName)
            {
                $scope.localStorageRetrieveCount++;
            }
            if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded && $scope.searchFilter.fromdate !='' && $scope.searchFilter.todate !='')
            {
                $scope.initTableFilter();
            }
            console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);

        });

        $scope.init = function ()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');

        };

        $scope.$on('onLocalStorageReadyEvent', function () {

            $timeout(function () {
                $scope.init();
            }, 300);
        });

        $scope.getAccountlist();
         $scope.winprint = function (div)
        {
           var docHead = document.head.outerHTML;
            var printContents = document.getElementById(div).outerHTML;
            var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=865, height=600, resizable=yes, screenX=200, screenY=200, personalbar=no, scrollbars=yes";

            var newWin = window.open("", "_blank", winAttr);
            var writeDoc = newWin.document;
            writeDoc.open();
            writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
            writeDoc.close();
            newWin.focus();
        };

    }]);






