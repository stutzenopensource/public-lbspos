app.controller('acodeStatementCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', function ($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout) {

    $rootScope.getNavigationBlockMsg = null;

    $scope.accountStatementModel = {
        currentPage: 1,
        total: 0,
        total_Amount: 0,
        limit: 10,
        list: [],
        length: [],
        date: '',
        invoice_id: '',
        customer_id: '',
        amount: '',
        status: '',
        serverList: null,
        isLoadingProgress: false,
        totaldebit: 0,
        totalcredit: 0,
        totalamt: 0,
        totalbalance: 0,
        openingBalance: null,
        accountList: [],
        accountName: '',
        isSearchLoadingProgress: false,
        openingCredit: '',
        openingDebit: '',
        openingAmount: ''
    };
    $scope.adminService = adminService;
    $scope.pagePerCount = [50, 100];
    $scope.accountStatementModel.limit = $scope.pagePerCount[0];
    $scope.fromDateOpen = false;
    $scope.toDateOpen = false;
    $scope.dateFormat = $rootScope.appConfig.date_format;
    $scope.currentDate = new Date();
    $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
    $scope.validationFactory = ValidationFactory;
    $scope.openDate = function (index) {

        if (index === 0)
        {
            $scope.fromDateOpen = true;
        } else if (index === 1)
{
            $scope.toDateOpen = true;
        }

    };

    $scope.searchFilter = {
        fromdate: '',
        todate: '',
        acode: ''

    };
    $scope.searchFilter.todate = $scope.currentDate;
    $scope.clearFilters = function ()
    {
        $scope.searchFilter = {
            fromdate: '',
            todate: '',
            acode: ''
        };
        $scope.accountStatementModel.list = [];
        $scope.searchFilter.todate = $scope.currentDate;
    }
    $scope.searchFilterValue = "";

    $scope.initTableFilterTimeoutPromise = null;

    $scope.initTableFilter = function ()
    {
        if ($scope.initTableFilterTimeoutPromise !== null)
        {
            $timeout.cancel($scope.initTableFilterTimeoutPromise);
        }
        $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
    };
    $scope.validateDateFilterData = function ()
    {
        var retVal = false;
        if ($scope.searchFilter.todate !== '' || $scope.searchFilter.fromdate !== '')
        {
            retVal = true;
        }
        return retVal;
    };
    $scope.calculateTotal = function ()
    {
        var totDebit = 0;
        var totCredit = 0;
        var totamt = 0;
        $scope.accountStatementModel.totaldebit = 0.00;
        $scope.accountStatementModel.totalcredit = 0.00;
        for (var i = 0; i < $scope.accountStatementModel.list.length; i++)
        {
            $scope.accountStatementModel.list[i].credit = (typeof $scope.accountStatementModel.list[i].credit == 'undefined' || $scope.accountStatementModel.list[i].credit == null || isNaN($scope.accountStatementModel.list[i].credit)) ? 0 : parseFloat($scope.accountStatementModel.list[i].credit);
            $scope.accountStatementModel.list[i].debit = (typeof $scope.accountStatementModel.list[i].debit == 'undefined' || $scope.accountStatementModel.list[i].debit == null || isNaN($scope.accountStatementModel.list[i].debit)) ? 0 : parseFloat($scope.accountStatementModel.list[i].debit);
            $scope.accountStatementModel.list[i].balance = parseFloat($scope.accountStatementModel.list[i].credit - $scope.accountStatementModel.list[i].debit);
            $scope.accountStatementModel.list[i].displayCredit = 0;
            $scope.accountStatementModel.list[i].displayDebit = 0;
            if ($scope.accountStatementModel.list[i].balance > 0)
            {
                $scope.accountStatementModel.list[i].displayCredit = $scope.accountStatementModel.list[i].balance;
                $scope.accountStatementModel.list[i].displayDebit = 0;
            }
            if ($scope.accountStatementModel.list[i].balance < 0)
            {
                $scope.accountStatementModel.list[i].displayDebit = Math.abs($scope.accountStatementModel.list[i].balance);
                $scope.accountStatementModel.list[i].displayCredit = 0;
            }
            totDebit += parseFloat($scope.accountStatementModel.list[i].displayDebit);
            totCredit += parseFloat($scope.accountStatementModel.list[i].displayCredit);
        }
        $scope.accountStatementModel.totaldebit = totDebit;
        $scope.accountStatementModel.totaldebit = parseFloat($scope.accountStatementModel.totaldebit).toFixed(2);
        $scope.accountStatementModel.totaldebit = utilityService.changeCurrency($scope.accountStatementModel.totaldebit, $rootScope.appConfig.thousand_seperator);
        $scope.accountStatementModel.totalcredit = totCredit;
        $scope.accountStatementModel.totalcredit = parseFloat($scope.accountStatementModel.totalcredit).toFixed(2);
        $scope.accountStatementModel.totalcredit = utilityService.changeCurrency($scope.accountStatementModel.totalcredit, $rootScope.appConfig.thousand_seperator);

    }

    $scope.getList = function () {

        var getListParam = {};
        var headers = {};
        headers['screen-code'] = 'acodestatement';
        if ($scope.searchFilter.todate != '' && ($scope.searchFilter.fromdate == null || $scope.searchFilter.fromdate == ''))
        {
            if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
            {
                getListParam.to_Date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
            }
            getListParam.to_Date = utilityService.changeDateToSqlFormat(getListParam.to_Date, $scope.dateFormat);         //  getListParam.from_Date = getListParam.to_Date;
        } else if ($scope.searchFilter.fromdate != '' && ($scope.searchFilter.todate == null || $scope.searchFilter.todate == ''))
{
            if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
            {
                getListParam.from_Date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
            }
            getListParam.from_Date = utilityService.changeDateToSqlFormat(getListParam.from_Date, $scope.dateFormat);
        } else
{
            if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
            {
                getListParam.to_Date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
            }
            if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
            {
                getListParam.from_Date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
            }
            getListParam.to_Date = utilityService.changeDateToSqlFormat(getListParam.to_Date, $scope.dateFormat);
            getListParam.from_Date = utilityService.changeDateToSqlFormat(getListParam.from_Date, $scope.dateFormat);
        }
        if($scope.searchFilter.acode != '' && typeof $scope.searchFilter.acode != 'undefined' && $scope.searchFilter.acode != null)
        {
            getListParam.acode = $scope.searchFilter.acode.code;
        }
        getListParam.prefix = $scope.searchFilter.prefix;
        getListParam.start = 0;
        getListParam.limit = 0;
        $scope.accountStatementModel.isLoadingProgress = true;
        $scope.accountStatementModel.isSearchLoadingProgress = true;
        adminService.getAcodeStatement(getListParam, headers).then(function (response)
        {
            if (response.data.success === true)
            {
                var data = response.data;
                $scope.accountStatementModel.list = data.list;
                if ($scope.accountStatementModel.list.length !== 0)
                {

                    $scope.calculateTotal();
                    for (var i = 0; i < $scope.accountStatementModel.list.length; i++)
                    {
                        $scope.accountStatementModel.list[i].newdate = utilityService.parseStrToDate($scope.accountStatementModel.list[i].transaction_date);
                        $scope.accountStatementModel.list[i].transaction_date = utilityService.parseDateToStr($scope.accountStatementModel.list[i].newdate, $scope.adminService.appConfig.date_format);
                        $scope.accountStatementModel.list[i].displayCredit = parseFloat($scope.accountStatementModel.list[i].displayCredit).toFixed(2);
                        $scope.accountStatementModel.list[i].displayCredit = utilityService.changeCurrency($scope.accountStatementModel.list[i].displayCredit, $rootScope.appConfig.thousand_seperator);
                        $scope.accountStatementModel.list[i].displayDebit = parseFloat($scope.accountStatementModel.list[i].displayDebit).toFixed(2);
                        $scope.accountStatementModel.list[i].displayDebit = utilityService.changeCurrency($scope.accountStatementModel.list[i].displayDebit, $rootScope.appConfig.thousand_seperator);
                    }
                }
                $scope.accountStatementModel.total = data.total;
            }
            $scope.accountStatementModel.isLoadingProgress = false;
            $scope.accountStatementModel.isSearchLoadingProgress = false;
        });

    };

    $scope.print = function ()
    {
        $window.print();
    };
    $scope.getAcodeList = function (val)
    {
        var autosearchParam = {};
        autosearchParam.code = val;
        //  autosearchParam.ref_type = 'customer';
        return $httpService.get(APP_CONST.API.ACODE_SEARCH, autosearchParam, false).then(function (responseData)
        {
            var data = responseData.data.list;
            var hits = data;
            if (hits.length > 10)
            {
                hits.splice(10, hits.length);
            }
            return hits;
        });
    };
    $scope.formatAcodeModel = function (model)
    {
        if (model != null)
        {
            return model.code;
        }
        return  '';
    };
       $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'partysales_form';


        $scope.$on('onLocalStorageFormRenterCompleteEvent', function(event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });  
        
          $scope.$on('onLocalStorageFieldRenterCompleteEvent', function(event, formName) {
             if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRenterCount++;
                }
                if ($scope.localStorageRenterCount >= $scope.localStorageCount)
                {
                    console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                    $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
                }
            
              }
        });
        $scope.$on('updateTypeaheadFieldValue', function(event, data) {

            if (data.fieldName == "partysales_form-acodeDropDown")
            {
                $scope.searchFilter.acode = data.value;
            }
        });
        
        

        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function(event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRetrieveCount++;
                }
                if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
                {
                    $scope.initTableFilter();
                }
                console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);
            }
        });

        $scope.init = function()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');

        };

        $scope.$on('onLocalStorageReadyEvent', function() {
           if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                $timeout(function() {
                    $scope.init();
                }, 300);
            
            }
        });            
    
    $scope.getList();
}]);




