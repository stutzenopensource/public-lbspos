app.controller('advancepaymentAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', 'sweet', function ($scope, $rootScope, adminService, $httpService, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST, sweet) {

        $scope.paymentAddModel =
                {
                    "id": 0,
                    "account": '',
                    "date": '',
                    "list": [],
                    "comments": "",
                    "amount": '',
                    "payment_mode": '',
                    "paymentlist": [],
                    "salesorder_id": '',
                    "customerInfo": {},
                    "customer_id": '',
                    "categoryList": [],
                    "accountsList": [],
                    "category": '',
                    "salesorder_code": '',
                    "salesorderList": [],
                    "invoiceDetails": {}
                }


        $scope.adminService = adminService;
        $scope.validationFactory = ValidationFactory;
        $scope.getNavigationBlockMsg = function (pageReload)
        {
            if (typeof $scope.save_payment_form != 'undefined' && typeof $scope.save_payment_form.$pristine != 'undefined' && !$scope.save_payment_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.isDataSavingProcess = false;
        $scope.isSalesOrderLoading = false;

        $scope.formReset = function ( )
        {
            $scope.paymentAddModel.quote_id = '';
            $scope.paymentAddModel.customer_id = '';
            $scope.paymentAddModel.account = "";
            $scope.paymentAddModel.invoicedate = "";
            $scope.paymentAddModel.payment_mode = '';
            $scope.paymentAddModel.paymentdefault = '';
            $scope.paymentAddModel.amount = "";
            $scope.paymentAddModel.comments = "";
            $scope.paymentAddModel.invoiceDetails = {};
        }


        $scope.paymentDateOpen = false;
        $scope.openDate = function (index)
        {
            if (index == 0)
            {
                $scope.paymentDateOpen = true;
            }

        }

        $scope.getCustomerList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = [];
                if (data.length > 0)
                {
                    for (var i = 0; i < data.length; i++)
                    {
                        if (data[i].is_sale == '1' || data[i].is_sale == 1) {
                            hits.push(data[i]);
                        }
                    }
                }
                if (hits.length > 10) {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatCustomerModel = function (model)
        {
            if (model != null)
            {
                var phoneNumber = '';
                if (model.phone != undefined)
                {
                    if (model.phone.indexOf(','))
                    {
                        phoneNumber = model.phone.split(',')[0];
                    }
                }
                if (model.fname != undefined && phoneNumber != '' && model.email != undefined)
                {

                    return model.fname + '(' + phoneNumber + ',' + model.email + ')';
                } else if (model.fname != undefined && phoneNumber != '') {

                    return model.fname + '(' + phoneNumber + ')';
                } else if (model.fname != undefined)
                {
                    return model.fname;
                }
            }
            return  '';
        };
        $scope.custDrop = function () {
            if ($scope.paymentAddModel.customerInfo.id == undefined) {
                $scope.paymentAddModel.customerInfo = '';
            }
        };


//        $scope.$watch('paymentAddModel.customerInfo', function (newVal, oldVal) {
//            if (typeof newVal == 'undefined' || newVal == '' || newVal == null)
//            {
//                $scope.paymentAddModel.customerInfo = '';
//            } else
//            {
//                var addrss = "";
//
//                if (newVal.billing_address != undefined && newVal.billing_address != '')
//                {
//                    addrss += newVal.billing_address;
//                }
//                if (newVal.billing_city != undefined && newVal.billing_city != '')
//                {
//                    addrss += '\n' + newVal.billing_city;
//                }
//                if (newVal.billing_pincode != undefined && newVal.billing_pincode != '')
//                {
//                    if (newVal.billing_city != undefined && newVal.billing_city != '')
//                    {
//                        addrss += '-' + newVal.billing_pincode;
//                    } else
//                    {
//                        addrss += newVal.billing_pincode;
//                    }
//                }
//
//                if (newVal.billing_state != undefined && newVal.billing_state != '')
//                {
//                    addrss += '\n' + newVal.billing_state;
//                }
//
//                $scope.paymentAddModel.customerInfo.shopping_address = addrss;
//            }
//        });

        $scope.getsalesorderList = function ()
        {
            var getListParam = {};
            if ($scope.paymentAddModel.customerInfo != null && typeof $scope.paymentAddModel.customerInfo != 'undefined' && typeof $scope.paymentAddModel.customerInfo.id != 'undefined')
            {
                $scope.isSalesOrderLoading = true;
                getListParam.customerid = $scope.paymentAddModel.customerInfo.id;
                getListParam.status = 'new';
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getSalesOrderList(getListParam, configOption).then(function (response)
                {
                    if (response.data.success === true) {
                        var data = response.data.list;
                        $scope.paymentAddModel.salesorderList = data;
                        if ($scope.paymentAddModel.salesorderList.length > 0)
                        {
                            $scope.paymentAddModel.salesorder_id = $scope.paymentAddModel.salesorderList[0].id;
                            $scope.getInvoiceDetails(0);
                        } else
                        {
                            $scope.paymentAddModel.invoiceDetails = '';
                            $scope.paymentAddModel.salesorder_id = '';
                        }
                        $scope.isSalesOrderLoading = false;
                    }
                });
            }
        };

        $scope.getInvoiceDetails = function (index)
        {
            $scope.paymentAddModel.invoiceDetails = $scope.paymentAddModel.salesorderList[index];
        };

        $scope.getIncomeCategoryList = function () {

            var getListParam = {};
            getListParam.id = '';
            getListParam.is_active = 1;
            getListParam.type = 'income';
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;

            adminService.getIncomeCategoryList(getListParam, configOption).then(function (response) {
                var data = response.data.list;
                $scope.paymentAddModel.categoryList = data;
            });

        };
        $scope.getAccountlist = function () {

            var getListParam = {};
            getListParam.id = '';
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getAccountlist(getListParam, configOption).then(function (response) {
                var data = response.data.list;
                $scope.paymentAddModel.accountsList = data;
            });

        };

        $scope.createAdvancePayment = function ()
        {
            $scope.isDataSavingProcess = true;
            var createAdvanceParam = {};
            if ($scope.paymentAddModel.customerInfo !== null && typeof $scope.paymentAddModel.customerInfo !== 'undefined' && typeof $scope.paymentAddModel.customerInfo.id !== 'undefined')
            {
                createAdvanceParam.customer_id = $scope.paymentAddModel.customerInfo.id;
            }
            createAdvanceParam.comments = $scope.paymentAddModel.comments;
            if (typeof $scope.paymentAddModel.date == 'object')
            {
                var date = utilityService.parseDateToStr($scope.paymentAddModel.date, 'yyyy-MM-dd');
            }
            createAdvanceParam.date = utilityService.changeDateToSqlFormat(date, 'yyyy-MM-dd');

            createAdvanceParam.amount = $scope.paymentAddModel.amount;
            createAdvanceParam.payment_mode = $scope.paymentAddModel.payment_mode;
            createAdvanceParam.reference_no = '';
            createAdvanceParam.tra_category = $scope.paymentAddModel.category
            createAdvanceParam.voucher_type = "advance_invoice";
            createAdvanceParam.voucher_no = 0;
            if ($scope.paymentAddModel.salesorder_id != '' && $scope.paymentAddModel.salesorder_id != null && $scope.paymentAddModel.salesorder_id != 'undefined') {
                createAdvanceParam.voucher_no = $scope.paymentAddModel.salesorder_id;
            }
            createAdvanceParam.account_id = $scope.paymentAddModel.account_id;
            for (var i = 0; i < $scope.paymentAddModel.accountsList.length; i++)
            {
                if ($scope.paymentAddModel.accountsList[i].id == $scope.paymentAddModel.account_id)
                {
                    createAdvanceParam.account = $scope.paymentAddModel.accountsList[i].account;
                }
            }
            createAdvanceParam.is_active = 1;
            var headers = {};
            headers['screen-code'] = 'advancepayment';
            adminService.addAdvancepayment(createAdvanceParam, headers).then(function (response) {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.advancepayment');
                }
                $scope.isDataSavingProcess = false;
                var element = document.getElementById("btnLoad");
                element.classList.remove("btn-loader");
            }).catch(function (response) {
                console.error('Error occurred:', response.status, response.data);
                $scope.isDataSavingProcess = false;
                var element = document.getElementById("btnLoad");
                element.classList.remove("btn-loader");
            });
        };
        $scope.getpaymenttermslist = function () {

            var getListParam = {};
            getListParam.id = '';
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            var defaultlist = [];
            adminService.getpaymenttermslist(getListParam, configOption).then(function (response) {
                var data = response.data.list;
                $scope.paymentAddModel.paymentlist = data;
                for (var i = 0; i < $scope.paymentAddModel.paymentlist.length; i++)
                {
                    if ($scope.paymentAddModel.paymentlist[i].is_default == 1)
                    {
                        defaultlist.push($scope.paymentAddModel.paymentlist[i]);
                        //$scope.paymentAddModel.paymentdefault = $scope.paymentAddModel.paymentlist[i].name;
                    }
                }
                $scope.paymentAddModel.payment_mode = defaultlist[0].name;
                var latest = defaultlist[0].updated_at;
                for (var i = 0; i < defaultlist.length; i++)
                {
                    if (defaultlist[i].updated_at > latest)
                    {
                        latest = defaultlist[i].updated_at;
                        $scope.paymentAddModel.payment_mode = defaultlist[i].name;
                    }
                }
            });
        }

        $scope.getpaymenttermslist();
        $scope.getIncomeCategoryList();
        $scope.getAccountlist();
    }]);




