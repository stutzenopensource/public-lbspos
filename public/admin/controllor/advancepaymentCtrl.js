app.controller('advancepaymentCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$filter', 'Auth', '$timeout', '$httpService','$localStorage', 'APP_CONST', 'ValidationFactory', function($scope, $rootScope, adminService, utilityService, $filter, Auth, $timeout, $httpService, $localStorage,APP_CONST, ValidationFactory) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.advanceModel = {
            currentPage: 1,
            total: 0,
            limit: 4,
            list: [],
            status: '',
            isLoadingProgress: true
        };
        $scope.validationFactory = ValidationFactory;
        $scope.pagePerCount = [50, 100];
        $scope.advanceModel.limit = $scope.pagePerCount[0];
        $scope.adminService = adminService;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.searchFilter =
                {                    
                    customerInfo: {},
                    status: ''
                };       

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }

        $scope.refreshScreen = function()
        {
            $scope.searchFilter =
                    {                        
                        customerInfo: {},
                        status: ''
                    };
            $scope.initTableFilter();
            $scope.noResults = false;
        }
        $scope.selectedpaymentId = '';
        $scope.showdeletePopup = false;
        $scope.isdeleteProgress = false;
        $scope.showPopup = function(id)
        {
            $scope.selectedpaymentId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteProgress = false;
        };

        $scope.deleteAdvancePaymentItem = function( )
        {
            if ($scope.isdeleteProgress == false)
            {
                $scope.isdeleteProgress = true;

                var getListParam = {};
                getListParam.id = $scope.selectedpaymentId;
                getListParam.voucher_type  = 'advance_payment';
                var headers = {};
                headers['screen-code'] = 'advancepayment';
                adminService.deleteAdvancepayment(getListParam, getListParam.id, headers).then(function(response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getList();
                    }

                    $scope.isdeleteProgress = false;
                });

            }

        };

//        $scope.$watch('searchFilter.customerInfo', function(newVal, oldVal)
//        {
//            if (typeof newVal == 'undefined' || newVal == '')
//            {
//                $scope.initTableFilter();
//            }
//        });

        $scope.getCustomerList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            autosearchParam.mode = 1;
            if (autosearchParam.search != '')
            {
                return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function(responseData)
                {
                    var data = responseData.data.list;
                    var hits = data;
                    if (hits.length > 10)
                    {
                        hits.splice(10, hits.length);
                    }
                    return hits;
                });
            }
        };

        $scope.formatCustomerModel = function(model)
        {
            if (model != null)
            {
                if (model.fname != undefined && model.phone != undefined && model.email != undefined)
                {
                    return model.fname + '(' + model.phone + ',' + model.email + ')';
                }
                else if (model.fname != undefined && model.phone != undefined)
                {
                    return model.fname + '(' + model.phone + ')';
                }
            }
            return  '';
        };

        $scope.getList = function()
        {
            $scope.advanceModel.isLoadingProgress = true;
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'advancepayment';

            var configOption = adminService.handleOnlyErrorResponseConfig;
            
            if ($scope.searchFilter.customerInfo != null && typeof $scope.searchFilter.customerInfo != 'undefined' && typeof $scope.searchFilter.customerInfo.id != 'undefined')
            {
                getListParam.customer_id = $scope.searchFilter.customerInfo.id;
            }
            else
            {
                getListParam.customer_id = '';
            }
            
            getListParam.start = ($scope.advanceModel.currentPage - 1) * $scope.advanceModel.limit;
            getListParam.limit = $scope.advanceModel.limit;
            getListParam.is_active = 1;
            getListParam.status = $scope.searchFilter.status;
            getListParam.type = 1;
            adminService.getAdvancepaymentList(getListParam, configOption, headers).then(function(response)
            {

                var data = response.data;
                if (data.success)
                {
                    $scope.advanceModel.list = data.list;
                    if ($scope.advanceModel.list.length > 0)
                    {
                        for (var i = 0; i < $scope.advanceModel.list.length; i++)
                        {
                            $scope.advanceModel.list[i].newdate = utilityService.parseStrToDate($scope.advanceModel.list[i].date);
                            $scope.advanceModel.list[i].date = utilityService.parseDateToStr($scope.advanceModel.list[i].newdate, $scope.adminService.appConfig.date_format);
                            $scope.advanceModel.list[i].validuntildate = utilityService.parseStrToDate($scope.advanceModel.list[i].validuntil);
                            $scope.advanceModel.list[i].validuntil = utilityService.parseDateToStr($scope.advanceModel.list[i].validuntildate, $scope.adminService.appConfig.date_format);
                            $scope.advanceModel.list[i].total_amount = utilityService.changeCurrency($scope.advanceModel.list[i].total_amount, $rootScope.appConfig.thousand_seperator);
                            $scope.advanceModel.list[i].advance_amount = parseFloat(utilityService.changeCurrency($scope.advanceModel.list[i].advance_amount, $rootScope.appConfig.thousand_seperator)).toFixed(2);

                        }
                    }
                    $scope.advanceModel.total = data.total;
                }
                $scope.advanceModel.isLoadingProgress = false;
            });
        };
        
        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'advancepayment_list';


        $scope.$on('onLocalStorageFormRenterCompleteEvent', function(event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });  
        
          $scope.$on('onLocalStorageFieldRenterCompleteEvent', function(event, formName) {
             if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRenterCount++;
                }
                if ($scope.localStorageRenterCount >= $scope.localStorageCount)
                {
                    console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                    $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
                }
            
              }
        });
        $scope.$on('updateTypeaheadFieldValue', function(event, data) {

            if (data.fieldName == "advancepayment_list-customer_dropdown")
            {
                $scope.searchFilter.customerInfo = data.value;
            }
        });
        
        

        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function(event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRetrieveCount++;
                }
                if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
                {
                    $scope.initTableFilter();
                }
                console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);
            }
        });

        $scope.init = function()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');

        };

        $scope.$on('onLocalStorageReadyEvent', function() {
           if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                $timeout(function() {
                    $scope.init();
                }, 300);
            
            }
        });             
        //$scope.getList();
    }]);




