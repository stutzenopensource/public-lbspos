app.controller('advancepaymentViewCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', 'APP_CONST', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', function ($scope, $rootScope, adminService, $httpService, utilityService, APP_CONST, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory) {

        $scope.advanceModel = {
            "advanceDetails": '',
            'list': ''
        };

        $scope.validationFactory = ValidationFactory;

        $scope.getNavigationBlockMsg = function (pageReload)
        {
            if (typeof $scope.advancepayment_view !== 'undefined' && typeof $scope.advancepayment_view.$pristine !== 'undefined' && !$scope.advancepayment_view.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        };
        $scope.isDataSavingProcess = false;
        $scope.formReset = function () {
            $scope.transferlist_edit_form.$setPristine();
            $scope.updateTransferlistInfo();

        };
//        $scope.currentDate = new Date();
//       $scope.advanceModel.date = $filter('date')($scope.currentDate, $scope.dateFormat);
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.startDateOpen = false;

        $scope.openDate = function (index) {

            if (index === 0)
            {
                $scope.startDateOpen = true;
            }

        };

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.resetTabletCreate = function () {

            $scope.advanceModel.user = '';

        };


        $scope.updateadvanceInfo = function ()
        {
            if ($scope.advanceModel.list !== null)
            {

                $scope.advanceModel.advanceDetails = $scope.advanceModel.list;
                $scope.advanceModel.isLoadingProgress = false;
            }
        };




        $scope.getadvanceInfo = function () {

            if (typeof $stateParams.id !== 'undefined')
            {
                var getListParam = {};
                getListParam.id = $stateParams.id;
                if ($scope.currentPage == 'purchasepayment')
                {
                    getListParam.type = 2;
                }
                else{
                     getListParam.type = 1;
                }
//                getListParam.voucher_type  = 'advance_payment';
                var headers = {};
                headers['screen-code'] = 'advancepayment';
                $scope.advanceModel.isLoadingProgress = true;
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getAdvancepaymentList(getListParam, configOption).then(function (response) {
                    var data = response.data;
                    if (data.total !== 0)
                    {
                        data.list[0].date = utilityService.parseDateToStr(data.list[0].date, $rootScope.appConfig.date_format);
                        $scope.advanceModel.list = data.list[0];
                        $scope.updateadvanceInfo();
                    }
                    $scope.advanceModel.isLoadingProgress = false;

                });
            }
        };



        $scope.getadvanceInfo();


    }]);




