app.controller('albumAddCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$timeout', '$filter', 'Auth', 'ValidationFactory', '$state', function($scope, $rootScope, adminService, utilityService, $timeout, $filter, Auth, ValidationFactory, $state) {
        $scope.albumAddModel = {
            "name": "",
            "type": "",
            "code": "",
            "comments": ""
        }

        $scope.validationFactory = ValidationFactory;
        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.create_album_form != 'undefined' && typeof $scope.create_album_form.$pristine != 'undefined' && !$scope.create_album_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.formReset = function() {

            $scope.create_album_form.$setPristine();
            $scope.albumAddModel.name = '';
            $scope.albumAddModel.code = '';
            $scope.albumAddModel.type = '';
            $scope.albumAddModel.comments = '';

        }
//        $scope.initTableFilterTimeoutPromise = null;

//        $scope.initTableFilter = function()
//        {
//            if ($scope.initTableFilterTimeoutPromise != null)
//            {
//                $timeout.cancel($scope.initTableFilterTimeoutPromise);
//            }
//            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
//        }

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.createAlbum, 300);
        }
        $scope.showUploadFilePopup = false;
        $scope.showPopup = function(index)
        {
            if (index == 'fileupload')
            {
                $scope.showUploadFilePopup = true;
            }
           
        }
        $scope.closePopup = function(index)
        {
            if (index == 'fileupload')
            {
                $scope.showUploadFilePopup = false;
            }
         
        }

        $scope.uploadedFileQueue = [];
        $scope.uploadedFileCount = 0;
        $scope.isImageSavingProcess = false;

        $scope.$on("updateFileUploadEvent", updateFileUploadEventHandler);
        function updateFileUploadEventHandler(event, data)
        {
            $scope.uploadedFileQueue = data;
            console.log('$scope.uploadedFileQueue');
            console.log($scope.uploadedFileQueue);
        }


        $scope.deleteImage = function($index)
        {
            $scope.pendingTasksModel.imgs.splice($index, 1);
        }

        $scope.saveImagePath = function()
        {
            if ($scope.uploadedFileQueue.length == 0)
            {
                $scope.closePopup('fileupload');
            }
            var hasUploadCompleted = true;
            for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
            {
                if (!$scope.uploadedFileQueue[i].isSuccess)
                {
                    hasUploadCompleted = false;
                    $scope.$broadcast('genericErrorEvent', 'Image upload is in progress.');
                    break;
                }
            }
            if (hasUploadCompleted && $scope.uploadedFileQueue.length > 0)
            {
                $scope.uploadedFileCount = $scope.uploadedFileQueue.length;
                $scope.isImageSavingProcess = true;
                for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
                {
                    $scope.isImageSavingProcess = false;
                    var imageUpdateParam = {};
                    imageUpdateParam.path = $scope.uploadedFileQueue[i].urlpath;
                    imageUpdateParam.urlpath = $scope.uploadedFileQueue[i].urlpath;
                    $scope.pendingTasksModel.img.push(imageUpdateParam);
                    $scope.closePopup('fileupload');
                }
            }

        }

        $scope.saveImage = function(id)
        {
            var imgParam = []
            for (var i = 0; i < $scope.pendingTasksModel.imgs.length; i++)
            {
                var imageUpdateParam = {};
                imageUpdateParam.id = 0;
                imageUpdateParam.masterMappedId = id;
                imageUpdateParam.masterMappedType = 'CHIT_JAMIN_LETTER';
                imageUpdateParam.masterMappedTypeNo = 8;
                imageUpdateParam.entryMappedId = "-1";
                imageUpdateParam.entryMappedTypeNo = '114';
                imageUpdateParam.entryMappedType = 'OTHERS';
                imageUpdateParam.name = $scope.uploadedFileQueue[i].urlpath;
                imageUpdateParam.comments = "";
                imageUpdateParam.path = $scope.uploadedFileQueue[i].urlpath;
                imgParam.push(imageUpdateParam);
            }
            adminService.uploadImage(imgParam).then(function(response) {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.jamin');
                }
            });
        }

        $scope.$on("deletedUploadFileEvent", deletedUploadFileEventHandler);
        function deletedUploadFileEventHandler(event, index)
        {
            if ($scope.pendingTasksModel.img.length > index)
            {
                $scope.pendingTasksModel.img.splice(index, 1);
            }
        }

        $scope.createAlbum = function() {

            $scope.isDataSavingProcess = true;
            var createAlbumParam = {};
            var headers = {};
            headers['screen-code'] = 'albumlist';
            // createAlbumParam.id = 0;
            createAlbumParam.name = $scope.albumAddModel.name;
            createAlbumParam.code = $scope.albumAddModel.code;
            createAlbumParam.type = $scope.albumAddModel.type;
            createAlbumParam.comments = $scope.albumAddModel.comments;
            createAlbumParam.is_active = 1;
            adminService.createAlbum(createAlbumParam, headers).then(function(response) {

                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.albumlist')
                }
                $scope.isDataSavingProcess = false;

            });
        };

    }]);




