app.controller('appSettingsEditCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$timeout', '$filter', 'Auth', 'ValidationFactory', '$state', 'APP_CONST', '$stateParams', '$http', '$window', 'sweet', function ($scope, $rootScope, adminService, $httpService, utilityService, $timeout, $filter, Auth, ValidationFactory, $state, APP_CONST, $stateParams, $http, $window, sweet) {

        $scope.appSettingsEditModel = {
            "id": 0,
            "date_format": '',
            "timezone": '',
            "isactive": 1,
            "currency": "",
            list: [],
            "stateList": [],
            isLoadingProgress: false,
            "uom": '',
            "uomlabel": '',
            "invoice_item_custom1": '',
            "invoice_item_custom2": '',
            "invoice_item_custom3": '',
            "invoice_item_custom4": '',
            "invoice_item_custom5": '',
            "invoice_item_custom1_label": '',
            "invoice_item_custom2_label": '',
            "invoice_item_custom3_label": '',
            "invoice_item_custom4_label": '',
            "invoice_item_custom5_label": '',
            "purchase_invoice_item_custom1": '',
            "purchase_invoice_item_custom2": '',
            "purchase_invoice_item_custom3": '',
            "purchase_invoice_item_custom4": '',
            "purchase_invoice_item_custom5": '',
            "purchase_invoice_item_custom1_label": '',
            "purchase_invoice_item_custom2_label": '',
            "purchase_invoice_item_custom3_label": '',
            "purchase_invoice_item_custom4_label": '',
            "purchase_invoice_item_custom5_label": '',
            "purchase_uom": '',
            "purchase_uom_label": '',
            "apptitle": '',
            "invoiceprefix": '',
            "paytoaddress": '',
            "signature": '',
            "sms_count": '',
            "bank_detail": '',
            "invoicefrom": '',
            "invoicecc": '',
            "account_type": '',
            "invoiceprinttemplate": '',
            "thousand_seperator": 'dd,dd,dd,ddd',
            "dual_language_support": false,
            "dual_language_label": '',
            "type": '',
            "prefix": '',
            "sales_product_list": '',
            "poscusid": '',
            "special_date1_label": '',
            "special_date2_label": '',
            "special_date3_label": '',
            "special_date4_label": '',
            "special_date5_label": '',
            "customer_special_date1": '',
            "customer_special_date2": '',
            "customer_special_date3": '',
            "customer_special_date4": '',
            "customer_special_date5": '',
            "qty_format": '',
            'gst_state': '',
            'gst_state_name': '',
            'gst_auto_suggest': '',
            'customer_gst_no': '',
            'companynameinprint': '',
            'paytocity': '',
            'paytostate': '',
            'paytocountry': '',
            'customer_mobile_no': '',
            'customer_phone_no': '',
            'company_website': '',
            'company_email': ''

        }

        $scope.validationFactory = ValidationFactory;
        $scope.getNavigationBlockMsg = function (pageReload)
        {
            if (typeof $scope.app_settings_form != 'undefined' && typeof $scope.app_settings_form.$pristine != 'undefined' && !$scope.app_settings_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.formReset = function ()
        {
//            $scope.account_type_form.$setPristine();
//            $scope.appSettingsEditModel.account_type = "";
            if (typeof $scope.app_settings_form != 'undefined')
            {
                $scope.app_settings_form.$setPristine();
                $scope.getappSettingsInfo();
            } else
            {
                $scope.account_type_form.$setPristine();
                $scope.getappSettingsInfo();
            }
        }

        $scope.initUpdateDetailTimeoutPromise = null;

        $scope.initUpdateDetail = function ()
        {
            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
            } else
            {
                $scope.initUpdateDetailTimeoutPromise = $timeout($scope.initUpdateDetail, 300);
            }
        }
        $scope.showUploadFilePopup = false;
        $scope.showBackgroundUploadPopup = false;

        $scope.showlogoPopup = function (value) {

            if (value == 'fileupload')
            {
                $scope.showUploadFilePopup = true;
            } else if (value == 'background')
            {
                $scope.showBackgroundUploadPopup = true;
            }

        };
        $scope.showTitleReadOnly = false;
        $scope.showStateReadOnly = false;
        $scope.showPopup = false;
        $scope.closePopup = function (value) {

            if (value == 'fileupload')
            {
                $scope.showUploadFilePopup = false;

            } else if (value == 'background')
            {
                $scope.showBackgroundUploadPopup = false;
            } else if (value == "readonly")
            {
                $scope.showPopup = false;
            }
        }
        $scope.getAccountlist = function () {

            var getListParam = {};
            getListParam.id = '';
            //getListParam.pos_visibility = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getAccountlist(getListParam, configOption).then(function (response) {
                var data = response.data.list;
                $scope.appSettingsEditModel.accountsList = data;
            });

        };
        $scope.getAccountlist();
        $scope.closeLogoUpload = function ()
        {
            $scope.isImageSavingProcess = true;
            $scope.closePopup('fileupload');
            $window.location.reload();
            $scope.isImageSavingProcess = false;
        }

        $scope.closeBackgroundUpload = function ()
        {
            $scope.isImageSavingProcess = true;
            $scope.closePopup('background');
            $window.location.reload();
            $scope.isImageSavingProcess = false;
        }
        $scope.showAlert = true;
        $scope.updateSettingsInfo = function ( )
        {
            $scope.appSettingsEditModel.cityInfo = {};
            $scope.appSettingsEditModel.stateInfo = {};
            $scope.appSettingsEditModel.countryInfo = {};

            var loop;

            for (loop = 0; loop < $scope.appSettingsEditModel.list.length; loop++)
            {
                if ($scope.appSettingsEditModel.list[loop].setting == "date_format")
                {
                    $scope.appSettingsEditModel.date_format = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "currency")
                {
                    $scope.appSettingsEditModel.currency = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "thousand_seperator")
                {
                    $scope.appSettingsEditModel.thousand_seperator = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "invoice_item_uom")
                {
                    $scope.appSettingsEditModel.uom = $scope.appSettingsEditModel.list[loop].value == 1 ? true : false;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "invoice_item_uom_label")
                {
                    $scope.appSettingsEditModel.uomlabel = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "invoice_item_custom1")
                {
                    $scope.appSettingsEditModel.invoice_item_custom1 = $scope.appSettingsEditModel.list[loop].value == 1 ? true : false;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "invoice_item_custom2")
                {
                    $scope.appSettingsEditModel.invoice_item_custom2 = $scope.appSettingsEditModel.list[loop].value == 1 ? true : false;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "invoice_item_custom3")
                {
                    $scope.appSettingsEditModel.invoice_item_custom3 = $scope.appSettingsEditModel.list[loop].value == 1 ? true : false;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "invoice_item_custom4")
                {
                    $scope.appSettingsEditModel.invoice_item_custom4 = $scope.appSettingsEditModel.list[loop].value == 1 ? true : false;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "invoice_item_custom5")
                {
                    $scope.appSettingsEditModel.invoice_item_custom5 = $scope.appSettingsEditModel.list[loop].value == 1 ? true : false;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "invoice_item_custom1_label")
                {
                    $scope.appSettingsEditModel.invoice_item_custom1_label = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "invoice_item_custom2_label")
                {
                    $scope.appSettingsEditModel.invoice_item_custom2_label = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "invoice_item_custom3_label")
                {
                    $scope.appSettingsEditModel.invoice_item_custom3_label = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "invoice_item_custom4_label")
                {
                    $scope.appSettingsEditModel.invoice_item_custom4_label = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "invoice_item_custom5_label")
                {
                    $scope.appSettingsEditModel.invoice_item_custom5_label = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "purchase_invoice_item_custom1_label")
                {
                    $scope.appSettingsEditModel.purchase_invoice_item_custom1_label = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "purchase_invoice_item_custom2_label")
                {
                    $scope.appSettingsEditModel.purchase_invoice_item_custom2_label = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "purchase_invoice_item_custom3_label")
                {
                    $scope.appSettingsEditModel.purchase_invoice_item_custom3_label = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "purchase_invoice_item_custom4_label")
                {
                    $scope.appSettingsEditModel.purchase_invoice_item_custom4_label = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "purchase_invoice_item_custom5_label")
                {
                    $scope.appSettingsEditModel.purchase_invoice_item_custom5_label = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "purchase_invoice_item_custom1")
                {
                    $scope.appSettingsEditModel.purchase_invoice_item_custom1 = $scope.appSettingsEditModel.list[loop].value == 1 ? true : false;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "purchase_invoice_item_custom2")
                {
                    $scope.appSettingsEditModel.purchase_invoice_item_custom2 = $scope.appSettingsEditModel.list[loop].value == 1 ? true : false;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "purchase_invoice_item_custom3")
                {
                    $scope.appSettingsEditModel.purchase_invoice_item_custom3 = $scope.appSettingsEditModel.list[loop].value == 1 ? true : false;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "purchase_invoice_item_custom4")
                {
                    $scope.appSettingsEditModel.purchase_invoice_item_custom4 = $scope.appSettingsEditModel.list[loop].value == 1 ? true : false;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "purchase_invoice_item_custom5")
                {
                    $scope.appSettingsEditModel.purchase_invoice_item_custom5 = $scope.appSettingsEditModel.list[loop].value == 1 ? true : false;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "purchase_uom")
                {
                    $scope.appSettingsEditModel.purchase_uom = $scope.appSettingsEditModel.list[loop].value == 1 ? true : false;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "purchase_uom_label")
                {
                    $scope.appSettingsEditModel.purchase_uom_label = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "app_title")
                {
                    $scope.appSettingsEditModel.apptitle = $scope.appSettingsEditModel.list[loop].value;
                    $rootScope.appConfig.app_title = $scope.appSettingsEditModel.list[loop].value;
                    if (typeof $scope.appSettingsEditModel.list[loop].value == 'undefined' || $scope.appSettingsEditModel.list[loop].value == null || $scope.appSettingsEditModel.list[loop].value == "")
                    {
                        $scope.showTitleReadOnly = true;
                    }
                } else if ($scope.appSettingsEditModel.list[loop].setting == "pos_bank_account_id")
                {
                    $scope.appSettingsEditModel.account_type = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "invoice_prefix")
                {
                    $scope.appSettingsEditModel.invoiceprefix = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "pay_to_address")
                {
                    $scope.appSettingsEditModel.paytoaddress = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "signature")
                {
                    $scope.appSettingsEditModel.signature = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "sms_count")
                {
                    $scope.appSettingsEditModel.sms_count = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "poscusid")
                {
                    $scope.appSettingsEditModel.poscusid = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "bank_detail")
                {
                    $scope.appSettingsEditModel.bank_detail = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "invoicefrom")
                {
                    $scope.appSettingsEditModel.invoicefrom = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "invoicecc")
                {
                    $scope.appSettingsEditModel.invoicecc = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "invoice_print_template")
                {
                    $scope.appSettingsEditModel.invoiceprinttemplate = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "timezone")
                {
                    $scope.appSettingsEditModel.timezone = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "dual_language_support")
                {
                    $scope.appSettingsEditModel.dual_language_support = $scope.appSettingsEditModel.list[loop].value == 1 ? true : false;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "dual_language_label")
                {
                    $scope.appSettingsEditModel.dual_language_label = $scope.appSettingsEditModel.list[loop].value;
                } else if (($scope.appSettingsEditModel.list[loop].setting == "sales_estimate_prefix"
                        || $scope.appSettingsEditModel.list[loop].setting == "sales_order_prefix"
                        || $scope.appSettingsEditModel.list[loop].setting == "sales_invoice_prefix"
                        || $scope.appSettingsEditModel.list[loop].setting == "purchase_estimate_prefix"
                        || $scope.appSettingsEditModel.list[loop].setting == "purchase_invoice_prefix") && $scope.appSettingsEditModel.type != '')
                {
                    $scope.appSettingsEditModel.prefix = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "sales_product_list")
                {
                    $scope.appSettingsEditModel.sales_product_list = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "customer_special_date1")
                {
                    $scope.appSettingsEditModel.customer_special_date1 = $scope.appSettingsEditModel.list[loop].value == 1 ? true : false;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "customer_special_date2")
                {
                    $scope.appSettingsEditModel.customer_special_date2 = $scope.appSettingsEditModel.list[loop].value == 1 ? true : false;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "customer_special_date3")
                {
                    $scope.appSettingsEditModel.customer_special_date3 = $scope.appSettingsEditModel.list[loop].value == 1 ? true : false;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "customer_special_date4")
                {
                    $scope.appSettingsEditModel.customer_special_date4 = $scope.appSettingsEditModel.list[loop].value == 1 ? true : false;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "customer_special_date5")
                {
                    $scope.appSettingsEditModel.customer_special_date5 = $scope.appSettingsEditModel.list[loop].value == 1 ? true : false;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "customer_special_date1_label")
                {
                    $scope.appSettingsEditModel.special_date1_label = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "customer_special_date2_label")
                {
                    $scope.appSettingsEditModel.special_date2_label = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "customer_special_date3_label")
                {
                    $scope.appSettingsEditModel.special_date3_label = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "customer_special_date4_label")
                {
                    $scope.appSettingsEditModel.special_date4_label = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "customer_special_date5_label")
                {
                    $scope.appSettingsEditModel.special_date5_label = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "qty_decimal_format")
                {
                    $scope.appSettingsEditModel.qty_format = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "gst_state")
                {
                    $scope.appSettingsEditModel.gst_state = $scope.appSettingsEditModel.list[loop].value;
                    if (typeof $scope.appSettingsEditModel.list[loop].value == 'undefined' || $scope.appSettingsEditModel.list[loop].value == null || $scope.appSettingsEditModel.list[loop].value == "")
                    {
                        $scope.showStateReadOnly = true;
                    }
                } else if ($scope.appSettingsEditModel.list[loop].setting == "gst_state_name")
                {
                    $scope.appSettingsEditModel.gst_state_name = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "gst_auto_suggest")
                {
                    $scope.appSettingsEditModel.gst_auto_suggest = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "customer_gst_no")
                {
                    $scope.appSettingsEditModel.customer_gst_no = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "companyname_print")
                {
                    $scope.appSettingsEditModel.companynameinprint = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "customer_country_name")
                {
                    $scope.appSettingsEditModel.countryInfo.name = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "customer_country_id")
                {
                    $scope.appSettingsEditModel.countryInfo.id = $scope.appSettingsEditModel.list[loop].value;
                }
//                else if ($scope.appSettingsEditModel.list[loop].setting == "customer_state_name")
//                {
//                    $scope.appSettingsEditModel.stateInfo.name = $scope.appSettingsEditModel.list[loop].value;
//                }else if ($scope.appSettingsEditModel.list[loop].setting == "customer_state_id")
//                {
//                    $scope.appSettingsEditModel.stateInfo.id = $scope.appSettingsEditModel.list[loop].value;
//                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "customer_city_name")
                {
                    $scope.appSettingsEditModel.cityInfo.name = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "customer_city_id")
                {
                    $scope.appSettingsEditModel.cityInfo.id = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "customer_phone_no")
                {
                    $scope.appSettingsEditModel.customer_phone_no = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "customer_mobile_no")
                {
                    $scope.appSettingsEditModel.customer_mobile_no = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "company_mail")
                {
                    $scope.appSettingsEditModel.company_email = $scope.appSettingsEditModel.list[loop].value;
                } else if ($scope.appSettingsEditModel.list[loop].setting == "company_website")
                {
                    $scope.appSettingsEditModel.company_website = $scope.appSettingsEditModel.list[loop].value;
                }
            }
            $scope.appSettingsEditModel.isLoadingProgress = false;
        }
//        $scope.showAlert = true;
//        setTimeout(function () {
//            if ($rootScope.userModel.new_user == 1 && $rootScope.hideAlert == false)
//            {
//                $window.localStorage.setItem("demo_guide", "false");
//                swal({
//                    title: "Guide Tour!",
//                    text: "Kindly fill all the required details and click submit to proceed the demo..",
//                    confirmButtonText: "OK"
//                },
//                        function (isConfirm) {
//                            if (isConfirm) {
//                                $rootScope.hideAlert = true;
//                            }
//                        });
//            }
//        }, 2000);

        $scope.getappSettingsInfo = function ()
        {
            $scope.appSettingsEditModel.isLoadingProgress = true;
            $scope.appSettingsEditModel.prefix = '';
            //$scope.appSettingsEditModel.account_type = '';
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'appsettings';
            if ($rootScope.currentPage == 'invoicesettings')
            {
                headers['screen-code'] = 'invoicesettings';
            } else if ($rootScope.currentPage == 'prefixsettings')
            {
                headers['screen-code'] = 'prefixsettings';
            } else if ($rootScope.currentPage == 'businesssettings')
            {
                headers['screen-code'] = 'businesssettings';
            } else if ($rootScope.currentPage == 'customersettings')
            {
                headers['screen-code'] = 'customersettings';
            } else
            {
                headers['screen-code'] = 'appsettings';
            }
            var configOption = adminService.handleOnlyErrorResponseConfig;
            getListParam.setting = $scope.appSettingsEditModel.type;
            adminService.getAppSettingsList(getListParam, configOption, headers).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    if (data.list.length > 0)
                    {
                        $scope.appSettingsEditModel.list = data.list;
                        if ($scope.appSettingsEditModel.list.length > 0)
                        {
                            $rootScope.appSettingsAdded = true;
                            $window.localStorage.setItem("appSettingsAdded", "true");
                        }
                        if ($rootScope.userModel.new_user == 1 && $rootScope.accountNext != false && $rootScope.accountNext != undefined)
                        {
                            $window.localStorage.setItem("demo_guide", "true");
                            $rootScope.closeDemoPopup(1);
                        }
                        $scope.updateSettingsInfo( );
                        $rootScope.appSettingsAdded = true;
                    }

                }
                $scope.appSettingsEditModel.isLoadingProgress = false;
            });
        };
        $scope.getstateList = function () {

            var stateListParam = {};
            var headers = {};
            stateListParam.id = '';
            stateListParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getStateList(stateListParam, configOption, headers).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.appSettingsEditModel.stateList = data.list;
                }
            });

        };
        $scope.getstateList();
        $scope.getappSettingsInfo( );
        $scope.updateStateName = function ()
        {
            if ($scope.appSettingsEditModel.gst_state != '')
            {
                for (var i = 0; i < $scope.appSettingsEditModel.stateList.length; i++)
                {
                    if ($scope.appSettingsEditModel.gst_state == $scope.appSettingsEditModel.stateList[i].id)
                    {
                        $scope.appSettingsEditModel.gst_state_name = $scope.appSettingsEditModel.stateList[i].name;
                    }
                }
            } else
            {
                $scope.appSettingsEditModel.gst_state_name = '';
            }
        }
        $scope.removeReadonly = function ()
        {
            $scope.closePopup("readonly");
            $scope.showTitleReadOnly = false;
            $scope.showStateReadOnly = false;
            $scope.updateAppSettings();
        }
        $scope.updateAppSettings = function ()
        {
            if ($scope.showTitleReadOnly == true && $scope.appSettingsEditModel.apptitle != "" && $scope.appSettingsEditModel.apptitle != null && typeof $scope.appSettingsEditModel.apptitle != "undefined" || $scope.showStateReadOnly == true && $scope.appSettingsEditModel.gst_state != "" && $scope.appSettingsEditModel.gst_state != null && typeof $scope.appSettingsEditModel.gst_state != "undefined")
            {
                $scope.showPopup = true;
                swal({
                    title: "Do you want continue ?",
                    text: "Once updated, you will not be able to modify Company Name and GST Filed State  !",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                        .then((willDelete) => {
                            if (willDelete) {
                                $scope.removeReadonly();
                            } else {
                                $scope.showPopup = true;
                            }
                        });
            }
            if ($scope.isDataSavingProcess == false && $scope.showPopup != true)
            {
                $scope.isDataSavingProcess = true;
                var attributeParam = [];
                var headers = {};
                headers['screen-code'] = 'appsettings';
                if ($rootScope.currentPage == 'invoicesettings')
                {
                    headers['screen-code'] = 'invoicesettings';

                    var newRow = {
                        "setting": "invoice_item_uom",
                        "value": $scope.appSettingsEditModel.uom
                    }
                    attributeParam.push(newRow);

                    var newRow = {
                        "setting": "invoice_item_uom_label",
                        "value": $scope.appSettingsEditModel.uomlabel
                    }
                    attributeParam.push(newRow);

                    var newRow = {
                        "setting": "invoice_item_custom1",
                        "value": $scope.appSettingsEditModel.invoice_item_custom1
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "invoice_item_custom2",
                        "value": $scope.appSettingsEditModel.invoice_item_custom2
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "invoice_item_custom3",
                        "value": $scope.appSettingsEditModel.invoice_item_custom3
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "invoice_item_custom4",
                        "value": $scope.appSettingsEditModel.invoice_item_custom4
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "invoice_item_custom5",
                        "value": $scope.appSettingsEditModel.invoice_item_custom5
                    }
                    attributeParam.push(newRow);

                    var newRow = {
                        "setting": "invoice_item_custom1_label",
                        "value": $scope.appSettingsEditModel.invoice_item_custom1_label
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "invoice_item_custom2_label",
                        "value": $scope.appSettingsEditModel.invoice_item_custom2_label
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "invoice_item_custom3_label",
                        "value": $scope.appSettingsEditModel.invoice_item_custom3_label
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "invoice_item_custom4_label",
                        "value": $scope.appSettingsEditModel.invoice_item_custom4_label
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "invoice_item_custom5_label",
                        "value": $scope.appSettingsEditModel.invoice_item_custom5_label
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "purchase_invoice_item_custom1",
                        "value": $scope.appSettingsEditModel.purchase_invoice_item_custom1
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "purchase_invoice_item_custom2",
                        "value": $scope.appSettingsEditModel.purchase_invoice_item_custom2
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "purchase_invoice_item_custom3",
                        "value": $scope.appSettingsEditModel.purchase_invoice_item_custom3
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "purchase_invoice_item_custom4",
                        "value": $scope.appSettingsEditModel.purchase_invoice_item_custom4
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "purchase_invoice_item_custom5",
                        "value": $scope.appSettingsEditModel.purchase_invoice_item_custom5
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "purchase_invoice_item_custom1_label",
                        "value": $scope.appSettingsEditModel.purchase_invoice_item_custom1_label
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "purchase_invoice_item_custom2_label",
                        "value": $scope.appSettingsEditModel.purchase_invoice_item_custom2_label
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "purchase_invoice_item_custom3_label",
                        "value": $scope.appSettingsEditModel.purchase_invoice_item_custom3_label
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "purchase_invoice_item_custom4_label",
                        "value": $scope.appSettingsEditModel.purchase_invoice_item_custom4_label
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "purchase_invoice_item_custom5_label",
                        "value": $scope.appSettingsEditModel.purchase_invoice_item_custom5_label
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "purchase_uom",
                        "value": $scope.appSettingsEditModel.purchase_uom
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "purchase_uom_label",
                        "value": $scope.appSettingsEditModel.purchase_uom_label
                    }
                    attributeParam.push(newRow);

                    var newRow = {
                        "setting": "invoice_prefix",
                        "value": $scope.appSettingsEditModel.invoiceprefix
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "pay_to_address",
                        "value": $scope.appSettingsEditModel.paytoaddress
                    }
                    attributeParam.push(newRow);

                    var newRow = {
                        "setting": "invoicefrom",
                        "value": $scope.appSettingsEditModel.invoicefrom
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "invoicecc",
                        "value": $scope.appSettingsEditModel.invoicecc
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "invoice_print_template",
                        "value": $scope.appSettingsEditModel.invoiceprinttemplate
                    }
                    attributeParam.push(newRow);
                } else if ($rootScope.currentPage == 'prefixsettings')
                {
                    headers['screen-code'] = 'prefixsettings';
                    var newRow = {
                        "setting": $scope.appSettingsEditModel.type,
                        "value": $scope.appSettingsEditModel.prefix
                    }
                    attributeParam.push(newRow);
                } else if ($rootScope.currentPage == 'businesssettings')
                {
                    headers['screen-code'] = 'businesssettings';
                    var newRow = {
                        "setting": 'pos_bank_account_id',
                        "value": $scope.appSettingsEditModel.account_type
                    }
                    attributeParam.push(newRow);
                } else if ($rootScope.currentPage == 'customersettings')
                {
                    headers['screen-code'] = 'customersettings';
                    var newRow = {
                        "setting": "customer_special_date1",
                        "value": $scope.appSettingsEditModel.customer_special_date1
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "customer_special_date2",
                        "value": $scope.appSettingsEditModel.customer_special_date2
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "customer_special_date3",
                        "value": $scope.appSettingsEditModel.customer_special_date3
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "customer_special_date4",
                        "value": $scope.appSettingsEditModel.customer_special_date4
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "customer_special_date5",
                        "value": $scope.appSettingsEditModel.customer_special_date5
                    }
                    attributeParam.push(newRow);
                    var newRow = {};
                    newRow.setting = "customer_special_date1_label";
                    if ($scope.appSettingsEditModel.customer_special_date1 == true &&
                            $scope.appSettingsEditModel.special_date1_label == '')
                    {
                        newRow.value = 'Special Date1';
                    } else
                    {
                        newRow.value = $scope.appSettingsEditModel.special_date1_label;
                    }
                    attributeParam.push(newRow);
                    var newRow = {};
                    newRow.setting = "customer_special_date2_label";
                    if ($scope.appSettingsEditModel.customer_special_date2 == true &&
                            $scope.appSettingsEditModel.special_date2_label == '')
                    {
                        newRow.value = 'Special Date2';
                    } else
                    {
                        newRow.value = $scope.appSettingsEditModel.special_date2_label;
                    }
                    attributeParam.push(newRow);
                    var newRow = {};
                    newRow.setting = "customer_special_date3_label";
                    if ($scope.appSettingsEditModel.customer_special_date3 == true &&
                            $scope.appSettingsEditModel.special_date3_label == '')
                    {
                        newRow.value = 'Special Date3';
                    } else
                    {
                        newRow.value = $scope.appSettingsEditModel.special_date3_label;
                    }
                    attributeParam.push(newRow);
                    var newRow = {};
                    newRow.setting = "customer_special_date4_label";
                    if ($scope.appSettingsEditModel.customer_special_date4 == true &&
                            $scope.appSettingsEditModel.special_date4_label == '')
                    {
                        newRow.value = 'Special Date4';
                    } else
                    {
                        newRow.value = $scope.appSettingsEditModel.special_date4_label;
                    }
                    attributeParam.push(newRow);
                    var newRow = {};
                    newRow.setting = "customer_special_date5_label";
                    if ($scope.appSettingsEditModel.customer_special_date5 == true &&
                            $scope.appSettingsEditModel.special_date5_label == '')
                    {
                        newRow.value = 'Special Date5';
                    } else
                    {
                        newRow.value = $scope.appSettingsEditModel.special_date5_label;
                    }
                    attributeParam.push(newRow);

                } else
                {
                    headers['screen-code'] = 'appsettings';

                    var newRow = {
                        "setting": "date_format",
                        "value": $scope.appSettingsEditModel.date_format
                    }
                    attributeParam.push(newRow);

                    var newRow = {
                        "setting": "currency",
                        "value": $scope.appSettingsEditModel.currency
                    }
                    attributeParam.push(newRow);

                    var newRow = {
                        "setting": "thousand_seperator",
                        "value": $scope.appSettingsEditModel.thousand_seperator
                    }
                    attributeParam.push(newRow);

                    var newRow = {
                        "setting": "app_title",
                        "value": $scope.appSettingsEditModel.apptitle
                    }

                    attributeParam.push(newRow);

                    var newRow = {
                        "setting": "pay_to_address",
                        "value": $scope.appSettingsEditModel.paytoaddress
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "signature",
                        "value": $scope.appSettingsEditModel.signature
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "sms_count",
                        "value": $scope.appSettingsEditModel.sms_count
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "poscusid",
                        "value": $scope.appSettingsEditModel.poscusid
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "bank_detail",
                        "value": $scope.appSettingsEditModel.bank_detail
                    }
                    attributeParam.push(newRow);

                    var newRow = {
                        "setting": "timezone",
                        "value": $scope.appSettingsEditModel.timezone
                    }
                    attributeParam.push(newRow);

                    var newRow = {
                        "setting": "dual_language_support",
                        "value": $scope.appSettingsEditModel.dual_language_support
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "dual_language_label",
                        "value": $scope.appSettingsEditModel.dual_language_label
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "sales_product_list",
                        "value": $scope.appSettingsEditModel.sales_product_list
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "qty_decimal_format",
                        "value": $scope.appSettingsEditModel.qty_format
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "qty_decimal_format",
                        "value": $scope.appSettingsEditModel.qty_format
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "gst_state",
                        "value": $scope.appSettingsEditModel.gst_state
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "gst_state_name",
                        "value": $scope.appSettingsEditModel.gst_state_name
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "gst_auto_suggest",
                        "value": $scope.appSettingsEditModel.gst_auto_suggest
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "customer_gst_no",
                        "value": $scope.appSettingsEditModel.customer_gst_no
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "companyname_print",
                        "value": $scope.appSettingsEditModel.companynameinprint
                    }
                    attributeParam.push(newRow);
                    if ($scope.appSettingsEditModel.cityInfo.id != undefined && $scope.appSettingsEditModel.cityInfo.id != 'undefined')
                    {
                        var newRow = {
                            "setting": "customer_city_name",
                            "value": $scope.appSettingsEditModel.cityInfo.name
                        }
                    } else {
                        var newRow = {
                            "setting": "customer_city_name",
                            "value": ''
                        }
                    }
                    attributeParam.push(newRow);
                    if ($scope.appSettingsEditModel.cityInfo.id != undefined && $scope.appSettingsEditModel.cityInfo.id != 'undefined')
                    {
                        var newRow = {
                            "setting": "customer_city_id",
                            "value": $scope.appSettingsEditModel.cityInfo.id,
                        }
                    } else {
                        var newRow = {
                            "setting": "customer_city_id",
                            "value": '',
                        }
                    }
                    attributeParam.push(newRow);
//                    var newRow = {
//                        "setting": "customer_state_id",
//                        "value": $scope.appSettingsEditModel.stateInfo.id
//                    }
//                    attributeParam.push(newRow);
//                    var newRow = {
//                        "setting": "customer_state_name",
//                        "value": $scope.appSettingsEditModel.stateInfo.name
//                    }
//                    attributeParam.push(newRow);
                    if ($scope.appSettingsEditModel.countryInfo.id != undefined && $scope.appSettingsEditModel.countryInfo.id != 'undefined')
                    {
                        var newRow = {
                            "setting": "customer_country_id",
                            "value": $scope.appSettingsEditModel.countryInfo.id
                        }
                    } else {
                        var newRow = {
                            "setting": "customer_country_id",
                            "value": ''
                        }
                    }
                    attributeParam.push(newRow);
                    if ($scope.appSettingsEditModel.countryInfo.id != undefined && $scope.appSettingsEditModel.countryInfo.id != 'undefined')
                    {
                        var newRow = {
                            "setting": "customer_country_name",
                            "value": $scope.appSettingsEditModel.countryInfo.name
                        }
                    } else {
                        var newRow = {
                            "setting": "customer_country_name",
                            "value": ''
                        }
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "customer_mobile_no",
                        "value": $scope.appSettingsEditModel.customer_mobile_no
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "customer_phone_no",
                        "value": $scope.appSettingsEditModel.customer_phone_no
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "company_website",
                        "value": $scope.appSettingsEditModel.company_website
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "company_mail",
                        "value": $scope.appSettingsEditModel.company_email
                    }
                    attributeParam.push(newRow);
                }
                var configOption = adminService.handleBothSuccessAndErrorResponseConfig;
                adminService.editAppSettings(attributeParam, configOption, headers).then(function (response)
                {
                    if (response.data.success == true)
                    {
                        $scope.showAlert = false;
                        $rootScope.appConfig.currency = $scope.appSettingsEditModel.currency;
                        $rootScope.appConfig.thousand_seperator = $scope.appSettingsEditModel.thousand_seperator;
                        $rootScope.appConfig.date_format = $scope.appSettingsEditModel.date_format;
                        $rootScope.appConfig.uom = $scope.appSettingsEditModel.uom;
                        $rootScope.appConfig.invoice_item_custom1 = $scope.appSettingsEditModel.invoice_item_custom1;
                        $rootScope.appConfig.invoice_item_custom2 = $scope.appSettingsEditModel.invoice_item_custom2;
                        $rootScope.appConfig.invoice_item_custom3 = $scope.appSettingsEditModel.invoice_item_custom3;
                        $rootScope.appConfig.invoice_item_custom4 = $scope.appSettingsEditModel.invoice_item_custom4;
                        $rootScope.appConfig.invoice_item_custom5 = $scope.appSettingsEditModel.invoice_item_custom5;
                        $rootScope.appConfig.invoice_item_custom1_label = $scope.appSettingsEditModel.invoice_item_custom1_label;
                        $rootScope.appConfig.invoice_item_custom2_label = $scope.appSettingsEditModel.invoice_item_custom2_label;
                        $rootScope.appConfig.invoice_item_custom3_label = $scope.appSettingsEditModel.invoice_item_custom3_label;
                        $rootScope.appConfig.invoice_item_custom4_label = $scope.appSettingsEditModel.invoice_item_custom4_label;
                        $rootScope.appConfig.invoice_item_custom5_label = $scope.appSettingsEditModel.invoice_item_custom5_label;
                        $rootScope.appConfig.purchase_invoice_item_custom1 = $scope.appSettingsEditModel.purchase_invoice_item_custom1;
                        $rootScope.appConfig.purchase_invoice_item_custom2 = $scope.appSettingsEditModel.purchase_invoice_item_custom2;
                        $rootScope.appConfig.purchase_invoice_item_custom3 = $scope.appSettingsEditModel.purchase_invoice_item_custom3;
                        $rootScope.appConfig.purchase_invoice_item_custom4 = $scope.appSettingsEditModel.purchase_invoice_item_custom4;
                        $rootScope.appConfig.purchase_invoice_item_custom5 = $scope.appSettingsEditModel.purchase_invoice_item_custom5;
                        $rootScope.appConfig.purchase_invoice_item_custom1_label = $scope.appSettingsEditModel.purchase_invoice_item_custom1_label;
                        $rootScope.appConfig.purchase_invoice_item_custom2_label = $scope.appSettingsEditModel.purchase_invoice_item_custom2_label;
                        $rootScope.appConfig.purchase_invoice_item_custom3_label = $scope.appSettingsEditModel.purchase_invoice_item_custom3_label;
                        $rootScope.appConfig.purchase_invoice_item_custom4_label = $scope.appSettingsEditModel.purchase_invoice_item_custom4_label;
                        $rootScope.appConfig.purchase_invoice_item_custom5_label = $scope.appSettingsEditModel.purchase_invoice_item_custom5_label;
                        $rootScope.appConfig.purchase_uom = $scope.appSettingsEditModel.purchase_uom;
                        $rootScope.appConfig.purchase_uom_label = $scope.appSettingsEditModel.purchase_uom_label;
                        $rootScope.appConfig.app_title = $scope.appSettingsEditModel.apptitle;
                        if (typeof $scope.appSettingsEditModel.apptitle == 'undefined' || $scope.appSettingsEditModel.apptitle == null || $scope.appSettingsEditModel.apptitle == "")
                        {
                            $scope.showTitleReadOnly = true;
                        }
                        $rootScope.appConfig.pos_bank_account_id = $scope.appSettingsEditModel.account_type;
                        $rootScope.appConfig.invoice_prefix = $scope.appSettingsEditModel.invoiceprefix;
                        $rootScope.appConfig.pay_to_address = $scope.appSettingsEditModel.paytoaddress;
                        $rootScope.appConfig.signature = $scope.appSettingsEditModel.signature;
                        $rootScope.appConfig.poscusid = $scope.appSettingsEditModel.poscusid;
                        $rootScope.appConfig.bank_detail = $scope.appSettingsEditModel.bank_detail;
                        $rootScope.appConfig.invoicefrom = $scope.appSettingsEditModel.invoicefrom;
                        $rootScope.appConfig.invoice_print_template = $scope.appSettingsEditModel.invoiceprinttemplate;
                        $rootScope.appConfig.invoicecc = $scope.appSettingsEditModel.invoicecc;
                        $rootScope.appConfig.timezone = $scope.appSettingsEditModel.timezone;
                        $rootScope.appConfig.dual_language_support = $scope.appSettingsEditModel.dual_language_support;
                        $rootScope.appConfig.dual_language_label = $scope.appSettingsEditModel.dual_language_label;
                        $rootScope.appConfig.customer_special_date1 = $scope.appSettingsEditModel.customer_special_date1;
                        $rootScope.appConfig.customer_special_date2 = $scope.appSettingsEditModel.customer_special_date2;
                        $rootScope.appConfig.customer_special_date3 = $scope.appSettingsEditModel.customer_special_date3;
                        $rootScope.appConfig.customer_special_date4 = $scope.appSettingsEditModel.customer_special_date4;
                        $rootScope.appConfig.customer_special_date5 = $scope.appSettingsEditModel.customer_special_date5;
                        $rootScope.appConfig.customer_special_date1_label = $scope.appSettingsEditModel.special_date1_label;
                        $rootScope.appConfig.customer_special_date2_label = $scope.appSettingsEditModel.special_date2_label;
                        $rootScope.appConfig.customer_special_date3_label = $scope.appSettingsEditModel.special_date3_label;
                        $rootScope.appConfig.customer_special_date4_label = $scope.appSettingsEditModel.special_date4_label;
                        $rootScope.appConfig.customer_special_date5_label = $scope.appSettingsEditModel.special_date5_label;
                        $rootScope.appConfig.qty_format = $scope.appSettingsEditModel.qty_format;
                        $rootScope.appConfig.gst_state = $scope.appSettingsEditModel.gst_state;
                        if (typeof $scope.appSettingsEditModel.gst_state == 'undefined' || $scope.appSettingsEditModel.gst_state == null || $scope.appSettingsEditModel.gst_state == "")
                        {
                            $scope.showStateReadOnly = true;
                        }
                        $rootScope.appConfig.cityInfo = {
                            id: $scope.appSettingsEditModel.cityInfo.id,
                            name: $scope.appSettingsEditModel.cityInfo.name
                        };
//                        $rootScope.appConfig.stateInfo = {
//                            id: $scope.appSettingsEditModel.stateInfo.id,
//                            name: $scope.appSettingsEditModel.stateInfo.name
//                        };
                        $rootScope.appConfig.countryInfo = {
                            id: $scope.appSettingsEditModel.countryInfo.id,
                            name: $scope.appSettingsEditModel.countryInfo.name
                        };
                        $rootScope.appConfig.customer_website = $scope.appSettingsEditModel.customer_website;
                        $rootScope.appConfig.customer_email = $scope.appSettingsEditModel.customer_email;
                        $rootScope.appConfig.customer_mobile_no = $scope.appSettingsEditModel.customer_mobile_no;
                        $rootScope.appConfig.customer_phone_no = $scope.appSettingsEditModel.customer_phone_no;
                        $rootScope.appConfig.paytocity = $scope.appSettingsEditModel.paytocity;
                        $rootScope.appConfig.paytostate = $scope.appSettingsEditModel.paytostate;
                        $rootScope.appConfig.paytocountry = $scope.appSettingsEditModel.paytocountry;
                        $rootScope.appConfig.gst_state_name = $scope.appSettingsEditModel.gst_state_name;
                        $rootScope.appConfig.gst_auto_suggest = $scope.appSettingsEditModel.gst_auto_suggest;
                        $rootScope.appConfig.companyname_print = $scope.appSettingsEditModel.companynameinprint;
                        $scope.formReset();
                        if ($window.location.hash == "#/invoicesettings")
                        {
                            $state.go('app.invoicesettings', {'reload': true});
                        } else if ($window.location.hash == "#/prefixsettings")
                        {
                            $state.go('app.prefixsettings', {'reload': true});
                        } else if ($window.location.hash == "#/customersettings")
                        {
                            $state.go('app.customersettings', {'reload': true});
                        } else if ($window.location.hash == "#/businesssettings")
                        {
                            $state.go('app.businessSettings', {'reload': true});
                        } else
                        {
                            $state.go('app.appSettings', {'reload': true});
                            var element = document.getElementById("btnLoad");
                            element.classList.remove("btn-loader");
                        }
                        if ($rootScope.userModel.new_user == 1)
                        {
                            $window.localStorage.setItem("demo_guide", "true");
                            $rootScope.closeDemoPopup(1);
                        }
                    } 
                        $scope.isDataSavingProcess = false;
                        var element = document.getElementById("btnLoad");
                        element.classList.remove("btn-loader");
                    
                }).catch(function (response) {
                    console.error('Error occurred:', response.status, response.data);
                    $scope.isDataSavingProcess = false;
                    var element = document.getElementById("btnLoad");
                    element.classList.remove("btn-loader");
                });
            } 
        };
        $scope.getCountryList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            return $httpService.get(APP_CONST.API.COUNTRY_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatCountryModel = function (model)
        {
            if (model != null)
            {
                return model.name;
            }
            return  '';
        };
        $scope.getStateList = function (val)
        {
            var autosearchParam = {};
            if ($scope.appSettingsEditModel.countryInfo != null && $scope.appSettingsEditModel.countryInfo != "" && typeof $scope.appSettingsEditModel.countryInfo != "undefined" && typeof $scope.appSettingsEditModel.countryInfo.id != "undefined")
            {
                autosearchParam.country_name = $scope.appSettingsEditModel.countryInfo.name;
            }
            autosearchParam.name = val;
            return $httpService.get(APP_CONST.API.STATE_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatStateModel = function (model)
        {
            if (model != null)
            {
                return model.name;
            }
            return  '';
        };
        $scope.getCityList = function (val)
        {
            var autosearchParam = {};
            if ($scope.appSettingsEditModel.countryInfo != null && $scope.appSettingsEditModel.countryInfo != "" && typeof $scope.appSettingsEditModel.countryInfo != "undefined" && typeof $scope.appSettingsEditModel.countryInfo.id != "undefined")
            {
                autosearchParam.country_name = $scope.appSettingsEditModel.countryInfo.name;
            }
            if ($scope.appSettingsEditModel.stateInfo != null && $scope.appSettingsEditModel.stateInfo != "" && typeof $scope.appSettingsEditModel.stateInfo != "undefined" && typeof $scope.appSettingsEditModel.stateInfo.id != "undefined")
            {
                autosearchParam.state_name = $scope.appSettingsEditModel.stateInfo.name;
            }
            autosearchParam.name = val;
            return $httpService.get(APP_CONST.API.CITY_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatCityModel = function (model)
        {
            if (model != null)
            {
                return model.name;
            }
            return  '';
        };
    }]);

