app.controller('attributeAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$timeout', '$filter', 'Auth', 'ValidationFactory', '$state', 'APP_CONST', function ($scope, $rootScope, adminService, $httpService, utilityService, $timeout, $filter, Auth, ValidationFactory, $state, APP_CONST) {

        $scope.attributeAddModel = {
            "id": 0,
            "attributeTypeId": '',
            "isRequired": true,
            "isactive": true,
            "hasEditable": true,
            "attributeTypeList": {},
            "attibuteList": [],
            "showinlist": '',
            'showininvoice': ''
        }

        $scope.validationFactory = ValidationFactory;
        $scope.getNavigationBlockMsg = function (pageReload)
        {
            if (typeof $scope.create_attribute_form != 'undefined' && typeof $scope.create_attribute_form.$pristine != 'undefined' && !$scope.create_attribute_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }

        $scope.isDataSavingProcess = false;
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.formReset = function () {
            if (typeof $scope.create_attribute_form != 'undefined')
            {
                $scope.create_attribute_form.$setPristine();
            }
            $scope.attributeAddModel = {
                "id": 0,
                "attributeTypeId": '',
                "isRequired": true,
                "isactive": true,
                "hasEditable": true,
                "showinlist": '',
                'showininvoice': ''
            }
            $scope.getAttributeTypeList();
        }

        $scope.getAttributeTypeList = function ()
        {
            var attrTypeListParam = {};
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getAttributeTypeList(attrTypeListParam, configOption).then(function (response)
            {
                var data = response.data.list;
                $scope.attributeAddModel.attributeTypeList = data;
            });
        };

        $scope.validateCodeInput = function ( )
        {
            var attributeCode = $scope.attributeAddModel.attributeCode;
            var returnVal = true;

            //^[a-zA-Z,a-zA-Z0-9\-\//\s,!\#\$%&@()'\*\+\/=\?\^`\{\|\}~]*$/ 
//        var re = /^[a-zA-Z,a-zA-Z]+[a-zA-Z,a-zA-Z0-9\-\//\s,!\#\$%&@()'\*\+\/=\?\^`\{\|\}~]*$/;        
//        if (re.test(attributeCode)) 
//        {
//            return "attribute code should not contain space, special characters & symbols";
//        }             
            return returnVal;
        }

        $scope.getAttributeTypeList( );
        $scope.addAttributeDetails = function ()
        {
            if ($scope.isDataSavingProcess == false)
            {
                $scope.isDataSavingProcess = true;
                var attributeParam = {};
                var headers = {};
                headers['screen-code'] = 'attribute';
                var loop = 0;

                attributeParam.id = 0;
                attributeParam.attributetype_id = $scope.attributeAddModel.attributeTypeId;
                attributeParam.attribute_code = $scope.attributeAddModel.attributeCode;
                attributeParam.attribute_label = $scope.attributeAddModel.attributeLabel;
                attributeParam.sno = $scope.attributeAddModel.sequenceNo;
                attributeParam.input_type = $scope.attributeAddModel.inputType;
                attributeParam.default_value = $scope.attributeAddModel.defaultValue;
                attributeParam.option_value = $scope.attributeAddModel.optionValue;
                attributeParam.validation_type = $scope.attributeAddModel.validationType;
                attributeParam.is_permission = 1;
                attributeParam.is_required = ($scope.attributeAddModel.isRequired == true ? 1 : 0);
                attributeParam.has_editable = ($scope.attributeAddModel.hasEditable == true ? 1 : 0);
                attributeParam.is_show_in_list = ($scope.attributeAddModel.showinlist == true ? 1 : 0);
                attributeParam.is_show_in_invoice = ($scope.attributeAddModel.showininvoice == true ? 1 : 0);
                if (attributeParam.validation_type == "number")
                {
                    attributeParam.reg_pattern = "^[0-9//\s]*$";
                } else if (attributeParam.validation_type == "email")
                {
                    //attributeParam.reg_pattern = "^([a-z0-9,!\\#\\$%&'\\*\\+\\/=\\?\\^_`\\{\\|\\}~-]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])+(\\.([a-z0-9,!\\#\\$%&'\\*\\+\\/=\\?\\^_`\\{\\|\\}~-]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])+)*@([a-z0-9-]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])+(\\.([a-z0-9-]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])+)*\\.(([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF]){2,})$";
                    attributeParam.reg_pattern = "email";
                } else if (attributeParam.validation_type == "phoneno")
                {
                    attributeParam.reg_pattern = "^(\\+|\\d)[\\d]{5,}\\d*$";
                } else if (attributeParam.validation_type == "alphanumeric")
                {
                    attributeParam.reg_pattern = "^[a-zA-Z0-9//\s]*$";
                } else if (attributeParam.validation_type == "gst")
                {
                    attributeParam.reg_pattern = "^([0][1-9]|[1-2][0-9]|[3][0-5])([a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9a-zA-Z]{1}[zZ]{1}[0-9a-zA-Z]{1})+$";
                }

                adminService.addAttribute(attributeParam, headers).then(function (response) {
                    if (response.data.success == true)
                    {
                        $scope.formReset();
                        $state.go('app.attribute');
                    }
                    $scope.isDataSavingProcess = false;

                }).catch(function (response) {
                    console.error('Error occurred:', response.status, response.data);
                    $scope.isDataSavingProcess = false;
                    var element = document.getElementById("btnLoad");
                    element.classList.remove("btn-loader");
                });
            }
        };

        $scope.$on("updateItemChngEvent", function (event, id, val)
        {
            var loop;

            for (loop = 0; loop < $scope.attributeAddModel.attibuteList.length; loop++)
            {
                if (id == $scope.attributeAddModel.attibuteList[loop].id)
                {
                    console.log("Attribute Change");
                    console.log("id =", id);
                    console.log("val =", val);
                    $scope.attributeAddModel.attibuteList[loop].attribute_code = val;
                }
            }
        });

    }]);