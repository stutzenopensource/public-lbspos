app.controller('barcodereportCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', function ($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.barcodeReportModel = {
            currentPage: 1,
            total: 0,
            total_Amount: 0,
            limit: 10,
            list: [],
            length: [],
            date: '',
            invoice_id: '',
            customer_id: '',
            amount: '',
            barcode: '',
            status: '',
            total_Unit: 0,
            total_Mrp: 0,
            total_Qty: 0,
            serverList: null,
            isLoadingProgress: false,
            isSearchLoadingProgress: false,
            categorylist: []
        };
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.barcodeReportModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat);
        $scope.validationFactory = ValidationFactory;
        $scope.openDate = function (index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            } else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };

        $scope.searchFilter = {
            fromdate: '',
            todate: '',
            showprice: '',
            code: '',
            productInfo: {},
            categoryinfo: {}


        };
        $scope.searchFilter.todate = $scope.currentDate;

        $scope.clearFilters = function ()
        {
            $scope.searchFilter = {
                fromdate: '',
                todate: '',
                showprice: '',
                code: '',
                productInfo: {},
                categoryinfo: {}


            };

            $scope.barcodeReportModel.list = [];
            $scope.searchFilter.todate = $scope.currentDate;
        }

        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.validateDateFilterData = function ()
        {
            var retVal = false;
            if ($scope.searchFilter.todate !== null || $scope.searchFilter.fromdate !== '')
            {
                retVal = true;
            }
            return retVal;
        };
        $scope.fromdate = '';
        $scope.getList = function () {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'barcodewisestockreport';
            getListParam.from_date = $scope.searchFilter.fromdate;
            getListParam.to_date = $scope.searchFilter.todate;
            getListParam.code = $scope.searchFilter.code;

            if ($scope.searchFilter.todate != '' && ($scope.searchFilter.fromdate == null || $scope.searchFilter.fromdate == ''))
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
                getListParam.from_date = getListParam.to_date;
                $scope.fromdate = getListParam.from_date;
            } else if ($scope.searchFilter.fromdate != '' && ($scope.searchFilter.todate == null || $scope.searchFilter.todate == ''))
            {
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
                getListParam.to_date = getListParam.from_date;
                $scope.fromdate = getListParam.from_date;
            } else
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
                $scope.fromdate = getListParam.from_date;
            }
            if ($scope.searchFilter.productInfo != null && typeof $scope.searchFilter.productInfo != 'undefined' && typeof $scope.searchFilter.productInfo.id != 'undefined')
            {
                getListParam.product_id = $scope.searchFilter.productInfo.id;
            } else
            {
                getListParam.product_id = '';
            }
            if ($scope.searchFilter.categoryInfo != null && typeof $scope.searchFilter.categoryInfo != 'undefined' && typeof $scope.searchFilter.categoryInfo.id != 'undefined')
            {
                getListParam.cat_id = $scope.searchFilter.categoryInfo.id;
            } else
            {
                getListParam.cat_id = '';
            }

            getListParam.show_all = 1;
            if ($scope.searchFilter.showprice == '')
            {
                getListParam.show_price = '';
                $scope.showpricecolumn = false;
                $scope.colspan = 8;
            }
            if ($scope.searchFilter.showprice == 1)
            {
                getListParam.show_price = 1;
                $scope.showpricecolumn = true;
                $scope.colspan = 8;
            }
            getListParam.start = 0;
            getListParam.limit = 0;
            $scope.barcodeReportModel.isLoadingProgress = true;
            $scope.barcodeReportModel.isSearchLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            if ($scope.searchFilter.todate != '' && $scope.searchFilter.todate != null && $scope.searchFilter.todate != 'undefined' && $scope.searchFilter.fromdate != '' && $scope.searchFilter.fromdate != null)
            {
                adminService.getBarcodewiseStock(getListParam, configOption, headers).then(function (response)
                {
                    var totalqty = 0.00;
                    var totalMrp = 0.00;
                    var totalUnit = 0.00;
                    var totalselling = 0.00;
                    var overallPrice = 0.00;
                    if (response.data.success === true)
                    {
                        var data = response.data.list;
                        $scope.barcodeReportModel.list = data;
                        for (var i = 0; i < $scope.barcodeReportModel.list.length; i++)
                        {
                            $scope.barcodeReportModel.list[i].newdate = utilityService.parseStrToDate($scope.barcodeReportModel.list[i].date);
                            $scope.barcodeReportModel.list[i].date = utilityService.parseDateToStr($scope.barcodeReportModel.list[i].newdate, $scope.adminService.appConfig.date_format);
                            totalqty = totalqty + parseFloat($scope.barcodeReportModel.list[i].qty);
                            totalMrp = totalMrp + parseFloat($scope.barcodeReportModel.list[i].mrp_price);
                            $scope.barcodeReportModel.list[i].overallPrice = parseFloat($scope.barcodeReportModel.list[i].qty) * parseFloat($scope.barcodeReportModel.list[i].mrp_price);
                            $scope.barcodeReportModel.list[i].overallPrice = parseFloat($scope.barcodeReportModel.list[i].overallPrice).toFixed(2);
                            overallPrice = overallPrice + parseFloat($scope.barcodeReportModel.list[i].overallPrice);
                            if ($scope.searchFilter.showprice == 1)
                            {
                                totalselling = totalselling + parseFloat($scope.barcodeReportModel.list[i].selling_price);
                                totalUnit = totalUnit + parseFloat($scope.barcodeReportModel.list[i].unit_price);
                            }

                        }
                        $scope.barcodeReportModel.total = data.total;
                        $scope.barcodeReportModel.total_Qty = totalqty;
                        $scope.barcodeReportModel.total_Qty = parseFloat($scope.barcodeReportModel.total_Qty);
                        $scope.barcodeReportModel.total_Qty = utilityService.changeCurrency($scope.barcodeReportModel.total_Qty, $rootScope.appConfig.thousand_seperator);
                        $scope.barcodeReportModel.total_Mrp = totalMrp;
                        $scope.barcodeReportModel.total_Mrp = parseFloat($scope.barcodeReportModel.total_Mrp).toFixed(2);
                        $scope.barcodeReportModel.total_Mrp = utilityService.changeCurrency($scope.barcodeReportModel.total_Mrp, $rootScope.appConfig.thousand_seperator);

                        $scope.barcodeReportModel.total_MRPPrice = parseFloat(overallPrice).toFixed(2);
                        $scope.barcodeReportModel.total_MRPPrice = parseFloat($scope.barcodeReportModel.total_MRPPrice).toFixed(2);
                        $scope.barcodeReportModel.total_MRPPrice = utilityService.changeCurrency($scope.barcodeReportModel.total_MRPPrice, $rootScope.appConfig.thousand_seperator);
                        if ($scope.searchFilter.showprice == 1)
                        {
                            $scope.barcodeReportModel.total_Unit = totalUnit;
                            $scope.barcodeReportModel.total_Unit = parseFloat($scope.barcodeReportModel.total_Unit).toFixed(2);
                            $scope.barcodeReportModel.total_Unit = utilityService.changeCurrency($scope.barcodeReportModel.total_Unit, $rootScope.appConfig.thousand_seperator);
                            $scope.barcodeReportModel.total_Selling = totalselling;
                            $scope.barcodeReportModel.total_Selling = parseFloat($scope.barcodeReportModel.total_Selling).toFixed(2);
                            $scope.barcodeReportModel.total_Selling = utilityService.changeCurrency($scope.barcodeReportModel.total_Selling, $rootScope.appConfig.thousand_seperator);
                        }
                    }
                    $scope.barcodeReportModel.isLoadingProgress = false;
                    $scope.barcodeReportModel.isSearchLoadingProgress = false;
                });
            } else {
                $scope.barcodeReportModel.isLoadingProgress = false;
                $scope.barcodeReportModel.isSearchLoadingProgress = false;
            }

        };

        $scope.getProductList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
             autosearchParam.is_active=1;
            return $httpService.get(APP_CONST.API.PRODUCT_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatProductModel = function (model)
        {
            if (model != null)
            {
                if (model.name != undefined && model.sku != undefined)
                {
                    return model.name + '(' + model.sku + ')';
                } else if (model.name != undefined && model.sku != undefined)
                {
                    return model.name + '(' + model.sku + ')';
                }
            }
            return  '';
        };

        $scope.getCategoryList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.GET_CATEGORY_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatCategoryModel = function (model)
        {
            if (model != null)
            {
                if (model.name != undefined)
                {
                    return model.name;
                }
            }
            return  '';
        };

        $scope.print = function (div)
        {
            var docHead = document.head.outerHTML;
            var printContents = document.getElementById(div).outerHTML;
            var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=865, height=600, resizable=yes, screenX=200, screenY=200, personalbar=no, scrollbars=yes";

            var newWin = window.open("", "_blank", winAttr);
            var writeDoc = newWin.document;
            writeDoc.open();
            writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
            writeDoc.close();
            newWin.focus();
        };
        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'receivable_form';


        $scope.$on('onLocalStorageFormRenterCompleteEvent', function (event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });

        $scope.$on('onLocalStorageFieldRenterCompleteEvent', function (event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRenterCount++;
                }
                if ($scope.localStorageRenterCount >= $scope.localStorageCount)
                {
                    console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                    $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
                }

            }
        });
        $scope.$on('updateTypeaheadFieldValue', function (event, data) {

            if (data.fieldName == "receivable_form-product_dropdown")
            {
                $scope.searchFilter.productInfo = data.value;
            }
        });
        $scope.$on('updateTypeaheadFieldValue', function (event, data) {

            if (data.fieldName == "receivable_form-category_dropdown")
            {
                $scope.searchFilter.categoryInfo = data.value;
            }
        });



        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function (event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRetrieveCount++;
                }
                if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
                {
                    $scope.initTableFilter();
                }
                console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);
            }
        });

        $scope.init = function ()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');

        };

        $scope.$on('onLocalStorageReadyEvent', function () {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                $timeout(function () {
                    $scope.init();
                }, 300);

            }
        });

        //$scope.getList();
    }]);




