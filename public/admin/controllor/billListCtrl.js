app.controller('billListCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$timeout', function($scope, $rootScope, adminService, $filter, Auth, $state, $timeout) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.billModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
            status: '',
            isLoadingProgress: false
        };

        $scope.pagePerCount = [50, 100];
        $scope.billModel.limit = $scope.pagePerCount[0];

        $scope.searchFilter = {
            name: ''
        };

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }

        $scope.getList = function() 
        {
            var getListParam = {};
//            getListParam.id = '';
//            getListParam.name = $scope.searchFilter.name;
//            getListParam.start = ($scope.billModel.currentPage - 1) * $scope.billModel.limit;
//            getListParam.limit = $scope.billModel.limit;
            $scope.billModel.isLoadingProgress = true;

            adminService.getBillList(getListParam).then(function(response) {
                var data = response.data.data.list;
                $scope.billModel.list = data;
                $scope.billModel.total = response.data.total;
                $scope.billModel.isLoadingProgress = false;
            });
        };
        $scope.getList();
    }]);








