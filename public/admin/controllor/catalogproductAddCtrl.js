app.controller('catalogproductAddCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$timeout', 'ValidationFactory', 'APP_CONST', '$httpService', 'sweet', 'StutzenHttpService', function($scope, $rootScope, adminService, $filter, Auth, $state, $timeout, ValidationFactory, APP_CONST, $httpService, sweet, StutzenHttpService) {
        $scope.catalogproductAddModel = {
            id: '',
            name: '',
            categoryId: '',
            code: '',
            desc: '',
            weight: '',
            itemPhoto: '',
            itemThumbnailPhoto: '',
            limit: '',
            categoryList: [],
            tagList: [],
            isActive: true
        };
        $scope.pagePerCount = [50, 100];
        $scope.catalogproductAddModel.limit = $scope.pagePerCount[0];
        $scope.searchFilterValue = "";
        $scope.initTableFilterTimeoutPromise = null;
        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }
        $scope.validationFactory = ValidationFactory;
        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function()
        {
            if (typeof $scope.catalogproduct_add_form != 'undefined' && typeof $scope.catalogproduct_add_form.$pristine != 'undefined' && !$scope.catalogproduct_add_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }
            return "";
        }
        $scope.isDataSavingProcess = false;
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.uploadedFileQueue = [];
        $scope.uploadedFileCount = 0;
        $scope.isImageSavingProcess = false;
        $scope.$on("updateFileUploadEvent", updateFileUploadEventHandler);
        function updateFileUploadEventHandler(event, data)
        {
            $scope.uploadedFileQueue = data;
            console.log('$scope.uploadedFileQueue');
            console.log($scope.uploadedFileQueue);
            $scope.saveImagePath();
        }
        $scope.saveImagePath = function()
        {
            if ($scope.uploadedFileQueue.length == 0)
            {
                $scope.closePopup('fileupload');
            }
            var hasUploadCompleted = true;
            for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
            {
                if (!$scope.uploadedFileQueue[i].isSuccess)
                {
                    hasUploadCompleted = false;
                    $scope.$broadcast('genericErrorEvent', 'Image upload is in progress.');
                    break;
                }
            }
            if (hasUploadCompleted && $scope.uploadedFileQueue.length > 0)
            {
                $scope.uploadedFileCount = $scope.uploadedFileQueue.length;
                $scope.isImageSavingProcess = true;
                $scope.catalogproductAddModel.attach = [];
                for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
                {
                    var imageuploadparam = {};
                    imageuploadparam.path = $scope.uploadedFileQueue[i].urlpath;
                    imageuploadparam.urlpath = $scope.uploadedFileQueue[i].urlpath;
                    $scope.catalogproductAddModel.attach.push(imageuploadparam);
                }
                $scope.isImageSavingProcess = false;
                $scope.closePopup('fileupload');
            }
        }
        $scope.formCancel = function() {
            $scope.catalogproduct_add_form.$setPristine();
            if (typeof $scope.catalogproduct_add_form != 'undefined')
            {
                $scope.catalogproductAddModel.name = "";
                $scope.catalogproductAddModel.code = "";
                $scope.catalogproductAddModel.desc = "";
                $scope.catalogproductAddModel.weight = "";
                $scope.catalogproductAddModel.itemPhoto = "";
                $scope.catalogproductAddModel.categoryIds = "";
                $scope.catalogproductAddModel.itemThumbnailPhoto = "";
            }
        }
        $scope.getCategoryList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            autosearchParam.isActive = 1;
            return $httpService.get(APP_CONST.API.CATALOG_CATEGORY_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.data;
                var hits = data.categoryList;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatCategoryModel = function(model)
        {
            if (model != null)
            {
                return model.name;
            }
            return  '';
        }
        $scope.addCategory = function(info)
        {
            if(info != "undefined" && info != null && info != "" )
            {
                var found = false;
                if ($scope.catalogproductAddModel.categoryList.length > 0)
                {
                    for (var i = 0; i < $scope.catalogproductAddModel.categoryList.length; i++)
                    {
                        if (info.categoryId == $scope.catalogproductAddModel.categoryList[i].categoryId)
                        {
                            found = true;
                        }
                    }
                }
                if (found == true)
                {
                    sweet.show('Oops...', 'Already Added', 'error');
                }
                else
                {
                    $scope.catalogproductAddModel.categoryList.push(info);
                }
                $scope.catalogproductAddModel.categoryInfo = "";
            }
        }
        $scope.deleteCategory = function(id, index)
        {
            $scope.catalogproductAddModel.categoryList.splice(index, 1);
        }

        $scope.createCatalogProduct = function()
        {
            if ($scope.isDataSavingProcess == false)
            {
                $scope.isDataSavingProcess = true;
                var createCatalogProductParam = {};
                var headers = {};
                headers['screen-code'] = 'catalogproduct';
                createCatalogProductParam.itemId = 0;
                createCatalogProductParam.name = $scope.catalogproductAddModel.name;
                createCatalogProductParam.code = $scope.catalogproductAddModel.code;
                createCatalogProductParam.desc = $scope.catalogproductAddModel.desc;
                createCatalogProductParam.weight = $scope.catalogproductAddModel.weight;
                createCatalogProductParam.itemPhoto = "";
                createCatalogProductParam.itemThumbnailPhoto = "";
                if (typeof $scope.catalogproductAddModel.attach != "undefined" && $scope.catalogproductAddModel.attach.length > 0)
                {
                    createCatalogProductParam.itemPhoto = $scope.catalogproductAddModel.attach[0].urlpath;
                    createCatalogProductParam.itemThumbnailPhoto = $scope.catalogproductAddModel.attach[0].urlpath;
                }
                createCatalogProductParam.is_active = 1;
                var catagoryId = [];
                for (var i = 0; i < $scope.catalogproductAddModel.categoryList.length; i++)
                {
                    catagoryId.push($scope.catalogproductAddModel.categoryList[i].categoryId);
                }
                createCatalogProductParam.categoryId = String(catagoryId);
                var tagId = [];
                for (var i = 0; i < $scope.catalogproductAddModel.tagList.length; i++)
                {
                    tagId.push($scope.catalogproductAddModel.tagList[i].tagId);
                }
                createCatalogProductParam.tagId = String(tagId);
                var configOption = adminService.handleOnlyErrorResponseConfig;
                adminService.createCatalogProductList(createCatalogProductParam, createCatalogProductParam.categoryId, createCatalogProductParam.tagId,configOption, headers).then(function(response) {
                    if (response.data.success == true)
                    {
                        $scope.formCancel();
                        $state.go('app.catalogproduct');
                    }
                    $scope.isDataSavingProcess = false;
                });
            }
        };

        $scope.getTagList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.isActive = 1;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getTag(autosearchParam, configOption).then(function(response) {
                if (response.data.success === true)
                {
                    $scope.catalogproductAddModel.tagListAll = [];
                    var data = response.data.data;
                    $scope.catalogproductAddModel.tagListAll = data.list;
                }
            });
//            return $httpService.get(APP_CONST.API.TAG_LIST, autosearchParam, false).then(function(responseData)
//            {
//                var data = responseData.data.data;
//                var hits = data.list;
//                if (hits.length > 10)
//                {
//                    hits.splice(10, hits.length);
//                }
//                return hits;
//            });
        };
        $scope.formatTagModel = function(model)
        {
            if (model != null)
            {
                return model.name;
            }
            return  '';
        };
        $scope.getTagList();
        $scope.addTag = function(info)
        {
            if(info != "undefined" && info != null && info != "" )
            {
                var found = false;
                if ($scope.catalogproductAddModel.tagList.length > 0)
                {
                    for (var i = 0; i < $scope.catalogproductAddModel.tagList.length; i++)
                    {
                        if (info.tagId == $scope.catalogproductAddModel.tagList[i].tagId)
                        {
                            found = true;
                        }
                    }
                }
                if (found == true)
                {
                    sweet.show('Oops...', 'Already Added', 'error');
                }
                else
                {
                    $scope.catalogproductAddModel.tagList.push(info);
                }
                $scope.catalogproductAddModel.tagInfo = "";
            }
        };

        $scope.deleteTag = function(id, index)
        {
            $scope.catalogproductAddModel.tagList.splice(index, 1);
        }

    }]);




