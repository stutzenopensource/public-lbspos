app.controller('catalogproductCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$timeout', '$httpService', '$localStorage', 'APP_CONST', function ($scope, $rootScope, adminService, $filter, Auth, $state, $timeout, $httpService, $localStorage, APP_CONST) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.catalogproductModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
            status: '',
            //             sku: '',
            serverList: null,
            isLoadingProgress: false
        };

        $scope.pagePerCount = [50, 100];
        $scope.catalogproductModel.limit = $scope.pagePerCount[0];
        $scope.searchFilter = {
            product: ''
        };
        $scope.refreshScreen = function ()
        {
            $scope.searchFilter = {
                product: ''
            };
            $scope.initTableFilter();
        }
        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }

        $scope.selectcatalogproductId = '';
        $scope.showdeletePopup = false;
        $scope.isdeleteProgress = false;
        $scope.showPopup = function (id)
        {
            $scope.selectcatalogproductId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function ()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteProgress = false;
        };

        $scope.getList = function ()
        {
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'catalogproduct';
            getListParam.id = "";
            getListParam.is_active = 1;
//            if ($scope.searchFilter.productInfo != '' && $scope.searchFilter.productInfo != null && $scope.searchFilter.productInfo != 'undefined')
//            {
//                getListParam.item_id = $scope.searchFilter.productInfo.itemId;
//            }
//            else {
//                getListParam.item_id = '';
//            }
            getListParam.item_name = $scope.searchFilter.product;
            getListParam.start = ($scope.catalogproductModel.currentPage - 1) * $scope.catalogproductModel.limit;
            getListParam.limit = $scope.catalogproductModel.limit;
            $scope.catalogproductModel.isLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getCatalogProductDetail(getListParam, configOption, headers).then(function (response) {
                if (response.data.success === true)
                {
                    var data = response.data.data.data;
                    $scope.catalogproductModel.list = data;
                    $scope.catalogproductModel.total = response.data.data.total;
                }
                $scope.catalogproductModel.isLoadingProgress = false;
            });
        };
        //$scope.getList();

//        $scope.getProductList = function (val)
//        {
//            var autosearchParam = {};
//            autosearchParam.item_name = val;
//            autosearchParam.is_active = 1;
//            return $httpService.get(APP_CONST.API.CATALOGPRODUCT_DETAIL, autosearchParam, false).then(function (responseData)
//            {
//                var data = responseData.data.data.data;
//                var hits = data;
//                if (hits.length > 10)
//                {
//                    hits.splice(10, hits.length);
//                }
//                return hits;
//            });
//        };
//        $scope.formatProductModel = function (model)
//        {
//            if (model != null)
//            {
//                return model.name;
//            }
//            return  '';
//        };

        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'catalog_product_form';


        $scope.$on('onLocalStorageFormRenterCompleteEvent', function (event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });

        $scope.$on('onLocalStorageFieldRenterCompleteEvent', function (event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRenterCount++;
                }
                if ($scope.localStorageRenterCount >= $scope.localStorageCount)
                {
                    console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                    $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
                }

            }
        });
       

        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function (event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRetrieveCount++;
                }
                if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
                {
                    $scope.initTableFilter();
                }
                console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);
            }
        });

        $scope.init = function ()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');

        };

        $scope.$on('onLocalStorageReadyEvent', function () {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                $timeout(function () {
                    $scope.init();
                }, 300);

            }
        });
    }]);




