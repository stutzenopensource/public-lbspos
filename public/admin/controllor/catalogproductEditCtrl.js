app.controller('catalogproductEditCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', '$httpService', 'sweet', 'StutzenHttpService', function($scope, $rootScope, adminService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST, $httpService, sweet, StutzenHttpService) {

        $scope.catalogproductAddModel = {
            id: '',
            item_id: '',
            name: '',
            categoryId: '',
            code: '',
            desc: '',
            weight: '',
            itemPhoto: '',
            itemThumbnailPhoto: '',
            is_active: ' ',
            isLoadingProgress: false,
            catalogProductDetail: [],
            categoryList: [],
            tagList: [],
            priceDetails :[]
        };
        $scope.pagePerCount = [50, 100];
        $scope.catalogproductAddModel.limit = $scope.pagePerCount[0];

        $scope.validationFactory = ValidationFactory;

        $scope.userRoleList = ['', 'ROLE_ADMIN', 'ROLE_WORKFORCE'];

        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function()
        {
            if (typeof $scope.catalogproduct_edit_form != 'undefined' && typeof $scope.catalogproduct_edit_form.$pristine != 'undefined' && !$scope.catalogproduct_edit_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }
            return "";
        };
        $scope.isDataSavingProcess = false;
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.formReset = function() {
            $scope.catalogproduct_edit_form.$setPristine();
            if (typeof $scope.catalogproduct_edit_form != 'undefined')
            {
                $scope.catalogproduct_edit_form.$setPristine();
                $scope.updateCatalogProductInfo();
            }
        }
        $scope.uploadedFileQueue = [];
        $scope.uploadedFileCount = 0;
        $scope.isImageSavingProcess = false;
        $scope.$on("updateFileUploadEvent", updateFileUploadEventHandler);
        function updateFileUploadEventHandler(event, data)
        {

            $scope.uploadedFileQueue = data;
            console.log('$scope.uploadedFileQueue');
            console.log($scope.uploadedFileQueue);
            $scope.saveImagePath();
        }
        $scope.validateFileUploadStatus = function()
        {
            $scope.catalogproductAddModel.img = [];
            $scope.isImageSavingProcess = true;
            var hasUploadCompleted = true;
            for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
            {
                if (!$scope.uploadedFileQueue[i].isSuccess)
                {
                    hasUploadCompleted = false;
                    $scope.$broadcast('genericErrorEvent', 'Image upload is in progress.');
                    break;
                }
            }
            if (hasUploadCompleted && $scope.uploadedFileQueue.length > 0)
            {
                $scope.uploadedFileCount = $scope.uploadedFileQueue.length;
                $scope.isImageSavingProcess = false;
                $scope.isImageUploadComplete = true;
                $scope.saveImagePath();
            }
        }
        $scope.saveImagePath = function()
        {
            if ($scope.uploadedFileQueue.length == 0)
            {
                $scope.closePopup('fileupload');
            }
            var hasUploadCompleted = true;
            for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
            {
                if (!$scope.uploadedFileQueue[i].isSuccess)
                {
                    hasUploadCompleted = false;
                    $scope.$broadcast('genericErrorEvent', 'Image upload is in progress.');
                    break;
                }
            }
            if (hasUploadCompleted && $scope.uploadedFileQueue.length > 0)
            {
                $scope.uploadedFileCount = $scope.uploadedFileQueue.length;
                $scope.isImageSavingProcess = true;
                $scope.catalogproductAddModel.attach = [];
                for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
                {
                    var imageuploadparam = {};
                    imageuploadparam.path = $scope.uploadedFileQueue[i].urlpath;
                    imageuploadparam.urlpath = $scope.uploadedFileQueue[i].urlpath;
                    $scope.catalogproductAddModel.attach.push(imageuploadparam);
                }
                $scope.isImageSavingProcess = false;
                $scope.closePopup('fileupload');

            }
        }
        $scope.getCategoryList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.category_name = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.CATALOG_CATEGORY_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.data;
                var hits = data.categoryList;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatCategoryModel = function(model)
        {
            if (model != null)
            {
                return model.name;
            }
            return  '';
        }
        $scope.addCategory = function(info)
        {
            if(info != "undefined" && info != null && info != "" )
            {
                var found = false;
                if ($scope.catalogproductAddModel.categoryList.length > 0)
                {
                    for (var i = 0; i < $scope.catalogproductAddModel.categoryList.length; i++)
                    {
                        if (info.categoryId == $scope.catalogproductAddModel.categoryList[i].categoryId)
                        {
                            found = true;
                        }
                    }
                }
                if (found == true)
                {
                    sweet.show('Oops...', 'Already Added', 'error');
                }
                else
                {
                    $scope.catalogproductAddModel.categoryList.push(info);
                }
                $scope.catalogproductAddModel.categoryInfo = "";
            }
            
        }
        $scope.deleteCategory = function(id, index)
        {
            $scope.catalogproductAddModel.categoryList.splice(index, 1);
        }
// 3.display the data
        $scope.createCatalogProduct = function() {
            if ($scope.isDataSavingProcess == false)
            {
                var found = false;
                for(var i = 0; i < $scope.catalogproductAddModel.priceDetails.length; i++)
                {
                    if($scope.catalogproductAddModel.priceDetails[i].isprimary == true)
                    {
                        found = true;
                    }
                }
                if(found == true)
                {
                    $scope.isDataSavingProcess = true;
                    var modifyCatalogProductParam = {};
                    var headers = {};
                    headers['screen-code'] = 'catalogproduct';
                    modifyCatalogProductParam.itemId = $scope.catalogproductAddModel.id;
                    modifyCatalogProductParam.name = $scope.catalogproductAddModel.name;
                    modifyCatalogProductParam.code = $scope.catalogproductAddModel.code;
                    modifyCatalogProductParam.desc = $scope.catalogproductAddModel.desc;
                    modifyCatalogProductParam.weight = $scope.catalogproductAddModel.weight;
                    modifyCatalogProductParam.itemPhoto = $scope.catalogproductAddModel.attach[0].urlpath;
                    modifyCatalogProductParam.itemThumbnailPhoto = $scope.catalogproductAddModel.attach[0].urlpath;
                    modifyCatalogProductParam.is_active = 1;
                    var catagoryId = [];
                    for (var i = 0; i < $scope.catalogproductAddModel.categoryList.length; i++)
                    {
                        catagoryId.push($scope.catalogproductAddModel.categoryList[i].categoryId);
                    }
                    modifyCatalogProductParam.categoryId = String(catagoryId);
                    var tagId = [];
                    for (var i = 0; i < $scope.catalogproductAddModel.tagList.length; i++)
                    {
                        tagId.push($scope.catalogproductAddModel.tagList[i].tagId);
                    }
                    modifyCatalogProductParam.tagId = String(tagId);
                    adminService.createCatalogProductList(modifyCatalogProductParam, modifyCatalogProductParam.categoryId, modifyCatalogProductParam.tagId, headers).then(function(response) {
                        if (response.data.success === true)
                        {
                            $scope.formReset();
                            $scope.savePriceList();
                            $state.go('app.catalogproduct');
                        }
                        $scope.isDataSavingProcess = false;
                    });
                }
                else
                {
                    sweet.show ("Please select the primary price");
                    $scope.isDataSavingProcess = false;
                }
            }
        };
        //2.get the updated data and keep it in local
        $scope.updateCatalogProductInfo = function()
        {
            if ($scope.catalogproductAddModel.catalogProductDetail != null)
            {
                $scope.catalogproductAddModel.id = $scope.catalogproductAddModel.catalogProductDetail.itemId;
                $scope.catalogproductAddModel.name = $scope.catalogproductAddModel.catalogProductDetail.name;
                $scope.catalogproductAddModel.code = $scope.catalogproductAddModel.catalogProductDetail.code;
                $scope.catalogproductAddModel.desc = $scope.catalogproductAddModel.catalogProductDetail.desc;
                $scope.catalogproductAddModel.weight = $scope.catalogproductAddModel.catalogProductDetail.weight;
                $scope.catalogproductAddModel.desc = $scope.catalogproductAddModel.catalogProductDetail.description;
                $scope.catalogproductAddModel.categoryList = $scope.catalogproductAddModel.catalogProductDetail.categorymapping;
                $scope.catalogproductAddModel.tagList = $scope.catalogproductAddModel.catalogProductDetail.tagmapping;
//                var categoryId = {
//                    categoryId: $scope.catalogproductAddModel.catalogProductDetail.categoryId
//                }
//                var tagId = {
//                    tagId: $scope.catalogproductAddModel.catalogProductDetail.tagId
//                }
//                $scope.catalogproductAddModel.taskList.push(tagId);
//                $scope.catalogproductAddModel.categoryList.push(categoryId);
                $scope.catalogproductAddModel.attach = [];
                if ($scope.catalogproductAddModel.catalogProductDetail.itemPhoto != "undefined")
                {
                    var file = {
                        urlpath: $scope.catalogproductAddModel.catalogProductDetail.itemPhoto
                    };
                    $scope.catalogproductAddModel.attach.push(file);
                }
//                $scope.catalogproductAddModel.is_active = ($scope.catalogProductDetail.is_active == 1 ? true : false);
                $scope.catalogproductAddModel.isLoadingProgress = false;
            }
        }
        //1.get the data from the list    
        $scope.getCatalogProductInfo = function() {
            $scope.catalogproductAddModel.isLoadingProgress = true;
            if (typeof $stateParams.itemId != 'undefined')
            {
                var getListParam = {};
                getListParam.item_id = $stateParams.itemId;
                getListParam.limit = 1;
                getListParam.start = 0;
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getCatalogProductDetail(getListParam, configOption).then(function(response) {
                    var data = response.data.data.data;
                    if (data.length > 0)
                    {
                       $scope.catalogproductAddModel.catalogProductDetail = data[0];
                       $scope.updateCatalogProductInfo();
                       $scope.getPriceList ()
                    }
                });
            }
        };
        $scope.getTagList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.isActive = 1;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getTag(autosearchParam, configOption).then(function(response) {
                if (response.data.success === true)
                {
                    $scope.catalogproductAddModel.tagListAll = [];
                    var data = response.data.data;
                    $scope.catalogproductAddModel.tagListAll = data.list;
                }
            });
        };
        $scope.formatTagModel = function(model)
        {
            if (model != null)
            {
                return model.name;
            }
            return  '';
        };
        $scope.getTagList();
        $scope.addTag = function(info)
        {
            if(info != "undefined" && info != " " && info != null)
            {
                var found = false;
                if ($scope.catalogproductAddModel.tagList.length > 0)
                {
                    for (var i = 0; i < $scope.catalogproductAddModel.tagList.length; i++)
                    {
                        if (info.tagId == $scope.catalogproductAddModel.tagList[i].tagId)
                        {
                            found = true;
                        }
                    }
                }
                if (found == true)
                {
                    sweet.show('Oops...', 'Already Added', 'error');
                }
                else
                {
                    $scope.catalogproductAddModel.tagList.push(info);
                }
                $scope.catalogproductAddModel.tagInfo = "";
            }
            
        };

        $scope.deleteTag = function(id, index)
        {
            $scope.catalogproductAddModel.tagList.splice(index, 1);
        }

        $scope.getCatalogProductInfo();
        
        
        /// pricing details
        $scope.addPriceInfo = function () {
            var addPriceParam = {};
            var found = false;
            addPriceParam.id = " ";
            addPriceParam.desc = " ";
            addPriceParam.itemId = $scope.catalogproductAddModel.catalogProductDetail.itemId;
            addPriceParam.isprimary = false;
            addPriceParam.pricevalue = '';
            if($scope.catalogproductAddModel.priceDetails.length==0)
            {
                addPriceParam.isprimary = true;
            }
            $scope.catalogproductAddModel.priceDetails.push(addPriceParam);
              
      };
    
    $scope.checkPrimary = function(index)
    {
        for (var i = 0; i < $scope.catalogproductAddModel.priceDetails.length; i++)
        {
            if (index != $scope.catalogproductAddModel.priceDetails[i].index )
            {
                $scope.catalogproductAddModel.priceDetails[i].isprimary = false;
            }
        }
    };
    $scope.removePriceInfo = function(priceIndex)
    {
        var id ='';
        for (var i = 0; i < $scope.catalogproductAddModel.priceDetails.length; i++)
        {
            if (priceIndex == $scope.catalogproductAddModel.priceDetails[i].index )
            {
                id = i;
            }
        }
        $scope.catalogproductAddModel.priceDetails.splice(id, 1);
    };
    $scope.savePriceList = function () 
      {
          var savePriceListParam = [];
          $scope.isDataSavingProcess = true;
          for(var i = 0; i < $scope.catalogproductAddModel.priceDetails.length; i++)
          {
              var savePriceList = {};
              if($scope.catalogproductAddModel.priceDetails[i].priceId != null && $scope.catalogproductAddModel.priceDetails[i].priceId != "" && $scope.catalogproductAddModel.priceDetails[i].priceId != "undefined")
              {
                  savePriceList.priceId = $scope.catalogproductAddModel.priceDetails[i].priceId;
              }
              else
              {
                  savePriceList.priceId = 0;
              }
              savePriceList.itemId = $scope.catalogproductAddModel.priceDetails[i].itemId;
              savePriceList.pricevalue = $scope.catalogproductAddModel.priceDetails[i].pricevalue;
              savePriceList.desc = $scope.catalogproductAddModel.priceDetails[i].desc;
              savePriceList.priceisprimary = 0;
              if($scope.catalogproductAddModel.priceDetails[i].isprimary == true)
              {
                  savePriceList.priceisprimary = 1;
              }
              savePriceListParam.push(savePriceList);
          }
          var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
          adminService.savePrice(savePriceListParam,$stateParams.itemId, configOption).then(function(response) {
            var data = response.data;
            if (data.success == true )
            {
               $scope.isDataSavingProcess = false;
            }
        });

      }
      $scope.getPriceList = function()
        {
            var autosearchParam = {};
            autosearchParam.itemId = $scope.catalogproductAddModel.catalogProductDetail.itemId;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getPriceList(autosearchParam.itemId, configOption).then(function(response) {
                if (response.data.success === true)
                {
                    $scope.catalogproductAddModel.priceDetails = [];
                    var data = response.data.data;
                    if(data.priceList.length == 0)
                    {
                        $scope.addPriceInfo ();
                    }
                    else
                    {
                        $scope.catalogproductAddModel.priceDetails = data.priceList;
                        for(var i = 0;i<$scope.catalogproductAddModel.priceDetails.length;i++)
                        {
                            if($scope.catalogproductAddModel.priceDetails[i].priceisprimary == 1)
                            {
                                $scope.catalogproductAddModel.priceDetails[i].isprimary = true;
                            }
                            else
                            {
                                $scope.catalogproductAddModel.priceDetails[i].isprimary = false;
                            }
                            $scope.catalogproductAddModel.priceDetails[i].index = i;
                        }
                    }
                }
            });
        };
        
    }]);



