app.controller('categoryWiseReportCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', function ($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.categoryModel = {
            currentPage: 1,
            total: 0,
            total_Amount: 0,
            limit: 10,
            list: [],
            length: [],
            serverList: null,
            isLoadingProgress: false,
            isSearchLoadingProgress: false,
            categorylist: [],
            catName: '',
            total_qty: 0
        };
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.categoryModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
        $scope.validationFactory = ValidationFactory;
        $scope.checked = false;
        $scope.showCategory = false;
        $scope.openDate = function (index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            } else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };

        $scope.searchFilter = {
            category_id: '',
            inventory : '-1'
        };
        $scope.clearFilters = function ()
        {
            $scope.searchFilter = {
                category_id: '',
                inventory : '-1'
            };
            $scope.checked = false;
            $scope.showCategory = false;
            $scope.categoryModel.list = [];
        }

        $scope.print = function ()
        {
            $window.print();
        };

        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.show = function (index)
        {

            if ($scope.showDetails[index])
            {
                $scope.showDetails[index] = false;
            } else
                $scope.showDetails[index] = true;
        };

        $scope.getList = function () {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'categorystockreport';
            getListParam.show_all = 1;
            getListParam.category_id = $scope.searchFilter.category_id;
            getListParam.mode = $scope.searchFilter.inventory;
            $scope.categoryModel.isLoadingProgress = true;
            $scope.showDetails = [];
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getCategoryWise(getListParam, configOption, headers).then(function (response)
            {
                var totalAmt = 0.00;
                var totalMrp = 0.00;
                var totalQty = 0.00;
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.categoryModel.list = data;

                    for (var i = 0; i < $scope.categoryModel.list.length; i++)
                    {
                        totalAmt = parseFloat(totalAmt) + parseFloat($scope.categoryModel.list[i].cost);
                        totalMrp = parseFloat(totalMrp) + parseFloat($scope.categoryModel.list[i].tot_mrp);
                        totalQty = parseFloat(totalQty) + parseFloat($scope.categoryModel.list[i].qty);
                        //$scope.categoryModel.list[i].cost = utilityService.changeCurrency($scope.categoryModel.list[i].cost, $rootScope.appConfig.thousand_seperator);
                        //$scope.categoryModel.list[i].tot_mrp = utilityService.changeCurrency($scope.categoryModel.list[i].tot_mrp, $rootScope.appConfig.thousand_seperator);
                        $scope.categoryModel.list[i].flag = i;
                        $scope.showDetails[i] = false;
                        for (var j = 0; j < $scope.categoryModel.list[i].product.length; j++)
                        {
                            $scope.categoryModel.list[i].product[j].cost = parseFloat($scope.categoryModel.list[i].product[j].cost).toFixed(2);
                            $scope.categoryModel.list[i].product[j].cost = utilityService.changeCurrency($scope.categoryModel.list[i].product[j].cost, $rootScope.appConfig.thousand_seperator);
                            $scope.categoryModel.list[i].product[j].newdate = utilityService.parseStrToDate($scope.categoryModel.list[i].product[j].date);
                            $scope.categoryModel.list[i].product[j].date = utilityService.parseDateToStr($scope.categoryModel.list[i].product[j].newdate, $scope.adminService.appConfig.date_format);
                            $scope.categoryModel.list[i].product[j].flag = i;
                        }

                    }
                    $scope.categoryModel.total_Amount = totalAmt;
                    $scope.categoryModel.total_Amount = parseFloat($scope.categoryModel.total_Amount).toFixed(2);
                    $scope.categoryModel.total_Amount = utilityService.changeCurrency($scope.categoryModel.total_Amount, $rootScope.appConfig.thousand_seperator);

                    $scope.categoryModel.total_MRPAmount = totalMrp;
                    $scope.categoryModel.total_MRPAmount = parseFloat($scope.categoryModel.total_MRPAmount).toFixed(2);
                    $scope.categoryModel.total_MRPAmount = utilityService.changeCurrency($scope.categoryModel.total_MRPAmount, $rootScope.appConfig.thousand_seperator);

                    $scope.categoryModel.total_qty = totalQty;
                    $scope.categoryModel.total_qty = parseFloat($scope.categoryModel.total_qty);
                    $scope.categoryModel.total_qty = utilityService.changeCurrency($scope.categoryModel.total_qty, $rootScope.appConfig.thousand_seperator);

                }
                $scope.categoryModel.isLoadingProgress = false;
            });

        };
        $scope.getcategoryList = function ()
        {
            var getListParam = {};
            getListParam.id = "";
            getListParam.category_id = "";
            getListParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getCategoryList(getListParam, configOption).then(function (response)
            {
                var data = response.data;
                $scope.categoryModel.categorylist = data.list;

            });
        };

        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'category_report_form';


        $scope.$on('onLocalStorageFormRenterCompleteEvent', function (event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });

        $scope.$on('onLocalStorageFieldRenterCompleteEvent', function (event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRenterCount++;
                }
                if ($scope.localStorageRenterCount >= $scope.localStorageCount)
                {
                    console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                    $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
                }

            }
        });

        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function (event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRetrieveCount++;
                }
                if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
                {
                    $scope.initTableFilter();
                }
                console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);
            }
        });

        $scope.init = function ()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');

        };

        $scope.$on('onLocalStorageReadyEvent', function () {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                $timeout(function () {
                    $scope.init();
                }, 300);

            }
        });
        $scope.categoryNames = function ()
        {
            if ($scope.searchFilter.category_id !== null && $scope.searchFilter.category_id !== '')
            {
                for (var i = 0; i < $scope.categoryModel.categorylist.length; i++)
                {
                    if ($scope.searchFilter.category_id == $scope.categoryModel.categorylist[i].id)
                    {
                        $scope.categoryModel.catName = $scope.categoryModel.categorylist[i].name;
                    }
                }
            }
        };
$scope.print = function (div) {

            var docHead = document.head.outerHTML;
            var printContents = document.getElementById(div).outerHTML;
            var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=865, height=600, resizable=yes, screenX=200, screenY=200, personalbar=no, scrollbars=yes";

            var newWin = window.open("", "_blank", winAttr);
            var writeDoc = newWin.document;
            writeDoc.open();
            writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
            writeDoc.close();
            newWin.focus();

        }


        $scope.getcategoryList();
        //$scope.getList();
    }]);




