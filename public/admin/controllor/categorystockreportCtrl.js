app.controller('categorystockreportCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', function ($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.categoryModel = {
            currentPage: 1,
            total: 0,
            total_Amount: 0,
            limit: 10,
            list: [],
            length: [],
            serverList: null,
            isLoadingProgress: false,
            isSearchLoadingProgress: false,
            categorylist: [],
            total_qty: 0,
            categorylist: []
        };
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.categoryModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.quantityFormat = $rootScope.appConfig.qty_format;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
        $scope.validationFactory = ValidationFactory;
        $scope.checked = false;
        $scope.showCategory = false;
        $scope.openDate = function (index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            } else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };

        $scope.searchFilter = {
            productInfo: {},
            inventory:'-1',
            categoryinfo: {}
        };
        $scope.clearFilters = function ()
        {
            $scope.searchFilter = {
                productInfo: {},
                inventory:'-1',
                categoryinfo: {}
            };
            $scope.checked = false;
            $scope.showCategory = false;
            $scope.categoryModel.list = [];
        }

        $scope.print = function (div)
        {
            var docHead = document.head.outerHTML;
            var printContents = document.getElementById(div).outerHTML;
            var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=865, height=600, resizable=yes, screenX=200, screenY=200, personalbar=no, scrollbars=yes";

            var newWin = window.open("", "_blank", winAttr);
            var writeDoc = newWin.document;
            writeDoc.open();
            writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
            writeDoc.close();
            newWin.focus();
        };

        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.show = function (index)
        {

            if ($scope.showDetails[index])
            {
                $scope.showDetails[index] = false;
            } else
                $scope.showDetails[index] = true;
        };

        $scope.getList = function () {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'categorystockreport';
            //        if ($scope.searchFilter.todate != '' && ($scope.searchFilter.fromdate == null || $scope.searchFilter.fromdate == ''))
            //        {
            //            if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
            //            {
            //                getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
            //            }
            //            getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
            //            getListParam.from_date = getListParam.to_date;
            //        }
            //        else if ($scope.searchFilter.fromdate != '' && ($scope.searchFilter.todate == null || $scope.searchFilter.todate == ''))
            //        {
            //            if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
            //            {
            //                getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
            //            }
            //            getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
            //            getListParam.to_date = getListParam.from_date;
            //        }
            //        else
            //        {
            //            if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
            //            {
            //                getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
            //            }
            //            if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
            //            {
            //                getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
            //            }
            //            getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
            //            getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
            //        }
            getListParam.show_all = 1;
            getListParam.mode = $scope.searchFilter.inventory;
//            getListParam.category_id = $scope.searchFilter.category_id;
            if ($scope.searchFilter.productInfo != null && typeof $scope.searchFilter.productInfo != 'undefined' && typeof $scope.searchFilter.productInfo.id != 'undefined')
            {
                getListParam.product_id = $scope.searchFilter.productInfo.id;
            } else
            {
                getListParam.product_id = '';
            }
            getListParam.start = 0;
            getListParam.limit = 0;
            
            if ($scope.searchFilter.categoryInfo != null && typeof $scope.searchFilter.categoryInfo != 'undefined' && typeof $scope.searchFilter.categoryInfo.id != 'undefined')
            {
                getListParam.category_id = $scope.searchFilter.categoryInfo.id;
            } else
            {
                getListParam.category_id = '';
            }
            
            getListParam.is_active = 1;
            $scope.categoryModel.isLoadingProgress = true;
            $scope.showDetails = [];
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getCategorywiseStock(getListParam, configOption, headers).then(function (response)
            {
                var totalAmt = 0.00;
                var totalMrp = 0.00;
                var totalQty = 0.00;
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.categoryModel.list = data;

                    for (var i = 0; i < $scope.categoryModel.list.length; i++)
                    {
                        totalAmt = parseFloat(totalAmt) + parseFloat($scope.categoryModel.list[i].overall_sales_price == null ? 0 : $scope.categoryModel.list[i].overall_sales_price);
                        totalMrp = parseFloat(totalMrp) + parseFloat($scope.categoryModel.list[i].overall_purchase_price == null ? 0 : $scope.categoryModel.list[i].overall_purchase_price);
                        totalQty = parseFloat(totalQty) + parseFloat($scope.categoryModel.list[i].quantity);
                        //$scope.categoryModel.list[i].cost = utilityService.changeCurrency($scope.categoryModel.list[i].cost, $rootScope.appConfig.thousand_seperator);
                        //$scope.categoryModel.list[i].tot_mrp = utilityService.changeCurrency($scope.categoryModel.list[i].tot_mrp, $rootScope.appConfig.thousand_seperator);
//                        $scope.categoryModel.list[i].flag = i;
//                        $scope.showDetails[i] = false;
//                    for(var j = 0; j < $scope.categoryModel.list[i].product.length; j++)
//                    {  
//                        $scope.categoryModel.list[i].product[j].cost = parseFloat($scope.categoryModel.list[i].product[j].cost).toFixed(2);
//                        $scope.categoryModel.list[i].product[j].cost = utilityService.changeCurrency($scope.categoryModel.list[i].product[j].cost, $rootScope.appConfig.thousand_seperator);
//                        $scope.categoryModel.list[i].product[j].newdate = utilityService.parseStrToDate($scope.categoryModel.list[i].product[j].date);
//                        $scope.categoryModel.list[i].product[j].date = utilityService.parseDateToStr($scope.categoryModel.list[i].product[j].newdate, $scope.adminService.appConfig.date_format);
//                        $scope.categoryModel.list[i].product[j].flag = i;
//                    }

                    }
                    $scope.categoryModel.total_Amount = totalAmt;
                    $scope.categoryModel.total_Amount = parseFloat($scope.categoryModel.total_Amount).toFixed(2);
                    $scope.categoryModel.total_Amount = utilityService.changeCurrency($scope.categoryModel.total_Amount, $rootScope.appConfig.thousand_seperator);

                    $scope.categoryModel.total_MRPAmount = totalMrp;
                    $scope.categoryModel.total_MRPAmount = parseFloat($scope.categoryModel.total_MRPAmount).toFixed(2);
                    $scope.categoryModel.total_MRPAmount = utilityService.changeCurrency($scope.categoryModel.total_MRPAmount, $rootScope.appConfig.thousand_seperator);

                    $scope.categoryModel.total_qty = totalQty;
                    $scope.categoryModel.total_qty = parseFloat($scope.categoryModel.total_qty).toFixed($scope.quantityFormat);
                    $scope.categoryModel.total_qty = utilityService.changeCurrency($scope.categoryModel.total_qty, $rootScope.appConfig.thousand_seperator);

                }
                $scope.categoryModel.isLoadingProgress = false;
            });

        };
        $scope.getProductList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.is_active = 1
            return $httpService.get(APP_CONST.API.PRODUCT_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatProductModel = function (model)
        {
            if (model != null)
            {
                if (model.name != undefined && model.sku != undefined)
                {
                    return model.name + '(' + model.sku + ')';
                } else if (model.name != undefined && model.sku != undefined)
                {
                    return model.name + '(' + model.sku + ')';
                }
            }
            return  '';
        };
//        $scope.getcategoryList = function ()
//        {
//            var getListParam = {};
//            getListParam.id = "";
//            getListParam.category_id = "";
//
//            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
//            adminService.getCategoryList(getListParam, configOption).then(function (response)
//            {
//                var data = response.data;
//                $scope.categoryModel.categorylist = data.list;
//
//            });
//        };
        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'category_report_form';


        $scope.$on('onLocalStorageFormRenterCompleteEvent', function (event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });

        $scope.$on('onLocalStorageFieldRenterCompleteEvent', function (event, formName) {

            if ($scope.localStorageFormName == formName)
            {
                $scope.localStorageRenterCount++;
            }
            if ($scope.localStorageRenterCount >= $scope.localStorageCount)
            {
                console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
            }


        });



        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function (event, formName) {

            if ($scope.localStorageFormName == formName)
            {
                $scope.localStorageRetrieveCount++;
            }
            if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
            {
                $scope.initTableFilter();
            }
            console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);

        });

        $scope.init = function ()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');

        };
        $scope.$on('updateTypeaheadFieldValue', function (event, data) {

            if (data.fieldName == "category_report_form-product_dropdown")
            {
                $scope.searchFilter.productInfo = data.value;
            }
        });

        $scope.$on('onLocalStorageReadyEvent', function () {

            $timeout(function () {
                $scope.init();
            }, 300);
        });
        $scope.getCategoryList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.GET_CATEGORY_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatCategoryModel = function (model)
        {
            if (model != null)
            {
                if (model.name != undefined)
                {
                    return model.name;
                }
            }
            return  '';
        };
//        $scope.getcategoryList();
        //$scope.getList();
    }]);




