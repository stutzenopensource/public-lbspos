app.controller('catelogcategoryAddCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$timeout', 'ValidationFactory', function($scope, $rootScope, adminService, $filter, Auth, $state, $timeout, ValidationFactory) {

    $scope.categoryAddModel = {
        id: '',
        name: "",
        description: '',
        isActive: true,
        attachments: []

    };

    $scope.validationFactory = ValidationFactory;


    $scope.getNavigationBlockMsg = function(pageReload)
    {
        if (typeof $scope.catalog_category_form != 'undefined' && typeof $scope.catalog_category_form.$pristine != 'undefined' && !$scope.catalog_category_form.$pristine)
        {
            return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
        }

        return "";
    }
    $scope.isDataSavingProcess = false;
    $scope.formReset = function() {
        $scope.categoryAddModel.name = '';
        $scope.categoryAddModel.description = '';
        $scope.catalog_category_form.$setPristine();
        if($scope.uploadedFileQueue.length > 0)
        {    
            $scope.uploadedFileQueue[0].remove();
        }
        $scope.categoryAddModel.attachments = [];
        $scope.saveImagePath();
    };

    $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

    $scope.createCategory = function() {
        if ($scope.isDataSavingProcess == false)
        {
            $scope.isDataSavingProcess = true;
            var createParam = {};
            var headers = {};
            headers['screen-code'] = 'categoryCatlog';
            createParam.categoryId = 0;
            createParam.name = $scope.categoryAddModel.name;
            createParam.desc = $scope.categoryAddModel.description;
            createParam.imageId = 0;
            createParam.accountId = 0;
            if($scope.categoryAddModel.attachments.length > 0)
            {    
                createParam.categoryPhoto = $scope.categoryAddModel.attachments[0].url;
            }
            createParam.comments = "";
            createParam.isActive = 1;
            adminService.createCatalogCategory(createParam, adminService.handleSuccessAndErrorResponse, headers).then(function(response) {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.catelogcategory');
                }
                $scope.isDataSavingProcess = false;
            });
        }
    };

    $scope.showUploadFilePopup = false;
    $scope.showPhotoGalaryPopup = false;

    $scope.showPopup = function(value) {

        if (value == 'fileupload')
        {
            $scope.showUploadFilePopup = true;
        }
        else if (value == 'photogalary')
        {
            $scope.showPhotoGalaryPopup = true;
        }
    };

    $scope.closePopup = function(value) {

        if (value == 'fileupload')
        {
            $scope.showUploadFilePopup = false;
        }
        else if (value == 'photogalary')
        {
            $scope.showPhotoGalaryPopup = false;
        }
    };
    $scope.uploadedFileQueue = [];
    $scope.uploadedFileCount = 0;
    $scope.isImageSavingProcess = false;

    $scope.$on("updateFileUploadEvent", updateFileUploadEventHandler);
    function updateFileUploadEventHandler(event, data)
    {
        $scope.uploadedFileQueue = data;
        console.log('$scope.uploadedFileQueue');
        console.log($scope.uploadedFileQueue);
    }


    $scope.$on("deletedUploadFileEvent", deletedUploadFileEventHandler);
    function deletedUploadFileEventHandler(event, index)
    {
        if ($scope.categoryAddModel.imgs.length > index)
        {
            var imageUpdateParam = {};
            imageUpdateParam.id = $scope.categoryAddModel.imgs[index].id;
            var configData = {};
            configData.imgIndex = index;

            adminService.deleteProfileImage(imageUpdateParam, configData).then(function(response) {

                var config = response.config.config;
                if (typeof config.imgIndex !== 'undefined')
                {
                    var index = config.imgIndex;
                    $scope.categoryAddModel.imgs.splice(index, 1);
                    $scope.$broadcast('genericSuccessEvent', 'Image removed successfully.');
                }

            });
        }


        console.log('$scope.uploadedImageList');
        console.log($scope.uploadedImageList);
    }


    $scope.deleteImage = function($index)
    {
        $scope.categoryAddModel.attachments.splice($index, 1);
    }

    $scope.saveImagePath = function()
    {
        if ($scope.uploadedFileQueue.length == 0)
        {
            if ($scope.showUploadMoreFilePopup)
            {
                $scope.closePopup('fileupload');
            }
        }
        var hasUploadCompleted = true;
        for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
        {
            if (!$scope.uploadedFileQueue[i].isSuccess)
            {
                hasUploadCompleted = false;
                $scope.$broadcast('genericErrorEvent', 'Image upload is in progress.');
                break;
            }
        }
        if (hasUploadCompleted && $scope.uploadedFileQueue.length > 0)
        {
            $scope.uploadedFileCount = $scope.uploadedFileQueue.length;
            $scope.isImageSavingProcess = true;
            $scope.isImageUploadComplete = true;
            for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
            {
                //               $scope.categoryAddModel.attachments.push($scope.uploadedFileQueue[i]);
                var imageUpdateParam = {};
                //imageUpdateParam.id = 0;
                imageUpdateParam.url = $scope.uploadedFileQueue[i].urlpath;
                //imageUpdateParam.ref_id = $stateParams.id;
                //imageUpdateParam.type = 'product';
                imageUpdateParam.comments = '';
                imageUpdateParam.signedUrl = $scope.uploadedFileQueue[i].signedUrl;
                imageUpdateParam.path = $scope.uploadedFileQueue[i].signedUrl;
                $scope.categoryAddModel.attachments.push(imageUpdateParam);
                if (i == $scope.uploadedFileQueue.length - 1)
                {
                    $scope.closePopup('fileupload');
                }
            }
        } else
{
            $scope.closePopup('fileupload');
        }
    };

}]);




