app.controller('catelogcategoryEditCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', function($scope, $rootScope, adminService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory) {

    $scope.categoryEditModel = {
        id: '',
        name: "",
        description: '',
        isActive: true,
        limit: 10,
        currentPage: 1,
        total: 0,
        imgs: [],
        isLoadingProgress: false,
        attachments: []

    };

    $scope.validationFactory = ValidationFactory;
    $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
    $scope.getNavigationBlockMsg = function(pageReload)
    {
        if (typeof $scope.catalog_category_form != 'undefined' && typeof $scope.catalog_category_form.$pristine != 'undefined' && !$scope.catalog_category_form.$pristine)
        {
            return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
        }

        return "";
    };
        
    $scope.isDataSavingProcess = false;
    $scope.formReset = function() {
        if (typeof $scope.catalog_category_form != 'undefined')
        {
            //                $scope.catalog_category_form.$setPristine();
            $scope.categoryEditModel.name = "";
            $scope.categoryEditModel.description = "";
            $scope.categoryEditModel.attachments = [];               
            $scope.getCategory();
        }

    }

    $scope.updateCategoryInfo = function()
    {
        $scope.categoryEditModel.id = $scope.categoryEditModel.list.categoryId;
        $scope.categoryEditModel.name = $scope.categoryEditModel.list.name;
        $scope.categoryEditModel.description = $scope.categoryEditModel.list.desc;
        if ($scope.categoryEditModel.list.categoryPhoto != '' && $scope.categoryEditModel.list.categoryPhoto != null)
        {
            var imageParams = {};
            imageParams.url = $scope.categoryEditModel.list.categoryPhoto;
            $scope.categoryEditModel.attachments.push(imageParams);
            console.log($scope.categoryEditModel.attachments);
        }
        $scope.categoryEditModel.isLoadingProgress = false;
    };

    $scope.createCategory = function() {
        if ($scope.isDataSavingProcess == false)
        {
            $scope.isDataSavingProcess = true;
            var createParam = {};
            var headers = {};
            headers['screen-code'] = 'categoryCatlog';
            createParam.categoryId = $scope.categoryEditModel.id;
            createParam.name = $scope.categoryEditModel.name;
            createParam.desc = $scope.categoryEditModel.description;
            createParam.imageId = 0;
            createParam.accountId = 0;
            if($scope.categoryEditModel.attachments.length > 0)
            {
                createParam.categoryPhoto = $scope.categoryEditModel.attachments[0].url;
            }
                
            createParam.comments = "";
            createParam.isActive = 1;
            adminService.editCatalogCategory(createParam, adminService.handleSuccessAndErrorResponse, headers).then(function(response) {
                if (response.data.success === true)
                {
                    $scope.formReset();
                    $state.go('app.catelogcategory');
                }
                $scope.isDataSavingProcess = false;
            });
        }
    };

    $scope.showUploadFilePopup = false;
    $scope.showPhotoGalaryPopup = false;

    $scope.showPopup = function(value) {

        if (value == 'fileupload')
        {
            $scope.showUploadFilePopup = true;
        }
        else if (value == 'photogalary')
        {
            $scope.showPhotoGalaryPopup = true;
        }
    };

    $scope.closePopup = function(value) {

        if (value == 'fileupload')
        {
            $scope.showUploadFilePopup = false;
        }
        else if (value == 'photogalary')
        {
            $scope.showPhotoGalaryPopup = false;
        }
    };
    $scope.uploadedFileQueue = [];
    $scope.uploadedFileCount = 0;
    $scope.isImageSavingProcess = false;

    $scope.$on("updateFileUploadEvent", updateFileUploadEventHandler);
    function updateFileUploadEventHandler(event, data)
    {
        $scope.uploadedFileQueue = data;
        console.log('$scope.uploadedFileQueue');
        console.log($scope.uploadedFileQueue);
    }

    $scope.saveImagePath = function()
    {
        if ($scope.uploadedFileQueue.length == 0)
        {
            if ($scope.showUploadMoreFilePopup)
            {
                $scope.closePopup('fileupload');
            }
        }
        var hasUploadCompleted = true;
        for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
        {
            if (!$scope.uploadedFileQueue[i].isSuccess)
            {
                hasUploadCompleted = false;
                $scope.$broadcast('genericErrorEvent', 'Image upload is in progress.');
                break;
            }
        }
        if (hasUploadCompleted && $scope.uploadedFileQueue.length > 0)
        {
            $scope.uploadedFileCount = $scope.uploadedFileQueue.length;
            $scope.isImageSavingProcess = true;
            $scope.isImageUploadComplete = true;
            $scope.categoryEditModel.attachments = [];
            for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
            {
                //               $scope.categoryEditModel.attachments.push($scope.uploadedFileQueue[i]);
                var imageUpdateParam = {};
                //imageUpdateParam.id = 0;
                imageUpdateParam.url = $scope.uploadedFileQueue[i].urlpath;
                //imageUpdateParam.ref_id = $stateParams.id;
                imageUpdateParam.type = 'product';
                imageUpdateParam.comments = '';
                imageUpdateParam.signedUrl = $scope.uploadedFileQueue[i].signedUrl;
                imageUpdateParam.path = $scope.uploadedFileQueue[i].signedUrl;
                $scope.categoryEditModel.attachments.push(imageUpdateParam);
                if (i == $scope.uploadedFileQueue.length - 1)
                {
                    $scope.closePopup('fileupload');
                }
            }
        } else
{
            $scope.closePopup('fileupload');
        }
    };

    $scope.getCategory = function() {

        var getListParam = {};
        getListParam.category_id = $stateParams.id;
        getListParam.start = ($scope.categoryEditModel.currentPage - 1) * $scope.categoryEditModel.limit;
        getListParam.limit = $scope.categoryEditModel.limit;
        $scope.categoryEditModel.isLoadingProgress = true;
        var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
        adminService.getCatalogCategory(getListParam, configOption).then(function(response) {
            var data = response.data.data;
            if (data.total != 0)
            {
                $scope.categoryEditModel.list = data.categoryList[0];
            }
            $scope.categoryEditModel.total = response.data.total;

            $scope.updateCategoryInfo();

        });

    };
    $scope.getCategory();

}]);




