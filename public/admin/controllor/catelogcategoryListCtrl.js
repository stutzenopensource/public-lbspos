app.controller('catelogcategoryListCtrl', ['$scope', '$rootScope', 'adminService','$localStorage', '$filter', 'Auth', '$state', '$timeout', function($scope, $rootScope, adminService, localStorage,$filter, Auth, $state, $timeout) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.categoryModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
            status: '',
            isLoadingProgress: false
        };

        $scope.pagePerCount = [50, 100];
        $scope.categoryModel.limit = $scope.pagePerCount[0];

        $scope.searchFilter = {
            name: ''
        };

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }
        $scope.refreshScreen = function()
        {
            $scope.searchFilter = {
                name: ''
            };
            $scope.initTableFilter();
        }
        $scope.selectCategoryId = '';
        $scope.showdeletePopup = false;
        $scope.isdeleteCategoryLoadingProgress = false;
        $scope.showPopup = function(id)
        {
            $scope.selectCategoryId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteCategoryLoadingProgress = false;
        };
        
        $scope.deleteCategoryInfo = function( )
        {
            if ($scope.isdeleteCategoryLoadingProgress == false)
            {
                $scope.isdeleteCategoryLoadingProgress = true;
                var getListParam = {};
                getListParam.id = $scope.selectCategoryId;
                var headers = {};
                headers['screen-code'] = 'categoryCatlog';
                adminService.deleteCategory(getListParam, getListParam.id, headers).then(function(response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getList();
                    }
                    $scope.isdeleteCategoryLoadingProgress = false;
                });
            }
        };
        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'catelogcategory_form';


        $scope.$on('onLocalStorageFormRenterCompleteEvent', function(event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });  
        
          $scope.$on('onLocalStorageFieldRenterCompleteEvent', function(event, formName) {
             if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRenterCount++;
                }
                if ($scope.localStorageRenterCount >= $scope.localStorageCount)
                {
                    console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                    $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
                }
            
              }
        });
        $scope.$on('updateTypeaheadFieldValue', function(event, data) {

            if (data.fieldName == "quote_form-customer_dropdown")
            {
                $scope.searchFilter.customerInfo = data.value;
            }
        });
        
        

        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function(event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRetrieveCount++;
                }
                if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
                {
                    $scope.initTableFilter();
                }
                console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);
            }
        });

        $scope.init = function()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');

        };

        $scope.$on('onLocalStorageReadyEvent', function() {
           if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                $timeout(function() {
                    $scope.init();
                }, 300);
            
            }
        });            
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
          $scope.getList = function() {

            var getListParam = {};
            getListParam.id = '';
            var headers = {};
            headers['screen-code'] = 'categoryCatlog';
            getListParam.category_name = $scope.searchFilter.name;
            getListParam.start = ($scope.categoryModel.currentPage - 1) * $scope.categoryModel.limit;
            getListParam.limit = $scope.categoryModel.limit;
            $scope.categoryModel.isLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getCatalogCategory(getListParam, configOption, headers).then(function(response) {
                if (response.data.success === true)
                {
                    var data = response.data.data;
                    $scope.categoryModel.list = data.categoryList;
                    $scope.categoryModel.total = data.total;
                }
                $scope.categoryModel.isLoadingProgress = false;
            });

        };

        
        //$scope.getList();
    }]);








