app.controller('countryAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', 'ValidationFactory', '$state', '$localStorageService', '$timeout', 'APP_CONST', '$window', function ($scope, $rootScope, adminService, $httpService, $filter, Auth, ValidationFactory, $state, $localStorageService, $timeout, APP_CONST, $window) {

        $scope.countryModel = {
            id: '',
            name: '',
            isActive: true,
            code: '',
            isDefault: false
        }

        $scope.validationFactory = ValidationFactory;

        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function ()
        {
            if (typeof $scope.add_country_form != 'undefined' && typeof $scope.add_country_form.$pristine != 'undefined' && !$scope.add_country_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function ()
        {
            $scope.add_country_form.$setPristine();
            if (typeof $scope.add_country_form != 'undefined')
            {
                $scope.countryModel.name = "";
                $scope.countryModel.code = "";
                $scope.countryModel.isDefault = false;
                $scope.countryModel.isActive = true;
            }
        }
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
//        $scope.showAlert = true;
//        setTimeout(function () {
//            if ($rootScope.userModel.new_user == 1 && $rootScope.hideAlert == false)
//            {
//                $window.localStorage.setItem("demo_guide", "false");
//                swal({
//                    title: "Guide Tour!",
//                    text: "Kindly fill all the required details and click submit to proceed the demo..",
//                    confirmButtonText: "OK"
//                },
//                        function (isConfirm) {
//                            if (isConfirm) {
//                                $rootScope.hideAlert = true;
//                            }
//                        });
//            }
//        }, 2000);
        $scope.createcountry = function () {
            if ($scope.isDataSavingProcess)
            {
                return;
            }
            $scope.isDataSavingProcess = true;
            var addCountryParam = {};
            var headers = {};
            headers['screen-code'] = 'country';
            addCountryParam.id = 0;
            addCountryParam.name = $scope.countryModel.name;
            addCountryParam.code = $scope.countryModel.code;
            addCountryParam.is_default = $scope.countryModel.isDefault;
            addCountryParam.is_active = $scope.countryModel.isActive == true ? 1 : 0;
            var configOption = adminService.handleBothSuccessAndErrorResponseConfig;
            adminService.saveCountry(addCountryParam, configOption, headers).then(function (response)
            {
                if (response.data.success == true)
                {
                    $scope.showAlert = false;
                    $scope.formReset();
                    $state.go('app.country');
//                    if ($rootScope.userModel.new_user == 1)
//                    {
//                        $window.localStorage.setItem("demo_guide", "true");
//                        $rootScope.closeDemoPopup(2);
//                    }
                } else {
                    $scope.isDataSavingProcess = false;
                    var element = document.getElementById("btnLoad");
                    element.classList.remove("btn-loader");
                }
            }).catch(function (response) {
                console.error('Error occurred:', response.status, response.data);
                $scope.isDataSavingProcess = false;
                var element = document.getElementById("btnLoad");
                element.classList.remove("btn-loader");
            });
        };

    }]);



