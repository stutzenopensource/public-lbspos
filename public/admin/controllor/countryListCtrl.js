app.controller('countryListCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$timeout', '$window', function ($scope, $rootScope, adminService, $filter, Auth, $timeout, $window) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.countryModel = {

            currentPage: 1,
            total: 0,
            limit: 4,
            list: [],

            isLoadingProgress: true

        };
        $scope.searchFilterNameValue = ''

        $scope.pagePerCount = [50, 100];
        $scope.countryModel.limit = $scope.pagePerCount[0];

        $scope.searchFilter = {
            name: ''

        };

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }


        $scope.showdeletePopup = false;
        $scope.isdeleteProgress = false;

        $scope.showPopup = function (id)
        {
            $scope.selectCountryId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function ()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteProgress = false;
        };

        $scope.deleteCountryInfo = function ()
        {
            if ($scope.isdeleteProgress == false)
            {
                $scope.isdeleteProgress = true;

                var getListParam = {};
                getListParam.id = $scope.selectCountryId;
                var headers = {};
                headers['screen-code'] = 'country';
                adminService.deleteCountry(getListParam, getListParam.id, headers).then(function (response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getList();
                    }
                    $scope.isdeleteProgress = false;
                });
            }
        };

        $scope.refreshScreen = function ()
        {
            $scope.searchFilter = {
                name: ''
            };
            $scope.initTableFilter();
        }

        $scope.getList = function () {

            $scope.countryModel.isLoadingProgress = true;
            var countryListParam = {};
            var headers = {};
            headers['screen-code'] = 'country';
            countryListParam.name = $scope.searchFilter.name;

            countryListParam.id = '';

            countryListParam.is_active = 1;
            countryListParam.start = ($scope.countryModel.currentPage - 1) * $scope.countryModel.limit;
            countryListParam.limit = $scope.countryModel.limit;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getCountryList(countryListParam, configOption, headers).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.countryModel.list = data.list;
                    $scope.countryModel.total = data.total;
                }
                $scope.countryModel.isLoadingProgress = false;
            });

        };
        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'country_form';


        $scope.$on('onLocalStorageFormRenterCompleteEvent', function (event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });

        $scope.$on('onLocalStorageFieldRenterCompleteEvent', function (event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRenterCount++;
                }
                if ($scope.localStorageRenterCount >= $scope.localStorageCount)
                {
                    console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                    $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
                }

            }
        });
        $scope.$on('updateTypeaheadFieldValue', function (event, data) {

            if (data.fieldName == "quote_form-customer_dropdown")
            {
                $scope.searchFilter.customerInfo = data.value;
            }
        });



        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function (event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRetrieveCount++;
                }
                if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
                {
                    $scope.initTableFilter();
                    if ($rootScope.userModel.new_user == 1)
                    {
                        $window.localStorage.setItem("demo_guide", "true");
                        $rootScope.closeDemoPopup(2);
                    }
                }
                console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);
            }
        });

        $scope.init = function ()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');

        };

        $scope.$on('onLocalStorageReadyEvent', function () {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                $timeout(function () {
                    $scope.init();
                }, 300);

            }
        });
        //$scope.getList();

    }]);




