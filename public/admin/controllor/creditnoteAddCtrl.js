
app.controller('creditnoteAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', 'sweet', function ($scope, $rootScope, adminService, $httpService, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST, sweet) {

        $scope.creditModel =
                {
                    "id": 0,
                    "account": '',
                    "date": '',
                    "list": [],
                    "comments": "",
                    "amount": '',
                    "account_id": '',
                    "payment_mode": '',
                    "paymentlist": [],
                    "purchaseorder_id": '',
                    "customerInfo": {},
                    "customer_id": '',
                    "categoryList": [],
                    "accountsList": [],
                    "category": '',
                    "invoice_id": '',
                    "PurchaseInvoiceList": [],
                    "invoiceDetails": {},
                    "prefixList": [],
                    "invoicePrefix": ''
                }


        $scope.adminService = adminService;
        $scope.validationFactory = ValidationFactory;
        $scope.getNavigationBlockMsg = function (pageReload)
        {
            if (typeof $scope.save_credit_form != 'undefined' && typeof $scope.save_credit_form.$pristine != 'undefined' && !$scope.save_credit_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.isDataSavingProcess = false;
        $scope.isPurchaseOrderLoading = false;
        $scope.currentDate = new Date();
        $scope.creditModel.date = $scope.currentDate;
        $scope.formReset = function ( )
        {
            $scope.creditModel.invoice_id = '';
            $scope.creditModel.customerInfo = '';
            $scope.creditModel.account = "";
            $scope.creditModel.cheque_no = "";
            $scope.creditModel.invoicedate = "";
            $scope.creditModel.payment_mode = '';
            $scope.creditModel.paymentdefault = '';
            $scope.creditModel.amount = "";
            $scope.creditModel.date = "";
            $scope.creditModel.category = '';
            $scope.creditModel.account_id = '';
            $scope.creditModel.invoiceDetails = {};
            $scope.creditModel.PurchaseInvoiceList = [];
            $scope.creditModel.comments = "";
            $scope.creditModel.invoicePrefix = "";

        }


        $scope.paymentDateOpen = false;
        $scope.openDate = function (index)
        {
            if (index == 0)
            {
                $scope.paymentDateOpen = true;
            }

        }

        $scope.getCustomerList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = [];
                if (data.length > 0)
                {
                    for (var i = 0; i < data.length; i++)
                    {
                        if (data[i].is_purchase == '1' || data[i].is_purchase == 1)
                        {
                            hits.push(data[i]);
                        }
                    }
                }
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatCustomerModel = function (model)
        {
            if (model != null)
            {
                var phoneNumber = '';
                if (model.phone != undefined)
                {
                    if (model.phone.indexOf(','))
                    {
                        phoneNumber = model.phone.split(',')[0];
                    }
                }
                if (model.fname != undefined && phoneNumber != '' && model.email != undefined)
                {

                    return model.fname + '(' + phoneNumber + ',' + model.email + ')';
                } else if (model.fname != undefined && phoneNumber != '')
                {

                    return model.fname + '(' + phoneNumber + ')';
                } else if (model.fname != undefined)
                {
                    return model.fname;
                }
            }
            return  '';
        };
        //            $scope.getPurchaseInvoiceList = function ()
        //            {
        //                  var getListParam = {};
        //                  if ($scope.creditModel.customerInfo != null && typeof $scope.creditModel.customerInfo != 'undefined' && typeof $scope.creditModel.customerInfo.id != 'undefined')
        //                  {
        //                        $scope.isPurchaseOrderLoading = true;
        //                        getListParam.customer_id = $scope.creditModel.customerInfo.id;
        //                        var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
        //                        adminService.getPurchaseInvoiceList(getListParam, configOption).then(function (response)
        //                        {
        //                              if (response.data.success === true)
        //                              {
        //                                    var data = response.data.list;
        //                                    $scope.creditModel.PurchaseInvoiceList = data;
        //                                    if ($scope.creditModel.PurchaseInvoiceList.length > 0)
        //                                    {
        //                                          for (var i = 0; i < $scope.creditModel.PurchaseInvoiceList.length; i++)
        //                                          {
        //                                                $scope.creditModel.PurchaseInvoiceList[i].newdate = utilityService.parseStrToDate($scope.creditModel.PurchaseInvoiceList[i].date);
        //                                                $scope.creditModel.PurchaseInvoiceList[i].date = utilityService.parseDateToStr($scope.creditModel.PurchaseInvoiceList[i].newdate, $rootScope.appConfig.date_format);
        //                                                $scope.creditModel.PurchaseInvoiceList[i].balance_amount = parseFloat($scope.creditModel.PurchaseInvoiceList[i].total_amount) - parseFloat($scope.creditModel.PurchaseInvoiceList[i].paid_amount);
        //                                                $scope.creditModel.PurchaseInvoiceList[i].balance_amount = parseFloat($scope.creditModel.PurchaseInvoiceList[i].balance_amount).toFixed(2);
        //                                          }
        //                                          $scope.creditModel.invoice_id = $scope.creditModel.PurchaseInvoiceList[0].id;
        //                                          $scope.getInvoiceDetails(0);
        //                                    } else
        //                                    {
        //                                          $scope.creditModel.invoiceDetails = '';
        //                                          $scope.creditModel.invoice_id = '';
        //                                    }
        //                                    $scope.isPurchaseOrderLoading = false;
        //                              }
        //                        });
        //                  }
        //            };

        //            $scope.getInvoiceDetails = function (index)
        //            {
        //                  $scope.creditModel.invoiceDetails = $scope.creditModel.PurchaseInvoiceList[index];
        //
        //            };

        $scope.getIncomeCategoryList = function () {

            var getListParam = {};
            getListParam.id = '';
            getListParam.is_active = 1;
            getListParam.type = 'expense';
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;

            adminService.getIncomeCategoryList(getListParam, configOption).then(function (response) {
                var data = response.data.list;
                $scope.creditModel.categoryList = data;
            });

        };
        $scope.getAccountlist = function () {

            var getListParam = {};
            getListParam.id = '';
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;

            adminService.getAccountlist(getListParam, configOption).then(function (response) {
                var data = response.data.list;
                $scope.creditModel.accountsList = data;
            });

        };

        $scope.createcredit = function ()
        {
            var createAdvanceParam = {};
            var headers = {};
            headers['screen-code'] = 'creditnote';
            if ($scope.creditModel.customerInfo != null && typeof $scope.creditModel.customerInfo != 'undefined' && typeof $scope.creditModel.customerInfo.id != 'undefined')
            {
                createAdvanceParam.customer_id = $scope.creditModel.customerInfo.id;
            }
            createAdvanceParam.comments = $scope.creditModel.comments;
            if (typeof $scope.creditModel.date == 'object')
            {
                $scope.creditModel.date = utilityService.parseDateToStr($scope.creditModel.date, $scope.adminService.appConfig.date_format);
            }
            createAdvanceParam.date = utilityService.changeDateToSqlFormat($scope.creditModel.date, $scope.adminService.appConfig.date_format);
            createAdvanceParam.amount = $scope.creditModel.amount;
            createAdvanceParam.used_amount = $scope.creditModel.amount;
            createAdvanceParam.balance_amount = 0;

            createAdvanceParam.voucher_no = '';

            createAdvanceParam.prefix = $scope.creditModel.invoicePrefix;
            createAdvanceParam.reference_no = '';
            createAdvanceParam.payment_mode = '';
            createAdvanceParam.adv_payment_id = '';
            createAdvanceParam.tra_category = '';
            for (var i = 0; i < $scope.creditModel.accountsList.length; i++)
            {
                if ($scope.creditModel.account == $scope.creditModel.accountsList[i].id)
                {
                    createAdvanceParam.account_id = $scope.creditModel.accountsList[i].id;
                    createAdvanceParam.account = $scope.creditModel.accountsList[i].account;
                }
            }
            createAdvanceParam.is_active = 1;
            createAdvanceParam.voucher_type = "creditNote";
            $scope.isDataSavingProcess = true;
            var configOption = adminService.handleBothSuccessAndErrorResponseConfig;
            adminService.addAdvancepayment(createAdvanceParam, configOption, headers).then(function (response)
            {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.creditnote');
                }
                $scope.isDataSavingProcess = false;
            }).catch(function (response) {
                console.error('Error occurred:', response.status, response.data);
                $scope.isDataSavingProcess = false;
                var element = document.getElementById("btnLoad");
                element.classList.remove("btn-loader");
            });

        };
        $scope.getpaymenttermslist = function () {

            var getListParam = {};
            getListParam.id = '';
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;

            adminService.getpaymenttermslist(getListParam, configOption).then(function (response) {
                var data = response.data.list;
                $scope.creditModel.paymentlist = data;
            });
        }
        $scope.getPurchasePrefixList = function ()
        {
            var getListParam = {};
            getListParam.setting = 'purchase_invoice_prefix';
            $scope.creditModel.prefixList = [];
            //$scope.isPrefixListLoaded = false;
            var headers = {};
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getAppSettingsList(getListParam, configOption, headers).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    if (data.list.length > 0)
                    {
                        var perfix = data.list[0].value;
                        perfix = perfix.split('\n');
                        for (var i = 0; i < perfix.length; i++)
                        {
                            $scope.creditModel.prefixList.push(perfix[i]);
                        }
                    }
                    // $scope.getPurchasePrefixList();
                }
                $scope.isPrefixListLoaded = true;
            });

        };
        $scope.getPurchasePrefixList();
        $scope.getpaymenttermslist();
        $scope.getIncomeCategoryList();
        $scope.getAccountlist();
    }]);




