




app.controller('creditnoteViewCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', 'APP_CONST', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', function ($scope, $rootScope, adminService, $httpService, utilityService, APP_CONST, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory) {

        $scope.creditModel = {
            "creditDetails": {},
            'list': []
        };

        $scope.validationFactory = ValidationFactory;

        $scope.getNavigationBlockMsg = function (pageReload)
        {
            if (typeof $scope.advancepayment_view !== 'undefined' && typeof $scope.advancepayment_view.$pristine !== 'undefined' && !$scope.advancepayment_view.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        };
        $scope.isDataSavingProcess = false;
        $scope.formReset = function () {
            $scope.transferlist_edit_form.$setPristine();
            $scope.updateTransferlistInfo();

        };

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.startDateOpen = false;

        $scope.openDate = function (index) {

            if (index === 0)
            {
                $scope.startDateOpen = true;
            }

        };

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.updatecreditInfo = function ()
        {
            $scope.dateFormat = $rootScope.appConfig.date_format;
            if ($scope.creditModel.list !== null)
            {
            $scope.creditModel.creditDetails = $scope.creditModel.list;
            $scope.creditModel.creditDetails.newdate = utilityService.parseStrToDate($scope.creditModel.creditDetails.date);
            $scope.creditModel.creditDetails.date = utilityService.parseDateToStr($scope.creditModel.creditDetails.newdate, $rootScope.appConfig.date_format);
            $scope.creditModel.creditDetails.total_amount = parseFloat($scope.creditModel.creditDetails.amount).toFixed(2);
            $scope.creditModel.creditDetails.amount = utilityService.changeCurrency($scope.creditModel.creditDetails.total_amount, $rootScope.appConfig.thousand_seperator);
            $scope.creditModel.isLoadingProgress = false;
            }
        };




        $scope.getcreditInfo = function () {

            if (typeof $stateParams.id !== 'undefined')
            {
                var getListParam = {};
                getListParam.id = $stateParams.id;
                var headers = {};
                headers['screen-code'] = 'creditnote';
                $scope.creditModel.isLoadingProgress = true;
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getAdvancepaymentList(getListParam, configOption,headers).then(function (response) {
                    var data = response.data;
                    if (data.total !== 0)
                    {
                        $scope.creditModel.list = data.list[0];
                        $scope.updatecreditInfo();
                    }

                });
            }
        };



        $scope.getcreditInfo();


    }]);




