app.controller('daylogReportCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', function ($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.dayLogModel = {
            currentPage: 1,
            total: 0,
            total_Amount: 0,
            limit: 10,
            list: [],
            length: [],
            serverList: null,
            isLoadingProgress: false,
            isSearchLoadingProgress: false,
            total_qty: 0
        };
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.dayLogModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat);
        $scope.validationFactory = ValidationFactory;
        $scope.checked = false;
        $scope.showCategory = false;
        $scope.openDate = function (index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            } else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };

        $scope.searchFilter = {
            
            fromdate: '',
            todate: '',
            daycode: ''
        };
        $scope.clearFilters = function ()
        {
            $scope.searchFilter = {
                
                fromdate: '',
                todate: '',
                daycode: ''
            };
            $scope.checked = false;
            $scope.showCategory = false;
            $scope.dayLogModel.list = [];
        }

        $scope.print = function ()
        {
            $window.print();
        };

        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.show = function (index)
        {

            if ($scope.showDetails[index])
            {
                $scope.showDetails[index] = false;
            } else
                $scope.showDetails[index] = true;
        };

        $scope.getList = function () {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'empsalesreport';
            getListParam.from_date = '';
            getListParam.to_date = '';
            if ($scope.searchFilter.fromdate != null && $scope.searchFilter.fromdate != '')
            {
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.adminService.appConfig.date_format);
                }
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.adminService.appConfig.date_format);
            }
            if ($scope.searchFilter.todate != null && $scope.searchFilter.todate != '')
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.adminService.appConfig.date_format);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.adminService.appConfig.date_format);
            }
            getListParam.day_code = $scope.searchFilter.daycode;
            $scope.dayLogModel.isLoadingProgress = true;
            $scope.showDetails = [];
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getDayLogReport(getListParam, configOption, headers).then(function (response)
            {
                var total_card = 0.00;
                var total_cash = 0.00;
                var total_credit = 0.00;
                var total_advance = 0.00;
                var total_others = 0.00; 
                var total_amount = 0.00; 
                $scope.dayLogModel.total_purchase_price = 0.00;
                $scope.dayLogModel.total_selling_price = 0.00;
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.dayLogModel.list = data;
                    for (var i = 0; i < $scope.dayLogModel.list.length; i++)
                    {
                        $scope.dayLogModel.list[i].start_on = new Date($scope.dayLogModel.list[i].start_on);
                        if($scope.dayLogModel.list[i].end_on == "0000-00-00 00:00:00" || $scope.dayLogModel.list[i].end_on == null)
                        {
                            $scope.dayLogModel.list[i].end_on = null
                        }
                        else
                        {
                            $scope.dayLogModel.list[i].end_on = new Date($scope.dayLogModel.list[i].end_on);
                        }
//                        if($scope.dayLogModel.list[i].end_on == null)
//                        {
//                            $scope.dayLogModel.list[i].end_on  = "-";
//                        }
//                        if($scope.dayLogModel.list[i].start_on == null)
//                        {
//                            $scope.dayLogModel.list[i].start_on  = " ";
//                        }
                        total_cash = parseFloat(total_cash) + parseFloat($scope.dayLogModel.list[i].cash == null ? 0 : $scope.dayLogModel.list[i].cash);
                        total_card = parseFloat(total_card) + parseFloat($scope.dayLogModel.list[i].card == null ? 0 : $scope.dayLogModel.list[i].card);
                        total_credit = parseFloat(total_credit) + parseFloat($scope.dayLogModel.list[i].credit == null ? 0 : $scope.dayLogModel.list[i].credit);
                        total_advance = parseFloat(total_advance) + parseFloat($scope.dayLogModel.list[i].others == null ? 0 : $scope.dayLogModel.list[i].others);
                        total_others = parseFloat(total_others) + parseFloat($scope.dayLogModel.list[i].tot_given_qty == null ? 0 : $scope.dayLogModel.list[i].tot_given_qty);
                        total_amount = parseFloat(total_amount) + parseFloat($scope.dayLogModel.list[i].total_amount == null ? 0 : $scope.dayLogModel.list[i].tot_given_qty);
                        $scope.dayLogModel.list[i].total_amount = parseFloat($scope.dayLogModel.list[i].total_amount).toFixed(2);
                        $scope.dayLogModel.list[i].total_amount = utilityService.changeCurrency($scope.dayLogModel.list[i].total_amount, $rootScope.appConfig.thousand_seperator);
                        $scope.dayLogModel.list[i].cash = parseFloat($scope.dayLogModel.list[i].cash).toFixed(2);
                        $scope.dayLogModel.list[i].cash = utilityService.changeCurrency($scope.dayLogModel.list[i].cash, $rootScope.appConfig.thousand_seperator);
                        $scope.dayLogModel.list[i].card = parseFloat($scope.dayLogModel.list[i].card).toFixed(2);
                        $scope.dayLogModel.list[i].card = utilityService.changeCurrency($scope.dayLogModel.list[i].card, $rootScope.appConfig.thousand_seperator);
                        $scope.dayLogModel.list[i].credit = parseFloat($scope.dayLogModel.list[i].credit).toFixed(2);
                        $scope.dayLogModel.list[i].credit = utilityService.changeCurrency($scope.dayLogModel.list[i].credit, $rootScope.appConfig.thousand_seperator);
                        $scope.dayLogModel.list[i].advance = parseFloat($scope.dayLogModel.list[i].advance).toFixed(2);
                        $scope.dayLogModel.list[i].advance = utilityService.changeCurrency($scope.dayLogModel.list[i].advance, $rootScope.appConfig.thousand_seperator);
                        $scope.dayLogModel.list[i].others = parseFloat($scope.dayLogModel.list[i].others).toFixed(2);
                        $scope.dayLogModel.list[i].others = utilityService.changeCurrency($scope.dayLogModel.list[i].others, $rootScope.appConfig.thousand_seperator);
                        $scope.dayLogModel.list[i].flag = i;
                        $scope.showDetails[i] = false;
                         if($scope.dayLogModel.list[i].data.length >0)
                         {
                            for (var j = 0; j<$scope.dayLogModel.list[i].data.length; j++)
                            {
                                $scope.dayLogModel.list[i].data[j].date = new Date($scope.dayLogModel.list[i].data[j].date);
                                $scope.dayLogModel.list[i].data[j].total_amount = parseFloat($scope.dayLogModel.list[i].data[j].total_amount).toFixed(2);
                                $scope.dayLogModel.list[i].data[j].total_amount = utilityService.changeCurrency($scope.dayLogModel.list[i].data[j].total_amount, $rootScope.appConfig.thousand_seperator);
                            }
                         }
                    }
                    $scope.dayLogModel.total_card = total_card;
                    $scope.dayLogModel.total_card = parseFloat($scope.dayLogModel.total_card).toFixed(2);
                    $scope.dayLogModel.total_card = utilityService.changeCurrency($scope.dayLogModel.total_card, $rootScope.appConfig.thousand_seperator);
                    $scope.dayLogModel.total_cash = total_cash;
                    $scope.dayLogModel.total_cash = parseFloat($scope.dayLogModel.total_cash).toFixed(2);
                    $scope.dayLogModel.total_cash = utilityService.changeCurrency($scope.dayLogModel.total_cash, $rootScope.appConfig.thousand_seperator);
                    $scope.dayLogModel.total_credit = total_credit;
                    $scope.dayLogModel.total_credit = parseFloat($scope.dayLogModel.total_credit).toFixed(2);
                    $scope.dayLogModel.total_credit = utilityService.changeCurrency($scope.dayLogModel.total_credit, $rootScope.appConfig.thousand_seperator);
                    $scope.dayLogModel.total_advance = total_advance;
                    $scope.dayLogModel.total_advance = parseFloat($scope.dayLogModel.total_advance).toFixed(2);
                    $scope.dayLogModel.total_advance = utilityService.changeCurrency($scope.dayLogModel.total_advance, $rootScope.appConfig.thousand_seperator);
                    $scope.dayLogModel.total_amount = total_amount;
                    $scope.dayLogModel.total_amount = parseFloat($scope.dayLogModel.total_amount).toFixed(2);
                    $scope.dayLogModel.total_amount = utilityService.changeCurrency($scope.dayLogModel.total_amount, $rootScope.appConfig.thousand_seperator);
                    $scope.dayLogModel.total_others = total_others;
                    $scope.dayLogModel.total_others = parseFloat($scope.dayLogModel.total_others).toFixed(2);
                    $scope.dayLogModel.total_others = utilityService.changeCurrency($scope.dayLogModel.total_others, $rootScope.appConfig.thousand_seperator);
                   
                }
                $scope.dayLogModel.isLoadingProgress = false;
            });
        };
        $scope.getProductList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.PRODUCT_BASED_LIST, autosearchParam, false).then(function (responseData) {
            
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatProductModel = function (model)
        {
            if (model != null && model != undefined && model != '')
            {
                return model.name;
            }
            return  '';
        };

        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'delivery_note_report_form';
        $scope.$on('onLocalStorageFormRenterCompleteEvent', function (event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });

        $scope.$on('onLocalStorageFieldRenterCompleteEvent', function (event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRenterCount++;
                }
                if ($scope.localStorageRenterCount >= $scope.localStorageCount)
                {
                    console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                    $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
                }
            }
        });

        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function (event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRetrieveCount++;
                }
                if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
                {
                    $scope.initTableFilter();
                }
                console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);
            }
        });
        $scope.$on('updateTypeaheadFieldValue', function (event, data) {

            if (data.fieldName == "delivery_note_report_form-product_dropdown")
            {
                $scope.searchFilter.productInfo = data.value;
            }
        });

        $scope.init = function ()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');
        };

        $scope.$on('onLocalStorageReadyEvent', function () {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                $timeout(function () {
                    $scope.init();
                }, 300);
            }
        });
        //$scope.getList();
    }]);




