

app.controller('dealAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService','utilityService', '$filter', 'Auth', '$state', '$timeout', 'ValidationFactory', 'APP_CONST', function($scope, $rootScope, adminService, $httpService, utilityService, $filter, Auth, $state, $timeout, ValidationFactory, APP_CONST) {

        $scope.dealAddModel = {
            id: '',
            name: "",
            stage: '',
            isActive: true,
            stageList: [],
            customer: {},
            userList: [],
            user: '',
            type: '',
            closedate: '',
            amount: ''
        };

        $scope.validationFactory = ValidationFactory;


        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.deal_form != 'undefined' && typeof $scope.deal_form.$pristine != 'undefined' && !$scope.deal_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function() {

            //$scope.deal_form.$setPristine();
            $scope.dealAddModel = {
                id: '',
                name: "",
                stage: '',
                isActive: true,
                customer: {},
                user: '',
                amount: '',
                closedate: '',
                nextdate: '',
                type: ''
            };
            $scope.getStageList();
            $scope.getUserList();

        }
        $scope.currentDate = new Date();
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.closeDateOpen = false;
        $scope.nextDateOpen = false;
        $scope.openDate = function(index)
        {
            if (index == 0)
            {
                $scope.closeDateOpen = true;
            }
             if (index == 1)
            {
                $scope.nextDateOpen = true;
            }
        }
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.showCustomerAddPopup = false;
        $scope.showCustomerPopup = function()
        {
            $scope.showCustomerAddPopup = true;
//            if (typeof $scope.dealAddModel.customer != undefined && $scope.dealAddModel.customer != '' && $scope.dealAddModel.customer != null)
//            {
                $rootScope.$broadcast('customerInfo', $scope.dealAddModel.customer);
            //}
        }
        $scope.closeCustomerPopup = function()
        {
            $scope.showCustomerAddPopup = false;
        }

        $scope.$on("updateSavedCustomerDetail", function(event, customerDetail)
        {
            if (typeof customerDetail === 'undefined')
                return;
            $scope.dealAddModel.customer = customerDetail;
        });


        $scope.getCustomerList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            if ($scope.dealAddModel.type == 'existing')
            {
                return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function(responseData)
                {
                    var data = responseData.data.list;
                    var hits = data;
                    if (hits.length > 10)
                    {
                        hits.splice(10, hits.length);
                    }
                    return hits;
                });
            }
        };

        $scope.formatCustomerModel = function(model)
        {
            if (model != null)
            {
                if (model.fname != undefined)
                {
                    return model.fname + '(' + model.phone + ',' + model.email + ')';
                }
                else if (model.fname != undefined && model.phone != undefined)
                {
                    return model.fname + '(' + model.phone + ')';
                }
                else
                {
                    return model.fname;
                }
            }
            return  '';
        };

        $scope.getStageList = function()
        {
            var createParam = {};
            createParam.is_active = 1;

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getStageList(createParam, configOption).then(function(response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.dealAddModel.stageList = data.list;
                }
            });
        };



        $scope.getUserList = function()
        {
            var createParam = {};
            createParam.isactive = 1;

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getUserList(createParam, configOption).then(function(response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.dealAddModel.userList = data.list;
                }
            });
        };

        $scope.createDeal = function() {
            if ($scope.isDataSavingProcess == false)
            {
                $scope.isDataSavingProcess = true;
                var createParam = {};
                var headers = {};
                headers['screen-code'] = 'deal';
                createParam.name = $scope.dealAddModel.name;
                createParam.stage_id = $scope.dealAddModel.stage;
                for (var i = 0; i < $scope.dealAddModel.stageList.length; i++)
                {
                    if ($scope.dealAddModel.stageList[i].id == $scope.dealAddModel.stage)
                    {
                        createParam.stage_name = $scope.dealAddModel.stageList[i].name;
                    }
                }
                createParam.type = $scope.dealAddModel.type;
                createParam.amount = $scope.dealAddModel.amount;
                createParam.customer_id = $scope.dealAddModel.customer.id;
                createParam.customer_name = $scope.dealAddModel.customer.fname;
                createParam.is_active = 1;
                if ($rootScope.userModel.rolename != 'superadmin')
                {
                    createParam.user_id = $rootScope.userModel.id;
                    createParam.user_name = $rootScope.userModel.f_name;

                }
                else
                {
                    createParam.user_id = $scope.dealAddModel.user;
                    for (var i = 0; i < $scope.dealAddModel.userList.length; i++)
                    {
                        if ($scope.dealAddModel.userList[i].id == $scope.dealAddModel.user)
                        {
                            createParam.user_name = $scope.dealAddModel.userList[i].f_name;
                        }
                    }
                }
                if ($scope.dealAddModel.closedate != '')
                {
                    if ($scope.dealAddModel.closedate != null && typeof $scope.dealAddModel.closedate == 'object')
                    {
                        createParam.colsed_date = utilityService.parseDateToStr($scope.dealAddModel.closedate, 'yyyy-MM-dd');
                    }
                    createParam.colsed_date = utilityService.changeDateToSqlFormat(createParam.colsed_date, 'yyyy-MM-dd');
                }
                if ($scope.dealAddModel.nextdate != '')
                {
                    if ($scope.dealAddModel.nextdate != null && typeof $scope.dealAddModel.nextdate == 'object')
                    {
                        createParam.next_follow_up = utilityService.parseDateToStr($scope.dealAddModel.nextdate, 'yyyy-MM-dd');
                    }
                    createParam.next_follow_up = utilityService.changeDateToSqlFormat(createParam.next_follow_up, 'yyyy-MM-dd');
                }
                adminService.addDeal(createParam, headers).then(function(response) {
                    if (response.data.success == true)
                    {
                        $scope.formReset();
                        $state.go('app.deal');
                    }
                    $scope.isDataSavingProcess = false;
                });
            }

        };
        $scope.getStageList();
        $scope.getUserList();
    }]);




