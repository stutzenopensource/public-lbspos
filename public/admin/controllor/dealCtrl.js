app.controller('dealCtrl', ['$scope', '$rootScope', '$state', '$stateParams', 'adminService', 'utilityService', '$filter', 'Auth', '$timeout', '$httpService', 'APP_CONST', 'ValidationFactory', function($scope, $rootScope, $state, $stateParams, adminService, utilityService, $filter, Auth, $timeout, $httpService, APP_CONST, ValidationFactory) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.dealModel = {
            currentPage: 1,
            total: 0,
            limit: 50,
            list: [],
            stage: '',
            filterCustomerId: '',
            isLoadingProgress: true,
            stageList: []
        };
        $scope.validationFactory = ValidationFactory;
        $scope.pagePerCount = [50, 100];
        $scope.dealModel.limit = $scope.pagePerCount[0];
        $scope.currencyFormat = $rootScope.appConfig.currency;
        $scope.adminService = adminService;
        $scope.searchFilter =
                {
                    name: '',
                    customerInfo: {},
                    stage: ''
                };
        $scope.refreshScreen = function()
        {
            $scope.searchFilter = {
                name: '',
                customerInfo: {},
                stage: ''
            };
            $scope.initTableFilter();
        }

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getdealListInfo, 300);
        }
        $scope.customAttributeList = [];
        $scope.isdeleteProgress = false;
        $scope.selectDealId = '';
        $scope.showdeletePopup = false;
        $scope.showPopup = function(id)
        {
            $scope.selectDealId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteProgress = false;
        };

        $scope.deleteDealItem = function( )
        {
            if ($scope.isdeleteProgress == false)
            {
                $scope.isdeleteProgress = true;

                var getListParam = {};
                getListParam.id = $scope.selectDealId;
                var headers = {};
                headers['screen-code'] = 'deal';
                adminService.deleteDeal(getListParam, getListParam.id, headers).then(function(response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getdealListInfo();
                    }

                    $scope.isdeleteProgress = false;
                });

            }

        };

        $scope.$watch('searchFilter.customerInfo', function(newVal, oldVal)
        {
            if (typeof newVal === 'undefined' || newVal == null || newVal === '')
            {
//            if ($scope.searchFilter.customerInfo != null && typeof $scope.searchFilter.customerInfo != 'undefined' && typeof $scope.searchFilter.customerInfo.id != 'undefined')
                {
                    $scope.initTableFilter();
                }
            }
        });

        $scope.getCustomerList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
//            if (autosearchParam.search != '')
//            {
                return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function(responseData)
                {
                    var data = responseData.data.list;
                    var hits = data;
                    if (hits.length > 10)
                    {
                        hits.splice(10, hits.length);
                    }
                    return hits;
                });
//            }
        };

        $scope.formatCustomerModel = function(model)
        {
            if (model != null)
            {
                if (model.fname != undefined)
                {
                    return model.fname + '(' + model.phone + ',' + model.email + ')';
                }
                else if (model.fname != undefined && model.phone != undefined)
                {
                    return model.fname + '(' + model.phone + ')';
                }
                else
                {
                    return model.fname;
                }
            }
            return  '';
        };

        $scope.getStageList = function()
        {
            $scope.dealModel.isLoadingProgress = true;
            var getListParam = {};
            getListParam.is_active = 1;

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getStageList(getListParam, configOption).then(function(response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.dealModel.stageList = data.list;
                }
            });
        };


        $scope.getdealListInfo = function()
        {
            $scope.dateFormat = $rootScope.appConfig.date_format;
            $scope.dealModel.isLoadingProgress = true;
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'deal';
            getListParam.name = $scope.searchFilter.name;
            if ($scope.searchFilter.customerInfo != null && typeof $scope.searchFilter.customerInfo != 'undefined' && typeof $scope.searchFilter.customerInfo.id != 'undefined')
            {
                getListParam.customer_id = $scope.searchFilter.customerInfo.id;
            }
            else
            {
                getListParam.customer_id = '';
            }
            getListParam.start = ($scope.dealModel.currentPage - 1) * $scope.dealModel.limit;
            getListParam.limit = $scope.dealModel.limit;
            getListParam.is_active = 1;
            getListParam.stage_id = $scope.searchFilter.stage;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getDealList(getListParam, configOption, headers).then(function(response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.dealModel.list = data.list;
                    if ($scope.dealModel.list.length > 0)
                    {
                        for (var i = 0; i < $scope.dealModel.list.length; i++)
                        {
                            $scope.dealModel.list[i].amount = utilityService.changeCurrency($scope.dealModel.list[i].amount, $rootScope.appConfig.thousand_seperator);
                        }
                    }
                    $scope.dealModel.total = data.total;
                }
                $scope.dealModel.isLoadingProgress = false;
            });
        };
        $scope.getdealListInfo();
        $scope.getStageList();
    }]);




