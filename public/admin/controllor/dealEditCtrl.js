

app.controller('dealEditCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', function ($scope, $rootScope, adminService, $httpService, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST) {

        $scope.dealAddModel = {
            id: '',
            name: "",
            stage: '',
            isActive: true,
            stageList: [],
            customer: {},
            userList: [],
            user: '',
            type: '',
            closedate: '',
            nextdate: '',
            amount: '',
            activityList: [],
            notes: '',
            estimateList: [],
            attach: [],
            estimatecurrentPage: 1,
            estimatetotal: 0,
            estimatelimit: 4,
            activity_date: '',
            activity_time: '',
            log_type: '',
            log_subtype: ''
        };

        $scope.validationFactory = ValidationFactory;
        $scope.pagePerCount = [50, 100];
        $scope.dealAddModel.estimatelimit = $scope.pagePerCount[0];

        $scope.tabChange = function (value) {

            $scope.currentStep = value;
        }
        $scope.getNavigationBlockMsg = function (pageReload)
        {
            if (typeof $scope.deal_form != 'undefined' && typeof $scope.deal_form.$pristine != 'undefined' && !$scope.deal_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function (index) {
            if (index == 0)
            {
                $scope.deal_form.$setPristine();
                $scope.updateDealInfo();
            }
            if (index == 1)
            {
                $scope.deal_edit_form1.$setPristine();
                $scope.dealAddModel.notes = "";
                $scope.dealAddModel.attach = [];
            }
            if (index == 3)
            {

                $scope.deal_edit_form3.$setPristine();
                $scope.dealAddModel.log_subtype = "";
                $scope.dealAddModel.activity_time = new Date();
                $scope.dealAddModel.activity_date = new Date();
                $scope.dealAddModel.log_type = "";
            }

        }
        $scope.currentDate = new Date();
        $scope.dealAddModel.activity_time = new Date();
        $scope.dealAddModel.activity_date = new Date();
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.closeDateOpen = false;
        $scope.nextDateOpen = false;
        $scope.activity_DateOpen = false;
        $scope.openDate = function (index)
        {
            if (index == 0)
            {
                $scope.closeDateOpen = true;
            }
            if (index == 1)
            {
                $scope.activity_DateOpen = true;
            }
            if (index == 2)
            {
                $scope.nextDateOpen = true;
            }
        }
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.showCustomerAddPopup = false;
        $scope.showCustomerPopup = function ()
        {
            $scope.showCustomerAddPopup = true;
            if (typeof $scope.dealAddModel.customer != undefined && $scope.dealAddModel.customer != '' && $scope.dealAddModel.customer != null)
            {

                $rootScope.$broadcast('customerInfo', $scope.dealAddModel.customer);
            }
        }
        $scope.closeCustomerPopup = function ()
        {
            $scope.showCustomerAddPopup = false;
        }
        $scope.showUploadFilePopup = false;
        $scope.showPopup = function (value) {

            if (value == 'fileupload')
            {
                $scope.showUploadFilePopup = true;
            }

        }
        $scope.closePopup = function (value) {

            if (value == 'fileupload')
            {
                $scope.showUploadFilePopup = false;

            }
        }
        $scope.uploadedFileQueue = [];
        $scope.uploadedFileCount = 0;
        $scope.isImageSavingProcess = false;
        $scope.$on("updateFileUploadEvent", updateFileUploadEventHandler);
        function updateFileUploadEventHandler(event, data)
        {
            $scope.uploadedFileQueue = data;
            console.log('$scope.uploadedFileQueue');
            console.log($scope.uploadedFileQueue);
        }
        $scope.saveImagePath = function ()
        {
            if ($scope.uploadedFileQueue.length == 0)
            {
                $scope.closePopup('fileupload');
            }
            var hasUploadCompleted = true;
            for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
            {
                if (!$scope.uploadedFileQueue[i].isSuccess)
                {
                    hasUploadCompleted = false;
                    $scope.$broadcast('genericErrorEvent', 'Image upload is in progress.');
                    break;
                }
            }
            if (hasUploadCompleted && $scope.uploadedFileQueue.length > 0)
            {

                $scope.uploadedFileCount = $scope.uploadedFileQueue.length;
                $scope.isImageSavingProcess = true;
                $scope.dealAddModel.attach = [];
                for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
                {
                    var imageuploadparam = {};
                    imageuploadparam.path = $scope.uploadedFileQueue[i].urlpath;
                    imageuploadparam.urlpath = $scope.uploadedFileQueue[i].urlpath;
                    $scope.dealAddModel.attach.push(imageuploadparam);

                }
                $scope.isImageSavingProcess = false;
                $scope.closePopup('fileupload');
            }

        }
        $scope.$on("updateSavedCustomerDetail", function (event, customerDetail)
        {
            if (typeof customerDetail === 'undefined')
                return;
            $scope.dealAddModel.customer = customerDetail;
        });

        $scope.initUpdateDetailTimeoutPromise = null;

        $scope.initUpdateDetail = function ()
        {

            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
            }

            if ($scope.isUserLoaded && $scope.isStageLoaded)
            {
                $scope.updateDealInfo();
            } else
            {
                $scope.initUpdateDetailTimeoutPromise = $timeout($scope.initUpdateDetail, 300);
            }
        }

        $scope.getCustomerList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            if ($scope.dealAddModel.type == 'existing')
            {
                return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function (responseData)
                {
                    var data = responseData.data.list;
                    var hits = data;
                    if (hits.length > 10)
                    {
                        hits.splice(10, hits.length);
                    }
                    return hits;
                });
            }
        };

        $scope.formatCustomerModel = function (model)
        {
            if (model != null)
            {
                if (model.fname != undefined && model.phone != undefined && model.email != undefined)
                {
                    return model.fname + '(' + model.phone + ',' + model.email + ')';
                } else if (model.fname != undefined && model.phone != undefined)
                {
                    return model.fname + '(' + model.phone + ')';
                } else
                {
                    return model.fname;
                }
            }
            return  '';
        };
        $scope.dealStageLoading = false;
        $scope.updateDealInfo = function ()
        {
            if ($scope.dealDetail != null)
            {
                $scope.dealAddModel.id = parseInt($scope.dealDetail.id, 10);
                $scope.dealAddModel.name = $scope.dealDetail.name;
                $scope.dealAddModel.stage = $scope.dealDetail.stage_id + '';
                $scope.dealAddModel.amount = $scope.dealDetail.amount;
                $scope.dealAddModel.type = $scope.dealDetail.type;
                $scope.dealAddModel.customer =
                        {
                            id: $scope.dealDetail.customer_id,
                            fname: $scope.dealDetail.customer_name
                        }
                $scope.dealAddModel.closedate = $scope.dealDetail.colsed_date;

                if ($scope.dealDetail.next_follow_up != null && $scope.dealDetail.next_follow_up != undefined && $scope.dealDetail.next_follow_up != '')
                {
                    var nextdate = $scope.dealDetail.next_follow_up.split(' ');
                        var newnextdate = utilityService.convertToUTCDate(nextdate[0]);
                        $scope.dealAddModel.nextdate = utilityService.parseDateToStr(newnextdate, $rootScope.appConfig.date_format);
                }
                else
                {
                    $scope.dealAddModel.nextdate = '';
                }

                $scope.dealAddModel.user = $scope.dealDetail.user_id + '';
            }
            $scope.dealStageLoading = true;
            $scope.dealAddModel.isLoadingProgress = false;
        }

        $scope.getStageList = function ()
        {
            $scope.isStageLoaded = false;
            var getListParam = {};
            getListParam.is_active = 1;

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getStageList(getListParam, configOption).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.dealAddModel.stageList = data.list;
                    $scope.isStageLoaded = true;
                }
            });
        };

        $scope.getDealActivityList = function ()
        {
            $scope.isStageLoaded = false;
            var getListParam = {};
            getListParam.deal_id = $stateParams.id;

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getDealActivityList(getListParam, configOption).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.dealAddModel.activityList = data.list;
                    for(var i=0; i<$scope.dealAddModel.activityList.length; i++)
                    {
                        var newdate = new Date($scope.dealAddModel.activityList[i].created_at).toISOString();
                        $scope.dealAddModel.activityList[i].created_at_new = newdate;
                    }
                }
            });
        };
        //nisha fix timezone syed
        app.filter('timestampToISO', function() {
            return function(input) {
                input = new Date(input).toISOString();
                return input;
            };
        });
        $scope.getUserList = function()
        {
            $scope.isUserLoaded = false;
            var getListParam = {};
            getListParam.isactive = 1;

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getUserList(getListParam, configOption).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.dealAddModel.userList = data.list;
                    $scope.isUserLoaded = true;
                }
            });
        };

        $scope.changeStatus = function (id)
        {
            $scope.dealAddModel.stage = id;
            $scope.modifyDeal();
        };
        $scope.progressIndicatorStatus = function (indicatorValue)
        {
            var retVal = false;
            var stage = '';
            for (var i = 0; i < $scope.dealAddModel.stageList.length; i++)
            {
                if ($scope.dealAddModel.stageList[i].id == $scope.dealAddModel.stage)
                {
                    stage = parseInt($scope.dealAddModel.stageList[i].level);
                }
            }
            if (stage >= indicatorValue && $scope.dealStageLoading == true)
            {
                retVal = true;
            }

            return retVal;

        };
        $scope.modifyDeal = function () {
            if ($scope.isDataSavingProcess == false)
            {
                $scope.isDataSavingProcess = true;
                var createParam = {};
                var headers = {};
                headers['screen-code'] = 'deal';
                createParam.name = $scope.dealAddModel.name;
                createParam.stage_id = $scope.dealAddModel.stage;
                for (var i = 0; i < $scope.dealAddModel.stageList.length; i++)
                {
                    if ($scope.dealAddModel.stageList[i].id == $scope.dealAddModel.stage)
                    {
                        createParam.stage_name = $scope.dealAddModel.stageList[i].name;
                    }
                }
                createParam.type = $scope.dealAddModel.type;
                createParam.amount = $scope.dealAddModel.amount;
                createParam.customer_id = $scope.dealAddModel.customer.id;
                createParam.customer_name = $scope.dealAddModel.customer.fname;
                createParam.is_active = 1;
                if ($rootScope.userModel.rolename != 'superadmin')
                {
                    createParam.user_id = $rootScope.userModel.id;
                    createParam.user_name = $rootScope.userModel.f_name;
                } else
                {
                    createParam.user_id = $scope.dealAddModel.user;
                    for (var i = 0; i < $scope.dealAddModel.userList.length; i++)
                    {
                        if ($scope.dealAddModel.userList[i].id == $scope.dealAddModel.user)
                        {
                            createParam.user_name = $scope.dealAddModel.userList[i].f_name;
                        }
                    }
                }
                if ($scope.dealAddModel.closedate != '')
                {
                    if ($scope.dealAddModel.closedate != null && typeof $scope.dealAddModel.closedate == 'object')
                    {
                        createParam.colsed_date = utilityService.parseDateToStr($scope.dealAddModel.closedate, 'yyyy-MM-dd');
                        createParam.colsed_date = utilityService.changeDateToSqlFormat(createParam.colsed_date, 'yyyy-MM-dd');
                    }

                }
                if ($scope.dealAddModel.nextdate != '')
                {
                    if ($scope.dealAddModel.nextdate != null && typeof $scope.dealAddModel.nextdate == 'object')
                    {
                        createParam.next_follow_up = utilityService.parseDateToStr($scope.dealAddModel.nextdate, 'yyyy-MM-dd');
                        createParam.next_follow_up = utilityService.changeDateToSqlFormat(createParam.next_follow_up, 'yyyy-MM-dd');
                    }
                }
                adminService.editDeal(createParam, $scope.dealAddModel.id, adminService.handleSuccessAndErrorResponse, headers).then(function (response) {
                    if (response.data.success == true)
                    {
                        $scope.formReset(0);
                        $state.go('app.dealedit', {'id': $stateParams.id}, {'reload': true});
                    }
                    $scope.isDataSavingProcess = false;
                });
            }

        };

        $scope.getdealListInfo = function ()
        {
            $scope.id = '';
            $scope.dealAddModel.isLoadingProgress = true;
            var getListParam = {};
            getListParam.id = $stateParams.id;
            getListParam.is_active = 1;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getDealList(getListParam, configOption).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    if (data.list.length > 0)
                    {
                        $scope.dealDetail = data.list[0];
                        $scope.id = $stateParams.id;
                        $scope.getStageList();
                        $scope.getUserList();
                        $scope.initUpdateDetail();
                        $scope.getDealActivityList();
                    }
                }

            });
        };

        $scope.createInvoice = function (id)
        {
            if ($scope.currentStep == 3)
            {
                $state.go('app.dealInvoiceAdd', {'deal_id': $stateParams.id});
            }
            if ($scope.currentStep == 1)
            {
                $state.go('app.dealInvoiceAdd', {'id': id, 'deal_id': $stateParams.id});
            }
        };
        
        $scope.createQuote = function ()
        {
            $state.go('app.dealQuoteAdd', {'id': $stateParams.id});
        };
        
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.createNotes = function ()
        {
            if ($scope.isDataSavingProcess == false)
            {
                $scope.isDataSavingProcess = true;
                var createParam = {};
                var headers = {};
                headers['screen-code'] = 'deal';
                createParam.description = $scope.dealAddModel.notes;
                createParam.type = 'note';
                createParam.deal_id = $stateParams.id;
                createParam.comments = 'left a note';
                createParam.customer_id = $scope.dealAddModel.customer.id;
                //createParam.customer_name = $scope.dealAddModel.customer.fname;
                createParam.is_active = 1;
                createParam.date = $filter('date')($scope.currentDate, 'yyyy-MM-dd');
                createParam.user_id = $rootScope.userModel.id;
                createParam.user_name = $rootScope.userModel.f_name;
                createParam.log_activity_type = '';
                createParam.log_activity_sub_type = '';
                createParam.time = '';
                createParam.attachment = [];
                var arr = [];
                for (var i = 0; i < $scope.dealAddModel.attach.length; i++)
                {
                    var att = {};
                    att.file_path = $scope.dealAddModel.attach[i].urlpath;
                    arr.push(att);
                }
                createParam.attachment = arr;
                adminService.addDealActivity(createParam, headers).then(function (response) {
                    if (response.data.success == true)
                    {
                        $scope.formReset(1);
                        $state.go('app.dealedit', {'id': $stateParams.id}, {'reload': true});
                        // $state.go('app.invoiceView1', {'id': response.data.id}, {'reload': true});
                    }
                    $scope.isDataSavingProcess = false;
                });
            }

        };

        $scope.createLog = function ()
        {
            if ($scope.isDataSavingProcess == false)
            {
                $scope.isDataSavingProcess = true;
                var createParam = {};
                var headers = {};
                headers['screen-code'] = 'deal';
                createParam.description = $scope.dealAddModel.notes;
                createParam.type = 'logActivity';
                createParam.log_activity_sub_type = '';
                createParam.comments = '';
                createParam.deal_id = $stateParams.id;
                createParam.log_activity_type = $scope.dealAddModel.log_type;
                createParam.customer_id = $scope.dealAddModel.customer.id;
                createParam.attachments = [];
                if ($scope.dealAddModel.log_subtype != undefined && $scope.dealAddModel.log_subtype != null && $scope.dealAddModel.log_type == 'call')
                {
                    createParam.log_activity_sub_type = $scope.dealAddModel.log_subtype;
                    createParam.comments = 'made a call';
                    createParam.description = $scope.dealAddModel.notes + '-- Call outcome: ' + $scope.dealAddModel.log_subtype + '--';
                }
                if ($scope.dealAddModel.log_type == 'email')
                {
                    createParam.comments = 'sent an email';
                    //createParam.description = '';
                }
                if ($scope.dealAddModel.log_type == 'meeting')
                {
                    createParam.comments = 'arranged a meeting';
                    //createParam.description = '';
                }
                createParam.customer_id = $scope.dealAddModel.customer.id;
                //createParam.customer_name = $scope.dealAddModel.customer.fname;
                createParam.is_active = 1;
                createParam.date = $filter('date')($scope.dealAddModel.activity_date, 'yyyy-MM-dd');
                createParam.time = $filter('date')($scope.dealAddModel.activity_time, 'shortTime');
                createParam.user_id = $rootScope.userModel.id;
                createParam.user_name = $rootScope.userModel.f_name;


                adminService.addDealActivity(createParam, headers).then(function (response) {
                    if (response.data.success == true)
                    {
                        $scope.formReset(3);
                        $state.go('app.dealedit', {'id': $stateParams.id}, {'reload': true});
                        // $state.go('app.invoiceView1', {'id': response.data.id}, {'reload': true});
                    }
                    $scope.isDataSavingProcess = false;
                });
            }
        };

        $scope.initEstimateTableFilterTimeoutPromise = null;

        $scope.initEstimateTableFilter = function ()
        {
            if ($scope.initEstimateTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initEstimateTableFilterTimeoutPromise);
            }
            $scope.initEstimateTableFilterTimeoutPromise = $timeout($scope.getestimateListInfo, 300);
        }

        $scope.getestimateListInfo = function ()
        {
            $scope.dealAddModel.isLoadingProgress = true;
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'salesestimate';

            var configOption = adminService.handleOnlyErrorResponseConfig;

            getListParam.id = '';
            getListParam.deal_id = $stateParams.id;
            getListParam.start = ($scope.dealAddModel.estimatecurrentPage - 1) * $scope.dealAddModel.estimatelimit;
            getListParam.limit = $scope.dealAddModel.estimatelimit;
            getListParam.is_active = 1;
            getListParam.status = '';
            adminService.getQuoteList(getListParam, configOption, headers).then(function (response)
            {

                var data = response.data;
                if (data.success)
                {
                    $scope.dealAddModel.estimateList = data.list;
                    if ($scope.dealAddModel.estimateList.length > 0)
                    {
                        for (var i = 0; i < $scope.dealAddModel.estimateList.length; i++)
                        {
                            $scope.dealAddModel.estimateList[i].newdate = utilityService.parseStrToDate($scope.dealAddModel.estimateList[i].date);
                            $scope.dealAddModel.estimateList[i].date = utilityService.parseDateToStr($scope.dealAddModel.estimateList[i].newdate, "dd-MM-yyyy");
                            $scope.dealAddModel.estimateList[i].validuntildate = utilityService.parseStrToDate($scope.dealAddModel.estimateList[i].validuntil);
                            $scope.dealAddModel.estimateList[i].validuntil = utilityService.parseDateToStr($scope.dealAddModel.estimateList[i].validuntildate, "dd-MM-yyyy");
                            $scope.dealAddModel.estimateList[i].total_amount = utilityService.changeCurrency($scope.dealAddModel.estimateList[i].total_amount, $rootScope.appConfig.thousand_seperator);
                            $scope.dealAddModel.estimateList[i].advance_amount = parseFloat(utilityService.changeCurrency($scope.dealAddModel.estimateList[i].advance_amount, $rootScope.appConfig.thousand_seperator)).toFixed(2);

                        }
                    }
                    $scope.dealAddModel.estimatetotal = data.total;
                }
                $scope.dealAddModel.isLoadingProgress = false;
            });
        };
        $scope.getestimateListInfo();
        $scope.getdealListInfo();

    }]);




