
app.controller('debitnoteAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', 'sweet', function ($scope, $rootScope, adminService, $httpService, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST, sweet) {

        $scope.debitModel =
                {
                    "id": 0,
                    "account": '',
                    "date": '',
                    "list": [],
                    "comments": "",
                    "amount": '',
                    "payment_mode": '',
                    "paymentlist": [],
                    "invoice_id": '',
                    "customerInfo": {},
                    "customer_id": '',
                    "categoryList": [],
                    "accountsList": [],
                    "category": '',
                    "cheque_no": '',
                    "invoiceList": [],
                    "invoiceDetails": {}
                }


        $scope.adminService = adminService;
        $scope.validationFactory = ValidationFactory;
        $scope.getNavigationBlockMsg = function (pageReload)
        {
            if (typeof $scope.save_debit_form != 'undefined' && typeof $scope.save_debit_form.$pristine != 'undefined' && !$scope.save_debit_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.isDataSavingProcess = false;
        $scope.isSalesOrderLoading = false;
        $scope.currentDate = new Date();
        $scope.debitModel.date = $scope.currentDate;

        $scope.formReset = function ( )
        {
            $scope.debitModel.invoice_id = '';
            $scope.debitModel.customer_id = '';
            $scope.debitModel.account = "";
            $scope.debitModel.date = "";
            $scope.debitModel.payment_mode = '';
            $scope.debitModel.cheque_no = '';
            $scope.debitModel.paymentdefault = '';
            $scope.debitModel.amount = "";
            $scope.debitModel.comments = "";
            $scope.debitModel.invoiceDetails = {};
            $scope.debitModel.invoiceList = [];
        }


        $scope.paymentDateOpen = false;
        $scope.openDate = function (index)
        {
            if (index == 0)
            {
                $scope.paymentDateOpen = true;
            }

        }

        $scope.getCustomerList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = [];
                if (data.length > 0)
                {
                    for (var i = 0; i < data.length; i++)
                    {
                        if (data[i].is_sale == '1' || data[i].is_sale == 1) {
                            hits.push(data[i]);
                        }
                    }
                }
                if (hits.length > 10) {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatCustomerModel = function (model)
        {
            if (model != null)
            {
                var phoneNumber = '';
                if (model.phone != undefined)
                {
                    if (model.phone.indexOf(','))
                    {
                        phoneNumber = model.phone.split(',')[0];
                    }
                }
                if (model.fname != undefined && phoneNumber != '' && model.email != undefined)
                {

                    return model.fname + '(' + phoneNumber + ',' + model.email + ')';
                } else if (model.fname != undefined && phoneNumber != '') {

                    return model.fname + '(' + phoneNumber + ')';
                } else if (model.fname != undefined)
                {
                    return model.fname;
                }
            }
            return  '';
        };

        //            $scope.getinvoiceList = function ()
        //            {
        //                  var getListParam = {};
        //                  if ($scope.debitModel.customerInfo != null && typeof $scope.debitModel.customerInfo != 'undefined' && typeof $scope.debitModel.customerInfo.id != 'undefined')
        //                  {
        //                        $scope.isSalesOrderLoading = true;
        //                        getListParam.customer_id = $scope.debitModel.customerInfo.id;
        //                        getListParam.pending = 1;
        //                        var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
        //                        adminService.getInvoiceList(getListParam, configOption).then(function (response)
        //                        {
        //                              if (response.data.success === true) {
        //                                    var data = response.data.list;
        //                                    $scope.debitModel.invoiceList = data;
        //                                    if ($scope.debitModel.invoiceList.length > 0)
        //                                    {
        //                                          for (var i = 0; i < $scope.debitModel.invoiceList.length; i++)
        //                                          {
        //                                                $scope.debitModel.invoiceList[i].newdate = utilityService.parseStrToDate($scope.debitModel.invoiceList[i].date);
        //                                                $scope.debitModel.invoiceList[i].date = utilityService.parseDateToStr($scope.debitModel.invoiceList[i].newdate, $rootScope.appConfig.date_format);
        //                                                $scope.debitModel.invoiceList[i].balance_amount = parseFloat($scope.debitModel.invoiceList[i].total_amount) - parseFloat($scope.debitModel.invoiceList[i].paid_amount);
        //                                                $scope.debitModel.invoiceList[i].balance_amount = parseFloat($scope.debitModel.invoiceList[i].balance_amount).toFixed(2);
        //                                          }
        //                                          $scope.debitModel.invoice_id = $scope.debitModel.invoiceList[0].id;
        //                                          $scope.getInvoiceDetails(0);
        //                                    } else
        //                                    {
        //                                          $scope.debitModel.invoiceDetails = '';
        //                                          $scope.debitModel.invoice_id = '';
        //                                    }
        //                                    $scope.isSalesOrderLoading = false;
        //                              }
        //                        });
        //                  }
        //            };

        //            $scope.getInvoiceDetails = function (index)
        //            {
        //                  $scope.debitModel.invoiceDetails = $scope.debitModel.invoiceList[index];
        //            };

        $scope.getIncomeCategoryList = function () {

            var getListParam = {};
            getListParam.id = '';
            getListParam.is_active = 1;
            getListParam.type = 'income';
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;

            adminService.getIncomeCategoryList(getListParam, configOption).then(function (response) {
                var data = response.data.list;
                $scope.debitModel.categoryList = data;
            });

        };
        $scope.getAccountlist = function () {

            var getListParam = {};
            getListParam.id = '';
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getAccountlist(getListParam, configOption).then(function (response) {
                var data = response.data.list;
                $scope.debitModel.accountsList = data;
            });

        };

        $scope.createdebit = function ()
        {
            var createAdvanceParam = {};
            var headers = {};
            headers['screen-code'] = 'debitnote';
            if ($scope.debitModel.customerInfo != null && typeof $scope.debitModel.customerInfo != 'undefined' && typeof $scope.debitModel.customerInfo.id != 'undefined')
            {
                createAdvanceParam.customer_id = $scope.debitModel.customerInfo.id;
            }
            createAdvanceParam.comments = $scope.debitModel.comments;
            if (typeof $scope.debitModel.date == 'object')
            {
                $scope.debitModel.date = utilityService.parseDateToStr($scope.debitModel.date, $scope.adminService.appConfig.date_format);
            }
            createAdvanceParam.date = utilityService.changeDateToSqlFormat($scope.debitModel.date, $scope.adminService.appConfig.date_format);
            createAdvanceParam.amount = $scope.debitModel.amount;
            createAdvanceParam.voucher_type = "debitNote";
            createAdvanceParam.used_amount = $scope.debitModel.amount;
            createAdvanceParam.balance_amount = 0;

            createAdvanceParam.voucher_no = '';
            createAdvanceParam.payment_mode = '';
            createAdvanceParam.tra_category = '';
            createAdvanceParam.reference_no = '';
            createAdvanceParam.prefix = $scope.debitModel.invoicePrefix;
            for (var i = 0; i < $scope.debitModel.accountsList.length; i++)
            {
                if ($scope.debitModel.account == $scope.debitModel.accountsList[i].id)
                {
                    createAdvanceParam.account_id = $scope.debitModel.accountsList[i].id;
                    createAdvanceParam.account = $scope.debitModel.accountsList[i].account;
                }
            }
            createAdvanceParam.is_active = 1;
            $scope.isDataSavingProcess = true;
            var configOption = adminService.handleBothSuccessAndErrorResponseConfig;
            adminService.addAdvancepayment(createAdvanceParam, configOption, headers).then(function (response) {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.debitnote');
                }
                $scope.isDataSavingProcess = false;
            }).catch(function (response) {
                console.error('Error occurred:', response.status, response.data);
                $scope.isDataSavingProcess = false;
                var element = document.getElementById("btnLoad");
                element.classList.remove("btn-loader");
            });


        };
        $scope.getpaymenttermslist = function () {

            var getListParam = {};
            getListParam.id = '';
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;

            adminService.getpaymenttermslist(getListParam, configOption).then(function (response) {
                var data = response.data.list;
                $scope.debitModel.paymentlist = data;
            });
        }

        $scope.getsalesPrefixList = function ()
        {
            var getListParam = {};
            getListParam.setting = 'sales_invoice_prefix';
            $scope.debitModel.prefixList = [];
            var headers = {};
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getAppSettingsList(getListParam, configOption, headers).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    if (data.list.length > 0)
                    {
                        var perfix = data.list[0].value;
                        perfix = perfix.split('\n');
                        for (var i = 0; i < perfix.length; i++)
                        {
                            $scope.debitModel.prefixList.push(perfix[i]);
                        }
                    }
                    // $scope.getPurchasePrefixList();

                }
                $scope.isPrefixListLoaded = true;
            });
        };
        $scope.getsalesPrefixList();
        $scope.getpaymenttermslist();
        $scope.getIncomeCategoryList();
        $scope.getAccountlist();
    }]);




