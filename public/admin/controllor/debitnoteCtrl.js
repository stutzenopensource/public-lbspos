app.controller('debitnoteCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$filter', 'Auth', '$timeout', '$httpService', 'APP_CONST', 'ValidationFactory', function($scope, $rootScope, adminService, utilityService, $filter, Auth, $timeout, $httpService, APP_CONST, ValidationFactory) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.debitModel = {
            currentPage: 1,
            total: 0,
            limit: 4,
            list: [],
            status: '',
            isLoadingProgress: true,
            accountsList:[]
        };
        $scope.validationFactory = ValidationFactory;
        $scope.pagePerCount = [50, 100];
        $scope.debitModel.limit = $scope.pagePerCount[0];
        $scope.adminService = adminService;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.searchFilter =
                {
                    customerInfo: {},
                    from_date: '',
                    to_date: '',
                    invoicePrefix: ''
                };
        $scope.openDate = function(index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            }
            else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }

        $scope.refreshScreen = function()
        {
            $scope.searchFilter =
                    {
                        customerInfo: {},
                        from_date: '',
                        to_date: '',
                        invoicePrefix: ''
                    };
            $scope.initTableFilter();
        }
        $scope.selectedpaymentId = '';
        $scope.showdeletePopup = false;
        $scope.isdeleteProgress = false;
        $scope.showPopup = function(id)
        {
            $scope.selectedpaymentId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteProgress = false;
        };

        $scope.deleteAdvancePaymentItem = function( )
        {
            if ($scope.isdeleteProgress == false)
            {
                $scope.isdeleteProgress = true;

                var getListParam = {};
                getListParam.id = $scope.selectedpaymentId;
                getListParam.voucher_type = 'advance_payment';
                var headers = {};
                headers['screen-code'] = 'advancepayment';
                adminService.deleteAdvancepayment(getListParam, getListParam.id, headers).then(function(response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getList();
                    }

                    $scope.isdeleteProgress = false;
                });

            }

        };

        $scope.getCustomerList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            autosearchParam.mode = 2;
            if (autosearchParam.search != '')
            {
                return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function(responseData)
                {
                    var data = responseData.data.list;
                    var hits = data;
                    if (hits.length > 10)
                    {
                        hits.splice(10, hits.length);
                    }
                    return hits;
                });
            }
        };

        $scope.formatCustomerModel = function(model)
        {
            if (model != null)
            {
                if (model.fname != undefined && model.phone != undefined && model.email != undefined)
                {
                    return model.fname + '(' + model.phone + ',' + model.email + ')';
                }
                else if (model.fname != undefined && model.phone != undefined)
                {
                    return model.fname + '(' + model.phone + ')';
                }
            }
            return  '';
        };
        $scope.getAccountlist = function() {

            var getListParam = {};
            getListParam.id = '';
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;

            adminService.getAccountlist(getListParam, configOption).then(function(response) {
                var data = response.data.list;
                $scope.debitModel.accountsList = data;
                //$scope.getList();
            });

        };
        
         $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'debitpayment_list';


        $scope.$on('onLocalStorageFormRenterCompleteEvent', function(event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });  
        
          $scope.$on('onLocalStorageFieldRenterCompleteEvent', function(event, formName) {
             if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRenterCount++;
                }
                if ($scope.localStorageRenterCount >= $scope.localStorageCount)
                {
                    console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                    $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
                }
            
              }
        });
        $scope.$on('updateTypeaheadFieldValue', function(event, data) {

            if (data.fieldName == "debitpayment_list-customer_dropdown")
            {
                $scope.searchFilter.customerInfo = data.value;
            }
        });
        
        

        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function(event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRetrieveCount++;
                }
                if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
                {
                    $scope.initTableFilter();
                }
                console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);
            }
        });

        $scope.init = function()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');

        };

        $scope.$on('onLocalStorageReadyEvent', function() {
           if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                $timeout(function() {
                    $scope.init();
                }, 300);
            
            }
        });          
        $scope.getList = function()
        {
            $scope.debitModel.isLoadingProgress = true;
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'debitnote';

            var configOption = adminService.handleOnlyErrorResponseConfig;

            if ($scope.searchFilter.customerInfo != null && typeof $scope.searchFilter.customerInfo != 'undefined' && typeof $scope.searchFilter.customerInfo.id != 'undefined')
            {
                getListParam.customer_id = $scope.searchFilter.customerInfo.id;
            }
            else
            {
                getListParam.customer_id = '';
            }
//            for (var i = 0; i < $scope.debitModel.accountsList.length; i++)
//            {
//                if ($scope.debitModel.accountsList[i].account == 'Debit Note' && $scope.debitModel.accountsList[i].system_generated == 1)
//                {
//                    getListParam.account_id = $scope.debitModel.accountsList[i].id;
//                }
//            }
            getListParam.prefix = $scope.searchFilter.invoicePrefix;
//        getListParam.from_date = $scope.searchFilter.from_date;
//        getListParam.to_date = $scope.searchFilter.to_date;
//        if ($scope.searchFilter.to_date != '' && ($scope.searchFilter.from_date == null || $scope.searchFilter.from_date == ''))
//        {
//            if ($scope.searchFilter.to_date != null && typeof $scope.searchFilter.to_date == 'object')
//            {
//                getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.to_date, $scope.dateFormat);
//
//                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
//                getListParam.from_date = getListParam.to_date;
//            }
//        }
//        else if ($scope.searchFilter.from_date != '' && ($scope.searchFilter.to_date == null || $scope.searchFilter.to_date == ''))
//        {
//            if ($scope.searchFilter.from_date != null && typeof $scope.searchFilter.from_date == 'object')
//            {
//                getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.from_date, $scope.dateFormat);
//
//                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
//                getListParam.to_date = getListParam.from_date;
//            }
//        }
//        else if ($scope.searchFilter.to_date != null && $scope.searchFilter.to_date != '' && $scope.searchFilter.from_date != null && $scope.searchFilter.from_date != '')
//        {
//            if ($scope.searchFilter.to_date != null && typeof $scope.searchFilter.to_date == 'object')
//            {
//                getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.to_date, $scope.dateFormat);
//            }
//            if ($scope.searchFilter.from_date != null && typeof $scope.searchFilter.from_date == 'object')
//            {
//                getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.from_date, $scope.dateFormat);
//            }
//            getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
//            getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
//        } 
            getListParam.voucher_type = 'debitNote';
            getListParam.start = ($scope.debitModel.currentPage - 1) * $scope.debitModel.limit;
            getListParam.limit = $scope.debitModel.limit;
            getListParam.is_active = 1;
            adminService.getAdvancepaymentList(getListParam, configOption, headers).then(function(response)
            {

                var data = response.data;
                if (data.success)
                {
                    $scope.debitModel.list = data.list;
                    if ($scope.debitModel.list.length > 0)
                    {
                        for (var i = 0; i < $scope.debitModel.list.length; i++)
                        {
                            $scope.debitModel.list[i].newdate = utilityService.parseStrToDate($scope.debitModel.list[i].date);
                            $scope.debitModel.list[i].date = utilityService.parseDateToStr($scope.debitModel.list[i].newdate, $rootScope.appConfig.date_format);
                            $scope.debitModel.list[i].total_amount = parseFloat($scope.debitModel.list[i].amount).toFixed(2);
                            $scope.debitModel.list[i].amount = utilityService.changeCurrency($scope.debitModel.list[i].total_amount, $rootScope.appConfig.thousand_seperator);

                        }
                    }
                    $scope.debitModel.total = data.total;
                }
                $scope.debitModel.isLoadingProgress = false;
            });
        };

        $scope.getsalesPrefixList = function()
        {
            var getListParam = {};
            getListParam.setting = 'sales_invoice_prefix';
            $scope.debitModel.prefixList = [];
            var headers = {};
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getAppSettingsList(getListParam, configOption, headers).then(function(response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    if (data.list.length > 0)
                    {
                        var perfix = data.list[0].value;
                        perfix = perfix.split('\n');
                        for (var i = 0; i < perfix.length; i++)
                        {
                            $scope.debitModel.prefixList.push(perfix[i]);
                        }
                    }
                    // $scope.getPurchasePrefixList();

                }
                $scope.isPrefixListLoaded = true;
            });
        };
        $scope.getsalesPrefixList();
        $scope.getAccountlist();
    }]);




