app.controller('debitnoteViewCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', 'APP_CONST', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', function ($scope, $rootScope, adminService, $httpService, utilityService, APP_CONST, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory) {

    $scope.debitModel = {
        "debitDetails": {},
        'list': ''
    };

    $scope.validationFactory = ValidationFactory;

    $scope.getNavigationBlockMsg = function (pageReload)
    {
        if (typeof $scope.advancepayment_view !== 'undefined' && typeof $scope.advancepayment_view.$pristine !== 'undefined' && !$scope.advancepayment_view.$pristine)
        {
            return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
        }

        return "";
    };
    $scope.isDataSavingProcess = false;
    $scope.formReset = function () {
        $scope.transferlist_edit_form.$setPristine();
        $scope.updateTransferlistInfo();

    };
    //        $scope.currentDate = new Date();
    //       $scope.debitModel.date = $filter('date')($scope.currentDate, $scope.dateFormat);

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
    $scope.updatedebitInfo = function ()
    {
        if ($scope.debitModel.list !== null)
        {
                
            $scope.debitModel.debitDetails = $scope.debitModel.list;
            $scope.debitModel.debitDetails.newdate = utilityService.parseStrToDate($scope.debitModel.debitDetails.date);
            $scope.debitModel.debitDetails.date = utilityService.parseDateToStr($scope.debitModel.debitDetails.newdate, $rootScope.appConfig.date_format);
            $scope.debitModel.debitDetails.total_amount = parseFloat($scope.debitModel.debitDetails.amount).toFixed(2);
            $scope.debitModel.debitDetails.amount = utilityService.changeCurrency($scope.debitModel.debitDetails.total_amount, $rootScope.appConfig.thousand_seperator);
            $scope.debitModel.isLoadingProgress = false;
        }
    };
    $scope.getDebitInfo = function () {

        if (typeof $stateParams.id !== 'undefined')
        {
            var getListParam = {};
            getListParam.id = $stateParams.id;
            var headers = {};
            headers['screen-code'] = 'debitnote';
            $scope.debitModel.isLoadingProgress = true;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getAdvancepaymentList(getListParam, configOption,headers).then(function (response) {
                var data = response.data;
                if (data.total !== 0)
                {
                    $scope.debitModel.list = data.list[0];
                    $scope.updatedebitInfo();
                }

            });
        }
    };



    $scope.getDebitInfo();


}]);




