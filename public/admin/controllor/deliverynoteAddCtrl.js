
app.controller('deliverynoteAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', 'sweet', function ($scope, $rootScope, adminService, $httpService, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST, sweet) {

        $scope.orderListModel =
                {
                    "id": 0,
                    "productname": "",
                    "sales_price": "",
                    "qty": "",
                    "rowtotal": "",
                    "isActive": "1",
                    "total": 0,
                    "comments": "",
                    "ref_no": "",
                    "subtotal": '0.00',
                    list: [],
                    "customerInfo": '',
                    customerIconInfo: [],
                    customerInfoLoadingProgress: false
                }
        $scope.adminService = adminService;
        $scope.round = '0.00';

        $scope.closePopup = function () {
            $scope.showpopup = false;
        }

        $scope.validationFactory = ValidationFactory;
        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function ()
        {
            if (typeof $scope.delivery_add_form != 'undefined' && typeof $scope.delivery_add_form.$pristine != 'undefined' && !$scope.delivery_add_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }

        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currencyFormat = $rootScope.appConfig.currency;
        $scope.quantityFormat = $rootScope.appConfig.qty_format;
        $scope.currentDate = new Date();
        $scope.orderListModel.billdate = $scope.currentDate;

        $scope.isDataSavingProcess = false;
        $scope.formReset = function () {

            $scope.delivery_add_form.$setPristine();
        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.initTableFilterTimeoutPromise = null;
        $scope.deletedProductList = [];
        $scope.undeletedProductList = [];
        $scope.showbillPopup = false;
        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }
        $scope.maxDate = $scope.maxDate ? null : new Date();
        $scope.startDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.openDate = function (index) {

            if (index == 0)
            {
                $scope.startDateOpen = true;
            } else if (index == 1)
            {
                $scope.toDateOpen = true;
            }
        }

        $scope.showCustomerAddPopup = false;
        $scope.showCustomerPopup = function ()
        {
            $scope.showCustomerAddPopup = true;
            if (typeof $scope.orderListModel.customerInfo != undefined && $scope.orderListModel.customerInfo != '' && $scope.orderListModel.customerInfo != null)
            {
                $rootScope.$broadcast('customerInfo', $scope.orderListModel.customerInfo);
            }
        }
        $scope.closeCustomerPopup = function ()
        {
            $scope.showCustomerAddPopup = false;
        }

        $scope.$on("updateSavedCustomerDetail", function (event, customerDetail)
        {
            if (typeof customerDetail === 'undefined')
                return;
            $scope.orderListModel.customerInfo = customerDetail;
        });
        $scope.showInfoIcon = false;
        $scope.showIconInfo = function ()
        {
            $scope.orderListModel.list = [];
            $scope.addNewProduct();
            if (typeof $scope.orderListModel.customerInfo != undefined && $scope.orderListModel.customerInfo != '' && $stateParams.type == "in")
            {
                $scope.showInfoIcon = true;
                $scope.getCustomerProductInfo();
            } else
            {
                $scope.showInfoIcon = false;
            }
        }
        $scope.closeCusInfoPopup = function ()
        {
            $scope.showcustomerinfopopup = false;
        }
        $scope.showCusInfoPopup = function ()
        {
            $scope.showcustomerinfopopup = true;
        }
        $scope.getCustomerProductInfo = function ()
        {
            $scope.orderListModel.customerInfoLoadingProgress = true;
            var getCustomerInfoParam = {};
            var headers = {};
            headers['screen-code'] = 'deliverynote';
            getCustomerInfoParam.customer_id = $scope.orderListModel.customerInfo.id;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getBalanceQty(getCustomerInfoParam, configOption, headers).then(function (response) {
                if (response.data.success == true)
                {
                    if (response.data.list.length > 0)
                    {
                        $scope.orderListModel.customerIconInfo = [];
                        $scope.orderListModel.customerIconInfo = response.data.list;
                        $scope.orderListModel.customerInfoLoadingProgress = false;
                    } else
                    {
                        sweet.show('Oops...', "Selected customer doesn't have any outwards", 'error');
                        $scope.orderListModel.customerInfoLoadingProgress = false;
                        $scope.orderListModel.customerInfo = '';
                    }

                }


            });
        };
        $scope.checkProductInfo = function (model, index)
        {
            if ($scope.orderListModel.customerIconInfo.length > 0)
            {
                var found = false;
                $scope.balance = 0;
                for (var i = 0; i < $scope.orderListModel.customerIconInfo.length; i++)
                {
                    if ($scope.orderListModel.customerIconInfo[i].product_id == model.id)
                    {
                        found = true;
                        $scope.balance = $scope.orderListModel.customerIconInfo[i].balance_qty;
                        $scope.outWardDate = $scope.orderListModel.customerIconInfo[i].outward_date;
                    }
                }
                if (found == true)
                {
                    $scope.orderListModel.list[index].qtyCheck = false;
                    $scope.orderListModel.list[index].limit = parseFloat($scope.balance).toFixed($scope.quantityFormat);
                } else
                {
                    $scope.orderListModel.list[index].qty = 0;
                    $scope.orderListModel.list[index].qtyCheck = true;
                }

            } else
            {
                $scope.orderListModel.list[index].qty = 0;
                $scope.orderListModel.list[index].qtyCheck = true;
            }
        }
        $scope.productDrop = function (index) {

            if ($scope.orderListModel.list[index].id == '') {
                $scope.orderListModel.list[index].productname = '';
            }
        };
        $scope.getCustomerList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            autosearchParam.mode = 1;
            return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
//                var hits = [];
//                if (data.length > 0)
//                {
//                    for (var i = 0; i < data.length; i++)
//                    {
//                        if (data[i].is_sale == '1' || data[i].is_sale == 1)
//                        {
//                            hits.push(data[i]);
//                        }
//                    }
//                }
//                if (hits.length > 10)
//                {
//                    hits.splice(10, hits.length);
//                }
                return hits;
            });
        };

        $scope.formatCustomerModel = function (model)
        {
            if (model != null)
            {
                if (model.fname != undefined && model.phone != undefined && model.email != undefined)
                {
                    return model.fname + '(' + model.phone + ',' + model.email + ')';
                } else if (model.fname != undefined && model.phone != undefined)
                {
                    return model.fname + '(' + model.phone + ')';
                } else if (model.fname != undefined)
                {
                    return model.fname;
                }
            }
            return  '';
        };
        $scope.custDrop = function () {
            if ($scope.orderListModel.customerInfo.id == undefined) {
                $scope.orderListModel.customerInfo = '';
            }
        };
        $scope.validateDateFilterData = function ()
        {
            if ($stateParams.type == "in")
            {
                var retVal = true;
                var minDate = $scope.orderListModel.billdate;
                var value = $scope.outWardDate;
                if (minDate != null && value != null)
                {
                    var minDateValue = moment(minDate).valueOf();
                    var value = moment(value).valueOf();
                    if (value > minDateValue)
                    {
                        retVal = false;
                    }
                }
                return retVal;
            } else
            {
                var retVal = true;
                return retVal;
            }

        };
        $scope.addNewProduct = function ()
        {
            var newRow = {
                "id": '',
                "productName": '',
                "sales_price": '',
                "qty": '',
                "rowtotal": '',
                "limit": 0,
                "qtyCheck": false
            };
            $scope.orderListModel.list.push(newRow);
        };
        $scope.addNewProduct();
        $scope.deleteProduct = function (index)
        {
            $scope.orderListModel.list.splice(index, 1);
            $scope.calculatetotal();
        };

        $scope.totalcalculationTimeoutPromise != null
        $scope.updateInvoiceTotal = function ()
        {
            if ($scope.totalcalculationTimeoutPromise != null)
            {
                $timeout.cancel($scope.totalcalculationTimeoutPromise);
            }
            $scope.totalcalculationTimeoutPromise = $timeout($scope.calculatetotal, 300);
        }

        $scope.initUpdateDetailTimeoutPromise = null;
        $scope.initUpdateDetail = function ()
        {
            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
                $scope.getCustomerList();
            }
        }

        $scope.getItemList = function (val)
        {
            if ($scope.orderListModel.customerInfo != '' && $scope.orderListModel.customerInfo != undefined && $scope.orderListModel.customerInfo != null && $scope.orderListModel.customerInfo.id != undefined)
            {
                var autosearchParam = {};
                autosearchParam.search = val;
                autosearchParam.is_active = 1;
                return $httpService.get(APP_CONST.API.PRODUCT_BASED_LIST, autosearchParam, false).then(function (responseData)
                {
                    var data = responseData.data.list;
                    var hits = [];
                    if (data.length > 0)
                    {
                        for (var i = 0; i < data.length; i++)
                        {
                            if (data[i].is_sale == '1' || data[i].is_sale == 1)
                            {
                                hits.push(data[i]);
                            }
                        }
                    }
                    if (hits.length > 10)
                    {
                        hits.splice(10, hits.length);
                    }
                    return hits;
                });
            } else
            {
                sweet.show("Oops", "Please select customer", "error");
                return;
            }
        };

        $scope.formatProductModel = function (model, index)
        {
            if (model != null && typeof model.id != "undefined")
            {
                $scope.updateProductInfo(model, index);
                return model.code + '(' + model.name + ')';
            }
            return  '';
        }
        
        $scope.getUomList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            return $httpService.get(APP_CONST.API.UOM, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatUomModel = function (model)
        {
            if (model != null)
            {
                return model.name;
            }
            return  '';
        };

        $scope.updateProductInfo = function (model, index)
        {
            if ($scope.orderListModel.list.length != 0)
            {
                if (typeof model == 'undefined')
                    return;

                $scope.orderListModel.list[index].id = model.id;
                $scope.orderListModel.list[index].sku = model.sku;
                $scope.orderListModel.list[index].sales_price = model.selling_price;
                if ($scope.orderListModel.list[index].qtyCheck == false)
                {
                    $scope.orderListModel.list[index].qty = 1;
                }
                $scope.orderListModel.list[index].rowtotal = model.selling_price;
                $scope.orderListModel.list[index].mrp_price = model.mrp_price;
                $scope.orderListModel.list[index].purchase_price = model.purchase_price;
                $scope.orderListModel.list[index].barcode = model.code;
                $scope.orderListModel.list[index].bcn_id = model.bcn_id;
                $scope.orderListModel.list[index].qty_limit = parseFloat (model.qty_in_hand);
                $scope.orderListModel.list[index].ref_id = model.ref_id;
                $scope.orderListModel.list[index].uomInfo = {
                    "name": model.uom,
                }
                if (model.uomid != undefined)
                {
                    $scope.orderListModel.list[index].uomid = model.uomid;
                }
                if (model.uom_id != undefined)
                {
                    $scope.orderListModel.list[index].uomid = model.uom_id;
                }
                if (model.uomid != undefined && model.uom_id != undefined)
                {
                    $scope.orderListModel.list[index].uomid = "";
                }
                $scope.updateInvoiceTotal();
                return;
            }
        }

        $scope.formValidator = function ()
        {
            //performace issue in form validator bug
            if ($scope.delivery_add_form.$submitted)
            {
                $timeout(function () {
                    $scope.delivery_add_form.$submitted = false;
                }, 0);
                $timeout(function () {
                    $scope.delivery_add_form.$setSubmitted();
                }, 100);
            } else
            {
                $timeout(function () {
                    $scope.delivery_add_form.$setSubmitted();
                }, 100);
            }
        }

        $scope.calculatetotal = function ()
        {
            var subTotal = 0;
            $scope.orderListModel.purchase_price = 0.00;
            $scope.orderListModel.mrp_price = 0.00;
            $scope.orderListModel.qty = 0.00;
            console.log("bfr loop", $scope.orderListModel.list.length);
            for (var i = 0; i < $scope.orderListModel.list.length; i++)
            {
                $scope.orderListModel.list[i].rowtotal = $scope.orderListModel.list[i].sales_price * $scope.orderListModel.list[i].qty;
                subTotal += parseFloat($scope.orderListModel.list[i].rowtotal);
                $scope.orderListModel.list[i].rowtotal = $scope.orderListModel.list[i].rowtotal.toFixed(2);
                $scope.orderListModel.purchase_price += parseFloat($scope.orderListModel.list[i].purchase_price);
                $scope.orderListModel.mrp_price += parseFloat($scope.orderListModel.list[i].mrp_price);
                $scope.orderListModel.qty += parseFloat($scope.orderListModel.list[i].qty);
            }
            $scope.orderListModel.subtotal = parseFloat(subTotal).toFixed(2);
            $scope.orderListModel.overall_purchase_price = parseFloat($scope.orderListModel.purchase_price).toFixed(2);
            $scope.orderListModel.overall_mrp = parseFloat($scope.orderListModel.mrp_price).toFixed(2);
            $scope.orderListModel.overall_selling_price = $scope.orderListModel.subtotal;
            $scope.orderListModel.overall_qty = $scope.orderListModel.qty;
            $scope.updateRoundoff();
        }
        $scope.updateRoundoff = function ()
        {
            $scope.round = Math.round($scope.orderListModel.subtotal);
            $scope.orderListModel.roundoff = $scope.round - parseFloat($scope.orderListModel.subtotal);
            $scope.orderListModel.roundoff = parseFloat($scope.orderListModel.roundoff).toFixed(2);
            $scope.round = parseFloat($scope.round).toFixed(2);
        }

        $scope.createOrder = function (forceSave)
        {
            if (!forceSave)
            {
                if ($scope.isDataSavingProcess)
                {
                    return;
                }
            }
            $scope.isDataSavingProcess = true;
            $scope.isSave = false;
            for (var i = 0; i < $scope.orderListModel.list.length; i++)
            {
                if ($scope.orderListModel.list[i].productname != '' && $scope.orderListModel.list[i].productname != null &&
                        $scope.orderListModel.list[i].qty != '' && $scope.orderListModel.list[i].qty != null &&
                        $scope.orderListModel.list[i].sales_price != '' && $scope.orderListModel.list[i].sales_price != null &&
                        $scope.orderListModel.list[i].rowtotal != '' && $scope.orderListModel.list[i].rowtotal != null)
                {
                    $scope.isSave = true;
                } else
                {
                    $scope.isSave = false;
                }
                if ($scope.orderListModel.list.length == 1)
                {
                    if ($scope.orderListModel.list[0].productname != '' && $scope.orderListModel.list[0].productname != null &&
                            $scope.orderListModel.list[0].qty != '' && $scope.orderListModel.list[0].qty != null &&
                            $scope.orderListModel.list[0].sales_price != '' && $scope.orderListModel.list[0].sales_price != null &&
                            $scope.orderListModel.list[0].rowtotal != '' && $scope.orderListModel.list[0].rowtotal != null)
                    {
                        $scope.isSave = true;
                    } else
                    {
                        $scope.isSave = false;
                        if ($scope.orderListModel.list[i].qty == 0 && $scope.orderListModel.list[i].rowtotal == "0.00")
                        {
                            $scope.isSave = true;
                        }
                    }
                }
            }

            if ($scope.isSave)
            {
                $scope.saveOrder();

            } else
            {
                sweet.show('Oops...', 'Fill Product Detail.. ', 'error');
                $scope.isDataSavingProcess = false;
                var element = document.getElementById("btnLoad");
                element.classList.remove("btn-loader");
            }
        };

        $scope.currentDate = new Date();
        $scope.dateFormat = $rootScope.appConfig.date_format;

        $scope.saveOrder = function ()
        {
            var createOrderParam = {};
            var headers = {};
            headers['screen-code'] = 'deliverynote';
            if (typeof $scope.orderListModel.billdate == 'object')
            {
                var billdate = utilityService.parseDateToStr($scope.orderListModel.billdate, 'yyyy-MM-dd');
            }
            createOrderParam.date = utilityService.changeDateToSqlFormat(billdate, 'yyyy-MM-dd');
            createOrderParam.overall_purchase_price = $scope.orderListModel.overall_purchase_price;
            createOrderParam.overall_selling_price = $scope.orderListModel.overall_selling_price;
            createOrderParam.overall_mrp = $scope.orderListModel.overall_mrp;
            createOrderParam.overall_qty = parseFloat($scope.orderListModel.overall_qty).toFixed($scope.quantityFormat);
            createOrderParam.detail = [];
            createOrderParam.customer_id = $scope.orderListModel.customerInfo.id;
            if ($stateParams.type == 'in')
            {
                createOrderParam.type = 'inWard';
                createOrderParam.comments = " ";
                createOrderParam.ref_no = " ";
            }
            if ($stateParams.type == 'out')
            {
                createOrderParam.type = 'outWard';
                createOrderParam.comments = $scope.orderListModel.comments;
                createOrderParam.ref_no = $scope.orderListModel.ref_no;
            }
            for (var i = 0; i < $scope.orderListModel.list.length; i++)
            {
                var ordereditems = {};
                ordereditems.product_id = $scope.orderListModel.list[i].id;
                ordereditems.customer_id = $scope.orderListModel.customerInfo.id;
                ordereditems.product_sku = $scope.orderListModel.list[i].sku;
                ordereditems.barcode = $scope.orderListModel.list[i].barcode;
                ordereditems.bcn_id = $scope.orderListModel.list[i].bcn_id;
                ordereditems.ref_id = $scope.orderListModel.list[i].ref_id;
                if ($rootScope.appConfig.sales_product_list.toUpperCase() == 'PURCHASE BASED')
                {
                    ordereditems.ref_type = 'purchase_based';
                } else {
                    ordereditems.ref_type = 'product_based';
                }
                ordereditems.mrp = $scope.orderListModel.list[i].mrp_price;
                ordereditems.selling_price = $scope.orderListModel.list[i].sales_price;
                ordereditems.purchase_price = $scope.orderListModel.list[i].purchase_price;
                if ($scope.orderListModel.list[i].productname.name != null && $scope.orderListModel.list[i].productname.name != '')
                {
                    ordereditems.product_name = $scope.orderListModel.list[i].productname.name;
                } else
                {
                    ordereditems.product_name = $scope.orderListModel.list[i].productname;
                }
//                ordereditems.unit_price = parseFloat($scope.orderListModel.list[i].sales_price);
                ordereditems.given_qty = parseFloat($scope.orderListModel.list[i].qty).toFixed($scope.quantityFormat);
                ordereditems.balance_qty = parseFloat($scope.orderListModel.list[i].qty).toFixed($scope.quantityFormat);
                if (typeof $scope.orderListModel.list[i].uomInfo != undefined && $scope.orderListModel.list[i].uomInfo != null && $scope.orderListModel.list[i].uomInfo != '') {
                    if ($scope.orderListModel.list[i].uomInfo.name != null && $scope.orderListModel.list[i].uomInfo.name != '')
                    {
                        ordereditems.uom_name = $scope.orderListModel.list[i].uomInfo.name;
                    }
                    
                }
                ordereditems.uom_id = $scope.orderListModel.list[i].uomid;
                //ordereditems.uom_name = '';
                //ordereditems.uom_id = '';
                createOrderParam.detail.push(ordereditems);
            }
            var configOption = adminService.handleBothSuccessAndErrorResponseConfig;
            adminService.saveDeliveryNote(createOrderParam, configOption, headers).then(function (response) {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $scope.isDataSavingProcess = false;
                    $state.go('app.deliverynote');
                } else
                {
                    $scope.isDataSavingProcess = false;
                    var element = document.getElementById("btnLoad");
                    element.classList.remove("btn-loader");
                }
            }).catch(function (response) {
                console.error('Error occurred:', response.status, response.data);
                $scope.isDataSavingProcess = false;
                var element = document.getElementById("btnLoad");
                element.classList.remove("btn-loader");
            });
        }

    }]);




