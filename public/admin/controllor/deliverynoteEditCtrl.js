
app.controller('deliverynoteEditCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', 'sweet', function ($scope, $rootScope, adminService, $httpService, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST, sweet) {

        $scope.orderListModel =
                {
                    "id": 0,
                    "productname": "",
                    "sales_price": "",
                    "qty": "",
                    "rowtotal": "",
                    "isActive": "1",
                    "total": 0,
                    "comments": "",
                    "ref_no": "",
                    "subtotal": 0,
                    list: [],
                    "customerInfo": "",
                    "invoiceDetail": {},
                    "isLoadingProgress": false,
                    "type": '',
                    customerIconInfo: [],
                    customerInfoLoadingProgress: false
                }
        $scope.adminService = adminService;
        $scope.validationFactory = ValidationFactory;
        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function ()
        {
            if (typeof $scope.delivery_edit_form != 'undefined' && typeof $scope.delivery_edit_form.$pristine != 'undefined' && !$scope.delivery_edit_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function ()
        {
            $scope.delivery_edit_form.$setPristine();
            $scope.orderListModel.invoiceDetail = {};
            $scope.orderListModel.list = [];
            $scope.getDeliveryInfo();
        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.resetOrderCreate = function () {

            $scope.orderListModel.user = '';
        }
        $scope.validateDateFilterData = function ()
        {
            if ($scope.orderListModel.type == "inWard")
            {
                var retVal = true;
                var minDate = $scope.orderListModel.billdate;
                var value = $scope.outWardDate;
                if (minDate != null && value != null)
                {
                    var minDateValue = moment(minDate).valueOf();
                    var value = moment(value).valueOf();
                    if (value > minDateValue)
                    {
                        retVal = false;
                    }
                    return retVal;
                }
            } else
            {
                var retVal = true;
                return retVal;
            }
        };
        $scope.initTableFilterTimeoutPromise = null;
        $scope.deletedProductList = [];
        $scope.undeletedProductList = [];
        $scope.showbillPopup = false;
        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }
        $scope.maxDate = $scope.maxDate ? null : new Date();
        $scope.startDateOpen = false;
        $scope.dueDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currencyFormat = $rootScope.appConfig.currency;
        $scope.quantityFormat = $rootScope.appConfig.qty_format;
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.currentDate = new Date();
        $scope.quoteEditId = $stateParams.id;

        $scope.openDate = function (index) {

            if (index == 0)
            {
                $scope.startDateOpen = true;
            } else if (index == 1)
            {
                $scope.dueDateOpen = true;
            }
        }

        $scope.closePopup = function () {
            $scope.showpopup = false;
        }

        $scope.getCustomerList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            autosearchParam.mode = 1;
            return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
//                var hits = [];
//                if (data.length > 0)
//                {
//                    for (var i = 0; i < data.length; i++)
//                    {
//                        if (data[i].is_sale == '1' || data[i].is_sale == 1)
//                        {
//                            hits.push(data[i]);
//                        }
//                    }
//                }
//                if (hits.length > 10)
//                {
//                    hits.splice(10, hits.length);
//                }
                return hits;
            });
        };

        $scope.formatCustomerModel = function (model)
        {
            if (model != null)
            {
                if (model.fname != undefined && model.phone != undefined && model.email != undefined)
                {
                    return model.fname + '(' + model.phone + ',' + model.email + ')';
                } else if (model.fname != undefined && model.phone != undefined)
                {
                    return model.fname + '(' + model.phone + ')';
                } else
                {
                    return model.fname;
                }
            }
            return  '';
        };

        $scope.totalcalculationTimeoutPromise != null
        $scope.updateInvoiceTotal = function ()
        {
            if ($scope.totalcalculationTimeoutPromise != null)
            {
                $timeout.cancel($scope.totalcalculationTimeoutPromise);
            }
            $scope.totalcalculationTimeoutPromise = $timeout($scope.calculatetotal, 300);
        }

        $scope.initUpdateDetailTimeoutPromise = null;

        $scope.initUpdateDetail = function ()
        {

            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
            }

            if ($scope.isLoadedTax)
            {
                $scope.updateInvoiceDetails();
            } else
            {
                $scope.initUpdateDetailTimeoutPromise = $timeout($scope.initUpdateDetail, 300);
            }
        }

        $scope.updateInvoiceDetails = function ( )
        {
            $scope.dateFormat = $rootScope.appConfig.date_format;
            $scope.orderListModel.customerInfo = {};
            $scope.orderListModel.customerInfo.id = $scope.orderListModel.invoiceDetail.customer_id;
            $scope.orderListModel.customerInfo.fname = $scope.orderListModel.invoiceDetail.customer_name;
            $scope.orderListModel.type = $scope.orderListModel.invoiceDetail.type;
            $scope.showIconInfo();
            $scope.orderListModel.comments = " ";
            $scope.orderListModel.ref_no = "";
            if (typeof $scope.orderListModel.invoiceDetail.ref_no != "undefined" && $scope.orderListModel.invoiceDetail.ref_no != " " && $scope.orderListModel.invoiceDetail.ref_no != null)
            {
                $scope.orderListModel.ref_no = $scope.orderListModel.invoiceDetail.ref_no
            }
            if (typeof $scope.orderListModel.invoiceDetail.comments != "undefined" && $scope.orderListModel.invoiceDetail.comments != " " && $scope.orderListModel.invoiceDetail.comments != null)
            {
                $scope.orderListModel.comments = $scope.orderListModel.invoiceDetail.comments
            }
            $scope.orderListModel.subtotal = $scope.orderListModel.invoiceDetail.subtotal;
            var idate = utilityService.parseStrToDate($scope.orderListModel.invoiceDetail.date);
            $scope.orderListModel.billdate = idate;
            var duedate = utilityService.parseStrToDate($scope.orderListModel.invoiceDetail.validuntil);
            $scope.orderListModel.duedate = duedate;
            var loop;
            for (loop = 0; loop < $scope.orderListModel.invoiceDetail.detail.length; loop++)
            {
                var newRow =
                        {
                            "id": $scope.orderListModel.invoiceDetail.detail[loop].product_id,
                            "name": $scope.orderListModel.invoiceDetail.detail[loop].product_name,
                            "code": $scope.orderListModel.invoiceDetail.detail[loop].barcode,
                            "bcn_id": $scope.orderListModel.invoiceDetail.detail[loop].bcn_id,
                            "sku": $scope.orderListModel.invoiceDetail.detail[loop].product_sku,
                            "selling_price": $scope.orderListModel.invoiceDetail.detail[loop].selling_price,
                            "qty": $scope.orderListModel.invoiceDetail.detail[loop].given_qty,
                            "rowtotal": $scope.orderListModel.invoiceDetail.detail[loop].total_price,
                            "mrp_price": $scope.orderListModel.invoiceDetail.detail[loop].mrp_price,
                            "uomInfo": {
                                name: $scope.orderListModel.invoiceDetail.detail[loop].uom_name,
                                id: $scope.orderListModel.invoiceDetail.detail[loop].uom_id,
                            },
                            "purchase_price": $scope.orderListModel.invoiceDetail.detail[loop].purchase_price,
                            "ref_id": $scope.orderListModel.invoiceDetail.detail[loop].ref_id,
                            "ref_type": $scope.orderListModel.invoiceDetail.detail[loop].ref_type,

                        };
                $scope.orderListModel.list.push(newRow);
                $scope.checkProductInfo($scope.orderListModel.list[loop], loop, "update");
                $scope.orderListModel.list[loop].productname = $scope.orderListModel.list[loop];
            }
            $scope.orderListModel.isLoadingProgress = false;
        }

        $scope.getItemList = function (val)
        {
            if ($scope.orderListModel.customerInfo != '' && $scope.orderListModel.customerInfo != undefined && $scope.orderListModel.customerInfo != null && $scope.orderListModel.customerInfo.id != undefined)
            {
                var autosearchParam = {};
                autosearchParam.search = val;
                autosearchParam.is_active = 1;
                return $httpService.get(APP_CONST.API.PRODUCT_BASED_LIST, autosearchParam, false).then(function (responseData)
                {
                    var data = responseData.data.list;
                    var hits = [];
                    if (data.length > 0)
                    {
                        for (var i = 0; i < data.length; i++)
                        {
                            if (data[i].is_sale == '1' || data[i].is_sale == 1)
                            {
                                hits.push(data[i]);
                            }
                        }
                    }
                    if (hits.length > 10)
                    {
                        hits.splice(10, hits.length);
                    }
                    return hits;
                });
            } else
            {
                sweet.show("Oops", "Please select customer", "error");
                return;
            }
        };

        $scope.formatProductModel = function (model, index)
        {
            if (model != null && typeof model != 'undefined' && model != '')
            {
                $scope.updateProductInfo(model, index);
                return model.code + '(' + model.name + ')';
            }
            return  '';
        }
        $scope.updateProductInfo = function (model, index)
        {
            if ($scope.orderListModel.list.length != 0)
            {
                if (typeof model == 'undefined')
                    return;

                $scope.orderListModel.list[index].id = model.id;
                $scope.orderListModel.list[index].productname.name = model.name;
                $scope.orderListModel.list[index].sales_price = model.selling_price;
                $scope.orderListModel.list[index].received_qty = model.received_qty;
                $scope.orderListModel.list[index].sku = model.sku;
                if (model.qty != null && model.qty != '')
                {
                    $scope.orderListModel.list[index].qty = model.qty;
                } else
                {
                    if ($scope.orderListModel.list[index].qtyCheck == false)
                    {
                        $scope.orderListModel.list[index].qty = 1;
                    }
                }
                $scope.orderListModel.list[index].uomInfo = {
                    id: model.uomInfo.id,
                    name: model.uomInfo.name
                }
                if (model.uomid != undefined)
                {
                    $scope.orderListModel.list[index].uomid = model.uomid;
                }
                if (model.uom_id != undefined)
                {
                    $scope.orderListModel.list[index].uomid = model.uom_id;
                }
                if (model.uomid != undefined && model.uom_id != undefined)
                {
                    $scope.orderListModel.list[index].uomid = "";
                }
                $scope.orderListModel.list[index].rowtotal = model.selling_price;
                $scope.orderListModel.list[index].barcode = model.code;
                $scope.orderListModel.list[index].bcn_id = model.bcn_id;
                $scope.orderListModel.list[index].mrp_price = model.mrp_price;
                $scope.orderListModel.list[index].purchase_price = model.purchase_price;
                $scope.orderListModel.list[index].ref_id = model.ref_id;
                if ($rootScope.appConfig.sales_product_list.toUpperCase() == 'PURCHASE BASED')
                {
                    $scope.orderListModel.list[index].ref_type = 'purchase_based';
                } else
                {
                    $scope.orderListModel.list[index].ref_type = 'product_based';
                }
                $scope.updateInvoiceTotal();
                return;
            }
        }
        $scope.initvalidateProductDataAndAddNewPromise = null;

        $scope.formValidator = function ()
        {
            //performace issue in form validator bug
            if ($scope.delivery_edit_form.$submitted)
            {
                $timeout(function () {
                    $scope.delivery_edit_form.$submitted = false;
                }, 0);
                $timeout(function () {
                    $scope.delivery_edit_form.$setSubmitted();
                }, 100);
            } else
            {
                $timeout(function () {
                    $scope.delivery_edit_form.$setSubmitted();
                }, 100);
            }
        }

        $scope.addNewProduct = function ()
        {
            var newRow = {
                "id": '',
                "productName": '',
                "sales_price": '',
                "qty": '',
                "rowtotal": '',
                "limit": 0,
                "qtyCheck": false
            };
            $scope.orderListModel.list.push(newRow);
        }
        $scope.deleteEvent = false;
        $scope.deleteProduct = function (index)
        {
            $scope.orderListModel.list.splice(index, 1);
            $scope.calculatetotal();
            $scope.deleteEvent = true;
        };
        $scope.showInfoIcon = false;
        $scope.showIconInfo = function (type)
        {
            if (type == "change")
            {
                $scope.orderListModel.list = [];
                $scope.addNewProduct();
                if (typeof $scope.orderListModel.customerInfo != undefined && $scope.orderListModel.customerInfo != '' && $scope.orderListModel.type.toLowerCase() == "inward")
                {
                    $scope.getCustomerProductInfo();
                }
            }
            if (typeof $scope.orderListModel.customerInfo != undefined && $scope.orderListModel.customerInfo != '' && $scope.orderListModel.type.toLowerCase() == "inward")
            {
                $scope.showInfoIcon = true;
                //$scope.getCustomerProductInfo ();
            } else
            {
                $scope.showInfoIcon = false;
            }
        }
        $scope.closeCusInfoPopup = function ()
        {
            $scope.showcustomerinfopopup = false;
        }
        $scope.getCustomerProductInfo = function (model, index)
        {
            $scope.orderListModel.customerInfoLoadingProgress = true;
            var getCustomerInfoParam = {};
            var headers = {};
            headers['screen-code'] = 'deliverynote';
            getCustomerInfoParam.customer_id = $scope.orderListModel.customerInfo.id;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getBalanceQty(getCustomerInfoParam, configOption, headers).then(function (response) {
                if (response.data.success == true)
                {
                    if (response.data.list.length > 0)
                    {
                        $scope.orderListModel.customerIconInfo = [];
                        $scope.orderListModel.customerIconInfo = response.data.list;
                        $scope.orderListModel.customerInfoLoadingProgress = false;
                        if (model != '' && typeof model != "undefined" && model != null)
                        {
                            $scope.checkProductInfo(model, index);
                        }
                    } else
                    {
                        sweet.show('Oops...', "Selected customer doesn't have any outwards", 'error');
                        $scope.orderListModel.customerInfoLoadingProgress = false;
                        $scope.orderListModel.customerInfo.id = $scope.orderListModel.invoiceDetail.customer_id;
                        $scope.orderListModel.customerInfo.fname = $scope.orderListModel.invoiceDetail.customer_name;
                    }
                }
            });
        };
        $scope.checkProductInfo = function (model, index, value)
        {
            if (value == "update")
            {
                $scope.getCustomerProductInfo(model, index);
                return;
            }
            if ($scope.orderListModel.customerIconInfo.length > 0)
            {
                var found = false;
                $scope.balance = 0;
                for (var i = 0; i < $scope.orderListModel.customerIconInfo.length; i++)
                {
                    if ($scope.orderListModel.customerIconInfo[i].product_id == model.id)
                    {
                        found = true;
                        $scope.balance = $scope.orderListModel.customerIconInfo[i].balance_qty;
                        $scope.outWardDate = $scope.orderListModel.customerIconInfo[i].outward_date;
                    }
                }
                if (found == true)
                {
                    $scope.orderListModel.list[index].qtyCheck = false;
                    $scope.orderListModel.list[index].limit = parseFloat($scope.balance).toFixed($scope.quantityFormat);
                } else
                {
                    $scope.orderListModel.list[index].qty = 0;
                    $scope.orderListModel.list[index].qtyCheck = true;
                }

            } else
            {
                $scope.orderListModel.list[index].qty = 0;
                $scope.orderListModel.list[index].qtyCheck = true;
            }
        }
        $scope.showCusInfoPopup = function ()
        {
            $scope.showcustomerinfopopup = true;
        }
        $scope.custDrop = function () {
            if ($scope.orderListModel.customerInfo.id == undefined) {
                $scope.orderListModel.customerInfo = '';
            }
        };

        $scope.calculatetotal = function ()
        {
            var subTotal = 0;
            console.log("bfr loop", $scope.orderListModel.list.length);
            for (var i = 0; i < $scope.orderListModel.list.length; i++)
            {
                $scope.orderListModel.list[i].rowtotal = $scope.orderListModel.list[i].sales_price * $scope.orderListModel.list[i].qty;
                subTotal += parseFloat($scope.orderListModel.list[i].rowtotal);
                $scope.orderListModel.list[i].rowtotal = $scope.orderListModel.list[i].rowtotal.toFixed(2);
            }
            $scope.orderListModel.subtotal = parseFloat(subTotal).toFixed(2);
            $scope.orderListModel.overall_selling_price = $scope.orderListModel.subtotal;
            $scope.updateRoundoff();
        }

        $scope.updateRoundoff = function ()
        {
            $scope.round = Math.round($scope.orderListModel.subtotal);
            $scope.orderListModel.roundoff = $scope.round - parseFloat($scope.orderListModel.subtotal);
            $scope.orderListModel.roundoff = parseFloat($scope.orderListModel.roundoff).toFixed(2);
            $scope.round = parseFloat($scope.round).toFixed(2);
        }

        $scope.getDeliveryInfo = function ( )
        {
            $scope.orderListModel.isLoadingProgress = true;
            var getListParam = {};
            getListParam.id = $stateParams.id;
            getListParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getDeliveryDetail(getListParam, configOption).then(function (response)
            {
                var data = response.data.data;
                $scope.orderListModel.invoiceDetail = data;
                $scope.updateInvoiceDetails();
            });
        }

        $scope.getDeliveryInfo( );
        $scope.updateProductInfo();
        $scope.updateOrder = function (forceSave)
        {
            if (!forceSave)
            {
                if ($scope.isDataSavingProcess)
                {
                    return;
                }
            }
            $scope.isDataSavingProcess = true;
            $scope.isSave = false;
            for (var i = 0; i < $scope.orderListModel.list.length; i++)
            {

                if ($scope.orderListModel.list[i].productname != '' && $scope.orderListModel.list[i].productname != null &&
                        $scope.orderListModel.list[i].qty != '' && $scope.orderListModel.list[i].qty != null &&
                        $scope.orderListModel.list[i].sales_price != '' && $scope.orderListModel.list[i].sales_price != null &&
                        $scope.orderListModel.list[i].rowtotal != '' && $scope.orderListModel.list[i].rowtotal != null)
                {
                    $scope.isSave = true;
                } else
                {
                    $scope.isSave = false;
                    if ($scope.orderListModel.list[i].qty == 0 && $scope.orderListModel.list[i].rowtotal == "0.00")
                    {
                        $scope.isSave = true;
                    }

                }


            }

            if ($scope.isSave)
            {
                $scope.modifyOrder();

            } else
            {
                sweet.show('Oops...', 'Fill Product Detail.. ', 'error');
                $scope.isDataSavingProcess = false;
                var element = document.getElementById("btnLoad");
                element.classList.remove("btn-loader");
            }

        };
        
        $scope.getUomList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;

            return $httpService.get(APP_CONST.API.UOM, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatUomModel = function (model)
        {
            if (model != null)
            {
                return model.name;
            }
            return  '';
        };
        
        $scope.modifyOrder = function ()
        {
            var updateOrderParam = {};
            var headers = {};
            headers['screen-code'] = 'deliverynote';
            if (typeof $scope.orderListModel.billdate == 'object')
            {
                var billdate = utilityService.parseDateToStr($scope.orderListModel.billdate, 'yyyy-MM-dd');
            }
            updateOrderParam.date = utilityService.changeDateToSqlFormat(billdate, 'yyyy-MM-dd');
            updateOrderParam.detail = [];
            updateOrderParam.customer_id = $scope.orderListModel.customerInfo.id;
            updateOrderParam.overall_selling_price = $scope.orderListModel.overall_selling_price;
            updateOrderParam.type = $scope.orderListModel.type;
            updateOrderParam.comments = $scope.orderListModel.comments;
            updateOrderParam.ref_no = $scope.orderListModel.ref_no;
            var overall_qty = 0;
            for (var i = 0; i < $scope.orderListModel.list.length; i++)
            {
                if ($scope.orderListModel.list[i].productname != undefined && $scope.orderListModel.list[i].productname != '' && $scope.orderListModel.list[i].productname != null && $scope.orderListModel.list[i].productname.id != undefined)
                {
                    var ordereditems = {};
                    ordereditems.product_id = $scope.orderListModel.list[i].id;
                    ordereditems.product_sku = $scope.orderListModel.list[i].sku;
                    ordereditems.customer_id = $scope.orderListModel.customerInfo.id;
                    if (typeof $scope.orderListModel.list[i].productname != undefined && $scope.orderListModel.list[i].productname != null && $scope.orderListModel.list[i].productname != '')
                    {
                        if ($scope.orderListModel.list[i].productname.name != null && $scope.orderListModel.list[i].productname.name != '')
                        {
                            ordereditems.product_name = $scope.orderListModel.list[i].productname.name;
                        } else
                        {
                            ordereditems.product_name = $scope.orderListModel.list[i].productname;
                        }
                    }
                    ordereditems.ref_id = $scope.orderListModel.list[i].ref_id;
                    ordereditems.ref_type = $scope.orderListModel.list[i].ref_type;
                    ordereditems.given_qty = parseFloat($scope.orderListModel.list[i].qty).toFixed($scope.quantityFormat);
                    ordereditems.balance_qty = parseFloat($scope.orderListModel.list[i].qty).toFixed($scope.quantityFormat);
                    ordereditems.mrp = parseFloat($scope.orderListModel.list[i].mrp_price);
                    ordereditems.purchase_price = parseFloat($scope.orderListModel.list[i].purchase_price);
                    ordereditems.selling_price = parseFloat($scope.orderListModel.list[i].sales_price);
                    if (typeof $scope.orderListModel.list[i].uomInfo != undefined && $scope.orderListModel.list[i].uomInfo != null && $scope.orderListModel.list[i].uomInfo != '')
                    {
                        if ($scope.orderListModel.list[i].uomInfo.name != null && $scope.orderListModel.list[i].uomInfo.name != '')
                        {
                            ordereditems.uom_name = $scope.orderListModel.list[i].uomInfo.name;
                            ordereditems.uom_id = $scope.orderListModel.list[i].uomInfo.id;
                        } else
                        {
                            ordereditems.uom_name = '';
                            ordereditems.uom_id = '';
                        }
                    } else
                    {
                        ordereditems.uom_name = '';
                        ordereditems.uom_id = '';
                    }
                    ordereditems.bcn_id = $scope.orderListModel.list[i].bcn_id;
                    ordereditems.barcode = $scope.orderListModel.list[i].barcode;
                    updateOrderParam.detail.push(ordereditems);
                    overall_qty += parseFloat($scope.orderListModel.list[i].qty);
                }

            }
            updateOrderParam.overall_qty = parseFloat(overall_qty).toFixed($scope.quantityFormat);

            adminService.editDeliveryNoteDetails(updateOrderParam, $stateParams.id, headers).then(function (response)
            {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.deliverynote');
                } else {
                    $scope.isDataSavingProcess = false;
                    var element = document.getElementById("btnLoad");
                    element.classList.remove("btn-loader");
                }
            }).catch(function (response) {
                console.error('Error occurred:', response.status, response.data);
                $scope.isDataSavingProcess = false;
                var element = document.getElementById("btnLoad");
                element.classList.remove("btn-loader");
            });
        }
    }]);