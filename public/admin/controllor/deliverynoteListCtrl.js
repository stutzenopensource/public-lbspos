app.controller('deliverynoteListCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$filter', 'Auth', '$timeout', '$httpService', '$localStorage', 'APP_CONST', 'ValidationFactory', function ($scope, $rootScope, adminService, utilityService, $filter, Auth, $timeout, $httpService, $localStorage, APP_CONST, ValidationFactory) {
        $rootScope.getNavigationBlockMsg = null;
        $scope.deliverynoteModel = {
            currentPage: 1,
            total: 0,
            limit: 4,
            list: [],
            status: '',
            isLoadingProgress: true
        };
        $scope.validationFactory = ValidationFactory;
        $scope.pagePerCount = [50, 100];
        $scope.deliverynoteModel.limit = $scope.pagePerCount[0];
        $scope.adminService = adminService;
        $scope.searchFilter =
                {
                    id: '',
                    todate: '',
                    fromdate: '',
                    customerInfo: {},
                    barcode : '',
                    sku : '',
                    type:''
                };
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.openDate = function (index)
        {
            if (index == 0)
            {
                $scope.fromDateOpen = true;
            } else if (index == 1)
            {
                $scope.toDateOpen = true;
            }
        }
        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getdeliveryNoteList, 300);
        }
        $scope.refreshScreen = function ()
        {
            $scope.searchFilter =
                    {
                        id: '',
                        todate: '',
                        fromdate: '',
                        customerInfo: {},
                        barcode : '',
                        sku : '',
                        type:''
                    };
            $scope.initTableFilter();
            $scope.noResults = false;
        }
        $scope.selectedId = '';
        $scope.showdeletePopup = false;
        $scope.isdeleteProgress = false;
        $scope.showPopup = function (id)
        {
            $scope.selectedId = id;
            $scope.showdeletePopup = true;
        };
        $scope.validateDateFilterData = function ()
        {
            var retVal = false;
            if ($scope.searchFilter.todate !== '' || $scope.searchFilter.fromdate !== '')
            {
                retVal = true;
            }
            return retVal;
        };
        $scope.closePopup = function ()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteProgress = false;
        };
        $scope.deleteInvoiceItem = function ()
        {
            if ($scope.isdeleteProgress == false)
            {
                $scope.isdeleteProgress = true;

                var getListParam = {};
                getListParam.id = $scope.selectedId;
                var headers = {};
                headers['screen-code'] = 'deliverynote';
                adminService.deleteDeliverynote(getListParam, getListParam.id, headers).then(function (response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getdeliveryNoteList();
                    }
                    $scope.isdeleteProgress = false;
                });
            }
        };
//        $scope.$watch('searchFilter.customerInfo', function (newVal, oldVal)
//        {
//            if (typeof newVal == 'undefined' || newVal == '')
//            {
//                $scope.initTableFilter();
//            }
//        });
        $scope.getCustomerList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            autosearchParam.mode = 1;
            if (autosearchParam.search != '')
            {
                return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function (responseData)
                {
                    var data = responseData.data.list;
                    var hits = data;
                    if (hits.length > 10)
                    {
                        hits.splice(10, hits.length);
                    }
                    return hits;
                });
            }
        };
        $scope.formatCustomerModel = function (model)
        {
            if (model != null)
            {
                if (model.fname != undefined && model.phone != undefined && model.email != undefined)
                {
                    return model.fname + '(' + model.phone + ',' + model.email + ')';
                } else if (model.fname != undefined && model.phone != undefined)
                {
                    return model.fname + '(' + model.phone + ')';
                }
            }
            return  '';
        };
        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'delivery_list_form';
        $scope.$on('onLocalStorageFormRenterCompleteEvent', function (event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });
        $scope.$on('onLocalStorageFieldRenterCompleteEvent', function (event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRenterCount++;
                }
                if ($scope.localStorageRenterCount >= $scope.localStorageCount)
                {
                    console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                    $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
                }
            }
        });
        $scope.$on('updateTypeaheadFieldValue', function (event, data) {

            if (data.fieldName == "delivery_list_form-customer_dropdown")
            {
                $scope.searchFilter.customerInfo = data.value;
            }
        });
        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function (event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRetrieveCount++;
                }
                if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
                {
                    $scope.initTableFilter();
                }
                console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);
            }
        });

        $scope.init = function ()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');

        };

        $scope.$on('onLocalStorageReadyEvent', function () {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                $timeout(function () {
                    $scope.init();
                }, 300);

            }
        });
        $scope.getdeliveryNoteList = function ()
        {
            $scope.deliverynoteModel.isLoadingProgress = true;
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'deliverynote';
            var configOption = adminService.handleOnlyErrorResponseConfig;
            if ($scope.searchFilter.customerInfo != null && typeof $scope.searchFilter.customerInfo != 'undefined' && typeof $scope.searchFilter.customerInfo.id != 'undefined')
            {
                getListParam.customer_id = $scope.searchFilter.customerInfo.id;
            } else
            {
                getListParam.customer_id = '';
            }
            if ($scope.searchFilter.fromdate != null && $scope.searchFilter.fromdate != '')
            {
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
            }

            if ($scope.searchFilter.todate != null && $scope.searchFilter.todate != '' && $scope.searchFilter.todate != null && $scope.searchFilter.toDate != '')
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
            }
            getListParam.start = ($scope.deliverynoteModel.currentPage - 1) * $scope.deliverynoteModel.limit;
            getListParam.barcode = $scope.searchFilter.barcode;
            getListParam.type = $scope.searchFilter.type;
            getListParam.sku = $scope.searchFilter.sku;
            getListParam.limit = $scope.deliverynoteModel.limit;
            getListParam.is_active = 1;
            adminService.getDeliveryNoteList(getListParam, configOption, headers).then(function (response)
            {
                var data = response.data;
                if (data.success)
                {
                    $scope.deliverynoteModel.list = data.list;
                    if ($scope.deliverynoteModel.list.length > 0)
                    {
                        for (var i = 0; i < $scope.deliverynoteModel.list.length; i++)
                        {
                            $scope.deliverynoteModel.list[i].newdate = utilityService.parseStrToDate($scope.deliverynoteModel.list[i].date);
                            $scope.deliverynoteModel.list[i].date = utilityService.parseDateToStr($scope.deliverynoteModel.list[i].newdate, $scope.adminService.appConfig.date_format);
                        }
                    }
                    $scope.deliverynoteModel.total = data.total;
                }
                $scope.deliverynoteModel.isLoadingProgress = false;
            });
        };
        $scope.init();
        //$scope.getdeliveryNoteList();
    }]);




