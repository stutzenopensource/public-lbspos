app.controller('deliverynoteReportCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', function ($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.deliveryNoteModel = {
            currentPage: 1,
            total: 0,
            total_Amount: 0,
            limit: 10,
            list: [],
            length: [],
            serverList: null,
            isLoadingProgress: false,
            isSearchLoadingProgress: false,
            total_qty: 0
        };
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.deliveryNoteModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat);
        $scope.validationFactory = ValidationFactory;
        $scope.checked = false;
        $scope.showCategory = false;
        $scope.openDate = function (index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            } else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };

        $scope.searchFilter = {
            category_id: '',
            fromdate: '',
            todate: '',
            productInfo: ''
        };
        $scope.clearFilters = function ()
        {
            $scope.searchFilter = {
                category_id: '',
                fromdate: '',
                todate: '',
                productInfo: ''
            };
            $scope.checked = false;
            $scope.showCategory = false;
            $scope.deliveryNoteModel.list = [];
        }

        $scope.print = function ()
        {
            $window.print();
        };

        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.show = function (index)
        {

            if ($scope.showDetails[index])
            {
                $scope.showDetails[index] = false;
            } else
                $scope.showDetails[index] = true;
        };

        $scope.getList = function () {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'empsalesreport';
            getListParam.from_date = '';
            getListParam.to_date = '';
            if ($scope.searchFilter.fromdate != null && $scope.searchFilter.fromdate != '')
            {
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.adminService.appConfig.date_format);
                }
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.adminService.appConfig.date_format);
            }
            if ($scope.searchFilter.todate != null && $scope.searchFilter.todate != '')
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.adminService.appConfig.date_format);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.adminService.appConfig.date_format);
            }
            if ($scope.searchFilter.productInfo != '' && $scope.searchFilter.productInfo != null && $scope.searchFilter.productInfo != undefined && $scope.searchFilter.productInfo.id != '')
            {
                getListParam.product_id = $scope.searchFilter.productInfo.id;
            } else {
                getListParam.product_id = '';
            }
            $scope.deliveryNoteModel.isLoadingProgress = true;
            $scope.showDetails = [];
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getDeliveryNoteReport(getListParam, configOption, headers).then(function (response)
            {
                var total_purchase_price = 0.00;
                var total_selling_price = 0.00;
                var total_balance_qty = 0.00;
                var total_received_qty = 0.00;
                var total_given_qty = 0.00; 
                $scope.deliveryNoteModel.total_purchase_price = 0.00;
                $scope.deliveryNoteModel.total_selling_price = 0.00;
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.deliveryNoteModel.list = data;
                    for (var i = 0; i < $scope.deliveryNoteModel.list.length; i++)
                    {
                        total_purchase_price = parseFloat(total_purchase_price) + parseFloat($scope.deliveryNoteModel.list[i].purchase_price == null ? 0 : $scope.deliveryNoteModel.list[i].purchase_price);
                        total_selling_price = parseFloat(total_selling_price) + parseFloat($scope.deliveryNoteModel.list[i].selling_price == null ? 0 : $scope.deliveryNoteModel.list[i].selling_price);
                        total_balance_qty = parseFloat(total_balance_qty) + parseFloat($scope.deliveryNoteModel.list[i].tot_balance_qty == null ? 0 : $scope.deliveryNoteModel.list[i].tot_balance_qty);
                        total_received_qty = parseFloat(total_received_qty) + parseFloat($scope.deliveryNoteModel.list[i].tot_received_qty == null ? 0 : $scope.deliveryNoteModel.list[i].tot_received_qty);
                        total_given_qty = parseFloat(total_given_qty) + parseFloat($scope.deliveryNoteModel.list[i].tot_given_qty == null ? 0 : $scope.deliveryNoteModel.list[i].tot_given_qty);
                        $scope.deliveryNoteModel.list[i].total_amount = parseFloat($scope.deliveryNoteModel.list[i].total_amount).toFixed(2);
                        $scope.deliveryNoteModel.list[i].total_amount = utilityService.changeCurrency($scope.deliveryNoteModel.list[i].total_amount, $rootScope.appConfig.thousand_seperator);
                        $scope.deliveryNoteModel.list[i].flag = i;
                        $scope.showDetails[i] = false;
                    }
                    $scope.deliveryNoteModel.total_purchase_price = total_purchase_price;
                    $scope.deliveryNoteModel.total_purchase_price = parseFloat($scope.deliveryNoteModel.total_purchase_price).toFixed(2);
                    $scope.deliveryNoteModel.total_purchase_price = utilityService.changeCurrency($scope.deliveryNoteModel.total_purchase_price, $rootScope.appConfig.thousand_seperator);
                    $scope.deliveryNoteModel.total_selling_price = total_selling_price;
                    $scope.deliveryNoteModel.total_selling_price = parseFloat($scope.deliveryNoteModel.total_selling_price).toFixed(2);
                    $scope.deliveryNoteModel.total_selling_price = utilityService.changeCurrency($scope.deliveryNoteModel.total_selling_price, $rootScope.appConfig.thousand_seperator);
                    $scope.deliveryNoteModel.total_balance_qty = total_balance_qty;
                    $scope.deliveryNoteModel.total_balance_qty = parseFloat($scope.deliveryNoteModel.total_balance_qty);
                    $scope.deliveryNoteModel.total_given_qty = total_given_qty;
                    $scope.deliveryNoteModel.total_given_qty = parseFloat($scope.deliveryNoteModel.total_given_qty);
                    $scope.deliveryNoteModel.total_received_qty = total_received_qty;
                    $scope.deliveryNoteModel.total_received_qty = parseFloat($scope.deliveryNoteModel.total_received_qty);
                }
                $scope.deliveryNoteModel.isLoadingProgress = false;
            });
        };
        $scope.getProductList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.PRODUCT_LIST, autosearchParam,false).then(function (responseData) {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatProductModel = function (model)
        {
            if (model != null && model != undefined && model != '')
            {
                return model.name;
            }
            return  '';
        };

        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'delivery_note_report_form';
        $scope.$on('onLocalStorageFormRenterCompleteEvent', function (event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });

        $scope.$on('onLocalStorageFieldRenterCompleteEvent', function (event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRenterCount++;
                }
                if ($scope.localStorageRenterCount >= $scope.localStorageCount)
                {
                    console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                    $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
                }
            }
        });

        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function (event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRetrieveCount++;
                }
                if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
                {
                    $scope.initTableFilter();
                }
                console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);
            }
        });
        $scope.$on('updateTypeaheadFieldValue', function (event, data) {

            if (data.fieldName == "delivery_note_report_form-product_dropdown")
            {
                $scope.searchFilter.productInfo = data.value;
            }
        });

        $scope.init = function ()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');
        };

        $scope.$on('onLocalStorageReadyEvent', function () {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                $timeout(function () {
                    $scope.init();
                }, 300);
            }
        });
        //$scope.getList();
    }]);




