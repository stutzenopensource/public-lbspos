
app.controller('quoteViewCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', 'sweet', function ($scope, $rootScope, adminService, $httpService, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST, sweet) {

        $scope.orderListModel =
                {
                    "id": 0,
                    "productname": "",
                    "sku": "",
                    "unit": "",
                    "mrp": "",
                    "sales_price": "",
                    "qty": "",
                    "rowtotal": "",
                    "isActive": "1",
                    "total": 0,
                    "amountinwords": '',
                    "customername": "",
                    "phone": "",
                    "addressId": "",
                    "subtotal": 0,
                    "taxAmt": 0,
                    "taxInfo": "",
                    "discountAmt": 0,
                    list: [],
                    "billno": "",
                    "billdate": "",
                    "coupon": "",
                    "shipping": 0,
                    "discamount": 0,
                    "code": "",
                    "customerInfo": "",
                    "invoiceDetail": {},
                    "paymentList": {},
                    //"paymenttype": "CASH",
                    "couponcode": "",
                    "invoicePrefix": "",
                    "paymentTerm": "",
                    "taxPercentage": "",
                    "discountPercentage": "",
                    "totalDisPercentage": "0",
                    "totalTaxPercentage": "",
                    "discountamount": "",
                    "taxamount": "",
                    "cardDetail": "",
                    "duedate": "",
                    "tax_id": "",
                    "balance_amount": "",
                    "advance_amount": "",
                    "isLoadingProgress": false,
                    "dueAmount": '',
                    "status": "",
                    "created_at": "",
                    "accounts": '',
                    "accountsList": [],
                    "paymentlist": [],
                    "payment_mode": "",
                    "paymentdefault": ""
                }

        $scope.currentDate = new Date();
        $scope.stateParamData = $stateParams.id;
        $scope.colsPan = $rootScope.appConfig.uomdisplay;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.qtyFormat = $rootScope.appConfig.qty_format;
        $scope.currencyFormat = $rootScope.appConfig.currency;
        $scope.companyname_inprint = $rootScope.appConfig.companynameinprint;
        //$scope.orderListModel.billdate = $filter('date')($scope.currentDate, $scope.dateFormat);
        $scope.validationFactory = ValidationFactory;
        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function ()
        {
            if (typeof $scope.order_view_form != 'undefined' && typeof $scope.order_view_form.$pristine != 'undefined' && !$scope.order_view_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.isDataDeletingProcess = false;
        $scope.formReset = function ()
        {
            $scope.order_view_form.$setPristine();
            $scope.orderListModel.invoiceDetail = {};
            $scope.orderListModel.list = [];
            $scope.getInvoiceInfo( );
        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.resetOrderCreate = function () {

            $scope.orderListModel.user = '';
        }

        $scope.initTableFilterTimeoutPromise = null;
        $scope.deletedProductList = [];
        $scope.undeletedProductList = [];
        $scope.showbillPopup = false;
        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }
        $scope.maxDate = $scope.maxDate ? null : new Date();
        $scope.startDateOpen = false;
        $scope.dueDateOpen = false;
        $scope.invoiceDateOpen = false;
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.openDate = function (index) {

            if (index == 0)
            {
                $scope.startDateOpen = true;
            } else if (index == 1)
            {
                $scope.dueDateOpen = true;
            } else if (index == 2)
            {
                $scope.invoiceDateOpen = true;
            }
        }
        $scope.paymentAddModel =
                {
                    "id": 0,
                    "account": '',
                    list: [],
                    "comments": " ",
                    "invoicedate": "",
                    "amount": '',
                    "payment_mode": '',
                    "paymentdefault": '',
                    "paymentlist": [],
                    "quote_id": '',
                    "cheque_no": '',
                    "customer_id": '',
                    "categoryList": '',
                    "category": ''
                }

        $scope.reset_payment_form = function ( )
        {
            $scope.paymentAddModel.quote_id = '';
            $scope.paymentAddModel.customer_id = '';
            $scope.paymentAddModel.account = "";
            $scope.paymentAddModel.invoicedate = "";
            $scope.paymentAddModel.payment_mode = '';
            $scope.paymentAddModel.paymentdefault = '';
            $scope.paymentAddModel.amount = "";
            $scope.paymentAddModel.comments = "";
            $scope.showPaymentPopup( );
        }


        $scope.senderEmail = '';
        $scope.receiver = [];
        $scope.ccEmail = '';

        $scope.showSendInvoicePopup = false;
        $scope.showSendInvPopup = function ( )
        {
            $scope.showSendInvoicePopup = true;
            $scope.mailHeader = "Send a Receipt";
            $scope.senderEmail = $scope.orderListModel.customerInfo.email;
            var newRow = {
                "email": $scope.orderListModel.customerInfo.email
            };
            $scope.receiver.push(newRow);
            $scope.ccEmail = $scope.orderListModel.customerInfo.email;
            $scope.subject = "Invoice No " + $scope.orderListModel.invoiceDetail.id + " from company name";
            $scope.sendMsg = "";
            $scope.isAttachInvoicePDF = true;
            $scope.$broadcast("initPopupEvent");
        };

        $scope.addMoreReceiverEmail = function ( )
        {
            var newRow = {
                "email": ""
            };
            $scope.receiver.push(newRow);
        }

        $scope.showpopup = false;
        $scope.showPopup = function (index) {
            var removePaymentAmount;

            $scope.showpopup = true;
            $scope.mailHeader = "Send a Receipt";
            $scope.senderEmail = $scope.orderListModel.customerInfo.email;
            $scope.receiver = [];
            var newRow = {
                "email": $scope.orderListModel.customerInfo.email
            };
            $scope.receiver.push(newRow);
            $scope.receiverEmail = $scope.orderListModel.customerInfo.email;
            $scope.ccEmail = $scope.orderListModel.customerInfo.email;
            $scope.subject = "Payment Receipt for Invoice No " + $scope.orderListModel.invoiceDetail.id;
            for (var loop = 0; loop < $scope.orderListModel.paymentList.length; loop++)
            {
                if ($scope.orderListModel.paymentList[loop].id == index)
                {
                    removePaymentAmount = $scope.orderListModel.paymentList[loop].amount;
                    break;
                }
            }
            $scope.isAttachInvoicePDF = false;
            $scope.sendMsg = "Hi " + "\n" + "Here's your Payment receipt for invoice no " + $scope.orderListModel.invoiceDetail.id + " of payment amount " + removePaymentAmount + ".\n  If you have any questions, Please let me know. \n Thanks";
            //        $scope.subject = "Invoice " + $scope.orderListModel.invoiceDetail.id + "from company name";
            $scope.$broadcast("initPopupEvent");
        };

        $scope.closePopup = function ()
        {
            $scope.showSendInvoicePopup = false;
            $scope.showpopup = false;
            $scope.getInvoiceInfo( );
        }



        $scope.print = function ()
        {
            window.print();
        }



        $scope.totalcalculationTimeoutPromise != null
        $scope.updateInvoiceTotal = function ()
        {
            if ($scope.totalcalculationTimeoutPromise != null)
            {
                $timeout.cancel($scope.totalcalculationTimeoutPromise);
            }
            $scope.totalcalculationTimeoutPromise = $timeout($scope.calculatetotal, 300);
        }

        $scope.initUpdateDetailTimeoutPromise = null;

        $scope.initUpdateDetail = function ()
        {

            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
            }

            if ($scope.isLoadedTax)
            {
                $scope.updateInvoiceDetails();
                $scope.getCustomerInfo();
            } else
            {
                $scope.initUpdateDetailTimeoutPromise = $timeout($scope.initUpdateDetail, 300);
            }
        }

        $scope.updateRoundoff = function ()
        {
            $scope.round = Math.round($scope.orderListModel.total);
            $scope.orderListModel.roundoff = $scope.round - parseFloat($scope.orderListModel.total);
            $scope.orderListModel.roundoff = parseFloat($scope.orderListModel.roundoff).toFixed(2);
            $scope.round = parseFloat($scope.round).toFixed(2);
        }
        $scope.qtyUpdate = function ()
        {

            var updateParam = {};
            updateParam.id = $stateParams.id;
            updateParam.detail = [];
            for (var i = 0; i < $scope.orderListModel.list.length; i++)
            {
                var detailParam = {};
                detailParam.detail_id = $scope.orderListModel.list[i].detail_id;
                detailParam.qty = $scope.orderListModel.list[i].receiving_qty;
                updateParam.detail.push(detailParam);
            }
            var configOption = adminService.handleBothSuccessAndErrorResponseConfig;
            adminService.deliveryQtyUpdate(updateParam, configOption).then(function (response)
            {
                if (response.data.success == true)
                {
                    $scope.getDeliveryInfo();
                }
            });

        }
        $scope.minNumber = function (minValue, value, index) {


            if (isNaN(minValue) || isNaN(value)) {
                $('#received_qty_' + index).addClass('empBoxColor');
                $scope.receivedrequired = true;
            }
            else if (parseFloat(value) > parseFloat(minValue))
            {
                $('#received_qty_' + index).addClass('empBoxColor');
                $scope.receivedrequired = true;
            } else
            {
                $('#received_qty_' + index).removeClass('empBoxColor');
                $scope.receivedrequired = false;
            }


        };
        $scope.updateInvoiceDetails = function ( )
        {
            $scope.dateFormat = $rootScope.appConfig.date_format;
            if ($scope.orderListModel.invoiceDetail != null)
            {
                $scope.orderListModel.customerInfo = {};
                $scope.orderListModel.customerInfo.id = $scope.orderListModel.invoiceDetail.customer_id;
                $scope.orderListModel.customerInfo.fname = $scope.orderListModel.invoiceDetail.customer_name;

                $scope.orderListModel.subtotal = utilityService.changeCurrency($scope.orderListModel.invoiceDetail.subtotal, $rootScope.appConfig.thousand_seperator);
//                $scope.orderListModel.taxAmt = utilityService.changeCurrency($scope.orderListModel.invoiceDetail.tax_amount, $rootScope.appConfig.thousand_seperator);
//                $scope.orderListModel.discountAmt = utilityService.changeCurrency($scope.orderListModel.invoiceDetail.discount_amount, $rootScope.appConfig.thousand_seperator);
//                $scope.orderListModel.total = utilityService.changeCurrency($scope.orderListModel.invoiceDetail.total_amount, $rootScope.appConfig.thousand_seperator);
//                $scope.orderListModel.roundoff = $scope.orderListModel.invoiceDetail.round_off;
//                // $scope.orderListModel.amountinwords = utilityService.convertNumberToWords($scope.orderListModel.invoiceDetail.total_amount);
//                $scope.orderListModel.advance_amount = utilityService.changeCurrency($scope.orderListModel.invoiceDetail.advance_amount, $rootScope.appConfig.thousand_seperator);
//                $scope.orderListModel.advance_amount = parseFloat($scope.orderListModel.advance_amount).toFixed(2);
//                $scope.orderListModel.balance_amount = $scope.orderListModel.invoiceDetail.total_amount - $scope.orderListModel.invoiceDetail.advance_amount;
//                $scope.orderListModel.balance_amount = parseFloat($scope.orderListModel.balance_amount).toFixed(2);
//                $scope.orderListModel.new_balance_amount = utilityService.changeCurrency($scope.orderListModel.balance_amount, $rootScope.appConfig.thousand_seperator);
                $scope.orderListModel.overall_selling_price = $scope.orderListModel.invoiceDetail.overall_selling_price;
                var billdate = utilityService.parseStrToDate($scope.orderListModel.invoiceDetail.date, $scope.dateFormat);
                $scope.orderListModel.validdate = billdate;
                $scope.orderListModel.billdate = $filter('date')(billdate, $scope.dateFormat);
                var billdate = utilityService.parseStrToDate($scope.orderListModel.invoiceDetail.validuntil, $scope.dateFormat);
                $scope.orderListModel.duedate = $filter('date')(billdate, $scope.dateFormat);
                $scope.orderListModel.overall_qty = $scope.orderListModel.invoiceDetail.overall_qty;
                $scope.orderListModel.is_received = $scope.orderListModel.invoiceDetail.is_received;
                for (var loop = 0; loop < $scope.orderListModel.invoiceDetail.detail.length; loop++)
                {
                    var newRow =
                            {
                                "detail_id": $scope.orderListModel.invoiceDetail.detail[loop].id,
                                "id": $scope.orderListModel.invoiceDetail.detail[loop].product_id,
                                "productName": $scope.orderListModel.invoiceDetail.detail[loop].product_name,
                                "received_qty": $scope.orderListModel.invoiceDetail.detail[loop].received_qty,
                                "received_qty_valid": $scope.orderListModel.invoiceDetail.detail[loop].received_qty,
                                "balance_qty": $scope.orderListModel.invoiceDetail.detail[loop].balance_qty,
                                "sku": $scope.orderListModel.invoiceDetail.detail[loop].product_sku,
                                "sales_price": $scope.orderListModel.invoiceDetail.detail[loop].selling_price,
                                "qty": $scope.orderListModel.invoiceDetail.detail[loop].given_qty,
                                "rowtotal": (parseFloat($scope.orderListModel.invoiceDetail.detail[loop].selling_price * $scope.orderListModel.invoiceDetail.detail[loop].given_qty)).toFixed(2),
                                "uom": $scope.orderListModel.invoiceDetail.detail[loop].uom_name,
                                "uomid": $scope.orderListModel.invoiceDetail.detail[loop].uom_id,
                                "barcode": $scope.orderListModel.invoiceDetail.detail[loop].barcode,
                                "receiving_qty" : 0
                            };
                    $scope.orderListModel.list.push(newRow);
                }
                for (var i = 0; i < $scope.orderListModel.list.length; i++)
                {
                    if ($scope.orderListModel.list[i].received_qty == null || $scope.orderListModel.list[i].received_qty == '')
                    {
                        $scope.orderListModel.list[i].received_qty = 0;
                    } else
                    {
                        $scope.orderListModel.list[i].received_qty_valid = parseFloat($scope.orderListModel.list[i].qty - $scope.orderListModel.list[i].received_qty);
                    }
                    if ($scope.qtyFormat != 0 && $scope.qtyFormat != undefined && $scope.qtyFormat != null && $scope.qtyFormat !== '')
                    {
                        $scope.orderListModel.list[i].qty = parseFloat($scope.orderListModel.list[i].qty).toFixed($scope.qtyFormat);
                    }
                    $scope.orderListModel.list[i].rowtotal = utilityService.changeCurrency($scope.orderListModel.list[i].rowtotal, $rootScope.appConfig.thousand_seperator);
                }

                $scope.orderListModel.isLoadingProgress = false;
            }
        }


        $scope.calculatetotal = function ()
        {
            var subTotal = 0;
            var totDiscountPercentage = 0;
            var totTaxPercentage = 0;
            var totalTaxPercentage = 0;
            var totMapTax = 0;
            console.log("bfr loop", $scope.orderListModel.list.length);
            for (var i = 0; i < $scope.orderListModel.list.length; i++)
            {
                $scope.orderListModel.list[i].rowtotal = (parseFloat($scope.orderListModel.list[i].sales_price * $scope.orderListModel.list[i].qty)).toFixed(2);
                subTotal += (parseFloat($scope.orderListModel.list[i].rowtotal)).toFixed(2);

            }
            $scope.orderListModel.subtotal = parseFloat(subTotal).toFixed(2);


            $scope.orderListModel.total = parseFloat(subTotal).toFixed(2);
            $scope.updateRoundoff();
            // $scope.orderListModel.balance_amount = (parseFloat($scope.orderListModel.total - $scope.orderListModel.advance_amount)).toFixed(2);
        }



        $scope.getDeliveryInfo = function ( )
        {
            $scope.orderListModel.isLoadingProgress = true;
            var getListParam = {};
            getListParam.id = $stateParams.id;
            getListParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getDeliveryDetail(getListParam, configOption).then(function (response)
            {
                if (response.data.success == true)
                {
                    var data = response.data.data;
                    $scope.orderListModel.invoiceDetail = data;
                    $scope.orderListModel.list = [];
                    $scope.updateInvoiceDetails();
                }
            });
        }




        $scope.getDeliveryInfo( );


        $scope.steColspan = function () {
            $scope.colsPanValue = '';
            if ($scope.colsPan == true)
            {
                $scope.colsPanValue = 5;

            } else
            {
                $scope.colsPanValue = 4;
            }

        };
        $scope.amountColspan = function () {
            $scope.amountcolsPan = '';
            if ($scope.colsPan == true)
            {
                $scope.amountcolsPan = 3;

            } else
            {
                $scope.amountcolsPan = 2;
            }

        };
        $scope.steColspan();
        $scope.amountColspan();
    }]);