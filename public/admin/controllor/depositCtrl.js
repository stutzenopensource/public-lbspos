app.controller('depositCtrl', ['$scope', '$rootScope', 'adminService', 'ValidationFactory', 'utilityService', 'APP_CONST', '$window', '$filter', 'Auth', '$state', '$timeout', '$httpService','$localStorage',function($scope, $rootScope, adminService, ValidationFactory, utilityService, APP_CONST, $window, $filter, Auth, $state, $timeout, $httpService,$localStorage) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.depositModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
            id: '',
            date: '',
            particular: '',
            category: '',
            created_by: '',
            incomeCategoryInfo: '',
            amount: '',
            is_active: 1,
            status: '',
//             sku: '',

            serverList: null,
            isLoadingProgress: false
        };
        $scope.searchFilter = {
            accountid: '',
            customerid: '',
            name: '',
            start: '',
            fromdate: '',
            todate: '',
            depositInfo: '',
            userInfo: ''

        };
        $scope.validationFactory = ValidationFactory;
        $scope.refreshScreen = function()
        {
            $scope.searchFilter = {
                fromdate: '',
                todate: '',
                incomeCategoryInfo: '',
                user: ''
            };
            $scope.searchFilter.fromdate = $scope.currentDate;
            $scope.initTableFilter();
        }
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.depositModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currencyFormat = $rootScope.appConfig.currency;
        $scope.currentDate = new Date();
        $scope.searchFilter.fromdate = $scope.currentDate;

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.openDate = function(index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            }
            else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };



        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.selectDepositId = '';
        $scope.selectDepositVoucher_number = '';
        $scope.selectDepositVoucher_type = "";
        $scope.showdeletePopup = false;
        $scope.isdeleteLoadingProgress = false;
        $scope.showPopup = function(id, voucher_number,voucher_type)
        {
            $scope.selectDepositId = id;
            $scope.selectDepositVoucher_number = voucher_number;
            if(typeof voucher_type != "undefined")
            {
                $scope.selectDepositVoucher_type = voucher_type;
            }
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteLoadingProgress = false;

        };

        $scope.deleteDepositInfo = function( )
        {

            if (!$scope.isdeleteLoadingProgress)
            {
                $scope.isdeleteLoadingProgress = true;
                if ($scope.selectDepositVoucher_number === '' || $scope.selectDepositVoucher_number == 0)
                {
                    var getListParam = {};
                    getListParam.id = $scope.selectDepositId;
                    if($scope.selectDepositVoucher_type.toLowerCase() == "advance_invoice")
                    {   
                        getListParam.voucher_type  = 'advance_payment';
                        var headers = {};
                        headers['screen-code'] = 'transdeposit';
                        adminService.deleteAdvancepayment(getListParam, getListParam.id, headers).then(function(response)
                        {
                            var data = response.data;
                            if (data.success == true)
                            {
                                $scope.closePopup();
                                $scope.getList();
                            }

                            $scope.isdeleteLoadingProgress = false;
                        });

                     }
                    else
                    {
                        var headers = {};
                        headers['screen-code'] = 'transdeposit';
                        adminService.deleteDeposit(getListParam, getListParam.id, headers).then(function(response)
                        {
                            var data = response.data;
                            if (data.success === true)
                            {
                                $scope.closePopup();
                                $scope.getList();
                            }
                            $scope.isdeleteLoadingProgress = false;
                        });
                        }
                }        
                else
                {
                    var getListParam = {};
                    getListParam.id = $scope.selectDepositVoucher_number;
                    if($scope.selectDepositVoucher_type.toLowerCase() == "advance_invoice")
                    {   
                        getListParam.voucher_type  = 'advance_payment';
                        var headers = {};
                        headers['screen-code'] = 'transdeposit';
                        adminService.deleteAdvancepayment(getListParam, getListParam.id, headers).then(function(response)
                        {
                            var data = response.data;
                            if (data.success == true)
                            {
                                $scope.closePopup();
                                $scope.getList();
                            }

                            $scope.isdeleteLoadingProgress = false;
                        });

                    }
                    else
                    {
                       var headers = {};
                       headers['screen-code'] = 'transdeposit';
                       adminService.deleteVoucher(getListParam, getListParam.id, headers).then(function(response)
                       {
                           var data = response.data;
                           if (data.success === true)
                           {
                               $scope.closePopup();
                               $scope.getList();
                           }
                           $scope.isdeleteLoadingProgress = false;
                       });
                    }
                }
            }
        };
        $scope.$watch('searchFilter.incomeCategoryInfo', function(newVal, oldVal)
        {
            if (typeof newVal == 'undefined' || newVal == '' || newVal == null)
            {
                $scope.initTableFilter();
            }
        });
        $scope.getList = function() {
            $scope.validateDateFlag = false;
            if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate != 'undefined' && $scope.searchFilter.fromdate != '' && $scope.searchFilter.todate != null && typeof $scope.searchFilter.todate != 'undefined' && $scope.searchFilter.todate != '')
            {
                var minDateValue = $filter('date')($scope.searchFilter.fromdate, 'yyyy-MM-dd');
                var value = $filter('date')($scope.searchFilter.todate, 'yyyy-MM-dd');
                minDateValue = moment(minDateValue).valueOf();
                value = moment(value).valueOf();
                if (minDateValue <= value)
                {
                    $scope.validateDateFlag = true;
                }
            }
            if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate != 'undefined' && $scope.searchFilter.fromdate != '' && $scope.searchFilter.todate == null || typeof $scope.searchFilter.todate == 'undefined' || $scope.searchFilter.todate == '')
            {
                $scope.validateDateFlag = true;
            }
            if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate != 'undefined' && $scope.searchFilter.todate != '' && $scope.searchFilter.fromdate == null || typeof $scope.searchFilter.fromdate == 'undefined' || $scope.searchFilter.fromdate == '')
            {
                $scope.validateDateFlag = true;
            }
            if ($scope.searchFilter.todate == null || typeof $scope.searchFilter.todate == 'undefined' || $scope.searchFilter.todate == '' && $scope.searchFilter.fromdate == null || typeof $scope.searchFilter.fromdate == 'undefined' || $scope.searchFilter.fromdate == '')
            {
                $scope.validateDateFlag = true;
            }
            if ($scope.validateDateFlag == true)
            {   
                $scope.depositModel.isLoadingProgress = true;
                var getListParam = {};
                var headers = {};
                headers['screen-code'] = 'transdeposit';
                // getListParam.from_Date = $filter('date')($scope.searchFilter.fromdate, 'yyyy-MM-dd');
                //getListParam.to_Date = $filter('date')($scope.searchFilter.todate, 'yyyy-MM-dd');


                getListParam.from_Date = $scope.searchFilter.fromdate;
                getListParam.to_Date = $scope.searchFilter.todate;

                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_Date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.adminService.appConfig.date_format);
                }
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_Date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.adminService.appConfig.date_format);
                }

                if ($scope.searchFilter.fromdate != null && $scope.searchFilter.fromdate != '')
                {
                    getListParam.from_Date = utilityService.changeDateToSqlFormat(getListParam.from_Date, $scope.adminService.appConfig.date_format);

                }
                if ($scope.searchFilter.todate != null && $scope.searchFilter.todate != '')
                {
                    getListParam.to_Date = utilityService.changeDateToSqlFormat(getListParam.to_Date, $scope.adminService.appConfig.date_format);

                }
                getListParam.id = '';
                getListParam.account_id = '';
                getListParam.customer_id = '';
                getListParam.created_by = $scope.searchFilter.user;
                if ($scope.searchFilter.incomeCategoryInfo != null && typeof $scope.searchFilter.incomeCategoryInfo != 'undefined' && typeof $scope.searchFilter.incomeCategoryInfo.id != 'undefined')
                {
                    getListParam.account_category = $scope.searchFilter.incomeCategoryInfo.name;
                }
                else
                {
                    getListParam.account_category = '';
                }
                getListParam.name = '';
                getListParam.start = ($scope.depositModel.currentPage - 1) * $scope.depositModel.limit;
                getListParam.limit = $scope.depositModel.limit;
                getListParam.is_active = 1;
                $scope.depositModel.isLoadingProgress = true;
                var configOption = adminService.handleOnlyErrorResponseConfig;
                adminService.getDepositList(getListParam, configOption, headers).then(function(response) {
    //                var balanceamt = 0.00;
                    if (response.data.success === true)
                    {
                        var data = response.data.list;
                        $scope.depositModel.list = data;

                        for (var i = 0; i < $scope.depositModel.list.length; i++)
                        {
                            $scope.depositModel.list[i].newdate = utilityService.parseStrToDate($scope.depositModel.list[i].transaction_date);
                            $scope.depositModel.list[i].transaction_date = utilityService.parseDateToStr($scope.depositModel.list[i].newdate, $scope.adminService.appConfig.date_format);
                        }

                        $scope.depositModel.total = data.total;
                    }
                    $scope.depositModel.isLoadingProgress = false;
                });
            }
            else
            {
                $scope.depositModel.isLoadingProgress = false;
            }
        };

        $scope.getIncomeCategoryList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.type = 'income';
            autosearchParam.is_active=1;
            return $httpService.get(APP_CONST.API.GET_INCOME_CATEGORY_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
       $scope.formatincomeCategoryModel = function(model) {

            if (model !== null && model != undefined)
            {

                return model.name;
            }
            return  '';
        };
        $scope.getUserList = function() {

            var getListParam = {};
            getListParam.date = '';
            getListParam.particular = '';
            getListParam.category = '';
            getListParam.credited_by = '';
            getListParam.amount = '';
            getListParam.isactive = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getUserList(getListParam, configOption).then(function(response) {
                var data = response.data;
                $scope.depositModel.userlist = data.list;

            });

        };
         $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'deposit_form';


        $scope.$on('onLocalStorageFormRenterCompleteEvent', function(event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });  
        
          $scope.$on('onLocalStorageFieldRenterCompleteEvent', function(event, formName) {
             if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRenterCount++;
                }
                if ($scope.localStorageRenterCount >= $scope.localStorageCount)
                {
                    console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                    $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
                }
            
              }
        });
        $scope.$on('updateTypeaheadFieldValue', function(event, data) {

            if (data.fieldName == "deposit_form-incomecategory_dropdown")
            {
                $scope.searchFilter.incomeCategoryInfo = data.value;
            }
        });
        
        

        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function(event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRetrieveCount++;
                }
                if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
                {
                    $scope.initTableFilter();
                }
                console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);
            }
        });

        $scope.init = function()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');

        };

        $scope.$on('onLocalStorageReadyEvent', function() {
           if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                $timeout(function() {
                    $scope.init();
                }, 300);
            
            }
        });          
        
        
        
        
        
        $scope.getListPrint = function(val)
        {
            var headers = {};
            headers['screen-code'] = 'salesinvoice';

            var getListParam = {};
            getListParam.Id = "";
            getListParam.start = 0;
            $scope.depositModel.isLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;

            adminService.getDepositList(getListParam, configOption, headers).then(function(response) {

                if (response.data.success)
                {
                    var data = response.data.list;
                    $scope.depositModel.printList = data;
                }
                $scope.depositModel.isLoadingProgress = false;
            });
        };


        //$scope.getList();
        $scope.getUserList();
        //   $scope.getListPrint();
    }]);




