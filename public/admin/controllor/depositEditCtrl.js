




app.controller('depositEditCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', 'APP_CONST', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', function($scope, $rootScope, adminService, $httpService, utilityService, APP_CONST, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory) {

        $scope.depositModel = {
            "date": "",
            "deposit": "",
            "description": "",
            "amount": "",
            "incomeCategoryInfo": "",
            "id": "",
            "status": "",
            "isActive": true,
            isLoadingProgress: false



        };
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.validationFactory = ValidationFactory;

        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.deposit_add_form !== 'undefined' && typeof $scope.deposit_add_form.$pristine !== 'undefined' && !$scope.deposit_add_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        };
        $scope.isDataSavingProcess = false;
        $scope.formReset = function() {
            $scope.deposit_edit_form.$setPristine();
            if (typeof $scope.deposit_edit_form !== 'undefined')
            {
                //$scope.deposit_edit_form.$setPristine();
                $scope.updateDepositInfo();
                $scope.depositModel.date = "";
                $scope.depositModel.particular = "";
                $scope.depositModel.category = '';
                $scope.depositModel.amount = "";
                $scope.depositModel.created_by = "";
                $scope.depositModel.isActive = true;
            }

        };
//        $scope.currentDate = new Date();
//       $scope.depositModel.date = $filter('date')($scope.currentDate, $scope.dateFormat);

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.startDateOpen = false;

        $scope.openDate = function(index) {

            if (index === 0)
            {
                $scope.startDateOpen = true;
            }

        };

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.resetTabletCreate = function() {

            $scope.depositModel.user = '';

        };
        $scope.selectDepositId = '';
        $scope.selectDepositVoucher_number = '';
        $scope.showdeletePopup = false;
        $scope.isdeleteLoadingProgress = false;
        $scope.showPopup = function(id, voucher_number)
        {
            $scope.selectDepositId = id;
            $scope.selectDepositVoucher_number = voucher_number;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteLoadingProgress = false;

        };

        $scope.deleteDepositInfo = function( )
        {

            if (!$scope.isdeleteLoadingProgress)
            {
                $scope.isdeleteLoadingProgress = true;
                if ($scope.selectDepositVoucher_number === '' || $scope.selectDepositVoucher_number == 0)
                {
                    var getListParam = {};
                    getListParam.id = $scope.selectDepositId;
                    var headers = {};
                    headers['screen-code'] = 'transdeposit';
                    adminService.deleteDeposit(getListParam, getListParam.id, headers).then(function(response)
                    {
                        var data = response.data;
                        if (data.success === true)
                        {
                            $scope.closePopup();
                            // $scope.getList();
                            $state.go('app.deposit', {'id': response.data.id}, {'reload': true});
                        }
                        $scope.isdeleteLoadingProgress = false;
                    });
                }
                else
                {
                    var getListParam = {};
                    getListParam.id = $scope.selectDepositVoucher_number;
                    var headers = {};
                    headers['screen-code'] = 'transdeposit';
                    adminService.deleteVoucher(getListParam, getListParam.id, headers).then(function(response)
                    {
                        var data = response.data;
                        if (data.success === true)
                        {
                            $scope.closePopup();
                            //$scope.getList();
                            $state.go('app.deposit', {'id': response.data.id}, {'reload': true});
                        }
                        $scope.isdeleteLoadingProgress = false;
                    });
                }

            }
        };
        $scope.modifyDeposit = function() {
            if ($scope.isDataSavingProcess == false)
            {

                $scope.isDataSavingProcess = true;
                var modifyDepositParam = {};
                var headers = {};
                headers['screen-code'] = 'transdeposit';
                modifyDepositParam.date = $scope.depositModel.deposit;
                modifyDepositParam.description = $scope.depositModel.description;
                modifyDepositParam.amount = $scope.depositModel.amount;
                modifyDepositParam.isactive = 1;
                adminService.modifyDeposit(modifyDepositParam, $scope.depositModel.id, headers).then(function(response) {
                    if (response.data.success === true)
                    {
                        $scope.isDataSavingProcess = false;
                        $scope.formReset();
                        $state.go('app.deposit');
                    }
                });
            }
        };

        $scope.updateDepositInfo = function()
        {
            if ($scope.depositModel.list !== null)
            {
                $scope.depositModel.date = $scope.depositModel.list.date;
                $scope.depositModel.voucher_number = $scope.depositModel.list.voucher_number;
                $scope.depositModel.description = $scope.depositModel.list.particulars;
                $scope.depositModel.amount = $scope.depositModel.list.amount;
                $scope.depositModel.id = $scope.depositModel.list.id;
                var date = utilityService.parseStrToDate($scope.depositModel.list.transaction_date);
                $scope.depositModel.date = $filter('date')(date, $rootScope.appConfig.date_format);
                $scope.depositModel.account = $scope.depositModel.list.account;
                $scope.depositModel.incomeCategoryInfo =
                        {
                            name: $scope.depositModel.list.account_category
                        }
            }
            $scope.depositModel.isLoadingProgress = false;
        };




        $scope.getDepositInfo = function() {

            if (typeof $stateParams.id !== 'undefined')
            {
                $scope.depositModel.isLoadingProgress = true;
                var getListParam = {};
                getListParam.id = $stateParams.id;
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getDepositList(getListParam, configOption).then(function(response) {
                    var data = response.data;
                    if (data.total !== 0)
                    {
                        $scope.depositModel.list = data.list[0];
                        $scope.updateDepositInfo();
                    }
                    $scope.depositModel.isLoadingProgress = false;
                });
            }
        };
        $scope.getAccountlist = function()
        {
            var getListParam = {};
            getListParam.id = "";
            getListParam.name = "";
            getListParam.start = ($scope.depositModel.currentPage - 1) * $scope.depositModel.limit;
            getListParam.limit = $scope.depositModel.limit;
            $scope.isDataSavingProcess = true;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getAccountlist(getListParam, configOption).then(function(response)
            {
                var data = response.data;
                $scope.depositModel.Accountlist = data.list;
                $scope.isDataSavingProcess = false;
            });
        };
        $scope.getIncomeCategoryList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.type = 'income';
            autosearchParam.name = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.GET_INCOME_CATEGORY_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatincomeCategoryModel = function(model) {

            if (model !== null)
            {

                return model.name;
            }
            return  '';
        };

        $scope.getDepositInfo();
        $scope.getAccountlist();
        $scope.getIncomeCategoryList();


    }]);




