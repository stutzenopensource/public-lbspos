



app.controller('designationAddCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$timeout', 'ValidationFactory', 'APP_CONST', 'StutzenHttpService', function($scope, $rootScope, adminService, $filter, Auth, $state, $timeout, ValidationFactory, APP_CONST, StutzenHttpService) {

        $scope.designationModel = {
            id: '',
            uomname: '',
            note: '',
            limit: '',
            isDefault: false
        };
        $scope.pagePerCount = [50, 100];
        $scope.designationModel.limit = $scope.pagePerCount[0];

        $scope.searchFilterValue = "";
        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }


        $scope.validationFactory = ValidationFactory;


        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function()
        {
            if (typeof $scope.designation_add_form != 'undefined' && typeof $scope.designation_add_form.$pristine != 'undefined' && !$scope.designation_add_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        //form reset code
        $scope.formReset = function() {

            $scope.designation_add_form.$setPristine();
        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.createDesignation = function() {

            if ($scope.isDataSavingProcess == false)
            {

                $scope.isDataSavingProcess = true;
                var createParam = {};
                var headers = {};
                headers['screen-code'] = 'designation';
                createParam.id = 0;
                createParam.name = $scope.designationModel.desname;
                createParam.comments = $scope.designationModel.note;
                createParam.is_default = ($scope.designationModel.isDefault==true?1:0);            
                adminService.saveDesignation(createParam, headers).then(function(response) {
                    if (response.data.success == true)
                    {
                        $scope.formReset();
                        $state.go('app.designation');
                    }
                    $scope.isDataSavingProcess = false;
                }).catch(function (response) {
                    console.error('Error occurred:', response.status, response.data);
                    $scope.isDataSavingProcess = false;
                    var element = document.getElementById("btnLoad");
                    element.classList.remove("btn-loader");
                });
            }

        };
    }]);




