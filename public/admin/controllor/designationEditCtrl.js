app.controller('designationEditCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', 'StutzenHttpService', function ($scope, $rootScope, adminService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST, StutzenHttpService) {

        $scope.designationModel = {
            id: '',
            uomname: '',
            note: '',
            isDefault: ' ',
            isLoadingProgress: false
        };
        $scope.pagePerCount = [50, 100];
        $scope.designationModel.limit = $scope.pagePerCount[0];
        $scope.uomDetail = {};

        $scope.validationFactory = ValidationFactory;

        $scope.userRoleList = ['', 'ROLE_ADMIN', 'ROLE_WORKFORCE'];

        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function ()
        {
            if (typeof $scope.uom_edit_form != 'undefined' && typeof $scope.uom_edit_form.$pristine != 'undefined' && !$scope.uom_edit_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function () {

            $scope.designation_edit_form.$setPristine();
            $scope.updateDetails();
        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

// 3.display the data
        $scope.modifyDesignation = function () {
            if ($scope.isDataSavingProcess == false)
            {

                $scope.isDataSavingProcess = true;
                var createParam = {};
                var headers = {};
                headers['screen-code'] = 'designation';
                createParam.name = $scope.designationModel.desname;
                createParam.comments = $scope.designationModel.note;
                createParam.is_default = ($scope.designationModel.isDefault == true ? 1 : 0);
                createParam.is_active = 1;
                adminService.editDesignation(createParam, $stateParams.id, headers).then(function (response) {
                    if (response.data.success == true)
                    {

                        $scope.formReset();
                        $state.go('app.designation');
                    }
                    $scope.isDataSavingProcess = false;
                }).catch(function (response) {
                    console.error('Error occurred:', response.status, response.data);
                    $scope.isDataSavingProcess = false;
                    var element = document.getElementById("btnLoad");
                    element.classList.remove("btn-loader");
                });
            }
        };

        //2.get the updated data and keep it in local
        $scope.updateDetails = function ()
        {
            if ($scope.designationDetail != null)
            {
                $scope.designationModel.desname = $scope.designationDetail.name;
                $scope.designationModel.note = $scope.designationDetail.comments;
                $scope.designationModel.isDefault = ($scope.designationDetail.is_default == 1 ? true : false);
                $scope.designationModel.isLoadingProgress = false;
            }
        }

        //1.get the data from the list    
        $scope.getDesignation = function () {

            $scope.designationModel.isLoadingProgress = true;
            if (typeof $stateParams.id != 'undefined')
            {
                var getListParam = {};
                getListParam.id = $stateParams.id;
                getListParam.limit = 1;
                getListParam.start = 0;
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getDesignationList(getListParam, configOption).then(function (response) {
                    var data = response.data;
                    if (data.list.length > 0)
                    {
                        $scope.designationDetail = data.list[0];
                        $scope.updateDetails();
                    }

                });
            }
        };

        $scope.getDesignation();

    }]);



