app.controller('designationListCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$timeout', function ($scope, $rootScope, adminService, $filter, Auth, $state, $timeout) {
      $rootScope.getNavigationBlockMsg = null;
       $scope.designationModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
            status: '',
            //             sku: '',

            serverList: null,
            isLoadingProgress: false
        };

        $scope.pagePerCount = [50, 100];
        $scope.designationModel.limit = $scope.pagePerCount[0];

        $scope.searchFilter = {
            filteruomname: ''
        };
        $scope.refreshScreen = function ()
        {
            $scope.searchFilter = {
                filteruomname: ''
            };
            $scope.initTableFilter();
        }
        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }

        $scope.selectUOMId = '';
        $scope.showdeletePopup = false;
        $scope.isdeleteProgress = false;
        $scope.showPopup = function (id)
        {
            $scope.selectDesId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function ()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteProgress = false;
        };

        $scope.deleteDesignation = function ( )
        {
            if ($scope.isdeleteProgress == false)
            {
                $scope.isdeleteProgress = true;

                var getListParam = {};
                getListParam.id = $scope.selectDesId;
                var headers = {};
                headers['screen-code'] = 'designation';
                adminService.deleteDesignation(getListParam, getListParam.id, headers).then(function (response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getList();
                    }
                    $scope.isdeleteProgress = false;

                });
            }
        };

        $scope.getList = function ()
        {
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'designation';
            getListParam.id = "";
            getListParam.is_active = 1;
            getListParam.name = $scope.searchFilter.desname;
            getListParam.start = ($scope.designationModel.currentPage - 1) * $scope.designationModel.limit;
            getListParam.limit = $scope.designationModel.limit;
            $scope.designationModel.isLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getDesignationList(getListParam, configOption, headers).then(function (response) {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.designationModel.list = data.list;
                    $scope.designationModel.total = data.total;
                }
                $scope.designationModel.isLoadingProgress = false;
            });
        };

        $scope.getList();
    }]);




