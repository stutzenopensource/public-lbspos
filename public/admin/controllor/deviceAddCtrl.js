




app.controller('deviceAddCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', function($scope, $rootScope, adminService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory) {

        $scope.deviceModel = {
            "name": "",
            "devicevode": "",
            "shortcode": "",
            "isActive": true
        }
        $scope.validationFactory = ValidationFactory;
        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.device_add_form != 'undefined' && typeof $scope.device_add_form.$pristine != 'undefined' && !$scope.device_add_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function() {

            $scope.device_add_form.$setPristine();
            $scope.deviceModel = {
                "name": "",
                "devicecode": "",
                "shortcode": "",
                "comments": ""
            }
        };

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.resetTabletCreate = function() {

            $scope.deviceModel.user = '';

        }

        $scope.createDevice = function() {

            $scope.isDataSavingProcess = true;
            var createDeviceParam = {};
            createDeviceParam.id = 0;
            createDeviceParam.name = $scope.deviceModel.name;
            createDeviceParam.device_code = $scope.deviceModel.devicecode;
            createDeviceParam.short_code = $scope.deviceModel.shortcode;
            createDeviceParam.comments = " ";

            adminService.createDevice(createDeviceParam).then(function(response) {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.device');
                }
                $scope.isDataSavingProcess = false;
            });            
        };


    }]);




