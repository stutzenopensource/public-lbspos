app.controller('discountedreportCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', function ($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.discountModel = {
            currentPage: 1,
            total: 0,
            Amount: 0,
            limit: 10,
            list: {},
            length: [],
            date: '',
            sno: '',
            name: '',
            amount: '',
            status: '',
            //             sku: '',

            serverList: null,
            isLoadingProgress: false,
            isSearchLoadingProgress: false
        };
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.discountModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
        $scope.validationFactory = ValidationFactory;
        $scope.openDate = function (index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            } else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };

        $scope.searchFilter = {
            fromdate: '',
            todate: ''

        };
        $scope.searchFilter.fromdate = $scope.currentDate;
        $scope.clearFilters = function ()
        {
            $scope.searchFilter = {
                fromdate: '',
                todate: ''
            };
            $scope.discountModel.list = [];
            $scope.searchFilter.fromdate = $scope.currentDate;
        }

        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.validateDateFilterData = function ()
        {
            var retVal = false;
            if ($scope.searchFilter.todate !== '' || $scope.searchFilter.fromdate !== '')
            {
                retVal = true;
            }
            return retVal;
        };
        $scope.getList = function () {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'discountedreport';
            getListParam.from_date = $scope.searchFilter.fromdate;
            getListParam.to_date = $scope.searchFilter.todate;
            $scope.discountModel.isLoadingProgress = true;
            $scope.discountModel.isSearchLoadingProgress = true;
            if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
            {
                getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
            }
            if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
            {
                getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
            }

            if ($scope.searchFilter.fromdate != null && $scope.searchFilter.fromdate != '')
            {
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);

            }
            if ($scope.searchFilter.todate != null && $scope.searchFilter.todate != '')
            {
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
            }
            getListParam.start = 0;
            getListParam.limit = 0;
            if ($scope.searchFilter.todate != '' && $scope.searchFilter.todate != null && $scope.searchFilter.todate != 'undefined' && $scope.searchFilter.fromdate != '' && $scope.searchFilter.fromdate != null)
            {
                adminService.getdiscountedReportList(getListParam, headers).then(function (response)
                {
                    var totalAmt = 0.00;
                    var totalDiscount = 0.00;
                    var totalInvoiceAmt = 0.00;
                    if (response.data.success === true)
                    {
                        var data = response.data.list;
                        $scope.discountModel.list = data;

                        for (var i = 0; i < $scope.discountModel.list.length; i++)
                        {
                            $scope.discountModel.list[i].newdate = utilityService.parseStrToDate($scope.discountModel.list[i].date);
                            $scope.discountModel.list[i].date = utilityService.parseDateToStr($scope.discountModel.list[i].newdate, $rootScope.appConfig.date_format);
                            if ($scope.discountModel.list[i].subtotal == null)
                            {
                                $scope.discountModel.list[i].subtotal = 0.00;
                            }
                            totalAmt = totalAmt + parseFloat($scope.discountModel.list[i].subtotal);
                            $scope.discountModel.list[i].subtotal = utilityService.changeCurrency($scope.discountModel.list[i].subtotal, $rootScope.appConfig.thousand_seperator);
                            if ($scope.discountModel.list[i].discount_amount == null)
                            {
                                $scope.discountModel.list[i].discount_amount = 0.00;
                            }
                            totalDiscount = totalDiscount + parseFloat($scope.discountModel.list[i].discount_amount);
                            $scope.discountModel.list[i].discount_amount = utilityService.changeCurrency($scope.discountModel.list[i].discount_amount, $rootScope.appConfig.thousand_seperator);
                            if ($scope.discountModel.list[i].total_amount == null)
                            {
                                $scope.discountModel.list[i].total_amount = 0.00;
                            }
                            totalInvoiceAmt = totalInvoiceAmt + parseFloat($scope.discountModel.list[i].total_amount);
                        }
                        $scope.discountModel.total = data.total;
                        $scope.discountModel.Amount = totalAmt;
                        $scope.discountModel.Amount = parseFloat($scope.discountModel.Amount).toFixed(2);
                        $scope.discountModel.Amount = utilityService.changeCurrency($scope.discountModel.Amount, $rootScope.appConfig.thousand_seperator);
                        $scope.discountModel.DiscountAmt = totalDiscount;
                        $scope.discountModel.DiscountAmt = parseFloat($scope.discountModel.DiscountAmt).toFixed(2);
                        $scope.discountModel.DiscountAmt = utilityService.changeCurrency($scope.discountModel.DiscountAmt, $rootScope.appConfig.thousand_seperator);
                        $scope.discountModel.InvoiceAmt = parseFloat(totalInvoiceAmt).toFixed(2);
                        $scope.discountModel.InvoiceAmt = utilityService.changeCurrency($scope.discountModel.InvoiceAmt, $rootScope.appConfig.thousand_seperator);

                        $scope.discountModel.isLoadingProgress = false;
                        $scope.discountModel.isSearchLoadingProgress = false;
                    }
                });
            } else {
                $scope.discountModel.isLoadingProgress = false;
                $scope.discountModel.isSearchLoadingProgress = false;
                $scope.discountModel.list = [];
            }

        };

        $scope.print = function (div)
        {
            var docHead = document.head.outerHTML;
            var printContents = document.getElementById(div).outerHTML;
            var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=865, height=600, resizable=yes, screenX=200, screenY=200, personalbar=no, scrollbars=yes";

            var newWin = window.open("", "_blank", winAttr);
            var writeDoc = newWin.document;
            writeDoc.open();
            writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
            writeDoc.close();
            newWin.focus();
        };

        $scope.validateAmount = function (amount)
        {
            if (amount > 0)
            {
                return true;
            } else
            {
                return false;
            }
        };

//    $scope.getList();
        $scope.viewRedirect = function (id)
        {
            if ($rootScope.appConfig.invoice_print_template === 't1')
            {
                $state.go('app.invoiceView1', {
                    'id': id
                }, {
                    'reload': true
                });
            } else if ($rootScope.appConfig.invoice_print_template === 't2')
            {
                $state.go('app.invoiceView2', {
                    'id': id
                }, {
                    'reload': true
                });
            } else
            {
                $state.go('app.invoiceView1', {
                    'id': id
                }, {
                    'reload': true
                });
            }
        };
        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'discounted_form';
        $scope.$on('onLocalStorageFormRenterCompleteEvent', function (event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });

        $scope.$on('onLocalStorageFieldRenterCompleteEvent', function (event, formName) {

            if ($scope.localStorageFormName == formName)
            {
                $scope.localStorageRenterCount++;
            }
            if ($scope.localStorageRenterCount >= $scope.localStorageCount)
            {
                console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
            }


        });

        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function (event, formName) {

            if ($scope.localStorageFormName == formName)
            {
                $scope.localStorageRetrieveCount++;
            }
            if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
            {
                $scope.initTableFilter();
            }
            console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);

        });

        $scope.init = function ()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');

        };

        $scope.$on('onLocalStorageReadyEvent', function () {

            $timeout(function () {
                $scope.init();
            }, 300);
        });
    }]);






