app.controller('employeeAddCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$timeout', '$filter', 'Auth', 'ValidationFactory', '$state', '$window', function ($scope, $rootScope, adminService, utilityService, $timeout, $filter, Auth, ValidationFactory, $state, $window) {
        $scope.employeeAddModel = {
            "fname": "",
            "lname": "",
            "gender": "",
            "dob": '',
            "teamid": '',
            "email": "",
            "mobile": "",
            "city_id": "",
            "state_id": "",
            "country_id": "",
            "employeetype": "",
            "address": "",
            "country_list": [],
            "state_list": [],
            "city_list": [],
            "isactive": true,
            img: '',
            designation: '',
            "designationList": []
        }

        $scope.validationFactory = ValidationFactory;
        $scope.getNavigationBlockMsg = function (pageReload)
        {
            if (typeof $scope.create_employee_form != 'undefined' && typeof $scope.create_employee_form.$pristine != 'undefined' && !$scope.create_employee_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        //    $scope.currentDate = new Date();

        $scope.formReset = function () {

            $scope.create_employee_form.$setPristine();
            $scope.employeeAddModel.fname = '';
            $scope.employeeAddModel.dob = '';
            $scope.employeeAddModel.gender = '';
            $scope.employeeAddModel.mobile = '';
            $scope.employeeAddModel.email = '';
            $scope.employeeAddModel.address = '';
            $scope.employeeAddModel.city_id = '';
            $scope.employeeAddModel.state_id = '';
            $scope.employeeAddModel.country_id = '';
            $scope.employeeAddModel.address = '';
            $scope.employeeAddModel.isactive = '';
        }
        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }
        $scope.currentDate = new Date();
        $scope.startDateOpen = false;
        $scope.DOJDateOpen = false;
        $scope.RelevingDateOpen = false;
        $scope.openDate = function (index) {

            if (index == 1)
            {
                $scope.startDateOpen = true;
            }
            if (index == 2)
            {
                $scope.DOJDateOpen = true;
            }
            if (index == 3)
            {
                $scope.RelevingDateOpen = true;
            }

        }

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.createEmployee, 300);
        }
//        $scope.showAlert = true;
//        setTimeout(function () {
//            if ($rootScope.userModel.new_user == 1 && $rootScope.hideAlert == false)
//            {
//                $window.localStorage.setItem("demo_guide", "false");
//                swal({
//                    title: "Guide Tour!",
//                    text: "Kindly fill all the required details and click submit to proceed the demo..",
//                    confirmButtonText: "OK"
//                },
//                        function (isConfirm) {
//                            if (isConfirm) {
//                                $rootScope.hideAlert = true;
//                            }
//                        });
//            }
//        }, 2000);

        $scope.createEmployee = function () {

            $scope.isDataSavingProcess = true;
            var createEmployeeParam = {};
            var headers = {};
            headers['screen-code'] = 'employee';
            createEmployeeParam.designation_id = $scope.employeeAddModel.designation;
            createEmployeeParam.name = $scope.employeeAddModel.fname;
            createEmployeeParam.mobile = $scope.employeeAddModel.mobile;
            createEmployeeParam.email = $scope.employeeAddModel.email;
            createEmployeeParam.address = $scope.employeeAddModel.address;
            createEmployeeParam.country_id = $scope.employeeAddModel.country_id;
            createEmployeeParam.state_id = $scope.employeeAddModel.state_id;
            createEmployeeParam.city_id = $scope.employeeAddModel.city_id;
            createEmployeeParam.pincode = $scope.employeeAddModel.pincode;
            createEmployeeParam.img = "";
            if ($scope.employeeAddModel.dob != null && $scope.employeeAddModel.dob != '')
            {
                createEmployeeParam.dob = utilityService.parseDateToStr($scope.employeeAddModel.dob, 'yyyy-MM-dd');
            } else
            {
                createEmployeeParam.dob = '';
            }
            createEmployeeParam.gender = $scope.employeeAddModel.gender;

            createEmployeeParam.employee_code = $scope.employeeAddModel.empcode;
            var configOption = adminService.handleBothSuccessAndErrorResponseConfig;
            adminService.saveEmployee(createEmployeeParam, configOption, headers).then(function (response) {

                if (response.data.success == true)
                {
                    $scope.showAlert = false;
                    $scope.formReset();
                    $state.go('app.employee');
                    if ($rootScope.userModel.new_user == 1)
                    {
                        $window.localStorage.setItem("demo_guide", "true");
                        $rootScope.closeDemoPopup(8);
                    }
                }
                $scope.isDataSavingProcess = false;

            }).catch(function (response) {
                console.error('Error occurred:', response.status, response.data);
                $scope.isDataSavingProcess = false;
                var element = document.getElementById("btnLoad");
                element.classList.remove("btn-loader");
            });
        };
        $scope.getcountryList = function () {

            $scope.employeeAddModel.isLoadingProgress = true;
            var countryListParam = {};
            countryListParam.id = '';

            countryListParam.is_active = 1;

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getCountryList(countryListParam, configOption).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.employeeAddModel.country_list = data.list;
                    for (var i = 0; i < $scope.employeeAddModel.country_list.length; i++)
                    {
                        if ($scope.employeeAddModel.country_list[i].is_default == 1)
                        {
                            $scope.employeeAddModel.country_id = $scope.employeeAddModel.country_list[i].id;

                        }
                    }
                    $scope.getstateList();
                }
                $scope.employeeAddModel.isLoadingProgress = false;

            });

        };
        $scope.getcountryList();

        $scope.getstateList = function () {

            $scope.employeeAddModel.isLoadingProgress = true;
            var stateListParam = {};

            stateListParam.id = '';

            stateListParam.is_active = 1;
            stateListParam.country_id = $scope.employeeAddModel.country_id;

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getStateList(stateListParam, configOption).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.employeeAddModel.state_list = data.list;
                    for (var i = 0; i < $scope.employeeAddModel.state_list.length; i++)
                    {
                        if ($scope.employeeAddModel.state_list[i].is_default == 1)
                        {
                            $scope.employeeAddModel.state_id = $scope.employeeAddModel.state_list[i].id;

                        }
                    }
                    $scope.getcityList();
                }
                $scope.employeeAddModel.isLoadingProgress = false;
            });

        };

        $scope.getcityList = function () {

            $scope.employeeAddModel.isLoadingProgress = true;
            var cityListParam = {};
            cityListParam.id = '';

            cityListParam.is_active = 1;
            cityListParam.country_id = $scope.employeeAddModel.country_id;
            cityListParam.state_id = $scope.employeeAddModel.state_id;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getCityList(cityListParam, configOption).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.employeeAddModel.city_list = data.list;
                }
                $scope.employeeAddModel.isLoadingProgress = false;
            });

        };

        $scope.CountryChange = function ()
        {
            $scope.employeeAddModel.state_id = '';
            $scope.employeeAddModel.city_id = '';
            $scope.getstateList();

        }
        $scope.StateChange = function ()
        {
            $scope.employeeAddModel.city_id = '';
            $scope.getcityList();

        }
        $scope.getDesignation = function ()
        {
            var getListParam = {};
            getListParam.is_active = 1;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getDesignationList(getListParam, configOption).then(function (response) {
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.employeeAddModel.designationList = data;
                    if ($scope.employeeAddModel.designationList.length > 0)
                    {
                        for (var i = 0; i < $scope.employeeAddModel.designationList.length; i++)
                        {
                            if ($scope.employeeAddModel.designationList[i].is_default == 1)
                            {
                                $scope.employeeAddModel.designation = $scope.employeeAddModel.designationList[i].id + '';

                            }
                        }
                    }
                }
            });
        };
        $scope.getCodeAutoGenerated = function ()
        {
            var getListParam = {};
            getListParam.type = 'employee';
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getCodeAutoGeneratedList(getListParam, configOption).then(function (response) {
                if (response.data.success === true)
                {
                    var data = response.data.no;
                    $scope.employeeAddModel.empcode = data;
                }
            });
        };
        $scope.getDesignation();

        $scope.getCodeAutoGenerated();
    }]);






