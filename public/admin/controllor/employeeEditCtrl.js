

app.controller('employeeEditCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$httpService', 'APP_CONST', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', function ($scope, $rootScope, adminService, utilityService, $httpService, APP_CONST, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory) {

        $scope.employeeEditModel = {
            "fname": "",
            "lname": "",
            "gender": "",
            "dob": '',
            "email": "",
            "mobile": "",
            "city_id": "",
            "state_id": "",
            "country_id": "",
            "address": "",
            "country_list": [],
            "state_list": [],
            "city_list": [],
            "country_name": '',
            "state_name": '',
            currentPage: 1,
            total: 0,
            isLoadingProgress: false,
            "img": [],
            "designation": '',
            "designationList": []
        };
        $scope.validationFactory = ValidationFactory;
        $scope.getNavigationBlockMsg = function (pageReload)
        {
            if (typeof $scope.employee_edit_form != 'undefined' && typeof $scope.employee_edit_form.$pristine != 'undefined' && !$scope.employee_edit_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }

        $scope.currentDate = new Date();
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.startDateOpen = false;
        $scope.DOJDateOpen = false;
        $scope.RelevingDateOpen = false;
        $scope.openDate = function (index) {

            if (index == 1)
            {
                $scope.startDateOpen = true;
            }

        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function () {
            if (typeof $scope.employee_edit_form != 'undefined')
            {
                $scope.employee_edit_form.$setPristine();
                $scope.updateEmployeeInfo();
            }

        }
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.initUpdateDetailTimeoutPromise = null;

        $scope.initUpdateDetail = function ()
        {
            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
            }
            if ($scope.isCountryLoaded)
            {
                $scope.updateEmployeeInfo();
            } else
            {
                $scope.initUpdateDetailTimeoutPromise = $timeout($scope.initUpdateDetail, 300);
            }
        }

        $scope.updateEmployeeInfo = function ()
        {
            $scope.employeeEditModel.fname = $scope.employeeEditModel.list.name;
            $scope.employeeEditModel.mobile = $scope.employeeEditModel.list.mobile;
            $scope.employeeEditModel.email = $scope.employeeEditModel.list.email;
            if ($scope.employeeEditModel.list.country_id != '' && $scope.employeeEditModel.list.country_id != null && $scope.employeeEditModel.list.country_id != 0)
            {
                $scope.employeeEditModel.country_id = $scope.employeeEditModel.list.country_id;
                $scope.getstateList();
            }
            if ($scope.employeeEditModel.list.city_id != '' && $scope.employeeEditModel.list.city_id != null && $scope.employeeEditModel.list.city_id != 0)
            {
                $scope.employeeEditModel.city_id = $scope.employeeEditModel.list.city_id;
            }
            if ($scope.employeeEditModel.list.state_id != '' && $scope.employeeEditModel.list.state_id != null && $scope.employeeEditModel.list.state_id != 0)
            {
                $scope.employeeEditModel.state_id = $scope.employeeEditModel.list.state_id;
                $scope.getcityList();
            }
            $scope.employeeEditModel.address = $scope.employeeEditModel.list.address;
            var date = moment(new Date($scope.employeeEditModel.list.dob)).valueOf();
            if (!isNaN(date) && date != undefined && date != null)
            {
                $scope.employeeEditModel.dob = utilityService.parseStrToDate($scope.employeeEditModel.list.dob);
            } else
            {
                $scope.employeeEditModel.dob = '';
            }

            $scope.employeeEditModel.gender = $scope.employeeEditModel.list.gender;
            $scope.employeeEditModel.designation = $scope.employeeEditModel.list.designation_id + '';
            if ($scope.employeeEditModel.list.designation_id == '' || $scope.employeeEditModel.list.designation_id == null || $scope.employeeEditModel.list.designation_id == 0)
            {
                if ($scope.employeeEditModel.designationList.length > 0)
                {
                    for (var i = 0; i < $scope.employeeEditModel.designationList.length; i++)
                    {
                        if ($scope.employeeEditModel.designationList[i].is_default == 1)
                        {
                            $scope.employeeEditModel.designation = $scope.employeeEditModel.designationList[i].id + '';

                        }
                    }
                }
            }
            $scope.employeeEditModel.pincode = $scope.employeeEditModel.list.pincode;
            $scope.employeeEditModel.empcode = $scope.employeeEditModel.list.employee_code;
            $scope.employeeEditModel.img = [];
            if ($scope.employeeEditModel.list.attachment.length > 0)
            {
                var fileItem = {}
                fileItem.urlpath = $scope.employeeEditModel.list.attachment[0].url;
                $scope.employeeEditModel.img.push(fileItem);
                console.log('imgtest');
                console.log($scope.employeeEditModel.img);
            } else
            {
                $scope.employeeEditModel.img = [];

            }
        }

        $scope.modifyEmployee = function () {
            if ($scope.isDataSavingProcess == false)
            {
                $scope.isDataSavingProcess = true;
                var modifyEmployeeParam = {};
                modifyEmployeeParam.attachment = [];
                var headers = {};
                headers['screen-code'] = 'employee';
                modifyEmployeeParam.name = $scope.employeeEditModel.fname;
                modifyEmployeeParam.mobile = $scope.employeeEditModel.mobile;
                modifyEmployeeParam.email = $scope.employeeEditModel.email;
                modifyEmployeeParam.city_id = $scope.employeeEditModel.city_id;
                modifyEmployeeParam.country_id = $scope.employeeEditModel.country_id;
                modifyEmployeeParam.state_id = $scope.employeeEditModel.state_id;
                modifyEmployeeParam.employee_code = $scope.employeeEditModel.empcode;
                modifyEmployeeParam.dob = utilityService.parseDateToStr($scope.employeeEditModel.dob, 'yyyy-MM-dd');
                modifyEmployeeParam.gender = $scope.employeeEditModel.gender;
                modifyEmployeeParam.address = $scope.employeeEditModel.address;
                modifyEmployeeParam.designation_id = $scope.employeeEditModel.designation;
                if ($scope.employeeEditModel.img.length > 0)
                {
                    var imgParam = {};
                    imgParam.url = $scope.employeeEditModel.img[0].urlpath;
                    imgParam.comments = "";
                    modifyEmployeeParam.attachment.push(imgParam);
                } else
                {
                    modifyEmployeeParam.attachment = [];
                }
                var configOption = adminService.handleBothSuccessAndErrorResponseConfig;
                adminService.editEmployee(modifyEmployeeParam, $stateParams.id, configOption, headers).then(function (response) {
                    if (response.data.success == true)
                    {
                        $scope.formReset();
                        $state.go('app.employee');
                    }
                    $scope.isDataSavingProcess = false;
                }).catch(function (response) {
                    console.error('Error occurred:', response.status, response.data);
                    $scope.isDataSavingProcess = false;
                    var element = document.getElementById("btnLoad");
                    element.classList.remove("btn-loader");
                });
            }
        };
        $scope.getcountryList = function () {

            $scope.employeeEditModel.isLoadingProgress = true;
            var countryListParam = {};
            $scope.isCountryLoaded = false;
            countryListParam.id = '';

            countryListParam.is_active = 1;

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getCountryList(countryListParam, configOption).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.employeeEditModel.country_list = data.list;
                    $scope.isCountryLoaded = true;
                }
                $scope.employeeEditModel.isLoadingProgress = false;
            });

        };
        $scope.getcountryList();

        $scope.getstateList = function () {

            $scope.employeeEditModel.isLoadingProgress = true;
            var stateListParam = {};
            $scope.isStateLoaded = false;
            stateListParam.id = '';

            stateListParam.is_active = 1;
            stateListParam.country_id = $scope.employeeEditModel.country_id;
//            stateListParam.country_name = $scope.employeeEditModel.country_name;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getStateList(stateListParam, configOption).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.employeeEditModel.state_list = data.list;
                    $scope.isStateLoaded = true;
                }
                $scope.employeeEditModel.isLoadingProgress = false;
            });

        };

        $scope.getcityList = function () {

            $scope.employeeEditModel.isLoadingProgress = true;
            var cityListParam = {};
            $scope.isCityLoaded = false;
            cityListParam.id = '';
            cityListParam.is_active = 1;
            cityListParam.country_id = $scope.employeeEditModel.country_id;
            cityListParam.state_id = $scope.employeeEditModel.state_id;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getCityList(cityListParam, configOption).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.employeeEditModel.city_list = data.list;
                    $scope.isCityLoaded = true;
                }
                $scope.employeeEditModel.isLoadingProgress = false;
            });

        };

        $scope.CountryChange = function ()
        {
            $scope.employeeEditModel.state_id = '';
            $scope.employeeEditModel.city_id = '';
            $scope.getstateList();

        }
        $scope.StateChange = function ()
        {
            $scope.employeeEditModel.city_id = '';
            $scope.getcityList();

        }

        $scope.getList = function () {
            var id = $stateParams.id.split('-')
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'employee';
            getListParam.id = id[0];
            if(id[1] == "Act")
            {
                getListParam.is_active = 1;
            }
            else
            {
                getListParam.is_active = 0;
            }
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getEmployeeList(getListParam, configOption, headers).then(function (response) {
                var data = response.data;
                if (response.data.success === true)
                {
                    $scope.employeeEditModel.list = data.list[0];
                    $scope.initUpdateDetail();
                }
            });

        };

        $scope.showUploadFilePopup = false;
        $scope.showPhotoGalaryPopup = false;
        $scope.showPopup = function (value) {

            if (value == 'fileupload')
            {
                $scope.showUploadFilePopup = true;
            } else if (value == 'photogalary')
            {
                $scope.showPhotoGalaryPopup = true;
            }


        }

        $scope.closePopup = function (value) {

            if (value == 'fileupload')
            {
                $scope.showUploadFilePopup = false;

            } else if (value == 'photogalary')
            {
                $scope.showPhotoGalaryPopup = false;
            }

        }
        $scope.uploadedFileQueue = [];
        $scope.uploadedFileCount = 0;
        $scope.isImageSavingProcess = false;
        $scope.isImageUploadComplete = false;
        $scope.$on("updateFileUploadEvent", updateFileUploadEventHandler);
        function updateFileUploadEventHandler(event, data)
        {
            $scope.uploadedFileQueue = data;
            console.log('$scope.uploadedFileQueue');
            console.log($scope.uploadedFileQueue);
            $scope.validateFileUploadStatus();
        }

        $scope.validateFileUploadStatus = function ()
        {
            $scope.isImageSavingProcess = true;
            var hasUploadCompleted = true;
            for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
            {
                if (!$scope.uploadedFileQueue[i].isSuccess)
                {
                    hasUploadCompleted = false;
                    $scope.$broadcast('genericErrorEvent', 'Image upload is in progress.');
                    break;
                }
            }
            if (hasUploadCompleted && $scope.uploadedFileQueue.length > 0)
            {
                $scope.uploadedFileCount = $scope.uploadedFileQueue.length;
                $scope.isImageSavingProcess = false;
                $scope.isImageUploadComplete = true;
            }

        }
        $scope.saveImagePath = function ()
        {
            if ($scope.uploadedFileQueue.length == 0)
            {
                $scope.closePopup('fileupload');
            }
            var hasUploadCompleted = true;
            for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
            {
                if (!$scope.uploadedFileQueue[i].isSuccess)
                {
                    hasUploadCompleted = false;
                    $scope.$broadcast('genericErrorEvent', 'Image upload is in progress.');
                    break;
                }
            }
            if (hasUploadCompleted && $scope.uploadedFileQueue.length > 0)
            {
                $scope.employeeEditModel.img = [];
                var fileItem = {}
                fileItem.urlpath = $scope.uploadedFileQueue[0].urlpath;
                $scope.employeeEditModel.img.push(fileItem);
                console.log($scope.employeeEditModel.img);
                $scope.closePopup('fileupload');
            }
        }
        $scope.getDesignation = function ()
        {
            var getListParam = {};
            $scope.designationLoaded = false;
            getListParam.is_active = 1;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getDesignationList(getListParam, configOption).then(function (response) {
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.employeeEditModel.designationList = data;
                    $scope.designationLoaded = true;

                }
            });
        };
        $scope.getDesignation();
        $scope.getList();

    }]);






