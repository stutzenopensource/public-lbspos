


app.controller('employeeListCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$timeout', 'ValidationFactory', '$window', function ($scope, $rootScope, adminService, $filter, Auth, $state, $timeout, ValidationFactory, $window) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.employeeModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            email: '',
            list: [],
            country: '',
            city: '',
            id: '',
            status: '',
            serverList: null,
            isLoadingProgress: false,
            teamList: []
        };
        $scope.pagePerCount = [50, 100];
        $scope.employeeModel.limit = $scope.pagePerCount[0];
        $scope.validationFactory = ValidationFactory;
        $scope.searchFilter = {
            fname: '',
            mobile: '',
            emp_code: '',
            activeOrDeactiveEmployee: ''
        };

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }
        $scope.refreshScreen = function ()
        {
            $scope.searchFilter = {
                fname: '',
                mobile: '',
                emp_code: '',
                activeOrDeactiveEmployee: ''
            };
            $scope.initTableFilter();
        }
        $scope.selectEmployeeId = '';
        $scope.showdeletePopup = false;
        $scope.isdeleteEmployeeLoadingProgress = false;
        $scope.showPopup = function (id)
        {
            $scope.selectEmployeeId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function ()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteCategoryLoadingProgress = false;
        };

        $scope.deleteEmployeeInfo = function ( )
        {
            if ($scope.isdeleteEmployeeLoadingProgress == false)
            {
                $scope.isdeleteEmployeeLoadingProgress = true;
                var getListParam = {};
                getListParam.id = $scope.selectEmployeeId;
                var headers = {};
                headers['screen-code'] = 'employee';
                adminService.deleteEmployee(getListParam, getListParam.id, headers).then(function (response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getList();
                    }
                    $scope.isdeleteEmployeeLoadingProgress = false;
                });
            }
        };

        $scope.getList = function () {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'employee';
            getListParam.employee_code = $scope.searchFilter.emp_code;
            getListParam.name = $scope.searchFilter.fname;
            if ($scope.searchFilter.activeOrDeactiveEmployee == 1)
            {
                getListParam.is_active = 0;
            } else
            {
                getListParam.is_active = 1;
            }
            //getListParam.is_active = 1;
            getListParam.mobile = $scope.searchFilter.mobile;
            if ($rootScope.employeeNext == false) //For Demo Guide purpose
            {
                getListParam.start = 0;
                getListParam.limit = 1;
            } else {
                getListParam.start = ($scope.employeeModel.currentPage - 1) * $scope.employeeModel.limit;
                getListParam.limit = $scope.employeeModel.limit;
            }
            $scope.employeeModel.isLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getEmployeeList(getListParam, configOption, headers).then(function (response) {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.employeeModel.list = data.list;
                    $scope.employeeModel.total = data.total;
                    for(var i =0;i<$scope.employeeModel.list.length;i++)
                    {
                        if($scope.searchFilter.activeOrDeactiveEmployee == 1)
                        {
                            $scope.employeeModel.list[i].statusType =$scope.employeeModel.list[i].id+"-Deact"
                        }
                        else
                        {
                            $scope.employeeModel.list[i].statusType =$scope.employeeModel.list[i].id+"-Act"
                        }
                    }
                    if($scope.searchFilter.activeOrDeactiveEmployee == 1)
                    {
                        $scope.list = "Deact"
                    }
                    else
                    {
                        $scope.list = "Act"
                    }
                    if ($scope.employeeModel.list.length > 0)
                    {
                        $window.localStorage.setItem("employeeAdded", "true");
                    }
                    if ($rootScope.userModel.new_user == 1 && $rootScope.employeeNext != false && $rootScope.employeeNext != undefined)
                    {
                        $window.localStorage.setItem("demo_guide", "true");
                        $rootScope.closeDemoPopup(8);
                    }
                }
                $scope.employeeModel.isLoadingProgress = false;
            });

        };
        $scope.getList();
    }]);










