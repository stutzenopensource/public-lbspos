app.controller('employeeSalesReportCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', function ($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.empSalesModel = {
            currentPage: 1,
            total: 0,
            total_Amount: 0,
            limit: 10,
            list: [],
            length: [],
            serverList: null,
            isLoadingProgress: false,
            isSearchLoadingProgress: false,
            total_qty: 0
        };
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.empSalesModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat);
        $scope.validationFactory = ValidationFactory;
        $scope.checked = false;
        $scope.showCategory = false;
        $scope.openDate = function (index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            } else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };

        $scope.searchFilter = {
            category_id: '',
            fromdate: '',
            todate: '',
            empInfo: ''
        };
        $scope.clearFilters = function ()
        {
            $scope.searchFilter = {
                category_id: '',
                fromdate: '',
                todate: '',
                empInfo: ''
            };
            $scope.checked = false;
            $scope.showCategory = false;
            $scope.empSalesModel.list = [];
        }

        $scope.print = function (div)
        {
            var docHead = document.head.outerHTML;
            var printContents = document.getElementById(div).outerHTML;
            var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=865, height=600, resizable=yes, screenX=200, screenY=200, personalbar=no, scrollbars=yes";

            var newWin = window.open("", "_blank", winAttr);
            var writeDoc = newWin.document;
            writeDoc.open();
            writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
            writeDoc.close();
            newWin.focus();
        };

        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.show = function (index)
        {

            if ($scope.showDetails[index])
            {
                $scope.showDetails[index] = false;
            } else
                $scope.showDetails[index] = true;
        };

        $scope.getList = function () {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'empsalesreport';
            getListParam.from_date = '';
            getListParam.to_date = '';
            if ($scope.searchFilter.fromdate != null && $scope.searchFilter.fromdate != '')
            {
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.adminService.appConfig.date_format);
                }
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.adminService.appConfig.date_format);
            }
            if ($scope.searchFilter.todate != null && $scope.searchFilter.todate != '')
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.adminService.appConfig.date_format);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.adminService.appConfig.date_format);
            }
            if ($scope.searchFilter.empInfo != '' && $scope.searchFilter.empInfo != null && $scope.searchFilter.empInfo != undefined && $scope.searchFilter.empInfo.id != '')
            {
                getListParam.emp_id = $scope.searchFilter.empInfo.id;
            } else {
                getListParam.emp_id = '';
            }
            $scope.empSalesModel.isLoadingProgress = true;
            $scope.showDetails = [];
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getEmployeeSalesReport(getListParam, configOption, headers).then(function (response)
            {
                var totalMrp = 0.00;
                var totalQty = 0.00;
                var totbillAmt = 0.00;
                $scope.empSalesModel.total_Amount = 0.00;
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.empSalesModel.list = data;
                    for (var i = 0; i < $scope.empSalesModel.list.length; i++)
                    {
                        totalMrp = parseFloat(totalMrp) + parseFloat($scope.empSalesModel.list[i].total_amount == null ? 0 : $scope.empSalesModel.list[i].total_amount);
                        totalQty = parseFloat(totalQty) + parseFloat($scope.empSalesModel.list[i].total_qty == null ? 0 : $scope.empSalesModel.list[i].total_qty);
                        totbillAmt = parseFloat(totbillAmt) + parseFloat($scope.empSalesModel.list[i].bill_value == null ? 0 : $scope.empSalesModel.list[i].bill_value);
                        $scope.empSalesModel.list[i].total_amount = parseFloat($scope.empSalesModel.list[i].total_amount).toFixed(2);
                        $scope.empSalesModel.list[i].total_amount = utilityService.changeCurrency($scope.empSalesModel.list[i].total_amount, $rootScope.appConfig.thousand_seperator);
                        $scope.empSalesModel.list[i].flag = i;
                        $scope.showDetails[i] = false;
                    }
                    $scope.empSalesModel.total_Amount = totalMrp;
                    $scope.empSalesModel.total_Amount = parseFloat($scope.empSalesModel.total_Amount).toFixed(2);
                    $scope.empSalesModel.total_Amount = utilityService.changeCurrency($scope.empSalesModel.total_Amount, $rootScope.appConfig.thousand_seperator);
                    $scope.empSalesModel.total_Bill_Amount = totbillAmt;
                    $scope.empSalesModel.total_Bill_Amount = parseFloat($scope.empSalesModel.total_Bill_Amount).toFixed(2);
                    $scope.empSalesModel.total_Bill_Amount = utilityService.changeCurrency($scope.empSalesModel.total_Bill_Amount, $rootScope.appConfig.thousand_seperator);
                    $scope.empSalesModel.total_qty = totalQty;
                    $scope.empSalesModel.total_qty = parseFloat($scope.empSalesModel.total_qty);
                }
                $scope.empSalesModel.isLoadingProgress = false;
            });
        };
        $scope.getEmployeeList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.EMPLOYEE_LIST_ALL, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatEmployeeModel = function (model)
        {
            if (model != null && model != undefined && model != '')
            {
                return model.name + '(' + model.employee_code + ')';
            }
            return  '';
        };

        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'employee_sales_report_form';
        $scope.$on('onLocalStorageFormRenterCompleteEvent', function (event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });

        $scope.$on('onLocalStorageFieldRenterCompleteEvent', function (event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRenterCount++;
                }
                if ($scope.localStorageRenterCount >= $scope.localStorageCount)
                {
                    console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                    $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
                }
            }
        });

        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function (event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRetrieveCount++;
                }
                if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
                {
                    $scope.initTableFilter();
                }
                console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);
            }
        });
        $scope.$on('updateTypeaheadFieldValue', function (event, data) {

            if (data.fieldName == "employee_sales_report_form-employee_dropdown")
            {
                $scope.searchFilter.empInfo = data.value;
            }
        });

        $scope.init = function ()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');
        };

        $scope.$on('onLocalStorageReadyEvent', function () {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                $timeout(function () {
                    $scope.init();
                }, 300);
            }
        });
        //$scope.getList();
    }]);




