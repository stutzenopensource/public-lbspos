app.controller('hsncodeAddCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$timeout', '$filter', 'Auth', 'ValidationFactory', '$state', '$httpService', 'APP_CONST', function($scope, $rootScope, adminService, utilityService, $timeout, $filter, Auth, ValidationFactory, $state, $httpService, APP_CONST) {

        $scope.hsnModel = {
            "id": 0,
            "hsn_code": "",
            "type": "",
            "description": "",
            "hsnTaxMapping": [],
            "isactive": 1,
            currentPage: 1,
            total: 0,
            role: '',
            isLoadingProgress: false
        }

        $scope.validationFactory = ValidationFactory;
        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.create_hsncode_form != 'undefined' && typeof $scope.create_hsncode_form.$pristine != 'undefined' && !$scope.create_hsncode_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.formReset = function()
        {
            $scope.create_hsncode_form.$setPristine();
            $scope.hsnModel = {
                "id": 0,
                "hsn_code": "",
                "type": "",
                "description": "",
                "hsnTaxMapping": [],
                "isactive": 1,
                currentPage: 1,
                total: 0,
                role: '',
                isLoadingProgress: false
            }
        }

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.createUser, 300);
        }

        $scope.getTaxList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.tax_name = val;
            autosearchParam.is_group = 1;
            return $httpService.get(APP_CONST.API.TAX_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatTaxModel = function (model) {

            if (model !== null && model != undefined)
            {

                return model.tax_name;
            }
            return  '';
        };
        $scope.addTaxInfo = function (fromPrice)
        {
            var newRow =
            {
                from_price: 0.00,
                to_price: 0.00,
                taxInfo: {}
            };
            if(typeof fromPrice != "undefined")
            {
                newRow.from_price = parseFloat(fromPrice)+parseFloat(1);
            }
            $scope.hsnModel.hsnTaxMapping.push(newRow);
            for(var i = 0 ; i<$scope.hsnModel.hsnTaxMapping.length;i++)
            {
                $scope.hsnModel.hsnTaxMapping[i].index = i;
            }
        };
        $scope.updateNextRow = function(taxInfo,index)
        {
            if(typeof $scope.hsnModel.hsnTaxMapping[index+1] == "undefined" || $scope.hsnModel.hsnTaxMapping[index+1].from_price == '' || $scope.hsnModel.hsnTaxMapping[index+1].from_price == null || typeof $scope.hsnModel.hsnTaxMapping[index+1].from_price  == "undefined" )
            {
                $scope.addTaxInfo(taxInfo.to_price);
            }
        }
        $scope.checkTaxInfo = function(taxInfo,index,toPrice)
        {
            taxInfo.taxInfo = {};
            if(typeof $scope.hsnModel.hsnTaxMapping[index+1] != "undefined" && $scope.hsnModel.hsnTaxMapping.length > index)
            {
                $scope.hsnModel.hsnTaxMapping[index+1].from_price = parseFloat(toPrice)+parseFloat(1);
            }
        }
        $scope.deleteTaxInfo = function(index)
        {
            for (var i = 0; i < $scope.hsnModel.hsnTaxMapping.length; i++)
            {
                if (index == $scope.hsnModel.hsnTaxMapping[i].index)
                {
                    $scope.hsnModel.hsnTaxMapping.splice(i, 1);
                }
            }
        }
//        $scope.addTaxInfo();
        $scope.createHsncode = function()
        {
            if ($scope.isDataSavingProcess == false)
            {
                $scope.isDataSavingProcess = true;
                var createHsnParam = {};
                var headers = {};
                headers['screen-code'] = 'hsncode';
                createHsnParam.hsn_code = $scope.hsnModel.hsn_code;
                createHsnParam.type = $scope.hsnModel.type;
                createHsnParam.description = $scope.hsnModel.description;
                createHsnParam.is_approved = 0;            
                createHsnParam.is_active = 1;
                createHsnParam.taxDetail = [];
                createHsnParam.hsnTaxMapping = [];
                if($scope.hsnModel.hsnTaxMapping.length > 0)
                {
                    for(var i = 0 ; i<$scope.hsnModel.hsnTaxMapping.length;i++ )
                    {
                        var taxInfo = {
                            from_price: $scope.hsnModel.hsnTaxMapping[i].from_price,
                            to_price: $scope.hsnModel.hsnTaxMapping[i].to_price,
                            group_tax_id: $scope.hsnModel.hsnTaxMapping[i].taxInfo.id,
                            group_tax_ids: $scope.hsnModel.hsnTaxMapping[i].taxInfo.group_tax_ids,
                            intra_group_tax_ids: $scope.hsnModel.hsnTaxMapping[i].taxInfo.intra_group_tax_ids
                        };
                        createHsnParam.hsnTaxMapping.push(taxInfo);
                    }
                }
                createHsnParam.is_sso = 0;
                adminService.saveHSNCode(createHsnParam, headers).then(function(response)
                {
                    if (response.data.success == true)
                    {
                        $scope.formReset();
                        $state.go('app.hsncode')
                    }
                    $scope.isDataSavingProcess = false;
                });
            }
        };
    }]);




