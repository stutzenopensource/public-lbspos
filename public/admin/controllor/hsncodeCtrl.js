app.controller('hsncodeCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$timeout', function($scope, $rootScope, adminService, $filter, Auth, $state, $timeout) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.hsnModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
            id: '',
            status: '',
            total:'',
                    isLoadingProgress: false
        };
        $scope.pagePerCount = [50, 100];
        $scope.hsnModel.limit = $scope.pagePerCount[0];
        $scope.searchFilter = {
            hsn_code: ''
        };
        $scope.searchFilterValue = "";
        $scope.refreshScreen = function()
        {
            $scope.searchFilter = {
                hsn_code: ''

            };
            $scope.initTableFilter();
        }

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }

        $scope.selectUserId = '';
        $scope.showdeletePopup = false;
        $scope.isdeleteUserroleLoadingProgress = false;
        $scope.showPopup = function(id)
        {
            $scope.selectUserId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function()
        {
            $scope.showdeletePopup = false;
             $scope.isdeleteUserroleLoadingProgress = false;
        };

        $scope.deleteUserRoleInfo = function( )
        {
            if( $scope.isdeleteUserroleLoadingProgress == false)
            {
            $scope.isdeleteUserroleLoadingProgress = true;
            var getListParam = {};
            getListParam.id = $scope.selectUserId;
            var headers = {};
            headers['screen-code'] = 'role';
            adminService.deleteUserRoleInfo(getListParam, getListParam.id, headers).then(function(response)
            {
                var data = response.data;
                if (data.success == true)
                {
                    $scope.closePopup();
                    $scope.getList();
                }
                $scope.isdeleteUserroleLoadingProgress = false;
            });
        }
        };

        $scope.getList = function()
        {
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'role';
            getListParam.id = '';
            getListParam.hsn_code = $scope.searchFilter.hsn_code;
            getListParam.is_active = 1;    
            getListParam.start = ($scope.hsnModel.currentPage - 1) * $scope.hsnModel.limit;
            getListParam.limit = $scope.hsnModel.limit;
            $scope.hsnModel.isLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getHSNList(getListParam, configOption, headers).then(function(response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.hsnModel.list = data.list;
                    $scope.hsnModel.total = data.total;
                }
                $scope.hsnModel.isLoadingProgress = false;
            });
        };
        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'user_role';


        $scope.$on('onLocalStorageFormRenterCompleteEvent', function(event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });  
        
          $scope.$on('onLocalStorageFieldRenterCompleteEvent', function(event, formName) {
             if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRenterCount++;
                }
                if ($scope.localStorageRenterCount >= $scope.localStorageCount)
                {
                    console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                    $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
                }
            
              }
        });
               
               $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function(event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRetrieveCount++;
                }
                if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
                {
                    $scope.initTableFilter();
                }
                console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);
            }
        });

        $scope.init = function()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');

        };

        $scope.$on('onLocalStorageReadyEvent', function() {
           if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                $timeout(function() {
                    $scope.init();
                }, 300);
            
            }
        });            

        //$scope.getList();
    }]);








