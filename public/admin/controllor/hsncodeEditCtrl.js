app.controller('hsncodeEditCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$timeout', '$filter', 'Auth', 'ValidationFactory', '$state', '$httpService', 'APP_CONST', '$stateParams', function($scope, $rootScope, adminService, utilityService, $timeout, $filter, Auth, ValidationFactory, $state, $httpService, APP_CONST, $stateParams) {

        $scope.hsnModel = {
            "id": 0,
            "hsn_code": "",
            "type": "",
            "description": "",
            "hsnTaxMapping": [],
            list:[],
            "isactive": 1,
            currentPage: 1,
            total: 0,
            role: '',
            isLoadingProgress: false
        }

        $scope.validationFactory = ValidationFactory;
        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.edit_hsncode_form != 'undefined' && typeof $scope.edit_hsncode_form.$pristine != 'undefined' && !$scope.edit_hsncode_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.formReset = function()
        {
            $scope.edit_hsncode_form.$setPristine();
            
        }

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.createUser, 300);
        }
        $scope.updateHsnDetails = function ()
        {
            $scope.hsnModel.hsn_code = $scope.hsnModel.list.hsn_code;
            $scope.hsnModel.type = $scope.hsnModel.list.type;
            $scope.hsnModel.description = $scope.hsnModel.list.description;
            $scope.hsnModel.is_approved = $scope.hsnModel.list.is_approved;
            $scope.hsnModel.hsnTaxMapping = [];
            if($scope.hsnModel.list.hsnTaxMapping.length > 0)
            {
                for(var i = 0 ; i<$scope.hsnModel.list.hsnTaxMapping.length;i++)
                {
                    var taxInfo = 
                    {
                        from_price :$scope.hsnModel.list.hsnTaxMapping[i].from_price,
                        to_price :$scope.hsnModel.list.hsnTaxMapping[i].to_price,
                        taxInfo :
                        {
                            id :$scope.hsnModel.list.hsnTaxMapping[i].group_tax_id,
                            tax_name :$scope.hsnModel.list.hsnTaxMapping[i].tax_name,
                            tax_percentage :$scope.hsnModel.list.hsnTaxMapping[i].tax_percentage
                        }
                    };
                    $scope.hsnModel.hsnTaxMapping.push(taxInfo);
                }
            }
        }
        $scope.getTaxList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.tax_name = val;
            autosearchParam.is_group = 1;
            return $httpService.get(APP_CONST.API.TAX_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatTaxModel = function (model) {

            if (model !== null && model != undefined)
            {

                return model.tax_name;
            }
            return  '';
        };
        $scope.addTaxInfo = function (fromPrice)
        {
            var newRow =
            {
                from_price: 0.00,
                to_price: 0.00,
                taxInfo: {}
            };
            if(typeof fromPrice != "undefined")
            {
                newRow.from_price = parseFloat(fromPrice)+parseFloat(1);
            }
            $scope.hsnModel.hsnTaxMapping.push(newRow);
            for(var i = 0 ; i<$scope.hsnModel.hsnTaxMapping.length;i++)
            {
                $scope.hsnModel.hsnTaxMapping[i].index = i;
            }
        };
        $scope.updateNextRow = function(taxInfo,index)
        {
            if(typeof $scope.hsnModel.hsnTaxMapping[index+1] == "undefined" || $scope.hsnModel.hsnTaxMapping[index+1].from_price == '' || $scope.hsnModel.hsnTaxMapping[index+1].from_price == null || typeof $scope.hsnModel.hsnTaxMapping[index+1].from_price  == "undefined" )
            {
                $scope.addTaxInfo(taxInfo.to_price);
            }
        }
        $scope.checkTaxInfo = function(taxInfo,index,toPrice)
        {
            taxInfo.taxInfo = {};
            if(typeof $scope.hsnModel.hsnTaxMapping[index+1] != "undefined" && $scope.hsnModel.hsnTaxMapping.length > index)
            {
                $scope.hsnModel.hsnTaxMapping[index+1].from_price = parseFloat(toPrice)+parseFloat(1);
            }
        }
        $scope.deleteTaxInfo = function(index)
        {
            for (var i = 0; i < $scope.hsnModel.hsnTaxMapping.length; i++)
            {
                if (index == $scope.hsnModel.hsnTaxMapping[i].index)
                {
                    $scope.hsnModel.hsnTaxMapping.splice(i, 1);
                }
            }
        }
        $scope.modifyHsncode = function()
        {
            if ($scope.isDataSavingProcess == false)
            {
                $scope.isDataSavingProcess = true;
                var modifyHsnParam = {};
                var headers = {};
                headers['screen-code'] = 'hsncode';
                modifyHsnParam.hsn_code = $scope.hsnModel.hsn_code;
                modifyHsnParam.type = $scope.hsnModel.type;
                modifyHsnParam.description = $scope.hsnModel.description;
                modifyHsnParam.is_approved = $scope.hsnModel.is_approved;            
                modifyHsnParam.is_active = 1;
                modifyHsnParam.taxDetail = [];
                modifyHsnParam.hsnTaxMapping = [];
                if($scope.hsnModel.hsnTaxMapping.length > 0)
                {
                    for(var i = 0 ; i<$scope.hsnModel.hsnTaxMapping.length;i++ )
                    {
                        var taxInfo = {
                            from_price: $scope.hsnModel.hsnTaxMapping[i].from_price,
                            to_price: $scope.hsnModel.hsnTaxMapping[i].to_price,
                            group_tax_id: $scope.hsnModel.hsnTaxMapping[i].taxInfo.id,
                            group_tax_ids: $scope.hsnModel.hsnTaxMapping[i].taxInfo.group_tax_ids,
                            intra_group_tax_ids: $scope.hsnModel.hsnTaxMapping[i].taxInfo.intra_group_tax_ids
                        };
                        modifyHsnParam.hsnTaxMapping.push(taxInfo);
                    }
                }
                createHsnParam.is_sso = 0;
                adminService.editHSNCode(modifyHsnParam,$stateParams.id, headers).then(function(response)
                {
                    if (response.data.success == true)
                    {
                        $scope.formReset();
                        $state.go('app.hsncode')
                    }
                    $scope.isDataSavingProcess = false;
                });
            }
        };
        $scope.getHsnInfo = function()
        {
            var getListParam = {};
            getListParam.id = $stateParams.id;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getHSNList(getListParam,configOption).then(function(response)
                {
                    if (response.data.success == true)
                    {
                        var data = response.data;
                        $scope.hsnModel.list = data.list[0];
                        $scope.updateHsnDetails();
                    }
                    $scope.isDataSavingProcess = false;
                });
        }
        $scope.getHsnInfo ();
    }]);




