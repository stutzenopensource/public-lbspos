


app.controller('incomecategoryListCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$timeout', function($scope, $rootScope, adminService, $filter, Auth, $state, $timeout) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.incomeCategoryModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
            status: '',
            isLoadingProgress: false
        };

        $scope.pagePerCount = [50, 100];
        $scope.incomeCategoryModel.limit = $scope.pagePerCount[0];

        $scope.searchFilter = {
            name: ''
        };

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }
//        $scope.refreshScreen = function()
//        {
//            $scope.initTableFilter();
//        }

        $scope.selectCategoryId = '';
        $scope.showdeletePopup = false;
        $scope.isdeleteCategoryLoadingProgress = false;
        $scope.showPopup = function(id)
        {
            $scope.selectCategoryId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteCategoryLoadingProgress = false;
        };

        $scope.deleteCategoryInfo = function( )
        {
            if($scope.isdeleteCategoryLoadingProgress == false)
            {
            $scope.isdeleteCategoryLoadingProgress = true;
            var getListParam = {};
            getListParam.id = $scope.selectCategoryId;
            var headers = {};
            headers['screen-code'] = 'incomecategory';
            adminService.deleteIncomeCategory(getListParam, getListParam.id, headers).then(function(response)
            {
                var data = response.data;
                if (data.success == true)
                {
                    $scope.closePopup();
                    $scope.getList();
                }
                $scope.isdeleteCategoryLoadingProgress = false;
            });
        } 
        };

        $scope.getList = function() {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'incomecategory';
            getListParam.id = '';
            getListParam.is_active = 1;
            getListParam.type = 'income';
            //  getListParam.name = $scope.searchFilter.name;
            getListParam.start = ($scope.incomeCategoryModel.currentPage - 1) * $scope.incomeCategoryModel.limit;
            getListParam.limit = $scope.incomeCategoryModel.limit;
            $scope.incomeCategoryModel.isLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getIncomeCategoryList(getListParam, configOption, headers).then(function(response) {
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.incomeCategoryModel.list = data;
                    $scope.incomeCategoryModel.total = response.data.total;
                }
                $scope.incomeCategoryModel.isLoadingProgress = false;
            });

        };


        $scope.getList();
    }]);








