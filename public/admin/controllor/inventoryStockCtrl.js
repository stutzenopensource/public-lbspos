app.controller('inventoryStockCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', '$timeout', '$window', 'APP_CONST', 'sweet', function ($scope, $rootScope, adminService, $httpService, $filter, Auth, $timeout, $window, APP_CONST, sweet) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.inventoryStockModel = {
            scopeId: "",
            configId: 5,
            type: "",
            currentPage: 1,
            total: 0,
            limit: 4,
            list: [],
            categoryList: [],
            status: '',
            currencyFormat: '',
            serverList: null,
            isLoadingProgress: true,
            qty: '',
            downqty: '',
            purchaseid: ''
        };

        $scope.searchFilterNameValue = ''

        $scope.pagePerCount = [50, 100];
        $scope.inventoryStockModel.limit = $scope.pagePerCount[0];
        $scope.currentDate = new Date();
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
        $scope.searchFilter = {
            miniquantity: '',
            name: '',
            category: '',
            miniquantity_name: '',
            category_name: ''
        };
        $scope.checked = false;
        $scope.showCategory = false;
        $scope.clearFilters = function ()
        {
            $scope.searchFilter = {
                miniquantity: '',
                name: '',
                category: '',
                miniquantity_name: '',
                category_name: ''
            };
            $scope.checked = false;
            $scope.showCategory = false;
            //$scope.searchFilter.productInfo = {};
            //$scope.inventoryStockModel.list = [];
            $scope.initTableFilter();
        }
        $scope.refreshScreen = function ()
        {
            $scope.clearFilters();
        }
        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function (type)
        {
            if (type == "qty")
            {
                if ($scope.searchFilter.miniquantity == 1)
                {
                    $scope.searchFilter.miniquantity_name = "More than Minimum Stock"
                }
                if ($scope.searchFilter.miniquantity == 2)
                {
                    $scope.searchFilter.miniquantity_name = "Less than Minimum Stock"
                }
            }
            if (type == "category")
            {
                for (var i = 0; i < $scope.inventoryStockModel.categoryList.length; i++)
                {
                    if ($scope.inventoryStockModel.categoryList[i].id == $scope.searchFilter.category)
                    {
                        $scope.searchFilter.category_name = $scope.inventoryStockModel.categoryList[i].name;
                    }
                }
            }
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getInventoryStockListInfo, 300);
        }

        $scope.showLabel = false;
        $scope.productIndex = '';
        $scope.purchaseIndex = '';
        $scope.showLabelPopup = function ($index, $purindex)
        {
            $scope.productIndex = $index;
            $scope.purchaseIndex = $purindex;
            $scope.showLabel = true;
            if ($rootScope.appConfig.sales_product_list.toUpperCase() == 'PRODUCT BASED')
            {
                $scope.inventoryStockModel.price = $scope.inventoryStockModel.list[$scope.productIndex].sales_price;
                $scope.inventoryStockModel.qty = $scope.inventoryStockModel.list[$scope.productIndex].quantity;
            } else if ($rootScope.appConfig.sales_product_list.toUpperCase() == 'PURCHASE BASED')
            {
                $scope.inventoryStockModel.price = $scope.inventoryStockModel.list[$scope.productIndex].data[$scope.purchaseIndex].selling_price;
                $scope.inventoryStockModel.qty = $scope.inventoryStockModel.list[$scope.productIndex].data[$scope.purchaseIndex].qty_in_hand;
            }
        }

        $scope.closePopup = function ()
        {
            $scope.productIndex = '';
            $scope.purchaseIndex = '';
            $scope.showLabel = false;
            $scope.showQty = false;
            $scope.showDownQty = false;
        }


        $scope.showQty = false;
        $scope.showqty = function ()
        {
            $scope.showQty = true;
        }
        $scope.showDownQty = false;
        $scope.showDownloadQty = function ()
        {
            $scope.showDownQty = true;
            if ($rootScope.appConfig.sales_product_list.toUpperCase() == 'PRODUCT BASED')
            {
                $scope.inventoryStockModel.purchaseid = $scope.inventoryStockModel.list[$scope.productIndex].id;
                $scope.inventoryStockModel.downqty = $scope.inventoryStockModel.list[$scope.productIndex].quantity;
            } else if ($rootScope.appConfig.sales_product_list.toUpperCase() == 'PURCHASE BASED')
            {
                $scope.inventoryStockModel.purchaseid = $scope.inventoryStockModel.list[$scope.productIndex].data[$scope.purchaseIndex].bcnId;
                $scope.inventoryStockModel.downqty = $scope.inventoryStockModel.list[$scope.productIndex].data[$scope.purchaseIndex].qty_in_hand;
            }
        }

        $scope.enableButtons = function ()
        {
            $scope.showQty = false;
            $scope.showDownQty = false;
        }

        $scope.print = function ()
        {
            $window.print();
        };
        $scope.formatproductlistModel = function (model) {
            if (model != null)
            {
                return model.name;
            }
            return  '';
        };
        $scope.show = function (index)
        {

            if ($scope.showDetails[index])
            {
                $scope.showDetails[index] = false;
            } else
                $scope.showDetails[index] = true;

        };

        $scope.getproductlist = function (val) {

            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.type = "Product";
            return $httpService.get(APP_CONST.API.PRODUCT_LIST, autosearchParam, false).then(function (responseData) {
                var data = responseData.data.list;
                if (data.success == true)
                {

                }
            });
        };
        $scope.getCategoryList = function () {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'category';
            getListParam.name = $scope.searchFilter.category;
            getListParam.is_active = 1;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getCategoryList(getListParam, configOption, headers).then(function (response) {
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.inventoryStockModel.categoryList = data;
                }
                $scope.inventoryStockModel.isLoadingProgress = false;
            });
        };
        $scope.relabelSaveProcess = false;
        $scope.saveRelabel = function ( )
        {
            if ($rootScope.appConfig.sales_product_list.toUpperCase() == 'PRODUCT BASED')
            {
                if (parseFloat($scope.inventoryStockModel.qty) <= $scope.inventoryStockModel.list[$scope.productIndex].quantity)
                {
                    $scope.callRelabel();
                } else
                {
                    sweet.show('Oops...', 'Quantity Exceeds Stock.', 'error');
                }
            } else if ($rootScope.appConfig.sales_product_list.toUpperCase() == 'PURCHASE BASED')
            {
                if (parseFloat($scope.inventoryStockModel.qty) <= $scope.inventoryStockModel.list[$scope.productIndex].data[$scope.purchaseIndex].qty_in_hand)
                {
                    $scope.callRelabel();
                } else
                {
                    sweet.show('Oops...', 'Quantity Exceeds Stock.', 'error');
                }
            }

        };

        $scope.callRelabel = function ()
        {
            if ($scope.relabelSaveProcess == false)
            {
                $scope.relabelSaveProcess = true;
                var saveRelabelParam = {};
                saveRelabelParam.bcn_id = '';
                if ($rootScope.appConfig.sales_product_list.toUpperCase() == 'PRODUCT BASED')
                {
                    saveRelabelParam.bcn_id = $scope.inventoryStockModel.list[$scope.productIndex].bcn_id;
                    saveRelabelParam.qty_in_hand = $scope.inventoryStockModel.list[$scope.productIndex].quantity;
                    saveRelabelParam.selling_price = $scope.inventoryStockModel.price;
                    saveRelabelParam.purchase_price = '';
                    saveRelabelParam.mrp_price = $scope.inventoryStockModel.list[$scope.productIndex].mrp_price;
                    saveRelabelParam.sales_qty = $scope.inventoryStockModel.qty;
                    saveRelabelParam.discount_mode = '';
                    saveRelabelParam.discount_value = '';
                    saveRelabelParam.discount_amount = '';
                    saveRelabelParam.uom_id = $scope.inventoryStockModel.list[$scope.productIndex].uom_id;
                    saveRelabelParam.uom_name = $scope.inventoryStockModel.list[$scope.productIndex].uom;
                    saveRelabelParam.product_name = $scope.inventoryStockModel.list[$scope.productIndex].name;
                    saveRelabelParam.product_id = $scope.inventoryStockModel.list[$scope.productIndex].id;
                    saveRelabelParam.product_sku = $scope.inventoryStockModel.list[$scope.productIndex].sku;
                    saveRelabelParam.is_active = $scope.inventoryStockModel.list[$scope.productIndex].is_active;
                } else if ($rootScope.appConfig.sales_product_list.toUpperCase() == 'PURCHASE BASED')
                {
                    saveRelabelParam.purchase_invoice_id = $scope.inventoryStockModel.list[$scope.productIndex].data[$scope.purchaseIndex].purchase_invoice_id;
                    saveRelabelParam.bcn_id = $scope.inventoryStockModel.list[$scope.productIndex].data[$scope.purchaseIndex].bcnId;
                    saveRelabelParam.qty_in_hand = $scope.inventoryStockModel.list[$scope.productIndex].data[$scope.purchaseIndex].qty_in_hand;
                    saveRelabelParam.selling_price = $scope.inventoryStockModel.price;
                    saveRelabelParam.purchase_price = $scope.inventoryStockModel.list[$scope.productIndex].data[$scope.purchaseIndex].purchase_price;
                    saveRelabelParam.mrp_price = $scope.inventoryStockModel.list[$scope.productIndex].data[$scope.purchaseIndex].mrp_price;
                    saveRelabelParam.sales_qty = $scope.inventoryStockModel.qty;
                    saveRelabelParam.discount_mode = $scope.inventoryStockModel.list[$scope.productIndex].data[$scope.purchaseIndex].discount_mode;
                    saveRelabelParam.discount_value = $scope.inventoryStockModel.list[$scope.productIndex].data[$scope.purchaseIndex].discount_value;
                    saveRelabelParam.discount_amount = $scope.inventoryStockModel.list[$scope.productIndex].data[$scope.purchaseIndex].discount_amount;
                    saveRelabelParam.uom_id = $scope.inventoryStockModel.list[$scope.productIndex].data[$scope.purchaseIndex].uom_id;
                    saveRelabelParam.uom_name = $scope.inventoryStockModel.list[$scope.productIndex].data[$scope.purchaseIndex].uom;
                    saveRelabelParam.product_name = $scope.inventoryStockModel.list[$scope.productIndex].data[$scope.purchaseIndex].product_name;
                    saveRelabelParam.product_id = $scope.inventoryStockModel.list[$scope.productIndex].data[$scope.purchaseIndex].product_id;
                    saveRelabelParam.product_sku = $scope.inventoryStockModel.list[$scope.productIndex].data[$scope.purchaseIndex].product_sku;
                    saveRelabelParam.is_active = $scope.inventoryStockModel.list[$scope.productIndex].data[$scope.purchaseIndex].is_active;

                }

                var headers = {};
                headers['screen-code'] = 'inventorystock';
                adminService.saveRelabel(saveRelabelParam, headers).then(function (response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.productIndex = '';
                        $scope.purchaseIndex = '';
                        $scope.closePopup();
                        $scope.getInventoryStockListInfo();
                    }
                    $scope.relabelSaveProcess = false;

                });
            }
        }

        $scope.downloadSaveProcess = false;
        $scope.downloadCurrentLabel = function ()
        {
            $scope.downloadSaveProcess = true;
            var downloadLabelParam = {};
            if ($rootScope.appConfig.sales_product_list.toUpperCase() == 'PRODUCT BASED')
            {
                downloadLabelParam.id = $scope.inventoryStockModel.list[$scope.productIndex].id;
                downloadLabelParam.type = 'inventory';
                downloadLabelParam.qty = $scope.inventoryStockModel.downqty;
            } else if ($rootScope.appConfig.sales_product_list.toUpperCase() == 'PURCHASE BASED')
            {
                downloadLabelParam.id = $scope.inventoryStockModel.list[$scope.productIndex].data[$scope.purchaseIndex].id;
                downloadLabelParam.type = 'inventory';
                downloadLabelParam.qty = $scope.inventoryStockModel.downqty;
            }
            var headers = {};
            headers['screen-code'] = 'inventorystock';
            adminService.downloadCurrentLabel(downloadLabelParam, headers).then(function (response)
            {
                var data = response.data;
                if (data.success == true)
                {
                    $scope.closePopup();
                }

            });
            $scope.downloadSaveProcess = false;
        }

        $scope.getInventoryStockListInfo = function () {

            $scope.inventoryStockModel.isLoadingProgress = true;
            var materialListParam = {};
            var headers = {};
            headers['screen-code'] = 'inventorystock';
            materialListParam.id = '';
            if ($scope.checked)
            {
                $scope.showCategory = true;
                materialListParam.show_all = 1;
            } else
            {
                $scope.showCategory = false;
            }
            if ($rootScope.appConfig.sales_product_list.toUpperCase() == 'PURCHASE BASED')
            {
                materialListParam.type = 1;
            }
            materialListParam.product_name = $scope.searchFilter.name;
            materialListParam.category_id = $scope.searchFilter.category;
            materialListParam.mode = $scope.searchFilter.miniquantity;
            if ($scope.searchFilter.miniquantity == 2)
            {
                materialListParam.min_stock_qty = $scope.searchFilter.miniquantity > 2;
            } else if ($scope.searchFilter.miniquantity == 1)
            {
                materialListParam.min_stock_qty = $scope.searchFilter.miniquantity < 1;
            } else
            {
                materialListParam.min_stock_qty = '';
            }
            $scope.showDetails = [];
            adminService.getInventoryStockListInfo(materialListParam, headers).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.inventoryStockModel.list = data.list;
                    for (var i = 0; i < $scope.inventoryStockModel.list.length; i++)
                    {
                        $scope.inventoryStockModel.list[i].diff_stock_qty = parseFloat($scope.inventoryStockModel.list[i].quantity - $scope.inventoryStockModel.list[i].min_stock_qty);
                        $scope.inventoryStockModel.list[i].diff_stock_qty = parseFloat($scope.inventoryStockModel.list[i].diff_stock_qty);
                        if ($scope.inventoryStockModel.list[i].quantity == '' && $scope.inventoryStockModel.list[i].quantity == null && $scope.inventoryStockModel.list[i].quantity == undefined)
                        {
                            $scope.inventoryStockModel.list[i].quantity = 0.00;
                        }
                        if ($scope.inventoryStockModel.list[i].quantity != '' && $scope.inventoryStockModel.list[i].quantity != null && $scope.inventoryStockModel.list[i].quantity != undefined)
                        {
                            $scope.inventoryStockModel.list[i].diff_qty_percentage = parseFloat($scope.inventoryStockModel.list[i].quantity / $scope.inventoryStockModel.list[i].min_stock_qty * 100);
                            if (isNaN($scope.inventoryStockModel.list[i].diff_qty_percentage))
                            {
                                $scope.inventoryStockModel.list[i].diff_qty_percentage = 0;
                            } else
                            {
                                $scope.inventoryStockModel.list[i].diff_qty_percentage = parseFloat($scope.inventoryStockModel.list[i].diff_qty_percentage).toFixed();
                            }
                        }
                        if (parseFloat($scope.inventoryStockModel.list[i].quantity) < parseFloat($scope.inventoryStockModel.list[i].min_stock_qty))
                        {
                            $scope.inventoryStockModel.list[i].isOrangeApplied = true;
                        } else
                        {
                            $scope.inventoryStockModel.list[i].isOrangeApplied = false;
                        }
                        $scope.inventoryStockModel.list[i].flag = i;
                        $scope.showDetails[i] = false;
                        if ($scope.checked)
                        {
                            for (var j = 0; j < $scope.inventoryStockModel.list[i].data.length; j++)
                            {
                                $scope.inventoryStockModel.list[i].data[j].flag = i;
                            }
                        }
                    }
                    $scope.inventoryStockModel.total = data.total;
                }
                $scope.inventoryStockModel.isLoadingProgress = false;
            });

        };

        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'inventory_stock_list_form';


        $scope.$on('onLocalStorageFormRenterCompleteEvent', function (event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });

        $scope.$on('onLocalStorageFieldRenterCompleteEvent', function (event, formName) {

            if ($scope.localStorageFormName == formName)
            {
                $scope.localStorageRenterCount++;
            }
            if ($scope.localStorageRenterCount >= $scope.localStorageCount)
            {
                console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
            }
        });
//        $scope.$on('updateTypeaheadFieldValue', function (event, data) {
//
//            if (data.fieldName == "inventory_stock_list_form-customer_dropdown")
//            {
//                $scope.searchFilter.customerInfo = data.value;
//            }
//        });



        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function (event, formName) {

            if ($scope.localStorageFormName == formName)
            {
                $scope.localStorageRetrieveCount++;
            }
            if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
            {
                $scope.initTableFilter();
            }
            console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);
        });

        $scope.init = function ()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');
        };

        $scope.$on('onLocalStorageReadyEvent', function () {
            $timeout(function () {
                $scope.init();
            }, 300);
        });


//        $scope.getInventoryStockListInfo();
        $scope.getCategoryList();
    }]);




