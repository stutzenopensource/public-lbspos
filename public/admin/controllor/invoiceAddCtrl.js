app.controller('invoiceAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', 'sweet', function ($scope, $rootScope, adminService, $httpService, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST, sweet) {

        $scope.orderListModel =
                {
                    "id": 0,
                    "productName": "",
                    "sku": "",
                    "unit": "",
                    "mrp": "",
                    "sales_price": "",
                    "qty": "",
                    "rowtotal": "",
                    "isActive": "1",
                    "total": 0,
                    "customername": "",
                    "phone": "",
                    "addressId": "",
                    "subtotal": '0.00',
                    "taxAmt": '0.00',
                    "taxInfo": "",
                    "is_next_autogenerator_num_changed": false,
                    "next_autogenerator_num": "",
                    "prefixList": [],
                    "discountAmt": '0.00',
                    list: [],
                    "billno": "",
                    "billdate": "",
                    "coupon": "",
                    "shipping": 0,
                    "discamount": 0,
                    "code": "",
                    "customerInfo": '',
                    "invoiceDetail": {},
                    "invoicePrefix": "",
                    "paymentTerm": "",
                    "taxPercentage": "",
                    "discountPercentage": "",
                    "totalDisPercentage": "0",

                    "totalTaxPercentage": "",
                    "duedate": "",
                    "balance_amount": "",
                    "paid_amount": "",
                    "tax_id": "",
                    "productList": [],
                    "productId": '',
                    "notes": '',
                    "discountMode": '%',
                    "roundoff": '0.00',
                    "invoiceno": '',
                    "taxAmountDetail": {
                        "tax_amount": ''
                    },
                    //"taxAmountDetail": '',
                    "subtotalWithDiscount": '',
                    "round": '0.00',
                    "dealId": '',
                    "dealName": '',
                    "taxMappingDetail": [],
                    "activeTaxList": [],
                    "gst_no": ""
                };
        $scope.adminService = adminService;
        $scope.orderListModel.gst_no = adminService.appConfig.customer_gst_no;
        $scope.customAttributeList = [];
//        $scope.orderListModel.invoicePrefix = adminService.appConfig.invoice_prefix;
        $scope.senderEmail = '';
        $scope.receiver = [];
        $scope.ccEmail = '';
        $scope.showPopup = function (index) {
            $scope.showpopup = true;
            $scope.mailHeader = "Send a Receipt";
            $scope.senderEmail = $scope.orderListModel.customerInfo.email;
            $scope.receiver = [];
            var newRow = {
                "email": $scope.orderListModel.customerInfo.email
            };
            $scope.receiver.push(newRow);
            $scope.ccEmail = $scope.orderListModel.customerInfo.email;

            $scope.$broadcast("initPopupEvent");
        };

        $scope.addMoreReceiverEmail = function ( )
        {
            var newRow = {
                "email": ""
            };
            $scope.receiver.push(newRow);
        }

        $scope.closePopup = function () {
            $scope.showpopup = false;
        }

        $scope.onFocus = function (e) {
            $timeout(function () {
                $(e.target).trigger('input');
            });
        };

        $scope.showItems = false;
        $scope.showAreaPopup = function ()
        {
            $scope.showItems = true;
        }
        $scope.showCustomerAddPopup = false;
        $scope.showCustomerPopup = function ()
        {
            $scope.showCustomerAddPopup = true;
            if (typeof $scope.orderListModel.customerInfo != undefined && $scope.orderListModel.customerInfo != '' && $scope.orderListModel.customerInfo != null)
            {
                $rootScope.$broadcast('customerInfo', $scope.orderListModel.customerInfo);
            }
        }
        $scope.closeCustomerPopup = function ()
        {
            $scope.showCustomerAddPopup = false;
        }

        $scope.$on("updateSavedCustomerDetail", function (event, customerDetail)
        {
            if (typeof customerDetail === 'undefined')
                return;
            $scope.orderListModel.customerInfo = customerDetail;
        });

        $scope.validationFactory = ValidationFactory;
        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function ()
        {
            if (typeof $scope.order_add_form != 'undefined' && typeof $scope.order_add_form.$pristine != 'undefined' && !$scope.order_add_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function () {

            $scope.order_add_form.$setPristine();
        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.resetOrderCreate = function () {

            $scope.orderListModel.user = '';
        }

        $scope.initTableFilterTimeoutPromise = null;
        $scope.deletedProductList = [];
        $scope.undeletedProductList = [];
        $scope.showbillPopup = false;
        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }
        $scope.maxDate = $scope.maxDate ? null : new Date();
        $scope.startDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currencyFormat = $rootScope.appConfig.currency;
        $scope.currentDate = new Date();
        $scope.orderListModel.billdate = $scope.currentDate;
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.openDate = function (index) {

            if (index == 0)
            {
                $scope.startDateOpen = true;
            } else if (index == 1)
            {
                $scope.toDateOpen = true;
            }
        }

        $scope.getCustomerList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            autosearchParam.mode = 1;
            return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = [];
                if (data.length > 0)
                {
                    for (var i = 0; i < data.length; i++)
                    {
                        if (data[i].is_sale == '1' || data[i].is_sale == 1)
                        {
                            hits.push(data[i]);
                        }
                    }
                }
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatCustomerModel = function (model)
        {
            if (model != null)
            {
                if (model.fname != undefined && model.phone != undefined && model.email != undefined)
                {
                    return model.fname + '(' + model.phone + ',' + model.email + ')';
                } else if (model.fname != undefined && model.phone != undefined)
                {
                    return model.fname + '(' + model.phone + ')';
                } else if (model.fname != undefined)
                {
                    return model.fname;
                }
            }
            return  '';
        };
        $scope.updateAddress = function ()
        {
            if ($scope.orderListModel.customerInfo.shopping_city != "" && $scope.orderListModel.customerInfo.shopping_city != null)
            {
                if ($scope.orderListModel.customerInfo.shopping_address != "")
                {
                    $scope.orderListModel.customerInfo.shopping_address = $scope.orderListModel.customerInfo.shopping_address + "," + $scope.orderListModel.customerInfo.shopping_city;
                } else
                {
                    $scope.orderListModel.customerInfo.shopping_address = $scope.orderListModel.customerInfo.shopping_city;
                }
            }
            if ($scope.orderListModel.customerInfo.shopping_state != "" && $scope.orderListModel.customerInfo.shopping_state != null)
            {
                if ($scope.orderListModel.customerInfo.shopping_address != "")
                {
                    $scope.orderListModel.customerInfo.shopping_address = $scope.orderListModel.customerInfo.shopping_address + "," + $scope.orderListModel.customerInfo.shopping_state;
                } else
                {
                    $scope.orderListModel.customerInfo.shopping_address = $scope.orderListModel.customerInfo.shopping_state;
                }
            }
            if ($scope.orderListModel.customerInfo.shopping_country != "" && $scope.orderListModel.customerInfo.shopping_country != null)
            {
                if ($scope.orderListModel.customerInfo.shopping_address != "")
                {
                    $scope.orderListModel.customerInfo.shopping_address = $scope.orderListModel.customerInfo.shopping_address + "," + $scope.orderListModel.customerInfo.shopping_country;
                } else
                {
                    $scope.orderListModel.customerInfo.shopping_address = $scope.orderListModel.customerInfo.shopping_country;
                }
            }
        }

        $scope.custDrop = function () {
            if ($scope.orderListModel.customerInfo.id == undefined) {
                $scope.orderListModel.customerInfo = '';
            }
        };
//        $scope.$watch('orderListModel.customerInfo', function (newVal, oldVal)
//        {
//            if (typeof newVal == 'undefined' || newVal == '' || newVal == null)
//            {
//                $scope.orderListModel.customerInfo = '';
//            } else if (typeof $stateParams.id == 'undefined' || $stateParams.id == null || $stateParams.id == '' || $stateParams.id == 0)
//            {
//                var addrss = "";
//
//                if (newVal.billing_address != undefined && newVal.billing_address != '')
//                {
//                    addrss += newVal.billing_address;
//                }
//                if (newVal.billing_city != undefined && newVal.billing_city != '')
//                {
//                    addrss += '\n' + newVal.billing_city;
//                }
//                if (newVal.billing_pincode != undefined && newVal.billing_pincode != '')
//                {
//                    if (newVal.billing_city != undefined && newVal.billing_city != '')
//                    {
//                        addrss += '-' + newVal.billing_pincode;
//                    } else
//                    {
//                        addrss += newVal.billing_pincode;
//                    }
//                }
//
//                if (newVal.billing_state != undefined && newVal.billing_state != '')
//                {
//                    addrss += '\n' + newVal.billing_state;
//                }
//
//                $scope.orderListModel.customerInfo.shopping_address = addrss;
//            }
//        });

        $scope.getEmpCodeList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.EMPLOYEE_LIST_ALL, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatCodeModel = function (model)
        {
            if (model != null && model != '' && model != undefined)
            {
                if (model.name != undefined && model.name != '')
                {
                    return model.employee_code + '(' + model.name + ')';
                } else {
                    return model.employee_code
                }
            }
            return  '';
        };

        $scope.empDrop = function (index) {

            if (typeof $scope.orderListModel.list[index].empCode == 'undefined') {
                $scope.orderListModel.list[index].empCode = '';
            }
        };
        $scope.barcodeDrop = function (index) {

            if ($scope.orderListModel.list[index].id == '') {
                $scope.orderListModel.list[index].productname = '';
            }
        };
        $scope.checkEmpty = function (index) {

            if ($scope.orderListModel.list[index].productname == '' || $scope.orderListModel.list[index].productname == null) {
                $scope.orderListModel.list[index].qty = '';
                $scope.orderListModel.list[index].sales_price = '';
                $scope.orderListModel.list[index].rowtotal = '';
                $scope.orderListModel.list[index].tax_name = '';
                $scope.orderListModel.list[index].taxAmount = '';
                $scope.orderListModel.list[index].discountPercentage = '';
                $scope.orderListModel.list[index].discountAmount = '';
            }
        };


        $scope.getUomList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            return $httpService.get(APP_CONST.API.UOM, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatUomModel = function (model)
        {
            if (model != null)
            {
                return model.name;
            }
            return  '';
        };

        //        $scope.isLoadedTax = false;
        //        $scope.getTaxListInfo = function ( )
        //        {
        //            $scope.isLoadedTax = false;
        //            var getListParam = {};
        //            getListParam.id = "";
        //            getListParam.name = "";
        //            getListParam.start = 0;
        //            getListParam.is_active = 1;
        //            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
        //
        //            adminService.getTaxList(getListParam, configOption).then(function (response)
        //            {
        //                var data = response.data.list;
        //                $scope.orderListModel.taxInfo = data;
        //                if (typeof $stateParams.id != 'undefined' && $stateParams.id != null && $stateParams.id != '' && $stateParams.id != 0)
        //                {
        //
        //                    $scope.orderListModel.tax_id = $scope.orderListModel.invoiceDetail.tax_id + '';
        //                } else
        //                {
        //                    if ($scope.orderListModel.taxInfo.length > 0) {
        //
        //                        $scope.orderListModel.tax_id = $scope.orderListModel.taxInfo[0].id + '';
        //                    }
        //
        //                }
        //                $scope.isLoadedTax = true;
        //            });
        //        };

        $scope.calculateDueDate = function ( )
        {
            if ($scope.orderListModel.paymentTerm != "")
            {
                var billdate = utilityService.parseStrToDate($scope.orderListModel.billdate);
                $scope.orderListModel.duedate = utilityService.incrementDate(billdate, $scope.orderListModel.paymentTerm);
            } else
            {
                $scope.orderListModel.duedate = $scope.orderListModel.billdate;
            }
        }

        $scope.deleteProduct = function (index)
        {
//            var index = -1;
//            for (var i = 0; i < $scope.orderListModel.list.length; i++) {
//                if (id == $scope.orderListModel.list[i].id) {
//                    index = i;
//                    break;
//                }
//            }
            $scope.orderListModel.list.splice(index, 1);
            $scope.calculatetotal();
            //$scope.calculateTaxAmt();
        };

        $scope.totalcalculationTimeoutPromise != null
        $scope.updateInvoiceTotal = function ()
        {
            if ($scope.totalcalculationTimeoutPromise != null)
            {
                $timeout.cancel($scope.totalcalculationTimeoutPromise);
            }
            $scope.totalcalculationTimeoutPromise = $timeout($scope.calculatetotal, 300);
        }

        $scope.initUpdateDetailTimeoutPromise = null;
        $scope.initUpdateDetail = function ()
        {
            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
            }
            if ($stateParams.id != "")
            {
                $scope.updateInvoiceDetails();
            }
        }

        $scope.showMailPopup = false;
        $scope.showPaymentPopup = function ()
        {
            $scope.showMailPopup = true;
            $scope.invoiceDateOpen = false;
            $scope.paymentAddModel.invoice_id = $scope.orderListModel.invoiceDetail.id;
            $scope.paymentAddModel.customer_id = $scope.orderListModel.invoiceDetail.customer_id;
            $scope.paymentAddModel.account = $scope.orderListModel.invoiceDetail.customer_fname;
            $scope.paymentAddModel.invoicedate = $filter('date')($scope.currentDate, $scope.dateFormat);
            $scope.paymentAddModel.amount = $scope.orderListModel.invoiceDetail.balance_amount;
            $scope.paymentAddModel.comments = $scope.orderListModel.invoiceDetail.comments;
        };

        $scope.closePaymentPopup = function ( )
        {
            $scope.showMailPopup = false;
        }

        $scope.getProductList = function ()
        {
            $scope.orderListModel.isLoadingProgress = true;
            var getParam = {};
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getProductBasedList(getParam, configOption).then(function (response) {

                var data = response.data;
                $scope.orderListModel.productList = data.list;
                $scope.orderListModel.isLoadingProgress = false;
            });
        }

        $scope.formatProductModel = function (model, index)
        {
            if (model != null && model != 'undefined' && model != '')
            {
                $scope.updateProductInfo(model, index);
                if (model.code != '' && model.code != null && model.code != 'undefined')
                {
                    return model.name + '(' + model.code + ')';
                } else {
                    return model.name;
                }
            }
            return  '';
        }

        $scope.updateProductInfo = function (model, index)
        {
            $scope.qtyExist = false;
            if ($scope.orderListModel.list.length != 0)
            {
                if (typeof model == 'undefined')
                    return;

                $scope.orderListModel.list[index].id = model.id;
                $scope.orderListModel.list[index].sku = model.sku;
                $scope.orderListModel.list[index].product_name = model.product_name;
                $scope.orderListModel.list[index].name = model.product_name;
                $scope.orderListModel.list[index].productName = model.product_name;
                if ($rootScope.appConfig.sales_product_list.toUpperCase() == 'PURCHASE BASED')
                {
                    $scope.orderListModel.list[index].sales_price = model.mrp_price;
                    $scope.orderListModel.list[index].mrp_price = model.mrp_price;
                    $scope.orderListModel.list[index].selling_price = model.mrp_price;
                    $scope.orderListModel.list[index].static_sales_price = model.mrp_price;
                    $scope.orderListModel.list[index].rowtotal = model.mrp_price;
                }
                if ($rootScope.appConfig.sales_product_list.toUpperCase() == 'PRODUCT BASED')
                {
                    $scope.orderListModel.list[index].sales_price = model.sales_price;
                    $scope.orderListModel.list[index].mrp_price = model.sales_price;
                    $scope.orderListModel.list[index].selling_price = model.sales_price;
                    $scope.orderListModel.list[index].static_sales_price = model.sales_price;
                    $scope.orderListModel.list[index].rowtotal = model.sales_price;
                }
                if (model.qty != null && model.qty != '' && model.qty != 'undefined')
                {
                    $scope.orderListModel.list[index].qty = model.qty;
                } else
                {
                    $scope.orderListModel.list[index].qty = 1;
                }
                if (model.tierPricing != undefined && model.tierPricing.length > 0)
                {
                    for (var j = 0; j < model.tierPricing.length; j++)
                    {
                        if ($scope.orderListModel.list[index].qty >= parseFloat(model.tierPricing[j].from_qty) && $scope.orderListModel.list[index].qty <= parseFloat(model.tierPricing[j].to_qty))
                        {
                            $scope.qtyExist = true;
                            $scope.selectedQtyIndex = j;
                        }
                        if (j == model.tierPricing.length - 1 && $scope.qtyExist)
                        {
                            $scope.orderListModel.list[index].sales_price = parseFloat(model.tierPricing[$scope.selectedQtyIndex].unit_price);
                        } else if (!$scope.qtyExist)
                        {
                            $scope.orderListModel.list[index].sales_price = parseFloat(model.mrp_price);
                        }
                    }
                } else
                {
                    if ($rootScope.appConfig.sales_product_list.toUpperCase() == 'PURCHASE BASED')
                    {
                        $scope.orderListModel.list[index].sales_price = model.mrp_price;
                    } else {
                        $scope.orderListModel.list[index].sales_price = model.sales_price;
                    }
                }
                if (model.purchase_invoice_item_id != 'undefined' && model.purchase_invoice_item_id != null)
                {
//                    $scope.orderListModel.list[index].purchase_invoice_item_id = model.purchase_invoice_item_id;
                    //previously in purchase invoice item id have bcn id but now changed by backendside
                    $scope.orderListModel.list[index].purchase_invoice_item_id = model.bcn_id;
                }
//                if (model.discountPercentage != 'undefined' && model.discountPercentage != null && model.discountPercentage != '')
//                {
//                    $scope.orderListModel.list[index].discountPercentage = model.discountPercentage;
                $scope.orderListModel.list[index].discountPercentage = $scope.orderListModel.list[0].discountPercentage;
//                } else
//                {
//                    $scope.orderListModel.list[index].discountPercentage = 0;
//                }
//                if (model.discountAmount != 'undefined' && model.discountAmount != null && model.discountAmount != '')
//                {
                $scope.orderListModel.list[index].discountAmount = $scope.orderListModel.list[0].discountAmount;
//                } else
//                {
//                    $scope.orderListModel.list[index].discountAmount = 0;
//                }
//                if (model.discountMode != 'undefined' && model.discountMode != null && model.discountMode != '')
//                {
                $scope.orderListModel.list[index].discountMode = $scope.orderListModel.list[0].discountMode;
//                } else
//                {
//                    $scope.orderListModel.list[index].discountMode = '%';
//                }
                if (model.taxPercentage != 'undefined' && model.taxPercentage != null && model.taxPercentage != '')
                {
                    $scope.orderListModel.list[index].taxPercentage = model.taxPercentage;
                } else
                {
                    $scope.orderListModel.list[index].taxPercentage = 0;
                }
                if (model.hsn_code != undefined)
                {
                    $scope.orderListModel.list[index].hsn_code = model.hsn_code;
                }
                $scope.orderListModel.list[index].uomInfo = {
                    "name": model.uom,
                }
                if (model.uomid != undefined)
                {
                    $scope.orderListModel.list[index].uomid = model.uomid;
                }
                if (model.uom_id != undefined)
                {
                    $scope.orderListModel.list[index].uomid = model.uom_id;
                }
                if (model.uomid != undefined && model.uom_id != undefined)
                {
                    $scope.orderListModel.list[index].uomid = "";
                }
                if (model.taxMapping != undefined && model.taxMapping.length > 0)
                {
                    $scope.orderListModel.list[index].tax_id = model.taxMapping[0].tax_id + '';
                    $scope.orderListModel.list[index].taxPercentage = model.taxMapping[0].tax_percentage;
                    $scope.orderListModel.list[index].tax_name = model.taxMapping[0].tax_name + '-' + model.taxMapping[0].tax_percentage;
                } else
                {
                    $scope.orderListModel.list[index].tax_id = '';
                    $scope.orderListModel.list[index].tax_name = '';
                }
                $scope.orderListModel.list[index].taxMapping = model.taxMapping;
                if (model.custom_opt1_key == $rootScope.appConfig.invoice_item_custom1_label)
                {
                    $scope.orderListModel.list[index].custom_opt1 = model.custom_opt1;
                }
                if (model.custom_opt2_key == $rootScope.appConfig.invoice_item_custom1_label)
                {
                    $scope.orderListModel.list[index].custom_opt1 = model.custom_opt2;
                }
                if (model.custom_opt3_key == $rootScope.appConfig.invoice_item_custom1_label)
                {
                    $scope.orderListModel.list[index].custom_opt1 = model.custom_opt3;
                }
                if (model.custom_opt4_key == $rootScope.appConfig.invoice_item_custom1_label)
                {
                    $scope.orderListModel.list[index].custom_opt1 = model.custom_opt4;
                }
                if (model.custom_opt5_key == $rootScope.appConfig.invoice_item_custom1_label)
                {
                    $scope.orderListModel.list[index].custom_opt1 = model.custom_opt5;
                }
                if (model.custom_opt1_key == $rootScope.appConfig.invoice_item_custom2_label)
                {
                    $scope.orderListModel.list[index].custom_opt2 = model.custom_opt1;
                }
                if (model.custom_opt2_key == $rootScope.appConfig.invoice_item_custom2_label)
                {
                    $scope.orderListModel.list[index].custom_opt2 = model.custom_opt2;
                }
                if (model.custom_opt3_key == $rootScope.appConfig.invoice_item_custom2_label)
                {
                    $scope.orderListModel.list[index].custom_opt2 = model.custom_opt3;
                }
                if (model.custom_opt4_key == $rootScope.appConfig.invoice_item_custom2_label)
                {
                    $scope.orderListModel.list[index].custom_opt2 = model.custom_opt4;
                }
                if (model.custom_opt5_key == $rootScope.appConfig.invoice_item_custom2_label)
                {
                    $scope.orderListModel.list[index].custom_opt2 = model.custom_opt5;
                }
                if (model.custom_opt1_key == $rootScope.appConfig.invoice_item_custom3_label)
                {
                    $scope.orderListModel.list[index].custom_opt3 = model.custom_opt1;
                }
                if (model.custom_opt2_key == $rootScope.appConfig.invoice_item_custom3_label)
                {
                    $scope.orderListModel.list[index].custom_opt3 = model.custom_opt2;
                }
                if (model.custom_opt3_key == $rootScope.appConfig.invoice_item_custom3_label)
                {
                    $scope.orderListModel.list[index].custom_opt3 = model.custom_opt3;
                }
                if (model.custom_opt4_key == $rootScope.appConfig.invoice_item_custom3_label)
                {
                    $scope.orderListModel.list[index].custom_opt3 = model.custom_opt4;
                }
                if (model.custom_opt5_key == $rootScope.appConfig.invoice_item_custom3_label)
                {
                    $scope.orderListModel.list[index].custom_opt3 = model.custom_opt5;
                }
                if (model.custom_opt1_key == $rootScope.appConfig.invoice_item_custom4_label)
                {
                    $scope.orderListModel.list[index].custom_opt4 = model.custom_opt1;
                }
                if (model.custom_opt2_key == $rootScope.appConfig.invoice_item_custom4_label)
                {
                    $scope.orderListModel.list[index].custom_opt4 = model.custom_opt2;
                }
                if (model.custom_opt3_key == $rootScope.appConfig.invoice_item_custom4_label)
                {
                    $scope.orderListModel.list[index].custom_opt4 = model.custom_opt3;
                }
                if (model.custom_opt4_key == $rootScope.appConfig.invoice_item_custom4_label)
                {
                    $scope.orderListModel.list[index].custom_opt4 = model.custom_opt4;
                }
                if (model.custom_opt5_key == $rootScope.appConfig.invoice_item_custom4_label)
                {
                    $scope.orderListModel.list[index].custom_opt4 = model.custom_opt5;
                }
                if (model.custom_opt1_key == $rootScope.appConfig.invoice_item_custom5_label)
                {
                    $scope.orderListModel.list[index].custom_opt5 = model.custom_opt1;
                }
                if (model.custom_opt2_key == $rootScope.appConfig.invoice_item_custom4_label)
                {
                    $scope.orderListModel.list[index].custom_opt5 = model.custom_opt5;
                }
                if (model.custom_opt3_key == $rootScope.appConfig.invoice_item_custom4_label)
                {
                    $scope.orderListModel.list[index].custom_opt5 = model.custom_opt5;
                }
                if (model.custom_opt4_key == $rootScope.appConfig.invoice_item_custom5_label)
                {
                    $scope.orderListModel.list[index].custom_opt5 = model.custom_opt4;
                }
                if (model.custom_opt5_key == $rootScope.appConfig.invoice_item_custom5_label)
                {
                    $scope.orderListModel.list[index].custom_opt5 = model.custom_opt5;
                }

//                for (var i = 0; i < $scope.orderListModel.activeTaxList.length; i++)
//                {
//                    if ($scope.orderListModel.activeTaxList[i].id == $scope.orderListModel.list[index].tax_id)
//                    {
//                        $scope.orderListModel.list[index].taxPercentage = $scope.orderListModel.activeTaxList[i].tax_percentage;
//                    }
//                }
                $scope.updateInvoiceTotal();

                console.log("Index for item edit ", index);
                console.log("List Length ", $scope.orderListModel.list.length);
                $scope.createNewProduct(index);
                return;
            } else
            {
                $scope.createNewProduct(-1);
            }
        }
        $scope.initvalidateProductDataAndAddNewPromise = null;

        $scope.initValidateProductDataAndAddNew = function (index)
        {

            if ($scope.initvalidateProductDataAndAddNewPromise != null)
            {

                $timeout.cancel($scope.initvalidateProductDataAndAddNewPromise);
            }

            $scope.formValidator();
            $scope.initvalidateProductDataAndAddNewPromise = $timeout(
                    function () {
                        $scope.createNewProduct(index);
                    },
                    300);
        };

        $scope.formValidator = function ()
        {
            //performace issue in form validator bug
            if ($scope.order_add_form.$submitted)
            {
                $timeout(function () {
                    $scope.order_add_form.$submitted = false;
                }, 0);
                $timeout(function () {
                    $scope.order_add_form.$setSubmitted();
                }, 100);
            } else
            {
                $timeout(function () {
                    $scope.order_add_form.$setSubmitted();
                }, 100);
            }
        }

        $scope.getItemList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            autosearchParam.is_active = 1;
            if ($scope.orderListModel.customerInfo != '' && $scope.orderListModel.customerInfo != undefined && $scope.orderListModel.customerInfo != null)
            {
                return $httpService.get(APP_CONST.API.PRODUCT_BASED_LIST, autosearchParam, false).then(function (responseData)
                {
                    var data = responseData.data.list;
                    var hits = [];
                    if (data.length > 0)
                    {
                        for (var i = 0; i < data.length; i++)
                        {
                            if (data[i].is_sale == '1' || data[i].is_sale == 1)
                            {
                                hits.push(data[i]);
                            }
                        }
                    }
                    if (hits.length > 10)
                    {
                        hits.splice(10, hits.length);
                    }
                    return hits;
                });
            } else {
                sweet.show("", "Please select customer before adding products", "");
            }
        };

        $scope.initGetPurchaseProductDetailTimeoutPromise = null;
        $scope.initGetPurchaseProductDetail = function (value, index)
        {
            if ($scope.initGetPurchaseProductDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initGetPurchaseProductDetailTimeoutPromise);
            }
            if (value != "")
            {
                $scope.initGetPurchaseProductDetailTimeoutPromise = $timeout(
                        function () {
                            $scope.getPurchaseProductDetail(value, index);
                        },
                        100);
            } else
            {
                if ($scope.orderListModel.list.length > 0)
                {
                    $scope.orderListModel.list[index].id = '';
                    $scope.orderListModel.list[index].purchase_invoice_item_id = '';
                    $scope.orderListModel.list[index].productName = '';
                    $scope.orderListModel.list[index].name = '';
                    $scope.orderListModel.list[index].sku = '';
                    $scope.orderListModel.list[index].sales_price = '';
                    $scope.orderListModel.list[index].static_sales_price = '';
                    $scope.orderListModel.list[index].mrp_price = '';
                    $scope.orderListModel.list[index].qty = '';
                    $scope.orderListModel.list[index].rowtotal = '';
                    $scope.orderListModel.list[index].taxMapping = [];
                    $scope.orderListModel.list[index].taxPercentage = '';
                    $scope.orderListModel.list[index].tax_id = '';
                    $scope.orderListModel.list[index].taxAmount = '';
                    $scope.orderListModel.list[index].discountPercentage = '';
                    $scope.orderListModel.list[index].discountAmount = '';
                    $scope.orderListModel.list[index].discountMode = '%';
                    $scope.orderListModel.list[index].tierPricing = [];
                }
                $scope.calculatetotal();
            }
        }

        $scope.getPurchaseProductDetail = function (value, index)
        {
            var getListParam = {};
            getListParam.search = value;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getProductBasedList(getListParam, configOption).then(function (response)
            {
                var data = response.data.list;
                if (response.data.total > 0)
                {
                    $scope.orderListModel.list[index].id = data[0].product_id;
                    $scope.orderListModel.list[index].purchase_invoice_item_id = data[0].purchase_invoice_item_id;
                    $scope.orderListModel.list[index].productName = data[0].product_name;
                    $scope.orderListModel.list[index].name = data[0].product_name;
                    $scope.orderListModel.list[index].sku = data[0].sku;
                    $scope.orderListModel.list[index].sales_price = data[0].selling_price;
                    $scope.orderListModel.list[index].static_sales_price = data[0].mrp_price;
                    $scope.orderListModel.list[index].mrp_price = data[0].mrp_price;
                    $scope.orderListModel.list[index].qty = 1;
                    $scope.orderListModel.list[index].rowtotal = data[0].selling_price;
                    $scope.orderListModel.list[index].taxMapping = data[0].taxMapping;
                    //$scope.orderListModel.list[index].taxPercentage = data[0].selling_tax_percentage;
                    $scope.orderListModel.list[index].hsn_code = '';
                    $scope.orderListModel.list[index].taxAmount = 0;
                    $scope.orderListModel.list[index].discountPercentage = 0;
                    $scope.orderListModel.list[index].discountAmount = 0;
                    $scope.orderListModel.list[index].discountMode = '%';
                    $scope.orderListModel.list[index].tierPricing = data[0].tierPricing;
                    $scope.orderListModel.list[index].tax_id = data[0].taxMapping[0].tax_id + '';
                    $scope.orderListModel.list[index].productname = $scope.orderListModel.list[index];
                    for (var i = 0; i < $scope.orderListModel.activeTaxList.length; i++)
                    {
                        if ($scope.orderListModel.activeTaxList[i].id == $scope.orderListModel.list[index].tax_id)
                        {
                            $scope.orderListModel.list[index].taxPercentage = $scope.orderListModel.activeTaxList[i].tax_percentage;
                            var taxName = $scope.orderListModel.activeTaxList[i].tax_name;
                        }
                    }
                    var taxparam = {};
                    taxparam.tax_id = data[0].selling_tax_id;
                    taxparam.tax_name = taxName;
                    taxparam.tax_percentage = $scope.orderListModel.list[index].taxPercentage;
                    //                    $scope.orderListModel.list[index].taxMapping = [];
                    //                    $scope.orderListModel.list[index].taxMapping.push(taxparam);
                    //                    if ($scope.orderListModel.list[index].taxMapping.length > 0)
                    //                    {
                    //                        for (var i = 0; i < $scope.orderListModel.list[index].taxMapping.length; i++)
                    //                        {
                    //                            $scope.orderListModel.list[index].taxPercentage = $scope.orderListModel.list[index].taxPercentage + parseFloat($scope.orderListModel.list[index].taxMapping[i].tax_percentage);
                    //                        }
                    //                    } else
                    //                    {
                    //                        $scope.orderListModel.list[index].taxPercentage = 0;
                    //                    }
                    if ($scope.orderListModel.list[$scope.orderListModel.list.length - 1].purchase_invoice_item_id != '')
                    {
                        $scope.addNewProduct();
                    }
                    //$scope.addNewProduct();
                } else
                {
                    $scope.orderListModel.list[index].id = '';
                    $scope.orderListModel.list[index].productName = '';
                    $scope.orderListModel.list[index].name = '';
                    $scope.orderListModel.list[index].sku = '';
                    $scope.orderListModel.list[index].sales_price = '';
                    $scope.orderListModel.list[index].static_sales_price = '';
                    $scope.orderListModel.list[index].mrp_price = '';
                    $scope.orderListModel.list[index].qty = '';
                    $scope.orderListModel.list[index].rowtotal = '';
                    $scope.orderListModel.list[index].taxMapping = [];
                    $scope.orderListModel.list[index].taxPercentage = '';
                    $scope.orderListModel.list[index].tax_id = '';
                    $scope.orderListModel.list[index].hsn_code = '';
                    $scope.orderListModel.list[index].taxAmount = '';
                    $scope.orderListModel.list[index].discountPercentage = '';
                    $scope.orderListModel.list[index].discountAmount = '';
                    $scope.orderListModel.list[index].discountMode = '%';
                    $scope.orderListModel.list[index].tierPricing = [];
                }

                $scope.updateTierPricing(index);

            });
        };

        $scope.updateTierPricing = function ($index)
        {
            $scope.newQtyExist = false;
            var newData = $scope.orderListModel.list[$index];
            if (newData.tierPricing != undefined && newData.tierPricing != null && newData.tierPricing.length > 0)
            {
                for (var j = 0; j < newData.tierPricing.length; j++)
                {
                    if (newData.qty >= parseFloat(newData.tierPricing[j].from_qty) && newData.qty <= parseFloat(newData.tierPricing[j].to_qty))
                    {
                        $scope.newQtyExist = true;
                        $scope.selectedNewQtyIndex = j;
                    }
                    if (j == newData.tierPricing.length - 1 && $scope.newQtyExist)
                    {
                        $scope.orderListModel.list[$index].sales_price = parseFloat(newData.tierPricing[$scope.selectedNewQtyIndex].unit_price);
                    } else if (!$scope.newQtyExist)
                    {
                        $scope.orderListModel.list[$index].sales_price = parseFloat(newData.static_sales_price);
                    }
                }
                $scope.updateInvoiceTotal();

            } else
            {
                $scope.updateInvoiceTotal();
            }
        }

//        $scope.event = null;
//        $scope.keyupHandler = function (event)
//        {
//            if (event.keyCode == 9)
//            {
//                event.preventDefault();
//                var currentFocusField = angular.element(event.currentTarget);
//                var nextFieldId = $(currentFocusField).attr('next-focus-id');
//                var currentFieldId = $(currentFocusField).attr('id');
//                var productIndex = $(currentFocusField).attr('product-index');
//                if (nextFieldId == undefined || nextFieldId == '')
//                {
//                    return;
//                }
//                if (currentFieldId.indexOf('_item_') != -1)
//                {
//                    var filedIdSplitedValue = currentFieldId.split('_');
//                    var existingBrunchCount = $scope.orderListModel.list.length - 1;
//                    if (existingBrunchCount == filedIdSplitedValue[filedIdSplitedValue.length - 2] && filedIdSplitedValue[filedIdSplitedValue.length - 1] == 2)
//                    {
//                        $scope.addNewProduct();
//                        $timeout(function () {
//                            var newNextFieldId = filedIdSplitedValue[0] + '_' + filedIdSplitedValue[1] + '_' + ($scope.orderListModel.list.length - 1) + '_1';
//                            $("#" + newNextFieldId).focus();
//                        }, 300);
//                        return;
//                    }
//                }
//                $timeout(function () {
//                    $("#" + nextFieldId).focus();
//                    window.document.getElementById(nextFieldId).setSelectionRange(0, window.document.getElementById(nextFieldId).value.length);
//                }, 300);
//            }
//        }

        $scope.createNewProduct = function (index)
        {
            var formDataError = false;
//            if (index != -1)
//            {
//                var productContainer = window.document.getElementById('invoice_product_container_1');
//                var errorCell = angular.element(productContainer).find('.has-error').length;
//                if (errorCell > 0)
//                {
//                    formDataError = true;
//                }
//            }

            if (!formDataError)
            {
                if (index == $scope.orderListModel.list.length - 1)
                {

                    var newRow = {
                        "id": '',
                        "purchase_invoice_item_id": '',
                        "productName": '',
                        "sku": '',
                        "sales_price": '',
                        "mrp_price": '',
                        "qty": '',
                        "rowtotal": '',
                        "discountPercentage": '',
                        "discountAmount": '',
                        "discountMode": '%',
                        "taxPercentage": '',
                        "tax_id": '',
                        "hsn_code": '',
                        "taxAmount": '',
                        "uom": '',
                        "uomid": '',
                        "custom_opt1": '',
                        "custom_opt2": '',
                        "custom_opt3": '',
                        "custom_opt4": '',
                        "custom_opt5": '',
                        "taxMapping": '',
                        "tierPricing": '',
                        "static_sales_price": '',
                        "empCode": ''
                    }
                    $scope.orderListModel.list.push(newRow);
                }
            }

        }

        $scope.addNewProduct = function ()
        {
            var newRow = {
                "id": '',
                "purchase_invoice_item_id": '',
                "productName": '',
                "sku": '',
                "sales_price": '',
                "mrp_price": '',
                "qty": '',
                "rowtotal": '',
                "discountPercentage": '',
                "discountAmount": '',
                "discountMode": '%',
                "taxPercentage": '',
                "tax_id": '',
                "hsn_code": '',
                "taxAmount": '',
                "uom": '',
                "uomid": '',
                "custom_opt1": '',
                "custom_opt2": '',
                "custom_opt3": '',
                "custom_opt4": '',
                "custom_opt5": '',
                "taxMapping": '',
                "tierPricing": '',
                "static_sales_price": '',
                "empCode": ''
            }
            $scope.orderListModel.list.push(newRow);
        }

        $scope.calculatetotal = function ()
        {
            var subTotal = 0;
            var totDiscountPercentage = 0;
            var totDiscountAmt = 0;
            var totTaxPercentage = 0;
            var totTaxAmt = 0;
            console.log("bfr loop", $scope.orderListModel.list.length);

            for (var i = 0; i < $scope.orderListModel.list.length; i++)
            {
                if ($scope.orderListModel.list[i].id != '' && $scope.orderListModel.list[i].id != 'undefined' && $scope.orderListModel.list[i].id != null)
                {
                    $scope.orderListModel.list[i].rowtotal = $scope.orderListModel.list[i].sales_price * $scope.orderListModel.list[i].qty;
                    subTotal += parseFloat($scope.orderListModel.list[i].rowtotal);
                    $scope.orderListModel.list[i].rowtotal = $scope.orderListModel.list[i].rowtotal.toFixed(2);

                    if ($scope.orderListModel.list[i].discountPercentage == null || $scope.orderListModel.list[i].discountPercentage == '' || isNaN($scope.orderListModel.list[i].discountPercentage))
                    {
                        $scope.orderListModel.list[i].discountPercentage = 0;
                    } else
                    {
                        $scope.orderListModel.list[i].discountPercentage = parseFloat($scope.orderListModel.list[i].discountPercentage);
                    }

                    if ($scope.orderListModel.list[i].discountMode == '%')
                    {
                        $scope.orderListModel.list[i].discountAmount = parseFloat(($scope.orderListModel.list[i].rowtotal * $scope.orderListModel.list[i].discountPercentage) / 100);
                        totDiscountPercentage += $scope.orderListModel.list[i].discountPercentage;
                    } else if ($scope.orderListModel.list[i].discountMode == 'amount')
                    {
                        $scope.orderListModel.list[i].discountAmount = $scope.orderListModel.list[i].discountPercentage;
                    }
                    $scope.orderListModel.list[i].discountAmount = $scope.orderListModel.list[i].discountAmount;
                    if ($scope.orderListModel.list[i].discountAmount < 0)
                    {
                        $scope.orderListModel.list[i].discountAmount = 0;
                    }
                    $scope.orderListModel.list[i].rowtotal = parseFloat($scope.orderListModel.list[i].rowtotal) - parseFloat($scope.orderListModel.list[i].discountAmount);
                    totDiscountAmt += parseFloat($scope.orderListModel.list[i].discountAmount);

                    if ($scope.orderListModel.list[i].taxPercentage == null || $scope.orderListModel.list[i].taxPercentage == '' || isNaN($scope.orderListModel.list[i].taxPercentage))
                    {
                        $scope.orderListModel.list[i].taxPercentage = 0;
                    } else
                    {
                        $scope.orderListModel.list[i].taxPercentage = parseFloat($scope.orderListModel.list[i].taxPercentage);
                    }
                    totTaxPercentage += $scope.orderListModel.list[i].taxPercentage;
                    $scope.orderListModel.list[i].taxAmount = parseFloat($scope.orderListModel.list[i].rowtotal) - ((parseFloat($scope.orderListModel.list[i].rowtotal)) / (1 + (parseFloat($scope.orderListModel.list[i].taxPercentage) / 100)));
                    $scope.orderListModel.list[i].taxAmount = $scope.orderListModel.list[i].taxAmount.toFixed(2);
                    if ($scope.orderListModel.list[i].taxAmount < 0)
                    {
                        $scope.orderListModel.list[i].taxAmount = 0;
                    }
                    totTaxAmt += parseFloat($scope.orderListModel.list[i].taxAmount);
                }
            }
            $scope.orderListModel.subtotal = parseFloat(subTotal).toFixed(2);
            $scope.orderListModel.discountAmt = parseFloat(totDiscountAmt).toFixed(2);
            $scope.orderListModel.taxAmt = parseFloat(totTaxAmt).toFixed(2);
            $scope.orderListModel.discountPercentage = parseFloat(totDiscountPercentage).toFixed(2);
            if ($scope.orderListModel.subtotal > 0)
            {
                $scope.orderListModel.subtotalWithDiscount = $scope.orderListModel.subtotal - $scope.orderListModel.discountAmt;
                $scope.orderListModel.total = parseFloat($scope.orderListModel.subtotalWithDiscount).toFixed(2);
            } else
            {
                $scope.orderListModel.subtotalWithDiscount = 0.00;
                $scope.orderListModel.total = 0.00;
            }
            $scope.orderListModel.balance_amount = (parseFloat($scope.orderListModel.total - $scope.orderListModel.paid_amount)).toFixed(2);
            $scope.updateRoundoff();
        }

        $scope.updateRoundoff = function ()
        {
//            $scope.orderListModel.round = Math.round($scope.orderListModel.total);
            $scope.orderListModel.round = $scope.orderListModel.total;
            $scope.orderListModel.roundoff = $scope.orderListModel.round - parseFloat($scope.orderListModel.total);
            $scope.orderListModel.roundoff = parseFloat($scope.orderListModel.roundoff).toFixed(2);
            $scope.orderListModel.round = parseFloat($scope.orderListModel.round).toFixed(2);
            $scope.calculateTaxAmt();
        }

        $scope.$watch('orderListModel.productname', function (newVal, oldVal)
        {
            if (typeof newVal != 'undefined' && newVal != '' && newVal != null)
            {
                $scope.initGetPurchaseProductDetail(newVal);
            }
        });

        //        $scope.$watch('orderListModel.subtotalWithDiscount', function (newVal, oldVal)
        //        {
        //            if (typeof newVal != 'undefined' && newVal != '' && newVal != null)
        //            {
        //                $scope.calculateTaxAmt(false);
        //            }
        //        });

        $scope.calculateTaxAmt = function (forceSave)
        {
            $scope.orderListModel.taxMappingDetail = [];
            for (var i = 0; i < $scope.orderListModel.list.length; i++)
            {
                if ($scope.orderListModel.list[i].id != '' && $scope.orderListModel.list[i].id != 'undefined' && $scope.orderListModel.list[i].id != null)
                {
                    if ($scope.orderListModel.list[i].taxMapping != undefined)
                    {
                        if ($scope.orderListModel.list[i].taxMapping.length > 0)
                        {
                            for (var j = 0; j < $scope.orderListModel.list[i].taxMapping.length; j++)
                            {
                                var taxAmt = parseFloat($scope.orderListModel.list[i].rowtotal) - ((parseFloat($scope.orderListModel.list[i].rowtotal)) / (1 + (parseFloat($scope.orderListModel.list[i].taxMapping[j].tax_percentage) / 100)));
                                //var taxAmt = parseFloat(($scope.orderListModel.list[i].taxMapping[j].tax_percentage * $scope.orderListModel.list[i].rowtotal) / 100);
                                $scope.orderListModel.list[i].taxMapping[j].tax_Amt = taxAmt;
                                console.log("bfr checkId", $scope.orderListModel.taxMappingDetail);
                                var result = $scope.checkTaxId($scope.orderListModel.list[i].taxMapping[j].tax_id, taxAmt);
                                if (result == false)
                                {
                                    var param = {};
                                    param.tax_id = $scope.orderListModel.list[i].taxMapping[j].tax_id;
                                    param.taxName = $scope.orderListModel.list[i].taxMapping[j].tax_name;
                                    param.taxPercentage = $scope.orderListModel.list[i].taxMapping[j].tax_percentage;
                                    param.overallTaxAmount = parseFloat(taxAmt).toFixed(2);
                                    $scope.orderListModel.taxMappingDetail.push(param);
                                }
                            }
                        }
                    }
                }
            }
        }

        $scope.checkTaxId = function (id, amt)
        {
            if ($scope.orderListModel.taxMappingDetail.length > 0)
            {
                for (var i = 0; i < $scope.orderListModel.taxMappingDetail.length; i++)
                {
                    console.log("taxMapping Detail", $scope.orderListModel.taxMappingDetail[i]);
                    if ($scope.orderListModel.taxMappingDetail[i].tax_id == id)
                    {
                        $scope.orderListModel.taxMappingDetail[i].overallTaxAmount = parseFloat(parseFloat($scope.orderListModel.taxMappingDetail[i].overallTaxAmount) + parseFloat(amt)).toFixed(2);
                        return true;
                    }
                }
                return false;
            }
            return false;
        };

        //        $scope.$on("updateSelectedProductEvent", function (event, selectedItems)
        //        {
        //            if (typeof selectedItems == 'undefined')
        //                return;
        //            if ($scope.orderListModel.list.length != 0)
        //            {
        //                var isDuplicateProduct = false;
        //                var duplicateProductIndex = -1;
        //                for (i = 0; i < $scope.orderListModel.list.length; i++)
        //                {
        //                    if (selectedItems.id == $scope.orderListModel.list[i].id)
        //                    {
        //                        isDuplicateProduct = true;
        //                        duplicateProductIndex = i;
        //                    }
        //
        //                }
        //                if (isDuplicateProduct)
        //                {
        //                    $scope.orderListModel.list[duplicateProductIndex].qty = $scope.orderListModel.list[duplicateProductIndex].qty + 1;
        //                    console.log("upd 4");
        //                    $scope.updateInvoiceTotal();
        //                } else
        //                {
        //                    var newRow = {
        //                        "id": selectedItems.id,
        //                        "productName": selectedItems.name,
        //                        "sku": selectedItems.sku,
        //                        "sales_price": selectedItems.sales_price,
        //                        "qty": 1,
        //                        "rowtotal": selectedItems.sales_price,
        //                        "discountPercentage": selectedItems.discountPercentage,
        //                        "taxPercentage": selectedItems.taxPercentage,
        //                        "uom": selectedItems.uom,
        //                        "uomid": selectedItems.uom_id
        //                    }
        //                    $scope.orderListModel.list.push(newRow);
        //                    console.log("upd 2");
        //                    $scope.updateInvoiceTotal();
        //                    return;
        //                }
        //            } else
        //            {
        //                var newRow = {
        //                    "id": selectedItems.id,
        //                    "productName": selectedItems.name,
        //                    "sku": selectedItems.sku,
        //                    "sales_price": selectedItems.sales_price,
        //                    "qty": 1,
        //                    "rowtotal": selectedItems.sales_price,
        //                    "discountPercentage": selectedItems.discountPercentage,
        //                    "taxPercentage": selectedItems.taxPercentage,
        //                    "uom": selectedItems.uom,
        //                    "uomid": selectedItems.uom_id
        //                }
        //                $scope.orderListModel.list.push(newRow);
        //                $scope.updateInvoiceTotal();
        //            }
        //        });
        $scope.display_balance = false;
        $scope.updateInvoiceDetails = function ( )
        {
            if (typeof $scope.orderListModel.invoiceDetail != 'undefined' && $scope.orderListModel.invoiceDetail != null)
            {
                $scope.orderListModel.customerInfo = {};
                $scope.orderListModel.customerInfo.id = $scope.orderListModel.invoiceDetail.customer_id;
                $scope.orderListModel.customerInfo.fname = $scope.orderListModel.invoiceDetail.customer_fname;
                $scope.orderListModel.customerInfo.shopping_address = $scope.orderListModel.invoiceDetail.customer_address;
                $scope.orderListModel.customerInfo.phone = $scope.orderListModel.invoiceDetail.phone;
                $scope.orderListModel.customerInfo.email = $scope.orderListModel.invoiceDetail.email;
                $scope.orderListModel.subtotal = $scope.orderListModel.invoiceDetail.subtotal;
                $scope.orderListModel.taxAmt = $scope.orderListModel.invoiceDetail.tax_amount;
                $scope.orderListModel.taxAmountDetail.tax_amount = $scope.orderListModel.invoiceDetail.tax_amount;
                $scope.orderListModel.discountAmt = $scope.orderListModel.invoiceDetail.discount_amount;
                $scope.orderListModel.total = $scope.orderListModel.invoiceDetail.total_amount;
                $scope.display_balance = true;
                $scope.orderListModel.paid_amount = $scope.orderListModel.invoiceDetail.advance_amount;
                $scope.orderListModel.balance_amount = (parseFloat($scope.orderListModel.total - $scope.orderListModel.paid_amount)).toFixed(2);
                $scope.orderListModel.invoicePrefix = $scope.orderListModel.invoiceDetail.prefix;
                //                var date = utilityService.parseStrToDate($scope.orderListModel.invoiceDetail.date);
                //                $scope.orderListModel.billdate = $filter('date')(date, $scope.dateFormat);
                $scope.orderListModel.duedate = $scope.orderListModel.invoiceDetail.duedate;
                $scope.orderListModel.discountPercentage = $scope.orderListModel.invoiceDetail.discount_percentage + '';
                $scope.orderListModel.taxAmountDetail.tax_id = $scope.orderListModel.invoiceDetail.tax_id + '';
                $scope.orderListModel.discountMode = $scope.orderListModel.invoiceDetail.discount_mode;
                var loop;

                for (loop = 0; loop < $scope.orderListModel.taxInfo.length; loop++)
                {
                    if ($scope.orderListModel.taxInfo[loop].id == $scope.orderListModel.invoiceDetail.tax_id)
                    {
                        $scope.orderListModel.taxPercentage = $scope.orderListModel.invoiceDetail.tax_percentage;
                        break;
                    }
                }

                for (loop = 0; loop < $scope.orderListModel.invoiceDetail.item.length; loop++)
                {
                    var newRow =
                            {
                                "id": $scope.orderListModel.invoiceDetail.item[loop].product_id,
                                "product_name": $scope.orderListModel.invoiceDetail.item[loop].product_name,
                                "sku": $scope.orderListModel.invoiceDetail.item[loop].product_sku,
                                "sales_price": $scope.orderListModel.invoiceDetail.item[loop].unit_price,
                                "mrp_price": $scope.orderListModel.invoiceDetail.item[loop].unit_price,
                                "selling_price": $scope.orderListModel.invoiceDetail.item[loop].unit_price,
                                "qty": $scope.orderListModel.invoiceDetail.item[loop].qty,
                                "rowtotal": $scope.orderListModel.invoiceDetail.item[loop].total_price,
                                "uom": $scope.orderListModel.invoiceDetail.item[loop].uom_name,
                                "uomid": $scope.orderListModel.invoiceDetail.item[loop].uom_id
                            };
                    $scope.orderListModel.list.push(newRow);
                    $scope.orderListModel.list[loop].productname = $scope.orderListModel.list[loop];
                }
                var newRow = {
                    "id": '',
                    "productName": '',
                    "sku": '',
                    "sales_price": '',
                    "qty": '',
                    "rowtotal": '',
                    "discountPercentage": '',
                    "taxPercentage": '',
                    "uom": '',
                    "uomid": '',
                    "empCode": ''
                }
                $scope.orderListModel.list.push(newRow);
                $scope.orderListModel.isLoadingProgress = false;
            }
        }

        $scope.getInvoiceInfo = function ( )
        {
            $scope.orderListModel.isLoadingProgress = true;
            var getListParam = {};
            if ($stateParams.id != "")
            {
                getListParam.id = $stateParams.id;
                getListParam.is_active = 1;
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getSalesOrderDetail(getListParam, configOption).then(function (response)
                {
                    var data = response.data.data;
                    $scope.orderListModel.invoiceDetail = data;
                    $scope.initUpdateDetail();
                    // $scope.getTaxListInfo( );
                });
            }
        };
        //        $scope.getProductList = function ()
        //        {
        //            $scope.orderListModel.isLoadingProgress = true;
        //            var getParam = {};
        //
        //            getParam.is_active = 1;
        //            getParam.is_sale = 1;
        //            getParam.type = 'Product';
        //
        //            if (typeof $scope.selectedAssignFrom != 'undefined' && typeof $scope.selectedAssignFrom.id != 'undefined')
        //            {
        //                getParam.scopeId = $scope.selectedAssignFrom.id;
        //            }
        //
        //            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
        //            adminService.getProductList(getParam, configOption).then(function (response) {
        //
        //                var data = response.data;
        //                $scope.orderListModel.productList = data.list;
        //                $scope.orderListModel.isLoadingProgress = false;
        //            });
        //        }
        $scope.checkId = function ()
        {
            if (typeof $stateParams.id != 'undefined' && $stateParams.id != null && $stateParams.id != '' && $stateParams.id != 0) {
                $scope.getInvoiceInfo();
            }
            if (typeof $stateParams.deal_id != 'undefined' && $stateParams.deal_id != null && $stateParams.deal_id != '')
            {
                $scope.getDealDetail($stateParams.deal_id);
            }
        }

        $scope.getDealDetail = function (id)
        {
            var getListParam = {};
            if (typeof id != 'undefined')
            {
                getListParam.id = id;
                getListParam.is_active = 1;
                var configOption = adminService.handleOnlyErrorResponseConfig;
                adminService.getDealList(getListParam, configOption).then(function (response)
                {
                    if (response.data.success === true)
                    {
                        var data = response.data;
                        if (data.list.length > 0)
                        {
                            $scope.dealDetail = data.list[0];
                            $scope.orderListModel.dealId = $scope.dealDetail.id;
                            $scope.orderListModel.dealName = $scope.dealDetail.name;
                        }
                    }

                });
            }
        }
        // $scope.getTaxListInfo();
        if (typeof $stateParams.id == 'undefined' || $stateParams.id == null || $stateParams.id == '' || $stateParams.id == 0)
        {
            //$scope.updateProductInfo();
            $scope.addNewProduct();
        }
        $scope.getProductList( );

        $scope.createOrder = function (forceSave)
        {
            if (!forceSave)
            {
                if ($scope.isDataSavingProcess)
                {
                    return;
                }
            }
            $scope.isDataSavingProcess = true;
            $scope.isSave = false;
            for (var i = 0; i < $scope.orderListModel.list.length; i++)
            {
                if (i != $scope.orderListModel.list.length - 1)
                {
                    if ($rootScope.appConfig.uomdisplay)
                    {
                        if ($scope.orderListModel.list[i].id != '' && $scope.orderListModel.list[i].id != null &&
                                $scope.orderListModel.list[i].productName != '' && $scope.orderListModel.list[i].productName != null &&
                                $scope.orderListModel.list[i].uomInfo != '' && $scope.orderListModel.list[i].uomInfo != null &&
                                $scope.orderListModel.list[i].empCode != '' && $scope.orderListModel.list[i].empCode != null &&
                                $scope.orderListModel.list[i].qty != '' && $scope.orderListModel.list[i].qty != null &&
                                !isNaN(parseFloat($scope.orderListModel.list[i].sales_price)) && $scope.orderListModel.list[i].sales_price != null &&
                                !isNaN(parseFloat($scope.orderListModel.list[i].rowtotal)) && $scope.orderListModel.list[i].rowtotal != null)
                        {
                            $scope.isSave = true;
                        } else
                        {
                            $scope.isSave = false;
                            break;
                        }
                    } else
                    {
                        if ($scope.orderListModel.list[i].id != '' && $scope.orderListModel.list[i].id != null &&
                                $scope.orderListModel.list[i].productName != '' && $scope.orderListModel.list[i].productName != null &&
                                $scope.orderListModel.list[i].qty != '' && $scope.orderListModel.list[i].qty != null &&
                                $scope.orderListModel.list[i].empCode != '' && $scope.orderListModel.list[i].empCode != null &&
                                !isNaN(parseFloat($scope.orderListModel.list[i].sales_price)) &&
                                !isNaN(parseFloat($scope.orderListModel.list[i].rowtotal)) && $scope.orderListModel.list[i].rowtotal != null)
                        {
                            $scope.isSave = true;
                        } else
                        {
                            $scope.isSave = false;
                            console.log(isNaN(parseFloat($scope.orderListModel.list[i].sales_price)));
                            console.log(!isNaN(parseFloat($scope.orderListModel.list[i].rowtotal)));
                            break;
                        }
                    }
                }
                if ($scope.orderListModel.list.length == 1)
                {
                    if ($rootScope.appConfig.uomdisplay)
                    {
                        if ($scope.orderListModel.list[0].id != '' && $scope.orderListModel.list[0].id != null &&
                                $scope.orderListModel.list[0].productName != '' && $scope.orderListModel.list[0].productName != null &&
                                $scope.orderListModel.list[0].uomInfo != '' && $scope.orderListModel.list[0].uomInfo != null &&
                                $scope.orderListModel.list[0].empCode != '' && $scope.orderListModel.list[0].empCode != null &&
                                $scope.orderListModel.list[0].qty != '' && $scope.orderListModel.list[0].qty != null &&
                                $scope.orderListModel.list[0].sales_price != '' && $scope.orderListModel.list[0].sales_price != null &&
                                $scope.orderListModel.list[0].rowtotal != '' && $scope.orderListModel.list[0].rowtotal != null)
                        {
                            $scope.isSave = true;
                        } else
                        {
                            $scope.isSave = false;
                            break;
                        }
                    } else
                    {
                        if ($scope.orderListModel.list[0].id != '' && $scope.orderListModel.list[0].id != null &&
                                $scope.orderListModel.list[0].productName != '' && $scope.orderListModel.list[0].productName != null &&
                                $scope.orderListModel.list[0].qty != '' && $scope.orderListModel.list[0].qty != null &&
                                $scope.orderListModel.list[0].empCode != '' && $scope.orderListModel.list[0].empCode != null &&
                                $scope.orderListModel.list[0].sales_price != '' && $scope.orderListModel.list[0].sales_price != null &&
                                $scope.orderListModel.list[0].rowtotal != '' && $scope.orderListModel.list[0].rowtotal != null)
                        {
                            $scope.isSave = true;
                        } else
                        {
                            $scope.isSave = false;
                            break;
                        }
                    }
                }
            }

            if ($scope.isSave)
            {
                //                if (typeof $scope.orderListModel.taxAmountDetail != undefined && $scope.orderListModel.taxAmountDetail != null && $scope.orderListModel.taxAmountDetail != '')
                //                {
                //                    if ($scope.orderListModel.taxAmountDetail.tax_amount == $scope.orderListModel.taxAmt)
                //                    {
                $scope.saveOrder();
                //                    } else
                //                    {
                //                        $scope.orderListModel.taxAmt = $scope.orderListModel.taxAmountDetail.tax_amount;
                //                        $scope.orderListModel.total = (parseFloat($scope.orderListModel.subtotal) + parseFloat($scope.orderListModel.taxAmt) - parseFloat($scope.orderListModel.discountAmt)).toFixed(2);
                //                        $scope.orderListModel.balance_amount = (parseFloat($scope.orderListModel.total - $scope.orderListModel.paid_amount)).toFixed(2);
                //                        $scope.updateRoundoff();
                //                        $scope.saveOrder();
                //$scope.calculateTaxAmt(true);
                //                    }
                //                } else
                //                {
                //                    $scope.calculateTaxAmt(true);
                //                }
            } else
            {
                if ($scope.orderListModel.list[i].productName == "")
                {
                    sweet.show('Oops...', 'Fill Product Details', 'error');
                    $scope.isDataSavingProcess = false;
                } else
                {
                    if ($scope.orderListModel.list[i].sales_price == "")
                    {
                        sweet.show('Oops...', 'Check Price Detail', 'error');
                        $scope.isDataSavingProcess = false;
                    } else
                    {
                        sweet.show('Oops...', 'Fill Product Details', 'error');
                        $scope.isDataSavingProcess = false;
                    }
                }
                var element = document.getElementById("btnLoad");
                element.classList.remove("btn-loader");
            }
        }

        $scope.currentDate = new Date();
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.createNotes = function ()
        {
            var createParam = {};
            var headers = {};
            headers['screen-code'] = 'deal';
            createParam.description = ''
            createParam.type = 'estimate';
            createParam.deal_id = $scope.orderListModel.dealId;
            createParam.customer_id = $scope.orderListModel.customerInfo.id;
            createParam.customer_name = $scope.orderListModel.customerInfo.fname;
            createParam.is_active = 1;
            createParam.date = $filter('date')($scope.currentDate, $scope.dateFormat);
            createParam.user_id = $rootScope.userModel.id;
            createParam.user_name = $rootScope.userModel.f_name;

            adminService.addDealActivity(createParam, headers).then(function (response) {
                if (response.data.success == true)
                {
                    $state.go('app.dealedit', {
                        'id': $stateParams.id
                    }, {
                        'reload': true
                    });
                }
                $scope.isDataSavingProcess = false;
            });

        };

        $scope.saveOrder = function ()
        {
            var totalQty = 0;
            var createOrderParam = {};
            var headers = {};
            headers['screen-code'] = 'salesinvoice';
            createOrderParam.quote_id = $stateParams.id;
            //   createOrderParam.status = 'unpaid';
            createOrderParam.is_pos = 0;
            createOrderParam.prefix = $scope.orderListModel.invoicePrefix;
            createOrderParam.gst_no = $scope.orderListModel.gst_no;
            if ($scope.orderListModel.customerInfo.gst_no != undefined && $scope.orderListModel.customerInfo.gst_no != null && $scope.orderListModel.customerInfo.gst_no != '')
            {
                createOrderParam.cust_gst_no = $scope.orderListModel.customerInfo.gst_no;
            } else
            {
                createOrderParam.cust_gst_no = '';
            }
            createOrderParam.invoice_no = $scope.orderListModel.invoiceno;
            //createOrderParam.id = $scope.orderListModel.invoiceno;
            $scope.calculateDueDate();
            if (typeof $scope.orderListModel.billdate == 'object')
            {
                var billdate = utilityService.parseDateToStr($scope.orderListModel.billdate, 'yyyy-MM-dd');
            }
            createOrderParam.date = utilityService.changeDateToSqlFormat(billdate, 'yyyy-MM-dd');
            if (typeof $scope.orderListModel.duedate == 'object')
            {
                var duedate = utilityService.parseDateToStr($scope.orderListModel.duedate, 'yyyy-MM-dd');
            }
            createOrderParam.duedate = utilityService.changeDateToSqlFormat(duedate, 'yyyy-MM-dd');
            createOrderParam.datepaid = '';
            createOrderParam.customer_address = $scope.orderListModel.customerInfo.shopping_address;
            createOrderParam.subtotal = (parseFloat($scope.orderListModel.subtotal)).toFixed(2);
            //            if (typeof $scope.orderListModel.taxAmountDetail != undefined && $scope.orderListModel.taxAmountDetail != null && $scope.orderListModel.taxAmountDetail != '')
            //            {
            //                createOrderParam.tax_amount = (parseFloat($scope.orderListModel.taxAmountDetail.tax_amount)).toFixed(2);
            //            } else
            //            {
            createOrderParam.tax_amount = (parseFloat($scope.orderListModel.taxAmt)).toFixed(2);
            // createOrderParam.total_qty += parseFloat($scope.orderListModel.taxAmt);
            //}
            createOrderParam.discount_amount = (parseFloat($scope.orderListModel.discountAmt)).toFixed(2);
            createOrderParam.discount_percentage = $scope.orderListModel.discountPercentage;
            createOrderParam.discount_mode = $scope.orderListModel.discountMode;
            createOrderParam.total_amount = $scope.orderListModel.round;
            createOrderParam.round_off = (parseFloat($scope.orderListModel.roundoff)).toFixed(2);
            createOrderParam.paymentmethod = "",
                    createOrderParam.notes = $scope.orderListModel.notes;
            createOrderParam.item = [];
            //createOrderParam.item.date = utilityService.parseDateToStr($scope.orderListModel.billdate, "yyyy-MM-dd");
            //createOrderParam.item.outletId = 1;
            //createOrderParam.item.status = 'initiated';
            createOrderParam.customer_id = $scope.orderListModel.customerInfo.id;
            //createOrderParam.item.orderDetails = [];
            if ($scope.orderListModel.list.length < 1)
            {
                return;
            }
            var length = 0;
            if ($scope.orderListModel.list.length == 1)
            {
                length = 2;
            } else
            {
                length = $scope.orderListModel.list.length;
            }
            for (var i = 0; i < length - 1; i++)
            {
                var ordereditems = {};
                ordereditems.product_id = $scope.orderListModel.list[i].id;
                ordereditems.purchase_invoice_item_id = $scope.orderListModel.list[i].purchase_invoice_item_id;
                ordereditems.discount_mode = $scope.orderListModel.list[i].discountMode;
                ordereditems.discount_value = $scope.orderListModel.list[i].discountPercentage;
                ordereditems.mrp_price = $scope.orderListModel.list[i].mrp_price;
                ordereditems.sales_price = $scope.orderListModel.list[i].sales_price;
                ordereditems.product_sku = $scope.orderListModel.list[i].sku;
                ordereditems.product_name = $scope.orderListModel.list[i].productName;
                ordereditems.unit_price = parseFloat($scope.orderListModel.list[i].sales_price);
                ordereditems.qty = parseInt($scope.orderListModel.list[i].qty);
                // createOrderParam.tax_id = $scope.orderListModel.list[i].tax_id;
                if (typeof $scope.orderListModel.list[i].uomInfo != undefined && $scope.orderListModel.list[i].uomInfo != null && $scope.orderListModel.list[i].uomInfo != '') {
                    if ($scope.orderListModel.list[i].uomInfo.name != null && $scope.orderListModel.list[i].uomInfo.name != '')
                    {
                        ordereditems.uom_name = $scope.orderListModel.list[i].uomInfo.name;
                    }
                    //                            else
                    //                            {
                    //                                ordereditems.uom_name = $scope.orderListModel.list[i].uomInfo;
                    //                            }
                }
                ordereditems.uom_id = $scope.orderListModel.list[i].uomid;
                if ($scope.orderListModel.list[i].tax_id != null && $scope.orderListModel.list[i].tax_id != undefined && $scope.orderListModel.list[i].tax_id != '')
                {
                    ordereditems.tax_id = $scope.orderListModel.list[i].tax_id;
                } else
                {
                    ordereditems.tax_id = '';
                }
                ordereditems.hsn_code = $scope.orderListModel.list[i].hsn_code;
                totalQty += parseFloat($scope.orderListModel.list[i].qty);
                ordereditems.custom_opt1 = '';
                ordereditems.custom_opt2 = '';
                ordereditems.custom_opt3 = '';
                ordereditems.custom_opt4 = '';
                ordereditems.custom_opt5 = '';
                if ($scope.orderListModel.list[i].custom_opt1 != '' && $scope.orderListModel.list[i].custom_opt1 != null)
                {
                    ordereditems.custom_opt1 = $scope.orderListModel.list[i].custom_opt1;
                }
                if ($scope.orderListModel.list[i].custom_opt2 != '' && $scope.orderListModel.list[i].custom_opt2 != null)
                {
                    ordereditems.custom_opt2 = $scope.orderListModel.list[i].custom_opt2;
                }
                if ($scope.orderListModel.list[i].custom_opt3 != '' && $scope.orderListModel.list[i].custom_opt3 != null)
                {
                    ordereditems.custom_opt3 = $scope.orderListModel.list[i].custom_opt3;
                }
                if ($scope.orderListModel.list[i].custom_opt4 != '' && $scope.orderListModel.list[i].custom_opt4 != null)
                {
                    ordereditems.custom_opt4 = $scope.orderListModel.list[i].custom_opt4;
                }
                if ($scope.orderListModel.list[i].custom_opt5 != '' && $scope.orderListModel.list[i].custom_opt5 != null)
                {
                    ordereditems.custom_opt5 = $scope.orderListModel.list[i].custom_opt5;
                }
                ordereditems.custom_opt1_key = $rootScope.appConfig.invoice_item_custom1_label;
                ordereditems.custom_opt2_key = $rootScope.appConfig.invoice_item_custom2_label;
                ordereditems.custom_opt3_key = $rootScope.appConfig.invoice_item_custom3_label;
                ordereditems.custom_opt4_key = $rootScope.appConfig.invoice_item_custom4_label;
                ordereditems.custom_opt5_key = $rootScope.appConfig.invoice_item_custom5_label;
                ordereditems.total_price = $scope.orderListModel.list[i].sales_price * ordereditems.qty;
                ordereditems.emp_code = $scope.orderListModel.list[i].empCode.employee_code;
                ordereditems.emp_id = $scope.orderListModel.list[i].empCode.id;
                if (ordereditems.id != '')
                {
                    createOrderParam.item.push(ordereditems);
                }
            }
            createOrderParam.total_qty = totalQty;
            createOrderParam.customattribute = [];
            for (var i = 0; i < $scope.customAttributeList.length; i++)
            {
                var customattributeItem = {};
                if ($scope.customAttributeList[i].input_type == 'date')
                {
                    if ($scope.customAttributeList[i].value != '' && $scope.customAttributeList[i].value != null && $scope.customAttributeList[i].value != undefined)
                    {
                        if (typeof $scope.customAttributeList[i].value == 'object')
                        {
                            $scope.customdate = utilityService.parseDateToStr($scope.customAttributeList[i].value, $scope.adminService.appConfig.date_format);
                        }
                        $scope.customAttributeList[i].value = utilityService.changeDateToSqlFormat($scope.customdate, $scope.adminService.appConfig.date_format);
                    } else
                    {
                        $scope.customAttributeList[i].value = '';
                    }
                }
                if ($scope.customAttributeList[i].input_type == "YesOrNo")
                {
                    if ($scope.customAttributeList[i].value == true || $scope.customAttributeList[i].value == "yes")
                    {
                        $scope.customAttributeList[i].value = "yes";
                    } else
                    {
                        $scope.customAttributeList[i].value = "no";
                    }
                }
                if ($scope.customAttributeList[i].value != undefined)
                {
                    customattributeItem.value = $scope.customAttributeList[i].value;
                } else if ($scope.customAttributeList[i].default_value != undefined)
                {
                    customattributeItem.value = $scope.customAttributeList[i].default_value;
                } else
                {
                    customattributeItem.value = "";
                }
                customattributeItem.attribute_code = $scope.customAttributeList[i].attribute_code;
                customattributeItem.sno = $scope.customAttributeList[i].id;
                createOrderParam.customattribute.push(customattributeItem);
            }
            createOrderParam.deal_id = $scope.orderListModel.dealId;
            createOrderParam.deal_name = $scope.orderListModel.dealName;
//        if (parseFloat(createOrderParam.total_amount) > 0)
//        {
            if ($scope.orderListModel.round > 0)
            {
                adminService.addInvoice(createOrderParam, headers).then(function (response) {
                    if (response.data.success == true)
                    {
                        //                    if (typeof $stateParams.deal_id != 'undefined' && $stateParams.deal_id != null && $stateParams.deal_id != '')
                        //                    {
                        //                        $scope.createNotes();
                        //                    }
                        //                    else
                        //                    {
                        $scope.formReset();
                        if ($rootScope.appConfig.invoice_print_template === 't1')
                        {
                            $state.go('app.invoiceView1', {
                                'id': response.data.id
                            }, {
                                'reload': true
                            });
                        } else if ($rootScope.appConfig.invoice_print_template === 't2')
                        {
                            $state.go('app.invoiceView2', {
                                'id': response.data.id
                            }, {
                                'reload': true
                            });
                        } else
                        {
                            $state.go('app.invoiceView1', {
                                'id': response.data.id
                            }, {
                                'reload': true
                            });
                        }
                        //   }
                    }
                    $scope.isDataSavingProcess = false;


                }).catch(function (response) {
                    console.error('Error occurred:', response.status, response.data);
                    $scope.isDataSavingProcess = false;
                    var element = document.getElementById("btnLoad");
                    element.classList.remove("btn-loader");
                });
            } else
            {
                sweet.show('Oops...', 'Invoice amount cannot be negative', 'error');
                $scope.isDataSavingProcess = false;
                var element = document.getElementById("btnLoad");
                element.classList.remove("btn-loader");
            }
        }
        $scope.getCustomAttributeList = function () {

            var getListParam = {};
            getListParam.id = "";
            getListParam.attributetype_id = "";
            getListParam.attribute_code = "";
            getListParam.attributetypecode = "invoice";
            getListParam.mode = "1";

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getAttributeList(getListParam, configOption).then(function (response) {
                var data = response.data.list;
                $scope.customAttributeList = data;

                for (var i = 0; i < $scope.customAttributeList.length; i++)
                {
                    if ($scope.customAttributeList[i].input_type == "YesOrNo")
                    {
                        if ($scope.customAttributeList[i].default_value.toLowerCase() == "yes")
                        {
                            $scope.customAttributeList[i].value = true;
                        } else
                        {
                            $scope.customAttributeList[i].value = false;
                        }
                    }
                }


            });
        };

        $scope.$on('customSelectOptionChangeEvent', function (event, data)
        {
            console.log(data);
            $scope.updateCustomSelectValue(data);
        });
        $scope.updateCustomSelectValue = function (data)
        {
            for (var i = 0; i < $scope.customAttributeList.length; i++)
            {
                if ($scope.customAttributeList[i].attribute_code == data.code)
                {
                    $scope.customAttributeList[i].value = data.value;
                    break;
                }
            }
        }

        $scope.getCustomAttributeList();

        /*
         * Listening the Custom field value change
         */
        $scope.$on('customfieldValueChangeEvent', function (event, data)
        {
            console.log(data);
            $scope.updateCustomFieldValue(data);
        });
        $scope.updateCustomFieldValue = function (data)
        {
            for (var i = 0; i < $scope.customAttributeList.length; i++)
            {
                if ($scope.customAttributeList[i].attribute_code == data.code)
                {
                    $scope.customAttributeList[i].value = data.value;
                    break;
                }
            }
        }


        $scope.isPrefixListLoaded = false;
        $scope.getPrefixList = function ()
        {
            $scope.defaultprefix = false;
            var getListParam = {};
            getListParam.setting = 'sales_invoice_prefix';
            var headers = {};
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getAppSettingsList(getListParam, configOption, headers).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    if (data.list.length > 0)
                    {
                        var perfix = data.list[0].value;
                        perfix = perfix.split('\n');
                        for (var i = 0; i < perfix.length; i++)
                        {
                            if (perfix[i].split('-')[1] == 'default')
                            {
                                perfix[i] = perfix[i].split('-')[0];
                                $scope.orderListModel.invoicePrefix = perfix[i] + '';
                                $scope.defaultprefix = true;
                            }
                            if ($scope.defaultprefix == false)
                            {
                                $scope.orderListModel.invoicePrefix = '';
                            }
                            $scope.orderListModel.prefixList.push(perfix[i]);

                        }
                    }

                }
                $scope.getNextInvoiceno();
                $scope.isPrefixListLoaded = true;
            });

        };

        $scope.isNextAutoGeneratorNumberLoaded = false;
        $scope.getNextInvoiceno = function ()
        {
            $scope.isNextAutoGeneratorNumberLoaded = true;
            var getListParam = {};
            getListParam.type = "sales_invoice";
            getListParam.prefix = $scope.orderListModel.invoicePrefix;

            var headers = {};
            var configOption = adminService.handleOnlyErrorResponseConfig;
            configOption.perfix = getListParam.prefix;
            adminService.getNextInvoiceNo(getListParam, configOption, headers).then(function (response)
            {
                var config = response.config.config;
                if (response.data.success === true && typeof config.perfix !== 'undefined' && config.perfix == $scope.orderListModel.invoicePrefix)
                {
                    if (!$scope.orderListModel.is_next_autogenerator_num_changed)
                    {
                        $scope.orderListModel.next_autogenerator_num = response.data.no;
                    }
                }
                $scope.isNextAutoGeneratorNumberLoaded = false;

            });

        };

        $scope.invoiceNumberChange = function ()
        {

            $scope.orderListModel.is_next_autogenerator_num_changed = true;
            $timeout(function () {
                $scope.orderListModel.invoiceno = $scope.orderListModel.next_autogenerator_num;
            }, 0);
        }

        $scope.getTaxListInfo = function ( )
        {
            $scope.isLoadedTax = false;
            var getListParam = {};
            getListParam.id = "";
            getListParam.name = "";
            getListParam.start = 0;
            getListParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;

            adminService.getActiveTaxList(getListParam, configOption).then(function (response)
            {
                var data = response.data.list;
                $scope.orderListModel.activeTaxList = data;
                $scope.isLoadedTax = true;
            });
        };

        $scope.updateDiscountToAll = function ()
        {
            for (var i = 0; i < $scope.orderListModel.list.length; i++)
            {
                $scope.orderListModel.list[i].discountPercentage = $scope.orderListModel.list[i].discountPercentage;
            }
            $scope.calculatetotal();
        }

        $scope.getTaxListInfo();
        $scope.checkId();
//        $scope.getNextInvoiceno();
        $scope.getPrefixList();
    }]);




