app.controller('invoiceListCtrl', ['$scope', '$rootScope', '$state', '$stateParams', 'adminService', 'utilityService', '$filter', 'Auth', '$timeout', '$httpService', '$localStorage', 'APP_CONST', 'ValidationFactory', function ($scope, $rootScope, $state, $stateParams, adminService, utilityService, $filter, Auth, $timeout, $httpService, $localStorage, APP_CONST, ValidationFactory) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.invoiceModel = {
            currentPage: 1,
            total: 0,
            limit: 4,
            list: [],
            status: '',
            filterCustomerId: '',
            isLoadingProgress: true,
            isSummaryProgress: true
        };
        $scope.validationFactory = ValidationFactory;
        $scope.pagePerCount = [50, 100];
        $scope.invoiceModel.limit = $scope.pagePerCount[0];
        $scope.currencyFormat = $rootScope.appConfig.currency;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.adminService = adminService;
        $scope.searchFilter =
                {
                    id: '',
                    invoiceId: '',
                    duedate: '',
                    invoicedate: '',
                    customerInfo: {},
                    status: '',
                    notes: '',
                    customAttribute: []
                };
        $scope.refreshScreen = function ()
        {
            $scope.searchFilter = {
                invoicedate: '',
                duedate: '',
                customerInfo: {},
                invoiceId: '',
                status: '',
                customAttribute: []
            };
            $scope.initTableFilter();
            $scope.noResults = false;
        }
        $scope.invoicedateOpen = false;
        $scope.duedateOpen = false;
        //    'dd-MM-yyyy';
        $scope.openDate = function (index)
        {
            if (index == 0)
            {
                $scope.invoicedateOpen = true;
            } else if (index == 1)
            {
                $scope.duedateOpen = true;
            }
        }

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getinvoiceListInfo, 300);
        }
        $scope.customAttributeList = [];
        $scope.isdeleteProgress = false;
        $scope.selectInvoiceId = '';
        $scope.showdeletePopup = false;
        $scope.showPopup = function (id)
        {
            $scope.selectInvoiceId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function ()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteProgress = false;
        };

        $scope.showOptions = false;
        $scope.openOptions = function (index)
        {
            for (var i = 0; i < $scope.invoiceModel.list.length; i++)
            {
                $scope.invoiceModel.list[i].show = false;
                var element = "#highlight_" + i;
                $(element).removeClass("highlight");
                var popup = "#popup_" + i;
                $(popup).hide();
            }
            var selectedElement = "#highlight_" + index;
            $(selectedElement).addClass("highlight");
            var selectedPopup = "#popup_" + index;
            $(selectedPopup).show();
            $scope.invoiceModel.list[index].show = true;
            $scope.showOptions = true;
        }
        $scope.closeOptions = function (index)
        {
            for (var i = 0; i < $scope.invoiceModel.list.length; i++)
            {
                $scope.invoiceModel.list[i].show = false;
            }
            $scope.invoiceModel.list[index].show = false;
            $scope.showOptions = false;
        }

        $scope.deleteInvoiceItem = function ( )
        {
            if ($scope.isdeleteProgress == false)
            {
                $scope.isdeleteProgress = true;

                var getListParam = {};
                getListParam.id = $scope.selectInvoiceId;
                var headers = {};
                headers['screen-code'] = 'salesinvoice';
                adminService.deleteInvoice(getListParam, getListParam.id, headers).then(function (response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.getinvoiceListInfo();
                    }
                    $scope.closePopup();

                    $scope.isdeleteProgress = false;
                });

            }

        };
        $scope.print = function (id, pop)
        {
            if ($rootScope.appConfig.invoice_print_template === 't1')
            {
                $state.go('app.invoiceView1', {'id': id, 'pop': pop}, {'reload': true});
            } else if ($rootScope.appConfig.invoice_print_template === 't2')
            {
                $state.go('app.invoiceView2', {'id': id, 'pop': pop}, {'reload': true});
            } else
            {
                $state.go('app.invoiceView1', {'id': id, 'pop': pop}, {'reload': true});
            }
        };
        $scope.$watch('searchFilter.customerInfo', function (newVal, oldVal)
        {
            if (typeof newVal === 'undefined' || newVal === '')
            {
//            if ($scope.searchFilter.customerInfo != null && typeof $scope.searchFilter.customerInfo != 'undefined' && typeof $scope.searchFilter.customerInfo.id != 'undefined')
                {
                    $scope.initTableFilter();
                }
            }
        });
        $scope.length = 0;
        $scope.colspan = 9;
        $scope.pagespan = 7;
        $scope.getCustomAttributeList = function () {

            var getListParam = {};
            getListParam.id = "";
            getListParam.attributetype_id = "";
            getListParam.attribute_code = "";
            getListParam.attributetypecode = "invoice";
            getListParam.mode = "1";
            getListParam.is_show_in_list = "1";
            getListParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getAttributeList(getListParam, configOption).then(function (response)
            {
                var data = response.data.list;
                if (data.length != 0)
                {
                    $scope.customAttributeList = data;
                    for (var i = 0; i < $scope.customAttributeList.length; i++)
                    {
                        if ($scope.customAttributeList[i].input_type == 'dropdown')
                        {
                            $scope.optionList = [];
                            $scope.optionList = $scope.customAttributeList[i].option_value.split(',');
                            $scope.customAttributeList[i].option_value = $scope.optionList;
                        }
                    }
                    $scope.length = $scope.customAttributeList.length;
                    $scope.colspan = $scope.colspan + $scope.length;
                    $scope.pagespan = $scope.colspan - 2;
                }
            });
        };

        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'invoice_form';


        $scope.$on('onLocalStorageFormRenterCompleteEvent', function (event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });

        $scope.$on('onLocalStorageFieldRenterCompleteEvent', function (event, formName) {

            if ($scope.localStorageFormName == formName)
            {
                $scope.localStorageRenterCount++;
            }
            if ($scope.localStorageRenterCount >= $scope.localStorageCount)
            {
                console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
            }


        });
        $scope.$on('updateTypeaheadFieldValue', function (event, data) {

            if (data.fieldName == "invoice_form-customer_dropdown")
            {
                $scope.searchFilter.customerInfo = data.value;
            }
        });



        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function (event, formName) {

            if ($scope.localStorageFormName == formName)
            {
                $scope.localStorageRetrieveCount++;
            }
            if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
            {
                $scope.initTableFilter();
            }
            console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);
        });

        $scope.init = function ()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');
        };

        $scope.$on('onLocalStorageReadyEvent', function () {

            $timeout(function () {
                $scope.init();
            }, 300);
        });





        $scope.invoiceSummary = {};
        $scope.getInvoiceSummaryList = function () {
            $scope.invoiceModel.isSummaryProgress = true;
            var getListParam = {};
            getListParam.invoice_code = $scope.searchFilter.invoiceId;
            if ($scope.searchFilter.customerInfo != null && typeof $scope.searchFilter.customerInfo != 'undefined' && typeof $scope.searchFilter.customerInfo.id != 'undefined')
            {
                getListParam.customer_id = $scope.searchFilter.customerInfo.id;
            } else
            {
                getListParam.customer_id = '';
            }
            if ($scope.searchFilter.invoicedate != null && $scope.searchFilter.invoicedate != '')
            {
                if ($scope.searchFilter.invoicedate != null && typeof $scope.searchFilter.invoicedate == 'object')
                {
                    getListParam.invoice_date = utilityService.parseDateToStr($scope.searchFilter.invoicedate, $scope.dateFormat);
                }
                getListParam.invoice_date = utilityService.changeDateToSqlFormat(getListParam.invoice_date, $scope.dateFormat);
            }

            if ($scope.searchFilter.duedate != null && $scope.searchFilter.duedate != '' && $scope.searchFilter.duedate != null && $scope.searchFilter.toDate != '')
            {
                if ($scope.searchFilter.duedate != null && typeof $scope.searchFilter.duedate == 'object')
                {
                    getListParam.due_date = utilityService.parseDateToStr($scope.searchFilter.duedate, $scope.dateFormat);
                }
                getListParam.due_date = utilityService.changeDateToSqlFormat(getListParam.due_date, $scope.dateFormat);
            }
            getListParam.start = ($scope.invoiceModel.currentPage - 1) * $scope.invoiceModel.limit;
            getListParam.limit = $scope.invoiceModel.limit;
            getListParam.status = $scope.searchFilter.status;
            getListParam.notes = $scope.searchFilter.notes;
            adminService.getInvoiceSummaryList(getListParam).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.invoiceSummary = data;
                    $scope.invoiceSummary.overdue.amount = (parseFloat($scope.invoiceSummary.overdue.amount)).toFixed(2);
                    $scope.invoiceSummary.overdue.amount = utilityService.changeCurrency($scope.invoiceSummary.overdue.amount, $rootScope.appConfig.thousand_seperator);
                    $scope.invoiceSummary.todate.amount = (parseFloat($scope.invoiceSummary.todate.amount)).toFixed(2);
                    $scope.invoiceSummary.todate.amount = utilityService.changeCurrency($scope.invoiceSummary.todate.amount, $rootScope.appConfig.thousand_seperator);
                    $scope.invoiceSummary.nextdate.amount = (parseFloat($scope.invoiceSummary.nextdate.amount)).toFixed(2);
                    $scope.invoiceSummary.nextdate.amount = utilityService.changeCurrency($scope.invoiceSummary.nextdate.amount, $rootScope.appConfig.thousand_seperator);
                }
                $scope.invoiceModel.isSummaryProgress = false;
            });
        };

        $scope.getCustomerList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            autosearchParam.mode = 1;
            if (autosearchParam.search != '')
            {
                return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function (responseData)
                {
                    var data = responseData.data.list;
                    var hits = data;
                    if (hits.length > 10)
                    {
                        hits.splice(10, hits.length);
                    }
                    return hits;
                });
            }
        };

        $scope.formatCustomerModel = function (model)
        {
            if (model != null)
            {
                if (model.fname != undefined && model.phone != undefined && model.email != undefined)
                {
                    return model.fname + '(' + model.phone + ',' + model.email + ')';
                } else if (model.fname != undefined && model.phone != undefined)
                {
                    return model.fname + '(' + model.phone + ')';
                }
            }
            return  '';
        };

        $scope.update = function (isfilteropen)
        {
            $scope.searchFilter.customAttribute = [];
            if (isfilteropen == false)
            {
                $scope.initTableFilter();
            }
        }
        $scope.findCustomFilterVar = function ()
        {
            var j = 0;
            var data = '&';
            for (var i = 0; i < $scope.customAttributeList.length; i++)
            {
                if ($scope.searchFilter.customAttribute[i] != 'undefined' && $scope.searchFilter.customAttribute[i] != null && $scope.searchFilter.customAttribute[i] != '')
                {
                    var j = j + 1;
                    if (j > 1)
                    {
                        data = data + '&' + 'cus_attr_' + j + '=' + $scope.customAttributeList[i].attribute_code + '::' + $scope.searchFilter.customAttribute[i];
                    } else
                    {
                        data = data + 'cus_attr_' + j + '=' + $scope.customAttributeList[i].attribute_code + '::' + $scope.searchFilter.customAttribute[i];
                    }
                }
            }
            if (data != '&')
            {
                return data;
            }
        }

        $scope.getinvoiceListInfo = function ()
        {
            $scope.dateFormat = $rootScope.appConfig.date_format;
            $scope.invoiceModel.isLoadingProgress = true;
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'salesinvoice';
            getListParam.invoice_code = $scope.searchFilter.invoiceId;
            if ($scope.searchFilter.customerInfo != null && typeof $scope.searchFilter.customerInfo != 'undefined' && typeof $scope.searchFilter.customerInfo.id != 'undefined')
            {
                getListParam.customer_id = $scope.searchFilter.customerInfo.id;
            } else
            {
                getListParam.customer_id = '';
            }
            if ($scope.searchFilter.invoicedate != null && $scope.searchFilter.invoicedate != '')
            {
                if ($scope.searchFilter.invoicedate != null && typeof $scope.searchFilter.invoicedate == 'object')
                {
                    getListParam.invoice_date = utilityService.parseDateToStr($scope.searchFilter.invoicedate, $scope.dateFormat);
                }
                getListParam.invoice_date = utilityService.changeDateToSqlFormat(getListParam.invoice_date, $scope.dateFormat);
            }

            if ($scope.searchFilter.duedate != null && $scope.searchFilter.duedate != '' && $scope.searchFilter.duedate != null && $scope.searchFilter.toDate != '')
            {
                if ($scope.searchFilter.duedate != null && typeof $scope.searchFilter.duedate == 'object')
                {
                    getListParam.due_date = utilityService.parseDateToStr($scope.searchFilter.duedate, $scope.dateFormat);
                }
                getListParam.due_date = utilityService.changeDateToSqlFormat(getListParam.due_date, $scope.dateFormat);
            }
            var j = 0;
            for (var i = 0; i < $scope.customAttributeList.length; i++)
            {
                if ($scope.searchFilter.customAttribute[i] != 'undefined' && $scope.searchFilter.customAttribute[i] != null && $scope.searchFilter.customAttribute[i] != '')
                {
                    var j = j + 1;
                    getListParam['cus_attr_' + j] = $scope.customAttributeList[i].attribute_code + '::' + $scope.searchFilter.customAttribute[i];
                }
            }
            getListParam.start = ($scope.invoiceModel.currentPage - 1) * $scope.invoiceModel.limit;
            getListParam.limit = $scope.invoiceModel.limit;
            getListParam.is_active = 1;
            getListParam.status = $scope.searchFilter.status;
            getListParam.notes = $scope.searchFilter.notes;
            getListParam.mode = 0;
            getListParam.show = false;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getInvoiceList(getListParam, configOption, headers).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.invoiceModel.list = data.list;
                    if ($scope.invoiceModel.list.length > 0)
                    {
                        for (var i = 0; i < $scope.invoiceModel.list.length; i++)
                        {
                            $scope.invoiceModel.list[i].newdate = utilityService.parseStrToDate($scope.invoiceModel.list[i].date);
                            $scope.invoiceModel.list[i].date = utilityService.parseDateToStr($scope.invoiceModel.list[i].newdate, $scope.adminService.appConfig.date_format);
                            $scope.invoiceModel.list[i].newduedate = utilityService.parseStrToDate($scope.invoiceModel.list[i].duedate);
                            $scope.invoiceModel.list[i].duedate = utilityService.parseDateToStr($scope.invoiceModel.list[i].newduedate, $scope.adminService.appConfig.date_format);
                            $scope.invoiceModel.list[i].total_amount = utilityService.changeCurrency($scope.invoiceModel.list[i].total_amount, $rootScope.appConfig.thousand_seperator);
                            $scope.invoiceModel.list[i].paid_amount = utilityService.changeCurrency($scope.invoiceModel.list[i].paid_amount, $rootScope.appConfig.thousand_seperator);
                            $scope.invoiceModel.list[i].show = false;
                        }
                    }
                    $scope.getInvoiceSummaryList();
                    $scope.invoiceModel.total = data.total;
                }
                $scope.invoiceModel.isLoadingProgress = false;
            });
        };
        //$scope.getinvoiceListInfo();
        $scope.getCustomAttributeList();
        $scope.getInvoiceSummaryList();
    }]);




