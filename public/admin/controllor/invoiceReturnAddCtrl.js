
app.controller('invoiceReturnAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', 'sweet', function ($scope, $rootScope, adminService, $httpService, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST, sweet) {

        $scope.orderListModel =
                {
                    "id": 0,
                    "productname": "",
                    "sku": "",
                    "unit": "",
                    "mrp": "",
                    "sales_price": "",
                    "qty": "",
                    "rowtotal": "",
                    "isActive": "1",
                    "total": 0,
                    "customername": "",
                    "phone": "",
                    "addressId": "",
                    "subtotal": 0,
                    "taxAmt": 0,
                    "taxInfo": "",
                    "discountAmt": 0,
                    list: [],
                    "prefixList": [],
                    "billno": "",
                    "billdate": "",
                    "date": "",
                    "coupon": "",
                    "shipping": 0,
                    "discamount": 0,
                    "code": "",
                    "customerInfo": "",
                    "invoiceDetail": {},
                    "paymenttype": "CASH",
                    "couponcode": "",
                    "invoicePrefix": "",
                    "paymentTerm": "",
                    "taxPercentage": "",
                    "discountPercentage": "",
                    "totalDisPercentage": "0",
                    "totalTaxPercentage": "",
                    "discountamount": "",
                    "taxamount": "",
                    "cardDetail": "",
                    "duedate": "",
                    "tax_id": "",
                    "discountMode": "",
                    "balance_amount": "",
                    "paid_amount": "",
                    "productList": [],
                    "productId": '',
                    "isLoadingProgress": false,
                    "notes": '',
                    "roundoff": '',
                    "invoiceNo": '',
                    "taxAmountDetail": {
                        "tax_amount": ''
                    },
                    "subtotalWithDiscount": '',
                    "taxMappingDetail": [],
                    "activeTaxList": []
                };
        $scope.orderListModel.date = new Date();
        $scope.adminService = adminService;
        $scope.customAttributeList = [];
        $scope.validationFactory = ValidationFactory;
        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function ()
        {
            if (typeof $scope.order_edit_form != 'undefined' && typeof $scope.order_edit_form.$pristine != 'undefined' && !$scope.order_edit_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function ()
        {
            $scope.order_edit_form.$setPristine();
            $scope.orderListModel.invoiceDetail = {};
            $scope.orderListModel.list = [];
            $scope.getInvoiceInfo( );
        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.resetOrderCreate = function () {

            $scope.orderListModel.user = '';
        }

        $scope.invoiceEditId = $stateParams.id;
        $scope.opened = false;
        $scope.initTableFilterTimeoutPromise = null;
        $scope.deletedProductList = [];
        $scope.undeletedProductList = [];
        $scope.showbillPopup = false;
        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }
        $scope.maxDate = $scope.maxDate ? null : new Date();
        $scope.startDateOpen = false;
        $scope.dueDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currencyFormat = $rootScope.appConfig.currency;
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.currentDate = new Date();

        $scope.openDate = function (index)
        {
            if (index == 0)
            {
                $scope.startDateOpen = true;
            } else if (index == 1)
            {
                $scope.dueDateOpen = true;
            }
        }

        $scope.senderEmail = '';
        $scope.receiver = [];
        $scope.ccEmail = '';
        $scope.showPopup = function (index) {
            $scope.showpopup = true;
            $scope.mailHeader = "Send a Receipt";
            $scope.senderEmail = $scope.orderListModel.customerInfo.email;
            $scope.receiver = [];
            var newRow = {
                "email": $scope.orderListModel.customerInfo.email
            };
            $scope.receiver.push(newRow);
            $scope.ccEmail = $scope.orderListModel.customerInfo.email;

            console.log("Sender Email ", $scope.senderEmail);
            console.log("Sender Email ", $scope.receiverEmail);
            console.log("Sender Email ", $scope.ccEmail);
            $scope.$broadcast("initPopupEvent");
        };

        $scope.addMoreReceiverEmail = function ( )
        {
            var newRow = {
                "email": ""
            };
            $scope.receiver.push(newRow);
        }

        $scope.closePopup = function () {
            $scope.showpopup = false;
        }

        $scope.getCustomerList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = [];
                if (data.length > 0)
                {
                    for (var i = 0; i < data.length; i++)
                    {
                        if (data[i].is_sale == '1' || data[i].is_sale == 1)
                        {
                            hits.push(data[i]);
                        }
                    }
                }
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatCustomerModel = function (model)
        {
            if (model != null)
            {
                if (model.fname != undefined && model.phone != undefined && model.email != undefined)
                {
                    return model.fname + '(' + model.phone + ',' + model.email + ')';
                } else if (model.fname != undefined && model.phone != undefined)
                {
                    return model.fname + '(' + model.phone + ')';
                }
            }
            return  '';
        };


        $scope.$watch('orderListModel.customerInfo', function (newVal, oldVal)
        {
            if (typeof newVal == 'undefined' || newVal == '' || newVal == null)
            {
                $scope.orderListModel.customerInfo = '';
            } else
            {
                var addrss = "";
                if (newVal.billing_address != undefined && newVal.billing_address != '')
                {
                    addrss += newVal.billing_address;
                }
                if (newVal.billing_city != undefined && newVal.billing_city != '')
                {
                    addrss += '\n' + newVal.billing_city;
                }
                if (newVal.billing_pincode != undefined && newVal.billing_pincode != '')
                {
                    if (newVal.billing_city != undefined && newVal.billing_city != '')
                    {
                        addrss += '-' + newVal.billing_pincode;
                    } else
                    {
                        addrss += newVal.billing_pincode;
                    }
                }

                if (newVal.billing_state != undefined && newVal.billing_state != '')
                {
                    addrss += '\n' + newVal.billing_state;
                }
                if (addrss == '' && newVal.id != undefined && $scope.orderListModel.customerInfo.id != undefined && $scope.orderListModel.customerInfo.id == newVal.id)
                {
                    $scope.orderListModel.customerInfo.shopping_address = $scope.orderListModel.invoiceDetail.customer_address;
                } else
                {
                    $scope.orderListModel.customerInfo.shopping_address = addrss;
                }
            }
        });

        $scope.isPrefixListLoaded = false;
        $scope.getPrefixList = function ()
        {
            var getListParam = {};
            getListParam.setting = 'sales_invoice_prefix';
            var headers = {};
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getAppSettingsList(getListParam, configOption, headers).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    if (data.list.length > 0)
                    {
                        var perfix = data.list[0].value;
                        perfix = perfix.split('\n');
                        for (var i = 0; i < perfix.length; i++)
                        {
                            $scope.orderListModel.prefixList.push(perfix[i]);
                        }
                    }

                }
                $scope.isPrefixListLoaded = true;
            });

        };

        $scope.getUomList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;

            return $httpService.get(APP_CONST.API.UOM, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatUomModel = function (model)
        {
            if (model != null)
            {
                return model.name;
            }
            return  '';
        };

        $scope.isLoadedTax = false;
//        $scope.getTaxListInfo = function ( )
//        {
//            $scope.isLoadedTax = false;
//            var getListParam = {};
//            getListParam.id = "";
//            getListParam.name = "";
//            getListParam.start = 0;
//            getListParam.is_active = 1;
//            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
//
//            adminService.getTaxList(getListParam, configOption).then(function (response)
//            {
//                var data = response.data.list;
//                $scope.orderListModel.taxInfo = data;
//                $scope.isLoadedTax = true;
//            });
//        };

        $scope.totalcalculationTimeoutPromise != null
        $scope.updateInvoiceTotal = function ()
        {
            if ($scope.totalcalculationTimeoutPromise != null)
            {
                $timeout.cancel($scope.totalcalculationTimeoutPromise);
            }
            $scope.totalcalculationTimeoutPromise = $timeout($scope.calculatetotal, 300);
        }

        $scope.initUpdateDetailTimeoutPromise = null;

        $scope.initUpdateDetail = function ()
        {

            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
            }

            if ($stateParams.id != "" && $scope.isLoadedTax)
            {
                $scope.updateInvoiceDetails();
            } else
            {
                $scope.initUpdateDetailTimeoutPromise = $timeout($scope.initUpdateDetail, 300);
            }
        }

        $scope.updateInvoiceDetails = function ( )
        {
            $scope.orderListModel.customerInfo = {};
            $scope.orderListModel.id = $scope.orderListModel.invoiceDetail.id;
            $scope.orderListModel.customerInfo.id = $scope.orderListModel.invoiceDetail.customer_id;
            $scope.orderListModel.customerInfo.fname = $scope.orderListModel.invoiceDetail.customer_fname;
            $scope.orderListModel.customerInfo.shopping_address = $scope.orderListModel.invoiceDetail.customer_address;
            $scope.orderListModel.customerInfo.phone = $scope.orderListModel.invoiceDetail.phone;
            $scope.orderListModel.customerInfo.email = $scope.orderListModel.invoiceDetail.email;
            $scope.orderListModel.subtotal = $scope.orderListModel.invoiceDetail.subtotal;
            $scope.orderListModel.notes = $scope.orderListModel.invoiceDetail.notes;
            $scope.orderListModel.taxAmt = $scope.orderListModel.invoiceDetail.tax_amount;
            //$scope.orderListModel.taxAmountDetail.tax_amount = $scope.orderListModel.invoiceDetail.tax_amount;
            $scope.orderListModel.discountAmt = $scope.orderListModel.invoiceDetail.discount_amount;
            $scope.orderListModel.total = $scope.orderListModel.invoiceDetail.total_amount;
            $scope.orderListModel.paid_amount = $scope.orderListModel.invoiceDetail.paid_amount;
            $scope.orderListModel.balance_amount = (parseFloat($scope.orderListModel.total - $scope.orderListModel.paid_amount)).toFixed(2);
            $scope.orderListModel.invoiceCode = $scope.orderListModel.invoiceDetail.invoice_code;
            $scope.orderListModel.invoiceNo = $scope.orderListModel.invoiceDetail.invoice_no;
            $scope.orderListModel.invoicePrefix = $scope.orderListModel.invoiceDetail.prefix;
            var idate = utilityService.parseStrToDate($scope.orderListModel.invoiceDetail.date);
            $scope.orderListModel.billdate = idate;
            var duedate = utilityService.parseStrToDate($scope.orderListModel.invoiceDetail.duedate);
            $scope.orderListModel.duedate = duedate;
            $scope.orderListModel.discountPercentage = $scope.orderListModel.invoiceDetail.discount_percentage + '';
            $scope.orderListModel.discountMode = $scope.orderListModel.invoiceDetail.discount_mode;
            $scope.orderListModel.tax_id = 'invoiced_tax_rate';
            $scope.customAttributeList = $scope.orderListModel.invoiceDetail.customattribute;
            var loop;
            $scope.orderListModel.taxMappingDetail = [];
            for (loop = 0; loop < $scope.orderListModel.invoiceDetail.tax_summary.length; loop++)
            {
                var tax = {};
                tax.tax_id = $scope.orderListModel.invoiceDetail.tax_summary[loop].tax_id;
                tax.taxName = $scope.orderListModel.invoiceDetail.tax_summary[loop].tax_name;
                tax.taxPercentage = $scope.orderListModel.invoiceDetail.tax_summary[loop].tax_percentage;
                tax.overallTaxAmount = parseFloat($scope.orderListModel.invoiceDetail.tax_summary[loop].amount);
                $scope.orderListModel.taxMappingDetail.push(tax);
            }

            for (loop = 0; loop < $scope.orderListModel.invoiceDetail.item.length; loop++)
            {
                var newRow =
                        {
                            "invoice_item_id": $scope.orderListModel.invoiceDetail.item[loop].id,
                            "id": $scope.orderListModel.invoiceDetail.item[loop].product_id,
                            "purchase_invoice_item_id": $scope.orderListModel.invoiceDetail.item[loop].purchase_invoice_item_id,
                            "productName": $scope.orderListModel.invoiceDetail.item[loop].product_name,
                            "product_name": $scope.orderListModel.invoiceDetail.item[loop].product_name,
                            "name": $scope.orderListModel.invoiceDetail.item[loop].product_name,
                            "sku": $scope.orderListModel.invoiceDetail.item[loop].product_sku,
                            "sales_price": $scope.orderListModel.invoiceDetail.item[loop].unit_price,
                            "mrp_price": $scope.orderListModel.invoiceDetail.item[loop].mrp_price,
                            "qty": parseInt($scope.orderListModel.invoiceDetail.item[loop].qty) - parseInt($scope.orderListModel.invoiceDetail.item[loop].refund_qty),
                            "balance_qty": parseInt($scope.orderListModel.invoiceDetail.item[loop].qty) - parseInt($scope.orderListModel.invoiceDetail.item[loop].refund_qty),
                            "invoiceqty": $scope.orderListModel.invoiceDetail.item[loop].qty,
                            "rowtotal": $scope.orderListModel.invoiceDetail.item[loop].total_price,
                            "discountPercentage": $scope.orderListModel.invoiceDetail.item[loop].discount_value,
                            "discountAmount": $scope.orderListModel.invoiceDetail.item[loop].discount_amount,
                            "discountMode": $scope.orderListModel.invoiceDetail.item[loop].discount_mode,
                            "taxPercentage": $scope.orderListModel.invoiceDetail.item[loop].tax_percentage,
                            "tax_id": $scope.orderListModel.invoiceDetail.item[loop].tax_id + '',
                            "taxAmount": $scope.orderListModel.invoiceDetail.item[loop].tax_amount,
                            "uom": $scope.orderListModel.invoiceDetail.item[loop].uom_name,
                            "uomid": $scope.orderListModel.invoiceDetail.item[loop].uom_id,
                            "custom_opt1": $scope.orderListModel.invoiceDetail.item[loop].custom_opt1,
                            "custom_opt2": $scope.orderListModel.invoiceDetail.item[loop].custom_opt2,
                            "custom_opt3": $scope.orderListModel.invoiceDetail.item[loop].custom_opt3,
                            "custom_opt4": $scope.orderListModel.invoiceDetail.item[loop].custom_opt4,
                            "custom_opt5": $scope.orderListModel.invoiceDetail.item[loop].custom_opt5,
                            "taxMapping": $scope.orderListModel.invoiceDetail.item[loop].tax_list,
                            "hsn_code": $scope.orderListModel.invoiceDetail.item[loop].hsn_code
                        };
                $scope.orderListModel.list.push(newRow);
                $scope.orderListModel.list[loop].productname = newRow;
                console.log('invoicelist');
                console.log($scope.orderListModel.list);
            }
            for (var index = 0; index < $scope.orderListModel.list.length; index++)
            {
                var taxparam = {};
                var taxName = '';
                taxparam.tax_id = $scope.orderListModel.list[index].tax_id;
                for (var i = 0; i < $scope.orderListModel.activeTaxList.length; i++)
                {
                    if ($scope.orderListModel.activeTaxList[i].id == $scope.orderListModel.list[index].tax_id)
                    {
                        $scope.orderListModel.list[index].taxPercentage = $scope.orderListModel.activeTaxList[i].tax_percentage;
                        taxName = $scope.orderListModel.activeTaxList[i].tax_name;
                    }
                }
                taxparam.tax_name = taxName;
                taxparam.tax_percentage = $scope.orderListModel.list[index].taxPercentage;
                $scope.orderListModel.list[index].taxMapping = [];
                $scope.orderListModel.list[index].taxMapping.push(taxparam);
            }
            //    $scope.addNewProduct();
            $scope.updateInvoiceTotal();
            $scope.orderListModel.isLoadingProgress = false;
        }

        $scope.getProductList = function ()
        {
            $scope.orderListModel.isLoadingProgress = true;
            var getParam = {};
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getProductBasedList(getParam, configOption).then(function (response) {

                var data = response.data;
                $scope.orderListModel.productList = data.list;
                $scope.orderListModel.isLoadingProgress = false;
            });
        }

        $scope.formatProductModel = function (model, index)
        {
            if (model != null)
            {
                $scope.updateProductInfo(model, index);
                return model.name;
            }
            return  '';
        }

        $scope.updateProductInfo = function (model, index)
        {
            $scope.qtyExist = false;
            if ($scope.orderListModel.list.length != 0)
            {
                if (typeof model == 'undefined')
                    return;

                $scope.orderListModel.list[index].id = model.id;
                $scope.orderListModel.list[index].sku = model.sku;
                $scope.orderListModel.list[index].product_name = model.product_name;
                $scope.orderListModel.list[index].name = model.product_name;
                $scope.orderListModel.list[index].productName = model.product_name;
                $scope.orderListModel.list[index].sales_price = model.sales_price;
                $scope.orderListModel.list[index].static_sales_price = model.sales_price;
                $scope.orderListModel.list[index].custom_opt1 = model.custom_opt1;
                $scope.orderListModel.list[index].custom_opt2 = model.custom_opt2;
                $scope.orderListModel.list[index].custom_opt3 = model.custom_opt3;
                $scope.orderListModel.list[index].custom_opt4 = model.custom_opt4;
                $scope.orderListModel.list[index].custom_opt5 = model.custom_opt5;
                $scope.orderListModel.list[index].qty_hand = model.qty_in_hand;
                if (model.qty == 0 || model.qty != null && model.qty != '')
                {
                    $scope.orderListModel.list[index].qty = model.qty;
                } else
                {
                    $scope.orderListModel.list[index].qty = 1;
                }
//                if (model.tierPricing != undefined && model.tierPricing.length > 0)
//                {
//                    for (var j = 0; j < model.tierPricing.length; j++)
//                    {
//                        if ($scope.orderListModel.list[index].qty >= parseFloat(model.tierPricing[j].from_qty) && $scope.orderListModel.list[index].qty <= parseFloat(model.tierPricing[j].to_qty))
//                        {
//                            $scope.qtyExist = true;
//                            $scope.selectedQtyIndex = j;
//                        }
//                        if (j == model.tierPricing.length - 1 && $scope.qtyExist)
//                        {
//                            $scope.orderListModel.list[index].sales_price = parseFloat(model.tierPricing[$scope.selectedQtyIndex].unit_price);
//                        } else if (!$scope.qtyExist)
//                        {
//                            $scope.orderListModel.list[index].sales_price = parseFloat(model.sales_price);
//                        }
//                    }
//                } else
//                {
//                    $scope.orderListModel.list[index].sales_price = model.sales_price;
//                }
                //$scope.orderListModel.list[index].tierPricing = model.tierPricing;
                $scope.orderListModel.list[index].rowtotal = model.sales_price;
                if (model.purchase_invoice_item_id != 'undefined' && model.purchase_invoice_item_id != null)
                {
                    $scope.orderListModel.list[index].purchase_invoice_item_id = model.purchase_invoice_item_id;
                }
                if (model.discountPercentage != 'undefined' && model.discountPercentage != null && model.discountPercentage != '')
                {
                    $scope.orderListModel.list[index].discountPercentage = model.discountPercentage;
                } else
                {
                    $scope.orderListModel.list[index].discountPercentage = 0;
                }
                if (model.discountAmount != 'undefined' && model.discountAmount != null && model.discountAmount != '')
                {
                    $scope.orderListModel.list[index].discountAmount = model.discountAmount;
                } else
                {
                    $scope.orderListModel.list[index].discountAmount = 0;
                }
                if (model.discountMode != 'undefined' && model.discountMode != null && model.discountMode != '')
                {
                    $scope.orderListModel.list[index].discountMode = model.discountMode;
                } else
                {
                    $scope.orderListModel.list[index].discountMode = 0;
                }
                if (model.taxPercentage != 'undefined' && model.taxPercentage != null && model.taxPercentage != '')
                {
                    $scope.orderListModel.list[index].taxPercentage = model.taxPercentage;
                } else
                {
                    $scope.orderListModel.list[index].taxPercentage = 0;
                }

                if (model.hsn_code != undefined)
                {
                    $scope.orderListModel.list[index].hsncode = model.hsn_code;
                }
                $scope.orderListModel.list[index].uomInfo = {
                    "name": model.uom,
                }
                if (model.uomid != undefined)
                {
                    $scope.orderListModel.list[index].uomid = model.uomid;
                }
                if (model.uom_id != undefined)
                {
                    $scope.orderListModel.list[index].uomid = model.uom_id;
                }
                if (model.uomid != undefined && model.uom_id != undefined)
                {
                    $scope.orderListModel.list[index].uomid = "";
                }

                $scope.orderListModel.list[index].tax_id = model.tax_id + '';
                if (model.taxMapping != 'undefined' && model.taxMapping != null && model.taxMapping != '')
                {
                    $scope.orderListModel.list[index].tax_id = model.taxMapping[0].tax_id + '';
                }
                for (var i = 0; i < $scope.orderListModel.activeTaxList.length; i++)
                {
                    if ($scope.orderListModel.activeTaxList[i].id == $scope.orderListModel.list[index].tax_id)
                    {
                        $scope.orderListModel.list[index].taxPercentage = $scope.orderListModel.activeTaxList[i].tax_percentage;
                    }
                }
                $scope.updateInvoiceTotal();

                console.log("Index for item edit ", index);
                console.log("List Length ", $scope.orderListModel.list.length);
                //  $scope.createNewProduct(index);
                return;
            } else
            {
                // $scope.createNewProduct(-1);
            }
        }
        $scope.initvalidateProductDataAndAddNewPromise = null;

        $scope.initValidateProductDataAndAddNew = function (index)
        {

            if ($scope.initvalidateProductDataAndAddNewPromise != null)
            {

                $timeout.cancel($scope.initvalidateProductDataAndAddNewPromise);
            }

            $scope.formValidator();
            $scope.initvalidateProductDataAndAddNewPromise = $timeout(
                    function () {
                        $scope.createNewProduct(index);
                    },
                    300);
        };

        $scope.formValidator = function ()
        {
            //performace issue in form validator bug
            if ($scope.order_add_form.$submitted)
            {
                $timeout(function () {
                    $scope.order_add_form.$submitted = false;
                }, 0);
                $timeout(function () {
                    $scope.order_add_form.$setSubmitted();
                }, 100);
            } else
            {
                $timeout(function () {
                    $scope.order_add_form.$setSubmitted();
                }, 100);
            }
        }

        $scope.getItemList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.PRODUCT_BASED_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.initGetPurchaseProductDetailTimeoutPromise = null;
        $scope.initGetPurchaseProductDetail = function (value, index)
        {
            if ($scope.initGetPurchaseProductDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initGetPurchaseProductDetailTimeoutPromise);
            }
            if (value != "")
            {
                $scope.initGetPurchaseProductDetailTimeoutPromise = $timeout(
                        function () {
                            $scope.getPurchaseProductDetail(value, index);
                        },
                        100);
            } else
            {
                if ($scope.orderListModel.list.length > 0)
                {
                    $scope.orderListModel.list[index].id = '';
                    $scope.orderListModel.list[index].purchase_invoice_item_id = '';
                    $scope.orderListModel.list[index].productName = '';
                    $scope.orderListModel.list[index].name = '';
                    $scope.orderListModel.list[index].sku = '';
                    $scope.orderListModel.list[index].sales_price = '';
                    $scope.orderListModel.list[index].static_sales_price = '';
                    $scope.orderListModel.list[index].mrp_price = '';
                    $scope.orderListModel.list[index].qty = '';
                    $scope.orderListModel.list[index].rowtotal = '';
                    $scope.orderListModel.list[index].taxMapping = [];
                    $scope.orderListModel.list[index].taxPercentage = '';
                    $scope.orderListModel.list[index].taxAmount = '';
                    $scope.orderListModel.list[index].discountPercentage = '';
                    $scope.orderListModel.list[index].discountAmount = '';
                    $scope.orderListModel.list[index].discountMode = '%';
                    $scope.orderListModel.list[index].tierPricing = [];
                }
                $scope.updateInvoiceTotal();
            }
        }

        $scope.getPurchaseProductDetail = function (id, index)
        {
            var getListParam = {};
            getListParam.id = id;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getProductBasedList(getListParam, configOption).then(function (response)
            {
                var data = response.data.list;
                if (response.data.total > 0)
                {
                    $scope.orderListModel.list[index].id = data[0].product_id;
                    $scope.orderListModel.list[index].purchase_invoice_item_id = id;
                    $scope.orderListModel.list[index].productName = data[0].product_name;
                    $scope.orderListModel.list[index].name = data[0].product_name;
                    $scope.orderListModel.list[index].product_name = data[0].product_name;
                    $scope.orderListModel.list[index].sku = data[0].sku;
                    $scope.orderListModel.list[index].sales_price = data[0].selling_price;
                    $scope.orderListModel.list[index].static_sales_price = data[0].selling_price;
                    $scope.orderListModel.list[index].mrp_price = data[0].mrp_price;
                    $scope.orderListModel.list[index].qty = 1;
                    $scope.orderListModel.list[index].rowtotal = data[0].selling_price;
                    $scope.orderListModel.list[index].taxMapping = data[0].taxMapping;
                    //$scope.orderListModel.list[index].taxPercentage = data[0].selling_tax_percentage;
                    $scope.orderListModel.list[index].taxAmount = 0;
                    $scope.orderListModel.list[index].discountPercentage = 0;
                    $scope.orderListModel.list[index].discountAmount = 0;
                    $scope.orderListModel.list[index].discountMode = '%';
                    $scope.orderListModel.list[index].tierPricing = data[0].tierPricing;
                    $scope.orderListModel.list[index].tax_id = data[0].taxMapping[0].tax_id + '';
//                    $scope.orderListModel.list[index].productname = {
//                        "name": data[0].product_name,
//                    };
                    $scope.orderListModel.list[index].productname = $scope.orderListModel.list[index];
                    for (var i = 0; i < $scope.orderListModel.activeTaxList.length; i++)
                    {
                        if ($scope.orderListModel.activeTaxList[i].id == $scope.orderListModel.list[index].tax_id)
                        {
                            $scope.orderListModel.list[index].taxPercentage = $scope.orderListModel.activeTaxList[i].tax_percentage;
                            var taxName = $scope.orderListModel.activeTaxList[i].tax_name;
                        }
                    }
                    var taxparam = {};
                    taxparam.tax_id = data[0].selling_tax_id;
                    taxparam.tax_name = taxName;
                    taxparam.tax_percentage = $scope.orderListModel.list[index].taxPercentage;
//                    $scope.orderListModel.list[index].taxMapping = [];
//                    $scope.orderListModel.list[index].taxMapping.push(taxparam);
//                    if ($scope.orderListModel.list[index].taxMapping.length > 0)
//                    {
//                        for (var i = 0; i < $scope.orderListModel.list[index].taxMapping.length; i++)
//                        {
//                            $scope.orderListModel.list[index].taxPercentage = $scope.orderListModel.list[index].taxPercentage + parseFloat($scope.orderListModel.list[index].taxMapping[i].tax_percentage);
//                        }
//                    } else
//                    {
//                        $scope.orderListModel.list[index].taxPercentage = 0;
//                    }
//                    $scope.addNewProduct();
                    if ($scope.orderListModel.list[$scope.orderListModel.list.length - 1].purchase_invoice_item_id != '')
                    {
                        $scope.addNewProduct();
                    }
                } else
                {
                    $scope.orderListModel.list[index].id = '';
                    $scope.orderListModel.list[index].productName = '';
                    $scope.orderListModel.list[index].name = '';
                    $scope.orderListModel.list[index].sku = '';
                    $scope.orderListModel.list[index].sales_price = '';
                    $scope.orderListModel.list[index].static_sales_price = '';
                    $scope.orderListModel.list[index].mrp_price = '';
                    $scope.orderListModel.list[index].qty = '';
                    $scope.orderListModel.list[index].rowtotal = '';
                    $scope.orderListModel.list[index].taxMapping = [];
                    $scope.orderListModel.list[index].taxPercentage = '';
                    $scope.orderListModel.list[index].tax_id = '';
                    $scope.orderListModel.list[index].taxAmount = '';
                    $scope.orderListModel.list[index].discountPercentage = '';
                    $scope.orderListModel.list[index].discountAmount = '';
                    $scope.orderListModel.list[index].discountMode = '%';
                    $scope.orderListModel.list[index].tierPricing = [];
                }
                $scope.updateTierPricing(index);

            });
        };

        $scope.updateTierPricing = function ($index)
        {
            $scope.newQtyExist = false;
            var newData = $scope.orderListModel.list[$index];
            if (newData.tierPricing != undefined && newData.tierPricing != null && newData.tierPricing.length > 0)
            {
                for (var j = 0; j < newData.tierPricing.length; j++)
                {
                    if (newData.qty >= parseFloat(newData.tierPricing[j].from_qty) && newData.qty <= parseFloat(newData.tierPricing[j].to_qty))
                    {
                        $scope.newQtyExist = true;
                        $scope.selectedNewQtyIndex = j;
                    }
                    if (j == newData.tierPricing.length - 1 && $scope.newQtyExist)
                    {
                        $scope.orderListModel.list[$index].sales_price = parseFloat(newData.tierPricing[$scope.selectedNewQtyIndex].unit_price);
                    } else if (!$scope.newQtyExist)
                    {
                        $scope.orderListModel.list[$index].sales_price = parseFloat(newData.static_sales_price);
                    }
                }
                $scope.updateInvoiceTotal();

            } else
            {
                $scope.updateInvoiceTotal();
            }
        }

        $scope.event = null;
        $scope.keyupHandler = function (event)
        {
            if (event.keyCode == 9)
            {
                event.preventDefault();
                var currentFocusField = angular.element(event.currentTarget);
                var nextFieldId = $(currentFocusField).attr('next-focus-id');
                var currentFieldId = $(currentFocusField).attr('id');
                var productIndex = $(currentFocusField).attr('product-index');
                if (nextFieldId == undefined || nextFieldId == '')
                {
                    return;
                }
                if (currentFieldId.indexOf('_item_') != -1)
                {
                    var filedIdSplitedValue = currentFieldId.split('_');
                    var existingBrunchCount = $scope.orderListModel.list.length - 1;
                    if (existingBrunchCount == filedIdSplitedValue[filedIdSplitedValue.length - 2] && filedIdSplitedValue[filedIdSplitedValue.length - 1] == 2)
                    {
                        $scope.addNewProduct();
                        $timeout(function () {
                            var newNextFieldId = filedIdSplitedValue[0] + '_' + filedIdSplitedValue[1] + '_' + ($scope.orderListModel.list.length - 1) + '_1';
                            $("#" + newNextFieldId).focus();
                        }, 300);
                        return;
                    }
                }
                $timeout(function () {
                    $("#" + nextFieldId).focus();
                    window.document.getElementById(nextFieldId).setSelectionRange(0, window.document.getElementById(nextFieldId).value.length);
                }, 300);
            }
        }

        $scope.createNewProduct = function (index)
        {
            var formDataError = false;
            if (index != -1)
            {
                var productContainer = window.document.getElementById('invoice_product_container');
                var errorCell = angular.element(productContainer).find('.has-error').length;
                if (errorCell > 0)
                {
                    formDataError = true;
                }
            }

            if (!formDataError)
            {
                if (index == $scope.orderListModel.list.length - 1)
                {

                    var newRow = {
                        "id": '',
                        "purchase_invoice_item_id": '',
                        "productName": '',
                        "sku": '',
                        "name": '',
                        "sales_price": '',
                        "mrp_price": '',
                        "qty": '',
                        "rowtotal": '',
                        "discountPercentage": '',
                        "discountAmount": '',
                        "discountMode": '%',
                        "taxPercentage": '',
                        "tax_id": '',
                        "hsn_code": '',
                        "taxAmount": '',
                        "uom": '',
                        "uomid": '',
                        "custom_opt1": '',
                        "custom_opt2": '',
                        "custom_opt3": '',
                        "custom_opt4": '',
                        "custom_opt5": '',
                        "taxMapping": '',
                        "tierPricing": '',
                        "static_sales_price": ''
                    }
                    $scope.orderListModel.list.push(newRow);
                }
            }

        }

        $scope.addNewProduct = function ()
        {
            var newRow = {
                "id": '',
                "purchase_invoice_item_id": '',
                "name": '',
                "productName": '',
                "sku": '',
                "sales_price": '',
                "mrp_price": '',
                "qty": '',
                "rowtotal": '',
                "discountPercentage": '',
                "discountAmount": '',
                "discountMode": '%',
                "taxPercentage": '',
                "tax_id": '',
                "hsn_code": '',
                "taxAmount": '',
                "uom": '',
                "uomid": '',
                "custom_opt1": '',
                "custom_opt2": '',
                "custom_opt3": '',
                "custom_opt4": '',
                "custom_opt5": '',
                "taxMapping": '',
                "tierPricing": '',
                "static_sales_price": ''
            }
            $scope.orderListModel.list.push(newRow);
        }
        $scope.deleteEvent = false;
        $scope.deleteProduct = function (id)
        {
            var index = -1;
            for (var i = 0; i < $scope.orderListModel.list.length; i++) {
                if (id == $scope.orderListModel.list[i].id) {
                    index = i;
                    break;
                }
            }
            $scope.orderListModel.list.splice(index, 1);
            $scope.calculatetotal();
            //$scope.calculateTaxAmt();
            $scope.deleteEvent = true;
        };


        $scope.calculatetotal = function ()
        {
            var subTotal = 0;
            var totDiscountPercentage = 0;
            var totDiscountAmt = 0;
            var totTaxPercentage = 0;
            var totTaxAmt = 0;
            console.log("bfr loop", $scope.orderListModel.list.length);
            for (var i = 0; i < $scope.orderListModel.list.length; i++)
            {
                if ($scope.orderListModel.list[i].id != '' && $scope.orderListModel.list[i].id != 'undefined' && $scope.orderListModel.list[i].id != null || $scope.orderListModel.list[i].product_name != null && $scope.orderListModel.list[i].product_name != "" && typeof $scope.orderListModel.list[i].product_name != "undefined")
                {
                    $scope.orderListModel.list[i].rowtotal = $scope.orderListModel.list[i].sales_price * $scope.orderListModel.list[i].qty;
                    subTotal += parseFloat($scope.orderListModel.list[i].rowtotal);
                    $scope.orderListModel.list[i].rowtotal = $scope.orderListModel.list[i].rowtotal.toFixed(2);
                    if ($scope.orderListModel.list[i].taxPercentage == null || $scope.orderListModel.list[i].taxPercentage == '' || isNaN($scope.orderListModel.list[i].taxPercentage))
                    {
                        $scope.orderListModel.list[i].taxPercentage = 0;
                    } else
                    {
                        $scope.orderListModel.list[i].taxPercentage = parseFloat($scope.orderListModel.list[i].taxPercentage);
                    }
                    totTaxPercentage += $scope.orderListModel.list[i].taxPercentage;

                    if ($scope.orderListModel.list[i].discountPercentage == null || $scope.orderListModel.list[i].discountPercentage == '' || isNaN($scope.orderListModel.list[i].discountPercentage))
                    {
                        $scope.orderListModel.list[i].discountPercentage = 0;
                    } else
                    {
                        $scope.orderListModel.list[i].discountPercentage = parseFloat($scope.orderListModel.list[i].discountPercentage);
                    }

                    if ($scope.orderListModel.list[i].discountMode == '%')
                    {
                        $scope.orderListModel.list[i].discountAmount = parseFloat(($scope.orderListModel.list[i].rowtotal * $scope.orderListModel.list[i].discountPercentage) / 100);
                        totDiscountPercentage += $scope.orderListModel.list[i].discountPercentage;
                    } else if ($scope.orderListModel.list[i].discountMode == 'amount')
                    {
                        $scope.orderListModel.list[i].discountAmount = $scope.orderListModel.list[i].discountPercentage;
                    }
                    $scope.orderListModel.list[i].discountAmount = $scope.orderListModel.list[i].discountAmount.toFixed(2);
                    if ($scope.orderListModel.list[i].discountAmount < 0)
                    {
                        $scope.orderListModel.list[i].discountAmount = 0;
                    }
                    $scope.orderListModel.list[i].rowtotal = parseFloat($scope.orderListModel.list[i].rowtotal) - parseFloat($scope.orderListModel.list[i].discountAmount);
                    totDiscountAmt += parseFloat($scope.orderListModel.list[i].discountAmount);
//                    $scope.orderListModel.list[i].taxAmount = parseFloat((($scope.orderListModel.list[i].rowtotal - $scope.orderListModel.list[i].discountAmount) * $scope.orderListModel.list[i].taxPercentage) / 100);
                    $scope.orderListModel.list[i].taxAmount = parseFloat($scope.orderListModel.list[i].rowtotal) - ((parseFloat($scope.orderListModel.list[i].rowtotal)) / (1 + (parseFloat($scope.orderListModel.list[i].taxPercentage) / 100)));
                    $scope.orderListModel.list[i].taxAmount = $scope.orderListModel.list[i].taxAmount.toFixed(2);
                    if ($scope.orderListModel.list[i].taxAmount < 0)
                    {
                        $scope.orderListModel.list[i].taxAmount = 0;
                    }
                    totTaxAmt += parseFloat($scope.orderListModel.list[i].taxAmount);
                }
            }
            $scope.orderListModel.subtotal = parseFloat(subTotal).toFixed(2);
            $scope.orderListModel.discountAmt = parseFloat(totDiscountAmt).toFixed(2);
            $scope.orderListModel.taxAmt = parseFloat(totTaxAmt).toFixed(2);
            $scope.orderListModel.discountPercentage = parseFloat(totDiscountPercentage).toFixed(2);
            if ($scope.orderListModel.subtotal > 0)
            {
                $scope.orderListModel.subtotalWithDiscount = $scope.orderListModel.subtotal - $scope.orderListModel.discountAmt;
                $scope.orderListModel.total = (parseFloat(subTotal) - parseFloat($scope.orderListModel.discountAmt)).toFixed(2);
            } else
            {
                $scope.orderListModel.subtotalWithDiscount = 0.00;
                $scope.orderListModel.total = 0.00;
            }
            $scope.orderListModel.balance_amount = (parseFloat($scope.orderListModel.total - $scope.orderListModel.paid_amount)).toFixed(2);
            $scope.updateRoundoff();
        }

        $scope.updateRoundoff = function ()
        {
            $scope.orderListModel.round = Math.round($scope.orderListModel.total);
            $scope.orderListModel.roundoff = $scope.orderListModel.round - parseFloat($scope.orderListModel.total);
            $scope.orderListModel.roundoff = parseFloat($scope.orderListModel.roundoff).toFixed(2);
            $scope.orderListModel.round = parseFloat($scope.orderListModel.round).toFixed(2);
            $scope.calculateTaxAmt();
        }


//        $scope.$watch('orderListModel.subtotalWithDiscount', function (newVal, oldVal)
//        {
//            if (typeof newVal != 'undefined' && newVal != '' && newVal != null)
//            {
//                $scope.calculateTaxAmt(false);
//            }
//        });

        $scope.calculateTaxAmt = function (forceSave)
        {
            $scope.orderListModel.taxMappingDetail = [];
            for (var i = 0; i < $scope.orderListModel.list.length; i++)
            {
                if ($scope.orderListModel.list[i].id != '' && $scope.orderListModel.list[i].id != 'undefined' && $scope.orderListModel.list[i].id != null)
                {
                    if ($scope.orderListModel.list[i].taxMapping.length > 0)
                    {
                        for (var j = 0; j < $scope.orderListModel.list[i].taxMapping.length; j++)
                        {
                            if ($rootScope.appConfig.sales_product_list.toUpperCase() == 'PURCHASE BASED')
                            {
                                var taxAmt = parseFloat($scope.orderListModel.list[i].rowtotal) - ((parseFloat($scope.orderListModel.list[i].rowtotal)) / (1 + (parseFloat($scope.orderListModel.list[i].taxMapping[j].tax_percentage) / 100)));
                            } else {
                                var taxAmt = parseFloat(($scope.orderListModel.list[i].taxMapping[j].tax_percentage * ($scope.orderListModel.list[i].rowtotal - $scope.orderListModel.list[i].discountAmount)) / 100);
                            }
                            $scope.orderListModel.list[i].taxMapping[j].tax_Amt = taxAmt;
                            console.log("bfr checkId", $scope.orderListModel.taxMappingDetail);
                            var result = $scope.checkTaxId($scope.orderListModel.list[i].taxMapping[j].tax_id, taxAmt);
                            if (result == false)
                            {
                                var param = {};
                                param.tax_id = $scope.orderListModel.list[i].taxMapping[j].tax_id;
                                param.taxName = $scope.orderListModel.list[i].taxMapping[j].tax_name;
                                param.taxPercentage = $scope.orderListModel.list[i].taxMapping[j].tax_percentage;
                                param.overallTaxAmount = parseFloat(taxAmt).toFixed(2);
                                $scope.orderListModel.taxMappingDetail.push(param);
                            }
                        }
                    }
                }
            }
        }

        $scope.checkTaxId = function (id, amt)
        {
            if ($scope.orderListModel.taxMappingDetail.length > 0)
            {
                for (var i = 0; i < $scope.orderListModel.taxMappingDetail.length; i++)
                {
                    console.log("taxMapping Detail", $scope.orderListModel.taxMappingDetail[i]);
                    if ($scope.orderListModel.taxMappingDetail[i].tax_id == id)
                    {
                        $scope.orderListModel.taxMappingDetail[i].overallTaxAmount = parseFloat(parseFloat($scope.orderListModel.taxMappingDetail[i].overallTaxAmount) + parseFloat(amt)).toFixed(2);
                        return true;
                    }
                }
                return false;
            }
            return false;
        };

        $scope.getInvoiceInfo = function ( )
        {
            $scope.orderListModel.isLoadingProgress = true;
            var getListParam = {};
            getListParam.id = $stateParams.id;
            getListParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getInvoiceDetail(getListParam, configOption).then(function (response)
            {
                var data = response.data.data;
                $scope.orderListModel.invoiceDetail = data;
                $scope.initUpdateDetail();
                //$scope.getTaxListInfo( );
            });
        }

        $scope.$on('customSelectOptionChangeEvent', function (event, data)
        {
            console.log(data);
            $scope.updateCustomSelectValue(data);
        });

        $scope.updateCustomSelectValue = function (data)
        {
            for (var i = 0; i < $scope.customAttributeList.length; i++)
            {
                if ($scope.customAttributeList[i].attribute_code == data.code)
                {
                    $scope.customAttributeList[i].value = data.value;
                    break;
                }
            }
        }

        $scope.$on('customfieldValueChangeEvent', function (event, data)
        {
            console.log(data);
            $scope.updateCustomFieldValue(data);
        });

        $scope.updateCustomFieldValue = function (data)
        {
            for (var i = 0; i < $scope.customAttributeList.length; i++)
            {
                if ($scope.customAttributeList[i].attribute_code == data.code)
                {
                    $scope.customAttributeList[i].value = data.value;
                    break;
                }
            }
        }

        $scope.getInvoiceInfo( );
        $scope.getProductList( );
        //$scope.updateProductInfo();
        $scope.updateOrder = function (forceSave)
        {
            if (!forceSave)
            {
                if ($scope.isDataSavingProcess)
                {
                    return;
                }
            }
            $scope.isDataSavingProcess = true;
            $scope.isSave = false;
            for (var i = 0; i < $scope.orderListModel.list.length; i++)
            {
                $scope.isSave = true;
            }
//                if (i != $scope.orderListModel.list.length - 1)
//                {
//                    if ($rootScope.appConfig.uomdisplay)
//                    {
//                        if ($scope.orderListModel.list[i].id != '' && $scope.orderListModel.list[i].id != null &&
//                                $scope.orderListModel.list[i].productName != '' && $scope.orderListModel.list[i].productName != null &&
//                                $scope.orderListModel.list[i].uomInfo != '' && $scope.orderListModel.list[i].uomInfo != null &&
//                                $scope.orderListModel.list[i].qty != '' && $scope.orderListModel.list[i].qty != null &&
//                                $scope.orderListModel.list[i].sales_price != '' && $scope.orderListModel.list[i].sales_price != null &&
//                                $scope.orderListModel.list[i].rowtotal != '' && $scope.orderListModel.list[i].rowtotal != null)
//                        {
//                            $scope.isSave = true;
//                        } else
//                        {
//                            $scope.isSave = false;
//                        }
//                    } else
//                    {
//                        if ($scope.orderListModel.list[i].id != '' && $scope.orderListModel.list[i].id != null &&
//                                $scope.orderListModel.list[i].productName != '' && $scope.orderListModel.list[i].productName != null &&
//                                $scope.orderListModel.list[i].qty != '' && $scope.orderListModel.list[i].qty != null &&
//                                $scope.orderListModel.list[i].sales_price != '' && $scope.orderListModel.list[i].sales_price != null &&
//                                $scope.orderListModel.list[i].rowtotal != '' && $scope.orderListModel.list[i].rowtotal != null)
//                        {
//                            $scope.isSave = true;
//                        } else
//                        {
//                            $scope.isSave = false;
//                        }
//                    }
//                }
//            }
//
            if ($scope.isSave)
            {

                $scope.saveOrderRefund();

            } else
            {
                sweet.show('Oops...', 'Fill Product Detail.. ', 'error');
                $scope.isDataSavingProcess = false;
            }

        };

        $scope.isNextAutoGeneratorNumberLoaded = false;
        $scope.getNextInvoiceno = function ()
        {
            $scope.isNextAutoGeneratorNumberLoaded = true;
            var getListParam = {};
            getListParam.type = "sales_invoice";
            getListParam.prefix = $scope.orderListModel.invoicePrefix;

            var headers = {};
            var configOption = adminService.handleOnlyErrorResponseConfig;
            configOption.perfix = getListParam.prefix;
            adminService.getNextInvoiceNo(getListParam, configOption, headers).then(function (response)
            {
                var config = response.config.config;
                if (response.data.success === true && typeof config.perfix !== 'undefined' && config.perfix == $scope.orderListModel.invoicePrefix)
                {
                    if (!$scope.orderListModel.is_next_autogenerator_num_changed)
                    {
                        $scope.orderListModel.invoiceNo = response.data.no;
                    }
                }
                $scope.isNextAutoGeneratorNumberLoaded = false;

            });

        };

        $scope.saveOrderRefund = function ()
        {
            var updateOrderParam = {};
            var headers = {};
            updateOrderParam.invoice_id = $scope.orderListModel.id;
            if (typeof $scope.orderListModel.date == 'object')
            {
                $scope.orderListModel.date = utilityService.parseDateToStr($scope.orderListModel.date, $scope.adminService.appConfig.date_format);
            }
            updateOrderParam.date = utilityService.changeDateToSqlFormat($scope.orderListModel.date, $scope.adminService.appConfig.date_format);
            updateOrderParam.customer_address = $scope.orderListModel.customerInfo.shopping_address;
            updateOrderParam.subtotal = $scope.orderListModel.subtotal;
            updateOrderParam.tax_amount = (parseFloat($scope.orderListModel.taxAmt)).toFixed(2);
            updateOrderParam.discount_amount = $scope.orderListModel.discountAmt;
            updateOrderParam.total_amount = $scope.orderListModel.round;
            updateOrderParam.round_off = $scope.orderListModel.roundoff;
            updateOrderParam.notes = $scope.orderListModel.notes;
            updateOrderParam.item = [];

            updateOrderParam.customer_id = $scope.orderListModel.customerInfo.id;
            for (var i = 0; i < $scope.orderListModel.list.length; i++)
            {
                var ordereditems = {};
                ordereditems.invoice_item_id = $scope.orderListModel.list[i].invoice_item_id
                ordereditems.product_id = $scope.orderListModel.list[i].id;
                ordereditems.purchase_invoice_item_id = $scope.orderListModel.list[i].purchase_invoice_item_id;
                ordereditems.discount_mode = $scope.orderListModel.list[i].discountMode;
                ordereditems.discount_value = $scope.orderListModel.list[i].discountPercentage;
                ordereditems.discount_amount = $scope.orderListModel.list[i].discountAmount;
                ordereditems.product_sku = $scope.orderListModel.list[i].sku;
                ordereditems.product_name = $scope.orderListModel.list[i].product_name;
                ordereditems.hsn_code = $scope.orderListModel.list[i].hsn_code;
                ordereditems.unit_price = parseFloat($scope.orderListModel.list[i].sales_price);
                ordereditems.mrp_price = parseFloat($scope.orderListModel.list[i].mrp_price);
                ordereditems.qty = parseInt($scope.orderListModel.list[i].qty);
                if (typeof $scope.orderListModel.list[i].uomInfo != undefined && $scope.orderListModel.list[i].uomInfo != null && $scope.orderListModel.list[i].uomInfo != '')
                {
                    if ($scope.orderListModel.list[i].uomInfo.name != null && $scope.orderListModel.list[i].uomInfo.name != '')
                    {
                        ordereditems.uom_name = $scope.orderListModel.list[i].uomInfo.name;
                    }
                }
                if ($scope.orderListModel.list[i].uomid == '' || $scope.orderListModel.list[i].uomid == undefined || $scope.orderListModel.list[i].uomid == null)
                {
                    $scope.orderListModel.list[i].uomid = 0;
                }
                ordereditems.tax_id = $scope.orderListModel.list[i].tax_id;
                ordereditems.tax_percentage = $scope.orderListModel.list[i].taxPercentage;
                ordereditems.tax_amount = $scope.orderListModel.list[i].taxAmount;

                ordereditems.uom_id = $scope.orderListModel.list[i].uomid;
                ordereditems.custom_opt1 = '';
                ordereditems.custom_opt2 = '';
                ordereditems.custom_opt3 = '';
                ordereditems.custom_opt4 = '';
                ordereditems.custom_opt5 = '';
                if ($scope.orderListModel.list[i].custom_opt1 != '' && $scope.orderListModel.list[i].custom_opt1 != null)
                {
                    ordereditems.custom_opt1 = $scope.orderListModel.list[i].custom_opt1;
                }
                if ($scope.orderListModel.list[i].custom_opt2 != '' && $scope.orderListModel.list[i].custom_opt2 != null)
                {
                    ordereditems.custom_opt2 = $scope.orderListModel.list[i].custom_opt2;
                }
                if ($scope.orderListModel.list[i].custom_opt3 != '' && $scope.orderListModel.list[i].custom_opt3 != null)
                {
                    ordereditems.custom_opt3 = $scope.orderListModel.list[i].custom_opt3;
                }
                if ($scope.orderListModel.list[i].custom_opt4 != '' && $scope.orderListModel.list[i].custom_opt4 != null)
                {
                    ordereditems.custom_opt4 = $scope.orderListModel.list[i].custom_opt4;
                }
                if ($scope.orderListModel.list[i].custom_opt5 != '' && $scope.orderListModel.list[i].custom_opt5 != null)
                {
                    ordereditems.custom_opt5 = $scope.orderListModel.list[i].custom_opt5;
                }
                ordereditems.custom_opt1_key = $rootScope.appConfig.purchase_invoice_item_custom1_label;
                ordereditems.custom_opt2_key = $rootScope.appConfig.purchase_invoice_item_custom2_label;
                ordereditems.custom_opt3_key = $rootScope.appConfig.purchase_invoice_item_custom3_label;
                ordereditems.custom_opt4_key = $rootScope.appConfig.purchase_invoice_item_custom4_label;
                ordereditems.custom_opt5_key = $rootScope.appConfig.purchase_invoice_item_custom5_label;
                ordereditems.total_price = $scope.orderListModel.list[i].sales_price * ordereditems.qty;
                if (ordereditems.id != '')
                {
                    updateOrderParam.item.push(ordereditems);
                }

            }

            adminService.invoiceReturnSave(updateOrderParam, headers).then(function (response)
            {
                if (response.data.success == true)
                {

                    $scope.formReset();
                    $state.go('app.invoiceView1', {'id': $stateParams.id}, {'reload': true});
                }
                $scope.isDataSavingProcess = false;
            });
        }

        $scope.getTaxListInfo = function ( )
        {
            $scope.isLoadedTax = false;
            var getListParam = {};
            getListParam.id = "";
            getListParam.name = "";
            getListParam.start = 0;
            getListParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;

            adminService.getActiveTaxList(getListParam, configOption).then(function (response)
            {
                var data = response.data.list;
                $scope.orderListModel.activeTaxList = data;
                $scope.isLoadedTax = true;
            });
        };

        $scope.getTaxListInfo();
        $scope.getPrefixList();
    }]);