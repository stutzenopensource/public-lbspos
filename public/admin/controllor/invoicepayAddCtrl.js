app.controller('invoicepayAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', 'utilityService', '$timeout', '$filter', 'Auth', 'ValidationFactory', '$state','$stateParams', function($scope, $rootScope, adminService, $httpService, APP_CONST, utilityService, $timeout, $filter, Auth, ValidationFactory, $state,$stateParams) {
    $scope.paymentAddModel = 
    {
        "id": 0,
        "account":'',                 
        list: [],
        "comments": " ",
        "invoicedate":"",            
        "amount": '',
        "paymentMode":''
    }
    $scope.validationFactory = ValidationFactory;
    $scope.getNavigationBlockMsg = function(pageReload)
    {
        if (typeof $scope.create_payment_form != 'undefined' && typeof $scope.create_payment_form.$pristine != 'undefined' && !$scope.create_payment_form.$pristine)
        {
            return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
        }

        return "";
    }
    $scope.isDataSavingProcess = false;
    $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
    $scope.formReset = function() 
    {
        $scope.create_payment_form.$setPristine();
        $scope.paymentAddModel.account = '';
        $scope.paymentAddModel.comments = '';
        $scope.paymentAddModel.invoicedate = '';        
        $scope.paymentAddModel.paymentMode = '';        
        $scope.paymentAddModel.amount = '';
        $scope.paymentAddModel.comments = '';
    }        

    $scope.dateFormat = "dd-MMMM-yyyy"
    $scope.paymentAddModel.invoicedate = new Date();    
    $scope.maxDate = $scope.maxDate ? null : new Date();
    $scope.invoiceDateOpen = false;    
    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };
    $scope.openDate = function(index) 
    {
        if (index == 0)
        {
            $scope.invoiceDateOpen = true;
        }
    }    

    $scope.fillDefaultInfo = function ( )
    {
        var getListParam = {};        
        getListParam.id =$stateParams.id;        
        adminService.getInvoiceList(getListParam).then(function(response) 
        {
            var data = response.data.data;
            $scope.paymentAddModel.account = data.customer_fname;
            $scope.paymentAddModel.amount = data.total_amount;
        });            
    }

    $scope.fillDefaultInfo( );
        
    $scope.createPayment = function()
    {
        $scope.isDataSavingProcess = true;
        var createPaymentParam = {};
        createPaymentParam.id = 0;
        createPaymentParam.companyId = $rootScope.userModel.companyId;
        createPaymentParam.userid = $rootScope.userModel.userid;
        createPaymentParam.clientId = $scope.paymentAddModel.clientInfo.id;
        createPaymentParam.comments = $scope.paymentAddModel.comments;
        createPaymentParam.amount = $scope.paymentAddModel.amount;

        adminService.createPayment(createPaymentParam).then(function(response)
        {
            if (response.data.success == true)
            {
                $scope.formReset();
                $state.go('app.invoice');
            }
            $scope.isDataSavingProcess = false;
        });
    };
}]);

