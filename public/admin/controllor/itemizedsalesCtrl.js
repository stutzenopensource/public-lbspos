app.controller('itemizedsalesCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$localStorage', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', function ($scope, $rootScope, adminService, $httpService, $localStorage, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout) {
        $rootScope.getNavigationBlockMsg = null;
        $scope.itemizedsalesModel = {
            currentPage: 1,
            total: 0,
            total_Amount: 0,
            limit: 10,
            list: [],
            length: [],
            productname: '',
            uom: '',
            quantity: '',
            amount: '',
            serverList: null,
            isLoadingProgress: false,
            isSearchLoadingProgress: false,
            categorylist: []
        };
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.itemizedsalesModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
        $scope.validationFactory = ValidationFactory;
        $scope.checked = false;
        $scope.showCategory = false;
        $scope.openDate = function (index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            } else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };

        $scope.searchFilter = {
            fromdate: '',
            todate: '',
            categoryinfo: {}
        };
        $scope.searchFilter.fromdate = $scope.currentDate;
        $scope.clearFilters = function ()
        {
            $scope.searchFilter = {
                fromdate: '',
                todate: '',
                categoryinfo: {}
            };
            $scope.checked = false;
            $scope.showCategory = false;
            $scope.itemizedsalesModel.list = [];
            $scope.searchFilter.fromdate = $scope.currentDate;
        }
        $scope.winprint = function (div)
        {
            var docHead = document.head.outerHTML;
            var printContents = document.getElementById(div).outerHTML;
            var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=865, height=600, resizable=yes, screenX=200, screenY=200, personalbar=no, scrollbars=yes";

            var newWin = window.open("", "_blank", winAttr);
            var writeDoc = newWin.document;
            writeDoc.open();
            writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
            writeDoc.close();
            newWin.focus();
        };

        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.validateDateFilterData = function ()
        {
            var retVal = false;
            if ($scope.searchFilter.todate !== '' || $scope.searchFilter.fromdate !== '')
            {
                retVal = true;
            }
            return retVal;
        };

        $scope.show = function (index)
        {
            //            for (var i = 0; i < $scope.itemizedsalesModel.list[index].data; i++)
            //            {
            //                if ($scope.itemizedsalesModel.list[index].data[i].flag == index)
            //                {
            if ($scope.showDetails[index])
            {
                $scope.showDetails[index] = false;
            } else
                $scope.showDetails[index] = true;
            //                }
            //            }
        };
        $scope.todate = '';
        $scope.getList = function () {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'itemsalesreport';
            if ($scope.searchFilter.todate != '' && ($scope.searchFilter.fromdate == null || $scope.searchFilter.fromdate == ''))
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
                getListParam.from_date = getListParam.to_date;
                $scope.todate = getListParam.to_date;
            } else if ($scope.searchFilter.fromdate != '' && ($scope.searchFilter.todate == null || $scope.searchFilter.todate == ''))
            {
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
                getListParam.to_date = getListParam.from_date;
                $scope.todate = getListParam.to_date;
            } else
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
                $scope.todate = getListParam.to_date;
            }
            if ($scope.checked)
            {
                $scope.showCategory = true;
                getListParam.show_all = 1;
            } else
            {
                $scope.showCategory = false;
            }
            getListParam.start = 0;
            getListParam.limit = 0;
            
            if ($scope.searchFilter.categoryInfo != null && typeof $scope.searchFilter.categoryInfo != 'undefined' && typeof $scope.searchFilter.categoryInfo.id != 'undefined')
            {
                getListParam.cat_id = $scope.searchFilter.categoryInfo.id;
            } else
            {
                getListParam.cat_id = '';
            }
            
            $scope.itemizedsalesModel.isLoadingProgress = true;
            $scope.showDetails = [];
            if ($scope.searchFilter.todate != '' && $scope.searchFilter.todate != null && $scope.searchFilter.todate != 'undefined' && $scope.searchFilter.fromdate != '' && $scope.searchFilter.fromdate != null)
            {
                adminService.getItemizedSalesList(getListParam, headers).then(function (response)
                {
                    var totalAmt = 0.00;
                    if (response.data.success === true)
                    {
                        var data = response.data.list;
                        $scope.itemizedsalesModel.list = data;

                        for (var i = 0; i < $scope.itemizedsalesModel.list.length; i++)
                        {
                            totalAmt = totalAmt + parseFloat($scope.itemizedsalesModel.list[i].total_price);
                            $scope.itemizedsalesModel.list[i].total_price = utilityService.changeCurrency($scope.itemizedsalesModel.list[i].total_price, $rootScope.appConfig.thousand_seperator);
                            $scope.itemizedsalesModel.list[i].flag = i;
                            $scope.showDetails[i] = false;
                            if ($scope.checked)
                            {
                                for (var j = 0; j < $scope.itemizedsalesModel.list[i].product.length; j++)
                                {
                                    $scope.itemizedsalesModel.list[i].product[j].flag = i;
                                }
                            } else
                            {
                                for (var j = 0; j < $scope.itemizedsalesModel.list[i].data.length; j++)
                                {
                                    $scope.itemizedsalesModel.list[i].data[j].newdate = utilityService.parseStrToDate($scope.itemizedsalesModel.list[i].data[j].date);
                                    $scope.itemizedsalesModel.list[i].data[j].date = utilityService.parseDateToStr($scope.itemizedsalesModel.list[i].data[j].newdate, $scope.adminService.appConfig.date_format);
                                    $scope.itemizedsalesModel.list[i].data[j].flag = i;
                                    $scope.itemizedsalesModel.list[i].data[j].selling_price = parseFloat($scope.itemizedsalesModel.list[i].data[j].selling_price).toFixed(2);
                                }
                            }
                        }
                        $scope.itemizedsalesModel.total_Amount = totalAmt;
                        $scope.itemizedsalesModel.total_Amount = parseFloat($scope.itemizedsalesModel.total_Amount).toFixed(2);
                        $scope.itemizedsalesModel.total_Amount = utilityService.changeCurrency($scope.itemizedsalesModel.total_Amount, $rootScope.appConfig.thousand_seperator);

                    }
                    $scope.itemizedsalesModel.isLoadingProgress = false;
                });
            } else {
                $scope.itemizedsalesModel.isLoadingProgress = false;
            }

        };
        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'itemized_report_form';
        $scope.$on('onLocalStorageFormRenterCompleteEvent', function (event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });

        $scope.$on('onLocalStorageFieldRenterCompleteEvent', function (event, formName) {

            if ($scope.localStorageFormName == formName)
            {
                $scope.localStorageRenterCount++;
            }
            if ($scope.localStorageRenterCount >= $scope.localStorageCount)
            {
                console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);

            }
        });

        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function (event, formName) {

            if ($scope.localStorageFormName == formName)
            {
                $scope.localStorageRetrieveCount++;
            }
            if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
            {
                $scope.initTableFilter();
            }
            console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);

        });

        $scope.init = function ()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');

        };

        $scope.$on('onLocalStorageReadyEvent', function () {

            $timeout(function () {
                $scope.init();
            }, 300);


        });




        $scope.print = function (id)
        {
            if ($rootScope.appConfig.invoice_print_template === 't1')
            {
                $state.go('app.invoiceView1', {
                    'id': id
                }, {
                    'reload': true
                });
            } else if ($rootScope.appConfig.invoice_print_template === 't2')
            {
                $state.go('app.invoiceView2', {
                    'id': id
                }, {
                    'reload': true
                });
            } else
            {
                $state.go('app.invoiceView1', {
                    'id': id
                }, {
                    'reload': true
                });
            }
        };
        $scope.getCategoryList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.GET_CATEGORY_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatCategoryModel = function (model)
        {
            if (model != null)
            {
                if (model.name != undefined)
                {
                    return model.name;
                }
            }
            return  '';
        };
        
        //$scope.getList();
    }]);




