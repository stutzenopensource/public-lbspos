app.controller('itemwiseAgeingReportCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', '$stateParams', function ($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout, $stateParams) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.itemwiseAgeingModel = {
            currentPage: 1,
            total: 0,
            total_Amount: 0,
            limit: 10,
            list: [],
            length: [],
            date: '',
            invoice_id: '',
            customer_id: '',
            amount: '',
            status: '',
            //             sku: '',

            serverList: null,
            isLoadingProgress: false,
            isSearchLoadingProgress: false,
            categorylist: []
        };
        $scope.showDetails = [];
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.itemwiseAgeingModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat);
        $scope.stockDate = $filter('date')($scope.currentDate, $scope.dateFormat);
        $scope.validationFactory = ValidationFactory;
        $scope.searchFilter = {
            fromdate: '',
            todate: '',
            productInfo: {},
            inventory : '',
            categoryinfo: {}

        };
        $scope.searchFilter.fromdate = $scope.currentDate;
        $scope.openDate = function (index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            } else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };


        $scope.clearFilters = function ()
        {
            $scope.searchFilter = {
                fromdate: '',
                todate: '',
                productInfo: {},
                inventory : '',
                categoryinfo: {}

            };
            $scope.searchFilter.fromdate = $scope.currentDate;
            $scope.itemwiseAgeingModel.list = [];
        }
        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'itemiwseAgeing_form';


        $scope.$on('onLocalStorageFormRenterCompleteEvent', function (event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });

        $scope.$on('onLocalStorageFieldRenterCompleteEvent', function (event, formName) {

            if ($scope.localStorageFormName == formName)
            {
                $scope.localStorageRenterCount++;
            }
            if ($scope.localStorageRenterCount >= $scope.localStorageCount)
            {
                console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
            }


        });
        $scope.$on('updateTypeaheadFieldValue', function (event, data) {

            if (data.fieldName == "itemiwseAgeing_form-product_dropdown")
            {
                $scope.searchFilter.productInfo = data.value;
            }
        });

        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function (event, formName) {

            if ($scope.localStorageFormName == formName)
            {
                $scope.localStorageRetrieveCount++;
            }
            if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
            {
                $scope.initTableFilter();
            }
            console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);

        });

        $scope.init = function ()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');

        };

        $scope.$on('onLocalStorageReadyEvent', function () {

            $timeout(function () {
                $scope.init();
            }, 300);


        });
        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.validateDateFilterData = function ()
        {
            var retVal = false;
            if ($scope.searchFilter.todate !== '' || $scope.searchFilter.fromdate !== '')
            {
                retVal = true;
            }
            return retVal;
        };
        $scope.show = function (index)
        {
            if ($scope.showDetails[index])
            {
                $scope.showDetails[index] = false;
            } else
                $scope.showDetails[index] = true;
        };

        $scope.getList = function () {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'itemwiseageingreport';
            getListParam.name = '';
            var config = adminService.handleOnlyErrorResponseConfig;
//            if ($scope.searchFilter.todate != '' && ($scope.searchFilter.fromdate == null || $scope.searchFilter.fromdate == ''))
//            {
//                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
//                {
//                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
//                }
//                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
//                getListParam.from_date = getListParam.to_date;
//            } else if ($scope.searchFilter.fromdate != '' && ($scope.searchFilter.todate == null || $scope.searchFilter.todate == ''))
//            {
//                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
//                {
//                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
//                }
//                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
//                getListParam.to_date = getListParam.from_date;
//            } else
//            {
//                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
//                {
//                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
//                }
//                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
//                {
//                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
//                }
//                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
//                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
//            }
            if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
            {
                getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
            }
            if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
            {
                getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
            }
            getListParam.has_inventory = $scope.searchFilter.inventory;
            if ($scope.searchFilter.fromdate != null && $scope.searchFilter.fromdate != '')
            {
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);

            }
            if ($scope.searchFilter.todate != null && $scope.searchFilter.todate != '')
            {
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
            }
            if ($scope.searchFilter.productInfo != '' && $scope.searchFilter.productInfo != null && $scope.searchFilter.productInfo != undefined)
            {
                getListParam.product_id = $scope.searchFilter.productInfo.id;
            }
            getListParam.is_active = 1;
            getListParam.start = 0;
            getListParam.limit = 0;
            
            if ($scope.searchFilter.categoryInfo != null && typeof $scope.searchFilter.categoryInfo != 'undefined' && typeof $scope.searchFilter.categoryInfo.id != 'undefined')
            {
                getListParam.category_id = $scope.searchFilter.categoryInfo.id;
            } else
            {
                getListParam.category_id = '';
            }
            
            $scope.itemwiseAgeingModel.isLoadingProgress = true;
            $scope.itemwiseAgeingModel.isSearchLoadingProgress = true;

            adminService.getItemwiseAgeingReport(getListParam, config, headers).then(function (response)
            {
                var totalPurchaseAmt = 0.00;
                var totalMrpAmt = 0.00;
                var detailPurchaseAmt = 0.00;
                var detailMrpAmt = 0.00;
                var totalQty = 0;
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.itemwiseAgeingModel.list = data;
                    for (var i = 0; i < $scope.itemwiseAgeingModel.list.length; i++)
                    {

                        totalQty = totalQty + parseFloat($scope.itemwiseAgeingModel.list[i].remaining_qty);
                        totalPurchaseAmt = totalPurchaseAmt + parseFloat($scope.itemwiseAgeingModel.list[i].purchase_value);
//                        $scope.itemwiseAgeingModel.list[i].purchase_value = utilityService.changeCurrency($scope.itemwiseAgeingModel.list[i].purchase_value, $rootScope.appConfig.thousand_seperator);
                        totalMrpAmt = totalMrpAmt + parseFloat($scope.itemwiseAgeingModel.list[i].mrp_price);
//                        $scope.itemwiseAgeingModel.list[i].mrp_price = utilityService.changeCurrency($scope.itemwiseAgeingModel.list[i].mrp_price, $rootScope.appConfig.thousand_seperator);
                        $scope.itemwiseAgeingModel.list[i].flag = i;
                        $scope.showDetails[i] = false;
                        for (var j = 0; j < $scope.itemwiseAgeingModel.list[i].detail.length; j++)
                        {
                            $scope.itemwiseAgeingModel.list[i].detail[j].newdate = utilityService.parseStrToDate($scope.itemwiseAgeingModel.list[i].detail[j].date);
                            $scope.itemwiseAgeingModel.list[i].detail[j].date = utilityService.parseDateToStr($scope.itemwiseAgeingModel.list[i].detail[j].newdate, $scope.adminService.appConfig.date_format);
                            $scope.itemwiseAgeingModel.list[i].detail[j].flag = i;

                            detailPurchaseAmt = detailPurchaseAmt + parseFloat($scope.itemwiseAgeingModel.list[i].detail[j].purchase_value);
                            $scope.itemwiseAgeingModel.list[i].detail[j].purchase_value = utilityService.changeCurrency($scope.itemwiseAgeingModel.list[i].detail[j].purchase_value, $rootScope.appConfig.thousand_seperator);
                            detailMrpAmt = detailMrpAmt + parseFloat($scope.itemwiseAgeingModel.list[i].detail[j].mrp_price);
                            $scope.itemwiseAgeingModel.list[i].detail[j].mrp_price = utilityService.changeCurrency($scope.itemwiseAgeingModel.list[i].detail[j].mrp_price, $rootScope.appConfig.thousand_seperator);
                        }

                    }


                    $scope.itemwiseAgeingModel.total = data.total;
                    $scope.itemwiseAgeingModel.totalQty = totalQty;
                    $scope.itemwiseAgeingModel.totalPurchaseAmount = totalPurchaseAmt;
                    $scope.itemwiseAgeingModel.totalPurchaseAmount = parseFloat($scope.itemwiseAgeingModel.totalPurchaseAmount).toFixed(2);
                    $scope.itemwiseAgeingModel.totalPurchaseAmount = utilityService.changeCurrency($scope.itemwiseAgeingModel.totalPurchaseAmount, $rootScope.appConfig.thousand_seperator);

                    $scope.itemwiseAgeingModel.totalMrpAmount = totalMrpAmt;
                    $scope.itemwiseAgeingModel.totalMrpAmount = parseFloat($scope.itemwiseAgeingModel.totalMrpAmount).toFixed(2);
                    $scope.itemwiseAgeingModel.totalMrpAmount = utilityService.changeCurrency($scope.itemwiseAgeingModel.totalMrpAmount, $rootScope.appConfig.thousand_seperator);
                }
                $scope.itemwiseAgeingModel.isLoadingProgress = false;
                $scope.itemwiseAgeingModel.isSearchLoadingProgress = false;
            });

        };
        $scope.getProductList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.is_active=1;
            return $httpService.get(APP_CONST.API.PRODUCT_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatProductModel = function (model)
        {
            if (model != null)
            {
                return model.name;
            }
            return  '';
        };

        $scope.print = function ()
        {
            $window.print();
        };
        
        $scope.getCategoryList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.GET_CATEGORY_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatCategoryModel = function (model)
        {
            if (model != null)
            {
                if (model.name != undefined)
                {
                    return model.name;
                }
            }
            return  '';
        };

//        $scope.getList();
    }]);




