
app.controller('newCustomerAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$timeout', '$filter', 'Auth', 'ValidationFactory', '$state', 'APP_CONST', 'customerFactory', function ($scope, $rootScope, adminService, $httpService, utilityService, $timeout, $filter, Auth, ValidationFactory, $state, APP_CONST, customerFactory) {

        $scope.customerAddModel = {
            "id": 0,
            "name": "",
            "companyName": "",
            "email": "",
            "mobile": "",
            "city": "",
            "state": "",
            "country": "",
            "postcode": "",
            "address": "",
            "tags": "",
            "currency": "",
            "group": "",
            "password": "",
            "confirmpassword": "",
            "iswelcomeEmail": true,
            "isactive": 1,
            "isPurchase": true,
            "isSale": true,
            "userList": [],
            "billingaddr": '',
            "billingcity": '',
            "billingstate": '',
            "billingpostcode": '',
            "billingcountry": '',
            "shoppingaddr": '',
            "shoppingcity": '',
            "shoppingstate": '',
            "shoppingpostcode": '',
            "shoppingcountry": '',
            "isSameAddr": '',
            "country_name": '',
            "country_id": '',
            "state_name": '',
            "state_id": '',
            "city_id": '',
            "country_list": [],
            "state_list": [],
            "city_list": []
        }

        $rootScope.$on('customerInfo', function (event, customerDetail) {
            $scope.customerAddModel.mobile = '';
            $scope.customerAddModel.email = '';
            $scope.customerAddModel.name = '';
            if (typeof customerDetail === 'undefined')
                return;
            if (customerDetail.id != null || customerDetail.id != undefined)
            {
                return;
            } else
            {
                if (!isNaN(customerDetail))
                {
                    $scope.customerAddModel.mobile = customerDetail;
                } else if (customerDetail == object)
                {
                    $scope.customerAddModel.name = '';
                } else if ($scope.validationFactory.emailValidator(customerDetail))
                {
                    $scope.customerAddModel.email = customerDetail;
                } else
                {
                    $scope.customerAddModel.name = customerDetail;
                    console.log('name');
                    console.log($scope.customerAddModel.name);
                }
            }
        });
        $scope.customAttributeList = [];
        $scope.validationFactory = ValidationFactory;
        $scope.getNavigationBlockMsg = function (pageReload)
        {
            if (typeof $scope.create_customer_form != 'undefined' && typeof $scope.create_customer_form.$pristine != 'undefined' && !$scope.create_customer_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.customerFactory = customerFactory;
        //  $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.formReset = function () {
            if (typeof $scope.create_customer_form != 'undefined')
            {
                $scope.create_customer_form.$setPristine();
                $scope.customerAddModel = {
                    "id": 0,
                    "name": "",
                    "companyName": "",
                    "email": "",
                    "phone": "",
                    "city": "",
                    "state": "",
                    "country": "",
                    "postcode": "",
                    "address": "",
                    "tags": "",
                    "currency": "",
                    "group": "",
                    "password": "",
                    "confirmpassword": "",
                    "iswelcomeEmail": true,
                    "isactive": 1,
                    "isPurchase": true,
                    "isSale": true,
                    "userList": [],
                    "billingaddr": '',
                    "billingcity": '',
                    "billingstate": '',
                    "billingpostcode": '',
                    "billingcountry": '',
                    "shoppingaddr": '',
                    "shoppingcity": '',
                    "shoppingstate": '',
                    "shoppingpostcode": '',
                    "shoppingcountry": '',
                    "isSameAddr": '',
                    "country_name": '',
                    "country_id": '',
                    "state_name": '',
                    "state_id": '',
                    "city_id": '',
                    "country_list": [],
                    "state_list": [],
                    "city_list": []
                }
            }
            $scope.getcountryList();
        }

        $scope.updateShoppingAddr = function ()
        {
            if ($scope.customerAddModel.isSameAddr == true)
            {
                $scope.customerAddModel.shoppingaddr = $scope.customerAddModel.billingaddr;
                $scope.customerAddModel.shoppingcity = $scope.customerAddModel.billingcity;
                $scope.customerAddModel.shoppingstate = $scope.customerAddModel.billingstate;
                $scope.customerAddModel.shoppingpostcode = $scope.customerAddModel.billingpostcode;
                $scope.customerAddModel.shoppingcountry = $scope.customerAddModel.billingcountry;
            } else if ($scope.customerAddModel.isSameAddr == false)
            {
                $scope.customerAddModel.shoppingaddr = '';
                $scope.customerAddModel.shoppingcity = '';
                $scope.customerAddModel.shoppingstate = '';
                $scope.customerAddModel.shoppingpostcode = '';
                $scope.customerAddModel.shoppingcountry = '';
            }

        }

        $scope.$on('CUSTOMER_SELECT_EVENT', function (e, data) {
            console.log('CUSTOMER_SELECT_EVENT');
            var savedCustomer = data;
            $rootScope.$broadcast('updateSavedCustomerDetail', savedCustomer);
        });
        $scope.createcustomer = function () {
            $scope.isDataSavingProcess = true;
            var headers = {};
            headers['screen-code'] = 'customer';
            var createcustomerParam = {};
            createcustomerParam.id = 0;
            createcustomerParam.fname = $scope.customerAddModel.name;
            createcustomerParam.company = $scope.customerAddModel.companyName;
            createcustomerParam.email = $scope.customerAddModel.email;
            createcustomerParam.phone = $scope.customerAddModel.mobile;
            createcustomerParam.address = $scope.customerAddModel.address;
            createcustomerParam.city_id = $scope.customerAddModel.city_id;
            createcustomerParam.state_id = $scope.customerAddModel.state_id;
            createcustomerParam.country_id = $scope.customerAddModel.country_id;
            if ($rootScope.currentPage == 'pos' || $rootScope.currentPage == 'invoiceAdd' || $rootScope.currentPage == 'estimateAdd' || $rootScope.currentPage == 'orderAdd')
            {
                createcustomerParam.customer_type = 'retailer';
            } else if ($rootScope.currentPage == 'purchase-invoiceAdd' || $rootScope.currentPage == 'purchase-orderAdd')
            {
                createcustomerParam.customer_type = 'suplier';
            } else {
                createcustomerParam.customer_type = 'wholesaler';
            }
            for (var i = 0; i < $scope.customerAddModel.country_list.length; i++)
            {
                if ($scope.customerAddModel.country_id == $scope.customerAddModel.country_list[i].id)
                {
                    createcustomerParam.country_name = $scope.customerAddModel.country_list[i].name;
                }
            }
            for (var i = 0; i < $scope.customerAddModel.state_list.length; i++)
            {
                if ($scope.customerAddModel.state_id == $scope.customerAddModel.state_list[i].id)
                {
                    createcustomerParam.state_name = $scope.customerAddModel.state_list[i].name;
                }
            }
            for (var i = 0; i < $scope.customerAddModel.city_list.length; i++)
            {
                if ($scope.customerAddModel.city_id == $scope.customerAddModel.city_list[i].id)
                {
                    createcustomerParam.city_name = $scope.customerAddModel.city_list[i].name;
                }
            }
            createcustomerParam.zip = $scope.customerAddModel.postcode;
            createcustomerParam.tags = $scope.customerAddModel.tags;
            createcustomerParam.currency = $scope.customerAddModel.currency;
            createcustomerParam.group = $scope.customerAddModel.group;
            createcustomerParam.password = $scope.customerAddModel.password;
            createcustomerParam.confirmPassword = $scope.customerAddModel.confirmpassword;
            createcustomerParam.iswelcomeEmail = ($scope.customerAddModel.iswelcomeEmail == true ? 1 : 0);
            createcustomerParam.is_purchase = 0;
            createcustomerParam.is_sale = 0;
            if ($rootScope.currentPage == 'invoiceAdd' || $rootScope.currentPage == 'estimateAdd' || $rootScope.currentPage == 'orderAdd')
            {
                createcustomerParam.is_sale = 1;
            } else if ($rootScope.currentPage == 'purchase-invoiceAdd' || $rootScope.currentPage == 'purchase-orderAdd')
            {
                createcustomerParam.is_purchase = 1;
            } else if ($rootScope.currentPage == 'pos')
            {
                createcustomerParam.is_sale = 1;
            }
            createcustomerParam.billing_address = $scope.customerAddModel.billingaddr;
            createcustomerParam.billing_city_id = $scope.customerAddModel.city_id;
            createcustomerParam.billing_state_id = $scope.customerAddModel.state_id;
            createcustomerParam.billing_country_id = $scope.customerAddModel.country_id;
            for (var i = 0; i < $scope.customerAddModel.country_list.length; i++)
            {
                if ($scope.customerAddModel.country_id.toLowerCase() == $scope.customerAddModel.country_list[i].name.toLowerCase())
                {
                    createcustomerParam.billing_country = $scope.customerAddModel.country_list[i].name;
                    createcustomerParam.billing_country_id = $scope.customerAddModel.country_list[i].id;
                }
            }
            for (var i = 0; i < $scope.customerAddModel.state_list.length; i++)
            {
                if ($scope.customerAddModel.state_id.toLowerCase() == $scope.customerAddModel.state_list[i].name.toLowerCase())
                {
                    createcustomerParam.billing_state_id = $scope.customerAddModel.state_list[i].id;
                    createcustomerParam.billing_state = $scope.customerAddModel.state_list[i].name;
                }
            }
            for (var i = 0; i < $scope.customerAddModel.city_list.length; i++)
            {
                if ($scope.customerAddModel.city_id.toLowerCase() == $scope.customerAddModel.city_list[i].name.toLowerCase())
                {
                    createcustomerParam.billing_city = $scope.customerAddModel.city_list[i].name;
                    createcustomerParam.billing_city_id = $scope.customerAddModel.city_list[i].id;
                }
            }
            createcustomerParam.billing_pincode = $scope.customerAddModel.billingpostcode;
            createcustomerParam.shopping_address = $scope.customerAddModel.billingaddr;
            createcustomerParam.shopping_city_id = $scope.customerAddModel.city_id;
            createcustomerParam.shopping_country_id = $scope.customerAddModel.country_id;
            for (var i = 0; i < $scope.customerAddModel.country_list.length; i++)
            {
                if ($scope.customerAddModel.country_id.toLowerCase() == $scope.customerAddModel.country_list[i].name.toLowerCase())
                {
                    createcustomerParam.shopping_country = $scope.customerAddModel.country_list[i].name;
                    createcustomerParam.shopping_country_id = $scope.customerAddModel.country_list[i].id;
                }
            }
            for (var i = 0; i < $scope.customerAddModel.state_list.length; i++)
            {
                if ($scope.customerAddModel.state_id.toLowerCase() == $scope.customerAddModel.state_list[i].name.toLowerCase())
                {
                    createcustomerParam.shopping_state_id = $scope.customerAddModel.state_list[i].id;
                    createcustomerParam.shopping_state = $scope.customerAddModel.state_list[i].name;
                }
            }
            for (var i = 0; i < $scope.customerAddModel.city_list.length; i++)
            {
                if ($scope.customerAddModel.city_id.toLowerCase() == $scope.customerAddModel.city_list[i].name.toLowerCase())
                {
                    createcustomerParam.shopping_city = $scope.customerAddModel.city_list[i].name;
                    createcustomerParam.shopping_city_id = $scope.customerAddModel.city_list[i].id;
                }
            }
            createcustomerParam.shopping_pincode = $scope.customerAddModel.billingpostcode;
            createcustomerParam.is_active = 1;
            var configOption = adminService.handleBothSuccessAndErrorResponseConfig;
            adminService.addCustomer(createcustomerParam, configOption, headers).then(function (response) {

                if (response.data.success == true)
                {
                    $scope.customerInfo = response.data.data;
                    $scope.formReset();
                    $scope.$broadcast('CUSTOMER_SELECT_EVENT', $scope.customerInfo);
                    $scope.closeCustomerPopup();
                }
                $scope.isDataSavingProcess = false;
                var element = document.getElementById("btnLoad1");
                element.classList.remove("btn-loader");

            }).catch(function (response) {
                console.error('Error occurred:', response.status, response.data);
                $scope.isDataSavingProcess = false;
                var element = document.getElementById("btnLoad1");
                element.classList.remove("btn-loader");
            });
        };

        $scope.getCustomAttributeList = function () {

            var getListParam = {};
            getListParam.id = "";
            getListParam.attributetype_id = "";
            getListParam.attribute_code = "";
            getListParam.attributetypecode = "crm";
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getAttributeList(getListParam, configOption).then(function (response)
            {
                var data = response.data.list;
                $scope.customAttributeList = data;

            });
        };

        $scope.$on('customSelectOptionChangeEvent', function (event, data)
        {
            console.log(data);
            $scope.updateCustomSelectValue(data);
        });

        $scope.updateCustomSelectValue = function (data)
        {
            for (var i = 0; i < $scope.customAttributeList.length; i++)
            {
                if ($scope.customAttributeList[i].attribute_code == data.code)
                {
                    $scope.customAttributeList[i].value = data.value;
                    break;
                }
            }
        }

        $scope.getCustomAttributeList();

        /*
         * Listening the Custom field value change
         */
        $scope.$on('customfieldValueChangeEvent', function (event, data)
        {
            console.log(data);
            $scope.updateCustomFieldValue(data);
        });

        $scope.updateCustomFieldValue = function (data)
        {
            for (var i = 0; i < $scope.customAttributeList.length; i++)
            {
                if ($scope.customAttributeList[i].attribute_code == data.code)
                {
                    $scope.customAttributeList[i].value = data.value;
                    break;
                }
            }
        }
        $scope.redirectToCustomer = function ()
        {
            var customerInfo = {};
            customerInfo.name = $scope.customerAddModel.name;
            customerInfo.companyName = $scope.customerAddModel.companyName;
            customerInfo.email = $scope.customerAddModel.email;
            customerInfo.mobile = $scope.customerAddModel.mobile;
            customerInfo.billingaddr = $scope.customerAddModel.billingaddr;
            customerInfo.country = $scope.customerAddModel.country_id;
            customerInfo.state = $scope.customerAddModel.state_id;
            customerInfo.city = $scope.customerAddModel.city_id;

            for (var i = 0; i < $scope.customerAddModel.country_list.length; i++)
            {
                if (customerInfo.country == $scope.customerAddModel.country_list[i].id)
                {
                    customerInfo.country_name = $scope.customerAddModel.country_list[i].name;
                }
            }

            for (var i = 0; i < $scope.customerAddModel.state_list.length; i++)
            {
                if (customerInfo.state == $scope.customerAddModel.state_list[i].id)
                {
                    customerInfo.state_name = $scope.customerAddModel.state_list[i].name;
                }
            }
            for (var i = 0; i < $scope.customerAddModel.city_list.length; i++)
            {
                if (customerInfo.city == $scope.customerAddModel.city_list[i].id)
                {
                    customerInfo.city_name = $scope.customerAddModel.city_list[i].name;
                }
            }
            customerInfo.billingpostcode = $scope.customerAddModel.billingpostcode;
            customerInfo.pageName = $rootScope.currentPage;
            customerFactory.set(customerInfo);
            if ($state.current.pageName == "pos" || $state.current.pageName == "estimateAdd" || $state.current.pageName == "orderAdd" || $state.current.pageName == "invoiceAdd") {
                $state.go('app.customeradd');
            } else if ($state.current.pageName == "purchase-orderAdd" || $state.current.pageName == "purchase-invoiceAdd") {
                $state.go('app.vendoradd');
            }

        };

        $scope.getcountryList = function () {

            $scope.customerAddModel.isLoadingProgress = true;
            var countryListParam = {};
            var headers = {};
            headers['screen-code'] = 'country';

            countryListParam.id = '';

            countryListParam.is_active = 1;

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getCountryList(countryListParam, configOption, headers).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.customerAddModel.country_list = data.list;
                    for (var i = 0; i < $scope.customerAddModel.country_list.length; i++)
                    {
                        if ($scope.customerAddModel.country_list[i].is_default == 1)
                        {
                            $scope.customerAddModel.country_id = $scope.customerAddModel.country_list[i].name;
                        }
                    }
                }
                $scope.customerAddModel.isLoadingProgress = false;
            });

        };
        $scope.initBillingAddressStateListTimeoutPromise = null;

        $scope.initBillingAddressStateList = function (searchkey)
        {
            if ($scope.initBillingAddressStateListTimeoutPromise != null)
            {
                $timeout.cancel($scope.initBillingAddressStateListTimeoutPromise);
            }
            $scope.initBillingAddressStateListTimeoutPromise = $timeout(function () {
                $scope.getstateList(searchkey);
            }, 300);

        };
        $scope.initBillingAddressCityListTimeoutPromise = null;

        $scope.initBillingAddressCityList = function (searchkey)
        {
            if ($scope.initBillingAddressCityListTimeoutPromise != null)
            {
                $timeout.cancel($scope.initBillingAddressCityListTimeoutPromise);
            }
            $scope.initBillingAddressCityListTimeoutPromise = $timeout(function () {
                $scope.getcityList(searchkey);
            }, 300);

        };

        $rootScope.customerState = false;
        $scope.getstateList = function (searchkey) {

            $scope.customerAddModel.isLoadingProgress = true;
            var stateListParam = {};
            var headers = {};
            headers['screen-code'] = 'state';
            stateListParam.name = searchkey;
            stateListParam.id = '';
            stateListParam.is_active = 1;
//            stateListParam.country_id = $scope.customerAddModel.country_id;
            stateListParam.country_name = $scope.customerAddModel.country_id;

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            configOption.searchkey = searchkey;
            adminService.getStateList(stateListParam, configOption, headers).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;

                    $scope.customerAddModel.state_list = data.list;
                    if ($rootScope.customerState == false)
                    {
                        for (var i = 0; i < $scope.customerAddModel.state_list.length; i++)
                        {
                            if ($scope.customerAddModel.state_list[i].is_default == 1)
                            {
                                $scope.customerAddModel.state_id = $scope.customerAddModel.state_list[i].name;
                                $rootScope.customerState = true;
                            }
                        }
                        if ($rootScope.customerState == false)
                        {
                            $scope.customerAddModel.state_id = $rootScope.appConfig.gst_state_name;
                        }
                    }
                }
                $scope.customerAddModel.isLoadingProgress = false;
            });

        };

        $scope.getcityList = function (searchkey) {

            $scope.customerAddModel.isLoadingProgress = true;
            var cityListParam = {};
            var headers = {};
            headers['screen-code'] = 'city';

            cityListParam.id = '';
            cityListParam.name = searchkey;
            cityListParam.is_active = 1;
//            cityListParam.country_id = $scope.customerAddModel.country_id;
            cityListParam.country_name = $scope.customerAddModel.country_id;
            cityListParam.state_name = $scope.customerAddModel.state_id;
//            cityListParam.state_id = $scope.customerAddModel.state_id;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            configOption.searchkey = searchkey;
            adminService.getCityList(cityListParam, configOption, headers).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.customerAddModel.city_list = data.list;
                }
                $scope.customerAddModel.isLoadingProgress = false;
            });

        };

        $scope.CountryChange = function ()
        {
            $scope.customerAddModel.state_id = '';
            $scope.customerAddModel.city_id = '';
            $scope.customerAddModel.state_list = [];
            $scope.customerAddModel.city_list = [];

        }
        $scope.StateChange = function ()
        {
            $scope.customerAddModel.city_id = '';
            $scope.customerAddModel.city_list = [];
        }
        $scope.getcountryList();
    }]);




