app.controller('partyPurchasesStatementCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', function ($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.partyPurchaseStatementModel = {
            currentPage: 1,
            total: 0,
            total_Amount: 0,
            limit: 10,
            list: [],
            length: [],
            date: '',
            invoice_id: '',
            customer_id: '',
            amount: '',
            status: '',
            serverList: null,
            isLoadingProgress: false,
            totaldebit: 0,
            totalcredit: 0,
            totalamt: 0,
            totalbalance: 0,
            openingBalance: null,
            isSearchLoadingProgress: false,
            openingCredit: '',
            openingDebit: '',
            openingAmount: ''
        };
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.partyPurchaseStatementModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
        $scope.validationFactory = ValidationFactory;
        $scope.openDate = function (index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            } else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };

        $scope.searchFilter = {
            fromdate: '',
            todate: '',
            customerInfo: {}

        };
        $scope.searchFilter.fromdate = '';
        $scope.clearFilters = function ()
        {
            $scope.searchFilter = {
                fromdate: '',
                todate: '',
                customerInfo: {}
            };
            $scope.partyPurchaseStatementModel.list = [];
            $scope.partyPurchaseStatementModel.openingBalance = null;
            $scope.getList();
        }

        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.validateDateFilterData = function ()
        {
            var retVal = false;
            if ($scope.searchFilter.todate !== '' || $scope.searchFilter.fromdate !== '')
            {
                retVal = true;
            }
            return retVal;
        };

        $scope.calculateTotal = function ()
        {
            var totDebit = 0;
            var totCredit = 0;
            var totamt = 0;
            for (var i = 0; i < $scope.partyPurchaseStatementModel.list.length; i++)
            {
                totDebit += parseFloat($scope.partyPurchaseStatementModel.list[i].debit);
                $scope.partyPurchaseStatementModel.totaldebit = totDebit;
                $scope.partyPurchaseStatementModel.totaldebit = parseFloat($scope.partyPurchaseStatementModel.totaldebit).toFixed(2);
                $scope.partyPurchaseStatementModel.totaldebit = utilityService.changeCurrency($scope.partyPurchaseStatementModel.totaldebit, $rootScope.appConfig.thousand_seperator);


                totCredit += parseFloat($scope.partyPurchaseStatementModel.list[i].credit);
                $scope.partyPurchaseStatementModel.totalcredit = totCredit;
                $scope.partyPurchaseStatementModel.totalcredit = parseFloat($scope.partyPurchaseStatementModel.totalcredit).toFixed(2);
                $scope.partyPurchaseStatementModel.totalcredit = utilityService.changeCurrency($scope.partyPurchaseStatementModel.totalcredit, $rootScope.appConfig.thousand_seperator);

                totamt += parseFloat($scope.partyPurchaseStatementModel.list[i].amount);
                $scope.partyPurchaseStatementModel.totalamt = totamt;
            }
        }

        $scope.updateBalanceAmount = function ()
        {
            if ($scope.partyPurchaseStatementModel.list.length >= 1)
            {
                var openingBalance = parseFloat($scope.partyPurchaseStatementModel.openingBalance.credit) - parseFloat($scope.partyPurchaseStatementModel.openingBalance.debit);
                for (var i = 0; i < $scope.partyPurchaseStatementModel.list.length; i++)
                {
                    var credit = $scope.partyPurchaseStatementModel.list[i].credit == null ? 0 : parseFloat($scope.partyPurchaseStatementModel.list[i].credit);
                    var debit = $scope.partyPurchaseStatementModel.list[i].debit == null ? 0 : parseFloat($scope.partyPurchaseStatementModel.list[i].debit);
                    if (credit != 0)
                    {
                        openingBalance = openingBalance + credit;
                    } else if (debit != 0)
                    {
                        openingBalance = openingBalance - debit;
                    }
                    $scope.partyPurchaseStatementModel.list[i].balance = openingBalance;
                    $scope.partyPurchaseStatementModel.totalbalance = parseFloat($scope.partyPurchaseStatementModel.totalbalance).toFixed(2);
                    $scope.partyPurchaseStatementModel.totalbalance = utilityService.changeCurrency($scope.partyPurchaseStatementModel.totalbalance, $rootScope.appConfig.thousand_seperator);

                }

                $scope.partyPurchaseStatementModel.totalbalance = openingBalance;

            } else
            {
                $scope.partyPurchaseStatementModel.totalbalance = 0.00;
            }
        }

        $scope.getList = function () {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'partypurchasereport';
            getListParam.to_date = $scope.searchFilter.todate;
            getListParam.from_date = $scope.searchFilter.fromdate;
            if ($scope.searchFilter.customerInfo != null && typeof $scope.searchFilter.customerInfo != 'undefined' && typeof $scope.searchFilter.customerInfo.id != 'undefined')
            {
                getListParam.id = $scope.searchFilter.customerInfo.id;
            } else
            {
                getListParam.id = '';
            }

            if ($scope.searchFilter.todate != '' && ($scope.searchFilter.fromdate == null || $scope.searchFilter.fromdate == ''))
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
                getListParam.from_date = getListParam.to_date;

            } else if ($scope.searchFilter.fromdate != '' && ($scope.searchFilter.todate == null || $scope.searchFilter.todate == ''))
            {
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
                getListParam.to_date = getListParam.from_date;

            } else if ($scope.searchFilter.todate != null && $scope.searchFilter.todate != '' && $scope.searchFilter.fromdate != null && $scope.searchFilter.fromdate != '')
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
            }
            getListParam.start = 0;
            getListParam.limit = 0;
            $scope.partyPurchaseStatementModel.isLoadingProgress = true;
            $scope.partyPurchaseStatementModel.isSearchLoadingProgress = true;
            if ($scope.searchFilter.todate != '' && $scope.searchFilter.todate != null && $scope.searchFilter.todate != 'undefined' && $scope.searchFilter.fromdate != '' && $scope.searchFilter.fromdate != null && getListParam.id != '')
            {
                adminService.getPartyPurchaseList(getListParam, headers).then(function (response)
                {
                    if (response.data.success === true)
                    {
                        var data = response.data.list;
                        $scope.partyPurchaseStatementModel.list = data;
                        $scope.partyPurchaseStatementModel.openingBalance = response.data.openBalance;
                        $scope.partyPurchaseStatementModel.openingBalance.credit = parseFloat($scope.partyPurchaseStatementModel.openingBalance.credit).toFixed(2);
                        $scope.partyPurchaseStatementModel.openingBalance.debit = parseFloat($scope.partyPurchaseStatementModel.openingBalance.debit).toFixed(2);
                        var openingBalance = parseFloat($scope.partyPurchaseStatementModel.openingBalance.credit) - parseFloat($scope.partyPurchaseStatementModel.openingBalance.debit);
                        $scope.partyPurchaseStatementModel.openingBalance.balance = openingBalance;
                        $scope.partyPurchaseStatementModel.openingBalance.balance = parseFloat($scope.partyPurchaseStatementModel.openingBalance.balance).toFixed(2);
                        $scope.partyPurchaseStatementModel.openingAmount = utilityService.changeCurrency($scope.partyPurchaseStatementModel.openingBalance.balance, $rootScope.appConfig.thousand_seperator);
                        $scope.partyPurchaseStatementModel.openingBalance.balance = parseFloat($scope.partyPurchaseStatementModel.openingBalance.balance).toFixed(2);
                        $scope.partyPurchaseStatementModel.openingCredit = utilityService.changeCurrency($scope.partyPurchaseStatementModel.openingBalance.credit, $rootScope.appConfig.thousand_seperator);
                        $scope.partyPurchaseStatementModel.openingDebit = utilityService.changeCurrency($scope.partyPurchaseStatementModel.openingBalance.debit, $rootScope.appConfig.thousand_seperator);

                        $scope.partyPurchaseStatementModel.openingBalance.balance = utilityService.changeCurrency($scope.partyPurchaseStatementModel.openingBalance.balance, $rootScope.appConfig.thousand_seperator);

                        if ($scope.partyPurchaseStatementModel.list.length != 0)
                        {
                            $scope.updateBalanceAmount();
                            $scope.calculateTotal();
                            for (var i = 0; i < $scope.partyPurchaseStatementModel.list.length; i++)
                            {
                                $scope.partyPurchaseStatementModel.list[i].newdate = utilityService.parseStrToDate($scope.partyPurchaseStatementModel.list[i].date);
                                $scope.partyPurchaseStatementModel.list[i].date = utilityService.parseDateToStr($scope.partyPurchaseStatementModel.list[i].newdate, $scope.adminService.appConfig.date_format);
                                //$scope.partyPurchaseStatementModel.list[i].balance = utilityService.changeCurrency($scope.partyPurchaseStatementModel.list[i].balance, $rootScope.appConfig.thousand_seperator);
                                $scope.partyPurchaseStatementModel.list[i].balance = parseFloat($scope.partyPurchaseStatementModel.list[i].balance).toFixed(2);
                                $scope.partyPurchaseStatementModel.list[i].balance = parseFloat($scope.partyPurchaseStatementModel.list[i].balance).toFixed(2);
                                $scope.partyPurchaseStatementModel.list[i].debit = utilityService.changeCurrency($scope.partyPurchaseStatementModel.list[i].debit, $rootScope.appConfig.thousand_seperator);
                                $scope.partyPurchaseStatementModel.list[i].credit = utilityService.changeCurrency($scope.partyPurchaseStatementModel.list[i].credit, $rootScope.appConfig.thousand_seperator);

                            }
                        } else
                        {
                            $scope.partyPurchaseStatementModel.totaldebit = 0.00;
                            $scope.partyPurchaseStatementModel.totalcredit = 0.00;
                            $scope.partyPurchaseStatementModel.totalbalance = 0.00;
                        }
                        $scope.partyPurchaseStatementModel.total = data.total;
                    }
                    $scope.partyPurchaseStatementModel.isLoadingProgress = false;
                    $scope.partyPurchaseStatementModel.isSearchLoadingProgress = false;
                });
            } else {
                $scope.partyPurchaseStatementModel.isLoadingProgress = false;
                $scope.partyPurchaseStatementModel.isSearchLoadingProgress = false;
            }

        };
        $scope.getCustomerList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            autosearchParam.mode = 2;
            return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatCustomerModel = function (model)
        {
            if (model != null)
            {
                if (model.fname != undefined && model.phone != undefined && model.email != undefined)
                {
                    return model.fname + '(' + model.phone + ',' + model.email + ')';
                } else if (model.fname != undefined && model.phone != undefined)
                {
                    return model.fname + '(' + model.phone + ')';
                }
            }
            return  '';
        };
        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'partyPurchase_form';


        $scope.$on('onLocalStorageFormRenterCompleteEvent', function (event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });

        $scope.$on('onLocalStorageFieldRenterCompleteEvent', function (event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRenterCount++;
                }
                if ($scope.localStorageRenterCount >= $scope.localStorageCount)
                {
                    console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                    $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
                }

            }
        });
        $scope.$on('updateTypeaheadFieldValue', function (event, data) {

            if (data.fieldName == "partyPurchase_form-customer_dropdown")
            {
                $scope.searchFilter.customerInfo = data.value;
            }
        });



        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function (event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRetrieveCount++;
                }
                if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
                {
                    $scope.initTableFilter();
                }
                console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);
            }
        });

        $scope.init = function ()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');

        };

        $scope.$on('onLocalStorageReadyEvent', function () {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                $timeout(function () {
                    $scope.init();
                }, 300);

            }
        });













//        $scope.formatcustomerModel = function(model) {
//
//            if (model !== null)
//            {
//                $scope.searchFilter.id = model.id;
//                return model.fname;
//            }
//            return  '';
//        };

        $scope.print = function (div)
        {
            var docHead = document.head.outerHTML;
            var printContents = document.getElementById(div).outerHTML;
            var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=865, height=600, resizable=yes, screenX=200, screenY=200, personalbar=no, scrollbars=yes";

            var newWin = window.open("", "_blank", winAttr);
            var writeDoc = newWin.document;
            writeDoc.open();
            writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
            writeDoc.close();
            newWin.focus();
        };
        //$scope.getList();
    }]);




