app.controller('partysalespurchasebasedCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', function($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.partysalespurchasebasedModel = {
            currentPage: 1,
            total: 0,
            total_Amount: 0,
            limit: 10,
            list: [],
            length: [],
            date: '',
            invoice_id: '',
            customer_id: '',
            product_id: '',
            amount: '',
            status: '',
            serverList: null,
            isLoadingProgress: false,
            isSearchLoadingProgress: false,
        };
        
        $scope.pagePerCount = [50, 100];
        $scope.partysalespurchasebasedModel.limit = $scope.pagePerCount[0];
        $scope.validationFactory = ValidationFactory;
        $scope.adminService = adminService;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat);


        $scope.searchFilter = {
            customer_id: '',
            product_id: '',
            customerInfo: {},
            productInfo: {}

        };
        $scope.clearFilters = function()
        {
            $scope.searchFilter = {
                customer_id: '',
                product_id: '',
                customerInfo: {},
                productInfo: {}
            };
            $scope.partysalespurchasebasedModel.list = [];

        }
        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        
        $scope.show = function(index)
        {
            if ($scope.showDetails[index])
            {
                $scope.showDetails[index] = false;
            }
            else
                $scope.showDetails[index] = true;
        };

        $scope.getList = function() {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'partysalespurbased';
            if ($scope.searchFilter.customerInfo != null && typeof $scope.searchFilter.customerInfo != 'undefined' && typeof $scope.searchFilter.customerInfo.id != 'undefined')
            {
                getListParam.customer_id = $scope.searchFilter.customerInfo.id;
            }
            else
            {
                getListParam.customer_id = '';
            }
               if($scope.showDetail)
            {
                getListParam.show_all = 1;
            }
            else
            {
                getListParam.show_all = '';
            }
            getListParam.from_date = $scope.searchFilter.fromdate;
            getListParam.to_date = $scope.searchFilter.todate;

            if ($scope.searchFilter.productInfo != null && typeof $scope.searchFilter.productInfo != 'undefined' && typeof $scope.searchFilter.productInfo.id != 'undefined')
            {

                getListParam.product_id = $scope.searchFilter.productInfo.id;
            }
            else
            {

                getListParam.product_id = '';
            }
            //getListParam.show_all = 1;
            $scope.partysalespurchasebasedModel.isLoadingProgress = true;
            $scope.partysalespurchasebasedModel.isSearchLoadingProgress = true;
            $scope.showDetails = [];
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getPartywiseSalespurchase(getListParam, configOption, headers).then(function(response)
            {
                if (response.data.list.length > 0)
                {
                    var data = response.data.list;
                    $scope.partysalespurchasebasedModel.list = data;
                    for (var i = 0; i < $scope.partysalespurchasebasedModel.list[i].length; i++)
                    {
                        $scope.partysalespurchasebasedModel.list[i].flag = i;
                        $scope.showDetails[i] = false;
                        for (var j = 0; j < $scope.partysalespurchasebasedModel.list[i].product_detail.length; j++)
                            {
                                $scope.partysalespurchasebasedModel.list[i].product_detail[j].flag = i;
                            }
                    }
                    $scope.partysalespurchasebasedModel.total = data.total;
                }
                else
                {
                    $scope.partysalespurchasebasedModel.list = [];
                }
                $scope.partysalespurchasebasedModel.isLoadingProgress = false;
                $scope.partysalespurchasebasedModel.isSearchLoadingProgress = false;
            });            

        };
        $scope.getCustomerList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            autosearchParam.mode = 2;
            return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatCustomerModel = function(model)
        {
            if (model != null)
            {
                if (model.fname != undefined && model.phone != undefined && model.email != undefined)
                {
                    return model.fname + '(' + model.phone + ',' + model.email + ')';
                }
                else if (model.fname != undefined && model.phone != undefined)
                {
                    return model.fname + '(' + model.phone + ')';
                }
            }
            return  '';
        };
           $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'partysales_form';


        $scope.$on('onLocalStorageFormRenterCompleteEvent', function(event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });  
        
          $scope.$on('onLocalStorageFieldRenterCompleteEvent', function(event, formName) {
             if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRenterCount++;
                }
                if ($scope.localStorageRenterCount >= $scope.localStorageCount)
                {
                    console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                    $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
                }
            
              }
        });
        $scope.$on('updateTypeaheadFieldValue', function(event, data) {

            if (data.fieldName == "partysales_form-customer_dropdown")
            {
                $scope.searchFilter.customerInfo = data.value;
            }
        });
         $scope.$on('updateTypeaheadFieldValue', function(event, data) {

            if (data.fieldName == "partysales_form-product_dropdown")
            {
                $scope.searchFilter.productInfo = data.value;
            }
        });        

        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function(event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRetrieveCount++;
                }
                if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
                {
                    $scope.initTableFilter();
                }
                console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);
            }
        });

        $scope.init = function()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');

        };

        $scope.$on('onLocalStorageReadyEvent', function() {
           if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                $timeout(function() {
                    $scope.init();
                }, 300);
            
            }
        });            
        
        

        $scope.getProductList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.is_active=1;
            return $httpService.get(APP_CONST.API.PRODUCT_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatProductModel = function(model)
        {
            if (model != null)
                {
                if (model.name != undefined && model.sku != undefined)
                {
                    return model.name + '(' + model.sku + ')';
                }
                else if (model.name != undefined && model.sku != undefined)
                {
                    return model.name + '(' + model.sku + ')';
                }
            }
            return  '';
        };
//        $scope.formatcustomerModel = function(model) {
//
//            if (model !== null)
//            {
//                $scope.searchFilter.id = model.id;
//                return model.fname;
//            }
//            return  '';
//        };

        $scope.print = function(div)
        {
           var docHead = document.head.outerHTML;
            var printContents = document.getElementById(div).outerHTML;
            var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=865, height=600, resizable=yes, screenX=200, screenY=200, personalbar=no, scrollbars=yes";

            var newWin = window.open("", "_blank", winAttr);
            var writeDoc = newWin.document;
            writeDoc.open();
            writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
            writeDoc.close();
            newWin.focus();
        };
        //$scope.getList();
    }]);




