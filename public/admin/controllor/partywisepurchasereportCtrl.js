app.controller('partywisepurchasereportCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', function ($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.purchaseModel = {
            currentPage: 1,
            total: 0,
            Amount: 0,
            Quantity: 0,
            limit: 10,
            list: [],
            length: [],
            date: '',
            sno: '',
            name: '',
            amount: '',
            status: '',
//             sku: '',

            serverList: null,
            isLoadingProgress: false,
            isSearchLoadingProgress: false
        };
        $scope.showDetail = false;
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.purchaseModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
        $scope.validationFactory = ValidationFactory;
        $scope.openDate = function (index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            } else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };

        $scope.searchFilter = {
            fromdate: '',
            todate: ''

        };
        $scope.searchFilter.fromdate = $scope.currentDate;

        $scope.clearFilters = function ()
        {
            $scope.searchFilter = {
                fromdate: '',
                todate: ''
            };
            $scope.purchaseModel.list = [];
            $scope.showDetail = false
            $scope.searchFilter.fromdate = $scope.currentDate;
        }

        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.validateDateFilterData = function ()
        {
            var retVal = false;
            if ($scope.searchFilter.todate !== '' || $scope.searchFilter.fromdate !== '')
            {
                retVal = true;
            }
            return retVal;
        };
        $scope.getList = function () {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'discountedreport';
//            if (!$scope.showDetail)
//            {
//                getListParam.summary = 1;
//            }
//            else
//            {
//                getListParam.summary = '';
//            }
            getListParam.summary = '';
            getListParam.from_date = $scope.searchFilter.fromdate;
            getListParam.to_date = $scope.searchFilter.todate;
            if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
            {
                getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
            }
            if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
            {
                getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
            }

            if ($scope.searchFilter.fromdate != null && $scope.searchFilter.fromdate != '')
            {
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);

            }
            if ($scope.searchFilter.todate != null && $scope.searchFilter.todate != '')
            {
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
            }
            getListParam.start = 0;
            getListParam.limit = 0;
            $scope.purchaseModel.isLoadingProgress = true;
            $scope.purchaseModel.isSearchLoadingProgress = true;
            if ($scope.searchFilter.todate != '' && $scope.searchFilter.todate != null && $scope.searchFilter.todate != 'undefined' && $scope.searchFilter.fromdate != '' && $scope.searchFilter.fromdate != null)
            {
                adminService.getPartywisePurchaseReportList(getListParam, headers).then(function (response)
                {
                    var totalAmt = 0.00;
                    var totalqty = 0;
                    if (response.data.success === true)
                    {
                        var data = response.data.list;
                        $scope.purchaseModel.list = data;

                        for (var i = 0; i < $scope.purchaseModel.list.length; i++)
                        {
                            if ($scope.purchaseModel.list[i].total_amount == null)
                            {
                                $scope.purchaseModel.list[i].total_amount = 0.00;
                            }
                            totalAmt = totalAmt + parseFloat($scope.purchaseModel.list[i].total_amount);
                            $scope.purchaseModel.list[i].total_amount = utilityService.changeCurrency($scope.purchaseModel.list[i].total_amount, $rootScope.appConfig.thousand_seperator);

                        }
                        for (var i = 0; i < $scope.purchaseModel.list.length; i++)
                        {
                            if ($scope.purchaseModel.list[i].Purchase_Qty == null)
                            {
                                $scope.purchaseModel.list[i].Purchase_Qty = 0;
                            }
                            totalqty = totalqty + parseFloat($scope.purchaseModel.list[i].Purchase_Qty);
                            $scope.purchaseModel.list[i].Purchase_Qty = utilityService.changeCurrency($scope.purchaseModel.list[i].Purchase_Qty, $rootScope.appConfig.thousand_seperator);

                        }
                        $scope.purchaseModel.total = data.total;
                        $scope.purchaseModel.Amount = totalAmt;
                        $scope.purchaseModel.Amount = parseFloat($scope.purchaseModel.Amount).toFixed(2);
                        $scope.purchaseModel.Amount = utilityService.changeCurrency($scope.purchaseModel.Amount, $rootScope.appConfig.thousand_seperator);
                        $scope.purchaseModel.Quantity = totalqty;
                        // $scope.purchaseModel.Quantity = parseFloat($scope.purchaseModel.Quantity).toFixed(2);
                        //$scope.purchaseModel.Quantity = utilityService.changeCurrency($scope.purchaseModel.Quantity, $rootScope.appConfig.thousand_seperator);

                        $scope.purchaseModel.isLoadingProgress = false;
                        $scope.purchaseModel.isSearchLoadingProgress = false;
                    }
                });
            } else {
                $scope.purchaseModel.isLoadingProgress = false;
                $scope.purchaseModel.isSearchLoadingProgress = false;
            }

        };

        $scope.print = function (div)
        {
            var docHead = document.head.outerHTML;
            var printContents = document.getElementById(div).outerHTML;
            var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=865, height=600, resizable=yes, screenX=200, screenY=200, personalbar=no, scrollbars=yes";

            var newWin = window.open("", "_blank", winAttr);
            var writeDoc = newWin.document;
            writeDoc.open();
            writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
            writeDoc.close();
            newWin.focus();
        };

        $scope.validateAmount = function (amount)
        {
            if (amount > 0)
            {
                return true;
            } else
            {
                return false;
            }
        };

        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'payable_form';


        $scope.$on('onLocalStorageFormRenterCompleteEvent', function (event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });

        $scope.$on('onLocalStorageFieldRenterCompleteEvent', function (event, formName) {
            if ($scope.searchFilter.fromdate == '' || typeof $scope.searchFilter.fromdate == 'undefined' || $scope.searchFilter.fromdate == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRenterCount++;
                }
                if ($scope.localStorageRenterCount >= $scope.localStorageCount)
                {
                    console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                    $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
                }

            }
        });
        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function (event, formName) {
            if ($scope.searchFilter.fromdate == '' || typeof $scope.searchFilter.fromdate == 'undefined' || $scope.searchFilter.fromdate == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRetrieveCount++;
                }
                if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
                {
                    $scope.initTableFilter();
                }
                console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);
            }
        });

        $scope.init = function ()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');

        };

        $scope.$on('onLocalStorageReadyEvent', function () {
            if ($scope.searchFilter.fromdate == '' || typeof $scope.searchFilter.fromdate == 'undefined' || $scope.searchFilter.fromdate == null)
            {
                $timeout(function () {
                    $scope.init();
                }, 300);

            }
        });



        $scope.getList();
    }]);








