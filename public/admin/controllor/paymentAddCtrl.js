app.controller('paymentAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', 'utilityService', '$timeout', '$filter', 'Auth', 'ValidationFactory', '$state', function($scope, $rootScope, adminService, $httpService, APP_CONST, utilityService, $timeout, $filter, Auth, ValidationFactory, $state) {
        $scope.paymentAddModel = {
            "id": 0,
            "companyId": 1,
            "userid": '',
            "clientId": '',
            list: [],
            "comments": " ",
            "amount": '',
        }
        $scope.validationFactory = ValidationFactory;
        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.create_payment_form != 'undefined' && typeof $scope.create_payment_form.$pristine != 'undefined' && !$scope.create_payment_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.formReset = function() {

            $scope.create_payment_form.$setPristine();
            $scope.paymentAddModel.clientInfo = '';
            $scope.paymentAddModel.amount = '';
            $scope.paymentAddModel.comments = '';

        }
        $scope.formatClientModel = function(model) {

            if (model != null)
            {
                return model.name;
            }
            return  '';
        };

        $scope.getClientList = function(val) {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.Id = "";
            autosearchParam.phno = "";
            autosearchParam.email = "";
            autosearchParam.country = '';
            autosearchParam.city = "";
            autosearchParam.state = "";
            autosearchParam.mode = 1;
            autosearchParam.sort = 1;
            autosearchParam.start = 0;
            autosearchParam.limit = 10;
            autosearchParam.companyId = $rootScope.userModel.companyId;
            return $httpService.get(APP_CONST.API.GET_CLIENT_LIST, autosearchParam, false).then(function(responseData) {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.createPayment = function() {

            $scope.isDataSavingProcess = true;
            var createPaymentParam = {};
            createPaymentParam.id = 0;
            createPaymentParam.companyId = $rootScope.userModel.companyId;
            createPaymentParam.userid = $rootScope.userModel.userid;
            createPaymentParam.clientId = $scope.paymentAddModel.clientInfo.id;
            createPaymentParam.comments = $scope.paymentAddModel.comments;
            createPaymentParam.amount = $scope.paymentAddModel.amount;

            adminService.createPayment(createPaymentParam).then(function(response) {

                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.paymentlist');
                }
                $scope.isDataSavingProcess = false;

            });
        };


    }]);

