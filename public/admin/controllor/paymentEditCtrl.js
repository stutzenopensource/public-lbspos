

app.controller('paymentEditCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', function($scope, $rootScope, adminService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory) {

        $scope.paymentModel = {
            "id": '',
            "companyId": '',
            "paymentId": '',
            list: [],
            "clientId": '',
            "comments": "",
            "amount": "",
            "clientname": '',
            "email": '',
            isLoadingProgress: false,
            isSettled: '',
            "username": ''

        };
        $scope.validationFactory = ValidationFactory;

        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.payment_edit_form != 'undefined' && typeof $scope.payment_edit_form.$pristine != 'undefined' && !$scope.payment_edit_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function() {

            if (typeof $scope.payment_edit_form != 'undefined')
            {
                $scope.payment_edit_form.$setPristine();
                $scope.updatePaymentInfo();
            }
        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.updatePaymentInfo = function()
        {
            $scope.paymentModel.id = $scope.paymentModel.list.id;
            $scope.paymentModel.companyId = $scope.paymentModel.list.companyId;
            $scope.paymentModel.userid = $scope.paymentModel.list.userid;
            $scope.paymentModel.username = $scope.paymentModel.list.firstName;
            $scope.paymentModel.clientId = $scope.paymentModel.list.clientId;
            $scope.paymentModel.comments = $scope.paymentModel.list.comments;
            $scope.paymentModel.amount = $scope.paymentModel.list.amount;
            $scope.paymentModel.clientname = $scope.paymentModel.list.clientName;
            $scope.paymentModel.email = $scope.paymentModel.list.email;
            $scope.paymentModel.isSettled = $scope.paymentModel.list.isSettled;
            $scope.paymentModel.isLoadingProgress = false;
        }

        $scope.modifyPayment = function() {
            $scope.isDataSavingProcess = true;
            var modifyPaymentParam = {};
            modifyPaymentParam.id = $scope.paymentModel.list.id;
            modifyPaymentParam.companyId = $scope.paymentModel.companyId;
            modifyPaymentParam.userid = $scope.paymentModel.userid;
            modifyPaymentParam.clientId = $scope.paymentModel.clientId;
            modifyPaymentParam.comments = $scope.paymentModel.comments;
            modifyPaymentParam.amount = $scope.paymentModel.amount;
            
            adminService.updatePayment(modifyPaymentParam,$scope.paymentModel.list.id).then(function(response) {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.paymentlist');
                }
                $scope.isDataSavingProcess = false;
            });
        };
        $scope.getPayment = function() {

            $scope.paymentModel.isLoadingProgress = true;
            var getListParam = {};
            getListParam.id = $stateParams.id;
            adminService.getPaymentList(getListParam).then(function(response) {
                var data = response.data;
                if (data.list.length != 0)
                {
                    $scope.paymentModel.list = data.list[0];
                }
                $scope.paymentModel.total = data.total;
                $scope.updatePaymentInfo();

            });
        };
        $scope.getPayment();
    }]);




