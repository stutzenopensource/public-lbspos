


app.controller('paymentListCtrl', ['$scope', '$rootScope', '$window', 'adminService', 'APP_CONST', '$httpService', '$filter', 'Auth', '$state', '$timeout', 'ValidationFactory', function($scope, $rootScope, $window, adminService, APP_CONST, $httpService, $filter, Auth, $state, $timeout, ValidationFactory) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.paymentModel = {
            currentPage: 1,
            id: '',
            total: 0,
            limit: 10,
            userid: '',
            clientId: '',
            list: [],
            isSettled: '',
            status: '',
            serverList: null,
            isLoadingProgress: false,
            userInfo: '',
            clientInfo: '',
            categoryList: [],
            userList: []
        };
        $scope.pagePerCount = [50, 100];
        $scope.paymentModel.limit = $scope.pagePerCount[0];
        $scope.validationFactory = ValidationFactory;
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = 'dd-MM-yyyy';

        $scope.openDate = function(index) {

            if (index == 0)
            {
                $scope.fromDateOpen = true;
            }
            else if (index == 1)
            {
                $scope.toDateOpen = true;
            }

        }

        $scope.searchFilter = {
            clientemail: '',
            userInfo: '',
            clientInfo: '',
            payment_type: '0',
            category: '',
            fromdate: '',
            todate: '',
            accno: '',
            amt: '',
            belongto: '',
            customername: '',
            symbol: ''
        };
        $scope.initTableFilterTimeoutPromise = null;
        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }

        $scope.update = function(isfilteropen)
        {
            $scope.searchFilter.accno = '';
            $scope.searchFilter.customername = '';
            $scope.searchFilter.fromdate = '';
            $scope.searchFilter.todate = '';
            $scope.searchFilter.belongto = '';
            $scope.searchFilter.amt = '';
            $scope.searchFilter.symbol = '';
            if (isfilteropen == false)
            {
                $scope.initTableFilter();
            }

        }


        $scope.formatUserModel = function(model) {

            if (model != null)
            {
                return model.username;
            }
            return  '';
        };



        $scope.getUserList = function(val) {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.companyId = $rootScope.userModel.companyId;
            return $httpService.get(APP_CONST.API.GET_USER_LIST, autosearchParam, false).then(function(responseData) {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.getBelongtoUserList = function() {
            var userListParam = {};
            userListParam.id = "";
            userListParam.name = "";
            userListParam.start = 0;
            userListParam.limit = 100;
            adminService.getUserList(userListParam).then(function(response) {
                var data = response.data;
                $scope.paymentModel.userList = data.data;

            });

        };


        $scope.formatClientModel = function(model) {

            if (model != null)
            {
                return model.name;
            }
            return  '';
        };

        $scope.getClientList = function(val) {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.companyId = $rootScope.userModel.companyId;
            autosearchParam.mode = 1;
            autosearchParam.sort = 1;
            return $httpService.get(APP_CONST.API.GET_CLIENT_LIST, autosearchParam, false).then(function(responseData) {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.getCategoryList = function() {
            var userListParam = {};
            userListParam.id = "";
            userListParam.name = "";
            userListParam.start = 0;
            userListParam.limit = 100;
            adminService.getCategoryList(userListParam).then(function(response) {
                var data = response.data;
                $scope.paymentModel.categoryList = data.list;

            });

        };

        $scope.print = function() {
            $window.print;
        }

        $scope.getList = function() {

            var getListParam = {};
            getListParam.id = "";
            getListParam.companyId = $rootScope.userModel.companyId;
            getListParam.userid = $scope.paymentModel.userInfo;
            if ($scope.paymentModel.clientInfo != null && typeof $scope.paymentModel.clientInfo != 'undefined' && typeof $scope.paymentModel.clientInfo.id != 'undefined')
            {
                getListParam.clientId = $scope.paymentModel.clientInfo.id;
            }
            else
            {
                getListParam.clientId = '';
            }
            getListParam.clientEmail = '';
            getListParam.categoryId = $scope.searchFilter.category;
            getListParam.isSettled = $scope.searchFilter.payment_type;

            getListParam.fromDate = $filter('date')($scope.searchFilter.fromdate, 'yyyy-MM-dd');
            getListParam.toDate = $filter('date')($scope.searchFilter.todate, 'yyyy-MM-dd');
            if (getListParam.fromDate === '' || getListParam.toDate === '')
            {
                if (getListParam.fromDate !== '')
                {
                    getListParam.toDate = getListParam.fromDate;
                }
                if (getListParam.toDate !== '')
                {
                    getListParam.fromDate = getListParam.toDate;
                }
            }
            getListParam.accountNumber = $scope.searchFilter.accno;
            getListParam.custName = $scope.searchFilter.customername;
            getListParam.amount = '';
            getListParam.customerOf = $scope.searchFilter.belongto;
            $scope.paymentModel.isLoadingProgress = true;
            adminService.getPaymentList(getListParam).then(function(response) {
                var data = response.data;
                $scope.paymentModel.list = data.list;
                $scope.paymentModel.total = data.total;
                console.log($scope.paymentModel.list);
                $scope.paymentModel.isLoadingProgress = false;
            });
        };
        $scope.getList();

        $scope.$watch('paymentModel.clientInfo', function(newVal, oldVal) {

            if (typeof (newVal == 'undefined' || newVal == '') && oldVal !='' ) 
            {
                $scope.initTableFilter();
            }
        });

        $scope.getCategoryList();
        $scope.getBelongtoUserList();
    }]);








