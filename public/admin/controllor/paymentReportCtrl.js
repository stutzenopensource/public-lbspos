





app.controller('paymentReportCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', function($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.paymentModel = {
            currentPage: 1,
            limit: 10,
            list: [],
            length: [],
            serverList: null,
            isLoadingProgress: false,
            categoryList: []
        };

        $scope.pagePerCount = [50, 100];
        $scope.paymentModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
        $scope.validationFactory = ValidationFactory;
        $scope.openDate = function(index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            }
            else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };

        $scope.searchFilter = {
            customer_id: '',
            fromdate: '',
            todate: '',
            invoiceid: '',
            category: '',
            customerInfo: {},
            receiverInfo: {}

        };

        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };

        $scope.$watch('searchFilter.customerInfo', function(newVal, oldVal)
        {
            if (typeof newVal == 'undefined' || newVal == '')
            {
                {
                    $scope.initTableFilter();
                }
            }
        });
        $scope.$watch('searchFilter.receiverInfo', function(newVal, oldVal)
        {
            if (typeof newVal == 'undefined' || newVal == '')
            {
                {
                    $scope.initTableFilter();
                }
            }
        });
        $scope.getCustomerList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.fname = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.CUSTOMERS_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatcustomerModel = function(model) {

            if (model !== null)
            {
                $scope.searchFilter.id = model.id;
                return model.fname;
            }
            return  '';
        };

        $scope.getReceiverList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;

            return $httpService.get(APP_CONST.API.GET_USER_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatReceiverModel = function(model) {

            if (model !== null)
            {
                $scope.searchFilter.id = model.id;
                return model.f_name;
            }
            return  '';
        };

        $scope.print = function()
        {
            $window.print();
        };


        $scope.getIncomeCategoryList = function() {

            var getListParam = {};
            getListParam.id = '';
            getListParam.is_active = 1;
            getListParam.type = '';
            adminService.getIncomeCategoryList(getListParam).then(function(response) {
                var data = response.data.list;
                $scope.paymentModel.categoryList = data;
            });

        };

        $scope.getList = function() {

            var getListParam = {};
            getListParam.id = '';
            if ($scope.searchFilter.customerInfo != null && typeof $scope.searchFilter.customerInfo != 'undefined' && typeof $scope.searchFilter.customerInfo.id != 'undefined')
            {
                getListParam.customer_id = $scope.searchFilter.customerInfo.id;
            }
            else
            {
                getListParam.customer_id = '';
            }
            getListParam.from_date = $scope.searchFilter.fromdate;
            getListParam.to_date = $scope.searchFilter.todate;
            if ($scope.searchFilter.todate !== '' && $scope.searchFilter.fromdate === '')
            {
                getListParam.to_date = $filter('date')($scope.searchFilter.todate, $scope.dateFormat);
                getListParam.from_date = $filter('date')($scope.searchFilter.todate, $scope.dateFormat);
            }
            else if ($scope.searchFilter.fromdate !== '' && $scope.searchFilter.todate === '')
            {
                getListParam.to_date = $filter('date')($scope.searchFilter.fromdate, $scope.dateFormat);
                getListParam.from_date = $filter('date')($scope.searchFilter.fromdate, $scope.dateFormat);
            }
            else
            {
                getListParam.to_date = $filter('date')($scope.searchFilter.todate, $scope.dateFormat);
                getListParam.from_date = $filter('date')($scope.searchFilter.fromdate, $scope.dateFormat);
            }
            getListParam.tra_category = $scope.searchFilter.category;
            if ($scope.searchFilter.receiverInfo != null && typeof $scope.searchFilter.receiverInfo != 'undefined' && typeof $scope.searchFilter.receiverInfo.id != 'undefined')
            {
                getListParam.recevied_by = $scope.searchFilter.receiverInfo.id;
            }
            else
            {
                getListParam.recevied_by = '';
            }
            getListParam.invoice_id = $scope.searchFilter.invoiceid;
            $scope.paymentModel.isLoadingProgress = true;
            adminService.getPaymentList(getListParam).then(function(response)
            {
                var totalAmt = 0.00;
                var data = response.data.list;
                $scope.paymentModel.list = data;
                for (var i = 0; i < $scope.paymentModel.list.length; i++)
                {
                    $scope.paymentModel.list[i].newdate = utilityService.parseStrToDate($scope.paymentModel.list[i].date);
                    $scope.paymentModel.list[i].date = utilityService.parseDateToStr($scope.paymentModel.list[i].newdate, $scope.dateFormat);
                    totalAmt = totalAmt + parseFloat($scope.paymentModel.list[i].amount)
                }
                $scope.paymentModel.total = data.total;
                $scope.paymentModel.total_Amount = totalAmt;
                $scope.paymentModel.isLoadingProgress = false;
            });

        };

        $scope.getList();
        $scope.getIncomeCategoryList();
    }]);




