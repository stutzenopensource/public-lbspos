


app.controller('paymentSettlementCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', '$state', 'APP_CONST', '$timeout', function($scope, $rootScope, adminService, $httpService, $filter, Auth, $state, APP_CONST, $timeout) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.paymentSettlementModel = {
            currentPage: 1,
            id: '',
            total: 0,
            limit: 10,
            userList: [],
            isSelectedAll: false,
            isLoadingProgress: false,
            list: [],
            totalamount: 0,
            settledList: [],
            totalsettleamount: 0
        };
        $scope.pagePerCount = [50, 100];
        $scope.paymentSettlementModel.limit = $scope.pagePerCount[0];
        $scope.formatUserModel = function(model) {

            if (model != null)
            {
                return model.username;
            }
            return  '';
        };
        $scope.updateTotal = function()
        {
            for (var i = 0; i < $scope.paymentSettlementModel.list.length; i++)
            {
                $scope.paymentSettlementModel.totalamount += $scope.paymentSettlementModel.list[i].amount;
            }
        }
        $scope.updateSettledTotal = function()
        {
            $scope.paymentSettlementModel.totalsettleamount = 0;
            for (var i = 0; i < $scope.settledList.length; i++)
            {
                $scope.paymentSettlementModel.totalsettleamount += $scope.settledList[i].amount;
            }
        }
        $scope.paymentSettlementdetail = {};
        $scope.settledList = [];

        $scope.checkAllItem = function()
        {
            for (var i = 0; i < $scope.paymentSettlementModel.list.length; i++)
            {
                $scope.paymentSettlementModel.isSelectedAll
                if ($scope.paymentSettlementModel.isSelectedAll)
                {
                    $scope.paymentSettlementModel.list[i].isSelected = true;
                }
                else
                {
                    $scope.paymentSettlementModel.list[i].isSelected = false;
                }
            }
            $scope.checkItem();
        }

        $scope.checkItem = function()
        {
            $scope.paymentSettlementdetail = {};
            $scope.settledList = [];
            for (var i = 0; i < $scope.paymentSettlementModel.list.length; i++)
            {
                if ($scope.paymentSettlementModel.list[i].isSelected == true)
                {
                    $scope.paymentSettlementdetail = angular.copy($scope.paymentSettlementModel.list[i]);
                    $scope.settledList.push($scope.paymentSettlementdetail);
                }
            }
            console.log("settledlist");
            console.log($scope.settledList);
            $scope.updateSettledTotal();
        }

        $scope.getUserList = function() {
            $scope.paymentSettlementModel.isLoadingProgress = true;
            var getListParam = {};
            getListParam.userid = '';
            adminService.getUserList(getListParam).then(function(response) {
                var data = response.data;
                if (response.data.total != 0)
                {
                    $scope.paymentSettlementModel.userList = data.data;
                }
                $scope.paymentSettlementModel.total = response.data.total;


            });
        };


//       //        $scope.settlementPrint = function()
//        {
//            if($scope.paymentSettlementModel.user != '' && typeof $scope.paymentSettlementModel.user.userid  != 'undefined')
//            {
//                
//            }
        //       }


        $scope.paymentDetail = {};

        $scope.getPaymentList = function(userid) {
            $scope.paymentSettlementModel.list = [];
            $scope.settledList = [];
            $scope.paymentSettlementModel.totalamount = 0;
            var getListParam = {};
            getListParam.id = "";
            getListParam.userid = userid;
            getListParam.clientId = "";
            getListParam.isSettled = 0;
            getListParam.start = 0;
            getListParam.limit = 100;
            adminService.getPaymentList(getListParam).then(function(response) {
                var data = response.data;
                for (i = 0; i < data.total; i++)
                {
                    if (data.list[i].user_id == userid)
                    {
                        $scope.paymentDetail = data.list[i];
                        $scope.paymentSettlementModel.list.push($scope.paymentDetail);
                    }
                }
                $scope.paymentSettlementModel.total = data.total;
                $scope.updateTotal();
            });
        };

        $scope.createPaymentSettlement = function() {

            var paymentSettlementParam = [];
            var configData = {};

            for (i = 0; i < $scope.settledList.length; i++)
            {
                var payment = {};
                payment.id = $scope.settledList[i].id;
                payment.companyId = $scope.settledList[i].companyId;
                payment.userid = $scope.settledList[i].userid;
                payment.clientId = $scope.settledList[i].clientId;
                payment.comments = $scope.settledList[i].comments;
                payment.amount = $scope.settledList[i].amount;
                payment.isSettled = false;
                
                paymentSettlementParam.push(payment);
            }
            adminService.createPaymentSettlement(paymentSettlementParam, configData).then(function(response) {
                var config = response.config.config;
                if (config.handleResponse == true)
                {
                    $state.go('app.paymentlist');
                }
                $scope.$broadcast('genericSuccessEvent', 'Payment Updated Successfully.');

            });

        };

        $scope.getUserList();

    }]);








