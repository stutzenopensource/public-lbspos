
app.controller('paymenttermsCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$timeout', '$window', function ($scope, $rootScope, adminService, $filter, Auth, $state, $timeout, $window) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.paymenttermsModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
            id: '',
            is_active: 1,
            status: '',
            total: '',
            isLoadingProgress: false
        };
        $scope.pagePerCount = [50, 100];
        $scope.paymenttermsModel.limit = $scope.pagePerCount[0];
        $scope.searchFilter = {
            name: '',
            id: ''
        };
        $scope.searchFilterValue = "";
        $scope.refreshScreen = function ()
        {
            $scope.searchFilter = {
                id: '',
                name: ''

            };
            $scope.initTableFilter();
        }

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }

        $scope.selectpaymenttermsId = '';
        $scope.showdeletePopup = false;
        $scope.isdeleteLoadingProgress = false;
        $scope.showPopup = function (id)
        {
            $scope.selectpaymenttermsId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function ()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteLoadingProgress = false;
        };

        $scope.deletepaymenttermsInfo = function ( )
        {
            if ($scope.isdeleteLoadingProgress == false)
            {
                $scope.isdeleteLoadingProgress = true;
                var getListParam = {};
                getListParam.id = $scope.selectpaymenttermsId;
                var headers = {};
                headers['screen-code'] = 'paymentterms';
                adminService.deletepaymentterms(getListParam, getListParam.id, headers).then(function (response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getList();
                    }
                    $scope.isdeleteLoadingProgress = false;
                });
            }
        };

        $scope.getList = function ()
        {
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'paymentterms';
            getListParam.id = $scope.searchFilter.id;
            getListParam.name = $scope.searchFilter.name;
            getListParam.is_active = 1;
//        getListParam.rolename = $scope.searchFilter.rolename;  
            if ($rootScope.paymentNext == false) //For Demo Guide purpose
            {
                getListParam.start = 0;
                getListParam.limit = 1;

            } else {
                getListParam.start = ($scope.paymenttermsModel.currentPage - 1) * $scope.paymenttermsModel.limit;
                getListParam.limit = $scope.paymenttermsModel.limit;
            }
            $scope.paymenttermsModel.isLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getpaymenttermslist(getListParam, configOption, headers).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.paymenttermsModel.list = data.list;
                    for (var i = 0; i < $scope.paymenttermsModel.list.length; i++)
                    {
                        $scope.paymenttermsModel.list[i].is_default = $scope.paymenttermsModel.list[i].is_default == 1 ? "YES" : "NO";

                    }
                    $scope.paymenttermsModel.total = data.total;
                    if ($scope.paymenttermsModel.list.length > 0)
                    {
                        $rootScope.paymentTermsAdded = true;

                        $window.localStorage.setItem("paymentTermsAdded", "true");
                    }
                    if ($rootScope.userModel.new_user == 1 && $rootScope.paymentNext != false && $rootScope.paymentNext != undefined)
                    {
                        $window.localStorage.setItem("demo_guide", "true");
                        $rootScope.closeDemoPopup(5);
                    }
                }
                $scope.paymenttermsModel.isLoadingProgress = false;
            });
        };

        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'paymentterms_form';

        $scope.$on('onLocalStorageFormRenterCompleteEvent', function (event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });

        $scope.$on('onLocalStorageFieldRenterCompleteEvent', function (event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRenterCount++;
                }
                if ($scope.localStorageRenterCount >= $scope.localStorageCount)
                {
                    console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                    $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
                }

            }
        });

        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function (event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRetrieveCount++;
                }
                if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
                {
                    $scope.initTableFilter();
                }
                console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);
            }
        });

        $scope.init = function ()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');

        };

        $scope.$on('onLocalStorageReadyEvent', function () {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                $timeout(function () {
                    $scope.init();
                }, 300);

            }
        });
        //$scope.getList();
    }]);








