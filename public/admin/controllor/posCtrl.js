


app.controller('posCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$httpService', 'APP_CONST', '$filter', 'Auth', '$state', '$timeout', 'ValidationFactory', 'sweet', '$window', '$http', '$localStorage', function ($scope, $rootScope, adminService, utilityService, $httpService, APP_CONST, $filter, Auth, $state, $timeout, ValidationFactory, sweet, $window, $http, $localStorage) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.posModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            productList: [],
            serverList: null,
            isLoadingProgress: false,
            uncategorizedList: [],
            categoryList: [],
            accountsList: [],
            addedProducts: [],
            type: '',
            mcurrentPage: 1,
            mtotal: 0,
            mlimit: 5,
            mstart: 0,
            category: '',
            ttcount: 0,
            ttcount1: 0,
            ttcount2: 0,
            ttcount3: 0,
            ttcount4: 0,
            ttcount5: 0,
            ttcount6: 0,
            ttcount7: 0,
            ttcount8: 0,
            ttamount: '0.00',
            ttamount1: '0.00',
            ttamount2: '0.00',
            ttamount3: '0.00',
            ttamount4: '0.00',
            ttamount5: '0.00',
            ttamount6: '0.00',
            ttamount7: '0.00',
            ttamount8: '0.00',
            cashamount: '0.00',
            cardamount: '0.00',
            credit: '0.00',
            others: '0.00',
            advance: '0.00',
            startdate: '',
            enddate: '',
            daycode: '',
            username: '',
            password: '',
            posCus: '',
            cusPurDetail: '',
            newproductname: '',
            newprodqty: '',
            newproddiscount: '0',
            newprodprice: '',
            newprodnote: '',
            allProductdiscount: ''
        };
        $scope.currencyFormat = $rootScope.appConfig.currency;
        $scope.buttontype = 'sale'
        $scope.orderListModel =
                {
                    "id": 0,
                    "productname": "",
                    "sku": "",
                    "unit": "",
                    "mrp": "",
                    "selling_price": "",
                    "qty": "",
                    "rowtotal": "",
                    "isActive": "1",
                    "total": '0.00',
                    "customername": "",
                    "phone": "",
                    "addressId": "",
                    "subtotal": '0.00',
                    "taxAmt": '0.00',
                    "taxInfo": "",
                    "discountAmt": '0.00',
                    list: [],
                    "billno": "",
                    "billdate": "",
                    "coupon": "",
                    "shipping": 0,
                    "discamount": 0,
                    "code": "",
                    "customerInfo": '',
                    //                    "payeeInfo": '',
                    //                    "consigneeInfo": '',
                    "invoiceDetail": {},
                    "invoicePrefix": "",
                    "paymentTerm": "",
                    "taxPercentage": "",
                    "discountPercentage": "0",
                    "totalDisPercentage": "0",
                    "totalTaxPercentage": "",
                    "duedate": "",
                    "balance_amount": "",
                    "paid_amount": "",
                    "tax_id": "",
                    "productList": [],
                    "productId": '',
                    "notes": '',
                    "discountMode": '%',
                    "roundoff": '0.00',
                    "invoiceno": '',
                    "taxAmountDetail": {
                        "tax_amount": ''
                    },
                    //"taxAmountDetail": '',
                    "subtotalWithDiscount": '',
                    "round": '0.00',
                    "dealId": '',
                    "dealName": '',
                    "insuranceCharge": 0,
                    "insuranceAmt": '0.00',
                    "packagingCharge": '0',
                    "packagingAmt": '0.00',
                    sepdiscountAmt: '0.00',
                    sepsubtotal: '0.00',
                    septaxAmt: '0.00',
                    sepround: '0.00',
                    payamt: '',
                    paymentList: [],
                    cashAmt: '0.00',
                    cardAmt: '0.00',
                    sepCashAmt: '',
                    sepCardAmt: '',
                    cardcomments: '',
                    savePaymentList: [],
                    prefixList: [],
                    totalQty: '',
                    is_next_autogenerator_num_changed: false
                };
        $scope.validationFactory = ValidationFactory;
        $scope.searchFilter = {
            name: '',
            sku: '',
            code: '',
            category: ''
        };
        $scope.currentStep = 0;
        $scope.currentDate = new Date();
        $scope.orderListModel.billdate = $scope.currentDate;
        $scope.adminService = adminService;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.newProduct = '';
        $scope.indexInAP = '';
        $scope.selectedNewProduct = '';
        $scope.isCategoryLoaded = false;
        $scope.isProductLoaded = false;
        $scope.isProductLoading = false;
        $scope.initTableProductFilterTimeoutPromise = null;
        $scope.initTableProductFilter = function ()
        {
            //            if ($scope.isProductLoading == false)
            //            {
            if ($scope.initTableProductFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableProductFilterTimeoutPromise);
            }
            if ($scope.isProductLoading == false)
            {
                $scope.getProductList();
            } else
            {
                $scope.initTableProductFilterTimeoutPromise = $timeout($scope.initTableProductFilter, 300);
            }
            //            }
        }
        $scope.initTableCustomerFilterTimeoutPromise = null;
        $scope.initTableCustomerFilter = function ()
        {
            if ($scope.initTableCustomerFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableCustomerFilterTimeoutPromise);
            }
            if ($scope.isCategoryLoaded && $scope.isProductLoaded)
            {
                $scope.getCustomerInfo();
            } else
            {
                $scope.initTableCustomerFilterTimeoutPromise = $timeout($scope.initTableCustomerFilter, 300);
            }
        }
        $scope.getNavigationBlockMsg = function (pageReload)
        {
            if (typeof $scope.save_invite_form != 'undefined' && typeof $scope.save_invite_form.$pristine != 'undefined' && !$scope.save_invite_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.formReset = function () {

            $scope.save_invite_form.$setPristine();
            $scope.posModel = {
                currentPage: 1,
                total: 0,
                limit: 10,
                productList: [],
                serverList: null,
                isLoadingProgress: false,
                uncategorizedList: [],
                categoryList: [],
                addedProducts: [],
                type: '',
                mcurrentPage: 1,
                mtotal: 0,
                mlimit: 5,
                mstart: 0,
                category: '',
                ttcount: 0,
                ttcount1: 0,
                ttcount2: 0,
                ttcount3: 0,
                ttcount4: 0,
                ttcount5: 0,
                ttcount6: 0,
                ttcount7: 0,
                ttcount8: 0,
                ttamount: '0.00',
                ttamount1: '0.00',
                ttamount2: '0.00',
                ttamount3: '0.00',
                ttamount4: '0.00',
                ttamount5: '0.00',
                ttamount6: '0.00',
                ttamount7: '0.00',
                ttamount8: '0.00',
                cashamount: '0.00',
                cardamount: '0.00',
                credit: '0.00',
                others: '0.00',
                advance: '0.00',
                startdate: '',
                enddate: '',
                daycode: '',
                username: '',
                password: '',
                posCus: '',
                newproductname: '',
                newprodqty: '',
                newproddiscount: '0',
                newprodprice: '',
                newprodnote: ''
            };
            $scope.searchFilter.name = '';
            $scope.currencyFormat = $rootScope.appConfig.currency;
            $scope.orderListModel =
                    {
                        "id": 0,
                        "productname": "",
                        "sku": "",
                        "unit": "",
                        "mrp": "",
                        "selling_price": "",
                        "qty": "",
                        "rowtotal": "",
                        "isActive": "1",
                        "total": '0.00',
                        "customername": "",
                        "phone": "",
                        "addressId": "",
                        "subtotal": '0.00',
                        "taxAmt": '0.00',
                        "taxInfo": "",
                        "discountAmt": '0.00',
                        list: [],
                        "billno": "",
                        "billdate": "",
                        "coupon": "",
                        "shipping": 0,
                        "discamount": 0,
                        "code": "",
                        "customerInfo": '',
                        //                    "payeeInfo": '',
                        //                    "consigneeInfo": '',
                        "invoiceDetail": {},
//                        "invoicePrefix": "",
                        "paymentTerm": "",
                        "taxPercentage": "",
                        "discountPercentage": "0",
                        "totalDisPercentage": "0",
                        "totalTaxPercentage": "",
                        "duedate": "",
                        "balance_amount": "",
                        "paid_amount": "",
                        "tax_id": "",
                        "productList": [],
                        "productId": '',
                        "notes": '',
                        "discountMode": '%',
                        "roundoff": '0.00',
                        "invoiceno": '',
                        "taxAmountDetail": {
                            "tax_amount": ''
                        },
                        //"taxAmountDetail": '',
                        "subtotalWithDiscount": '',
                        "round": '0.00',
                        "dealId": '',
                        "dealName": '',
                        "insuranceCharge": 0,
                        "insuranceAmt": '0.00',
                        "packagingCharge": '0',
                        "packagingAmt": '0.00',
                        sepdiscountAmt: '0.00',
                        sepsubtotal: '0.00',
                        septaxAmt: '0.00',
                        sepround: '0.00',
                        payamt: '',
                        paymentList: [],
                        cashAmt: '0.00',
                        cardAmt: '0.00',
                        sepCashAmt: '',
                        sepCardAmt: '',
                        totRevisedSellingRowTotal: '0',
                        septotRevisedSellingRowTotal: '0.00',
                        prefixList: [],
                        is_next_autogenerator_num_changed: false,
                        invoicePrefix: ''
                    };
            $scope.posModel.email = '';
            $scope.getCategoryList();
            $scope.getPrefixList();
            $scope.currentStep = 0;
            $scope.currentDate = new Date();
            $scope.orderListModel.billdate = $scope.currentDate;
            $scope.customername = '';
            $scope.phone = '';
            $scope.customerDetail = '';
            $scope.showcustomername = false;
            $scope.showcustomerpopup = false
        }

        $scope.showCustomerAddPopup = false;
        $scope.showAddCustomerPopup = function ()
        {
            $scope.showcustomerpopup = false;
            $scope.showCustomerAddPopup = true;
        }
        $scope.closeCustomerPopup = function ()
        {
            $scope.showCustomerAddPopup = false;
        }
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.openDate = function (index)
        {
            if (index == 0)
            {
                $scope.invoicedateOpen = true;
            }
        }
        $scope.showPayPopup = false;
        $scope.showPaymentPopup = function ()
        {
            if ($scope.posModel.addedProducts.length > 0)
            {
                $scope.showPayPopup = true;
                $scope.showcustomerpopup = false;
                $scope.orderListModel.paymentList = [];
                $scope.orderListModel.payamt = $scope.orderListModel.round;
                $scope.getAccountlist();
            } else
            {
                sweet.show('Oops...', 'Select a product.', 'error');
            }
        };
        $scope.closePaymentPopup = function ()
        {
            $scope.showPayPopup = false;
        };
        $scope.showOptions = false;
        $scope.openOptions = function ()
        {
            $scope.showOptions = !$scope.showOptions;
        }

        $scope.showcustomerpurchasepopup = false;
        $scope.showCusPurPopup = function ()
        {
            $scope.showcustomerpurchasepopup = true;
            $scope.selectCus();
        }
        $scope.closeCusPurPopup = function ()
        {
            $scope.showcustomerpurchasepopup = false;
        }


        $scope.$on("updateSavedCustomerDetail", function (event, customerDetail)
        {
            if (typeof customerDetail === 'undefined')
                return;
            $scope.orderListModel.customerInfo = customerDetail;
            $scope.customerDetail = $scope.orderListModel.customerInfo;
            $scope.customername = $scope.orderListModel.customerInfo.fname;
            $scope.phone = $scope.orderListModel.customerInfo.phone;
            $scope.showcustomername = true;
            $scope.showcustomerpopup = false;
        });
        $scope.showAddScreen = false;
        $scope.showNewProductScreen = function ()
        {
            $scope.showAddScreen = true;
        }
        $scope.typeSelect = function (type)
        {
            $scope.buttontype = type;
            $scope.getProductList();
        }
        $scope.checkSaleType = function (prodid, invoiceid)
        {
            if ($scope.buttontype == 'sale')
            {
                $scope.addProduct(prodid, invoiceid);

            }
            if ($scope.buttontype == 'return')
            {
                $scope.addReturnProduct(prodid, invoiceid);

            }
        }
        $scope.productExist = false;
        $scope.exitedIndex = 0;
        $scope.addProduct = function (prodid, invoiceid)
        {
            $scope.productExist = false;
            if ($scope.posModel.addedProducts.length > 0)
            {
                for (var i = 0; i < $scope.posModel.addedProducts.length; i++)
                {
                    $scope.posModel.addedProducts[i].newprod = false;
                }
                for (var i = 0; i < $scope.posModel.addedProducts.length; i++)
                {

                    if ($scope.posModel.addedProducts[i].bcn_id != undefined
                            && $scope.posModel.addedProducts[i].bcn_id != null
                            && $scope.posModel.addedProducts[i].bcn_id != 0
                            && $scope.posModel.addedProducts[i].saletype == 'sale') //purchase based
                    {
                        if (invoiceid == $scope.posModel.addedProducts[i].bcn_id
                                && prodid == $scope.posModel.addedProducts[i].id && $scope.posModel.addedProducts[i].saletype == 'sale')
                        {
                            $scope.productExist = true;
                            $scope.exitedIndex = i;
                            $scope.stockqty = parseFloat($scope.posModel.addedProducts[i].qty_in_hand);
                        }
                    } else if (prodid == $scope.posModel.addedProducts[i].id && $scope.posModel.addedProducts[i].saletype == 'sale') // product based
                    {
                        $scope.productExist = true;
                        $scope.exitedIndex = i;
                        $scope.stockqty = $scope.posModel.addedProducts[i].qty_in_hand;
                    }
                }
                if ($scope.productExist)
                {
                    if (($scope.posModel.addedProducts[$scope.exitedIndex].qty + 1) > $scope.stockqty)
                    {
                        $scope.newExistedProduct = $scope.posModel.addedProducts[$scope.exitedIndex];
                        $scope.showPopup(2);
                    } else
                    {
                        $scope.posModel.addedProducts[$scope.exitedIndex].newprod = true;
                        $scope.posModel.addedProducts[$scope.exitedIndex].qty = parseFloat($scope.posModel.addedProducts[$scope.exitedIndex].qty) + 1;
                        $scope.posModel.addedProducts[$scope.exitedIndex].revisedSellingPrice = parseFloat($scope.posModel.addedProducts[$scope.exitedIndex].revisedSellingPrice);
                        $scope.calculatetotal();
                    }
                } else if (!$scope.productExist) //product not found in already added product
                {
                    for (var j = 0; j < $scope.posModel.productList.length; j++)
                    {
                        if ($scope.posModel.productList[j].bcn_id != undefined
                                && $scope.posModel.productList[j].bcn_id != null
                                && $scope.posModel.productList[j].bcn_id != 0) //purchase based
                        {
                            if (invoiceid == $scope.posModel.productList[j].bcn_id
                                    && prodid == $scope.posModel.productList[j].id)
                            {
                                $scope.posModel.productList[j].qty = 1;
                                $scope.posModel.productList[j].saletype = 'sale';
                                $scope.posModel.productList[j].newprod = true;
                                $scope.posModel.productList[j].applyDiscount = false;
                                $scope.posModel.productList[j].rowtotal = 0;
                                $scope.posModel.productList[j].newrowtotal = 0;
                                if ($scope.posModel.productList[j].custom_opt1_key == $rootScope.appConfig.invoice_item_custom1_label)
                                {
                                    $scope.posModel.productList[j].custom_opt1 = $scope.posModel.productList[j].custom_opt1;
                                }
                                if ($scope.posModel.productList[j].custom_opt2_key == $rootScope.appConfig.invoice_item_custom1_label)
                                {
                                    $scope.posModel.productList[j].custom_opt1 = $scope.posModel.productList[j].custom_opt2;
                                }
                                if ($scope.posModel.productList[j].custom_opt3_key == $rootScope.appConfig.invoice_item_custom1_label)
                                {
                                    $scope.posModel.productList[j].custom_opt1 = $scope.posModel.productList[j].custom_opt3;
                                }
                                if ($scope.posModel.productList[j].custom_opt4_key == $rootScope.appConfig.invoice_item_custom1_label)
                                {
                                    $scope.posModel.productList[j].custom_opt1 = $scope.posModel.productList[j].custom_opt4;
                                }
                                if ($scope.posModel.productList[j].custom_opt5_key == $rootScope.appConfig.invoice_item_custom1_label)
                                {
                                    $scope.posModel.productList[j].custom_opt1 = $scope.posModel.productList[j].custom_opt5;
                                }
                                if ($scope.posModel.productList[j].custom_opt1_key == $rootScope.appConfig.invoice_item_custom2_label)
                                {
                                    $scope.posModel.productList[j].custom_opt2 = $scope.posModel.productList[j].custom_opt1;
                                }
                                if ($scope.posModel.productList[j].custom_opt2_key == $rootScope.appConfig.invoice_item_custom2_label)
                                {
                                    $scope.posModel.productList[j].custom_opt2 = $scope.posModel.productList[j].custom_opt2;
                                }
                                if ($scope.posModel.productList[j].custom_opt3_key == $rootScope.appConfig.invoice_item_custom2_label)
                                {
                                    $scope.posModel.productList[j].custom_opt2 = $scope.posModel.productList[j].custom_opt3;
                                }
                                if ($scope.posModel.productList[j].custom_opt4_key == $rootScope.appConfig.invoice_item_custom2_label)
                                {
                                    $scope.posModel.productList[j].custom_opt2 = $scope.posModel.productList[j].custom_opt4;
                                }
                                if ($scope.posModel.productList[j].custom_opt5_key == $rootScope.appConfig.invoice_item_custom2_label)
                                {
                                    $scope.posModel.productList[j].custom_opt2 = $scope.posModel.productList[j].custom_opt5;
                                }
                                if ($scope.posModel.productList[j].custom_opt1_key == $rootScope.appConfig.invoice_item_custom3_label)
                                {
                                    $scope.posModel.productList[j].custom_opt3 = $scope.posModel.productList[j].custom_opt1;
                                }
                                if ($scope.posModel.productList[j].custom_opt2_key == $rootScope.appConfig.invoice_item_custom3_label)
                                {
                                    $scope.posModel.productList[j].custom_opt3 = $scope.posModel.productList[j].custom_opt2;
                                }
                                if ($scope.posModel.productList[j].custom_opt3_key == $rootScope.appConfig.invoice_item_custom3_label)
                                {
                                    $scope.posModel.productList[j].custom_opt3 = $scope.posModel.productList[j].custom_opt3;
                                }
                                if ($scope.posModel.productList[j].custom_opt4_key == $rootScope.appConfig.invoice_item_custom3_label)
                                {
                                    $scope.posModel.productList[j].custom_opt3 = $scope.posModel.productList[j].custom_opt4;
                                }
                                if ($scope.posModel.productList[j].custom_opt5_key == $rootScope.appConfig.invoice_item_custom3_label)
                                {
                                    $scope.posModel.productList[j].custom_opt3 = $scope.posModel.productList[j].custom_opt5;
                                }
                                if ($scope.posModel.productList[j].custom_opt1_key == $rootScope.appConfig.invoice_item_custom4_label)
                                {
                                    $scope.posModel.productList[j].custom_opt4 = $scope.posModel.productList[j].custom_opt1;
                                }
                                if ($scope.posModel.productList[j].custom_opt2_key == $rootScope.appConfig.invoice_item_custom4_label)
                                {
                                    $scope.posModel.productList[j].custom_opt4 = $scope.posModel.productList[j].custom_opt2;
                                }
                                if ($scope.posModel.productList[j].custom_opt3_key == $rootScope.appConfig.invoice_item_custom4_label)
                                {
                                    $scope.posModel.productList[j].custom_opt4 = $scope.posModel.productList[j].custom_opt3;
                                }
                                if ($scope.posModel.productList[j].custom_opt4_key == $rootScope.appConfig.invoice_item_custom4_label)
                                {
                                    $scope.posModel.productList[j].custom_opt4 = $scope.posModel.productList[j].custom_opt4;
                                }
                                if ($scope.posModel.productList[j].custom_opt5_key == $rootScope.appConfig.invoice_item_custom4_label)
                                {
                                    $scope.posModel.productList[j].custom_opt4 = $scope.posModel.productList[j].custom_opt5;
                                }
                                if ($scope.posModel.productList[j].custom_opt1_key == $rootScope.appConfig.invoice_item_custom5_label)
                                {
                                    $scope.posModel.productList[j].custom_opt5 = $scope.posModel.productList[j].custom_opt1;
                                }
                                if ($scope.posModel.productList[j].custom_opt2_key == $rootScope.appConfig.invoice_item_custom4_label)
                                {
                                    $scope.posModel.productList[j].custom_opt5 = $scope.posModel.productList[j].custom_opt5;
                                }
                                if ($scope.posModel.productList[j].custom_opt3_key == $rootScope.appConfig.invoice_item_custom4_label)
                                {
                                    $scope.posModel.productList[j].custom_opt5 = $scope.posModel.productList[j].custom_opt5;
                                }
                                if ($scope.posModel.productList[j].custom_opt4_key == $rootScope.appConfig.invoice_item_custom5_label)
                                {
                                    $scope.posModel.productList[j].custom_opt5 = $scope.posModel.productList[j].custom_opt4;
                                }
                                if ($scope.posModel.productList[j].custom_opt5_key == $rootScope.appConfig.invoice_item_custom5_label)
                                {
                                    $scope.posModel.productList[j].custom_opt5 = $scope.posModel.productList[j].custom_opt5;
                                }
                                $scope.posModel.productList[j].revisedSellingPrice = $scope.posModel.productList[j].selling_price;
                                if ($scope.posModel.productList[j].defaultSellingPrice != undefined)
                                {
                                    $scope.posModel.productList[j].defaultSellingPrice = $scope.posModel.productList[j].defaultSellingPrice;
                                } else
                                {
                                    $scope.posModel.productList[j].defaultSellingPrice = $scope.posModel.productList[j].mrp_price;
                                }
                                $scope.posModel.productList[j].hsn_code = $scope.posModel.productList[j].hsn_code;
                                if ($scope.posModel.productList[j].qty_in_hand < 0)
                                {
                                    $scope.newProduct = $scope.posModel.productList[j];
                                    $scope.showPopup(1);
                                    $timeout($scope.updateAddProductCss, 100);
                                } else
                                {
                                    $scope.posModel.addedProducts.push($scope.posModel.productList[j]);
                                    $timeout(function () {
                                        var newNextFieldId = 'empucode_' + ($scope.posModel.addedProducts.length - 1);
                                        $("#" + newNextFieldId).focus();
                                    }, 100);
                                    //   $scope.posModel.addedProducts.reverse();
                                    $scope.updateAddProductCss();
                                    $scope.calculatetotal();
                                }
                                return;
                            }
                        }
                    }
                }

            } else
            {
                if (invoiceid != undefined &&
                        invoiceid != null &&
                        invoiceid != 0) //purchase based
                {
                    for (var i = 0; i < $scope.posModel.productList.length; i++)
                    {
                        if (invoiceid == $scope.posModel.productList[i].bcn_id
                                && prodid == $scope.posModel.productList[i].id)
                        {
                            if (parseFloat($scope.posModel.productList[i].qty_in_hand) >= 1)
                            {
                                $scope.posModel.productList[i].qty = 1;
                                $scope.posModel.productList[i].saletype = 'sale';
                                $scope.posModel.productList[i].newprod = true;
                                $scope.posModel.productList[i].applyDiscount = false;
                                $scope.posModel.productList[i].rowtotal = 0;
                                $scope.posModel.productList[i].newrowtotal = 0;
                                $scope.posModel.productList[i].discAmt = 0;
                                $scope.posModel.productList[i].discountPercentage = 0;
                                 $scope.posModel.productList[i].hsn_code = $scope.posModel.productList[i].hsn_code;
                                if ($scope.posModel.productList[i].custom_opt1_key == $rootScope.appConfig.invoice_item_custom1_label)
                                {
                                    $scope.posModel.productList[i].custom_opt1 = $scope.posModel.productList[i].custom_opt1;
                                }
                                if ($scope.posModel.productList[i].custom_opt2_key == $rootScope.appConfig.invoice_item_custom1_label)
                                {
                                    $scope.posModel.productList[i].custom_opt1 = $scope.posModel.productList[i].custom_opt2;
                                }
                                if ($scope.posModel.productList[i].custom_opt3_key == $rootScope.appConfig.invoice_item_custom1_label)
                                {
                                    $scope.posModel.productList[i].custom_opt1 = $scope.posModel.productList[i].custom_opt3;
                                }
                                if ($scope.posModel.productList[i].custom_opt4_key == $rootScope.appConfig.invoice_item_custom1_label)
                                {
                                    $scope.posModel.productList[i].custom_opt1 = $scope.posModel.productList[i].custom_opt4;
                                }
                                if ($scope.posModel.productList[i].custom_opt5_key == $rootScope.appConfig.invoice_item_custom1_label)
                                {
                                    $scope.posModel.productList[i].custom_opt1 = $scope.posModel.productList[i].custom_opt5;
                                }
                                if ($scope.posModel.productList[i].custom_opt1_key == $rootScope.appConfig.invoice_item_custom2_label)
                                {
                                    $scope.posModel.productList[i].custom_opt2 = $scope.posModel.productList[i].custom_opt1;
                                }
                                if ($scope.posModel.productList[i].custom_opt2_key == $rootScope.appConfig.invoice_item_custom2_label)
                                {
                                    $scope.posModel.productList[i].custom_opt2 = $scope.posModel.productList[i].custom_opt2;
                                }
                                if ($scope.posModel.productList[i].custom_opt3_key == $rootScope.appConfig.invoice_item_custom2_label)
                                {
                                    $scope.posModel.productList[i].custom_opt2 = $scope.posModel.productList[i].custom_opt3;
                                }
                                if ($scope.posModel.productList[i].custom_opt4_key == $rootScope.appConfig.invoice_item_custom2_label)
                                {
                                    $scope.posModel.productList[i].custom_opt2 = $scope.posModel.productList[i].custom_opt4;
                                }
                                if ($scope.posModel.productList[i].custom_opt5_key == $rootScope.appConfig.invoice_item_custom2_label)
                                {
                                    $scope.posModel.productList[i].custom_opt2 = $scope.posModel.productList[i].custom_opt5;
                                }
                                if ($scope.posModel.productList[i].custom_opt1_key == $rootScope.appConfig.invoice_item_custom3_label)
                                {
                                    $scope.posModel.productList[i].custom_opt3 = $scope.posModel.productList[i].custom_opt1;
                                }
                                if ($scope.posModel.productList[i].custom_opt2_key == $rootScope.appConfig.invoice_item_custom3_label)
                                {
                                    $scope.posModel.productList[i].custom_opt3 = $scope.posModel.productList[i].custom_opt2;
                                }
                                if ($scope.posModel.productList[i].custom_opt3_key == $rootScope.appConfig.invoice_item_custom3_label)
                                {
                                    $scope.posModel.productList[i].custom_opt3 = $scope.posModel.productList[i].custom_opt3;
                                }
                                if ($scope.posModel.productList[i].custom_opt4_key == $rootScope.appConfig.invoice_item_custom3_label)
                                {
                                    $scope.posModel.productList[i].custom_opt3 = $scope.posModel.productList[i].custom_opt4;
                                }
                                if ($scope.posModel.productList[i].custom_opt5_key == $rootScope.appConfig.invoice_item_custom3_label)
                                {
                                    $scope.posModel.productList[i].custom_opt3 = $scope.posModel.productList[i].custom_opt5;
                                }
                                if ($scope.posModel.productList[i].custom_opt1_key == $rootScope.appConfig.invoice_item_custom4_label)
                                {
                                    $scope.posModel.productList[i].custom_opt4 = $scope.posModel.productList[i].custom_opt1;
                                }
                                if ($scope.posModel.productList[i].custom_opt2_key == $rootScope.appConfig.invoice_item_custom4_label)
                                {
                                    $scope.posModel.productList[i].custom_opt4 = $scope.posModel.productList[i].custom_opt2;
                                }
                                if ($scope.posModel.productList[i].custom_opt3_key == $rootScope.appConfig.invoice_item_custom4_label)
                                {
                                    $scope.posModel.productList[i].custom_opt4 = $scope.posModel.productList[i].custom_opt3;
                                }
                                if ($scope.posModel.productList[i].custom_opt4_key == $rootScope.appConfig.invoice_item_custom4_label)
                                {
                                    $scope.posModel.productList[i].custom_opt4 = $scope.posModel.productList[i].custom_opt4;
                                }
                                if ($scope.posModel.productList[i].custom_opt5_key == $rootScope.appConfig.invoice_item_custom4_label)
                                {
                                    $scope.posModel.productList[i].custom_opt4 = $scope.posModel.productList[i].custom_opt5;
                                }
                                if ($scope.posModel.productList[i].custom_opt1_key == $rootScope.appConfig.invoice_item_custom5_label)
                                {
                                    $scope.posModel.productList[i].custom_opt5 = $scope.posModel.productList[i].custom_opt1;
                                }
                                if ($scope.posModel.productList[i].custom_opt2_key == $rootScope.appConfig.invoice_item_custom4_label)
                                {
                                    $scope.posModel.productList[i].custom_opt5 = $scope.posModel.productList[i].custom_opt5;
                                }
                                if ($scope.posModel.productList[i].custom_opt3_key == $rootScope.appConfig.invoice_item_custom4_label)
                                {
                                    $scope.posModel.productList[i].custom_opt5 = $scope.posModel.productList[i].custom_opt5;
                                }
                                if ($scope.posModel.productList[i].custom_opt4_key == $rootScope.appConfig.invoice_item_custom5_label)
                                {
                                    $scope.posModel.productList[i].custom_opt5 = $scope.posModel.productList[i].custom_opt4;
                                }
                                if ($scope.posModel.productList[i].custom_opt5_key == $rootScope.appConfig.invoice_item_custom5_label)
                                {
                                    $scope.posModel.productList[i].custom_opt5 = $scope.posModel.productList[i].custom_opt5;
                                }
                                $scope.posModel.productList[i].revisedSellingPrice = $scope.posModel.productList[i].selling_price;
                                if ($scope.posModel.productList[i].defaultSellingPrice != undefined)
                                {
                                    $scope.posModel.productList[i].defaultSellingPrice = $scope.posModel.productList[i].defaultSellingPrice;
                                } else
                                {
                                    $scope.posModel.productList[i].defaultSellingPrice = $scope.posModel.productList[i].mrp_price;
                                }
                                $scope.posModel.addedProducts.push($scope.posModel.productList[i]);
                                $timeout(function () {
                                    var newNextFieldId = 'empucode_' + ($scope.posModel.addedProducts.length - 1);
                                    $("#" + newNextFieldId).focus();
                                }, 100);
                                // $scope.posModel.addedProducts.reverse();
                                $scope.updateAddProductCss();
                                $scope.calculatetotal();
                                break;
                            } else
                            {
                                $scope.newProduct = $scope.posModel.productList[i];
                                if ($scope.posModel.productList[i].defaultSellingPrice != undefined)
                                {
                                    $scope.newProduct.defaultSellingPrice = $scope.posModel.productList[i].defaultSellingPrice;
                                } else
                                {
                                    $scope.newProduct.defaultSellingPrice = $scope.posModel.productList[i].mrp_price;
                                }
                                $scope.newProduct.revisedSellingPrice = $scope.posModel.productList[i].selling_price;
                                $scope.showPopup(1);
                                $scope.updateAddProductCss();
                            }
                        }
                    }
                } else //product based
                {
                    for (var i = 0; i < $scope.posModel.productList.length; i++)
                    {
                        if (prodid == $scope.posModel.productList[i].id)
                        {
                            var taxPercentage = 0;
                            var taxAmount = 0;
                            if (parseFloat($scope.posModel.productList[i].qty_in_hand) >= 1)
                            {
                                $scope.posModel.productList[i].qty = 1;
                                $scope.posModel.productList[i].saletype = 'sale';
                                $scope.posModel.productList[i].newprod = true;
                                $scope.posModel.productList[i].applyDiscount = false;
                                $scope.posModel.productList[i].rowtotal = 0;
                                $scope.posModel.productList[i].newrowtotal = 0;
                                $scope.posModel.productList[i].discAmt = 0;
                                $scope.posModel.productList[i].discountPercentage = 0;
                                 $scope.posModel.productList[i].hsn_code = $scope.posModel.productList[i].hsn_code;
                                if ($scope.posModel.productList[i].custom_opt1_key == $rootScope.appConfig.invoice_item_custom1_label)
                                {
                                    $scope.posModel.productList[i].custom_opt1 = $scope.posModel.productList[i].custom_opt1;
                                }
                                if ($scope.posModel.productList[i].custom_opt2_key == $rootScope.appConfig.invoice_item_custom1_label)
                                {
                                    $scope.posModel.productList[i].custom_opt1 = $scope.posModel.productList[i].custom_opt2;
                                }
                                if ($scope.posModel.productList[i].custom_opt3_key == $rootScope.appConfig.invoice_item_custom1_label)
                                {
                                    $scope.posModel.productList[i].custom_opt1 = $scope.posModel.productList[i].custom_opt3;
                                }
                                if ($scope.posModel.productList[i].custom_opt4_key == $rootScope.appConfig.invoice_item_custom1_label)
                                {
                                    $scope.posModel.productList[i].custom_opt1 = $scope.posModel.productList[i].custom_opt4;
                                }
                                if ($scope.posModel.productList[i].custom_opt5_key == $rootScope.appConfig.invoice_item_custom1_label)
                                {
                                    $scope.posModel.productList[i].custom_opt1 = $scope.posModel.productList[i].custom_opt5;
                                }
                                if ($scope.posModel.productList[i].custom_opt1_key == $rootScope.appConfig.invoice_item_custom2_label)
                                {
                                    $scope.posModel.productList[i].custom_opt2 = $scope.posModel.productList[i].custom_opt1;
                                }
                                if ($scope.posModel.productList[i].custom_opt2_key == $rootScope.appConfig.invoice_item_custom2_label)
                                {
                                    $scope.posModel.productList[i].custom_opt2 = $scope.posModel.productList[i].custom_opt2;
                                }
                                if ($scope.posModel.productList[i].custom_opt3_key == $rootScope.appConfig.invoice_item_custom2_label)
                                {
                                    $scope.posModel.productList[i].custom_opt2 = $scope.posModel.productList[i].custom_opt3;
                                }
                                if ($scope.posModel.productList[i].custom_opt4_key == $rootScope.appConfig.invoice_item_custom2_label)
                                {
                                    $scope.posModel.productList[i].custom_opt2 = $scope.posModel.productList[i].custom_opt4;
                                }
                                if ($scope.posModel.productList[i].custom_opt5_key == $rootScope.appConfig.invoice_item_custom2_label)
                                {
                                    $scope.posModel.productList[i].custom_opt2 = $scope.posModel.productList[i].custom_opt5;
                                }
                                if ($scope.posModel.productList[i].custom_opt1_key == $rootScope.appConfig.invoice_item_custom3_label)
                                {
                                    $scope.posModel.productList[i].custom_opt3 = $scope.posModel.productList[i].custom_opt1;
                                }
                                if ($scope.posModel.productList[i].custom_opt2_key == $rootScope.appConfig.invoice_item_custom3_label)
                                {
                                    $scope.posModel.productList[i].custom_opt3 = $scope.posModel.productList[i].custom_opt2;
                                }
                                if ($scope.posModel.productList[i].custom_opt3_key == $rootScope.appConfig.invoice_item_custom3_label)
                                {
                                    $scope.posModel.productList[i].custom_opt3 = $scope.posModel.productList[i].custom_opt3;
                                }
                                if ($scope.posModel.productList[i].custom_opt4_key == $rootScope.appConfig.invoice_item_custom3_label)
                                {
                                    $scope.posModel.productList[i].custom_opt3 = $scope.posModel.productList[i].custom_opt4;
                                }
                                if ($scope.posModel.productList[i].custom_opt5_key == $rootScope.appConfig.invoice_item_custom3_label)
                                {
                                    $scope.posModel.productList[i].custom_opt3 = $scope.posModel.productList[i].custom_opt5;
                                }
                                if ($scope.posModel.productList[i].custom_opt1_key == $rootScope.appConfig.invoice_item_custom4_label)
                                {
                                    $scope.posModel.productList[i].custom_opt4 = $scope.posModel.productList[i].custom_opt1;
                                }
                                if ($scope.posModel.productList[i].custom_opt2_key == $rootScope.appConfig.invoice_item_custom4_label)
                                {
                                    $scope.posModel.productList[i].custom_opt4 = $scope.posModel.productList[i].custom_opt2;
                                }
                                if ($scope.posModel.productList[i].custom_opt3_key == $rootScope.appConfig.invoice_item_custom4_label)
                                {
                                    $scope.posModel.productList[i].custom_opt4 = $scope.posModel.productList[i].custom_opt3;
                                }
                                if ($scope.posModel.productList[i].custom_opt4_key == $rootScope.appConfig.invoice_item_custom4_label)
                                {
                                    $scope.posModel.productList[i].custom_opt4 = $scope.posModel.productList[i].custom_opt4;
                                }
                                if ($scope.posModel.productList[i].custom_opt5_key == $rootScope.appConfig.invoice_item_custom4_label)
                                {
                                    $scope.posModel.productList[i].custom_opt4 = $scope.posModel.productList[i].custom_opt5;
                                }
                                if ($scope.posModel.productList[i].custom_opt1_key == $rootScope.appConfig.invoice_item_custom5_label)
                                {
                                    $scope.posModel.productList[i].custom_opt5 = $scope.posModel.productList[i].custom_opt1;
                                }
                                if ($scope.posModel.productList[i].custom_opt2_key == $rootScope.appConfig.invoice_item_custom4_label)
                                {
                                    $scope.posModel.productList[i].custom_opt5 = $scope.posModel.productList[i].custom_opt5;
                                }
                                if ($scope.posModel.productList[i].custom_opt3_key == $rootScope.appConfig.invoice_item_custom4_label)
                                {
                                    $scope.posModel.productList[i].custom_opt5 = $scope.posModel.productList[i].custom_opt5;
                                }
                                if ($scope.posModel.productList[i].custom_opt4_key == $rootScope.appConfig.invoice_item_custom5_label)
                                {
                                    $scope.posModel.productList[i].custom_opt5 = $scope.posModel.productList[i].custom_opt4;
                                }
                                if ($scope.posModel.productList[i].custom_opt5_key == $rootScope.appConfig.invoice_item_custom5_label)
                                {
                                    $scope.posModel.productList[i].custom_opt5 = $scope.posModel.productList[i].custom_opt5;
                                }
                                if ($scope.posModel.productList[i].defaultSellingPrice != undefined)
                                {
                                    $scope.posModel.productList[i].defaultSellingPrice = $scope.posModel.productList[i].defaultSellingPrice;
                                } else
                                {
                                    $scope.posModel.productList[i].defaultSellingPrice = $scope.posModel.productList[i].sales_price;
                                }
                                $scope.posModel.productList[i].revisedSellingPrice = parseFloat($scope.posModel.productList[i].defaultSellingPrice);
                                $scope.posModel.addedProducts.push($scope.posModel.productList[i]);
                                $timeout(function () {
                                    var newNextFieldId = '#empucode_' + ($scope.posModel.addedProducts.length - 1);
                                    $("#" + newNextFieldId).focus();
                                }, 100);
//                                $('#empucode_' + $scope.posModel.addedProducts.length - 1).focus();
                                // $scope.posModel.addedProducts.reverse();
                                $scope.calculatetotal();
                            } else
                            {
                                $scope.newProduct = $scope.posModel.productList[i];
                                if ($scope.posModel.productList[i].defaultSellingPrice != undefined)
                                {
                                    $scope.newProduct.defaultSellingPrice = $scope.posModel.productList[i].defaultSellingPrice;
                                } else
                                {
                                    $scope.newProduct.defaultSellingPrice = $scope.posModel.productList[i].selling_price;
                                }
                                $scope.posModel.productList[i].revisedSellingPrice = parseFloat($scope.posModel.productList[i].defaultSellingPrice);
                                $scope.showPopup(1);
                            }
                        }
                    }
                }

            }

            $timeout($scope.updateAddProductCss, 100);

        }

        $scope.productReturnExist = false;
        $scope.exitedReturnIndex = 0;
        $scope.addReturnProduct = function (prodid, invoiceid)
        {
            $scope.productReturnExist = false;
            if ($scope.posModel.addedProducts.length > 0)
            {

                for (var i = 0; i < $scope.posModel.addedProducts.length; i++)
                {
                    $scope.posModel.addedProducts[i].newprod = false;
                }
                for (var i = 0; i < $scope.posModel.addedProducts.length; i++)
                {

                    if ($scope.posModel.addedProducts[i].bcn_id != undefined
                            && $scope.posModel.addedProducts[i].bcn_id != null
                            && $scope.posModel.addedProducts[i].bcn_id != 0
                            && $scope.posModel.addedProducts[i].saletype == 'return') //purchase based
                    {
                        if (invoiceid == $scope.posModel.addedProducts[i].bcn_id
                                && prodid == $scope.posModel.addedProducts[i].id && $scope.posModel.addedProducts[i].saletype == 'return')
                        {
                            $scope.productReturnExist = true;
                            $scope.exitedReturnIndex = i;
                            $scope.stockqty = parseFloat($scope.posModel.addedProducts[i].qty_in_hand);
                        }
                    } else if (prodid == $scope.posModel.addedProducts[i].id && $scope.posModel.addedProducts[i].saletype == 'return') // product based
                    {
                        $scope.productReturnExist = true;
                        $scope.exitedReturnIndex = i;
                        $scope.stockqty = $scope.posModel.addedProducts[i].qty_in_hand;
                    }
                }
                if ($scope.productReturnExist)
                {

                    if ($scope.buttontype == 'return')
                    {
                        if (($scope.posModel.addedProducts[$scope.exitedReturnIndex].qty - 1) > $scope.stockqty)
                        {
                            $scope.newExistedProduct = $scope.posModel.addedProducts[$scope.exitedReturnIndex];
                            $scope.showPopup(2);
                        } else
                        {
                            $scope.posModel.addedProducts[$scope.exitedReturnIndex].newprod = true;
                            $scope.posModel.addedProducts[$scope.exitedReturnIndex].qty = parseFloat($scope.posModel.addedProducts[$scope.exitedReturnIndex].qty) - 1;
                            $scope.posModel.addedProducts[$scope.exitedReturnIndex].revisedSellingPrice = parseFloat($scope.posModel.addedProducts[$scope.exitedReturnIndex].revisedSellingPrice);
                            $scope.calculatetotal();
                        }
                    }
                } else if (!$scope.productReturnExist) //product not found in already added product
                {
                    for (var j = 0; j < $scope.posModel.productList.length; j++)
                    {
                        if ($scope.posModel.productList[j].bcn_id != undefined
                                && $scope.posModel.productList[j].bcn_id != null
                                && $scope.posModel.productList[j].bcn_id != 0) //purchase based
                        {
                            if (invoiceid == $scope.posModel.productList[j].bcn_id
                                    && prodid == $scope.posModel.productList[j].id)
                            {
                                $scope.posModel.productList[j].qty = -1;
                                $scope.posModel.productList[j].saletype = 'return';
                                $scope.posModel.productList[j].newprod = true;
                                $scope.posModel.productList[j].applyDiscount = false;
                                $scope.posModel.productList[j].rowtotal = 0;
                                $scope.posModel.productList[j].newrowtotal = 0;
                                $scope.posModel.productList[j].revisedSellingPrice = $scope.posModel.productList[j].selling_price;
                                if ($scope.posModel.productList[j].defaultSellingPrice != undefined)
                                {
                                    $scope.posModel.productList[j].defaultSellingPrice = $scope.posModel.productList[j].defaultSellingPrice;
                                } else
                                {
                                    $scope.posModel.productList[j].defaultSellingPrice = $scope.posModel.productList[j].mrp_price;
                                }
                                if ($scope.posModel.productList[j].qty_in_hand < 0)
                                {
                                    $scope.newProduct = $scope.posModel.productList[j];
                                    $scope.showPopup(1);
                                    $timeout($scope.updateAddProductCss, 100);
                                } else
                                {
                                    $scope.posModel.addedProducts.push($scope.posModel.productList[j]);
                                    $timeout(function () {
                                        var newNextFieldId = 'empucode_' + ($scope.posModel.addedProducts.length - 1);
                                        $("#" + newNextFieldId).focus();
                                    }, 100);
                                    // $scope.posModel.addedProducts.reverse();
                                    $scope.updateAddProductCss();
                                    $scope.calculatetotal();
                                }
                                return;
                            }
                        } else if (prodid == $scope.posModel.productList[j].id) //product based
                        {
                            $scope.posModel.productList[j].qty = -1;
                            $scope.posModel.productList[j].saletype = 'return';
                            $scope.posModel.productList[j].newprod = true;
                            $scope.posModel.productList[j].applyDiscount = false;
                            $scope.posModel.productList[j].rowtotal = 0;
                            $scope.posModel.productList[j].newrowtotal = 0;
                            $scope.posModel.productList[j].revisedSellingPrice = $scope.posModel.productList[j].selling_price;
                            if ($scope.posModel.productList[j].defaultSellingPrice != undefined)
                            {
                                $scope.posModel.productList[j].defaultSellingPrice = $scope.posModel.productList[j].defaultSellingPrice;
                            } else
                            {
                                $scope.posModel.productList[j].defaultSellingPrice = $scope.posModel.productList[j].selling_price;
                            }
                            if ($scope.posModel.productList[j].qty_in_hand < 0)
                            {
                                $scope.newProduct = $scope.posModel.productList[j];
                                $scope.showPopup(1);
                                $timeout($scope.updateAddProductCss, 100);
                            } else
                            {
                                $scope.posModel.addedProducts.push($scope.posModel.productList[j]);
                                $timeout(function () {
                                    var newNextFieldId = 'empucode_' + ($scope.posModel.addedProducts.length - 1);
                                    $("#" + newNextFieldId).focus();
                                }, 100);
                                //   $scope.posModel.addedProducts.reverse();
                                $scope.updateAddProductCss();
                                $scope.calculatetotal();
                            }
                            return;
                        }
                    }
                }

            } else
            {
                if (invoiceid != undefined &&
                        invoiceid != null &&
                        invoiceid != 0) //purchase based
                {
                    for (var i = 0; i < $scope.posModel.productList.length; i++)
                    {
                        if (invoiceid == $scope.posModel.productList[i].bcn_id
                                && prodid == $scope.posModel.productList[i].id)
                        {
                            if (parseFloat($scope.posModel.productList[i].qty_in_hand) >= 1)
                            {
                                $scope.posModel.productList[i].qty = -1;
                                $scope.posModel.productList[i].saletype = 'return';
                                $scope.posModel.productList[i].newprod = true;
                                $scope.posModel.productList[i].applyDiscount = false;
                                $scope.posModel.productList[i].rowtotal = 0;
                                $scope.posModel.productList[i].newrowtotal = 0;
                                $scope.posModel.productList[i].discAmt = 0;
                                $scope.posModel.productList[i].discountPercentage = 0;
                                $scope.posModel.productList[i].revisedSellingPrice = $scope.posModel.productList[i].selling_price;
                                if ($scope.posModel.productList[i].defaultSellingPrice != undefined)
                                {
                                    $scope.posModel.productList[i].defaultSellingPrice = $scope.posModel.productList[i].defaultSellingPrice;
                                } else
                                {
                                    $scope.posModel.productList[i].defaultSellingPrice = $scope.posModel.productList[i].mrp_price;
                                }
                                $scope.posModel.addedProducts.push($scope.posModel.productList[i]);
                                $timeout(function () {
                                    var newNextFieldId = 'empucode_' + ($scope.posModel.addedProducts.length - 1);
                                    $("#" + newNextFieldId).focus();
                                }, 100);
                                // $scope.posModel.addedProducts.reverse();
                                $scope.updateAddProductCss();
                                $scope.calculatetotal();
                                break;
                            } else
                            {
                                $scope.newProduct = $scope.posModel.productList[i];
                                if ($scope.posModel.productList[i].defaultSellingPrice != undefined)
                                {
                                    $scope.newProduct.defaultSellingPrice = $scope.posModel.productList[i].defaultSellingPrice;
                                } else
                                {
                                    $scope.newProduct.defaultSellingPrice = $scope.posModel.productList[i].mrp_price;
                                }
                                $scope.newProduct.revisedSellingPrice = $scope.posModel.productList[i].selling_price;
                                $scope.showPopup(1);
                                $scope.updateAddProductCss();
                            }
                        }
                    }
                } else //product based
                {
                    for (var i = 0; i < $scope.posModel.productList.length; i++)
                    {
                        if (prodid == $scope.posModel.productList[i].id)
                        {
                            var taxPercentage = 0;
                            var taxAmount = 0;
                            if (parseFloat($scope.posModel.productList[i].qty_in_hand) >= 1)
                            {
                                $scope.posModel.productList[i].qty = -1;
                                $scope.posModel.productList[i].saletype = 'return';
                                $scope.posModel.productList[i].newprod = true;
                                $scope.posModel.productList[i].applyDiscount = false;
                                $scope.posModel.productList[i].rowtotal = 0;
                                $scope.posModel.productList[i].newrowtotal = 0;
                                $scope.posModel.productList[i].discAmt = 0;
                                $scope.posModel.productList[i].discountPercentage = 0;
                                if ($scope.posModel.productList[i].defaultSellingPrice != undefined)
                                {
                                    $scope.posModel.productList[i].defaultSellingPrice = $scope.posModel.productList[i].defaultSellingPrice;
                                } else
                                {
                                    $scope.posModel.productList[i].defaultSellingPrice = $scope.posModel.productList[i].selling_price;
                                }
                                $scope.posModel.productList[i].revisedSellingPrice = parseFloat($scope.posModel.productList[i].defaultSellingPrice);
                                $scope.posModel.addedProducts.push($scope.posModel.productList[i]);
                                $timeout(function () {
                                    var newNextFieldId = '#empucode_' + ($scope.posModel.addedProducts.length - 1);
                                    $("#" + newNextFieldId).focus();
                                }, 100);
//                                $('#empucode_' + $scope.posModel.addedProducts.length - 1).focus();
                                // $scope.posModel.addedProducts.reverse();
                                $scope.calculatetotal();
                            } else
                            {
                                $scope.newProduct = $scope.posModel.productList[i];
                                if ($scope.posModel.productList[i].defaultSellingPrice != undefined)
                                {
                                    $scope.newProduct.defaultSellingPrice = $scope.posModel.productList[i].defaultSellingPrice;
                                } else
                                {
                                    $scope.newProduct.defaultSellingPrice = $scope.posModel.productList[i].selling_price;
                                }
                                $scope.posModel.productList[i].revisedSellingPrice = parseFloat($scope.posModel.productList[i].defaultSellingPrice);
                                $scope.showPopup(1);
                            }
                        }
                    }
                }

            }

            $timeout($scope.updateAddProductCss, 100);
        }

        $scope.updateAddProductCss = function ()
        {
            $scope.posModel.newprodqty = '';
            $scope.posModel.newprodqty = '';
            $scope.posModel.newproddiscount = '0';
            $scope.posModel.newprodnote = '';
            $scope.posModel.newprodprice = '';
            $scope.posModel.defaultSellingPrice = '';
            $scope.posModel.newproductname = '';
            $scope.posModel.tierPricing = [];
            $scope.posModel.taxMapping = [];
            $timeout(function () {
                for (var i = 0; i < $scope.posModel.addedProducts.length; i++)
                {
                    $scope.posModel.addedProducts[i].cssClass = "pro-added";
                }
            }, 0);
        }
        $scope.opendayclosepopup = false;
        $scope.openDayClosePopup = function ()
        {
            $scope.showOptions = false;
            $scope.opendayclosepopup = true;
            $scope.findCurrentDaycode();
            $scope.findDayCloseCredit();
        }
        $scope.closeDayClosePopup = function ()
        {
            $scope.opendayclosepopup = false;
            $scope.posModel.ttcount = 0;
            $scope.posModel.ttcount1 = 0;
            $scope.posModel.ttcount2 = 0;
            $scope.posModel.ttcount3 = 0;
            $scope.posModel.ttcount4 = 0;
            $scope.posModel.ttcount5 = 0;
            $scope.posModel.ttcount6 = 0;
            $scope.posModel.ttcount7 = 0;
            $scope.posModel.ttcount8 = 0;
            $scope.posModel.ttamount = '0.00';
            $scope.posModel.ttamount1 = '0.00';
            $scope.posModel.ttamount2 = '0.00';
            $scope.posModel.ttamount3 = '0.00';
            $scope.posModel.ttamount4 = '0.00';
            $scope.posModel.ttamount5 = '0.00';
            $scope.posModel.ttamount6 = '0.00';
            $scope.posModel.ttamount7 = '0.00';
            $scope.posModel.ttamount8 = '0.00';
            $scope.posModel.cashamount = '0.00';
            $scope.posModel.cardamount = '0.00';
            $scope.posModel.credit = '0.00';
            $scope.posModel.others = '0.00';
            $scope.posModel.advance = '0.00';

        }

        $scope.removeProduct = function ($index)
        {
            $scope.posModel.addedProducts.splice($index, 1);
            $scope.showProduct = false;
            $scope.allproductsDiscount = false;
            $scope.selectedProduct.empCode = '';
            $scope.calculatetotal();
        }
        $scope.newExistedProduct = '';
        $scope.showStockCheckPopup = false;
        $scope.showStockCheckPopup1 = false;
        $scope.showStockCheckPopup2 = false;
        $scope.showAdvancePopup = false;
        $scope.showPopup = function (id) {
            if (id == 1)
            {
                $scope.showStockCheckPopup = true;
            } else if (id == 2)
            {
                $scope.showStockCheckPopup1 = true;
            } else if (id == 3)
            {
                $scope.showStockCheckPopup2 = true;
            } else if (id == 4)
            {
                $scope.showAdvancePopup = true;
            }
        };
        $scope.closePopup = function () {

            $scope.showStockCheckPopup = false;
            $scope.showStockCheckPopup1 = false;
            $scope.showStockCheckPopup2 = false;
            $scope.showAdvancePopup = false;
        };
        $scope.updateChProDetails = function ()
        {
            if ($scope.indexInAP >= 0)
            {
                $scope.posModel.addedProducts[$scope.indexInAP].revisedSellingPrice = $scope.selectedNewProduct.newRevisedSellingPrice;
                $scope.posModel.addedProducts[$scope.indexInAP].qty = $scope.selectedNewProduct.prodnewqty;
                $scope.posModel.addedProducts[$scope.indexInAP].discountPercentage = $scope.selectedNewProduct.prodnewdiscount;
                $scope.posModel.addedProducts[$scope.indexInAP].empCode = $scope.selectedNewProduct.empCode;
                $scope.posModel.addedProducts[$scope.indexInAP].description = $scope.selectedNewProduct.prodnewdesc;
                $scope.posModel.addedProducts[$scope.indexInAP].rowtotal = parseFloat($scope.selectedNewProduct.prodnewtotal);
                $timeout(function () {
                    var newNextFieldId = 'empucode_' + ($scope.posModel.addedProducts.length - 1);
                    $("#" + newNextFieldId).focus();
                }, 100);
                $scope.calculatetotal();
                $scope.closePopup();

            }
        }

        $scope.updateNewSelectedProduct = function ()
        {
            if ($scope.newExistedProduct != '')
            {
                $scope.newExistedProduct.newprod = true;
                $scope.newExistedProduct.qty = parseFloat($scope.newExistedProduct.qty) + 1;
                $scope.newExistedProduct.revisedSellingPrice = parseFloat($scope.newExistedProduct.revisedSellingPrice);
                $scope.calculatetotal();
                $scope.newExistedProduct = '';
                $scope.closePopup();
                $timeout(function () {
                    var newNextFieldId = 'empucode_' + ($scope.posModel.addedProducts.length - 1);
                    $("#" + newNextFieldId).focus();
                }, 100);

            }
        }

        $scope.addNewSelectedProduct = function ()
        {
            if ($scope.newProduct != '')
            {
                $scope.newProduct.qty = 1;
                $scope.newProduct.newprod = true;
                $scope.newProduct.rowtotal = 0;
                $scope.newProduct.newrowtotal = 0;
                $scope.newProduct.discAmt = 0;
                $scope.newProduct.discountPercentage = 0;
                $scope.posModel.addedProducts.push($scope.newProduct);
                $timeout(function () {
                    var newNextFieldId = 'empucode_' + ($scope.posModel.addedProducts.length - 1);
                    $("#" + newNextFieldId).focus();
                }, 100);
                // $scope.posModel.addedProducts.reverse();
                $scope.updateAddProductCss();
                $scope.calculatetotal();
                $scope.newProduct = '';
                $scope.closePopup();

            }
        }
        $scope.checkAllEmployeeCode = function ()
        {
            $scope.empCode = false;
            for (var i = 0; i < $scope.posModel.addedProducts.length; i++)
            {
                if ($scope.posModel.addedProducts[i].empCode == '' || $scope.posModel.addedProducts[i].empCode == undefined || $scope.posModel.addedProducts[i].empCode == null)
                {
                    $scope.empCode = true;
                    $('#empucode_' + i).addClass('empBoxColor');

                    break;
                } else
                {
                    $('#empucode_' + i).removeClass('empBoxColor');
                }

            }
            if ($scope.empCode == false)
            {

                $scope.checkPosCustomer();
            }
        }
        $scope.uniqueCodeCheck = function (i)
        {
            if ($scope.posModel.addedProducts[i].empCode != '' && $scope.posModel.addedProducts[i].empCode != undefined && $scope.posModel.addedProducts[i].empCode != null)
            {
                $('#empucode_' + i).removeClass('empBoxColor');
            } else
            {
                $('#empucode_' + i).addClass('empBoxColor');
            }
        }
        $scope.showProduct = false;
        $scope.selectedProduct = {};
        $scope.viewProduct = function ($index)
        {
            $scope.showProduct = true;
            $scope.selectedProduct = $scope.posModel.addedProducts[$index];
            $scope.selectedProduct.selectedViewIndex = $index;
            $scope.selectedProduct.resetcode = $scope.selectedProduct.empCode;
            $scope.selectedProduct.resetnote = $scope.selectedProduct.prodnewdesc;
            if ($scope.posModel.addedProducts[$index].empCode == undefined || $scope.posModel.addedProducts[$index].empCode == null || $scope.posModel.addedProducts[$index].empCode == '')
            {
                $scope.selectedProduct.empCode = '';
            } else
            {
                $scope.selectedProduct.empCode = $scope.posModel.addedProducts[$index].empCode;
            }
            $scope.selectedProduct.prodnewtotal = $scope.posModel.addedProducts[$index].rowtotal;
            $scope.selectedProduct.prodnewqty = $scope.posModel.addedProducts[$index].qty;
            if ($scope.posModel.addedProducts[$index].applyDiscount == true)
            {
                $scope.selectedProduct.prodnewdiscount = $scope.posModel.allProductdiscount;
            } else {
                $scope.selectedProduct.prodnewdiscount = $scope.posModel.addedProducts[$index].discountPercentage;
            }
            $scope.selectedProduct.prodnewdesc = $scope.posModel.addedProducts[$index].description;
            $scope.selectedProduct.newRevisedSellingPrice = $scope.posModel.addedProducts[$index].revisedSellingPrice;
            $scope.selectedProduct.newDefaultSellingPrice = $scope.posModel.addedProducts[$index].defaultSellingPrice;
            $scope.selectedProduct.prodnewtax = '0';
            if ($scope.posModel.addedProducts[$index].taxMapping.length > 0)
            {
                var taxPer = 0;
                for (var i = 0; i < $scope.posModel.addedProducts[$index].taxMapping.length; i++)
                {
                    taxPer = taxPer + parseFloat($scope.posModel.addedProducts[$index].taxMapping[i].tax_percentage);
                }
                $scope.selectedProduct.prodnewtax = taxPer;
            }
            $scope.updateProdDetail();
        }

        $scope.hideProductDetail = function ()
        {
//            if ($scope.selectedProduct.resetcode == undefined || $scope.selectedProduct.resetcode == '' || $scope.selectedProduct.resetcode == null)
//            {
//                $scope.selectedProduct.empCode = '';
//                $('#empCode').addClass('empBoxColor');
//            } else
//            {
//                $scope.selectedProduct.empCode = $scope.selectedProduct.resetcode;
//            }
//            $scope.selectedProduct.prodnewdesc = $scope.selectedProduct.resetnote;
            $scope.showProduct = false;
        };
        $scope.checkEmployee = function ()
        {
            if ($scope.selectedProduct.empCode == '' || $scope.selectedProduct.empCode == null || $scope.selectedProduct.empCode == undefined)
            {
                sweet.show('OOps', 'Please select one employee', 'error');
            } else
            {
                $scope.addProductDetail();
            }
        }

        $scope.addProductDetail = function (prodtype, index)
        {
            if ($scope.selectedProduct.prodnewdiscount == '')
            {
                $scope.selectedProduct.prodnewdiscount = 0;
            }
            if ($scope.posModel.newproddiscount == '' || typeof $scope.posModel.newproddiscount == "undefined" || $scope.posModel.newproddiscount == null)
            {
                $scope.posModel.newproddiscount = 0;
            }
            if (prodtype == 'new')
            {
                $scope.showAddScreen = false;
                var newpro = {};
                newpro.qty = parseFloat($scope.posModel.newprodqty);
                newpro.discountPercentage = parseFloat($scope.posModel.newproddiscount);
                newpro.description = $scope.posModel.newprodnote;
                newpro.selling_price = $scope.posModel.newprodprice;
                newpro.defaultSellingPrice = $scope.posModel.newprodprice;
                newpro.mrp_price = $scope.posModel.newprodprice;
                newpro.name = $scope.posModel.newproductname;
                newpro.tierPricing = [];
                newpro.taxMapping = [];
                newpro.product_id = 0;
                newpro.refType = $rootScope.appConfig.sales_product_list;
                newpro.empCode = '';
                $scope.posModel.addedProducts.push(newpro);
                $scope.posModel.addedProducts.reverse();
                $scope.calculatetotal();
                $timeout($scope.updateAddProductCss, 100);
            } else
            {
                $scope.showProduct = false;
                var foundProduct = false;
                for (var i = 0; i < $scope.posModel.addedProducts.length; i++)
                {
                    if ($scope.selectedProduct.bcn_id != undefined &&
                            $scope.selectedProduct.bcn_id != null
                            && $scope.selectedProduct.bcn_id != 0
                            && $scope.selectedProduct.id == $scope.posModel.addedProducts[index].id
                            && $scope.selectedProduct.bcn_id == $scope.posModel.addedProducts[index].bcn_id)
                    {
                        foundProduct = true;
                        if (parseFloat($scope.selectedProduct.qty_in_hand) < parseFloat($scope.selectedProduct.prodnewqty))
                        {
                            $scope.selectedNewProduct = $scope.selectedProduct;
                            $scope.indexInAP = i;
                            $scope.showPopup(3);
                            break;
                        } else
                        {
                            $scope.posModel.addedProducts[index].rowtotal = parseFloat($scope.selectedProduct.prodnewtotal);
                            $scope.posModel.addedProducts[index].qty = parseFloat($scope.selectedProduct.prodnewqty);
                            $scope.posModel.addedProducts[index].discountPercentage = parseFloat($scope.selectedProduct.prodnewdiscount);
                            $scope.posModel.addedProducts[index].empCode = $scope.selectedProduct.empCode;
                            $scope.posModel.addedProducts[index].description = $scope.selectedProduct.prodnewdesc;
                            $('#empCode').addClass('empBoxColor');
                            if ($scope.selectedProduct.refType == 'Product Based')//Check if product based or purchase base, allow if product based
                            {
                                $scope.posModel.addedProducts[index].revisedSellingPrice = parseFloat($scope.selectedProduct.prodnewtotal);
                            } else
                            {
                                $scope.posModel.addedProducts[index].revisedSellingPrice = parseFloat($scope.selectedProduct.newRevisedSellingPrice);
                            }
                            $scope.calculatetotal();
                        }
                    }
                }
                if (foundProduct == false)
                {
                    for (var i = 0; i < $scope.posModel.addedProducts.length; i++)
                    {
                        if ($scope.selectedProduct.id == $scope.posModel.addedProducts[index].id)
                        {
                            if (parseFloat($scope.selectedProduct.qty_in_hand) < parseFloat($scope.selectedProduct.prodnewqty))
                            {
                                $scope.selectedNewProduct = $scope.selectedProduct;
                                $scope.indexInAP = i;
                                $scope.showPopup(3);
                                break;
                            } else
                            {
                                $scope.posModel.addedProducts[index].revisedSellingPrice = parseFloat($scope.selectedProduct.newRevisedSellingPrice);
                                $scope.posModel.addedProducts[index].qty = parseFloat($scope.selectedProduct.prodnewqty);
                                $scope.posModel.addedProducts[index].discountPercentage = parseFloat($scope.selectedProduct.prodnewdiscount);
                                $scope.posModel.addedProducts[index].empCode = $scope.selectedProduct.empCode;
                                $scope.posModel.addedProducts[index].description = $scope.selectedProduct.prodnewdesc;
                                $scope.calculatetotal();
                            }
                        }
                    }
                }
//                $scope.selectedProduct = {};
            }
        }
        $scope.getEmpCodeList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.EMPLOYEE_LIST_ALL, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatCodeModel = function (model, index)
        {
            if (model != null && model != "" && model != undefined)
            {
                $('#empucode_' + index).removeClass('empBoxColor');
                return model.name + '(' + model.employee_code + ')';
            }
            return  '';
        };
        $scope.hideNewProductScreen = function ()
        {
            $scope.posModel.newproductname = '';
            $scope.posModel.newprodqty = '';
            $scope.posModel.newproddiscount = '0';
            $scope.posModel.newprodprice = '';
            $scope.posModel.newprodnote = '';
            $scope.showAddScreen = false;
        }
        $scope.qtyExist = false;
        $scope.updateProdDetail = function ()
        {
            if ($scope.selectedProduct.prodnewdiscount != null && $scope.selectedProduct.prodnewdiscount != 0 && $scope.selectedProduct.prodnewqty > 0)
            {
                $scope.qtyExist = false;
                if ($scope.selectedProduct.tierPricing.length > 0)
                {
                    for (var j = 0; j < $scope.selectedProduct.tierPricing.length; j++)
                    {
                        if (parseFloat($scope.selectedProduct.prodnewqty) >= parseFloat($scope.selectedProduct.tierPricing[j].from_qty) && parseFloat($scope.selectedProduct.prodnewqty) <= parseFloat($scope.selectedProduct.tierPricing[j].to_qty))
                        {
                            $scope.qtyExist = true;
                            $scope.selectedQtyIndex = j;
                        }
                    }
                    if ($scope.qtyExist)
                    {
                        $scope.selectedProduct.selling_price = parseFloat($scope.selectedProduct.tierPricing[$scope.selectedQtyIndex].unit_price);
                        $scope.selectedProduct.newrowtotal = parseFloat($scope.selectedProduct.selling_price) * parseFloat($scope.selectedProduct.prodnewqty);
                    } else if (!$scope.qtyExist)
                    {
                        $scope.selectedProduct.newrowtotal = parseFloat($scope.selectedProduct.defaultSellingPrice) * parseFloat($scope.selectedProduct.prodnewqty);
                    }

                } else
                {
                    $scope.selectedProduct.newrowtotal = parseFloat($scope.selectedProduct.defaultSellingPrice) * parseFloat($scope.selectedProduct.prodnewqty);
                }
                $scope.selectedProduct.newdiscountAmount = parseFloat((parseFloat($scope.selectedProduct.newrowtotal) * parseFloat($scope.selectedProduct.prodnewdiscount)) / 100);
                $scope.selectedProduct.prodnewdistotal = parseFloat($scope.selectedProduct.newrowtotal) - parseFloat($scope.selectedProduct.newdiscountAmount);
                if ($scope.selectedProduct.newdiscountAmount > 0)
                {
                    // find new selling price by reverse tax calculation for prodnewdistotal
                    $scope.selectedProduct.newRevisedSellingPrice = parseFloat($scope.selectedProduct.prodnewdistotal / (1 + (parseFloat($scope.selectedProduct.prodnewtax) / 100)));
                    $scope.selectedProduct.newRevisedSellingPrice = parseFloat($scope.selectedProduct.newRevisedSellingPrice / $scope.selectedProduct.prodnewqty);
                } else
                {
                    $scope.selectedProduct.newRevisedSellingPrice = parseFloat($scope.selectedProduct.revisedSellingPrice);
                }
                //                        if ($scope.selectedProduct.prodnewtax > 0 && $scope.selectedProduct.refType == 'Product Based')//Check if product based or purchase base, allow if product based
                //                        {
                //                              $scope.selectedProduct.prodnewtotal = parseFloat($scope.selectedProduct.prodnewdistotal) + parseFloat((parseFloat($scope.selectedProduct.prodnewdistotal) * parseFloat($scope.selectedProduct.prodnewtax)) / 100);
                //                        } else
                //                        {
                $scope.selectedProduct.prodnewtotal = $scope.selectedProduct.prodnewdistotal;
                //                        }
            } else
            {
                $scope.qtyExist = false;
                if ($scope.selectedProduct.tierPricing.length > 0 && $scope.selectedProduct.prodnewqty > 0)
                {
                    for (var j = 0; j < $scope.selectedProduct.tierPricing.length; j++)
                    {
                        if (parseFloat($scope.selectedProduct.prodnewqty) >= parseFloat($scope.selectedProduct.tierPricing[j].from_qty) && parseFloat($scope.selectedProduct.prodnewqty) <= parseFloat($scope.selectedProduct.tierPricing[j].to_qty))
                        {
                            $scope.qtyExist = true;
                            $scope.selectedQtyIndex = j;
                        }
                    }
                    if ($scope.qtyExist)
                    {
                        $scope.selectedProduct.selling_price = parseFloat($scope.selectedProduct.tierPricing[$scope.selectedQtyIndex].unit_price);
                        $scope.selectedProduct.newrowtotal = parseFloat($scope.selectedProduct.selling_price) * parseFloat($scope.selectedProduct.prodnewqty);
                        $scope.selectedProduct.prodnewtotal = parseFloat($scope.selectedProduct.newrowtotal);
                    } else if (!$scope.qtyExist)
                    {
                        $scope.selectedProduct.newrowtotal = parseFloat($scope.selectedProduct.defaultSellingPrice) * parseFloat($scope.selectedProduct.prodnewqty);
                        //                                    if ($scope.selectedProduct.prodnewtax > 0 && $scope.selectedProduct.refType == 'Product Based')
                        //                                    {
                        //                                          $scope.selectedProduct.prodnewtotal = parseFloat($scope.selectedProduct.newrowtotal) + parseFloat((parseFloat($scope.selectedProduct.newrowtotal) * parseFloat($scope.selectedProduct.prodnewtax)) / 100);
                        //                                    } else
                        //                                    {
                        $scope.selectedProduct.prodnewtotal = parseFloat($scope.selectedProduct.newrowtotal);
                        //                                    }
                    }

                } else
                {
                    if ($scope.selectedProduct.prodnewqty > 0)
                    {
                        $scope.selectedProduct.newrowtotal = parseFloat($scope.selectedProduct.defaultSellingPrice) * parseFloat($scope.selectedProduct.prodnewqty);
                        //                              if ($scope.selectedProduct.prodnewtax > 0 && $scope.selectedProduct.refType == 'Product Based')
                        //                              {
                        //                                    $scope.selectedProduct.prodnewtotal = parseFloat($scope.selectedProduct.newrowtotal) + parseFloat((parseFloat($scope.selectedProduct.newrowtotal) * parseFloat($scope.selectedProduct.prodnewtax)) / 100);
                        //                              } else
                        //                              {
                        $scope.selectedProduct.prodnewtotal = parseFloat($scope.selectedProduct.newrowtotal);
                    }
                    //                              }
                }
                $scope.selectedProduct.newRevisedSellingPrice = parseFloat($scope.selectedProduct.selling_price);
            }
            $scope.selectedProduct.prodnewtotal = parseFloat($scope.selectedProduct.prodnewtotal).toFixed(2);
            if (isNaN($scope.selectedProduct.prodnewtotal))
            {
                $scope.selectedProduct.prodnewtotal = '0.00';
            }
        }
        $scope.sameQtyExist = false;
        $scope.taxList = [];
        $scope.calculatetotal = function ()
        {
            $scope.orderListModel.discountAmt = 0;
            $scope.orderListModel.taxAmt = 0;
            var subTotal = 0;
            var totDiscountPercentage = 0;
            var totTaxPercentage = 0;
            var totalTaxPercentage = 0;
            var totMapTax = 0;
            var totRevisedSellingRowTotal = 0;
            $scope.refType = '';
            console.log("bfr loop", $scope.posModel.addedProducts.length);
            $scope.sameQtyExist = false;
            for (var i = 0; i < $scope.posModel.addedProducts.length; i++)
            {
                if ($scope.posModel.allProductdiscount != '' && $scope.posModel.allProductdiscount != null && $scope.posModel.allProductdiscount != 'undefined')
                {
                    totDiscountPercentage = $scope.posModel.addedProducts[i].discountPercentage;
                } else {
                    totDiscountPercentage = $scope.posModel.addedProducts[i].discountPercentage;
                    $scope.allproductsDiscount = false;
                }
                $scope.refType = $scope.posModel.addedProducts[i].refType;
                totRevisedSellingRowTotal += parseFloat($scope.posModel.addedProducts[i].defaultSellingPrice) * parseFloat($scope.posModel.addedProducts[i].qty);
                $scope.sameQtyExist = false;
                if ($scope.posModel.addedProducts[i].tierPricing.length > 0)
                {
                    for (var j = 0; j < $scope.posModel.addedProducts[i].tierPricing.length; j++)
                    {
                        if ((parseFloat($scope.posModel.addedProducts[i].qty) >= parseFloat($scope.posModel.addedProducts[i].tierPricing[j].from_qty)) && (parseFloat($scope.posModel.addedProducts[i].qty) <= parseFloat($scope.posModel.addedProducts[i].tierPricing[j].to_qty)))
                        {
                            $scope.sameQtyExist = true;
                            $scope.selectedNewQtyIndex = j;
                            $scope.posModel.addedProducts[i].rowtotal = parseFloat($scope.posModel.addedProducts[i].tierPricing[j].unit_price) * parseFloat($scope.posModel.addedProducts[i].qty);
                        }
                    }
                    if ($scope.sameQtyExist)
                    {
                        $scope.posModel.addedProducts[i].selling_price = parseFloat($scope.posModel.addedProducts[i].tierPricing[$scope.selectedNewQtyIndex].unit_price);
                        $scope.posModel.addedProducts[i].rowtotal = parseFloat($scope.posModel.addedProducts[i].selling_price) * parseFloat($scope.posModel.addedProducts[i].qty);
                    } else if (!$scope.sameQtyExist)
                    {
                        $scope.posModel.addedProducts[i].rowtotal = parseFloat($scope.posModel.addedProducts[i].defaultSellingPrice) * parseFloat($scope.posModel.addedProducts[i].qty);
                    }
                } else
                {
                    $scope.posModel.addedProducts[i].rowtotal = parseFloat($scope.posModel.addedProducts[i].defaultSellingPrice) * parseFloat($scope.posModel.addedProducts[i].qty);
                }

                if ($scope.posModel.addedProducts[i].discountPercentage == null)
                {
                    $scope.posModel.addedProducts[i].discAmt = 0.00;
                    $scope.posModel.addedProducts[i].discountPercentage = 0;
                } else
                {
//                    if ($scope.posModel.addedProducts[i].applyDiscount == false)
//                    {
//                        $scope.posModel.addedProducts[i].discAmt = parseFloat($scope.posModel.addedProducts[i].defaultSellingPrice) * parseFloat($scope.posModel.addedProducts[i].qty) * parseFloat($scope.posModel.addedProducts[i].discountPercentage) / 100;
//                        $scope.posModel.addedProducts[i].rowtotal = parseFloat($scope.posModel.addedProducts[i].rowtotal - $scope.posModel.addedProducts[i].discAmt);
//                    } else {
                    $scope.posModel.addedProducts[i].discAmt = parseFloat($scope.posModel.addedProducts[i].defaultSellingPrice) * parseFloat($scope.posModel.addedProducts[i].qty) * parseFloat(totDiscountPercentage) / 100;
                    $scope.posModel.addedProducts[i].rowtotal = parseFloat($scope.posModel.addedProducts[i].rowtotal - $scope.posModel.addedProducts[i].discAmt);
                    //}

                }
                $scope.posModel.addedProducts[i].taxPercentage = 0;
                if ($scope.posModel.addedProducts[i].taxMapping.length > 0)
                {
                    console.log('taxmaplength');
                    console.log($scope.posModel.addedProducts[i].taxMapping.length);
                    for (var j = 0; j < $scope.posModel.addedProducts[i].taxMapping.length; j++)
                    {
                        $scope.orderListModel.tax_id = $scope.posModel.addedProducts[i].tax_id;
                        $scope.posModel.addedProducts[i].taxPercentage += parseFloat($scope.posModel.addedProducts[i].taxMapping[j].tax_percentage);
                        $scope.posModel.addedProducts[i].tax_id = $scope.posModel.addedProducts[i].taxMapping[j].tax_id;
                    }
                } else
                {
                    $scope.posModel.addedProducts[i].taxPercentage = 0;
                }
                if ($scope.posModel.addedProducts[i].taxPercentage == null)
                {
                    $scope.posModel.addedProducts[i].taxPercentage = 0;
                } else
                {
                    $scope.posModel.addedProducts[i].taxAmt = parseFloat($scope.posModel.addedProducts[i].rowtotal) - ((parseFloat($scope.posModel.addedProducts[i].rowtotal)) / (1 + (parseFloat($scope.posModel.addedProducts[i].taxPercentage) / 100)));
                }
                $scope.posModel.addedProducts[i].rowtotal = parseFloat($scope.posModel.addedProducts[i].rowtotal).toFixed(2);
                $scope.posModel.addedProducts[i].seprowtotal = utilityService.changeCurrency($scope.posModel.addedProducts[i].rowtotal, $rootScope.appConfig.thousand_seperator);
                subTotal += parseFloat($scope.posModel.addedProducts[i].rowtotal);
            }
            $scope.orderListModel.totRevisedSellingRowTotal = parseFloat(totRevisedSellingRowTotal).toFixed(2);
            $scope.orderListModel.subtotal = parseFloat(subTotal).toFixed(2);
            for (var i = 0; i < $scope.posModel.addedProducts.length; i++)
            {
                $scope.orderListModel.discountAmt = parseFloat($scope.orderListModel.discountAmt) + parseFloat($scope.posModel.addedProducts[i].discAmt);
                $scope.orderListModel.taxAmt = parseFloat($scope.orderListModel.taxAmt) + parseFloat($scope.posModel.addedProducts[i].taxAmt);
            }
            if (isNaN($scope.orderListModel.discountAmt))
            {
                $scope.orderListModel.discountAmt = 0.00;
            } else
            {
                $scope.orderListModel.discountAmt = parseFloat($scope.orderListModel.discountAmt).toFixed(2);
            }
            if (isNaN($scope.orderListModel.taxAmt))
            {
                $scope.orderListModel.taxAmt = 0.00;
            } else
            {
                $scope.orderListModel.taxAmt = parseFloat($scope.orderListModel.taxAmt).toFixed(2);
            }

            if ($scope.orderListModel.insuranceCharge == '' || isNaN($scope.orderListModel.insuranceCharge))
            {
                $scope.orderListModel.insuranceAmt = '0.00';
            } else
            {
                $scope.orderListModel.insuranceAmt = parseFloat($scope.orderListModel.insuranceCharge).toFixed(2);
            }
            if ($scope.orderListModel.packagingCharge == '' || isNaN($scope.orderListModel.packagingCharge))
            {
                $scope.orderListModel.packagingAmt = '0.00';
            } else
            {
                $scope.orderListModel.packagingAmt = parseFloat($scope.orderListModel.packagingCharge).toFixed(2);
            }
            if ($scope.orderListModel.subtotal > 0)
            {
                $scope.orderListModel.subtotalWithDiscount = $scope.orderListModel.subtotal - $scope.orderListModel.discountAmt;
                $scope.orderListModel.total = (parseFloat(subTotal) + parseFloat($scope.orderListModel.insuranceAmt) + parseFloat($scope.orderListModel.packagingAmt)).toFixed(2);
                //                        if ($scope.refType.toUpperCase() == 'PRODUCT BASED')
                //                        {
                //                              $scope.orderListModel.total = parseFloat($scope.orderListModel.total) + parseFloat($scope.orderListModel.taxAmt);
                //                        }
            } else
            {
                $scope.orderListModel.subtotalWithDiscount = 0.00;
//                $scope.orderListModel.total = 0.00;
                $scope.orderListModel.total = $scope.orderListModel.subtotal;
            }
            $scope.orderListModel.balance_amount = (parseFloat($scope.orderListModel.total - $scope.orderListModel.paid_amount)).toFixed(2);
            $scope.orderListModel.totalQty = 0;
            var qty = 0;
            if ($scope.posModel.addedProducts.length > 0)
            {
                for (var i = 0; i < $scope.posModel.addedProducts.length; i++)
                {
                    var newqty = parseInt($scope.posModel.addedProducts[i].qty) + qty;
                    $scope.orderListModel.totalQty += newqty;
                }

            }
            $scope.updateRoundoff();
        }
        $scope.updateRoundoff = function ()
        {
            $scope.orderListModel.taxAmt = parseFloat($scope.orderListModel.taxAmt).toFixed(2);
//            $scope.orderListModel.round = Math.round($scope.orderListModel.total);
            $scope.orderListModel.round = $scope.orderListModel.total;
            $scope.orderListModel.roundoff = $scope.orderListModel.round - parseFloat($scope.orderListModel.total);
            $scope.orderListModel.roundoff = parseFloat($scope.orderListModel.roundoff).toFixed(2);
            $scope.orderListModel.round = parseFloat($scope.orderListModel.round).toFixed(2);
            $scope.orderListModel.payamt = $scope.orderListModel.round;
            if ($scope.orderListModel.totRevisedSellingRowTotal > 0)
            {
                $scope.orderListModel.septotRevisedSellingRowTotal = utilityService.changeCurrency($scope.orderListModel.totRevisedSellingRowTotal, $rootScope.appConfig.thousand_seperator);
            } else
            {
                $scope.orderListModel.septotRevisedSellingRowTotal = $scope.orderListModel.totRevisedSellingRowTotal;
            }
            $scope.orderListModel.sepdiscountAmt = utilityService.changeCurrency($scope.orderListModel.discountAmt, $rootScope.appConfig.thousand_seperator);
            if ($scope.orderListModel.subtotal > 0)
            {
                $scope.orderListModel.sepsubtotal = utilityService.changeCurrency($scope.orderListModel.subtotal, $rootScope.appConfig.thousand_seperator);
            } else
            {
                $scope.orderListModel.sepsubtotal = $scope.orderListModel.subtotal;
            }
            $scope.orderListModel.septaxAmt = utilityService.changeCurrency($scope.orderListModel.taxAmt, $rootScope.appConfig.thousand_seperator);
            $scope.orderListModel.sepround = utilityService.changeCurrency($scope.orderListModel.round, $rootScope.appConfig.thousand_seperator);
        }
        $scope.orderListModel.totalTax = 0;
        for (var i = 0; i < $scope.taxList.length; i++)
        {
            $scope.orderListModel.totalTax += $scope.taxList[i].taxPercentage;
        }
        $scope.orderListModel.totalTaxPercentage = $scope.orderListModel.totalTax;
        $scope.removePay = function ($index)
        {
            $scope.orderListModel.paymentList.splice($index, 1);
            $scope.updatePayment();
        }

        $scope.updatePayment = function (type, id)
        {
            $scope.totalPayAmt = 0;
            $scope.totalBalAmt = 0;
            if (type != undefined && type != '' && !isNaN($scope.orderListModel.payamt))
            {
                var pay = {};
                pay.account_id = id;
                pay.type = type;
                pay.amt = parseFloat($scope.orderListModel.payamt);
                pay.sepamt = '';
                if (type.toLowerCase() == 'card' && $scope.orderListModel.cardcomments != '')
                {
                    pay.comments = $scope.orderListModel.cardcomments;
                } else
                {
                    pay.comments = '';
                }
                $scope.orderListModel.paymentList.push(pay);
            }
            if ($scope.orderListModel.paymentList.length > 0)
            {
                for (var i = 0; i < $scope.orderListModel.paymentList.length; i++)
                {
                    $scope.orderListModel.paymentList[i].amt = parseFloat($scope.orderListModel.paymentList[i].amt).toFixed(2);
                    $scope.orderListModel.paymentList[i].sepamt = utilityService.changeCurrency($scope.orderListModel.paymentList[i].amt, $rootScope.appConfig.thousand_seperator);
                    $scope.totalPayAmt = parseFloat($scope.totalPayAmt) + parseFloat($scope.orderListModel.paymentList[i].amt);
                }
            }
            $scope.orderListModel.payamt = parseFloat($scope.orderListModel.round) - parseFloat($scope.totalPayAmt);
            $scope.orderListModel.payamt = parseFloat($scope.orderListModel.payamt).toFixed(2);
            if ($scope.orderListModel.payamt < 0)
            {
                $scope.totalBalAmt = parseFloat($scope.orderListModel.payamt).toFixed(2);
            }
            $scope.totalPayAmt = parseFloat($scope.totalPayAmt).toFixed(2);
            if ($scope.orderListModel.round == parseFloat($scope.totalPayAmt))
            {
//                $scope.showPayPopup = false;
                $scope.saveOrder();
            }
        }

        $scope.calculateTaxAmt = function (forceSave)
        {
            if (($scope.orderListModel.subtotalWithDiscount != 0 && $scope.orderListModel.subtotalWithDiscount != '' && $scope.orderListModel.subtotalWithDiscount >= 0) &&
                    ($scope.orderListModel.total != 0 && $scope.orderListModel.total != '' && $scope.orderListModel.total >= 0))
            {
                var getListParam = {};
                getListParam.amount = $scope.orderListModel.subtotalWithDiscount;
                getListParam.tax_id = $scope.orderListModel.tax_id;
                adminService.calculateTaxAmt(getListParam).then(function (response)
                {
                    var data = response.data;
                    $scope.orderListModel.taxAmountDetail = data;
                    $scope.orderListModel.taxAmt = parseFloat($scope.orderListModel.taxAmountDetail.tax_amount).toFixed(2);
                    $scope.orderListModel.subtotalWithDiscount = $scope.orderListModel.subtotal - $scope.orderListModel.discountAmt;
                    $scope.orderListModel.total = (parseFloat($scope.orderListModel.subtotal) + parseFloat($scope.orderListModel.taxAmt) - parseFloat($scope.orderListModel.discountAmt)).toFixed(2);
                    $scope.orderListModel.balance_amount = (parseFloat($scope.orderListModel.total - $scope.orderListModel.paid_amount)).toFixed(2);
                    $scope.updateRoundoff();
                    if (forceSave)
                    {
                        $scope.createOrder(forceSave);
                    }

                    console.log('taxamtdetail');
                    console.log($scope.orderListModel.taxAmountDetail);
                });
            } else
            {
                $scope.orderListModel.taxAmountDetail = '';
                $scope.orderListModel.taxAmt = parseFloat(0).toFixed(2);
                if ($scope.orderListModel.subtotal > 0)
                {
                    $scope.orderListModel.subtotalWithDiscount = $scope.orderListModel.subtotal - $scope.orderListModel.discountAmt;
                    $scope.orderListModel.total = (parseFloat($scope.orderListModel.subtotal) + parseFloat($scope.orderListModel.taxAmt) - parseFloat($scope.orderListModel.discountAmt)).toFixed(2);
                    $scope.orderListModel.balance_amount = (parseFloat($scope.orderListModel.total - $scope.orderListModel.paid_amount)).toFixed(2);
                    $scope.updateRoundoff();
                }
                if (forceSave)
                {
                    $scope.createOrder(forceSave);
                }
                console.log('taxamtdetail');
                console.log($scope.orderListModel.taxAmountDetail);
            }
        }

        $scope.createOrder = function (forceSave)
        {
            if (!forceSave)
            {
                if ($scope.isDataSavingProcess)
                {
                    return;
                }
            }
            $scope.isDataSavingProcess = true;
            if (typeof $scope.orderListModel.taxAmountDetail != undefined && $scope.orderListModel.taxAmountDetail != null && $scope.orderListModel.taxAmountDetail != '')
            {
                if ($scope.orderListModel.taxAmountDetail.tax_amount == $scope.orderListModel.taxAmt)
                {
                    $scope.saveOrder();
                } else
                {
                    $scope.calculateTaxAmt(true);
                }
            } else
            {
                $scope.calculateTaxAmt(true);
            }

        }

        $scope.enableShow = false;
        $scope.updateEnable = function ()
        {
            $scope.enableShow = !$scope.enableShow;
        }
//        $scope.showAlert = true;
//        setTimeout(function () {
//            if ($rootScope.userModel.new_user == 1 && $rootScope.hideAlert == false)
//            {
//                $window.localStorage.setItem("demo_guide", "false");
//                swal({
//                    title: "Guide Tour!",
//                    text: "Kindly fill all the required details and click submit to proceed the demo..",
//                    confirmButtonText: "OK"
//                },
//                        function (isConfirm) {
//                            if (isConfirm) {
//                                $rootScope.hideAlert = true;
//                            }
//                        });
//            }
//        }, 2000);

        $scope.invoiceSavingProcess = false;
        $scope.saveOrder = function (onaccount)
        {
            if (onaccount == 'yes')
            {
                $scope.viaPayment = true;
                $scope.invoiceSavingProcess = true;
            } else
            {
                $scope.isDataSavingProcess = true;
            }
            var createOrderParam = {};
            var headers = {};
            headers['screen-code'] = 'salesinvoice';
            createOrderParam.quote_id = '';
            createOrderParam.is_pos = 1;
            createOrderParam.prefix = $scope.orderListModel.invoicePrefix;
            createOrderParam.invoice_no = $scope.orderListModel.invoiceno;
            if (typeof $scope.orderListModel.billdate == 'object')
            {
                $scope.orderListModel.billdate = utilityService.parseDateToStr($scope.orderListModel.billdate, $scope.adminService.appConfig.date_format);
            }
            createOrderParam.date = utilityService.changeDateToSqlFormat($scope.orderListModel.billdate, $scope.adminService.appConfig.date_format);
            createOrderParam.datepaid = '';
            createOrderParam.duedate = utilityService.changeDateToSqlFormat($scope.orderListModel.billdate, $scope.adminService.appConfig.date_format);
            createOrderParam.customer_address = '';
            createOrderParam.subtotal = (parseFloat($scope.orderListModel.subtotal)).toFixed(2);
            createOrderParam.tax_amount = (parseFloat($scope.orderListModel.taxAmt)).toFixed(2);
            createOrderParam.discount_amount = (parseFloat($scope.orderListModel.discountAmt)).toFixed(2);
            createOrderParam.discount_percentage = $scope.orderListModel.discountPercentage;
            if (isNaN($scope.orderListModel.discountPercentage) || $scope.orderListModel.discountPercentage == null)
            {
                createOrderParam.discount_percentage = 0.00;
            }
            createOrderParam.discount_mode = '%';
            createOrderParam.total_amount = $scope.orderListModel.total;
            createOrderParam.round_off = (parseFloat($scope.orderListModel.roundoff)).toFixed(2);
            createOrderParam.paymentmethod = "";
            createOrderParam.notes = $scope.orderListModel.notes;
            createOrderParam.item = [];
            createOrderParam.item.outletId = 1;
            createOrderParam.item.status = 'initiated';
            if ($scope.customerDetail != undefined && $scope.customerDetail.fname != undefined)
            {
                createOrderParam.customer_id = $scope.customerDetail.id;
            } else
            {
                createOrderParam.customer_id = $rootScope.appConfig.poscusid;
            }
            createOrderParam.item.orderDetails = [];
            if ($scope.posModel.addedProducts.length < 1 || createOrderParam.total_amount < 0)
            {
                return;
            }
            var length = 0;
            if ($scope.posModel.addedProducts.length == 1)
            {
                length = 2;
            } else
            {
                length = $scope.posModel.addedProducts.length + 1;
            }
            var totalQty = 0;
            for (var i = 0; i < length - 1; i++)
            {
                var ordereditems = {};
                if ($scope.posModel.addedProducts[i].bcn_id != undefined && $scope.posModel.addedProducts[i].bcn_id != '' && $scope.posModel.addedProducts[i].bcn_id != null)
                {
                    ordereditems.purchase_invoice_item_id = $scope.posModel.addedProducts[i].bcn_id;
                } else {
                    ordereditems.purchase_invoice_item_id = 0;
                }
                ordereditems.product_id = $scope.posModel.addedProducts[i].product_id;
                ordereditems.product_sku = $scope.posModel.addedProducts[i].sku;
                ordereditems.mrp_price = $scope.posModel.addedProducts[i].mrp_price;
                ordereditems.sales_price = $scope.posModel.addedProducts[i].sales_price;
                ordereditems.emp_code = $scope.posModel.addedProducts[i].empCode.employee_code;
                ordereditems.emp_id = $scope.posModel.addedProducts[i].empCode.id;
                ordereditems.product_name = $scope.posModel.addedProducts[i].name;
                if ($scope.refType.toUpperCase() == 'PURCHASE BASED')
                {
                    ordereditems.unit_price = parseFloat($scope.posModel.addedProducts[i].mrp_price);
                } else
                {
                    ordereditems.unit_price = parseFloat($scope.posModel.addedProducts[i].selling_price);
                }
                ordereditems.qty = parseInt($scope.posModel.addedProducts[i].qty);
                ordereditems.discount_mode = '%';
                if ($scope.allproductsDiscount == true)
                {
                    ordereditems.discount_value = $scope.posModel.allProductdiscount;
                } else {
                    ordereditems.discount_value = $scope.posModel.addedProducts[i].discountPercentage;
                }
                totalQty += parseFloat($scope.posModel.addedProducts[i].qty);
                ordereditems.comments = $scope.posModel.addedProducts[i].description;
                ordereditems.uom_name = $scope.posModel.addedProducts[i].uom;
                ordereditems.uom_id = $scope.posModel.addedProducts[i].uom_id;
                ordereditems.custom_opt1 = '';
                ordereditems.custom_opt2 = '';
                ordereditems.custom_opt3 = '';
                ordereditems.custom_opt4 = '';
                ordereditems.custom_opt5 = '';
                if ($scope.posModel.addedProducts[i].custom_opt1 != '' && $scope.posModel.addedProducts[i].custom_opt1 != null)
                {
                    ordereditems.custom_opt1 = $scope.posModel.addedProducts[i].custom_opt1;
                }
                if ($scope.posModel.addedProducts[i].custom_opt2 != '' && $scope.posModel.addedProducts[i].custom_opt2 != null)
                {
                    ordereditems.custom_opt2 = $scope.posModel.addedProducts[i].custom_opt2;
                }
                if ($scope.posModel.addedProducts[i].custom_opt3 != '' && $scope.posModel.addedProducts[i].custom_opt3 != null)
                {
                    ordereditems.custom_opt3 = $scope.posModel.addedProducts[i].custom_opt3;
                }
                if ($scope.posModel.addedProducts[i].custom_opt4 != '' && $scope.posModel.addedProducts[i].custom_opt4 != null)
                {
                    ordereditems.custom_opt4 = $scope.posModel.addedProducts[i].custom_opt4;
                }
                if ($scope.posModel.addedProducts[i].custom_opt5 != '' && $scope.posModel.addedProducts[i].custom_opt5 != null)
                {
                    ordereditems.custom_opt5 = $scope.posModel.addedProducts[i].custom_opt5;
                }
                ordereditems.custom_opt1_key = $rootScope.appConfig.invoice_item_custom1_label;
                ordereditems.custom_opt2_key = $rootScope.appConfig.invoice_item_custom2_label;
                ordereditems.custom_opt3_key = $rootScope.appConfig.invoice_item_custom3_label;
                ordereditems.custom_opt4_key = $rootScope.appConfig.invoice_item_custom4_label;
                ordereditems.custom_opt5_key = $rootScope.appConfig.invoice_item_custom5_label;
                ordereditems.hsn_code = $scope.posModel.addedProducts[i].hsn_code;
                if ($scope.posModel.addedProducts[i].tax_id != undefined
                        && $scope.posModel.addedProducts[i].tax_id != null
                        && $scope.posModel.addedProducts[i].tax_id != '')
                {
                    ordereditems.tax_id = $scope.posModel.addedProducts[i].tax_id;
                } else
                {
                    ordereditems.tax_id = '';
                }
                if ($scope.appConfig.sales_product_list.toUpperCase() == 'PURCHASE BASED')
                {
                    ordereditems.total_price = $scope.posModel.addedProducts[i].mrp_price * ordereditems.qty;
                } else {
                    ordereditems.total_price = $scope.posModel.addedProducts[i].sales_price * ordereditems.qty;
                }
                createOrderParam.item.push(ordereditems);
            }
            createOrderParam.total_qty = totalQty;
            createOrderParam.customattribute = [];
            createOrderParam.deal_id = '';
            createOrderParam.deal_name = '';
            adminService.addInvoice(createOrderParam, headers).then(function (response) {
                if (response.data.success == true)
                {
                    if (onaccount == 'yes')
                    {
                        if (parseFloat($scope.totalPayAmt) > 0)
                        {
                            $scope.savePayment(response.data.id);
                        }
                        else
                        {
                            $scope.formReset();
                            $scope.closePaymentPopup();
                            $state.go('app.pos', {
                                reload: true
                            });
                            $scope.invoiceSavingProcess = false;
                            $scope.getProductList('All');
                            $scope.getNextInvoiceno();
                            $scope.showPayPopup = false;
                            $scope.print(response.data.id);
                        }
                        
                    } else if ($scope.orderListModel.paymentList.length > 0)
                    {
                        //$scope.groupPayment(response.data.id);
                        $scope.savePayment(response.data.id);
                    }
                    if ($rootScope.userModel.new_user == 1)
                    {
                        $window.localStorage.setItem("demo_guide", "true");
                        $scope.updateOldUser();
                    }
                } else
                {
                    $scope.formReset();
                    $scope.showPayPopup = false;
                    $state.go('app.pos', {
                        reload: true
                    });
                    $scope.isDataSavingProcess = false;
                    $scope.getProductList('All');
                    $scope.getNextInvoiceno();
                }

            });
        }
        $scope.invoiceId = '';
        $scope.cashAmount = 0;
        $scope.groupPayment = function (id)
        {
            var cashCount = 0;
            $scope.cashAmount = 0;
            $scope.orderListModel.savePaymentList = [];
            $scope.invoiceId = id;
            for (var i = 0; i < $scope.orderListModel.paymentList.length; i++)
            {
                if ($scope.orderListModel.paymentList[i].type.toLowerCase() == 'cash')
                {
                    cashCount = cashCount + 1;
                    $scope.cashAmount = parseFloat($scope.cashAmount) + parseFloat($scope.orderListModel.paymentList[i].amt);
                } else
                {
                    $scope.orderListModel.savePaymentList.push($scope.orderListModel.paymentList[i]);
                }
                if (i == $scope.orderListModel.paymentList.length - 1)
                {
                    if ($scope.orderListModel.payamt < 0)
                    {
                        $scope.cashAmount = parseFloat($scope.cashAmount);
                    }
                    if (cashCount > 0)
                    {
                        var pay = {};
                        pay.type = 'Cash';
                        pay.amt = $scope.cashAmount;
                        pay.sepamt = '';
                        pay.account_id = $scope.orderListModel.paymentList[i].account_id;
                        pay.comments = '';
                        $scope.orderListModel.savePaymentList.push(pay);
                    }
                    $scope.savePayment($scope.invoiceId);
                }
            }
        }
        $scope.customername = '';
        $scope.phone = '';
        $scope.customerDetail = '';
        $scope.showcustomername = false;
        $scope.showcustomerpopup = false;
        $scope.showCustomer = function ()
        {
            $scope.showcustomerpopup = true;
            $timeout(function ()
            {
                $("#customerfocus").get(0).focus();
            }, 500);
        }
        $scope.closeCustomer = function ()
        {
            $scope.showcustomerpopup = false;
        }
        $scope.checkPosCustomer = function ()
        {

            if ($scope.orderListModel.customerInfo == '' || $scope.orderListModel.customerInfo.fname == undefined)
            {
                //sweet.show('Oops', 'Please select customer', 'error');
                $scope.showcustomerpopup = true;
                $timeout(function ()
                {
                    $("#customerfocus").focus();
                }, 300);


            } else
            {
                if ($scope.orderListModel.invoiceno == '' || $scope.orderListModel.invoiceno == undefined)
                {
                    sweet.show('Oops', 'Please enter Invoice No', 'error');
                } else
                {
                    $scope.showPaymentPopup();
                }

            }
        }

        $scope.selectCus = function ()
        {
            if ($scope.orderListModel.customerInfo != undefined && $scope.orderListModel.customerInfo.fname != undefined && $scope.orderListModel.customerInfo.fname != null)
            {
                $scope.customerDetail = $scope.orderListModel.customerInfo;
                $scope.customername = $scope.orderListModel.customerInfo.fname;
                $scope.phone = $scope.orderListModel.customerInfo.phone;
//                if (parseInt($scope.orderListModel.customerInfo.id, 10) != parseInt($rootScope.appConfig.poscusid))
//                {
                $scope.showcustomername = true;
//                } else
//                {
//                    $scope.showcustomername = false;
//                }
                $scope.showcustomerpopup = false;
                $scope.getAdvancepaymentList($scope.orderListModel.customerInfo.id);
                $scope.getCusPurDetail($scope.orderListModel.customerInfo.id);
                $scope.getCusBill($scope.orderListModel.customerInfo.id);
                //         $scope.orderListModel.customerInfo = '';
            } else
            {
                $scope.showcustomername = false;
                $scope.showcustomerpopup = false;
            }

        }

        $scope.getCustomerInfo = function () {
            if ($rootScope.appConfig.poscusid != undefined && $rootScope.appConfig.poscusid != null)
            {
                var getListParam = {};
                getListParam.id = $rootScope.appConfig.poscusid;
                adminService.getCustomerDetail(getListParam).then(function (response) {
                    var data = response.data;
                    if (data.total != 0)
                    {
                        $scope.posModel.posCus = data.data;
                    }
                });
            }
        };

        $scope.custDrop = function () {
            if ($scope.orderListModel.customerInfo.id == undefined) {
                $scope.orderListModel.customerInfo = '';
            }
        };
        $scope.getCusPurDetail = function (id) {

            var getListParam = {};
            getListParam.customer_id = id;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getCusPurDetail(getListParam, configOption).then(function (response) {
                var data = response.data.list;
                $scope.posModel.cusPurDetail = data;
                if($scope.posModel.cusPurDetail.last_date_of_billing != null && typeof $scope.posModel.cusPurDetail.last_date_of_billing != "undefined" && $scope.posModel.cusPurDetail.last_date_of_billing != "")
                {
                    $scope.posModel.cusPurDetail.new_last_date_of_billing = utilityService.parseStrToDate($scope.posModel.cusPurDetail.last_date_of_billing);
                    $scope.posModel.cusPurDetail.last_date_of_billing = utilityService.parseDateToStr($scope.posModel.cusPurDetail.new_last_date_of_billing, $scope.dateFormat);
                }
                else
                {
                    $scope.posModel.cusPurDetail.last_date_of_billing = "";
                }
                $scope.posModel.cusPurDetail.total_amount = parseFloat($scope.posModel.cusPurDetail.total_amount).toFixed(2);
            });
        };
        $scope.noData = false;
        $scope.getCustomerList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            autosearchParam.mode = 1;
            return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = [];
                if (data.length > 0)
                {
                    for (var i = 0; i < data.length; i++)
                    {
                        hits.push(data[i]);
                    }
                }
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                if (hits.length == 0)
                {
                    if ($scope.posModel.posCus != undefined && $scope.posModel.posCus != '')
                    {
                        hits.push($scope.posModel.posCus);
                    }
                    $scope.noData = true;
                }
                return hits;
            });
        };
        $scope.formatCustomerModel = function (model)
        {
            if (model != null)
            {
                if (model.fname != undefined && model.phone != undefined && model.email != undefined)
                {
                    return model.fname + '(' + model.phone + ',' + model.email + ')';
                } else if (model.fname != undefined && model.phone != undefined)
                {
                    return model.fname + '(' + model.phone + ')';
                } else if (model.fname != undefined)
                {
                    return model.fname;
                }
            }
            return  '';
        };
        $scope.print = function (id)
        {
            var landingUrl = $window.location.protocol + "//" + $window.location.host + "/public/printHelper.php?no=" + id;
            //   $window.location.href = landingUrl;
            $window.open(landingUrl, '_blank');
        }
        $scope.viaPayment = false;
        $scope.invoiceid = '';
        $scope.savePayment = function (id)
        {
            $scope.invoiceSavingProcess = true;
            for (var i = 0; i < $scope.orderListModel.paymentList.length; i++)
            {
                $scope.invoiceid = id;
                var headers = {};
                headers['screen-code'] = 'salesinvoice';
                var configOption = adminService.handleBothSuccessAndErrorResponseConfig;
                var createPaymentParam = {};
                createPaymentParam.customer_id = '';
                if ($scope.customerDetail != undefined && $scope.customerDetail.fname != undefined)
                {
                    createPaymentParam.customer_id = $scope.customerDetail.id;
                } else if ($rootScope.appConfig.poscusid != undefined && $rootScope.appConfig.poscusid != null)
                {
                    createPaymentParam.customer_id = $rootScope.appConfig.poscusid;
                }
                createPaymentParam.comments = '';
                if (typeof $scope.orderListModel.billdate == 'object')
                {
                    $scope.orderListModel.billdate = utilityService.parseDateToStr($scope.orderListModel.billdate, $scope.adminService.appConfig.date_format);
                }
                createPaymentParam.date = utilityService.changeDateToSqlFormat($scope.orderListModel.billdate, $scope.adminService.appConfig.date_format);
                createPaymentParam.is_settled = 1;
                createPaymentParam.invoice_id = id;
                createPaymentParam.payment_mode = $scope.orderListModel.paymentList[i].type;
                createPaymentParam.cheque_no = '';
                createPaymentParam.tra_category = '';
                createPaymentParam.account = '';
                createPaymentParam.account_id = $scope.orderListModel.paymentList[i].account_id;
                createPaymentParam.comments = $scope.orderListModel.paymentList[i].comments;
                var configData = {};
                configData.paymentIndex = i;
                //                if ($scope.orderListModel.savePaymentList[i].amt > 0)
                //                {
                createPaymentParam.amount = $scope.orderListModel.paymentList[i].amt;
                adminService.createPayment(createPaymentParam, configData, configOption, headers).then(function (response)
                {
                    var config = response.config.config.handleResponse;
                    if (config.paymentIndex == $scope.orderListModel.paymentList.length - 1)
                    {
                        if (response.data.success == true)
                        {
                            $scope.formReset();
                            $state.go('app.pos', {
                                'reload': true
                            });
                            $scope.viaPayment = true;
                            $scope.getProductList('All');
                            $scope.getNextInvoiceno();
                            $scope.showPayPopup = false;
                            $scope.print($scope.invoiceid);
                            $scope.invoiceSavingProcess = false;
                        } else
                        {
                            $scope.showPayPopup = false;
                            $scope.isDataSavingProcess = false;
                            $scope.invoiceSavingProcess = false;
                        }
                    }
                    $scope.isDataSavingProcess = false;
                });
                //                }
                $scope.isDataSavingProcess = false;
            }
        }
        $scope.enableTextArea = false;
        $scope.callEnableTextArea = function ()
        {
            $scope.enableTextArea = true;
        }

        $scope.tabChange = function (value) {
            if ($scope.searchFilter.name != '' && typeof $scope.searchFilter.name != undefined && $scope.searchFilter.name != null)
            {
                $scope.currentStep = value;
            }
        }

        $scope.event = null;
        $scope.keyupHandler = function (event)
        {
            if (event.keyCode != undefined && event.keyCode == 13 && $scope.searchFilter.name != '')
            {
//                $('#empucode_' + $scope.posModel.addedProducts.length - 1).focus();

                $scope.addProduct($scope.posModel.productList[0].id, $scope.posModel.productList[0].bcn_id);
                $scope.getProductList();
//                 if ($scope.orderListModel.customerInfo != '' && $scope.orderListModel.customerInfo != undefined && $scope.orderListModel.customerInfo != null)
//                {
//                    $scope.getProductList();
//                }

//                $timeout(function () {
//                                    var newNextFieldId = 'empucode_' + ($scope.posModel.addedProducts.length - 1);
//                                    $("#" + newNextFieldId).focus();
//                                }, 300);
                $scope.searchFilter.name = '';
                $scope.posModel.category = '';

            } else
            {
                return;
            }
        }
        $scope.keyCodeHandler = function (event)
        {
            if (event.keyCode != undefined && event.keyCode == 13)
            {
                $("#" + 'barcode_name_sku').focus();
            }
        }
        $scope.emptyProduct = function ()
        {
            $scope.posModel.productList = [];
        }
        $scope.productBarcodeDrop = function () {
            if ($scope.posModel.productList.length == 0) {
                $scope.searchFilter.name = '';
                $scope.posModel.category = '';
            }
        }


        $scope.getProductList = function (category, type) {

//                  if ($scope.isProductLoading == false)
//                  {
//$scope.isProductLoaded = false;  
            $scope.posModel.categorySearchLoading = true;
            if ($scope.searchFilter.name != '' && typeof $scope.searchFilter.name != undefined && $scope.searchFilter.name != null || type == "product based")
            {
                $scope.isProductLoading = true;
                var getProductListParam = {};
                $scope.posModel.productList = [];
                $scope.posModel.uncategorizedList = [];
                if (category != undefined)
                {
                    $scope.posModel.category = category;
                    $scope.categoryId = category;
                }
                getProductListParam.search = $scope.searchFilter.name;
                getProductListParam.category = $scope.categoryId;
                if ($scope.posModel.category == 'Uncategorized' || category == 'Uncategorized')
                {
                    getProductListParam.category = '';
                } else if ($scope.posModel.category == 'All' || category == 'All')
                {
                    getProductListParam.category = '';
                }
                $scope.posModel.isLoadingProgress = true;
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                var abortPendingFlag = false;
                if ($scope.isProductLoaded)
                    abortPendingFlag = true;
                var headers = {};
                getProductListParam.is_sale = 1;
                getProductListParam.is_active = 1;
//                if ($rootScope.appConfig.gst_auto_suggest == 1)
//                {
//                    if ($scope.orderListModel.customerInfo != '' && $scope.orderListModel.customerInfo != undefined && $scope.orderListModel.customerInfo != null)
//                    {
//                        getProductListParam.customer_id = $scope.orderListModel.customerInfo.id;
//                    } else
//                    {
//                        sweet.show("Oops", "Please select customer", "error");
//                        $scope.isProductLoading = false;
//                        $scope.customerSelect = false;
//                        return;
//                    }
//                }
                adminService.getProductBasedList(getProductListParam, configOption, headers, abortPendingFlag).then(function (response) {
                    if (response.data.success === true)
                    {
                        var data = response.data;
                        $scope.posModel.productList = data.list;
                        $scope.posModel.categorySearchLoading = false;
                        for (var i = 0; i < $scope.posModel.productList.length; i++)
                        {
                            $scope.posModel.productList[i].empCode = '';
                        }
                        if ($scope.posModel.category == 'Uncategorized')
                        {
                            for (var i = 0; i < $scope.posModel.productList.length; i++)
                            {
                                if ($scope.posModel.productList[i].category_id == 0 || $scope.posModel.productList[i].category_id == null || $scope.posModel.productList[i].category_id == 'undefined')
                                {
                                    $scope.posModel.uncategorizedList.push($scope.posModel.productList[i]);
                                    //  $scope.posModel.productList = $scope.posModel.uncategorizedList;
                                }
                                if (i == $scope.posModel.productList.length - 1)
                                {
                                    $scope.posModel.productList = [];
                                    $scope.posModel.productList = $scope.posModel.uncategorizedList;
                                }
// $scope.posModel.productList = $scope.posModel.uncategorizedList;
                            }

                        }
                        $scope.isProductLoaded = true;
                    }
                    $scope.productBarcodeDrop();
                    $scope.isProductLoading = false;
                    $scope.posModel.isLoadingProgress = false;
                });
            } else
            {
                $scope.isProductLoading = false;
                if ($scope.viaPayment == false)
                {
                    sweet.show("Oops", "Please Give the Barcode");
                }
                $scope.posModel.categorySearchLoading = false;
                $scope.viaPayment = false;
                $scope.posModel.productList = [];
            }
//                  }
        };
        $scope.incrStartLimit = function ()
        {
            $scope.posModel.mstart = $scope.posModel.mstart + 5;
            $scope.posModel.mlimit = $scope.posModel.mlimit + 5;
            $scope.getCategoryList();
        }

        $scope.decrStartLimit = function ()
        {
            $scope.posModel.mstart = $scope.posModel.mstart - 5;
            $scope.posModel.mlimit = $scope.posModel.mlimit - 5;
            $scope.getCategoryList();
        }

        $scope.updateTotal = function ()

        {
            if (!isNaN($scope.posModel.ttcount) && !isNaN($scope.posModel.ttcount1) && !isNaN($scope.posModel.ttcount2)
                    && !isNaN($scope.posModel.ttcount3) && !isNaN($scope.posModel.ttcount4) && !isNaN($scope.posModel.ttcount5)
                    && !isNaN($scope.posModel.ttcount6) && !isNaN($scope.posModel.ttcount7)
                    && !isNaN($scope.posModel.ttcount8))
            {
                $scope.posModel.cashamount = 0;
                $scope.posModel.ttamount = parseFloat(2000 * $scope.posModel.ttcount).toFixed(2);
                $scope.posModel.ttamount1 = parseFloat(1000 * $scope.posModel.ttcount1).toFixed(2);
                $scope.posModel.ttamount2 = parseFloat(500 * $scope.posModel.ttcount2).toFixed(2);
                $scope.posModel.ttamount3 = parseFloat(100 * $scope.posModel.ttcount3).toFixed(2);
                $scope.posModel.ttamount4 = parseFloat(50 * $scope.posModel.ttcount4).toFixed(2);
                $scope.posModel.ttamount5 = parseFloat(20 * $scope.posModel.ttcount5).toFixed(2);
                $scope.posModel.ttamount6 = parseFloat(10 * $scope.posModel.ttcount6).toFixed(2);
                $scope.posModel.ttamount7 = parseFloat(5 * $scope.posModel.ttcount7).toFixed(2);
                $scope.posModel.ttamount8 = parseFloat(1 * $scope.posModel.ttcount8).toFixed(2);
                $scope.posModel.cashamount = parseFloat($scope.posModel.ttamount) + parseFloat($scope.posModel.ttamount1) +
                        parseFloat($scope.posModel.ttamount2) + parseFloat($scope.posModel.ttamount3) + parseFloat($scope.posModel.ttamount4)
                        + parseFloat($scope.posModel.ttamount5) + parseFloat($scope.posModel.ttamount6) + parseFloat($scope.posModel.ttamount7) +
                        parseFloat($scope.posModel.ttamount8);
                $scope.posModel.cashamount = parseFloat($scope.posModel.cashamount).toFixed(2);
            }
        }
        $scope.isDayCloseSavingProcess = false;
        $scope.saveDaycode = function ()
        {
            $scope.isDayCloseSavingProcess = true;
            var configOption = adminService.handleBothSuccessAndErrorResponseConfig;
            var createPaymentParam = {};
            createPaymentParam.user_id = '';
            createPaymentParam.cash = $scope.posModel.cashamount;
            createPaymentParam.credit = $scope.posModel.credit;
            createPaymentParam.card = $scope.posModel.cardamount;
            createPaymentParam.others = $scope.posModel.others;
            createPaymentParam.advance = $scope.posModel.advance;
            createPaymentParam.comments = "";
            createPaymentParam.denomination = [];
            if ($scope.posModel.ttcount > 0)
            {
                var data = {};
                data.unit = "2000.00";
                data.count = $scope.posModel.ttcount;
                data.amount = parseFloat(2000 * parseFloat($scope.posModel.ttcount));
                data.type = '1';
                data.comments = '';
                createPaymentParam.denomination.push(data);
            }
            if ($scope.posModel.ttcount1 > 0)
            {
                var data = {};
                data.unit = "1000.00";
                data.count = $scope.posModel.ttcount1;
                data.amount = parseFloat(1000 * parseFloat($scope.posModel.ttcount1));
                data.type = '2';
                data.comments = '';
                createPaymentParam.denomination.push(data);
            }
            if ($scope.posModel.ttcount2 > 0)
            {
                var data = {};
                data.unit = "500.00";
                data.count = $scope.posModel.ttcount2;
                data.amount = parseFloat(500 * parseFloat($scope.posModel.ttcount2));
                data.type = '3';
                data.comments = '';
                createPaymentParam.denomination.push(data);
            }
            if ($scope.posModel.ttcount3 > 0)
            {
                var data = {};
                data.unit = "100.00";
                data.count = $scope.posModel.ttcount3;
                data.amount = parseFloat(2000 * parseFloat($scope.posModel.ttcount3));
                data.type = '4';
                data.comments = '';
                createPaymentParam.denomination.push(data);
            }
            if ($scope.posModel.ttcount4 > 0)
            {
                var data = {};
                data.unit = "50.00";
                data.count = $scope.posModel.ttcount4;
                data.amount = parseFloat(2000 * parseFloat($scope.posModel.ttcount4));
                data.type = '5';
                data.comments = '';
                createPaymentParam.denomination.push(data);
            }
            if ($scope.posModel.ttcount5 > 0)
            {
                var data = {};
                data.unit = "20.00";
                data.count = $scope.posModel.ttcount5;
                data.amount = parseFloat(2000 * parseFloat($scope.posModel.ttcount5));
                data.type = '6';
                data.comments = '';
                createPaymentParam.denomination.push(data);
            }
            if ($scope.posModel.ttcount6 > 0)
            {
                var data = {};
                data.unit = "10.00";
                data.count = $scope.posModel.ttcount6;
                data.amount = parseFloat(2000 * parseFloat($scope.posModel.ttcount6));
                data.type = '7';
                data.comments = '';
                createPaymentParam.denomination.push(data);
            }
            if ($scope.posModel.ttcount7 > 0)
            {
                var data = {};
                data.unit = "5.00";
                data.count = $scope.posModel.ttcount7;
                data.amount = parseFloat(5 * parseFloat($scope.posModel.ttcount7));
                data.type = '8';
                data.comments = '';
                createPaymentParam.denomination.push(data);
            }
            if ($scope.posModel.ttcount8 > 0)
            {
                var data = {};
                data.unit = "1.00";
                data.count = $scope.posModel.ttcount8;
                data.amount = parseFloat(5 * parseFloat($scope.posModel.ttcount8));
                data.type = '9';
                data.comments = '';
                createPaymentParam.denomination.push(data);
            }
            adminService.saveDaycode(createPaymentParam, configOption).then(function (response)
            {
                var config = response.config.config.handleResponse;
                if (response.data.success == true)
                {
                    $scope.opendayclosepopup = false;
                    $state.go('app.pos', {
                        'reload': true
                    });
                    //  $scope.getProductList('All');
                    $scope.findCurrentDaycode();
                    $scope.findDayCloseCredit();
                }
                $scope.isDayCloseSavingProcess = false;
            });
        }
        $scope.findCurrentDaycode = function () {

            var getListParam = {};
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.findCurrentDaycode(getListParam, configOption).then(function (response) {
                if (response.data.success === true)
                {
                    if (response.data.list.length > 0)
                    {
                        var data = response.data.list[0];
                        console.log(typeof data.start_on);
                        var startdate = data.start_on.split(' ');
                        $scope.posModel.startdate = utilityService.parseStrToDate(startdate);
                        $scope.posModel.startdate = utilityService.parseDateToStr($scope.posModel.startdate, $scope.dateFormat);
                        var enddate = data.end_on.split(' ');
                        $scope.posModel.enddate = utilityService.parseStrToDate(enddate);
                        $scope.posModel.enddate = utilityService.parseDateToStr($scope.posModel.enddate, $scope.dateFormat);
                        $scope.posModel.daycode = data.id;
                    }
                }
            });
        };
        $scope.isNextAutoGeneratorNumberLoaded = false;
        $scope.getNextInvoiceno = function ()
        {
            $scope.isNextAutoGeneratorNumberLoaded = true;
            var getListParam = {};
            getListParam.type = "sales_invoice";
            if ($scope.orderListModel.invoicePrefix != '' && $scope.orderListModel.invoicePrefix != undefined && $scope.orderListModel.invoicePrefix != null)
            {
                getListParam.prefix = $scope.orderListModel.invoicePrefix;
            } else {
                getListParam.prefix = "";
            }

            var headers = {};
            var configOption = adminService.handleOnlyErrorResponseConfig;
            configOption.perfix = getListParam.prefix;
            adminService.getNextInvoiceNo(getListParam, configOption, headers).then(function (response)
            {
                var config = response.config.config;
                if (response.data.success === true && typeof config.perfix !== 'undefined' && config.perfix == $scope.orderListModel.invoicePrefix)
                {
                    if (!$scope.orderListModel.is_next_autogenerator_num_changed)
                    {
                        $scope.orderListModel.next_autogenerator_num = response.data.no;
                        $scope.orderListModel.invoiceno = $scope.orderListModel.next_autogenerator_num;
                    }
                }
                $scope.isNextAutoGeneratorNumberLoaded = false;
            });
        };
        $scope.invoiceNumberChange = function ()
        {

            $scope.orderListModel.is_next_autogenerator_num_changed = true;
            $timeout(function () {
                $scope.orderListModel.invoiceno = $scope.orderListModel.next_autogenerator_num;
            }, 0);
        }

        $scope.findDayCloseCredit = function () {
            var getListParam = {};
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.findDayCloseCredit(getListParam, configOption).then(function (response) {
                if (response.data.success === true)
                {
                    if (response.data.list.length > 0)
                    {
                        var data = response.data.list[0];
                        $scope.posModel.cardamount = data.cardAmount;
                    }
                }
            });
        };
        $scope.getAdvancepaymentList = function (id)
        {
            $scope.posModel.isLoadingProgress = true;
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'advancepayment';
            if (id != null && id != 'undefined' && id != '')
            {
                getListParam.customer_id = $scope.orderListModel.customerInfo.id;
            } else
            {
                getListParam.customer_id = '';
            }

            getListParam.start = 0
            getListParam.limit = 10;
            getListParam.is_active = 1;
            getListParam.status = $scope.searchFilter.status;
            getListParam.voucher_type = 'advance_invoice';
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getAdvancepaymentList(getListParam, configOption, headers).then(function (response)
            {

                var data = response.data;
                if (data.success)
                {
                    $scope.posModel.advanceList = data.list;
                    $scope.posModel.total = data.total;
                }
                $scope.posModel.isLoadingProgress = false;
            });
        };
        $scope.getCategoryList = function () {

            var getListParam = {};
            getListParam.id = '';
            $scope.posModel.categoryList = [];
            //getListParam.start = $scope.posModel.mstart;
            //getListParam.limit = 5;
            $scope.isCategoryLoaded = false;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getCategoryList(getListParam, configOption).then(function (response) {
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    var allcategory = {};
                    allcategory.name = 'All';
                    allcategory.id = 'All';
                    $scope.posModel.categoryList.push(allcategory);
                    if (data.length > 0)
                    {
                        for (var i = 0; i < data.length; i++)
                        {
                            $scope.posModel.categoryList.push(data[i]);
                        }
                    }
                    var uncategorized = {};
                    uncategorized.name = 'Uncategorized';
                    uncategorized.id = 'Uncategorized';
                    $scope.posModel.categoryList.push(uncategorized);
                }
                var categoryList = [];
                $scope.posModel.mtotal = response.data.total;
                $scope.isCategoryLoaded = true;
                //                if ($scope.posModel.categoryList.length > 0)
                //                {
                //                    $scope.getProductList($scope.posModel.categoryList[0].id);
                //                }
            });
        };
        $scope.getAccountlist = function () {

            var getListParam = {};
            getListParam.id = '';
            getListParam.pos_visibility = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getAccountlist(getListParam, configOption).then(function (response) {
                var data = response.data.list;
                $scope.posModel.accountsList = data;
            });
        };
        $scope.isPrefixListLoaded = false;
        $scope.getPrefixList = function ()
        {
            var getListParam = {};
            getListParam.setting = 'sales_invoice_prefix';
            var headers = {};
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getAppSettingsList(getListParam, configOption, headers).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    if (data.list.length > 0)
                    {
                        var perfix = data.list[0].value;
                        perfix = perfix.split('\n');
                        for (var i = 0; i < perfix.length; i++)
                        {
                            if (perfix[i].split('-')[1] == 'default')
                            {
                                perfix[i] = perfix[i].split('-')[0];
                                $scope.orderListModel.invoicePrefix = perfix[i] + '';
                            }

                            $scope.orderListModel.prefixList.push(perfix[i]);
                        }
                    }
                    $scope.getNextInvoiceno();

                }
                $scope.isPrefixListLoaded = true;
            });
        };
        $scope.getPrefixList();
        //   $scope.getProductList('All');
        $scope.getCategoryList();

//        $scope.getCustomerInfo();
        $scope.getCusBill = function (id)
        {
            var getBillListParam = {};
            var headers = {};
            getBillListParam.customer_id = id;
            getBillListParam.start = 0
            getBillListParam.limit = 5;
            getBillListParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getInvoiceList(getBillListParam, configOption, headers).then(function (response)
            {
                var data = response.data.list;
                $scope.posModel.cusBillList = data;
                for (var i = 0; i < $scope.posModel.cusBillList.length; i++)
                {
                    $scope.posModel.cusBillList[i].newdate = utilityService.parseStrToDate($scope.posModel.cusBillList[i].date);
                    $scope.posModel.cusBillList[i].date = utilityService.parseDateToStr($scope.posModel.cusBillList[i].newdate, $scope.dateFormat);
                }
            });
        };
        $scope.showDiscountPopup = false;
        $scope.showDiscountAddedPopup = function ()
        {
            $scope.showDiscountPopup = true;
        };
        $scope.closeDiscAddedPopup = function ()
        {
            //$scope.viewProduct();
            $scope.showDiscountPopup = false;
        };
        $scope.allproductsDiscount = false;
        $scope.discountApplytoAll = false;
        $scope.allProductDiscount = function ()
        {
            if ($scope.posModel.addedProducts.length == 0 || $scope.posModel.addedProducts == '')
            {
                sweet.show("Oops!", "Choose the product !", "error");
                $scope.posModel.allProductdiscount = "";
            } else {
//            $scope.selectedProduct = $scope.posModel.addedProducts;
//                $scope.applyDiscAllProducts = [];
//                $scope.selectedProduct = {};
//                $scope.allproductsDiscount = true;
//                $scope.selectedProduct.prodnewdiscount = $scope.posModel.allProductdiscount;
//                for (var i = 0; i < $scope.posModel.addedProducts.length; i++)
//                {
//                    $scope.selectedProduct.prodnewtotal = $scope.posModel.addedProducts[i].rowtotal;
//                    if ($scope.posModel.addedProducts[i].applyDiscount == true)
//                    {
//                        $scope.selectedProduct.prodnewqty = $scope.selectedProduct.prodnewdiscount;
//                    } else {
//                        $scope.selectedProduct.prodnewqty = $scope.posModel.addedProducts[i].qty;
//                    }
//                    $scope.selectedProduct.prodnewdesc = $scope.posModel.addedProducts[i].description;
//                    $scope.selectedProduct.newRevisedSellingPrice = $scope.posModel.addedProducts[i].revisedSellingPrice;
//                    $scope.selectedProduct.newDefaultSellingPrice = $scope.posModel.addedProducts[i].defaultSellingPrice;
//                    $scope.selectedProduct.purchase_invoice_item_id = $scope.posModel.addedProducts[i].purchase_invoice_item_id;
//                    $scope.selectedProduct.tierPricing = $scope.posModel.addedProducts[i].tierPricing;
//                    $scope.selectedProduct.prodnewtax = '0';
//                    $scope.discountApplytoAll = true;
//                    $scope.posModel.addedProducts[i].applyDiscount = true;
//                    if ($scope.posModel.addedProducts[i].taxMapping.length > 0)
//                    {
//                        var taxPer = 0;
//                        for (var j = 0; j < $scope.posModel.addedProducts[i].taxMapping.length; j++)
//                        {
//                            taxPer = taxPer + parseFloat($scope.posModel.addedProducts[i].taxMapping[j].tax_percentage);
//                        }
//                        $scope.selectedProduct.prodnewtax = taxPer;
//                    }
//                    $scope.applyDiscAllProducts.push($scope.selectedProduct);
//                }
//                $scope.discountApplytoAll = false;
//                $scope.showDiscountPopup = false;
                if ($scope.posModel.allProductdiscount < 100 && $scope.posModel.allProductdiscount >= 0)
                {
                    for (var i = 0; i < $scope.posModel.addedProducts.length; i++)
                    {
                        $scope.posModel.addedProducts[i].discountPercentage = $scope.posModel.allProductdiscount;
                    }
                    $scope.calculatetotal();
                    $scope.showDiscountPopup = false;
                }
            }
        };
        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'pos_search_data';
        $scope.$on('onLocalStorageFormRenterCompleteEvent', function (event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });
        $scope.$on('onLocalStorageFieldRenterCompleteEvent', function (event, formName) {

            if ($scope.localStorageFormName == formName)
            {
                $scope.localStorageRenterCount++;
            }
            if ($scope.localStorageRenterCount >= $scope.localStorageCount)
            {
                console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
            }


        });
        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function (event, formName) {

            if ($scope.localStorageFormName == formName)
            {
                $scope.localStorageRetrieveCount++;
            }
//            if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
//            {
//                $scope.initTableFilter();
//            }
            console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);
        });
        $scope.init = function ()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');
        };
        $scope.$on('onLocalStorageReadyEvent', function () {

            $timeout(function () {
                $scope.init();
            }, 300);
        });

        $scope.empDrop = function (index) {
            if (typeof $scope.posModel.addedProducts[index].empCode == 'undefined') {
                $scope.posModel.addedProducts[index].empCode = '';
            }
        };
    }]);








