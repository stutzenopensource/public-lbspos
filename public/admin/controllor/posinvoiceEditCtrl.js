


app.controller('posinvoiceEditCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$httpService', 'APP_CONST', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'sweet', '$window', '$http', function ($scope, $rootScope, adminService, utilityService, $httpService, APP_CONST, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, sweet, $window, $http) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.posModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            productList: [],
            serverList: null,
            isLoadingProgress: false,
            uncategorizedList: [],
            categoryList: [],
            accountsList: [],
            addedProducts: [],
            type: '',
            mcurrentPage: 1,
            mtotal: 0,
            mlimit: 5,
            mstart: 0,
            category: '',
            ttcount: 0,
            ttcount1: 0,
            ttcount2: 0,
            ttcount3: 0,
            ttcount4: 0,
            ttcount5: 0,
            ttcount6: 0,
            ttcount7: 0,
            ttcount8: 0,
            ttamount: '0.00',
            ttamount1: '0.00',
            ttamount2: '0.00',
            ttamount3: '0.00',
            ttamount4: '0.00',
            ttamount5: '0.00',
            ttamount6: '0.00',
            ttamount7: '0.00',
            ttamount8: '0.00',
            cashamount: '0.00',
            cardamount: '0.00',
            startdate: '',
            enddate: '',
            daycode: '',
            username: '',
            password: '',
            posCus: '',
            cusPurDetail: '',
            newproductname: '',
            newprodqty: '',
            newproddiscount: '',
            newprodprice: '',
            newprodnote: ''
        };
        $scope.currencyFormat = $rootScope.appConfig.currency;
        $scope.orderListModel =
                {
                    "id": 0,
                    "productname": "",
                    "sku": "",
                    "unit": "",
                    "mrp": "",
                    "selling_price": "",
                    "qty": "",
                    "rowtotal": "",
                    "isActive": "1",
                    "total": '0.00',
                    "customername": "",
                    "phone": "",
                    "addressId": "",
                    "subtotal": '0.00',
                    "taxAmt": '0.00',
                    "taxInfo": "",
                    "discountAmt": '0.00',
                    list: [],
                    "billno": "",
                    "billdate": "",
                    "coupon": "",
                    "shipping": 0,
                    "discamount": 0,
                    "code": "",
                    "customerInfo": '',
                    //                    "payeeInfo": '',
                    //                    "consigneeInfo": '',
                    "invoiceDetail": {},
                    "invoicePrefix": "",
                    "paymentTerm": "",
                    "taxPercentage": "",
                    "discountPercentage": "0",
                    "totalDisPercentage": "0",
                    "totalTaxPercentage": "",
                    "duedate": "",
                    "balance_amount": "",
                    "paid_amount": "",
                    "tax_id": "",
                    "productList": [],
                    "productId": '',
                    "notes": '',
                    "discountMode": '%',
                    "roundoff": '0.00',
                    "invoiceno": '',
                    "taxAmountDetail": {
                        "tax_amount": ''
                    },
                    //"taxAmountDetail": '',
                    "subtotalWithDiscount": '',
                    "round": '0.00',
                    "dealId": '',
                    "dealName": '',
                    "insuranceCharge": 0,
                    "insuranceAmt": '0.00',
                    "packagingCharge": '0',
                    "packagingAmt": '0.00',
                    sepdiscountAmt: '0.00',
                    sepsubtotal: '0.00',
                    septaxAmt: '0.00',
                    sepround: '0.00',
                    payamt: '',
                    paymentList: [],
                    cashAmt: '0.00',
                    cardAmt: '0.00',
                    sepCashAmt: '',
                    sepCardAmt: '',
                    cardcomments: '',
                    savePaymentList: []
                };
        $scope.validationFactory = ValidationFactory;
        $scope.searchFilter = {
            name: '',
            sku: '',
            code: '',
            category: ''
        };
        $scope.currentStep = 0;
        $scope.currentDate = new Date();
        $scope.orderListModel.billdate = $scope.currentDate;
        $scope.adminService = adminService;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.newProduct = '';
        $scope.indexInAP = '';
        $scope.selectedNewProduct = '';
        $scope.initTableFilterTimeoutPromise = null;
        $scope.posModel.productLoaded = false;
        $scope.posModel.categoryLoaded = false;
        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getProductList, 300);
        }
        $scope.initUpdateDetailTimeoutPromise = null;
        $scope.initUpdateDetail = function ()
        {
            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
            }
            if ($scope.posModel.categoryLoaded && $scope.posModel.productLoaded)
            {
                $scope.updateInvoiceDetails();
            } else
            {
                $scope.initUpdateDetailTimeoutPromise = $timeout($scope.initUpdateDetail, 300);
            }
        }
        $scope.getNavigationBlockMsg = function (pageReload)
        {
            if (typeof $scope.save_invite_form != 'undefined' && typeof $scope.save_invite_form.$pristine != 'undefined' && !$scope.save_invite_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.formReset = function () {

            $scope.save_invite_form.$setPristine();
            $scope.posModel = {
                currentPage: 1,
                total: 0,
                limit: 10,
                productList: [],
                serverList: null,
                isLoadingProgress: false,
                uncategorizedList: [],
                categoryList: [],
                addedProducts: [],
                type: '',
                mcurrentPage: 1,
                mtotal: 0,
                mlimit: 5,
                mstart: 0,
                category: '',
                ttcount: 0,
                ttcount1: 0,
                ttcount2: 0,
                ttcount3: 0,
                ttcount4: 0,
                ttcount5: 0,
                ttcount6: 0,
                ttcount7: 0,
                ttcount8: 0,
                ttamount: '0.00',
                ttamount1: '0.00',
                ttamount2: '0.00',
                ttamount3: '0.00',
                ttamount4: '0.00',
                ttamount5: '0.00',
                ttamount6: '0.00',
                ttamount7: '0.00',
                ttamount8: '0.00',
                cashamount: '0.00',
                cardamount: '0.00',
                startdate: '',
                enddate: '',
                daycode: '',
                username: '',
                password: '',
                posCus: '',
                newproductname: '',
                newprodqty: '',
                newproddiscount: '',
                newprodprice: '',
                newprodnote: ''
            };
            $scope.currencyFormat = $rootScope.appConfig.currency;
            $scope.orderListModel =
                    {
                        "id": 0,
                        "productname": "",
                        "sku": "",
                        "unit": "",
                        "mrp": "",
                        "selling_price": "",
                        "qty": "",
                        "rowtotal": "",
                        "isActive": "1",
                        "total": '0.00',
                        "customername": "",
                        "phone": "",
                        "addressId": "",
                        "subtotal": '0.00',
                        "taxAmt": '0.00',
                        "taxInfo": "",
                        "discountAmt": '0.00',
                        list: [],
                        "billno": "",
                        "billdate": "",
                        "coupon": "",
                        "shipping": 0,
                        "discamount": 0,
                        "code": "",
                        "customerInfo": '',
                        //                    "payeeInfo": '',
                        //                    "consigneeInfo": '',
                        "invoiceDetail": {},
                        "invoicePrefix": "",
                        "paymentTerm": "",
                        "taxPercentage": "",
                        "discountPercentage": "0",
                        "totalDisPercentage": "0",
                        "totalTaxPercentage": "",
                        "duedate": "",
                        "balance_amount": "",
                        "paid_amount": "",
                        "tax_id": "",
                        "productList": [],
                        "productId": '',
                        "notes": '',
                        "discountMode": '%',
                        "roundoff": '0.00',
                        "invoiceno": '',
                        "taxAmountDetail": {
                            "tax_amount": ''
                        },
                        //"taxAmountDetail": '',
                        "subtotalWithDiscount": '',
                        "round": '0.00',
                        "dealId": '',
                        "dealName": '',
                        "insuranceCharge": 0,
                        "insuranceAmt": '0.00',
                        "packagingCharge": '0',
                        "packagingAmt": '0.00',
                        sepdiscountAmt: '0.00',
                        sepsubtotal: '0.00',
                        septaxAmt: '0.00',
                        sepround: '0.00',
                        payamt: '',
                        paymentList: [],
                        cashAmt: '0.00',
                        cardAmt: '0.00',
                        sepCashAmt: '',
                        sepCardAmt: '',
                        totRevisedSellingRowTotal: '0',
                        septotRevisedSellingRowTotal: '0.00'
                    };
            $scope.posModel.email = '';
            $scope.getCategoryList();
            $scope.currentStep = 0;
            $scope.currentDate = new Date();
            $scope.orderListModel.billdate = $scope.currentDate;
            $scope.customername = '';
            $scope.phone = '';
            $scope.customerDetail = '';
            $scope.showcustomername = false;
            $scope.showcustomerpopup = false
        }

        $scope.showCustomerAddPopup = false;
        $scope.showAddCustomerPopup = function ()
        {
            $scope.showcustomerpopup = false;
            $scope.showCustomerAddPopup = true;
        }
        $scope.closeCustomerPopup = function ()
        {
            $scope.showCustomerAddPopup = false;
        }

        $scope.showPayPopup = false;
        $scope.showPaymentPopup = function ()
        {
            if ($scope.posModel.addedProducts.length > 0)
            {
                $scope.showPayPopup = true;
                $scope.showcustomerpopup = false;
                $scope.orderListModel.paymentList = [];
                $scope.orderListModel.payamt = $scope.orderListModel.round;
            } else
            {
                sweet.show('Oops...', 'Select a product.', 'error');
            }
        };
        $scope.closePaymentPopup = function ()
        {
            $scope.showPayPopup = false;
        };
        $scope.showOptions = false;
        $scope.openOptions = function ()
        {
            $scope.showOptions = !$scope.showOptions;
        }

        $scope.showcustomerpurchasepopup = false;
        $scope.showCusPurPopup = function ()
        {
            $scope.showcustomerpurchasepopup = true;
        }
        $scope.closeCusPurPopup = function ()
        {
            $scope.showcustomerpurchasepopup = false;
        }


        $scope.$on("updateSavedCustomerDetail", function (event, customerDetail)
        {
            if (typeof customerDetail === 'undefined')
                return;
            $scope.orderListModel.customerInfo = customerDetail;
            $scope.customerDetail = $scope.orderListModel.customerInfo;
            $scope.customername = $scope.orderListModel.customerInfo.fname;
            $scope.phone = $scope.orderListModel.customerInfo.phone;
            $scope.showcustomername = true;
            $scope.showcustomerpopup = false;
            $scope.orderListModel.customerInfo = '';

        });
        $scope.showAddScreen = false;
        $scope.showNewProductScreen = function ()
        {
            $scope.showAddScreen = true;
        }
        $scope.productExist = false;
        $scope.exitedIndex = 0;
        $scope.addProduct = function (prodid, invoiceid)
        {
            $scope.productExist = false;
            if ($scope.posModel.addedProducts.length > 0)
            {
                for (var i = 0; i < $scope.posModel.addedProducts.length; i++)
                {
                    $scope.posModel.addedProducts[i].newprod = false;
                }
                for (var i = 0; i < $scope.posModel.addedProducts.length; i++)
                {

                    if ($scope.posModel.addedProducts[i].purchase_invoice_item_id != undefined
                            && $scope.posModel.addedProducts[i].purchase_invoice_item_id != null
                            && $scope.posModel.addedProducts[i].purchase_invoice_item_id != 0
                            )
                    {
                        if (invoiceid == $scope.posModel.addedProducts[i].purchase_invoice_item_id
                                && prodid == $scope.posModel.addedProducts[i].id)
                        {
                            $scope.productExist = true;
                            $scope.exitedIndex = i;
                            $scope.stockqty = parseFloat($scope.posModel.addedProducts[i].qty_in_hand);
                        }
                    } else if (prodid == $scope.posModel.addedProducts[i].id)
                    {
                        $scope.productExist = true;
                        $scope.exitedIndex = i;
                        $scope.stockqty = $scope.posModel.addedProducts[i].qty_in_hand;
                    }
                    if (i == $scope.posModel.addedProducts.length - 1 && $scope.productExist)
                    {
                        if (($scope.posModel.addedProducts[$scope.exitedIndex].qty + 1) > $scope.stockqty)
                        {
                            $scope.newExistedProduct = $scope.posModel.addedProducts[$scope.exitedIndex];
                            $scope.showPopup(2);
                        } else
                        {
                            $scope.posModel.addedProducts[$scope.exitedIndex].newprod = true;
                            $scope.posModel.addedProducts[$scope.exitedIndex].qty = parseFloat($scope.posModel.addedProducts[$scope.exitedIndex].qty) + 1;
                            $scope.posModel.addedProducts[$scope.exitedIndex].revisedSellingPrice = parseFloat($scope.posModel.addedProducts[$scope.exitedIndex].revisedSellingPrice);
                            $scope.calculatetotal();
                        }
                    } else if (i == $scope.posModel.addedProducts.length - 1 && !$scope.productExist)
                    {
                        for (var j = 0; j < $scope.posModel.productList.length; j++)
                        {
                            if ($scope.posModel.productList[j].purchase_invoice_item_id != undefined
                                    && $scope.posModel.productList[j].purchase_invoice_item_id != null
                                    && $scope.posModel.productList[j].purchase_invoice_item_id != 0)
                            {
                                if (invoiceid == $scope.posModel.productList[j].purchase_invoice_item_id
                                        && prodid == $scope.posModel.productList[j].id)
                                {
                                    $scope.posModel.productList[j].qty = 1;
                                    $scope.posModel.productList[j].newprod = true;
                                    $scope.posModel.productList[j].rowtotal = 0;
                                    $scope.posModel.productList[j].newrowtotal = 0;
                                    $scope.posModel.productList[j].revisedSellingPrice = $scope.posModel.productList[j].selling_price;
                                    if ($scope.posModel.productList[j].defaultSellingPrice != undefined)
                                    {
                                        $scope.posModel.productList[j].defaultSellingPrice = $scope.posModel.productList[j].defaultSellingPrice;
                                    } else
                                    {
                                        $scope.posModel.productList[j].defaultSellingPrice = $scope.posModel.productList[j].mrp_price;
                                    }

                                    //                                    write the next three lines in a separate function and call if $scope.posModel.productList[j].qty_in_hand < 0
                                    // $scope.posModel.addedProducts.push($scope.posModel.productList[j]);
                                    //$scope.calculatetotal();
                                    if ($scope.posModel.productList[j].qty_in_hand < 0)
                                    {
                                        $scope.newProduct = $scope.posModel.productList[j];
                                        $scope.showPopup(1);
                                        $timeout($scope.updateAddProductCss, 100);
                                    } else
                                    {
                                        $scope.posModel.addedProducts.push($scope.posModel.productList[j]);
                                        $scope.updateAddProductCss();
                                        $scope.calculatetotal();
                                    }
                                    //                                    $scope.newProduct = $scope.posModel.productList[j];
                                    //                                    $scope.showPopup(1);
                                    //                                    $timeout($scope.updateAddProductCss, 100);
                                    return;
                                }
                            } else if (prodid == $scope.posModel.productList[j].id)
                            {
                                $scope.posModel.productList[j].qty = 1;
                                $scope.posModel.productList[j].newprod = true;
                                $scope.posModel.productList[j].rowtotal = 0;
                                $scope.posModel.productList[j].newrowtotal = 0;
                                $scope.posModel.productList[j].revisedSellingPrice = $scope.posModel.productList[j].selling_price;
                                if ($scope.posModel.productList[j].defaultSellingPrice != undefined)
                                {
                                    $scope.posModel.productList[j].defaultSellingPrice = $scope.posModel.productList[j].defaultSellingPrice;
                                } else
                                {
                                    $scope.posModel.productList[j].defaultSellingPrice = $scope.posModel.productList[j].selling_price;
                                }
                                //                                write the next three lines in a separate function and call if $scope.posModel.productList[j].qty_in_hand < 0
                                // $scope.posModel.addedProducts.push($scope.posModel.productList[j]);

                                if ($scope.posModel.productList[j].qty_in_hand < 0)
                                {
                                    $scope.newProduct = $scope.posModel.productList[j];
                                    $scope.showPopup(1);
                                    $timeout($scope.updateAddProductCss, 100);
                                } else
                                {
                                    $scope.posModel.addedProducts.push($scope.posModel.productList[j]);
                                    $scope.updateAddProductCss();
                                    $scope.calculatetotal();
                                }
                                return;
                            }
                        }
                    }
                }
            } else
            {
                if (invoiceid != undefined &&
                        invoiceid != null &&
                        invoiceid != 0)
                {
                    for (var i = 0; i < $scope.posModel.productList.length; i++)
                    {
                        if (invoiceid == $scope.posModel.productList[i].purchase_invoice_item_id
                                && prodid == $scope.posModel.productList[i].id)
                        {
                            if (parseFloat($scope.posModel.productList[i].qty_in_hand) >= 1)
                            {
                                $scope.posModel.productList[i].qty = 1;
                                $scope.posModel.productList[i].newprod = true;
                                $scope.posModel.productList[i].rowtotal = 0;
                                $scope.posModel.productList[i].newrowtotal = 0;
                                $scope.posModel.productList[i].revisedSellingPrice = $scope.posModel.productList[i].selling_price;
                                if ($scope.posModel.productList[i].defaultSellingPrice != undefined)
                                {
                                    $scope.posModel.productList[i].defaultSellingPrice = $scope.posModel.productList[i].defaultSellingPrice;
                                } else
                                {
                                    $scope.posModel.productList[i].defaultSellingPrice = $scope.posModel.productList[i].mrp_price;
                                }
                                $scope.posModel.addedProducts.push($scope.posModel.productList[i]);
                                $scope.updateAddProductCss();
                                $scope.calculatetotal();
                                break;
                            } else
                            {
                                $scope.newProduct = $scope.posModel.productList[i];
                                if ($scope.posModel.productList[i].defaultSellingPrice != undefined)
                                {
                                    $scope.newProduct.defaultSellingPrice = $scope.posModel.productList[i].defaultSellingPrice;
                                } else
                                {
                                    $scope.newProduct.defaultSellingPrice = $scope.posModel.productList[i].mrp_price;
                                }
                                $scope.newProduct.revisedSellingPrice = $scope.posModel.productList[i].selling_price;
                                $scope.showPopup(1);
                                $scope.updateAddProductCss();
                            }
                        }
                    }
                } else
                {
                    for (var i = 0; i < $scope.posModel.productList.length; i++)
                    {
                        if (prodid == $scope.posModel.productList[i].id)
                        {
                            if (parseFloat($scope.posModel.productList[i].qty_in_hand) >= 1)
                            {
                                $scope.posModel.productList[i].qty = 1;
                                $scope.posModel.productList[i].newprod = true;
                                $scope.posModel.productList[i].rowtotal = 0;
                                $scope.posModel.productList[i].newrowtotal = 0;
                                if ($scope.posModel.productList[i].defaultSellingPrice != undefined)
                                {
                                    $scope.posModel.productList[i].defaultSellingPrice = $scope.posModel.productList[i].defaultSellingPrice;
                                } else
                                {
                                    $scope.posModel.productList[i].defaultSellingPrice = $scope.posModel.productList[i].mrp_price;
                                }
                                $scope.posModel.productList[i].revisedSellingPrice = $scope.posModel.productList[i].defaultSellingPrice;
                                $scope.posModel.addedProducts.push($scope.posModel.productList[i]);
                                $scope.calculatetotal();
                            } else
                            {
                                $scope.newProduct = $scope.posModel.productList[i];
                                if ($scope.posModel.productList[i].defaultSellingPrice != undefined)
                                {
                                    $scope.newProduct.defaultSellingPrice = $scope.posModel.productList[i].defaultSellingPrice;
                                } else
                                {
                                    $scope.newProduct.defaultSellingPrice = $scope.posModel.productList[i].selling_price;
                                }
                                $scope.posModel.productList[i].revisedSellingPrice = $scope.posModel.productList[i].defaultSellingPrice;
                                $scope.showPopup(1);
                            }
                        }
                    }
                }

            }

            $timeout($scope.updateAddProductCss, 100);
        }

        $scope.updateAddProductCss = function ()
        {
            $scope.posModel.newprodqty = '';
            $scope.posModel.newprodqty = '';
            $scope.posModel.newproddiscount = '';
            $scope.posModel.newprodnote = '';
            $scope.posModel.newprodprice = '';
            $scope.posModel.defaultSellingPrice = '';
            $scope.posModel.newproductname = '';
            $scope.posModel.tierPricing = [];
            $scope.posModel.taxMapping = [];

            $timeout(function () {
                for (var i = 0; i < $scope.posModel.addedProducts.length; i++)
                {
                    $scope.posModel.addedProducts[i].cssClass = "pro-added";
                }
            }, 0);
        }
        $scope.opendayclosepopup = false;
        $scope.openDayClosePopup = function ()
        {
            $scope.showOptions = false;
            $scope.opendayclosepopup = true;
            $scope.findCurrentDaycode();
            $scope.findDayCloseCredit();
        }
        $scope.closeDayClosePopup = function ()
        {
            $scope.opendayclosepopup = false;
            $scope.posModel.ttcount = 0;
            $scope.posModel.ttcount1 = 0;
            $scope.posModel.ttcount2 = 0;
            $scope.posModel.ttcount3 = 0;
            $scope.posModel.ttcount4 = 0;
            $scope.posModel.ttcount5 = 0;
            $scope.posModel.ttcount6 = 0;
            $scope.posModel.ttcount7 = 0;
            $scope.posModel.ttcount8 = 0;
            $scope.posModel.ttamount = '0.00';
            $scope.posModel.ttamount1 = '0.00';
            $scope.posModel.ttamount2 = '0.00';
            $scope.posModel.ttamount3 = '0.00';
            $scope.posModel.ttamount4 = '0.00';
            $scope.posModel.ttamount5 = '0.00';
            $scope.posModel.ttamount6 = '0.00';
            $scope.posModel.ttamount7 = '0.00';
            $scope.posModel.ttamount8 = '0.00';
            $scope.posModel.cashamount = '0.00';
            $scope.posModel.cardamount = '0.00';

        }

        $scope.removeProduct = function ($index)
        {
            $scope.posModel.addedProducts.splice($index, 1);
            $scope.showProduct = false;
            $scope.calculatetotal();
        }
        $scope.newExistedProduct = '';
        $scope.showStockCheckPopup = false;
        $scope.showStockCheckPopup1 = false;
        $scope.showStockCheckPopup2 = false;
        $scope.showAdvancePopup = false;
        $scope.showPopup = function (id) {
            if (id == 1)
            {
                $scope.showStockCheckPopup = true;
            } else if (id == 2)
            {
                $scope.showStockCheckPopup1 = true;
            } else if (id == 3)
            {
                $scope.showStockCheckPopup2 = true;
            } else if (id == 4)
            {
                $scope.showAdvancePopup = true;
            }
        };
        $scope.closePopup = function () {

            $scope.showStockCheckPopup = false;
            $scope.showStockCheckPopup1 = false;
            $scope.showStockCheckPopup2 = false;
            $scope.showAdvancePopup = false;
        };
        $scope.updateChProDetails = function ()
        {
            if ($scope.indexInAP >= 0)
            {
                $scope.posModel.addedProducts[$scope.indexInAP].revisedSellingPrice = $scope.selectedNewProduct.newRevisedSellingPrice;
                $scope.posModel.addedProducts[$scope.indexInAP].qty = $scope.selectedNewProduct.prodnewqty;
                $scope.posModel.addedProducts[$scope.indexInAP].discountPercentage = $scope.selectedNewProduct.prodnewdiscount;
                $scope.posModel.addedProducts[$scope.indexInAP].description = $scope.selectedNewProduct.prodnewdesc;
                $scope.posModel.addedProducts[$scope.indexInAP].rowtotal = parseFloat($scope.selectedNewProduct.prodnewtotal);
                $scope.calculatetotal();
                $scope.closePopup();
            }
        }

        $scope.updateNewSelectedProduct = function ()
        {
            if ($scope.newExistedProduct != '')
            {
                $scope.newExistedProduct.newprod = true;
                $scope.newExistedProduct.qty = parseFloat($scope.newExistedProduct.qty) + 1;
                $scope.newExistedProduct.revisedSellingPrice = parseFloat($scope.newExistedProduct.revisedSellingPrice);
                $scope.calculatetotal();
                $scope.newExistedProduct = '';
                $scope.closePopup();
            }
        }

        $scope.addNewSelectedProduct = function ()
        {
            if ($scope.newProduct != '')
            {
                $scope.newProduct.qty = 1;
                $scope.newProduct.newprod = true;
                $scope.newProduct.rowtotal = 0;
                $scope.newProduct.newrowtotal = 0;
                $scope.posModel.addedProducts.push($scope.newProduct);
                $scope.updateAddProductCss();
                $scope.calculatetotal();
                $scope.newProduct = '';
                $scope.closePopup();
            }
        }

        $scope.showProduct = false;
        $scope.selectedProduct = {};
        $scope.viewProduct = function ($index)
        {
            $scope.showProduct = true;
            $scope.selectedProduct = $scope.posModel.addedProducts[$index];
            $scope.selectedProduct.prodnewtotal = $scope.posModel.addedProducts[$index].rowtotal;
            $scope.selectedProduct.prodnewqty = $scope.posModel.addedProducts[$index].qty;
            $scope.selectedProduct.prodnewdiscount = $scope.posModel.addedProducts[$index].discountPercentage;
            $scope.selectedProduct.prodnewdesc = $scope.posModel.addedProducts[$index].description;
            $scope.selectedProduct.newRevisedSellingPrice = $scope.posModel.addedProducts[$index].revisedSellingPrice;
            $scope.selectedProduct.prodnewtax = '0';
            if ($scope.posModel.addedProducts[$index].taxMapping.length > 0)
            {
                var taxPer = 0;
                for (var i = 0; i < $scope.posModel.addedProducts[$index].taxMapping.length; i++)
                {
                    taxPer = taxPer + parseFloat($scope.posModel.addedProducts[$index].taxMapping[i].tax_percentage);
                }
                $scope.selectedProduct.prodnewtax = taxPer;
            }
            $scope.updateProdDetail();
        }

        $scope.hideProductDetail = function ()
        {
            $scope.showProduct = false;
        }

        $scope.addProductDetail = function (prodtype)
        {
            if ($scope.selectedProduct.prodnewdiscount == '')
            {
                $scope.selectedProduct.prodnewdiscount = 0;
            }
            if (prodtype == 'new')
            {
                $scope.showAddScreen = false;
                var newpro = {};
                newpro.qty = parseFloat($scope.posModel.newprodqty);
                newpro.discountPercentage = parseFloat($scope.posModel.newproddiscount);
                newpro.description = $scope.posModel.newprodnote;
                newpro.selling_price = $scope.posModel.newprodprice;
                newpro.defaultSellingPrice = $scope.posModel.newprodprice;
                newpro.name = $scope.posModel.newproductname;
                newpro.tierPricing = [];
                newpro.taxMapping = [];
                newpro.product_id = '';
                $scope.posModel.addedProducts.push(newpro);
                $scope.calculatetotal();
                $timeout($scope.updateAddProductCss, 100);
            } else
            {
                $scope.showProduct = false;
                var foundProduct = false;
                for (var i = 0; i < $scope.posModel.addedProducts.length; i++)
                {
                    if ($scope.selectedProduct.purchase_invoice_item_id != undefined &&
                            $scope.selectedProduct.purchase_invoice_item_id != null
                            && $scope.selectedProduct.purchase_invoice_item_id != 0
                            && $scope.selectedProduct.id == $scope.posModel.addedProducts[i].id
                            && $scope.selectedProduct.purchase_invoice_item_id == $scope.posModel.addedProducts[i].purchase_invoice_item_id)
                    {
                        foundProduct = true;
                        if (parseFloat($scope.selectedProduct.qty_in_hand) < parseFloat($scope.selectedProduct.prodnewqty))
                        {
                            $scope.selectedNewProduct = $scope.selectedProduct;
                            $scope.indexInAP = i;
                            $scope.showPopup(3);
                            break;
                        } else
                        {
                            $scope.posModel.addedProducts[i].rowtotal = parseFloat($scope.selectedProduct.prodnewtotal);
                            $scope.posModel.addedProducts[i].qty = parseFloat($scope.selectedProduct.prodnewqty);
                            $scope.posModel.addedProducts[i].discountPercentage = parseFloat($scope.selectedProduct.prodnewdiscount);
                            $scope.posModel.addedProducts[i].description = $scope.selectedProduct.prodnewdesc;
                            $scope.posModel.addedProducts[i].revisedSellingPrice = parseFloat($scope.selectedProduct.newRevisedSellingPrice);
                            $scope.calculatetotal();
                        }
                    }
                }
                if (foundProduct == false)
                {
                    for (var i = 0; i < $scope.posModel.addedProducts.length; i++)
                    {
                        if ($scope.selectedProduct.id == $scope.posModel.addedProducts[i].id)
                        {
                            if (parseFloat($scope.selectedProduct.qty_in_hand) < parseFloat($scope.selectedProduct.prodnewqty))
                            {
                                $scope.selectedNewProduct = $scope.selectedProduct;
                                $scope.indexInAP = i;
                                $scope.showPopup(3);
                                break;
                            } else
                            {
                                $scope.posModel.addedProducts[i].revisedSellingPrice = parseFloat($scope.selectedProduct.newRevisedSellingPrice);
                                $scope.posModel.addedProducts[i].qty = parseFloat($scope.selectedProduct.prodnewqty);
                                $scope.posModel.addedProducts[i].discountPercentage = parseFloat($scope.selectedProduct.prodnewdiscount);
                                $scope.posModel.addedProducts[i].description = $scope.selectedProduct.prodnewdesc;
                                $scope.calculatetotal();
                            }
                        }
                    }
                }
                $scope.selectedProduct = {};
            }
        }

        $scope.hideNewProductScreen = function ()
        {
            $scope.showAddScreen = false;
        }
        $scope.qtyExist = false;
        $scope.updateProdDetail = function ()
        {
            if ($scope.selectedProduct.prodnewdiscount != null && $scope.selectedProduct.prodnewdiscount != 0)
            {
                $scope.qtyExist = false;
                if ($scope.selectedProduct.tierPricing.length > 0)
                {
                    for (var j = 0; j < $scope.selectedProduct.tierPricing.length; j++)
                    {
                        if (parseFloat($scope.selectedProduct.prodnewqty) >= parseFloat($scope.selectedProduct.tierPricing[j].from_qty) && parseFloat($scope.selectedProduct.prodnewqty) <= parseFloat($scope.selectedProduct.tierPricing[j].to_qty))
                        {
                            $scope.qtyExist = true;
                            $scope.selectedQtyIndex = j;
                        }
                    }
                    if ($scope.qtyExist)
                    {
                        $scope.selectedProduct.selling_price = parseFloat($scope.selectedProduct.tierPricing[$scope.selectedQtyIndex].unit_price);
                        $scope.selectedProduct.newrowtotal = parseFloat($scope.selectedProduct.selling_price) * parseFloat($scope.selectedProduct.prodnewqty);
                    } else if (!$scope.qtyExist)
                    {
                        $scope.selectedProduct.newrowtotal = parseFloat($scope.selectedProduct.defaultSellingPrice) * parseFloat($scope.selectedProduct.prodnewqty);
                    }

                } else
                {
                    $scope.selectedProduct.newrowtotal = parseFloat($scope.selectedProduct.defaultSellingPrice) * parseFloat($scope.selectedProduct.prodnewqty);
                }
                $scope.selectedProduct.newdiscountAmount = parseFloat((parseFloat($scope.selectedProduct.newrowtotal) * parseFloat($scope.selectedProduct.prodnewdiscount)) / 100);
                $scope.selectedProduct.prodnewdistotal = parseFloat($scope.selectedProduct.newrowtotal) - parseFloat($scope.selectedProduct.newdiscountAmount);
                if ($scope.selectedProduct.newdiscountAmount > 0)
                {
                    // find new selling price by reverse tax calculation for prodnewdistotal
                    $scope.selectedProduct.newRevisedSellingPrice = parseFloat($scope.selectedProduct.prodnewdistotal / (1 + (parseFloat($scope.selectedProduct.prodnewtax) / 100)));
                    $scope.selectedProduct.newRevisedSellingPrice = parseFloat($scope.selectedProduct.newRevisedSellingPrice / $scope.selectedProduct.prodnewqty);
                } else
                {
                    $scope.selectedProduct.newRevisedSellingPrice = parseFloat($scope.selectedProduct.revisedSellingPrice);
                }
                if ($scope.selectedProduct.prodnewtax > 0 && $scope.selectedProduct.refType == 'Product Based')//Check if product based or purchase base, allow if product based
                {
                    $scope.selectedProduct.prodnewtotal = parseFloat($scope.selectedProduct.prodnewdistotal) + parseFloat((parseFloat($scope.selectedProduct.prodnewdistotal) * parseFloat($scope.selectedProduct.prodnewtax)) / 100);
                } else
                {
                    $scope.selectedProduct.prodnewtotal = $scope.selectedProduct.prodnewdistotal;
                }
            } else
            {
                $scope.qtyExist = false;
                if ($scope.selectedProduct.tierPricing.length > 0)
                {
                    for (var j = 0; j < $scope.selectedProduct.tierPricing.length; j++)
                    {
                        if (parseFloat($scope.selectedProduct.prodnewqty) >= parseFloat($scope.selectedProduct.tierPricing[j].from_qty) && parseFloat($scope.selectedProduct.prodnewqty) <= parseFloat($scope.selectedProduct.tierPricing[j].to_qty))
                        {
                            $scope.qtyExist = true;
                            $scope.selectedQtyIndex = j;
                        }
                    }
                    if ($scope.qtyExist)
                    {
                        $scope.selectedProduct.selling_price = parseFloat($scope.selectedProduct.tierPricing[$scope.selectedQtyIndex].unit_price);
                        $scope.selectedProduct.newrowtotal = parseFloat($scope.selectedProduct.selling_price) * parseFloat($scope.selectedProduct.prodnewqty);
                        $scope.selectedProduct.prodnewtotal = parseFloat($scope.selectedProduct.newrowtotal);
                    } else if (!$scope.qtyExist)
                    {
                        $scope.selectedProduct.newrowtotal = parseFloat($scope.selectedProduct.defaultSellingPrice) * parseFloat($scope.selectedProduct.prodnewqty);

                        if ($scope.selectedProduct.prodnewtax > 0 && $scope.selectedProduct.refType == 'Product Based')
                        {
                            $scope.selectedProduct.prodnewtotal = parseFloat($scope.selectedProduct.newrowtotal) + parseFloat((parseFloat($scope.selectedProduct.newrowtotal) * parseFloat($scope.selectedProduct.prodnewtax)) / 100);
                        } else
                        {
                            $scope.selectedProduct.prodnewtotal = parseFloat($scope.selectedProduct.newrowtotal);
                        }
                    }

                } else
                {
                    $scope.selectedProduct.newrowtotal = parseFloat($scope.selectedProduct.defaultSellingPrice) * parseFloat($scope.selectedProduct.prodnewqty);
                    if ($scope.selectedProduct.prodnewtax > 0 && $scope.selectedProduct.refType == 'Product Based')
                    {
                        $scope.selectedProduct.prodnewtotal = parseFloat($scope.selectedProduct.newrowtotal) + parseFloat((parseFloat($scope.selectedProduct.newrowtotal) * parseFloat($scope.selectedProduct.prodnewtax)) / 100);
                    } else
                    {
                        $scope.selectedProduct.prodnewtotal = parseFloat($scope.selectedProduct.newrowtotal);
                    }
                }
                $scope.selectedProduct.newRevisedSellingPrice = parseFloat($scope.selectedProduct.selling_price);
            }
            $scope.selectedProduct.prodnewtotal = parseFloat($scope.selectedProduct.prodnewtotal).toFixed(2);
            if (isNaN($scope.selectedProduct.prodnewtotal))
            {
                $scope.selectedProduct.prodnewtotal = '0.00';
            }
        }
        $scope.sameQtyExist = false;
        $scope.taxList = [];
        $scope.calculatetotal = function ()
        {

            $scope.orderListModel.discountAmt = 0;
            $scope.orderListModel.taxAmt = 0;
            var subTotal = 0;
            var totDiscountPercentage = 0;
            var totTaxPercentage = 0;
            var totalTaxPercentage = 0;
            var totMapTax = 0;
            var totRevisedSellingRowTotal = 0;
            console.log("bfr loop", $scope.posModel.addedProducts.length);
            $scope.sameQtyExist = false;
            for (var i = 0; i < $scope.posModel.addedProducts.length; i++)
            {
                totRevisedSellingRowTotal += parseFloat($scope.posModel.addedProducts[i].revisedSellingPrice) * parseFloat($scope.posModel.addedProducts[i].qty);
                $scope.sameQtyExist = false;
                //  $scope.posModel.addedProducts[i].rowtotal = 0;
                //  $scope.posModel.addedProducts[i].newrowtotal = 0;
                if ($scope.posModel.addedProducts[i].tierPricing.length > 0)
                {
                    for (var j = 0; j < $scope.posModel.addedProducts[i].tierPricing.length; j++)
                    {
                        if ((parseFloat($scope.posModel.addedProducts[i].qty) >= parseFloat($scope.posModel.addedProducts[i].tierPricing[j].from_qty)) && (parseFloat($scope.posModel.addedProducts[i].qty) <= parseFloat($scope.posModel.addedProducts[i].tierPricing[j].to_qty)))
                        {
                            $scope.sameQtyExist = true;
                            $scope.selectedNewQtyIndex = j;
                            $scope.posModel.addedProducts[i].rowtotal = parseFloat($scope.posModel.addedProducts[i].tierPricing[j].unit_price) * parseFloat($scope.posModel.addedProducts[i].qty);
                        }
                    }
                    if ($scope.sameQtyExist)
                    {
                        $scope.posModel.addedProducts[i].selling_price = parseFloat($scope.posModel.addedProducts[i].tierPricing[$scope.selectedNewQtyIndex].unit_price);
                        $scope.posModel.addedProducts[i].rowtotal = parseFloat($scope.posModel.addedProducts[i].selling_price) * parseFloat($scope.posModel.addedProducts[i].qty);
                        //     $scope.posModel.addedProducts[i].newrowtotal = parseFloat($scope.posModel.addedProducts[i].selling_price) * parseFloat($scope.posModel.addedProducts[i].qty);

                    } else if (!$scope.sameQtyExist)
                    {
                        $scope.posModel.addedProducts[i].rowtotal = parseFloat($scope.posModel.addedProducts[i].defaultSellingPrice) * parseFloat($scope.posModel.addedProducts[i].qty);
                        //    $scope.posModel.addedProducts[i].newrowtotal = parseFloat($scope.posModel.addedProducts[i].selling_price) * parseFloat($scope.posModel.addedProducts[i].qty);
                    }

                } else
                {
                    $scope.posModel.addedProducts[i].rowtotal = parseFloat($scope.posModel.addedProducts[i].mrp_price) * parseFloat($scope.posModel.addedProducts[i].qty);
                }

                //  $scope.posModel.addedProducts[i].rowtotal = $scope.posModel.addedProducts[i].rowtotal.toFixed(2);
                $scope.posModel.addedProducts[i].taxPercentage = 0;
                if ($scope.posModel.addedProducts[i].taxMapping.length > 0)
                {
                    console.log('taxmaplength');
                    console.log($scope.posModel.addedProducts[i].taxMapping.length);
                    for (var j = 0; j < $scope.posModel.addedProducts[i].taxMapping.length; j++)
                    {
                        $scope.orderListModel.tax_id = $scope.posModel.addedProducts[i].tax_id;
                        $scope.posModel.addedProducts[i].taxPercentage += parseFloat($scope.posModel.addedProducts[i].taxMapping[j].tax_percentage);
                        $scope.posModel.addedProducts[i].tax_id = $scope.posModel.addedProducts[i].taxMapping[j].tax_id;
                    }
                } else
                {
                    $scope.posModel.addedProducts[i].taxPercentage = 0;
                }
                if ($scope.posModel.addedProducts[i].discountPercentage == null)
                {
                    $scope.posModel.addedProducts[i].discAmt = 0.00;
                    $scope.posModel.addedProducts[i].discountPercentage = 0;
                    // $scope.posModel.addedProducts[i].rowtotalwithdiscount = parseFloat($scope.posModel.addedProducts[i].rowtotal);
                } else
                {
                    //   $scope.posModel.addedProducts[i].rowtotal = $scope.posModel.addedProducts[i].rowtotal - ($scope.posModel.addedProducts[i].rowtotal * $scope.posModel.addedProducts[i].discountPercentage / 100);
                    $scope.posModel.addedProducts[i].discAmt = parseFloat($scope.posModel.addedProducts[i].mrp_price) * parseFloat($scope.posModel.addedProducts[i].qty) * parseFloat($scope.posModel.addedProducts[i].discountPercentage) / 100;
                    $scope.posModel.addedProducts[i].rowtotal = parseFloat($scope.posModel.addedProducts[i].rowtotal - $scope.posModel.addedProducts[i].discAmt);
                }
                if ($scope.posModel.addedProducts[i].taxPercentage == null)
                {
                    $scope.posModel.addedProducts[i].taxPercentage = 0;
                } else
                {
                    //   $scope.posModel.addedProducts[i].rowtotal = $scope.posModel.addedProducts[i].rowtotal - ($scope.posModel.addedProducts[i].rowtotal * $scope.posModel.addedProducts[i].discountPercentage / 100);
                    $scope.posModel.addedProducts[i].taxAmt = ((parseFloat($scope.posModel.addedProducts[i].selling_price) * parseFloat($scope.posModel.addedProducts[i].qty)) - (parseFloat($scope.posModel.addedProducts[i].discAmt))) * parseFloat($scope.posModel.addedProducts[i].taxPercentage) / 100;
                }
                $scope.posModel.addedProducts[i].rowtotal = parseFloat($scope.posModel.addedProducts[i].rowtotal).toFixed(2);
                $scope.posModel.addedProducts[i].seprowtotal = utilityService.changeCurrency($scope.posModel.addedProducts[i].rowtotal, $rootScope.appConfig.thousand_seperator);
                subTotal += parseFloat($scope.posModel.addedProducts[i].rowtotal);
                //  $scope.posModel.addedProducts[i].discountPercentage = parseInt($scope.posModel.addedProducts[i].discountPercentage);
            }
            $scope.orderListModel.totRevisedSellingRowTotal = parseFloat(totRevisedSellingRowTotal).toFixed(2);
            $scope.orderListModel.subtotal = parseFloat(subTotal).toFixed(2);
            for (var i = 0; i < $scope.posModel.addedProducts.length; i++)
            {
                $scope.orderListModel.discountAmt = parseFloat($scope.orderListModel.discountAmt) + parseFloat($scope.posModel.addedProducts[i].discAmt);
                $scope.orderListModel.taxAmt = parseFloat($scope.orderListModel.taxAmt) + parseFloat($scope.posModel.addedProducts[i].taxAmt);
            }
            if (isNaN($scope.orderListModel.discountAmt))
            {
                $scope.orderListModel.discountAmt = 0.00;
            } else
            {
                $scope.orderListModel.discountAmt = parseFloat($scope.orderListModel.discountAmt).toFixed(2);
            }
            if (isNaN($scope.orderListModel.taxAmt))
            {
                $scope.orderListModel.taxAmt = 0.00;
            } else
            {
                $scope.orderListModel.taxAmt = parseFloat($scope.orderListModel.taxAmt).toFixed(2);
            }

            if ($scope.orderListModel.insuranceCharge == '' || isNaN($scope.orderListModel.insuranceCharge))
            {
                $scope.orderListModel.insuranceAmt = '0.00';
            } else
            {
                $scope.orderListModel.insuranceAmt = parseFloat($scope.orderListModel.insuranceCharge).toFixed(2);
            }
            if ($scope.orderListModel.packagingCharge == '' || isNaN($scope.orderListModel.packagingCharge))
            {
                $scope.orderListModel.packagingAmt = '0.00';
            } else
            {
                $scope.orderListModel.packagingAmt = parseFloat($scope.orderListModel.packagingCharge).toFixed(2);
            }
            if ($scope.orderListModel.subtotal > 0)
            {
                $scope.orderListModel.subtotalWithDiscount = $scope.orderListModel.subtotal - $scope.orderListModel.discountAmt;
                $scope.orderListModel.total = (parseFloat(subTotal) + parseFloat($scope.orderListModel.insuranceAmt) + parseFloat($scope.orderListModel.packagingAmt)).toFixed(2);
            } else
            {
                $scope.orderListModel.subtotalWithDiscount = 0.00;
                $scope.orderListModel.total = 0.00;
            }
            $scope.orderListModel.balance_amount = (parseFloat($scope.orderListModel.total - $scope.orderListModel.paid_amount)).toFixed(2);
            $scope.updateRoundoff();
        }
        $scope.updateRoundoff = function ()
        {
            $scope.orderListModel.taxAmt = parseFloat($scope.orderListModel.total - $scope.orderListModel.totRevisedSellingRowTotal).toFixed(2);
            $scope.orderListModel.round = Math.round($scope.orderListModel.total);
            $scope.orderListModel.roundoff = $scope.orderListModel.round - parseFloat($scope.orderListModel.total);
            $scope.orderListModel.roundoff = parseFloat($scope.orderListModel.roundoff).toFixed(2);
            $scope.orderListModel.round = parseFloat($scope.orderListModel.round).toFixed(2);
            $scope.orderListModel.payamt = $scope.orderListModel.round;
            $scope.orderListModel.septotRevisedSellingRowTotal = utilityService.changeCurrency($scope.orderListModel.totRevisedSellingRowTotal, $rootScope.appConfig.thousand_seperator);
            $scope.orderListModel.sepdiscountAmt = utilityService.changeCurrency($scope.orderListModel.discountAmt, $rootScope.appConfig.thousand_seperator);
            $scope.orderListModel.sepsubtotal = utilityService.changeCurrency($scope.orderListModel.subtotal, $rootScope.appConfig.thousand_seperator);
            $scope.orderListModel.septaxAmt = utilityService.changeCurrency($scope.orderListModel.taxAmt, $rootScope.appConfig.thousand_seperator);
            $scope.orderListModel.sepround = utilityService.changeCurrency($scope.orderListModel.round, $rootScope.appConfig.thousand_seperator);
        }
        $scope.orderListModel.totalTax = 0;
        for (var i = 0; i < $scope.taxList.length; i++)
        {
            $scope.orderListModel.totalTax += $scope.taxList[i].taxPercentage;
        }
        $scope.orderListModel.totalTaxPercentage = $scope.orderListModel.totalTax;
        $scope.removePay = function ($index)
        {
            $scope.orderListModel.paymentList.splice($index, 1);
            $scope.updatePayment();
        }

        $scope.updatePayment = function (type, id)
        {
            $scope.totalPayAmt = 0;
            $scope.totalBalAmt = 0;
            if (type != undefined && type != '' && !isNaN($scope.orderListModel.payamt))
            {
                var pay = {};
                pay.account_id = id;
                pay.type = type;
                pay.amt = parseFloat($scope.orderListModel.payamt);
                pay.sepamt = '';
                if (type.toLowerCase() == 'card' && $scope.orderListModel.cardcomments != '')
                {
                    pay.comments = $scope.orderListModel.cardcomments;
                } else
                {
                    pay.comments = '';
                }
                $scope.orderListModel.paymentList.push(pay);
            }
            if ($scope.orderListModel.paymentList.length > 0)
            {
                for (var i = 0; i < $scope.orderListModel.paymentList.length; i++)
                {
                    $scope.orderListModel.paymentList[i].amt = parseFloat($scope.orderListModel.paymentList[i].amt).toFixed(2);
                    $scope.orderListModel.paymentList[i].sepamt = utilityService.changeCurrency($scope.orderListModel.paymentList[i].amt, $rootScope.appConfig.thousand_seperator);
                    $scope.totalPayAmt = parseFloat($scope.totalPayAmt) + parseFloat($scope.orderListModel.paymentList[i].amt);
                }
            }
            $scope.orderListModel.payamt = parseFloat($scope.orderListModel.round) - parseFloat($scope.totalPayAmt);
            $scope.orderListModel.payamt = parseFloat($scope.orderListModel.payamt).toFixed(2);
            if ($scope.orderListModel.payamt < 0)
            {
                $scope.totalBalAmt = parseFloat($scope.orderListModel.payamt).toFixed(2);
            }
            $scope.totalPayAmt = parseFloat($scope.totalPayAmt).toFixed(2);
            if ($scope.orderListModel.round == parseFloat($scope.totalPayAmt))
            {
                $scope.saveOrder();
            }
        }

        $scope.calculateTaxAmt = function (forceSave)
        {
            if (($scope.orderListModel.subtotalWithDiscount != 0 && $scope.orderListModel.subtotalWithDiscount != '' && $scope.orderListModel.subtotalWithDiscount >= 0) &&
                    ($scope.orderListModel.total != 0 && $scope.orderListModel.total != '' && $scope.orderListModel.total >= 0))
            {
                var getListParam = {};
                getListParam.amount = $scope.orderListModel.subtotalWithDiscount;
                getListParam.tax_id = $scope.orderListModel.tax_id;
                adminService.calculateTaxAmt(getListParam).then(function (response)
                {
                    var data = response.data;
                    $scope.orderListModel.taxAmountDetail = data;
                    $scope.orderListModel.taxAmt = parseFloat($scope.orderListModel.taxAmountDetail.tax_amount).toFixed(2);
                    $scope.orderListModel.subtotalWithDiscount = $scope.orderListModel.subtotal - $scope.orderListModel.discountAmt;
                    $scope.orderListModel.total = (parseFloat($scope.orderListModel.subtotal) + parseFloat($scope.orderListModel.taxAmt) - parseFloat($scope.orderListModel.discountAmt)).toFixed(2);
                    $scope.orderListModel.balance_amount = (parseFloat($scope.orderListModel.total - $scope.orderListModel.paid_amount)).toFixed(2);
                    $scope.updateRoundoff();
                    if (forceSave)
                    {
                        $scope.createOrder(forceSave);
                    }

                    console.log('taxamtdetail');
                    console.log($scope.orderListModel.taxAmountDetail);
                });
            } else
            {
                $scope.orderListModel.taxAmountDetail = '';
                $scope.orderListModel.taxAmt = parseFloat(0).toFixed(2);
                if ($scope.orderListModel.subtotal > 0)
                {
                    $scope.orderListModel.subtotalWithDiscount = $scope.orderListModel.subtotal - $scope.orderListModel.discountAmt;
                    $scope.orderListModel.total = (parseFloat($scope.orderListModel.subtotal) + parseFloat($scope.orderListModel.taxAmt) - parseFloat($scope.orderListModel.discountAmt)).toFixed(2);
                    $scope.orderListModel.balance_amount = (parseFloat($scope.orderListModel.total - $scope.orderListModel.paid_amount)).toFixed(2);
                    $scope.updateRoundoff();
                }
                if (forceSave)
                {
                    $scope.createOrder(forceSave);
                }
                console.log('taxamtdetail');
                console.log($scope.orderListModel.taxAmountDetail);
            }
        }

        $scope.createOrder = function (forceSave)
        {
            if (!forceSave)
            {
                if ($scope.isDataSavingProcess)
                {
                    return;
                }
            }
            $scope.isDataSavingProcess = true;
            if (typeof $scope.orderListModel.taxAmountDetail != undefined && $scope.orderListModel.taxAmountDetail != null && $scope.orderListModel.taxAmountDetail != '')
            {
                if ($scope.orderListModel.taxAmountDetail.tax_amount == $scope.orderListModel.taxAmt)
                {
                    $scope.saveOrder();
                } else
                {
                    $scope.calculateTaxAmt(true);
                }
            } else
            {
                $scope.calculateTaxAmt(true);
            }

        }

        $scope.enableShow = false;
        $scope.updateEnable = function ()
        {
            $scope.enableShow = !$scope.enableShow;
        }

        $scope.invoiceSavingProcess = false;
        $scope.saveOrder = function (onaccount)
        {
            if (onaccount == 'yes')
            {
                $scope.invoiceSavingProcess = true;
            } else
            {
                $scope.isDataSavingProcess = true;
            }
            var createOrderParam = {};
            var headers = {};
            headers['screen-code'] = 'salesinvoice';
            createOrderParam.quote_id = '';
            createOrderParam.prefix = $scope.orderListModel.invoicePrefix;
            createOrderParam.invoice_no = $scope.orderListModel.invoiceno;
            if (typeof $scope.orderListModel.billdate == 'object')
            {
                $scope.orderListModel.billdate = utilityService.parseDateToStr($scope.orderListModel.billdate, $scope.adminService.appConfig.date_format);
            }
            createOrderParam.date = utilityService.changeDateToSqlFormat($scope.orderListModel.billdate, $scope.adminService.appConfig.date_format);
            createOrderParam.datepaid = '';
            createOrderParam.duedate = utilityService.changeDateToSqlFormat($scope.orderListModel.billdate, $scope.adminService.appConfig.date_format);
            createOrderParam.customer_address = '';
            createOrderParam.subtotal = (parseFloat($scope.orderListModel.subtotal)).toFixed(2);
            createOrderParam.tax_amount = (parseFloat($scope.orderListModel.taxAmt)).toFixed(2);
            createOrderParam.discount_amount = (parseFloat($scope.orderListModel.discountAmt)).toFixed(2);
            createOrderParam.discount_percentage = $scope.orderListModel.discountPercentage;
            if (isNaN($scope.orderListModel.discountPercentage) || $scope.orderListModel.discountPercentage == null)
            {
                createOrderParam.discount_percentage = 0.00;
            }
            createOrderParam.discount_mode = '%';
            createOrderParam.total_amount = $scope.orderListModel.total;
            createOrderParam.round_off = (parseFloat($scope.orderListModel.roundoff)).toFixed(2);
            createOrderParam.paymentmethod = "";
            createOrderParam.notes = $scope.orderListModel.notes;
            createOrderParam.item = [];
            createOrderParam.item.outletId = 1;
            createOrderParam.item.status = 'initiated';
            if ($scope.customerDetail != undefined && $scope.customerDetail.fname != undefined)
            {
                createOrderParam.customer_id = $scope.customerDetail.id;
            } else
            {
                createOrderParam.customer_id = $rootScope.appConfig.poscusid;
            }
            createOrderParam.item.orderDetails = [];
            if ($scope.posModel.addedProducts.length < 1 || createOrderParam.total_amount <= 0)
            {
                return;
            }
            var length = 0;
            if ($scope.posModel.addedProducts.length == 1)
            {
                length = 2;
            } else
            {
                length = $scope.posModel.addedProducts.length + 1;
            }
            for (var i = 0; i < length - 1; i++)
            {
                var ordereditems = {};
                ordereditems.purchase_invoice_item_id = $scope.posModel.addedProducts[i].purchase_invoice_item_id;
                ordereditems.product_id = $scope.posModel.addedProducts[i].id;
                ordereditems.product_sku = $scope.posModel.addedProducts[i].sku;
                ordereditems.mrp_price = $scope.posModel.addedProducts[i].mrp_price;
                ordereditems.product_name = $scope.posModel.addedProducts[i].name;
                ordereditems.unit_price = parseFloat($scope.posModel.addedProducts[i].revisedSellingPrice);
                ordereditems.qty = parseInt($scope.posModel.addedProducts[i].qty);
                ordereditems.discount_mode = '%';
                ordereditems.discount_value = $scope.posModel.addedProducts[i].discountPercentage;
                ordereditems.comments = $scope.posModel.addedProducts[i].description;
                ordereditems.uom_id = $scope.posModel.addedProducts[i].uom_id;
                ordereditems.custom_opt1 = '';
                ordereditems.custom_opt2 = '';
                ordereditems.custom_opt3 = '';
                ordereditems.custom_opt4 = '';
                ordereditems.custom_opt5 = '';
                ordereditems.hsn_code = '';
                if ($scope.posModel.addedProducts[i].tax_id != undefined
                        && $scope.posModel.addedProducts[i].tax_id != null
                        && $scope.posModel.addedProducts[i].tax_id != '')
                {
                    ordereditems.tax_id = $scope.posModel.addedProducts[i].tax_id;
                } else
                {
                    ordereditems.tax_id = '';
                }
                ordereditems.total_price = $scope.posModel.addedProducts[i].selling_price * ordereditems.qty;
                createOrderParam.item.push(ordereditems);
            }
            createOrderParam.customattribute = [];
            createOrderParam.deal_id = '';
            createOrderParam.deal_name = '';
            adminService.editInvoice(createOrderParam, $scope.posModel.invoiceId, headers).then(function (response) {
                if (response.data.success == true)
                {
//                    if (onaccount == 'yes')
//                    {
//                        $scope.formReset();
//                        $scope.showPayPopup = false;
                    $state.go('app.pos', {
                        'reload': true
                    });
                    $scope.invoiceSavingProcess = false;
//                        $scope.getProductList('All');
//                        $scope.print(response.data.id);
//                    } else if ($scope.orderListModel.paymentList.length > 0)
//                    {
//                        $scope.groupPayment(response.data.id);
//                        // $scope.savePayment(response.data.id);
//                    }
                } else
                {
                    $scope.formReset();
                    $state.go('app.pos', {
                        'reload': true
                    });
                    $scope.isDataSavingProcess = false;
                    //$scope.getProductList('All');
                }

            });
        }
        $scope.invoiceId = '';
        $scope.cashAmount = 0;
        $scope.groupPayment = function (id)
        {
            var cashCount = 0;
            $scope.orderListModel.savePaymentList = [];
            $scope.invoiceId = id;
            for (var i = 0; i < $scope.orderListModel.paymentList.length; i++)
            {
                if ($scope.orderListModel.paymentList[i].type.toLowerCase() == 'cash')
                {
                    cashCount = cashCount + 1;
                    $scope.cashAmount = parseFloat($scope.cashAmount) + parseFloat($scope.orderListModel.paymentList[i].amt);
                } else
                {
                    $scope.orderListModel.savePaymentList.push($scope.orderListModel.paymentList[i]);
                }
                if (i == $scope.orderListModel.paymentList.length - 1)
                {
                    if ($scope.orderListModel.payamt < 0)
                    {
                        $scope.cashAmount = parseFloat($scope.cashAmount);
                    }
                    if (cashCount > 0)
                    {
                        var pay = {};
                        pay.type = 'Cash';
                        pay.amt = $scope.cashAmount;
                        pay.sepamt = '';
                        pay.account_id = $scope.orderListModel.paymentList[i].account_id;
                        pay.comments = '';
                        $scope.orderListModel.savePaymentList.push(pay);
                    }
                    $scope.savePayment($scope.invoiceId);
                }
            }
        }
        $scope.customername = '';
        $scope.phone = '';
        $scope.customerDetail = '';
        $scope.showcustomername = false;
        $scope.showcustomerpopup = false;
        $scope.showCustomer = function ()
        {
            $scope.showcustomerpopup = true;
            $timeout(function ()
            {
                $("#customerfocus").get(0).focus();
            }, 500);
        }
        $scope.closeCustomer = function ()
        {
            $scope.showcustomerpopup = false;
        }
        $scope.checkPosCustomer = function ()
        {
            if ($scope.orderListModel.customerInfo == '' || $scope.orderListModel.customerInfo.fname == undefined)
            {
                sweet.show('Oops', 'Please select customer', 'error');
            } else
            {
                $scope.saveOrder();
            }
        }

        $scope.selectCus = function ()
        {
            if ($scope.orderListModel.customerInfo != undefined && $scope.orderListModel.customerInfo.fname != undefined && $scope.orderListModel.customerInfo.fname != null)
            {
                $scope.customerDetail = $scope.orderListModel.customerInfo;
                $scope.customername = $scope.orderListModel.customerInfo.fname;
                $scope.phone = $scope.orderListModel.customerInfo.phone;
                if (parseInt($scope.orderListModel.customerInfo.id, 10) != parseInt($rootScope.appConfig.poscusid))
                {
                    $scope.showcustomername = true;
                } else
                {
                    $scope.showcustomername = false;
                }
                $scope.showcustomerpopup = false;
                $scope.getAdvancepaymentList($scope.orderListModel.customerInfo.id);
                $scope.getCusPurDetail($scope.orderListModel.customerInfo.id);
                $scope.getCusBill($scope.orderListModel.customerInfo.id);
                //  $scope.orderListModel.customerInfo = '';
            } else
            {
                $scope.showcustomername = false;
                $scope.showcustomerpopup = false;
            }

        }

        $scope.getCustomerInfo = function () {
            if ($rootScope.appConfig.poscusid != undefined && $rootScope.appConfig.poscusid != null)
            {
                var getListParam = {};
                getListParam.id = $rootScope.appConfig.poscusid;
                adminService.getCustomerDetail(getListParam).then(function (response) {
                    var data = response.data;
                    if (data.total != 0)
                    {
                        $scope.posModel.posCus = data.data;
                    }
                });
            }
        };

        $scope.getCusPurDetail = function (id) {

            var getListParam = {};
            getListParam.customer_id = id;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getCusPurDetail(getListParam, configOption).then(function (response) {
                var data = response.data.list;
                $scope.posModel.cusPurDetail = data;
                $scope.posModel.cusPurDetail.new_last_date_of_billing = utilityService.parseStrToDate($scope.posModel.cusPurDetail.last_date_of_billing);
                $scope.posModel.cusPurDetail.last_date_of_billing = utilityService.parseDateToStr($scope.posModel.cusPurDetail.new_last_date_of_billing, $scope.dateFormat);
                $scope.posModel.cusPurDetail.total_amount = parseFloat($scope.posModel.cusPurDetail.total_amount).toFixed(2);
            });
        };

        $scope.noData = false;
        $scope.getCustomerList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = [];
                if (data.length > 0)
                {
                    for (var i = 0; i < data.length; i++)
                    {
                        hits.push(data[i]);
                    }
                }
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                if (hits.length == 0)
                {
                    if ($scope.posModel.posCus != undefined && $scope.posModel.posCus != '')
                    {
                        hits.push($scope.posModel.posCus);
                    }
                    $scope.noData = true;
                }
                return hits;
            });
        };
        $scope.formatCustomerModel = function (model)
        {
            if (model != null)
            {
                if (model.fname != undefined && model.phone != undefined && model.email != undefined)
                {
                    return model.fname + '(' + model.phone + ',' + model.email + ')';
                } else if (model.fname != undefined && model.phone != undefined)
                {
                    return model.fname + '(' + model.phone + ')';
                } else if (model.fname != undefined)
                {
                    return model.fname;
                }
            }
            return  '';
        };
        $scope.print = function (id)
        {
            var landingUrl = $window.location.protocol + "//" + $window.location.host + "/public/printHelper.php?no=" + id;
            //   $window.location.href = landingUrl;
            $window.open(landingUrl, '_blank');
        }
        $scope.invoiceid = '';
        $scope.savePayment = function (id)
        {
            for (var i = 0; i < $scope.orderListModel.savePaymentList.length; i++)
            {
                $scope.invoiceid = id;
                var headers = {};
                headers['screen-code'] = 'salesinvoice';
                var configOption = adminService.handleBothSuccessAndErrorResponseConfig;
                var createPaymentParam = {};
                createPaymentParam.customer_id = '';
                if ($scope.customerDetail != undefined && $scope.customerDetail.fname != undefined)
                {
                    createPaymentParam.customer_id = $scope.customerDetail.id;
                } else if ($rootScope.appConfig.poscusid != undefined && $rootScope.appConfig.poscusid != null)
                {
                    createPaymentParam.customer_id = $rootScope.appConfig.poscusid;
                }
                createPaymentParam.comments = '';
                if (typeof $scope.orderListModel.billdate == 'object')
                {
                    $scope.orderListModel.billdate = utilityService.parseDateToStr($scope.orderListModel.billdate, $scope.adminService.appConfig.date_format);
                }
                createPaymentParam.date = utilityService.changeDateToSqlFormat($scope.orderListModel.billdate, $scope.adminService.appConfig.date_format);
                createPaymentParam.is_settled = 1;
                createPaymentParam.invoice_id = id;
                createPaymentParam.payment_mode = $scope.orderListModel.savePaymentList[i].type;
                createPaymentParam.cheque_no = '';
                createPaymentParam.tra_category = '';
                createPaymentParam.account = '';
                createPaymentParam.account_id = $scope.orderListModel.savePaymentList[i].account_id;
                createPaymentParam.comments = $scope.orderListModel.savePaymentList[i].comments;
                var configData = {};
                configData.paymentIndex = i;
                if ($scope.orderListModel.savePaymentList[i].amt > 0)
                {
                    createPaymentParam.amount = $scope.orderListModel.savePaymentList[i].amt;
                    adminService.createPayment(createPaymentParam, configData, configOption, headers).then(function (response)
                    {
                        var config = response.config.config.handleResponse;
                        if (config.paymentIndex == $scope.orderListModel.savePaymentList.length - 1)
                        {
                            if (response.data.success == true)
                            {
                                $scope.formReset();
                                $scope.showPayPopup = false;
                                $state.go('app.pos', {
                                    'reload': true
                                });
                                $scope.getProductList('All');
                                $scope.print($scope.invoiceid);
                            } else
                            {
                                $scope.showPayPopup = false;
                                $scope.isDataSavingProcess = false;
                            }
                        }
                        $scope.isDataSavingProcess = false;
                    });
                }
                $scope.isDataSavingProcess = false;
            }
        }
        $scope.enableTextArea = false;
        $scope.callEnableTextArea = function ()
        {
            $scope.enableTextArea = true;
        }

        $scope.tabChange = function (value) {

            $scope.currentStep = value;
        }

        $scope.event = null;
        $scope.keyupHandler = function (event)
        {
            if (event.keyCode != undefined && event.keyCode == 13 && $scope.searchFilter.name != '' && $scope.posModel.productList.length > 0)
            {
                $scope.addProduct($scope.posModel.productList[0].id, $scope.posModel.productList[0].purchase_invoice_item_id);
                $scope.searchFilter.name = ''
                $scope.getProductList();
            } else
            {
                return;
            }
        }

        $scope.getProductList = function (category) {

            $scope.posModel.productLoaded = false;
            var getProductListParam = {};
            $scope.posModel.productList = [];
            $scope.posModel.uncategorizedList = [];
            if (category != undefined)
            {
                $scope.posModel.category = category;
            }
            getProductListParam.search = $scope.searchFilter.name;
            getProductListParam.category = $scope.posModel.category;
            if ($scope.posModel.category == 'Uncategorized' || category == 'Uncategorized')
            {
                getProductListParam.category = '';
            } else if ($scope.posModel.category == 'All' || category == 'All')
            {
                getProductListParam.category = '';
            }
            $scope.posModel.isLoadingProgress = true;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getProductBasedList(getProductListParam, configOption).then(function (response) {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.posModel.productList = data.list;
                    if ($scope.posModel.category == 'Uncategorized')
                    {
                        for (var i = 0; i < $scope.posModel.productList.length; i++)
                        {
                            if ($scope.posModel.productList[i].category_id == 0 || $scope.posModel.productList[i].category_id == null || $scope.posModel.productList[i].category_id == 'undefined')
                            {
                                $scope.posModel.uncategorizedList.push($scope.posModel.productList[i]);
                                //  $scope.posModel.productList = $scope.posModel.uncategorizedList;
                            }
                            if (i == $scope.posModel.productList.length - 1)
                            {
                                $scope.posModel.productList = [];
                                $scope.posModel.productList = $scope.posModel.uncategorizedList;
                            }
                            // $scope.posModel.productList = $scope.posModel.uncategorizedList;
                        }

                    }
                    $scope.posModel.productLoaded = true;
                }
                $scope.posModel.isLoadingProgress = false;
            });
        };
        $scope.incrStartLimit = function ()
        {
            $scope.posModel.mstart = $scope.posModel.mstart + 5;
            $scope.posModel.mlimit = $scope.posModel.mlimit + 5;
            $scope.getCategoryList();
        }

        $scope.decrStartLimit = function ()
        {
            $scope.posModel.mstart = $scope.posModel.mstart - 5;
            $scope.posModel.mlimit = $scope.posModel.mlimit - 5;
            $scope.getCategoryList();
        }

        $scope.updateTotal = function ()

        {
            if (!isNaN($scope.posModel.ttcount) && !isNaN($scope.posModel.ttcount1) && !isNaN($scope.posModel.ttcount2)
                    && !isNaN($scope.posModel.ttcount3) && !isNaN($scope.posModel.ttcount4) && !isNaN($scope.posModel.ttcount5)
                    && !isNaN($scope.posModel.ttcount6) && !isNaN($scope.posModel.ttcount7)
                    && !isNaN($scope.posModel.ttcount8))
            {
                $scope.posModel.cashamount = 0;
                $scope.posModel.ttamount = parseFloat(2000 * $scope.posModel.ttcount).toFixed(2);
                $scope.posModel.ttamount1 = parseFloat(1000 * $scope.posModel.ttcount1).toFixed(2);
                $scope.posModel.ttamount2 = parseFloat(500 * $scope.posModel.ttcount2).toFixed(2);
                $scope.posModel.ttamount3 = parseFloat(100 * $scope.posModel.ttcount3).toFixed(2);
                $scope.posModel.ttamount4 = parseFloat(50 * $scope.posModel.ttcount4).toFixed(2);
                $scope.posModel.ttamount5 = parseFloat(20 * $scope.posModel.ttcount5).toFixed(2);
                $scope.posModel.ttamount6 = parseFloat(10 * $scope.posModel.ttcount6).toFixed(2);
                $scope.posModel.ttamount7 = parseFloat(5 * $scope.posModel.ttcount7).toFixed(2);
                $scope.posModel.ttamount8 = parseFloat(1 * $scope.posModel.ttcount8).toFixed(2);
                $scope.posModel.cashamount = parseFloat($scope.posModel.ttamount) + parseFloat($scope.posModel.ttamount1) +
                        parseFloat($scope.posModel.ttamount2) + parseFloat($scope.posModel.ttamount3) + parseFloat($scope.posModel.ttamount4)
                        + parseFloat($scope.posModel.ttamount5) + parseFloat($scope.posModel.ttamount6) + parseFloat($scope.posModel.ttamount7) +
                        parseFloat($scope.posModel.ttamount8);
                $scope.posModel.cashamount = parseFloat($scope.posModel.cashamount).toFixed(2);
            }
        }
        $scope.isDayCloseSavingProcess = false;
        $scope.saveDaycode = function ()
        {
            $scope.isDayCloseSavingProcess = true;
            var configOption = adminService.handleBothSuccessAndErrorResponseConfig;
            var createPaymentParam = {};
            createPaymentParam.user_id = '';
            createPaymentParam.cash = $scope.posModel.cashamount;
            createPaymentParam.credit = '';
            createPaymentParam.card = $scope.posModel.cardamount;
            createPaymentParam.others = '';
            createPaymentParam.advance = '';
            createPaymentParam.comments = "";
            createPaymentParam.denomination = [];
            if ($scope.posModel.ttcount > 0)
            {
                var data = {};
                data.unit = "2000.00";
                data.count = $scope.posModel.ttcount;
                data.amount = parseFloat(2000 * parseFloat($scope.posModel.ttcount));
                data.type = '1';
                data.comments = '';
                createPaymentParam.denomination.push(data);
            }
            if ($scope.posModel.ttcount1 > 0)
            {
                var data = {};
                data.unit = "1000.00";
                data.count = $scope.posModel.ttcount1;
                data.amount = parseFloat(1000 * parseFloat($scope.posModel.ttcount1));
                data.type = '2';
                data.comments = '';
                createPaymentParam.denomination.push(data);
            }
            if ($scope.posModel.ttcount2 > 0)
            {
                var data = {};
                data.unit = "500.00";
                data.count = $scope.posModel.ttcount2;
                data.amount = parseFloat(500 * parseFloat($scope.posModel.ttcount2));
                data.type = '3';
                data.comments = '';
                createPaymentParam.denomination.push(data);
            }
            if ($scope.posModel.ttcount3 > 0)
            {
                var data = {};
                data.unit = "100.00";
                data.count = $scope.posModel.ttcount3;
                data.amount = parseFloat(2000 * parseFloat($scope.posModel.ttcount3));
                data.type = '4';
                data.comments = '';
                createPaymentParam.denomination.push(data);
            }
            if ($scope.posModel.ttcount4 > 0)
            {
                var data = {};
                data.unit = "50.00";
                data.count = $scope.posModel.ttcount4;
                data.amount = parseFloat(2000 * parseFloat($scope.posModel.ttcount4));
                data.type = '5';
                data.comments = '';
                createPaymentParam.denomination.push(data);
            }
            if ($scope.posModel.ttcount5 > 0)
            {
                var data = {};
                data.unit = "20.00";
                data.count = $scope.posModel.ttcount5;
                data.amount = parseFloat(2000 * parseFloat($scope.posModel.ttcount5));
                data.type = '6';
                data.comments = '';
                createPaymentParam.denomination.push(data);
            }
            if ($scope.posModel.ttcount6 > 0)
            {
                var data = {};
                data.unit = "10.00";
                data.count = $scope.posModel.ttcount6;
                data.amount = parseFloat(2000 * parseFloat($scope.posModel.ttcount6));
                data.type = '7';
                data.comments = '';
                createPaymentParam.denomination.push(data);
            }
            if ($scope.posModel.ttcount7 > 0)
            {
                var data = {};
                data.unit = "5.00";
                data.count = $scope.posModel.ttcount7;
                data.amount = parseFloat(5 * parseFloat($scope.posModel.ttcount7));
                data.type = '8';
                data.comments = '';
                createPaymentParam.denomination.push(data);
            }
            if ($scope.posModel.ttcount8 > 0)
            {
                var data = {};
                data.unit = "1.00";
                data.count = $scope.posModel.ttcount8;
                data.amount = parseFloat(5 * parseFloat($scope.posModel.ttcount8));
                data.type = '9';
                data.comments = '';
                createPaymentParam.denomination.push(data);
            }
            adminService.saveDaycode(createPaymentParam, configOption).then(function (response)
            {
                var config = response.config.config.handleResponse;
                if (response.data.success == true)
                {
                    $scope.opendayclosepopup = false;
                    $state.go('app.pos', {
                        'reload': true
                    });
                    $scope.getProductList('All');
                    $scope.findCurrentDaycode();
                    $scope.findDayCloseCredit();
                }
                $scope.isDayCloseSavingProcess = false;
            });
        }
        $scope.findCurrentDaycode = function () {

            var getListParam = {};
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.findCurrentDaycode(getListParam, configOption).then(function (response) {
                if (response.data.success === true)
                {
                    if (response.data.list.length > 0)
                    {
                        var data = response.data.list[0];
                        console.log(typeof data.start_on);
                        var startdate = data.start_on.split(' ');
                        $scope.posModel.startdate = utilityService.parseStrToDate(startdate);
                        $scope.posModel.startdate = utilityService.parseDateToStr($scope.posModel.startdate, $scope.dateFormat);
                        var enddate = data.end_on.split(' ');
                        $scope.posModel.enddate = utilityService.parseStrToDate(enddate);
                        $scope.posModel.enddate = utilityService.parseDateToStr($scope.posModel.enddate, $scope.dateFormat);
                        $scope.posModel.daycode = data.id;
                    }
                }
            });
        };

        $scope.findDayCloseCredit = function () {
            var getListParam = {};
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.findDayCloseCredit(getListParam, configOption).then(function (response) {
                if (response.data.success === true)
                {
                    if (response.data.list.length > 0)
                    {
                        var data = response.data.list[0];
                        $scope.posModel.cardamount = data.cardAmount;
                    }
                }
            });
        };
        $scope.getAdvancepaymentList = function (id)
        {
            $scope.posModel.isLoadingProgress = true;
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'advancepayment';
            if (id != null && id != 'undefined' && id != '')
            {
                getListParam.customer_id = $scope.orderListModel.customerInfo.id;
            } else
            {
                getListParam.customer_id = '';
            }

            getListParam.start = 0
            getListParam.limit = 10;
            getListParam.is_active = 1;
            getListParam.status = $scope.searchFilter.status;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getAdvancepaymentList(getListParam, configOption, headers).then(function (response)
            {

                var data = response.data;
                if (data.success)
                {
                    $scope.posModel.advanceList = data.list;
                    $scope.posModel.total = data.total;
                }
                $scope.posModel.isLoadingProgress = false;
            });
        };


        $scope.getCategoryList = function () {
            $scope.posModel.categoryLoaded = false;
            var getListParam = {};
            getListParam.id = '';
            $scope.posModel.categoryList = [];
            //getListParam.start = $scope.posModel.mstart;
            //getListParam.limit = 5;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getCategoryList(getListParam, configOption).then(function (response) {
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    var allcategory = {};
                    allcategory.name = 'All';
                    allcategory.id = 'All';
                    $scope.posModel.categoryList.push(allcategory);
                    if (data.length > 0)
                    {
                        for (var i = 0; i < data.length; i++)
                        {
                            $scope.posModel.categoryList.push(data[i]);
                        }
                    }
                    var uncategorized = {};
                    uncategorized.name = 'Uncategorized';
                    uncategorized.id = 'Uncategorized';
                    $scope.posModel.categoryList.push(uncategorized);
                    $scope.posModel.categoryLoaded = true;
                }
                var categoryList = [];
                $scope.posModel.mtotal = response.data.total;
                //                if ($scope.posModel.categoryList.length > 0)
                //                {
                //                    $scope.getProductList($scope.posModel.categoryList[0].id);
                //                }
            });
        };
        $scope.getAccountlist = function () {

            var getListParam = {};
            getListParam.id = '';
            getListParam.pos_visibility = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getAccountlist(getListParam, configOption).then(function (response) {
                var data = response.data.list;
                $scope.posModel.accountsList = data;
            });

        };

        $scope.updateInvoiceDetails = function ()
        {
            $scope.posModel.customerInfo = {};
            $scope.posModel.id = $scope.posModel.invoiceDetail.id;
            $scope.posModel.invoiceId = $scope.posModel.invoiceDetail.id;
            //     $scope.posModel.addedProducts = $scope.posModel.invoiceDetail.item;
            $scope.posModel.customerInfo.id = $scope.posModel.invoiceDetail.customer_id;
            $scope.posModel.customerInfo.fname = $scope.posModel.invoiceDetail.customer_fname;
            $scope.orderListModel.customerInfo =
                    {
                        id: $scope.posModel.invoiceDetail.customer_id,
                        fname: $scope.posModel.invoiceDetail.customer_fname
                    }
            $scope.customername = $scope.posModel.invoiceDetail.customer_fname;
            $scope.posModel.customerInfo.address = $scope.posModel.invoiceDetail.customer_address;
            $scope.posModel.customerInfo.phone = $scope.posModel.invoiceDetail.phone;
            $scope.posModel.customerInfo.email = $scope.posModel.invoiceDetail.email;
            $scope.customerDetail = $scope.posModel.customerInfo;
            $scope.posModel.subtotal = $scope.posModel.invoiceDetail.subtotal;
            $scope.posModel.notes = $scope.posModel.invoiceDetail.notes;
            $scope.posModel.taxAmt = $scope.posModel.invoiceDetail.tax_amount;
            //$scope.posModel.taxAmountDetail.tax_amount = $scope.posModel.invoiceDetail.tax_amount;
            $scope.posModel.discountAmt = $scope.posModel.invoiceDetail.discount_amount;
            $scope.posModel.total = $scope.posModel.invoiceDetail.total_amount;
            $scope.posModel.paid_amount = $scope.posModel.invoiceDetail.paid_amount;
            $scope.posModel.balance_amount = (parseFloat($scope.posModel.total - $scope.posModel.paid_amount)).toFixed(2);
            $scope.posModel.invoiceCode = $scope.posModel.invoiceDetail.invoice_code;
            $scope.posModel.invoiceno = $scope.posModel.invoiceDetail.invoice_no;
            $scope.posModel.invoicePrefix = $scope.posModel.invoiceDetail.prefix;
            var idate = utilityService.parseStrToDate($scope.posModel.invoiceDetail.date);
            $scope.posModel.billdate = idate;
            var duedate = utilityService.parseStrToDate($scope.posModel.invoiceDetail.duedate);
            $scope.posModel.duedate = duedate;
            $scope.posModel.discountPercentage = $scope.posModel.invoiceDetail.discount_percentage + '';
            $scope.posModel.discountMode = $scope.posModel.invoiceDetail.discount_mode;
            $scope.posModel.tax_id = 'invoiced_tax_rate';
            $scope.customAttributeList = $scope.posModel.invoiceDetail.customattribute;
            var loop;
            //            $scope.posModel.taxMappingDetail = [];
            //            for (loop = 0; loop < $scope.posModel.invoiceDetail.tax_summary.length; loop++)
            //            {
            //                var tax = {};
            //                tax.tax_id = $scope.posModel.invoiceDetail.tax_summary[loop].tax_id;
            //                tax.taxName = $scope.posModel.invoiceDetail.tax_summary[loop].tax_name;
            //                tax.taxPercentage = $scope.posModel.invoiceDetail.tax_summary[loop].tax_percentage;
            //                tax.overallTaxAmount = parseFloat($scope.posModel.invoiceDetail.tax_summary[loop].amount);
            //                $scope.posModel.taxMappingDetail.push(tax);
            //            }

            for (loop = 0; loop < $scope.posModel.invoiceDetail.item.length; loop++)
            {
                var newRow =
                        {
                            "id": $scope.posModel.invoiceDetail.item[loop].product_id,
                            "invoice_item_id": $scope.posModel.invoiceDetail.item[loop].id,
                            "purchase_invoice_item_id": $scope.posModel.invoiceDetail.item[loop].purchase_invoice_item_id,
                            "productName": $scope.posModel.invoiceDetail.item[loop].product_name,
                            "product_name": $scope.posModel.invoiceDetail.item[loop].product_name,
                            "name": $scope.posModel.invoiceDetail.item[loop].product_name,
                            "sku": $scope.posModel.invoiceDetail.item[loop].product_sku,
                            "type": $scope.posModel.invoiceDetail.item[loop].type,
                            "sales_price": parseFloat($scope.posModel.invoiceDetail.item[loop].unit_price),
                            "selling_price": parseFloat($scope.posModel.invoiceDetail.item[loop].unit_price),
                            "revisedSellingPrice": parseFloat($scope.posModel.invoiceDetail.item[loop].unit_price),
                            "mrp_price": parseFloat($scope.posModel.invoiceDetail.item[loop].mrp_price),
                            "defaultSellingPrice": parseFloat($scope.posModel.invoiceDetail.item[loop].mrp_price),
                            "qty": parseFloat($scope.posModel.invoiceDetail.item[loop].qty),
                            "rowtotal": parseFloat(parseFloat($scope.posModel.invoiceDetail.item[loop].qty) * parseFloat($scope.posModel.invoiceDetail.item[loop].mrp_price)),
                            "discountPercentage": parseFloat($scope.posModel.invoiceDetail.item[loop].discount_value),
                            "discountAmount": parseFloat($scope.posModel.invoiceDetail.item[loop].discount_amount),
                            "discountMode": $scope.posModel.invoiceDetail.item[loop].discount_mode,
                            "taxPercentage": parseFloat($scope.posModel.invoiceDetail.item[loop].tax_percentage),
                            "tax_id": $scope.posModel.invoiceDetail.item[loop].tax_id + '',
                            "taxAmount": $scope.posModel.invoiceDetail.item[loop].tax_amount,
                            "uom": $scope.posModel.invoiceDetail.item[loop].uom_name,
                            "taxMapping": $scope.posModel.invoiceDetail.item[loop].tax_list,
                            "uomid": $scope.posModel.invoiceDetail.item[loop].uom_id

                                    //                            "custom_opt1": $scope.posModel.invoiceDetail.item[loop].custom_opt1,
                                    //                            "custom_opt2": $scope.posModel.invoiceDetail.item[loop].custom_opt2,
                                    //                            "custom_opt3": $scope.posModel.invoiceDetail.item[loop].custom_opt3,
                                    //                            "custom_opt4": $scope.posModel.invoiceDetail.item[loop].custom_opt4,
                                    //                            "custom_opt5": $scope.posModel.invoiceDetail.item[loop].custom_opt5,
                                    //                            
                                    //                            "hsn_code": $scope.posModel.invoiceDetail.item[loop].hsn_code
                        };

                $scope.posModel.addedProducts.push(newRow);
                var length = $scope.posModel.addedProducts.length - 1;
                $scope.posModel.addedProducts[length].productInfo = [];

                console.log('invoicelist');
                console.log($scope.posModel.list);
            }
            //            for (var index = 0; index < $scope.posModel.list.length; index++)
            //            {
            //                var taxparam = {};
            //                var taxName = '';
            //                taxparam.tax_id = $scope.posModel.list[index].tax_id;
            //                for (var i = 0; i < $scope.posModel.activeTaxList.length; i++)
            //                {
            //                    if ($scope.posModel.activeTaxList[i].id == $scope.posModel.list[index].tax_id)
            //                    {
            //                        $scope.posModel.list[index].taxPercentage = $scope.posModel.activeTaxList[i].tax_percentage;
            //                        taxName = $scope.posModel.activeTaxList[i].tax_name;
            //                    }
            //                }
            //                taxparam.tax_name = taxName;
            //                taxparam.tax_percentage = $scope.posModel.list[index].taxPercentage;
            //                $scope.posModel.list[index].taxMapping = [];
            //                $scope.posModel.list[index].taxMapping.push(taxparam);
            //            }
            for (var j = 0; j < $scope.posModel.addedProducts.length; j++)
            {
                for (var i = 0; i < $scope.posModel.productList.length; i++)
                {
                    if ($scope.posModel.addedProducts[j].id == $scope.posModel.productList[i].id && $scope.posModel.addedProducts[j].purchase_invoice_item_id == $scope.posModel.productList[i].purchase_invoice_item_id)
                    {
                        $scope.posModel.productList[i].newprod = false;
                        $scope.posModel.productList[i].rowtotal = $scope.posModel.addedProducts[j].rowtotal;
                        $scope.posModel.productList[i].newrowtotal = $scope.posModel.addedProducts[j].rowtotal;
                        $scope.posModel.addedProducts[j].qty_in_hand = $scope.posModel.productList[i].qty_in_hand;
                        $scope.posModel.addedProducts[j].tierPricing = $scope.posModel.productList[i].tierPricing;
                        $scope.posModel.addedProducts[j].refType = $scope.posModel.productList[i].refType;
                    }
                }
            }
            $scope.calculatetotal();
            $timeout($scope.updateAddProductCss, 100);
            $scope.posModel.isLoadingProgress = false;
        }

        $scope.getCusBill = function (id)
        {
            var getBillListParam = {};
            var headers = {};
            getBillListParam.customer_id = id;
            getBillListParam.start = 0
            getBillListParam.limit = 5;
            getBillListParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getInvoiceList(getBillListParam, configOption, headers).then(function (response)
            {
                var data = response.data.list;
                $scope.posModel.cusBillList = data;
                for (var i = 0; i < $scope.posModel.cusBillList.length; i++)
                {
                    $scope.posModel.cusBillList[i].newdate = utilityService.parseStrToDate($scope.posModel.cusBillList[i].date);
                    $scope.posModel.cusBillList[i].date = utilityService.parseDateToStr($scope.posModel.cusBillList[i].newdate, $scope.dateFormat);
                }
            });
        }

        $scope.getInvoiceInfo = function ()
        {
            $scope.posModel.isLoadingProgress = true;
            var getListParam = {};
            getListParam.id = $stateParams.id;
            getListParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getInvoiceDetail(getListParam, configOption).then(function (response)
            {
                var data = response.data.data;
                $scope.posModel.invoiceDetail = data;
                $scope.initUpdateDetail();
                //$scope.getTaxListInfo( );
            });
        }
        $scope.getProductList('All');
        $scope.getCategoryList();
        $scope.getCustomerInfo();
        $scope.getAccountlist();
        $scope.getInvoiceInfo();

    }]);








