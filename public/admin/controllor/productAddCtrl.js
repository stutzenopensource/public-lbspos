

app.controller('productAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', 'ValidationFactory', '$state', '$localStorageService', '$timeout', 'APP_CONST', '$window', '$http', function ($scope, $rootScope, adminService, $httpService, $filter, Auth, ValidationFactory, $state, $localStorageService, $timeout, APP_CONST, $window, $http) {

        $scope.productModel = {
            id: '',
            name: '',
            sku: '',
            description: '',
            uomList: [],
            uomName: "",
            salesPrice: '',
            category_id: '',
            isActive: true,
            isPurchase: true,
            isSale: true,
            hasinventory: false,
            minstock: '',
            openstock: '',
            hsncode: '',
            hsnList: [],
            hsncodeTaxinfo: [],
            attachment: []
        }

        $scope.validationFactory = ValidationFactory;

        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function ()
        {
            if (typeof $scope.product_add_form != 'undefined' && typeof $scope.product_add_form.$pristine != 'undefined' && !$scope.product_add_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function ()
        {
            $scope.product_add_form.$setPristine();
            if (typeof $scope.product_add_form != 'undefined')
            {
                $scope.productModel.name = "";
                $scope.productModel.sku = "";
                $scope.productModel.salesPrice = '';
                $scope.productModel.description = "";
                $scope.productModel.uomName = "";
                $scope.productModel.isActive = true;
                $scope.productModel.isSale = true;
                $scope.productModel.isPurchase = true;
                $scope.productModel.hasinventory = '';
                $scope.productModel.minstock = '';
                $scope.productModel.id = '';
                $scope.productModel.openstock = '';
                $scope.productModel.hsncode = '';
            }
        }
        $scope.getcategoryList = function ()
        {
            var getListParam = {};
            getListParam.id = "";
            getListParam.category_id = "";
            getListParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getCategoryList(getListParam, configOption).then(function (response)
            {
                var data = response.data;
                $scope.productModel.categorylist = data.list;

            });
        };

        $scope.clearFilters = function ()
        {
            $scope.searchFilter = {
                hsn_code: '',
                search: ''
            };
            $scope.productModel.hsnList = [];
        };
        $scope.searchFilter = {
            hsn_code: '',
            search: ''
        };


        $scope.getCUOMNameList = function ()
        {
            var getListParam = {};
            getListParam.id = "";
            getListParam.name = "";
            getListParam.start = ($scope.productModel.currentPage - 1) * $scope.productModel.limit;
            getListParam.limit = $scope.productModel.limit;
            $scope.isDataSavingProcess = true;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getUom(getListParam, configOption).then(function (response)
            {
                var data = response.data;
                $scope.productModel.uomList = data.list;
                $scope.isDataSavingProcess = false;
            });
        };

        $scope.getCUOMNameList( );
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.addProductInfo = function () {

            if ($scope.isDataSavingProcess)
            {
                return;
            }

            $scope.isDataSavingProcess = true;
            var addProductParam = {};
            var headers = {};
            headers['screen-code'] = 'product';
            // addProductParam.id = 0;
            addProductParam.name = $scope.productModel.name;
            addProductParam.sku = $scope.productModel.sku;
            addProductParam.hsn_code = $scope.productModel.hsncode;
            addProductParam.hsn_id = $scope.hsn_id;
            addProductParam.sales_price = parseFloat($scope.productModel.salesPrice);
            addProductParam.description = $scope.productModel.description;
            addProductParam.category_id = $scope.productModel.id;
            addProductParam.attachment = [];
            addProductParam.type = "Product";
            // addProductParam.uom = $scope.productModel.uomName;
            if ($scope.productModel.uomName != "")
            {
                for (var loopIndex = 0; loopIndex < $scope.productModel.uomList.length; loopIndex++)
                {
                    if ($scope.productModel.uomName == $scope.productModel.uomList[loopIndex].name)
                    {
                        addProductParam.uom_id = $scope.productModel.uomList[loopIndex].id;
                        break;
                    }
                }
            } else
            {
                addProductParam.uom_id = "";
            }
            addProductParam.is_active = 1;
            //addProductParam.is_active = $scope.productModel.isActive == true ? 1 : 0;
            addProductParam.is_purchase = $scope.productModel.isPurchase == true ? 1 : 0;
            addProductParam.is_sale = $scope.productModel.isSale == true ? 1 : 0;
            addProductParam.min_stock_qty = $scope.productModel.minstock;
            addProductParam.opening_stock = $scope.productModel.openstock;
            addProductParam.has_inventory = $scope.productModel.hasinventory == true ? 1 : 0;
            var configOption = adminService.handleBothSuccessAndErrorResponseConfig;
            adminService.addProduct(addProductParam, configOption, headers).then(function (response)
            {
                if (response.data.success == true)
                {
                    $scope.saveHsnCode();
                    $scope.showAlert = false;
                    $scope.formReset();
                    $state.go('app.productEdit', {id: response.data.id}, {'reload': true});
                }
                $scope.isDataSavingProcess = false;
            });
        };

        $scope.productModel.isLoadingProgress = false;

        $scope.getHsnList = function () {
            $http.get('http://sso2.accozen.com/public/data.php').success(function (data) {
//            $.getJSON('http://sso2.accozen.com/public/data.php', function (data) {
                $scope.hsnList = data;
                $scope.productModel.hsnList = [];
                for (var i = 0; i < $scope.hsnList.length; i++)
                {
        //                if ($scope.hsnList[i].hsn_code == $scope.searchFilter.hsn_code)
        //                {
                        $scope.productModel.hsnList.push($scope.hsnList[i]);
        //                }
                }
                var element = document.getElementById("hsn_dropdown");
                element.classList.remove("has-error"); 
                var element1 = document.getElementById("desc");
                element1.classList.remove("has-error");
            });
        };
        $scope.addHsnInfo = function (prodcut) {
            $scope.clearFilters();
            $scope.closeHsnPopup();
            $scope.productModel.hsnList = [];
            $scope.productModel.hsnCodeInfo = prodcut;
            $scope.productModel.hsncode = prodcut.hsn_code;
            $scope.hsn_id = prodcut.id;
            $scope.productModel.hsncodeTaxinfo = [];
            if (prodcut.hsnTaxMapping.length > 0 && prodcut.hsnTaxMapping != null && typeof prodcut.hsnTaxMapping != "undefined")
            {
                for (var i = 0; i < prodcut.hsnTaxMapping.length; i++)
                {
                    $scope.productModel.hsncodeTaxinfo.push(prodcut.hsnTaxMapping[i]);
                }
            }
        }
        
//        $scope.addHsnInfo = function (index){
//            $scope.productModel.hsncode = $scope.productModel.hsnList[index].hsn_code;
//            $scope.closeHsnPopup();
//        };

        $scope.showHSNDetail = false;
        $scope.showHsnPopup = function ()
        {
            $scope.showHSNDetail = true;
        }
        $scope.closeHsnPopup = function ()
        {
            $scope.showHSNDetail = false;
        }
        $scope.saveHsnCode = function () {
            var createHSNParam = {};
            var headers = {};
            headers['screen-code'] = 'service';
            createHSNParam.hsn_code = $scope.productModel.hsnCodeInfo.hsn_code;
            createHSNParam.description = $scope.productModel.hsnCodeInfo.description;
            createHSNParam.Type = $scope.productModel.hsnCodeInfo.type;
            createHSNParam.is_approved = $scope.productModel.hsnCodeInfo.is_approved;
            createHSNParam.is_active = $scope.productModel.hsnCodeInfo.is_active;
            createHSNParam.hsnTaxMapping = [];
            for (var i = 0; i < $scope.productModel.hsnCodeInfo.hsnTaxMapping.length; i++)
            {
                var taxMap = {
                    group_tax_id: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].group_tax_id,
                    from_price: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].from_price,
                    to_price: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].to_price,
                    group_tax_ids: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].group_tax_ids,
                    intra_group_tax_ids: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].intra_group_tax_ids
                };
                createHSNParam.hsnTaxMapping.push(taxMap);
            }
            createHSNParam.taxDetail = [];
            for (var i = 0; i < $scope.productModel.hsnCodeInfo.hsnTaxMapping.length; i++)
            {
                if ($scope.productModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail.length > 0)
                {
                    for (var j = 0; j < $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail.length; j++)
                    {
                        var taxDetail =
                                {
                                    sso_tax_id: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].id,
                                    tax_no: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].tax_no,
                                    tax_name: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].tax_name,
                                    tax_percentage: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].tax_percentage,
                                    is_group: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].is_group,
                                    tax_type: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].tax_type,
                                    is_active: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].is_active,
                                    comments: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].comments,
                                    is_display: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].is_display,
                                    hsn_flag: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].hsn_flag,
                                    intra_group_tax_ids: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].intra_group_tax_ids
                                };
                        createHSNParam.taxDetail.push(taxDetail);
                    }
                }
                if ($scope.productModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail.length > 0)
                {
                    for (var j = 0; j < $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail.length; j++)
                    {
                        var taxDetail =
                                {
                                    sso_tax_id: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].id,
                                    tax_no: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].tax_no,
                                    tax_name: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].tax_name,
                                    tax_percentage: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].tax_percentage,
                                    is_group: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].is_group,
                                    tax_type: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].tax_type,
                                    is_active: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].is_active,
                                    comments: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].comments,
                                    is_display: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].is_display,
                                    hsn_flag: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].hsn_flag,
                                    intra_group_tax_ids: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].intra_group_tax_ids
                                };
                        createHSNParam.taxDetail.push(taxDetail);
                    }
                }
                var detail =
                        {
                            sso_tax_id: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].group_tax_id,
                            tax_no: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].tax_no,
                            tax_name: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].tax_name,
                            tax_percentage: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].tax_percentage,
                            is_group: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].is_group,
                            tax_type: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].tax_type,
                            is_active: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].is_active,
                            comments: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].comments,
                            is_display: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].is_display,
                            hsn_flag: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].hsn_flag,
                            group_tax_ids: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].group_tax_ids,
                            intra_group_tax_ids: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].intra_group_tax_ids
                        }
                createHSNParam.taxDetail.push(detail);
//                createHSNParam.hsnTaxMapping.push(taxMap);
            }
            createHSNParam.is_sso = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.saveHSNCode(createHSNParam, configOption, headers).then(function (response)
            {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.productList');
                }
                if (response.data.success == false)
                {
                    $scope.formReset();
                    $state.go('app.productList');
                }
                $scope.isDataSavingProcess = false;
            });
        }

        $scope.getcategoryList();
    }]);
