app.controller('productEditCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', 'ValidationFactory', '$state', '$localStorageService', '$timeout', 'APP_CONST', '$stateParams', 'sweet', '$window', '$http', function ($scope, $rootScope, adminService, $httpService, $filter, Auth, ValidationFactory, $state, $localStorageService, $timeout, APP_CONST, $stateParams, sweet, $window, $http) {

        $scope.productModel = {
            id: '',
            name: '',
            sku: '',
            description: '',
            uomList: [],
            salesPrice: '',
            taxMapping: [],
            category_id: '',
            is_active: '',
            is_purchase: '',
            is_sale: '',
            uom: '',
            isLoadingProgress: false,
            isSearchLoadingProgress: false,
            minstock: '',
            openstock: '',
            hasinventory: '',
            tax: '',
            mappedTax: '',
            tierPriceList: [],
            deletedProducts: [],
            hsncode: '',
            hsnList: [],
            hsncodeTaxinfo: [],
            hsnDetails :[],
            attachments: []
        }
        $scope.productDetail = {};
        $scope.validationFactory = ValidationFactory;
        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function ()
        {
            if (typeof $scope.product_edit_form != 'undefined' && typeof $scope.product_edit_form.$pristine != 'undefined' && !$scope.product_edit_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function ()
        {
            $scope.product_edit_form.$setPristine();
            if (typeof $scope.product_edit_form != 'undefined')
            {
                $scope.productModel.name = "";
                $scope.productModel.sku = "";
                $scope.productModel.id = "";
                $scope.productModel.salesPrice = '';
                $scope.productModel.description = "";
                $scope.productModel.is_active = '';
                $scope.productModel.is_purchase = '';
                $scope.productModel.is_sale = '';
                $scope.productModel.has_inventory = '';
                $scope.productModel.taxMapping = '';
                // $scope.productModel.uom = '';
                $scope.productModel.hsncode = '';

            }
            $scope.updateProductInfo();
//            $scope.getTierPriceMappingList();
        };
        $scope.searchFilter = {
            hsn_code: '',
            search: ''
        };
        $scope.clearFilters = function ()
        {
            $scope.searchFilter = {
                hsn_code: '',
                search: ''
            };
            //$scope.initTableFilter();
            $scope.productModel.hsnList = [];
            $scope.msgflag = false;
        };

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.initUpdateDetailTimeoutPromise = null;
        $scope.initUpdateDetail = function ()
        {
            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
            }

            if ($scope.isUOMLoaded && $scope.isCategoryLoaded)
            {
                $scope.updateProductInfo();
//                $scope.productModel.isLoadingProgress = false;
            } else
            {
                $scope.initUpdateDetailTimeoutPromise = $timeout($scope.initUpdateDetail, 300);
            }
        }
        $scope.updateProductInfo = function ()
        {
            $scope.productModel.id = $scope.productDetail.id;
            $scope.productModel.is_active = 1;
            //$scope.productModel.is_active = $scope.productModel.is_active == 1 ? true : false;
            $scope.productModel.is_sale = $scope.productDetail.is_sale == 1 ? true : false;
            $scope.productModel.is_purchase = $scope.productDetail.is_purchase == 1 ? true : false;
            $scope.productModel.openstock = $scope.productDetail.opening_stock;
            $scope.productModel.hasinventory = $scope.productDetail.has_inventory == 1 ? true : false;
            $scope.productModel.minstock = $scope.productDetail.min_stock_qty;
            $scope.productModel.name = $scope.productDetail.name;
            $scope.productModel.category_id = $scope.productDetail.category_id + '';
            $scope.productModel.sku = $scope.productDetail.sku;
            $scope.productModel.uom = $scope.productDetail.uom;
            $scope.productModel.sales_price = $scope.productDetail.sales_price;
            $scope.productModel.description = $scope.productDetail.description;
            $scope.productModel.hsncode = $scope.productDetail.hsn_code;
            $scope.hsn_id = $scope.productDetail.hsn_id;
            $scope.productModel.attachments = $scope.productModel.productListDetail.attachment;
            if (typeof $scope.productModel.attachments != "undefined" && $scope.productModel.attachments.length > 0)
            {
                for (var i = 0; i < $scope.productModel.attachments.length; i++)
                {
                    $scope.productModel.attachments[i].path = $scope.productModel.attachments[i].url;
                }
            }
            $scope.productModel.taxMapping = $scope.productDetail.taxMapping;
            if ($scope.productModel.taxMapping.length > 0)
            {
                for (var i = 0; i < $scope.productModel.taxMapping.length; i++)
                {
                    if ($scope.productModel.taxMapping[i].id != 0)
                    {
                        $scope.productModel.tax = $scope.productModel.taxMapping[i].tax_id + '';
                        $scope.productModel.mappedTax = $scope.productModel.taxMapping[i].id;
                    }
                }
            }
//            $.getJSON('http://sso2.accozen.com/public/data.php', function (data) {
//                $scope.hsnList = data;
//                $scope.productModel.hsnList = [];
//                
//            });
            $scope.setCheckedItems();
        }

        $scope.addNewRow = function ()
        {
            var tierprice = {};
            tierprice.id = 0;
            tierprice.from_qty = '';
            tierprice.to_qty = '';
            tierprice.unit_price = '';
            $scope.productModel.tierPriceList.push(tierprice);
        }

        $scope.deleteTierPrice = function (index, id)
        {
            if (id == 0)
            {
                $scope.productModel.tierPriceList.splice(index, 1);
            }
            if (id > 0)
            {
                $scope.productModel.deletedProducts.push($scope.productModel.tierPriceList[index]);
                $scope.productModel.tierPriceList.splice(index, 1);

            }
        }
        $scope.fromqtyexist = false;
        $scope.toqtyexist = false;
        $scope.checkQty = function (qty, index, type)
        {
            $timeout(function ()
            {
                $scope.checkQuantity(qty, index, type);
            }, 1000);
        }

        $scope.checkQuantity = function (qty, index, type)
        {
            $scope.fromqtyexist = false;
            $scope.toqtyexist = false;
            if ($scope.productModel.tierPriceList.length > 1)
            {
                for (var i = 0; i < $scope.productModel.tierPriceList.length; i++)
                {
                    if (i != index && $scope.productModel.tierPriceList[i].from_qty == qty)
                    {
                        if (type == 'from')
                        {
                            $scope.fromqtyexist = true;
                        } else if (type == 'to')
                        {
                            $scope.toqtyexist = true;
                        }
                    }
                    if (i != index && $scope.productModel.tierPriceList[i].to_qty == qty)
                    {
                        if (type == 'from')
                        {
                            $scope.fromqtyexist = true;
                        } else if (type == 'to')
                        {
                            $scope.toqtyexist = true;
                        }
                    }
                    if (i == $scope.productModel.tierPriceList.length - 1 && $scope.fromqtyexist)
                    {
                        sweet.show('Oops...', 'Qty Mismatched...', 'error');
                        $scope.productModel.tierPriceList[index].from_qty = '';
                    }
                    if (i == $scope.productModel.tierPriceList.length - 1 && $scope.toqtyexist)
                    {
                        sweet.show('Oops...', 'Qty Mismatched...', 'error');
                        $scope.productModel.tierPriceList[index].to_qty = '';
                    }

                }
            }
        }
        $scope.deleteAllTierPrice = function ()
        {
            var deleteTierPriceParam = [];
            var headers = {};
            headers['screen-code'] = 'product';
            for (var i = 0; i < $scope.productModel.deletedProducts.length > 0; i++)
            {
                var param = {};
                param.id = $scope.productModel.deletedProducts[i].id;
                deleteTierPriceParam.push(param);
            }
            adminService.deleteTierPriceMapping(deleteTierPriceParam, headers).then(function (response) {
                if (response.data.success == true)
                {
                    // $scope.getTierPriceMappingList();
                    if ($scope.productModel.tierPriceList.length > 0)
                    {
                        $scope.updateTierPriceMapping();
                    } else if ($scope.productModel.mappedTax != 0 || $scope.productModel.mappedTax == '')
                    {
                        $scope.addTaxMapping();
                    }
                }

            });
        }

        $scope.getTierPriceMappingList = function ()
        {
            var getListParam = {};
            getListParam.product_id = $stateParams.id;
            getListParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getTierPriceMappingList(getListParam, configOption).then(function (response)
            {
                var data = response.data;
                $scope.productModel.tierPriceList = data.list;
            });

        };

        $scope.updateTierPriceMapping = function () {

            var updateTierPriceParam = [];
            var headers = {};
            headers['screen-code'] = 'product';
            for (var i = 0; i < $scope.productModel.tierPriceList.length; i++)
            {
                var tierPrice = {};
                tierPrice.id = $scope.productModel.tierPriceList[i].id;
                tierPrice.product_id = $stateParams.id;
                tierPrice.from_qty = $scope.productModel.tierPriceList[i].from_qty;
                tierPrice.to_qty = $scope.productModel.tierPriceList[i].to_qty;
                tierPrice.unit_price = $scope.productModel.tierPriceList[i].unit_price;
                updateTierPriceParam.push(tierPrice);
            }

            adminService.updateTierPriceMapping(updateTierPriceParam, headers).then(function (response) {
                if (response.data.success == true)
                {
                    if ($scope.productModel.mappedTax != 0 || $scope.productModel.mappedTax == '')
                    {
                        $scope.addTaxMapping();
                    } else
                    {
                        $scope.formReset();
                        $scope.isDataSavingProcess = false;
                        $state.go('app.productList');
                    }
                }
            });
        };

        $scope.getProductInfo = function ()
        {
            $scope.productModel.isLoadingProgress = true;
            if (typeof $stateParams.id != 'undefined')
            {
                var productListParam = {};
                productListParam.id = $stateParams.id;
                productListParam.start = 0;
                productListParam.limit = 1;
                productListParam.isActive = 1;
                productListParam.is_group = 1;
                $scope.productDetail = {};
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getProductListWithTax(productListParam, configOption).then(function (response) {

                    var data = response.data;
                    if (data.list.length > 0)
                    {
                        $scope.productDetail = data.list[0];
//                        $scope.getHsnList("update");
                    }
                    $scope.getProductDetailList();
                    $scope.getCUOMList();
                    $scope.getcategoryList();
//                    $scope.initUpdateDetail();

                });
            }
        };
        $scope.isCategoryLoaded = false;
        $scope.getcategoryList = function ()
        {
            var getListParam = {};
            getListParam.category_id = "";
            getListParam.name = "";
            getListParam.id = "";
            getListParam.is_active = 1;
            getListParam.start = 0;
            getListParam.limit = $scope.productModel.limit;
            $scope.isDataSavingProcess = true;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getCategoryList(getListParam, configOption).then(function (response)
            {
                var data = response.data;
                $scope.productModel.categorylist = data.list;
                $scope.isCategoryLoaded = true;
                $scope.isDataSavingProcess = false;
            });

        };
        $scope.getProductInfo();

        $scope.isUOMLoaded = false;
        $scope.getCUOMList = function ()
        {
            var getListParam = {};
            getListParam.id = "";
            getListParam.name = "";
            getListParam.start = 0;
            getListParam.limit = $scope.productModel.limit;
            $scope.isDataSavingProcess = true;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getUom(getListParam, configOption).then(function (response)
            {
                var data = response.data;
                $scope.productModel.uomList = data.list;
                $scope.isUOMLoaded = true;
                $scope.isDataSavingProcess = false;
            });
        };
        //$scope.getProductInfo();
        /*
         *  It used to format the speciality model value in UI
         */
        $scope.editProductInfo = function () {

            if ($scope.isDataSavingProcess)
            {
                return;
            }

            $scope.isDataSavingProcess = true;
            var addProductParam = {};
            var headers = {};
            headers['screen-code'] = 'product';
            //     addProductParam.id = 0;
            addProductParam.name = $scope.productModel.name;
            addProductParam.sku = $scope.productModel.sku;
            addProductParam.sales_price = parseFloat($scope.productModel.sales_price);
            addProductParam.description = $scope.productModel.description;
            addProductParam.category_id = $scope.productModel.category_id;
            //      addProductParam.uom = $scope.productModel.uom;
            addProductParam.type = "Product";
            if ($scope.productModel.uom != "")
            {
                for (var loopIndex = 0; loopIndex < $scope.productModel.uomList.length; loopIndex++)
                {
                    if ($scope.productModel.uom == $scope.productModel.uomList[loopIndex].name)
                    {
                        addProductParam.uom_id = $scope.productModel.uomList[loopIndex].id;
                        break;
                    }
                }
            } else
            {
                addProductParam.uom_id = "";
            }

            addProductParam.is_active = $scope.productModel.is_active ? 1 : 0;
            addProductParam.is_sale = $scope.productModel.is_sale ? 1 : 0;
            addProductParam.is_purchase = $scope.productModel.is_purchase ? 1 : 0;
            addProductParam.min_stock_qty = $scope.productModel.minstock;
            addProductParam.opening_stock = $scope.productModel.openstock;
            addProductParam.has_inventory = $scope.productModel.hasinventory == true ? 1 : 0;
            addProductParam.hsn_code = $scope.productModel.hsncode;
            addProductParam.hsn_id = $scope.hsn_id;
            addProductParam.attachment = [];
            if (typeof $scope.productModel.attachments != "undefined" && $scope.productModel.attachments.length > 0)
            {
                for (var i = 0; i < $scope.productModel.attachments.length; i++)
                {
                    var imageParam = {};
                    imageParam.id = $scope.productModel.attachments[i].id;
                    imageParam.url = $scope.productModel.attachments[i].url;
                    // imageParam.ref_id = $scope.productModel.attachments[i].ref_id;
                    imageParam.type = $scope.productModel.attachments[i].type;
                    addProductParam.attachment.push(imageParam);
                }
            }
            var configOption = adminService.handleBothSuccessAndErrorResponseConfig;
            adminService.editProduct(addProductParam, $stateParams.id, configOption, headers).then(function (response) {
                if (response.data.success == true)
                {
//                    if ($rootScope.userModel.new_user == 1)
//                    {
//                        $window.localStorage.setItem("demo_guide", "true");
//                        $rootScope.closeDemoPopup(4);
//                    }
                    $scope.saveHsnCode();
                    if ($scope.productModel.deletedProducts.length > 0)
                    {
                        $scope.deleteAllTierPrice();
                    } else if ($scope.productModel.tierPriceList.length > 0)
                    {
                        $scope.updateTierPriceMapping();
                    } else if ($scope.productModel.mappedTax != 0 || $scope.productModel.mappedTax == '')
                    {
                        $scope.addTaxMapping();
                    } else
                    {
                        $scope.formReset();
                        $scope.isDataSavingProcess = false;
                        $state.go('app.productList');
                    }
//                    $scope.formReset();
//                    $state.go('app.productList');
                }
                //$scope.isDataSavingProcess = false;
            });
        };
        //$scope.getCUOMList();
        //$scope.getcategoryList();
        // $scope.getcategoryList();

        $scope.setCheckedItems = function ()
        {
            if ($scope.productModel.taxMapping.length > 0)
            {
                for (var i = 0; i < $scope.productModel.taxMapping.length; i++)
                {
                    $scope.productModel.taxMapping[i].isSelected = false;
                    if ($scope.productModel.taxMapping[i].id != 0)
                    {
                        $scope.productModel.taxMapping[i].isSelected = true;
                    }
                }
            }
            $scope.productModel.isLoadingProgress = false;
        };


        $scope.addTaxMapping = function ()
        {
            var addTaxMappingParam = [];
            var headers = {};
            headers['screen-code'] = 'product';
            if ($scope.productModel.tax != '')
            {
                var param = {};
                param.tax_id = $scope.productModel.tax;
                param.product_id = $stateParams.id;
                param.is_active = 1;
                addTaxMappingParam.push(param);
            }
            if (addTaxMappingParam.length > 0)
            {
                var configOption = adminService.handleOnlyErrorResponseConfig;
                adminService.addTaxMapping(addTaxMappingParam, configOption, headers).then(function (response) {
                    if (response.data.success == true)
                    {
                        if ($scope.productModel.mappedTax != '')
                        {
                            $scope.deleteTaxMapping();
                        } else
                        {
                            $scope.formReset();
                            $scope.isDataSavingProcess = false;
                            $state.go('app.productList');
                        }
                    }
                });
            } else
            {
                if ($scope.productModel.mappedTax != '')
                {
                    $scope.deleteTaxMapping();
                } else
                {
                    $scope.formReset();
                    $scope.isDataSavingProcess = false;
                    $state.go('app.productList');
                }
            }
        };

        $scope.deleteTaxMapping = function ()
        {
            var deleteTaxMappingParam = [];
            var headers = {};
            headers['screen-code'] = 'product';
            if ($scope.productModel.mappedTax != '')
            {
                var param = {};
                param.id = $scope.productModel.mappedTax;
                deleteTaxMappingParam.push(param);
            }
            if (deleteTaxMappingParam.length > 0)
            {
                var configOption = adminService.handleOnlyErrorResponseConfig;
                adminService.deleteTaxMapping(deleteTaxMappingParam, configOption, headers).then(function (response) {
                    if (response.data.success == true)
                    {
                        $scope.formReset();
                        $scope.isDataSavingProcess = false;
                        $state.go('app.productList');
                    }
                });
            } else
            {
                $scope.formReset();
                $scope.isDataSavingProcess = false;
                $state.go('app.productList');
            }
        };

        $scope.showPhotoGalaryPopup = false;
        $scope.showPopup = function (index, attachindex)
        {
            if (index == 'morefileupload')
            {
                $scope.showUploadMoreFilePopup = true;
            } else if (index === 'photogalary')
            {
                $scope.showPhotoGalaryPopup = true;
            }
        }
        $scope.closePopup = function (index)
        {
            if (index == 'morefileupload')
            {
                $scope.showUploadMoreFilePopup = false;
            } else if (index === 'photogalary')
            {
                $scope.showPhotoGalaryPopup = false;
            }
        }

        $scope.uploadedFileQueue = [];
        $scope.uploadedFileCount = 0;
        $scope.isImageSavingProcess = true;
        $scope.$on("updateFileUploadEvent", updateFileUploadEventHandler);
        function updateFileUploadEventHandler(event, data, category, imgcaption, index)
        {
            $scope.isImageSavingProcess = true;
            $scope.uploadedFileQueue = data;
            if (typeof category != undefined && category != null && category != '')
            {
                $scope.uploadedFileQueue[index].category = category;
            }
            if (typeof imgcaption != undefined && imgcaption != null && imgcaption != '')
            {
                $scope.uploadedFileQueue[index].imgcaption = imgcaption;
            }
            console.log('$scope.uploadedFileQueue');
            console.log($scope.uploadedFileQueue);
            $scope.validateFileUploadStatus();
        }

        $scope.$on("deletedUploadFileEvent", deletedUploadFileEventHandler);
        function deletedUploadFileEventHandler(event, index)
        {
            if ($scope.showUploadMoreFilePopup)
            {
                if ($scope.productModel.attachments.length > index)
                {
                    $scope.productModel.attachments.splice(index, 1);
                }
            }
        }

        $scope.validateFileUploadStatus = function ()
        {
            $scope.isImageSavingProcess = true;
            var hasUploadCompleted = true;
            for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
            {
                if (!$scope.uploadedFileQueue[i].isSuccess)
                {
                    hasUploadCompleted = false;
                    $scope.$broadcast('genericErrorEvent', 'Image upload is in progress.');
                    break;
                }
            }
            if (hasUploadCompleted && $scope.uploadedFileQueue.length > 0)
            {
                $scope.uploadedFileCount = $scope.uploadedFileQueue.length;
                $scope.isImageSavingProcess = false;
                $scope.isImageUploadComplete = true;
            }

        }
        $scope.deleteImage = function ($index)
        {
            $scope.productModel.attachments.splice($index, 1);
        }

        $scope.saveImagePath = function ()
        {
            if ($scope.uploadedFileQueue.length == 0)
            {
                if ($scope.showUploadMoreFilePopup)
                {
                    $scope.closePopup('morefileupload');
                }
            }
            var hasUploadCompleted = true;
            for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
            {
                if (!$scope.uploadedFileQueue[i].isSuccess)
                {
                    hasUploadCompleted = false;
                    $scope.$broadcast('genericErrorEvent', 'Image upload is in progress.');
                    break;
                }
            }
            if (hasUploadCompleted && $scope.uploadedFileQueue.length > 0)
            {
                $scope.uploadedFileCount = $scope.uploadedFileQueue.length;
                $scope.isImageSavingProcess = true;
                $scope.isImageUploadComplete = true;
                for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
                {
                    //               $scope.productModel.attachments.push($scope.uploadedFileQueue[i]);
                    var imageUpdateParam = {};
                    //imageUpdateParam.id = 0;
                    imageUpdateParam.url = $scope.uploadedFileQueue[i].urlpath;
                    //imageUpdateParam.ref_id = $stateParams.id;
                    imageUpdateParam.type = 'product';
//                    imageUpdateParam.comments = '';
//                    imageUpdateParam.signedUrl = $scope.uploadedFileQueue[i].signedUrl;
//                    imageUpdateParam.path = $scope.uploadedFileQueue[i].signedUrl;
                    $scope.productModel.attachments.push(imageUpdateParam);
                    if (i == $scope.uploadedFileQueue.length - 1)
                    {
                        $scope.closePopup('morefileupload');
                    }
                }
            } else
            {
                $scope.closePopup('morefileupload');
            }
        };
        $scope.getProductDetailList = function ()
        {
            if ($rootScope.userModel.new_user == 1)
                $scope.productModel.isLoadingProgress = true;
            if (typeof $stateParams.id != 'undefined')
            {
                var productListParam = {};
                productListParam.id = $stateParams.id;
                productListParam.start = 0;
                productListParam.limit = 1;
                productListParam.isActive = 1;
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getProductList(productListParam, configOption).then(function (response) {
                    var data = response.data;
                    if (response.data.success === true)
                    {
                        $scope.productModel.productListDetail = data.list[0];
                        $scope.getHsnDetails($scope.productDetail.hsn_code);
                    }
                });
            }
        };
        $scope.getHsnDetails = function (hsn_code)
        {
            var hsnListParam = {};
            hsnListParam.hsn_code = hsn_code;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getHSNTaxDetail(hsnListParam, configOption).then(function (response) {
                var data = response.data;
                if (response.data.success === true)
                {
                    $scope.hsnList = data.list;
                    for (var i = 0; i < $scope.hsnList.length; i++)
                    {
                        $scope.clearFilters();
                        $scope.closeHsnPopup();
                        $scope.hsnList[i].is_sso = 0;
                        $scope.productModel.hsnList.push($scope.hsnList[i]);
                        //$scope.productModel.hsnList = [];
                        $scope.productModel.hsnCodeInfo = $scope.hsnList[i];
                        $scope.productModel.hsncode = $scope.hsnList[i].hsn_code;
                        $scope.hsn_id = $scope.hsnList[i].id;
                        $scope.productModel.hsncodeTaxinfo = [];
                        if ($scope.hsnList[i].hsnTaxMapping.length > 0 && $scope.hsnList[i].hsnTaxMapping != null && typeof $scope.hsnList[i].hsnTaxMapping != "undefined")
                        {
                            for (var j = 0; j < $scope.hsnList[i].hsnTaxMapping.length; j++)
                            {
                                $scope.productModel.hsncodeTaxinfo.push($scope.hsnList[i].hsnTaxMapping[j]);
                            }
                        }
                    }
                    $scope.initUpdateDetail();
                }
            });
            
        }
        $scope.getProductlist = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.PRODUCT_LIST, autosearchParam, false).then(function (responseData) {
                var data = responseData.data;
                var hits = data.list;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatProductModel = function (model) {

            if (model != null && model != undefined)
            {
                return model.name;
            }
            return  '';
        };

        $scope.getProductlist = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.PRODUCT_LIST, autosearchParam, false).then(function (responseData) {
                var data = responseData.data;
                var hits = data.list;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatProductModel = function (model) {

            if (model != null && model != undefined)
            {
                return model.name;
            }
            return  '';
        };
        $scope.productModel.isLoadingProgress = false;

        $scope.getHsnList = function (type, code) {
            $http.get('http://sso2.accozen.com/public/data.php').success(function (data) {
//            $.getJSON('http://sso2.accozen.com/public/data.php', function (data) {
                $scope.hsnList = data;
                
                $scope.productModel.hsnList = [];
                for (var i = 0; i < $scope.hsnList.length; i++)
                {
                    $scope.hsnList[i].is_sso = 1;
                    $scope.productModel.hsnList.push($scope.hsnList[i]);
                }
                var element = document.getElementById("hsn_dropdown");
                element.classList.remove("has-error"); 
                var element1 = document.getElementById("desc");
                element1.classList.remove("has-error"); 
                
            });
        };
        $scope.updateHsn = function (service) {
            $scope.clearFilters();
            $scope.closeHsnPopup();
            $scope.productModel.hsnList = [];
            $scope.productModel.hsnCodeInfo = service;
            $scope.productModel.hsncode = service.hsn_code;
            $scope.hsn_id = service.id;
            $scope.productModel.hsncodeTaxinfo = [];
            if (service.hsnTaxMapping.length > 0 && service.hsnTaxMapping != null && typeof service.hsnTaxMapping != "undefined")
            {
                for (var i = 0; i < service.hsnTaxMapping.length; i++)
                {
                    $scope.productModel.hsncodeTaxinfo.push(service.hsnTaxMapping[i]);
                }
            }
        };
        $scope.editHsnInfo = function (prodcut) {
            $scope.clearFilters();
            $scope.closeHsnPopup();
            $scope.productModel.hsnList = [];
            $scope.productModel.hsnCodeInfo = prodcut
            $scope.productModel.hsncode = prodcut.hsn_code;
            $scope.hsn_id = prodcut.id;
            $scope.productModel.hsncodeTaxinfo = [];
            if (prodcut.hsnTaxMapping.length > 0 && prodcut.hsnTaxMapping != null && typeof prodcut.hsnTaxMapping != "undefined")
            {
                for (var i = 0; i < prodcut.hsnTaxMapping.length; i++)
                {
                    $scope.productModel.hsncodeTaxinfo.push(prodcut.hsnTaxMapping[i]);
                }
            }
        }
        $scope.saveHsnCode = function () {
            var createHSNParam = {};
            var headers = {};
            headers['screen-code'] = 'product';
            createHSNParam.hsn_code = $scope.productModel.hsnCodeInfo.hsn_code;
            createHSNParam.description = $scope.productModel.hsnCodeInfo.description;
            createHSNParam.Type = $scope.productModel.hsnCodeInfo.type;
            createHSNParam.is_approved = $scope.productModel.hsnCodeInfo.is_approved;
            createHSNParam.is_active = $scope.productModel.hsnCodeInfo.is_active;
            createHSNParam.hsnTaxMapping = [];
            for (var i = 0; i < $scope.productModel.hsnCodeInfo.hsnTaxMapping.length; i++)
            {
                var taxMap = {
                    group_tax_id: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].group_tax_id,
                    from_price: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].from_price,
                    to_price: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].to_price,
                    group_tax_ids: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].group_tax_ids,
                    intra_group_tax_ids: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].intra_group_tax_ids
                }
                createHSNParam.hsnTaxMapping.push(taxMap);
            }
            createHSNParam.taxDetail = [];
            for (var i = 0; i < $scope.productModel.hsnCodeInfo.hsnTaxMapping.length; i++)
            {
                if ($scope.productModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail.length > 0)
                {
                    for (var j = 0; j < $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail.length; j++)
                    {
                        var taxDetail =
                                {
                                    sso_tax_id: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].id,
                                    tax_no: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].tax_no,
                                    tax_name: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].tax_name,
                                    tax_percentage: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].tax_percentage,
                                    is_group: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].is_group,
                                    tax_type: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].tax_type,
                                    is_active: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].is_active,
                                    comments: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].comments,
                                    is_display: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].is_display,
                                    hsn_flag: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].hsn_flag,
                                    intra_group_tax_ids: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].intra_group_tax_ids
                                };
                        createHSNParam.taxDetail.push(taxDetail);
                    }
                }
                if ($scope.productModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail.length > 0)
                {
                    for (var j = 0; j < $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail.length; j++)
                    {
                        var taxDetail =
                                {
                                    sso_tax_id: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].id,
                                    tax_no: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].tax_no,
                                    tax_name: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].tax_name,
                                    tax_percentage: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].tax_percentage,
                                    is_group: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].is_group,
                                    tax_type: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].tax_type,
                                    is_active: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].is_active,
                                    comments: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].comments,
                                    is_display: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].is_display,
                                    hsn_flag: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].hsn_flag,
                                    intra_group_tax_ids: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].intra_group_tax_ids
                                };
                        createHSNParam.taxDetail.push(taxDetail);
                    }
                }
                var detail =
                        {
                            sso_tax_id: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].group_tax_id,
                            tax_no: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].tax_no,
                            tax_name: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].tax_name,
                            tax_percentage: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].tax_percentage,
                            is_group: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].is_group,
                            tax_type: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].tax_type,
                            is_active: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].is_active,
                            comments: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].comments,
                            is_display: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].is_display,
                            hsn_flag: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].hsn_flag,
                            group_tax_ids: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].group_tax_ids,
                            intra_group_tax_ids: $scope.productModel.hsnCodeInfo.hsnTaxMapping[i].intra_group_tax_ids
                        };
                createHSNParam.taxDetail.push(detail);
//                createHSNParam.hsnTaxMapping.push(taxMap);
            }
            createHSNParam.is_sso = $scope.productModel.hsnCodeInfo.is_sso;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.saveHSNCode(createHSNParam, configOption, headers).then(function (response)
            {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.productList');
                }
                if (response.data.success == false)
                {
                    $scope.formReset();
                    $state.go('app.productList');
                }
                $scope.isDataSavingProcess = false;
            });
        };
        $scope.showHSNDetail = false;
        $scope.showHsnPopup = function ()
        {
            $scope.showHSNDetail = true;
            $scope.productModel.hsnList = [];
        }
        $scope.closeHsnPopup = function ()
        {
            $scope.showHSNDetail = false;
        }
//        $scope.getTierPriceMappingList();
    }]);



