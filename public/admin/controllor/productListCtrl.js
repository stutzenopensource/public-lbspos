app.controller('productListCtrl', ['$scope', '$rootScope', 'adminService', '$localStorage', '$filter', 'Auth', '$timeout', '$window', function ($scope, $rootScope, adminService, localStorage, $filter, Auth, $timeout, $window) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.productModel = {
            scopeId: "",
            configId: 5,
            type: "",
            currentPage: 1,
            total: 0,
            limit: 4,
            list: [],
            status: '',
            currencyFormat: '',
            serverList: null,
            isLoadingProgress: true
        };

        $scope.searchFilterNameValue = ''

        $scope.pagePerCount = [50, 100];
        $scope.productModel.limit = $scope.pagePerCount[0];

        $scope.searchFilter = {
            name: '',
            salesOrPurchase: '',
            activeOrDeactiveProduct: ''
        };

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getProductListInfo, 300);
        }

        $scope.refreshScreen = function ()
        {
            $scope.searchFilter = {
                name: '',
                salesOrPurchase: '',
                activeOrDeactiveProduct: ''
            };
            $scope.initTableFilter();
        }

        $scope.selectProductId = '';
        $scope.showdeletePopup = false;
        $scope.isdeleteProgress = false;
        $scope.showPopup = function (id)
        {
            $scope.selectProductId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function ()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteProgress = false;
        };

        $scope.deleteProductInfo = function ( )
        {
            if ($scope.isdeleteProgress == false)
            {
                $scope.isdeleteProgress = true;

                var getListParam = {};
                getListParam.id = $scope.selectProductId;
                var headers = {};
                headers['screen-code'] = 'product';
                adminService.deleteProduct(getListParam, getListParam.id, headers).then(function (response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getProductListInfo();
                    }
                    $scope.isdeleteProgress = false;
                });
            }
        };

        /*
         * Get market list
         */
        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'product_list_form';


        $scope.$on('onLocalStorageFormRenterCompleteEvent', function (event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });

        $scope.$on('onLocalStorageFieldRenterCompleteEvent', function (event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRenterCount++;
                }
                if ($scope.localStorageRenterCount >= $scope.localStorageCount)
                {
                    console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                    $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
                }

            }
        });

        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function (event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRetrieveCount++;
                }
                if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
                {
                    $scope.initTableFilter();
                }
                console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);
            }
        });

        $scope.init = function ()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');

        };

        $scope.$on('onLocalStorageReadyEvent', function () {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                $timeout(function () {
                    $scope.init();
                }, 300);

            }
        });
        $scope.getProductListInfo = function () {

            // ?name=&sku=&type=&start=&limit=
            $scope.productModel.isLoadingProgress = true;
            var materialListParam = {};
            var headers = {};
            headers['screen-code'] = 'product';
            //materialListParam.inventoryScopeId = $rootScope.userModel.scopeId;
            //materialListParam.configId = 5;
            materialListParam.type = "Product";
            materialListParam.sku = $scope.searchFilterNameValue;
            //materialListParam.status = "initiated";
            materialListParam.id = '';
            materialListParam.name = $scope.searchFilter.name;
            if ($scope.searchFilter.salesOrPurchase == 1)
            {
                materialListParam.is_sale = 1;
            } else if ($scope.searchFilter.salesOrPurchase == 2)
            {
                materialListParam.is_purchase = 1;
            }
            if ($scope.searchFilter.activeOrDeactiveProduct == 1)
            {
                materialListParam.is_active = 0;
            } else
            {
                materialListParam.is_active = 1;
            }
            //materialListParam.is_active = 1;
            if ($rootScope.productNext == false) //For Demo Guide purpose
            {
                materialListParam.start = 0;
                materialListParam.limit = 10;
            } else {
                materialListParam.start = ($scope.productModel.currentPage - 1) * $scope.productModel.limit;
                materialListParam.limit = $scope.productModel.limit;
            }

            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getProductList(materialListParam, configOption, headers).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.productModel.list = data.list;
                    $scope.productModel.total = data.total;
                    if ($scope.productModel.list.length > 0)
                    {
                        $rootScope.productAdded = true;
                        $window.localStorage.setItem("productAdded", "true");
                    }
                    if ($rootScope.userModel.new_user == 1 && $rootScope.productNext != false && $rootScope.productNext != undefined)
                    {
                        $window.localStorage.setItem("demo_guide", "true");
                        $rootScope.closeDemoPopup(2);
                    }
                }
                $scope.productModel.isLoadingProgress = false;
            });

        };

        //$scope.getProductListInfo();

    }]);




