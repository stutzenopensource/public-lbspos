





app.controller('productselectCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$timeout', 'APP_CONST', function($scope, $rootScope, adminService, $filter, Auth, $timeout, APP_CONST) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.productModel =
                {
                    code: '',
                    sku: '',
                    productName: '',
                    unit: '',
                    list: [],
                    currentPage: 1,
                    total: 0,
                    limit: 10,
                    serverList: null,
                    isLoadingProgress: false
                };
        $scope.pagePerCount = [50, 100];
        $scope.productModel.limit = $scope.pagePerCount[0];

        $scope.initTableFilterTimeoutPromise = null;

        $scope.searchFilter = {
            code: '',
            name: '',
            sku: '',
            type:''
        }
        $scope.searchFilterValue = "";

        $scope.initTableFilter = function()
        {

            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);

        }

        $scope.onProductSelectHandler = function(rows) {
            var selectedItems = $scope.productModel.list[rows];
            console.log("selectedItems");
            console.log(selectedItems);
            $scope.$emit('updateSelectedProductEvent', selectedItems);

        }

        $scope.getList = function() {
            $scope.productModel.isLoadingProgress = true;
            var getParam = {};

            getParam.start = ($scope.productModel.currentPage - 1) * $scope.productModel.limit;
            getParam.limit = $scope.productModel.limit;
            getParam.name = $scope.searchFilter.name;
            getParam.sku = $scope.searchFilter.sku;
            getParam.type = $scope.searchFilter.type;
            getParam.is_active = 1;
            getParam.is_sale = 1;
            //getParam.mode = 1;
            if(typeof $scope.selectedAssignFrom != 'undefined' && typeof $scope.selectedAssignFrom.id != 'undefined')
            {    
                getParam.scopeId = $scope.selectedAssignFrom.id;
            }
            else
            {
                //12345 can not read property of roleName error
//                if ($rootScope.userModel.userRole.roleName != APP_CONST.ROLE.ROLE_ADMIN)
//                {
//                    getParam.scopeId = $scope.userModel.scopeId;
//                }
            }
            
                
            adminService.getProductList(getParam).then(function(response) {
                var data = response.data;
                $scope.productModel.list = data.list;
                $scope.productModel.total = data.total;
                $scope.productModel.isLoadingProgress = false;

            });

        };

        $scope.getList();



    }]);




