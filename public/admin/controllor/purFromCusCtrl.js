app.controller('purFromCusCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', function ($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.purFromCusModel = {
            currentPage: 1,
            total: 0,
            total_Amount: 0,
            limit: 10,
            list: [],
            length: [],
            productname: '',
            uom: '',
            colspan: '',
            quantity: '',
            amount: '',
            serverList: null,
            isLoadingProgress: false,
            isSearchLoadingProgress: false,
            tax_list: '',
            total_Tax_Amount: 0
        };
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.purFromCusModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
        $scope.validationFactory = ValidationFactory;
        $scope.openDate = function (index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            } else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };

        $scope.searchFilter = {
            fromdate: '',
            todate: '',
            code: ''
        };
        $scope.searchFilter.fromdate = $scope.currentDate;
        $scope.clearFilters = function ()
        {
            $scope.searchFilter = {
                fromdate: '',
                todate: '',
                code: ''
            };
            $scope.purFromCusModel.list = [];
            $scope.searchFilter.fromdate = $scope.currentDate;
        }

        $scope.print = function (div)
        {
            var docHead = document.head.outerHTML;
            var printContents = document.getElementById(div).outerHTML;
            var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=865, height=600, resizable=yes, screenX=200, screenY=200, personalbar=no, scrollbars=yes";

            var newWin = window.open("", "_blank", winAttr);
            var writeDoc = newWin.document;
            writeDoc.open();
            writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
            writeDoc.close();
            newWin.focus();
        };

        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.validateDateFilterData = function ()
        {
            var retVal = false;
            if ($scope.searchFilter.todate !== '' || $scope.searchFilter.fromdate !== '')
            {
                retVal = true;
            }
            return retVal;
        };

        $scope.todate = '';
        $scope.getList = function () {
            $scope.purFromCusModel.colspan = 18;
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'purfromcus';

            if ($scope.searchFilter.todate != '' && ($scope.searchFilter.fromdate == null || $scope.searchFilter.fromdate == ''))
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
                getListParam.from_date = getListParam.to_date;
                $scope.todate = getListParam.to_date;
            } else if ($scope.searchFilter.fromdate != '' && ($scope.searchFilter.todate == null || $scope.searchFilter.todate == ''))
            {
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
                getListParam.to_date = getListParam.from_date;
                $scope.todate = getListParam.to_date;
            } else
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
                $scope.todate = getListParam.to_date;
            }
            getListParam.purchase_code = $scope.searchFilter.code;
            $scope.purFromCusModel.isLoadingProgress = true;
            adminService.getPurFromCusList(getListParam, headers).then(function (response)
            {
                $scope.purFromCusModel.colspan = 12;
                var totalAmt = 0.00;
                var totalTaxAmt = 0.00;
                var GST = [];
                var totGST = 0.00;
                var totalDiscAmt = 0.00;
                var totalInvAmt = 0.00;
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.purFromCusModel.tax_list = response.data.tax_list;
                    $scope.purFromCusModel.list = data;
                    var tax_list_length = $scope.purFromCusModel.tax_list.length;
                    var count = 0;
                    $scope.purFromCusModel.colspan = $scope.purFromCusModel.colspan + $scope.purFromCusModel.tax_list.length;


                    for (var i = 0; i < $scope.purFromCusModel.list.length; i++)
                    {
                        totalAmt = totalAmt + parseFloat($scope.purFromCusModel.list[i].valauable_amount);
                        totalInvAmt = totalInvAmt + parseFloat($scope.purFromCusModel.list[i].total_amount);
                        // $scope.purFromCusModel.list[i].valauable_amount = utilityService.changeCurrency($scope.purFromCusModel.list[i].valauable_amount, $rootScope.appConfig.thousand_seperator);
                        totalTaxAmt = totalTaxAmt + parseFloat($scope.purFromCusModel.list[i].tax_amount);
                        totalDiscAmt = totalDiscAmt + parseFloat($scope.purFromCusModel.list[i].discount_amount);
                        if (tax_list_length >= count)
                        {
                            for (var j = 0; j < $scope.purFromCusModel.list[i].tax_detail[count].length; j++)
                            {
                                $scope.purFromCusModel.list[i].tax_detail[count].tax_amount = GST[count] + parseFloat($scope.purFromCusModel.list[i].tax_detail[count].tax_amount);
                                $scope.purFromCusModel.list[i].tax_detail[count].tax_amount = utilityService.changeCurrency($scope.purFromCusModel.list[i].tax_detail[count].tax_amount, $rootScope.appConfig.thousand_seperator);
                            }
                        }


                    }
                    //   count = count + 1;

                    for (var i = 0; i < $scope.purFromCusModel.tax_list.length; i++)
                    {
                        var totalTax = 0.00;
                        for (var j = 0; j < $scope.purFromCusModel.list.length; j++)
                        {
                            totalTax = totalTax + parseFloat($scope.purFromCusModel.list[j].tax_detail[i].tax_amount);
                        }
                        $scope.purFromCusModel.tax_list[i].total_tax = totalTax;
                        $scope.purFromCusModel.tax_list[i].total_tax = parseFloat(totalTax).toFixed(2);
                    }
                    $scope.purFromCusModel.total_Invoice_Amount = totalInvAmt;
                    $scope.purFromCusModel.total_Invoice_Amount = parseFloat($scope.purFromCusModel.total_Invoice_Amount).toFixed(2);
                    $scope.purFromCusModel.total_Invoice_Amount = utilityService.changeCurrency($scope.purFromCusModel.total_Invoice_Amount, $rootScope.appConfig.thousand_seperator);
                    $scope.purFromCusModel.total_Disc_Amount = totalDiscAmt;
                    $scope.purFromCusModel.total_Disc_Amount = parseFloat($scope.purFromCusModel.total_Disc_Amount).toFixed(2);
                    $scope.purFromCusModel.total_Disc_Amount = utilityService.changeCurrency($scope.purFromCusModel.total_Disc_Amount, $rootScope.appConfig.thousand_seperator);
                    $scope.purFromCusModel.totalTaxAmount = totGST;
                    $scope.purFromCusModel.totalTaxAmount = parseFloat($scope.purFromCusModel.totalTaxAmount).toFixed(2);
                    $scope.purFromCusModel.totalTaxAmount = utilityService.changeCurrency($scope.purFromCusModel.totalTaxAmount, $rootScope.appConfig.thousand_seperator);
                    $scope.purFromCusModel.total_Amount = totalAmt;
                    $scope.purFromCusModel.total_Amount = parseFloat($scope.purFromCusModel.total_Amount).toFixed(2);
                    $scope.purFromCusModel.total_Amount = utilityService.changeCurrency($scope.purFromCusModel.total_Amount, $rootScope.appConfig.thousand_seperator);
                    $scope.purFromCusModel.total_Tax_Amount = totalTaxAmt;
                    $scope.purFromCusModel.total_Tax_Amount = parseFloat($scope.purFromCusModel.total_Tax_Amount).toFixed(2);
                    $scope.purFromCusModel.total_Tax_Amount = utilityService.changeCurrency($scope.purFromCusModel.total_Tax_Amount, $rootScope.appConfig.thousand_seperator);

                }
                $scope.purFromCusModel.isLoadingProgress = false;
            });

        };
        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'purfromcus_report_form';


        $scope.$on('onLocalStorageFormRenterCompleteEvent', function (event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });

        $scope.$on('onLocalStorageFieldRenterCompleteEvent', function (event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRenterCount++;
                }
                if ($scope.localStorageRenterCount >= $scope.localStorageCount)
                {
                    console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                    $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
                }

            }
        });
        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function (event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRetrieveCount++;
                }
                if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
                {
                    $scope.initTableFilter();
                }
                console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);
            }
        });

        $scope.init = function ()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');

        };

        $scope.$on('onLocalStorageReadyEvent', function () {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                $timeout(function () {
                    $scope.init();
                }, 300);

            }
        });

        //$scope.getList();
    }]);




