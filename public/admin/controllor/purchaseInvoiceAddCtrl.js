
app.controller('purchaseInvoiceAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', 'sweet', '$window', function ($scope, $rootScope, adminService, $httpService, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST, sweet, $window) {

        $scope.orderListModel =
                {
                    "id": 0,
                    "productname": "",
                    "sku": "",
                    "unit": "",
                    "mrp": "",
                    "sales_price": "",
                    "qty": "",
                    "rowtotal": "",
                    "isActive": "1",
                    "total": 0,
                    "customername": "",
                    "phone": "",
                    "addressId": "",
                    "subtotal": '0.00',
                    "taxAmt": '0.00',
                    "taxList": [],
                    "taxInfo": "",
                    "is_next_autogenerator_num_changed": false,
                    "next_autogenerator_num": "",
                    "prefixList": [],
                    "discountAmt": '0.00',
                    list: [],
                    invoice_list: [],
                    "billno": "",
                    "billdate": "",
                    "coupon": "",
                    "shipping": 0,
                    "discamount": 0,
                    "code": "",
                    "customerInfo": '',
                    "invoiceDetail": {},
                    "invoicePrefix": "",
                    "paymentTerm": "",
                    "taxPercentage": "",
                    "discountPercentage": "",
                    "totalDisPercentage": "0",
                    "totalTaxPercentage": "",
                    "discountMode": '%',
                    "discount_value": '',
                    "duedate": "",
                    "balance_amount": "",
                    "paid_amount": "",
                    "tax_id": "",
                    "productList": [],
                    "productId": '',
                    "invoiceno": '',
                    "purchase_code": '',
                    "purchase_no": '',
                    "invoice_no": '',
                    "city_id": '',
                    "state_id": '',
                    "country_id": '',
                    "taxAmountDetail": {
                        "tax_amount": ''
                    },
                    "taxAmountDetail": '',
                    "subtotalWithDiscount": '',
                    "roundoff": '0.00',
                    "round": "0.00",
                    "totalQty": '0',
                    "reference_no": '',
                    taxGroupList: []
                };
        $scope.adminService = adminService;
        $scope.customAttributeList = [];
        $scope.orderListModel.invoicePrefix = adminService.appConfig.invoice_prefix;
        $scope.listType = "product";
        if ($rootScope.appConfig.sales_product_list.toLowerCase() == "purchase based")
        {
            $scope.listType = "purchse";
        }
        $scope.senderEmail = '';
        $scope.receiver = [];
        $scope.ccEmail = '';
        $scope.showPopup = function (index) {
            $scope.showpopup = true;
            $scope.mailHeader = "Send a Receipt";
            $scope.senderEmail = $scope.orderListModel.customerInfo.email;
            $scope.receiver = [];
            var newRow = {
                "email": $scope.orderListModel.customerInfo.email
            };
            $scope.receiver.push(newRow);
            $scope.ccEmail = $scope.orderListModel.customerInfo.email;

            $scope.$broadcast("initPopupEvent");
        };

        $scope.addMoreReceiverEmail = function ( )
        {
            var newRow = {
                "email": ""
            };
            $scope.receiver.push(newRow);
        }

        $scope.closePopup = function () {
            $scope.showpopup = false;
        }

        $scope.onFocus = function (e) {
            $timeout(function () {
                $(e.target).trigger('input');
            });
        };

        $scope.showItems = false;
        $scope.showAreaPopup = function ()
        {
            $scope.showItems = true;
        }
        $scope.showCustomerAddPopup = false;
        $scope.showCustomerPopup = function ()
        {
            $scope.showCustomerAddPopup = true;
            if (typeof $scope.orderListModel.customerInfo != undefined && $scope.orderListModel.customerInfo != '' && $scope.orderListModel.customerInfo != null)
            {
                $rootScope.$broadcast('customerInfo', $scope.orderListModel.customerInfo);
            }
        }
        $scope.closeCustomerPopup = function ()
        {
            $scope.showCustomerAddPopup = false;
            $scope.orderListModel.state_id = '';
            $scope.orderListModel.country_id = '';
            $scope.orderListModel.city_id = '';
            $scope.showCustomerAddPopup = false;
        }

        $scope.$on("updateSavedCustomerDetail", function (event, customerDetail)
        {
            if (typeof customerDetail === 'undefined')
                return;
            $scope.orderListModel.customerInfo = customerDetail;
        });

        $scope.validationFactory = ValidationFactory;
        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function ()
        {
            if (typeof $scope.purchase_invoice_add_form != 'undefined' && $scope.purchase_invoice_add_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function () {

            $scope.purchase_invoice_add_form.$setPristine();
        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.resetOrderCreate = function () {

            $scope.orderListModel.user = '';
        }

        $scope.initTableFilterTimeoutPromise = null;
        $scope.deletedProductList = [];
        $scope.undeletedProductList = [];
        $scope.showbillPopup = false;
        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }
        $scope.quantityFormat = $rootScope.appConfig.qty_format;
        $scope.maxDate = $scope.maxDate ? null : new Date();
        $scope.startDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currencyFormat = $rootScope.appConfig.currency;
        $scope.currentDate = new Date();
        $scope.billdate1 = utilityService.parseStrToDate($scope.currentDate);
        $scope.orderListModel.billdate = $scope.billdate1;
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.openDate = function (index) {

            if (index == 0)
            {
                $scope.startDateOpen = true;
            } else if (index == 1)
            {
                $scope.toDateOpen = true;
            }
        }

        $scope.getCustomerList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            autosearchParam.mode = 2;
            return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = [];
                if (data.length > 0)
                {
                    for (var i = 0; i < data.length; i++)
                    {
                        if (data[i].is_purchase == '1' || data[i].is_purchase == 1)
                        {
                            hits.push(data[i]);
                        }
                    }
                }
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatCustomerModel = function (model)
        {
            if (model != null)
            {
                //return model.fname;
                if (model.fname != undefined && model.phone != undefined && model.email != undefined)
                {
                    return model.fname + '(' + model.phone + ',' + model.email + ')';
                } else if (model.fname != undefined && model.phone != undefined)
                {
                    return model.fname + '(' + model.phone + ')';
                }
            }
            return  '';
        };
        $scope.updateAddress = function ()
        {
            if ($scope.orderListModel.customerInfo.shopping_city != "" && $scope.orderListModel.customerInfo.shopping_city != null)
            {
                if ($scope.orderListModel.customerInfo.shopping_address != "")
                {
                    $scope.orderListModel.customerInfo.shopping_address = $scope.orderListModel.customerInfo.shopping_address + "," + $scope.orderListModel.customerInfo.shopping_city;
                } else
                {
                    $scope.orderListModel.customerInfo.shopping_address = $scope.orderListModel.customerInfo.shopping_city;
                }
            }
            if ($scope.orderListModel.customerInfo.shopping_state != "" && $scope.orderListModel.customerInfo.shopping_state != null)
            {
                if ($scope.orderListModel.customerInfo.shopping_address != "")
                {
                    $scope.orderListModel.customerInfo.shopping_address = $scope.orderListModel.customerInfo.shopping_address + "," + $scope.orderListModel.customerInfo.shopping_state;
                } else
                {
                    $scope.orderListModel.customerInfo.shopping_address = $scope.orderListModel.customerInfo.shopping_state;
                }
            }
            if ($scope.orderListModel.customerInfo.shopping_country != "" && $scope.orderListModel.customerInfo.shopping_country != null)
            {
                if ($scope.orderListModel.customerInfo.shopping_address != "")
                {
                    $scope.orderListModel.customerInfo.shopping_address = $scope.orderListModel.customerInfo.shopping_address + "," + $scope.orderListModel.customerInfo.shopping_country;
                } else
                {
                    $scope.orderListModel.customerInfo.shopping_address = $scope.orderListModel.customerInfo.shopping_country;
                }
            }
        }
        $scope.custDrop = function () {
            if ($scope.orderListModel.customerInfo.id == undefined) {
                $scope.orderListModel.customerInfo = '';
            }
        };

//        $scope.$watch('orderListModel.customerInfo', function (newVal, oldVal)
//        {
//            if (typeof newVal == 'undefined' || newVal == '' || newVal == null)
//            {
//                $scope.orderListModel.customerInfo = '';
//            } else if (typeof $stateParams.id == 'undefined' || $stateParams.id == null || $stateParams.id == '')
//            {
//                var addrss = "";
//
//                if (newVal.billing_address != undefined && newVal.billing_address != '')
//                {
//                    addrss += newVal.billing_address;
//                }
//                if (newVal.billing_city != undefined && newVal.billing_city != '')
//                {
//                    addrss += '\n' + newVal.billing_city;
//                }
//                if (newVal.billing_pincode != undefined && newVal.billing_pincode != '')
//                {
//                    if (newVal.billing_city != undefined && newVal.billing_city != '')
//                    {
//                        addrss += '-' + newVal.billing_pincode;
//                    } else
//                    {
//                        addrss += newVal.billing_pincode;
//                    }
//                }
//
//                if (newVal.billing_state != undefined && newVal.billing_state != '')
//                {
//                    addrss += '\n' + newVal.billing_state;
//                }
//
//                $scope.orderListModel.customerInfo.shopping_address = addrss;
//            }
//        });


        $scope.getproductlist = function (val) {

            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.type = "Product";
            autosearchParam.mode = 1;
            if ($rootScope.appConfig.gst_auto_suggest == 1)
            {
                if ($scope.orderListModel.customerInfo != '' && $scope.orderListModel.customerInfo != undefined && $scope.orderListModel.customerInfo != null && $scope.orderListModel.customerInfo.id != undefined)
                {
                    autosearchParam.customer_id = $scope.orderListModel.customerInfo.id;
                } else
                {
                    sweet.show("Oops", "Please select customer", "error");
                    return;
                }
            }
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.PRODUCT_LIST_WITH_TAX, autosearchParam, false).then(function (responseData) {
                var data = responseData.data.list;
                var hits = [];
                if (data.length > 0)
                {
                    for (var i = 0; i < data.length; i++)
                    {
                        if (data[i].is_purchase == '1' || data[i].is_purchase == 1)
                        {
                            hits.push(data[i]);
                        }
                    }
                }
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.getUomList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;

            return $httpService.get(APP_CONST.API.UOM, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatUomModel = function (model)
        {
            if (model != null)
            {
                return model.name;
            }
            return  '';
        };

        $scope.isLoadedTax = false;
        $scope.getTaxListInfo = function ( )
        {
            $scope.isLoadedTax = false;
            var getListParam = {};
            getListParam.id = "";
            getListParam.name = "";
            getListParam.start = 0;
            getListParam.limit = 0;
            getListParam.is_display = 1;
            getListParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getActiveTaxList(getListParam, configOption).then(function (response)
            {
                var data = response.data.list;
                $scope.orderListModel.taxList = data;
                $scope.getTaxGroupListInfo();
                $scope.isLoadedTax = true;
                $scope.initUpdateDetail();
            });
        };
        $scope.isLoadedTaxGroup = false;
        $scope.getTaxGroupListInfo = function ( )
        {
            $scope.isLoadedTax = false;
            var getListParam = {};
            getListParam.is_group = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getTaxList(getListParam, configOption).then(function (response)
            {
                var data = response.data.list;
                $scope.orderListModel.taxGroupList = data;
                $scope.isLoadedTaxGroup = true;
            });
        };
        $scope.getPricebasedTaxInfo = function (index)
        {
            var getListParam = {};
            getListParam.hsn_code = $scope.orderListModel.list[index].hsncode;
            getListParam.purchase_price = $scope.orderListModel.list[index].purchase_price;
            if ($scope.orderListModel.list[index].discount_value != '' && $scope.orderListModel.list[index].discount_value != null)
            {
                if ($scope.orderListModel.list[index].discount_mode == '%')
                {
                    $scope.orderListModel.list[index].discount_amount = $scope.orderListModel.list[index].purchase_price * $scope.orderListModel.list[index].qty * ($scope.orderListModel.list[index].discount_value / 100);
                } else
                {
                    $scope.orderListModel.list[index].discount_amount = $scope.orderListModel.list[index].discount_value;
                }

                $scope.orderListModel.list[index].single_product_discount = $scope.orderListModel.list[index].discount_amount / $scope.orderListModel.list[index].qty;
                getListParam.purchase_price = parseFloat($scope.orderListModel.list[index].purchase_price - $scope.orderListModel.list[index].single_product_discount);
            }
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getPriceBasedTax(getListParam, configOption).then(function (response)
            {
                var data = response.data;
                $scope.orderListModel.pricebasedTax = data;
                $scope.orderListModel.list[index].tax_id = $scope.orderListModel.pricebasedTax.purchase_tax_id + '';
                $scope.initUpdateProductPurchasePrice();
            });
        };
        $scope.getSalesPricebasedTaxInfo = function (index)
        {
            var getListParam = {};
            getListParam.hsn_code = $scope.orderListModel.list[index].hsncode;
            getListParam.mrp_price = $scope.orderListModel.list[index].mrp_price;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getPriceBasedTax(getListParam, configOption).then(function (response)
            {
                var data = response.data;
                $scope.orderListModel.pricebasedTax = data;
                $scope.orderListModel.list[index].selling_tax_id = $scope.orderListModel.pricebasedTax.selling_tax_id + '';
                $scope.initUpdateProductPurchasePrice();
            });
        };
        $scope.calculateDueDate = function ( )
        {
            if ($scope.orderListModel.paymentTerm != "")
            {
                var billdate = utilityService.parseStrToDate($scope.orderListModel.billdate);
                $scope.orderListModel.duedate = utilityService.incrementDate(billdate, $scope.orderListModel.paymentTerm);
            } else
            {
                $scope.orderListModel.duedate = $scope.orderListModel.billdate;
            }
        }
        $scope.addNewProduct = function ()
        {

            var newRow = {
                "id": '',
                "productName": '',
                "productInfo": '',
                "sku": '',
                "sales_price": '',
                "qty": '',
                "rowtotal": '',
                "discountPercentage": '',
                "uom": '',
                "uomid": '',
                "custom_opt1": '',
                "custom_opt2": '',
                "custom_opt3": '',
                "custom_opt4": '',
                "custom_opt5": '',
                "mrp_price": '',
                "purchase_price": '',
                "purchase_price_with_tax": '',
                "tax_id": '',
                "taxPercentage": '',
                "selling_tax_id": '',
                "selling_tax_percentage": '',
                "discount_mode": '%',
                "discount_value": '',
                "discount_amount": 0,
                "discount_amount_display": "0.00",
                "hsncode": ''
            };
            newRow.productTaxMapping = [];
            for (var t = 0; t < $scope.orderListModel.taxGroupList.length; t++)
            {
                newRow.productTaxMapping.push($scope.orderListModel.taxGroupList[t]);
            }
            $scope.orderListModel.list.push(newRow);

        }
        $scope.deleteProduct = function (index)
        {
//            var index = -1;
//            for (var i = 0; i < $scope.orderListModel.list.length; i++) {
//                if (id == $scope.orderListModel.list[i].id) {
//                    index = i;
//                    break;
//                }
//            }
            $scope.orderListModel.list.splice(index, 1);
            $scope.calculatetotal();
            $scope.calculateTaxAmt();
        };

        $scope.totalcalculationTimeoutPromise != null
        $scope.updateInvoiceTotal = function ()
        {
            if ($scope.totalcalculationTimeoutPromise != null)
            {
                $timeout.cancel($scope.totalcalculationTimeoutPromise);
            }
            $scope.totalcalculationTimeoutPromise = $timeout($scope.calculatetotal, 300);
        }

        $scope.initUpdateDetailTimeoutPromise = null;
        $scope.initUpdateDetail = function ()
        {
            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
            }
            if ($scope.isLoadedTax == true && $scope.isPrefixListLoaded == true && $scope.isLoadedTaxGroup == true)
            {
                if ($stateParams.id != "")
                {
                    $scope.updateInvoiceDetails();
                } else
                {
                    $scope.updateProductInfo();
                }
                $scope.orderListModel.isLoadingProgress = false;

            } else
            {
                $scope.initUpdateDetailTimeoutPromise = $timeout($scope.initUpdateDetail, 300);
            }

        }

        $scope.showMailPopup = false;
        $scope.showPaymentPopup = function ()
        {
            $scope.showMailPopup = true;
            $scope.invoiceDateOpen = false;
            $scope.paymentAddModel.invoice_id = $scope.orderListModel.invoiceDetail.id;
            $scope.paymentAddModel.customer_id = $scope.orderListModel.invoiceDetail.customer_id;
            $scope.paymentAddModel.account = $scope.orderListModel.invoiceDetail.customer_fname;
            $scope.paymentAddModel.invoicedate = $filter('date')($scope.currentDate, $scope.dateFormat);
            $scope.paymentAddModel.amount = $scope.orderListModel.invoiceDetail.balance_amount;
            $scope.paymentAddModel.comments = $scope.orderListModel.invoiceDetail.comments;
        };

        $scope.closePaymentPopup = function ( )
        {
            $scope.showMailPopup = false;
        }



        $scope.formatProductModel = function (model, index)
        {
            if (model != null && model != "")
            {
                $scope.updateProductInfo(model, index);
                $timeout(function () {
                    for (var i = 0; i < model.taxMapping.length; i++)
                    {
                        if (model.taxMapping[i].id != 0)
                        {
//                            $scope.orderListModel.list[index].discount_mode = '%';
//                            $scope.orderListModel.list[index].discount_value = $scope.orderListModel.discount_value;
                            $scope.orderListModel.list[index].tax_id = model.taxMapping[i].tax_id + '';
                            $scope.orderListModel.list[index].tax_percentage = model.taxMapping[i].tax_percentage + '';
                            $scope.orderListModel.list[index].selling_tax_id = model.taxMapping[i].tax_id + '';
                            $scope.orderListModel.list[index].selling_tax_percentage = model.taxMapping[i].tax_percentage + '';
                            break;
                        }
                    }
                    $scope.updateProductPurchasePrice(index);

                }, 300);
                return model.name;
            }
            return  '';
        }

        $scope.updateProductInfo = function (model, index)
        {
            if ($scope.orderListModel.list.length != 0)
            {
                if (typeof model == 'undefined')
                    return;

                $scope.orderListModel.list[index].id = model.id;
                $scope.orderListModel.list[index].sku = model.sku;
                $scope.orderListModel.list[index].sales_price = model.sales_price;
                $scope.orderListModel.list[index].qty = 1;
                $scope.orderListModel.list[index].hsncode = model.hsn_code;
                $scope.orderListModel.list[index].rowtotal = model.sales_price;
                $scope.orderListModel.list[index].discountPercentage = $scope.orderListModel.list[0].discountPercentage;
                if (model.taxMapping != undefined && model.taxMapping.length > 0)
                {
                    var found = false;
                    var findIndex = 0;
                    for (var i = 0; i < model.taxMapping.length; i++)
                    {
                        if (model.taxMapping[i].product_id == model.id)
                        {
                            found = true;
                            findIndex = i;
                        }
                    }
                    if (found == true)
                    {
                        $scope.orderListModel.list[index].tax_id = model.taxMapping[findIndex].tax_id + '';
                        $scope.orderListModel.list[index].taxPercentage = model.taxMapping[findIndex].tax_percentage;
                        $scope.orderListModel.list[index].tax_name = model.taxMapping[findIndex].tax_name + '-' + model.taxMapping[findIndex].tax_percentage;
                    } else
                    {
                        $scope.orderListModel.list[index].tax_id = '';
                        $scope.orderListModel.list[index].tax_name = '';
                        $scope.orderListModel.list[index].taxPercentage = '';
                    }

                } else
                {
                    $scope.orderListModel.list[index].tax_id = '';
                    $scope.orderListModel.list[index].tax_name = '';
                }
                $scope.orderListModel.list[index].taxMapping = model.taxMapping;
                $scope.orderListModel.list[index].selling_tax_id = "";
                $scope.orderListModel.list[index].mrp_price = model.sales_price;
                $scope.orderListModel.list[index].purchase_price = model.sales_price;
                $scope.orderListModel.list[index].purchase_price_with_tax = model.sales_price;
                $scope.orderListModel.list[index].discount_value = $scope.orderListModel.list[0].discount_value;
                $scope.orderListModel.list[index].discount_mode = $scope.orderListModel.list[0].discount_mode;
                $scope.orderListModel.list[index].uomInfo = {
                    "name": model.uom,
                }
                if (model.uomid != undefined)
                {
                    $scope.orderListModel.list[index].uomid = model.uomid;
                }
                if (model.uom_id != undefined)
                {
                    $scope.orderListModel.list[index].uomid = model.uom_id;
                }
                if (model.uomid != undefined && model.uom_id != undefined)
                {
                    $scope.orderListModel.list[index].uomid = "";
                }
//                $scope.updateProductDiscountPrice();
                $scope.updateInvoiceTotal();

                console.log("Index for item edit ", index);
                console.log("List Length ", $scope.orderListModel.list.length);
                $scope.createNewProduct(index);
                return;
            } else
            {
                $scope.createNewProduct(-1);
            }
        }
        $scope.initvalidateProductDataAndAddNewPromise = null;

        $scope.initValidateProductDataAndAddNew = function (index)
        {

            if ($scope.initvalidateProductDataAndAddNewPromise != null)
            {

                $timeout.cancel($scope.initvalidateProductDataAndAddNewPromise);
            }

            $scope.formValidator();
            $scope.initvalidateProductDataAndAddNewPromise = $timeout(
                    function () {
                        $scope.createNewProduct(index);
                    },
                    300);
        };

        $scope.formValidator = function ()
        {
            //performace issue in form validator bug
            if ($scope.purchase_invoice_add_form.$submitted)
            {
                $timeout(function () {
                    $scope.purchase_invoice_add_form.$submitted = false;
                }, 0);
                $timeout(function () {
                    $scope.purchase_invoice_add_form.$setSubmitted();
                }, 100);
            } else
            {
                $timeout(function () {
                    $scope.purchase_invoice_add_form.$setSubmitted();
                }, 100);
            }
        }

        $scope.createNewProduct = function (index)
        {
            var formDataError = false;
            if (index != -1)
            {
                var productContainer = window.document.getElementById('purchase_invoice_product_container');
                var errorCell = angular.element(productContainer).find('.has-error').length;
                if (errorCell > 0)
                {
                    formDataError = true;
                }
            }
            if (!formDataError)
            {
                if (index == $scope.orderListModel.list.length - 1)
                {

                    var newRow = {
                        "id": '',
                        "productName": '',
                        "productInfo": '',
                        "sku": '',
                        "sales_price": '',
                        "qty": '',
                        "rowtotal": '',
                        "discountPercentage": '',
                        "uom": '',
                        "uomid": '',
                        "custom_opt1": '',
                        "custom_opt2": '',
                        "custom_opt3": '',
                        "custom_opt4": '',
                        "custom_opt5": '',
                        "mrp_price": '',
                        "purchase_price": '',
                        "purchase_price_with_tax": '',
                        "tax_id": '',
                        "taxPercentage": '',
                        "selling_tax_id": '',
                        "selling_tax_percentage": '',
                        "discount_mode": '%',
                        "discount_value": '',
                        "discount_amount": 0,
                        "discount_amount_display": "0.00",
                        "hsncode": ""
                    }
                    newRow.productTaxMapping = [];
                    for (var t = 0; t < $scope.orderListModel.taxGroupList.length; t++)
                    {
                        newRow.productTaxMapping.push($scope.orderListModel.taxGroupList[t]);
                    }

                    $scope.orderListModel.list.push(newRow);
                }
            }

        }

        $scope.initUpdateProductPurchasePriceTimeoutPromise != null
        $scope.initUpdateProductPurchasePrice = function ()
        {
            if ($scope.initUpdateProductPurchasePriceTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateProductPurchasePriceTimeoutPromise);
            }
            $scope.initUpdateProductPurchasePriceTimeoutPromise = $timeout(function () {
                $scope.updateProductPurchasePrice();
            }, 300);
        }
        $scope.initUpdateProductDiscountPriceTimeoutPromise != null
        $scope.initUpdateProductDiscountPrice = function ()
        {
            if ($scope.initUpdateProductDiscountPriceTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateProductDiscountPriceTimeoutPromise);
            }
            $scope.initUpdateProductDiscountPriceTimeoutPromise = $timeout(function () {
                $scope.updateProductDiscountPrice();
            }, 300);
        }
        $scope.updateProductDiscountPrice = function ()
        {
            for (var i = 0; i < $scope.orderListModel.list.length; i++)
            {
                if ($scope.orderListModel.list[i].id != '')
                {
                    var qty = parseFloat($scope.orderListModel.list[i].qty);
                    //var mrp_price = parseFloat($scope.orderListModel.list[index].mrp_price);
                    var purchase_price = parseFloat($scope.orderListModel.list[i].purchase_price);
                    var discount_mode = $scope.orderListModel.discountMode;
                    var discount_value = parseFloat($scope.orderListModel.list[i].discount_value);
                    var discount_amount = 0;
                    var discount_amount_display = "0.00";

                    if (discount_value == '' || isNaN(discount_value))
                    {
                        discount_value = 0;
                    }

                    if (discount_mode == '%')
                    {
                        discount_amount = purchase_price * qty * (discount_value / 100);
                    } else
                    {
                        discount_amount = discount_value;
                    }
                    $scope.orderListModel.list[i].discount_mode = discount_mode;
                    $scope.orderListModel.list[i].discount_value = discount_value;
                    $scope.orderListModel.list[i].discount_amount = discount_amount;
                    $scope.orderListModel.list[i].discount_amount_display = parseFloat(discount_amount).toFixed(2);
                }
            }
            $scope.updateProductPurchasePrice();
            $scope.calculatetotal();

        }

        $scope.updateProductPurchasePrice = function ()
        {

//            if (index < $scope.orderListModel.list.length)
//            {
            for (var i = 0; i < $scope.orderListModel.list.length; i++)
            {
                for (var j = 0; j < $scope.orderListModel.list[i].productTaxMapping.length; j++)
                {
                    var taxObj = $scope.orderListModel.list[i].productTaxMapping[j];
                    if ($scope.orderListModel.list[i].tax_id == taxObj.id)
                    {
                        $scope.orderListModel.list[i].tax_percentage = taxObj.tax_percentage;
                        break;
                    }
                }
            }
            for (var i = 0; i < $scope.orderListModel.list.length; i++)
            {
                for (var j = 0; j < $scope.orderListModel.list[i].productTaxMapping.length; j++)
                {
                    var taxObj = $scope.orderListModel.list[i].productTaxMapping[j];
                    if ($scope.orderListModel.list[i].selling_tax_id == taxObj.id)
                    {
                        $scope.orderListModel.list[i].selling_tax_percentage = taxObj.tax_percentage;
                        break;
                    }
                }
            }
            for (var i = 0; i < $scope.orderListModel.list.length; i++)
            {
                if ($scope.orderListModel.list[i].tax_id == '')
                {
                    $scope.orderListModel.list[i].tax_percentage = 0;
                }
                if ($scope.orderListModel.list[i].selling_tax_id == '')
                {
                    $scope.orderListModel.list[i].selling_tax_percentage = 0;
                }

                var qty = parseFloat($scope.orderListModel.list[i].qty);
                if ($scope.orderListModel.list[i].mrp_price != null && $scope.orderListModel.list[i].mrp_price != '')
                {
                    var mrp_price = parseFloat($scope.orderListModel.list[i].mrp_price);
                }else{
                    var mrp_price = 0;
                }
                var purchase_price = parseFloat($scope.orderListModel.list[i].purchase_price);
                var productTaxPercentage = parseFloat($scope.orderListModel.list[i].tax_percentage);
                var productSellingTaxPercentage = parseFloat($scope.orderListModel.list[i].selling_tax_percentage);

                var discount_mode = $scope.orderListModel.list[i].discount_mode;
                var discount_value = parseFloat($scope.orderListModel.list[i].discount_value);
                var discount_amount = 0;
                var discount_amount_display = "0.00";

                if (discount_value == '' || isNaN(discount_value))
                {
                    discount_value = 0;
                }

                if (!isNaN(qty) && !isNaN(purchase_price) && !isNaN(productTaxPercentage) && !isNaN(productSellingTaxPercentage))
                {
                    if (discount_mode == '%')
                    {
                        discount_amount = purchase_price * qty * (discount_value / 100);
                    } else
                    {
                        discount_amount = discount_value;
                    }

                    $scope.orderListModel.list[i].single_product_discount = discount_amount / qty;
//                    $scope.orderListModel.list[i].single_product_discount = discount_amount;
//                    if (discount_mode == '%')
//                    {
//                        $scope.orderListModel.list[i].single_product_discount = discount_amount / qty;
//                    }
                    $scope.orderListModel.list[i].single_product_discount = parseFloat($scope.orderListModel.list[i].single_product_discount).toFixed(2);

                    $scope.orderListModel.list[i].discount_amount = discount_amount;
                    $scope.orderListModel.list[i].discount_amount_display = parseFloat(discount_amount).toFixed(2);

                    var purchase_price_tax_incl_discount = parseFloat(purchase_price * 1) - parseFloat($scope.orderListModel.list[i].single_product_discount);
//                    var purchase_price_tax_incl_discount = parseFloat(purchase_price * 1);
                    var singleProducttax_amount = parseFloat(purchase_price_tax_incl_discount * productTaxPercentage / 100).toFixed(2);
                    $scope.orderListModel.list[i].purchase_price_with_tax = parseFloat(purchase_price_tax_incl_discount) + parseFloat(singleProducttax_amount);
                    $scope.orderListModel.list[i].purchase_price_with_tax = parseFloat($scope.orderListModel.list[i].purchase_price_with_tax).toFixed(2);

                    var row_total_with_tax_incl_discount = (purchase_price * qty) - parseFloat($scope.orderListModel.list[i].discount_amount);
                    $scope.orderListModel.list[i].tax_amount = parseFloat(row_total_with_tax_incl_discount * productTaxPercentage / 100).toFixed(2);
//                    $scope.orderListModel.list[index].row_total_with_tax = parseFloat(row_total_with_tax_incl_discount) + parseFloat($scope.orderListModel.list[index].tax_amount);
                    $scope.orderListModel.list[i].row_total_with_tax = parseFloat($scope.orderListModel.list[i].tax_amount);
                    $scope.orderListModel.list[i].row_total_with_tax = parseFloat($scope.orderListModel.list[i].row_total_with_tax).toFixed(2);

                    $scope.orderListModel.list[i].sales_price = parseFloat(mrp_price / (1 + productSellingTaxPercentage / 100)).toFixed(2);

                } else
                {

                    $scope.orderListModel.list[i].tax_amount = 0;
                    $scope.orderListModel.list[i].purchase_price_with_tax = 0;

                }

            }
            $scope.calculatetotal();

            //}

        }


        $scope.deleteProduct = function (index)
        {
//            var index = -1;
//            for (var i = 0; i < $scope.orderListModel.list.length; i++) {
//                if (id == $scope.orderListModel.list[i].id) {
//                    index = i;
//                    break;
//                }
//            }
            $scope.orderListModel.list.splice(index, 1);
            $scope.updateInvoiceTotal();
        };

        $scope.taxList = [];
        $scope.calculatetotal = function ()
        {
            var subTotal = 0;
            var totalDiscount = 0;
            var totalTaxAmount = 0;
            var totDiscountPercentage = 0;
            var totTaxPercentage = 0;
            var totalTaxPercentage = 0;
            var totMapTax = 0;
            var totQty = 0;
            console.log("bfr loop", $scope.orderListModel.list.length);
            for (var i = 0; i < $scope.orderListModel.list.length; i++)
            {

                if ($scope.orderListModel.list[i].productInfo != null && $scope.orderListModel.list[i].productInfo.id != undefined &&
                        !isNaN($scope.orderListModel.list[i].qty) &&
                        !isNaN($scope.orderListModel.list[i].sales_price) &&
                        !isNaN($scope.orderListModel.list[i].purchase_price) &&
                        !isNaN($scope.orderListModel.list[i].purchase_price_with_tax))
                {
                    totalDiscount += parseFloat($scope.orderListModel.list[i].discount_amount);
                    totalTaxAmount += parseFloat($scope.orderListModel.list[i].tax_amount);
                    totQty += parseFloat($scope.orderListModel.list[i].qty);
                    $scope.orderListModel.list[i].rowtotal = $scope.orderListModel.list[i].purchase_price * $scope.orderListModel.list[i].qty;
                    subTotal += parseFloat($scope.orderListModel.list[i].rowtotal);
                    $scope.orderListModel.list[i].rowtotal = $scope.orderListModel.list[i].rowtotal.toFixed(2);
                }

            }
//            if ($scope.orderListModel.discountMode == 'amount')
//            {
//                totalDiscount = $scope.orderListModel.discount_value;
//            }
            $scope.orderListModel.taxAmt = parseFloat(totalTaxAmount).toFixed(2);
            $scope.orderListModel.subtotal = parseFloat(subTotal).toFixed(2);
            $scope.orderListModel.totalQty = parseFloat(totQty);
            $scope.orderListModel.discountAmt = parseFloat(totalDiscount).toFixed(2);
            if ($scope.orderListModel.subtotal > 0)
            {
                $scope.orderListModel.subtotalWithDiscount = $scope.orderListModel.subtotal - totalDiscount;
                $scope.orderListModel.total = (parseFloat(subTotal) + parseFloat(totalTaxAmount) - parseFloat($scope.orderListModel.discountAmt)).toFixed(2);
            } else
            {
                $scope.orderListModel.subtotalWithDiscount = 0.00;
                $scope.orderListModel.total = 0.00;
            }
            $scope.orderListModel.balance_amount = (parseFloat($scope.orderListModel.total - $scope.orderListModel.paid_amount)).toFixed(2);
            $scope.updateRoundoff();
        }

        $scope.updateRoundoff = function ()
        {
            $scope.orderListModel.round = Math.round($scope.orderListModel.total);
            $scope.orderListModel.roundoff = $scope.orderListModel.round - parseFloat($scope.orderListModel.total);
            $scope.orderListModel.roundoff = parseFloat($scope.orderListModel.roundoff).toFixed(2);
            $scope.orderListModel.round = parseFloat($scope.orderListModel.round).toFixed(2);
        }

        $scope.$on("updateSelectedProductEvent", function (event, selectedItems)
        {
            if (typeof selectedItems == 'undefined')
                return;
            if ($scope.orderListModel.list.length != 0)
            {
                var isDuplicateProduct = false;
                var duplicateProductIndex = -1;
                for (i = 0; i < $scope.orderListModel.list.length; i++)
                {
                    if (selectedItems.id == $scope.orderListModel.list[i].id)
                    {
                        isDuplicateProduct = true;
                        duplicateProductIndex = i;
                    }

                }
                if (isDuplicateProduct)
                {
                    $scope.orderListModel.list[duplicateProductIndex].qty = $scope.orderListModel.list[duplicateProductIndex].qty + 1;
                    console.log("upd 4");
                    $scope.updateInvoiceTotal();
                } else
                {
                    var newRow = {
                        "id": selectedItems.id,
                        "productName": selectedItems.name,
                        "sku": selectedItems.sku,
                        "sales_price": selectedItems.sales_price,
                        "qty": 1,
                        "rowtotal": selectedItems.sales_price,
                        "discountPercentage": selectedItems.discountPercentage,
                        "taxPercentage": selectedItems.taxPercentage,
                        "uom": selectedItems.uom,
                        "uomid": selectedItems.uom_id
                    }
                    $scope.orderListModel.list.push(newRow);
                    console.log("upd 2");
                    $scope.updateInvoiceTotal();
                    return;
                }
            } else
            {
                var newRow = {
                    "id": selectedItems.id,
                    "productName": selectedItems.name,
                    "sku": selectedItems.sku,
                    "sales_price": selectedItems.sales_price,
                    "qty": 1,
                    "rowtotal": selectedItems.sales_price,
                    "discountPercentage": selectedItems.discountPercentage,
                    "taxPercentage": selectedItems.taxPercentage,
                    "uom": selectedItems.uom,
                    "uomid": selectedItems.uom_id
                }
                $scope.orderListModel.list.push(newRow);
                $scope.updateInvoiceTotal();
            }
        });
        $scope.display_balance = false;
        $scope.updateInvoiceDetails = function ( )
        {
            if (typeof $scope.orderListModel.invoiceDetail != 'undefined' && $scope.orderListModel.invoiceDetail != null)
            {
                $scope.orderListModel.customerInfo = {};
                $scope.orderListModel.customerInfo.id = $scope.orderListModel.invoiceDetail.customer_id;
                $scope.orderListModel.customerInfo.fname = $scope.orderListModel.invoiceDetail.customer_fname;
                $scope.orderListModel.customerInfo.shopping_address = $scope.orderListModel.invoiceDetail.customer_address;
                $scope.orderListModel.customerInfo.phone = $scope.orderListModel.invoiceDetail.phone;
                $scope.orderListModel.customerInfo.email = $scope.orderListModel.invoiceDetail.email;
                $scope.orderListModel.subtotal = $scope.orderListModel.invoiceDetail.subtotal;
                $scope.orderListModel.taxAmt = $scope.orderListModel.invoiceDetail.tax_amount;
                $scope.orderListModel.taxAmountDetail.tax_amount = $scope.orderListModel.invoiceDetail.tax_amount;
                $scope.orderListModel.discountAmt = $scope.orderListModel.invoiceDetail.discount_amount;
                $scope.orderListModel.total = $scope.orderListModel.invoiceDetail.total_amount;
                $scope.display_balance = true;
                $scope.orderListModel.paid_amount = $scope.orderListModel.invoiceDetail.advance_amount;
                $scope.orderListModel.balance_amount = (parseFloat($scope.orderListModel.total - $scope.orderListModel.paid_amount)).toFixed(2);
                $scope.orderListModel.invoicePrefix = $scope.orderListModel.invoiceDetail.prefix;
                //                var date = utilityService.parseStrToDate($scope.orderListModel.invoiceDetail.date);
                //                $scope.orderListModel.billdate = $filter('date')(date, $scope.dateFormat);
                $scope.orderListModel.duedate = $scope.orderListModel.invoiceDetail.duedate;
                $scope.orderListModel.discountPercentage = $scope.orderListModel.invoiceDetail.discount_percentage + '';
                $scope.orderListModel.discountMode = $scope.orderListModel.invoiceDetail.discount_mode;
                $scope.orderListModel.tax_id = $scope.orderListModel.invoiceDetail.tax_id + '';
                var loop;

                for (loop = 0; loop < $scope.orderListModel.taxInfo.length; loop++)
                {
                    if ($scope.orderListModel.taxInfo[loop].id = $scope.orderListModel.invoiceDetail.tax_id)
                    {
                        $scope.orderListModel.taxPercentage = $scope.orderListModel.invoiceDetail.tax_percentage;
                        break;
                    }
                }

                for (loop = 0; loop < $scope.orderListModel.invoiceDetail.item.length; loop++)
                {
                    var newRow =
                            {
                                "id": $scope.orderListModel.invoiceDetail.item[loop].product_id,
                                "name": $scope.orderListModel.invoiceDetail.item[loop].product_name,
                                "sku": $scope.orderListModel.invoiceDetail.item[loop].product_sku,
                                "sales_price": $scope.orderListModel.invoiceDetail.item[loop].unit_price,
                                "qty": $scope.orderListModel.invoiceDetail.item[loop].qty,
                                "rowtotal": $scope.orderListModel.invoiceDetail.item[loop].total_price,
                                "uom": $scope.orderListModel.invoiceDetail.item[loop].uom_name,
                                "uomid": $scope.orderListModel.invoiceDetail.item[loop].uom_id
                            };
                    $scope.orderListModel.list.push(newRow);
                    $scope.orderListModel.list[loop].productInfo = $scope.orderListModel.list[loop];
                }

//                var newRow = {
//                    "id": '',
//                    "productName": '',
//                    "sku": '',
//                    "sales_price": '',
//                    "qty": '',
//                    "rowtotal": '',
//                    "discountPercentage": '',
//                    "taxPercentage": '',
//                    "uom": '',
//                    "uomid": ''
//                }
//                $scope.orderListModel.list.push(newRow);
                $scope.orderListModel.isLoadingProgress = false;
            }
        }

        $scope.getInvoiceInfo = function ( )
        {
            $scope.orderListModel.isLoadingProgress = true;
            var getListParam = {};
            if ($stateParams.id != "")
            {
                getListParam.id = $stateParams.id;
                getListParam.is_active = 1;
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getPurchaseQuoteDetail(getListParam, configOption).then(function (response)
                {
                    var data = response.data.data;
                    $scope.orderListModel.invoiceDetail = data;
                    $scope.initUpdateDetail();
                });
            }
        }

        $scope.checkId = function ()
        {
            if (typeof $stateParams.id != 'undefined' && $stateParams.id != null && $stateParams.id != '')
            {
                $scope.getInvoiceInfo();
            }
        };
        $scope.checkId();


        $scope.createOrder = function (forceSave)
        {
            if (!forceSave)
            {
                if ($scope.isDataSavingProcess)
                {
                    return;
                }
            }
            $scope.isDataSavingProcess = true;
            $scope.isSave = false;
            for (var i = 0; i < $scope.orderListModel.list.length; i++)
            {
                if (i != $scope.orderListModel.list.length - 1)
                {
                    if ($rootScope.appConfig.uomdisplay)
                    {


                        if ($scope.orderListModel.list[i].productInfo != '' && $scope.orderListModel.list[i].productInfo.id != undefined &&
                                $scope.orderListModel.list[i].uomInfo != '' && $scope.orderListModel.list[i].uomInfo != undefined &&
                                !isNaN($scope.orderListModel.list[i].qty) &&
                                !isNaN($scope.orderListModel.list[i].sales_price) && ($scope.orderListModel.list[i].sales_price != null) && ($scope.orderListModel.list[i].sales_price != '') &&
                                !isNaN($scope.orderListModel.list[i].purchase_price) &&
                                !isNaN($scope.orderListModel.list[i].mrp_price) && ($scope.orderListModel.list[i].mrp_price != null) && ($scope.orderListModel.list[i].mrp_price != '') &&
                                !isNaN($scope.orderListModel.list[i].purchase_price_with_tax))
                        {
                            $scope.isSave = true;
                        } else
                        {
                            $scope.isSave = false;
                            break;
                        }
                    } else
                    {
                        if ($scope.orderListModel.list[i].productInfo != '' && $scope.orderListModel.list[i].productInfo != undefined &&
                                !isNaN($scope.orderListModel.list[i].qty) && ($scope.orderListModel.list[i].qty != null) && ($scope.orderListModel.list[i].qty != '') &&
                                !isNaN($scope.orderListModel.list[i].sales_price) && ($scope.orderListModel.list[i].sales_price != null) && ($scope.orderListModel.list[i].sales_price != '') &&
                                !isNaN($scope.orderListModel.list[i].purchase_price) && ($scope.orderListModel.list[i].purchase_price != null) && ($scope.orderListModel.list[i].purchase_price != '') &&
                                !isNaN($scope.orderListModel.list[i].mrp_price) && ($scope.orderListModel.list[i].mrp_price != null) && ($scope.orderListModel.list[i].mrp_price != '') &&
                                !isNaN($scope.orderListModel.list[i].purchase_price_with_tax))
                        {
                            $scope.isSave = true;
                        } else
                        {
                            $scope.isSave = false;
                            break;
                        }
                    }
                }
                if ($scope.orderListModel.list.length == 1)
                {
                    if ($rootScope.appConfig.uomdisplay)
                    {
                        if ($scope.orderListModel.list[i].productInfo != '' && $scope.orderListModel.list[i].productInfo.id != undefined &&
                                $scope.orderListModel.list[i].uomInfo != '' && $scope.orderListModel.list[i].uomInfo != undefined &&
                                !isNaN($scope.orderListModel.list[i].qty) &&
                                !isNaN($scope.orderListModel.list[i].sales_price) && ($scope.orderListModel.list[i].sales_price != null) && ($scope.orderListModel.list[i].sales_price != '') &&
                                !isNaN($scope.orderListModel.list[i].purchase_price) &&
                                !isNaN($scope.orderListModel.list[i].mrp_price) && ($scope.orderListModel.list[i].mrp_price != null) && ($scope.orderListModel.list[i].mrp_price != '') &&
                                !isNaN($scope.orderListModel.list[i].purchase_price_with_tax))
                        {
                            $scope.isSave = true;
                        } else
                        {
                            $scope.isSave = false;
                            break;
                        }
                    } else
                    {
                        if ($scope.orderListModel.list[i].productInfo != '' && $scope.orderListModel.list[i].productInfo != undefined &&
                                !isNaN($scope.orderListModel.list[i].qty) &&
                                !isNaN($scope.orderListModel.list[i].sales_price) &&
                                !isNaN($scope.orderListModel.list[i].purchase_price) &&
                                !isNaN($scope.orderListModel.list[i].mrp_price) &&
                                !isNaN($scope.orderListModel.list[i].purchase_price_with_tax))
                        {
                            $scope.isSave = true;
                        } else
                        {
                            $scope.isSave = false;
                            break;
                        }
                    }
                }
            }

            if ($scope.isSave)
            {
                $scope.getReferenceList(false);

            } else
            {
                if ($scope.orderListModel.list[i].purchase_price != "" || $scope.orderListModel.list[i].sales_price == '' || $scope.orderListModel.list[i].mrp_price == '' || $scope.orderListModel.list[i].sales_price == null || $scope.orderListModel.list[i].mrp_price == null)
                {
                    sweet.show('Oops...', 'Fill Product Details', 'error');
                    $scope.isDataSavingProcess = false;
                } else
                {
                    if ($scope.orderListModel.list[i].purchase_price == "" || $scope.orderListModel.list[i].sales_price == '' || $scope.orderListModel.list[i].mrp_price == '' || $scope.orderListModel.list[i].sales_price == null && $scope.orderListModel.list[i].mrp_price == null)
                    {
                        sweet.show('Oops...', 'Check Price Detail', 'error');
                        $scope.isDataSavingProcess = false;
                    } else
                    {
                        sweet.show('Oops...', 'Fill Product Details', 'error');
                        $scope.isDataSavingProcess = false;
                    }
                }
                var element = document.getElementById("btnLoad");
                element.classList.remove("btn-loader");
            }
        };
//        $scope.showAlert = true;
//        setTimeout(function () {
//            if ($rootScope.userModel.new_user == 1 && $rootScope.hideAlert == false)
//            {
//                $window.localStorage.setItem("demo_guide", "false");
//                swal({
//                    title: "Guide Tour!",
//                    text: "Kindly fill all the required details and click submit to proceed the demo..",
//                    confirmButtonText: "OK"
//                },
//                        function (isConfirm) {
//                            if (isConfirm) {
//                                $rootScope.hideAlert = true;
//                            }
//                        });
//            }
//        }, 2000);
        $scope.saveOrder = function ()
        {
            var createOrderParam = {};
            var headers = {};
            headers['screen-code'] = 'purchaseinvoice';
            createOrderParam.purchase_quote_id = $stateParams.id;
            createOrderParam.status = 'unpaid';
            createOrderParam.id = $scope.orderListModel.invoiceno;
            createOrderParam.prefix = $scope.orderListModel.invoicePrefix;
            createOrderParam.purchase_no = $scope.orderListModel.purchase_no;
            createOrderParam.reference_no = $scope.orderListModel.invoice_no;
            //createOrderParam.purchase_code = $scope.orderListModel.purchase_code;
            $scope.calculateDueDate();
            if (typeof $scope.orderListModel.billdate == 'object')
            {
                var billdate = utilityService.parseDateToStr($scope.orderListModel.billdate, $scope.adminService.appConfig.date_format);
            }
            createOrderParam.date = utilityService.changeDateToSqlFormat(billdate, $scope.adminService.appConfig.date_format);
            if (typeof $scope.orderListModel.duedate == 'object')
            {
                duedate = utilityService.parseDateToStr($scope.orderListModel.duedate, $scope.adminService.appConfig.date_format);
            }
            createOrderParam.duedate = utilityService.changeDateToSqlFormat(duedate, $scope.adminService.appConfig.date_format);
            createOrderParam.customer_address = $scope.orderListModel.customerInfo.shopping_address;
            createOrderParam.subtotal = $scope.orderListModel.subtotal;
            createOrderParam.tax_amount = (parseFloat($scope.orderListModel.taxAmt)).toFixed(2);
            createOrderParam.discount_amount = (parseFloat($scope.orderListModel.discountAmt)).toFixed(2);
            createOrderParam.discount_mode = $scope.orderListModel.discountMode;
            createOrderParam.discount_percentage = $scope.orderListModel.discount_value;
            createOrderParam.total_amount = $scope.orderListModel.round;
            createOrderParam.round_off = (parseFloat($scope.orderListModel.roundoff)).toFixed(2);
            createOrderParam.paymentmethod = "";
            createOrderParam.notes = "";
            createOrderParam.item = [];
            createOrderParam.customer_id = $scope.orderListModel.customerInfo.id;
            createOrderParam.reference_no = $scope.orderListModel.reference_no;
            //Don't remove below line overall qty
            createOrderParam.overall_qty = $scope.orderListModel.totalQty;
            //Don't remove above line it must sent
            var length = 0;
            if ($scope.orderListModel.list.length == 1)
            {
                length = 2;
            } else
            {
                length = $scope.orderListModel.list.length;
            }
            for (var i = 0; i < length - 1; i++)
            {


                var ordereditems = {};

                if (typeof $scope.orderListModel.list[i].productInfo != undefined && $scope.orderListModel.list[i].productInfo.id != undefined)
                {
                    ordereditems.product_id = $scope.orderListModel.list[i].productInfo.id;
                    ordereditems.product_sku = $scope.orderListModel.list[i].productInfo.sku;
                    ordereditems.product_name = $scope.orderListModel.list[i].productInfo.name;
                }
                ordereditems.unit_price = parseFloat($scope.orderListModel.list[i].purchase_price);
                ordereditems.qty = parseFloat($scope.orderListModel.list[i].qty).toFixed($scope.quantityFormat);
                ordereditems.hsn_code = $scope.orderListModel.list[i].hsncode;
                if (typeof $scope.orderListModel.list[i].uomInfo != undefined && $scope.orderListModel.list[i].uomInfo != null && $scope.orderListModel.list[i].uomInfo != '')
                {
                    if ($scope.orderListModel.list[i].uomInfo.name != null && $scope.orderListModel.list[i].uomInfo.name != '')
                    {
                        ordereditems.uom_name = $scope.orderListModel.list[i].uomInfo.name;
                    }
                }
                ordereditems.uom_id = $scope.orderListModel.list[i].uomid;
                ordereditems.custom_opt1 = '';
                ordereditems.custom_opt2 = '';
                ordereditems.custom_opt3 = '';
                ordereditems.custom_opt4 = '';
                ordereditems.custom_opt5 = '';
                if ($scope.orderListModel.list[i].custom_opt1 != '' && $scope.orderListModel.list[i].custom_opt1 != null)
                {
                    ordereditems.custom_opt1 = $scope.orderListModel.list[i].custom_opt1;
                }
                if ($scope.orderListModel.list[i].custom_opt2 != '' && $scope.orderListModel.list[i].custom_opt2 != null)
                {
                    ordereditems.custom_opt2 = $scope.orderListModel.list[i].custom_opt2;
                }
                if ($scope.orderListModel.list[i].custom_opt3 != '' && $scope.orderListModel.list[i].custom_opt3 != null)
                {
                    ordereditems.custom_opt3 = $scope.orderListModel.list[i].custom_opt3;
                }
                if ($scope.orderListModel.list[i].custom_opt4 != '' && $scope.orderListModel.list[i].custom_opt4 != null)
                {
                    ordereditems.custom_opt4 = $scope.orderListModel.list[i].custom_opt4;
                }
                if ($scope.orderListModel.list[i].custom_opt5 != '' && $scope.orderListModel.list[i].custom_opt5 != null)
                {
                    ordereditems.custom_opt5 = $scope.orderListModel.list[i].custom_opt5;
                }

                ordereditems.custom_opt1_key = $rootScope.appConfig.purchase_invoice_item_custom1_label;
                ordereditems.custom_opt2_key = $rootScope.appConfig.purchase_invoice_item_custom2_label;
                ordereditems.custom_opt3_key = $rootScope.appConfig.purchase_invoice_item_custom3_label;
                ordereditems.custom_opt4_key = $rootScope.appConfig.purchase_invoice_item_custom4_label;
                ordereditems.custom_opt5_key = $rootScope.appConfig.purchase_invoice_item_custom5_label;
                ordereditems.mrp_price = $scope.orderListModel.list[i].mrp_price;
                ordereditems.is_new = 0;
                ordereditems.purchase_price = $scope.orderListModel.list[i].purchase_price;
                ordereditems.purchase_price_w_tax = $scope.orderListModel.list[i].purchase_price_with_tax;
                ordereditems.selling_price = $scope.orderListModel.list[i].sales_price;
                ordereditems.selling_tax_id = $scope.orderListModel.list[i].selling_tax_id;
                ordereditems.selling_tax_percentage = $scope.orderListModel.list[i].selling_tax_percentage;
                ordereditems.discount_mode = $scope.orderListModel.list[i].discount_mode;
                ordereditems.discount_value = $scope.orderListModel.list[i].discount_value;
                ordereditems.discount_amount = parseFloat($scope.orderListModel.list[i].discount_amount).toFixed(2);
                ordereditems.tax_id = $scope.orderListModel.list[i].tax_id;
                ordereditems.tax_percentage = $scope.orderListModel.list[i].tax_percentage;
                ordereditems.tax_amount = ordereditems.purchase_price * ordereditems.qty * (ordereditems.tax_percentage / 100);
                ordereditems.tax_amount = parseFloat(ordereditems.tax_amount).toFixed(2);
                ordereditems.total_price = parseFloat($scope.orderListModel.list[i].purchase_price) * parseFloat(ordereditems.qty) - parseFloat(ordereditems.discount_amount) + parseFloat(ordereditems.tax_amount);
                ordereditems.total_price = parseFloat(ordereditems.total_price).toFixed(2);

                createOrderParam.item.push(ordereditems);

            }
            createOrderParam.customattribute = [];
            for (var i = 0; i < $scope.customAttributeList.length; i++)
            {
                var customattributeItem = {};
                if ($scope.customAttributeList[i].input_type == 'date')
                {
                    if ($scope.customAttributeList[i].value != '' && $scope.customAttributeList[i].value != null && $scope.customAttributeList[i].value != undefined)
                    {
                        if (typeof $scope.customAttributeList[i].value == 'object')
                        {
                            $scope.customdate = utilityService.parseDateToStr($scope.customAttributeList[i].value, $scope.adminService.appConfig.date_format);
                        }
                        $scope.customAttributeList[i].value = utilityService.changeDateToSqlFormat($scope.customdate, $scope.adminService.appConfig.date_format);
                    } else
                    {
                        $scope.customAttributeList[i].value = '';
                    }
                }
                if ($scope.customAttributeList[i].input_type == "YesOrNo")
                {
                    if ($scope.customAttributeList[i].value == true || $scope.customAttributeList[i].value == "yes")
                    {
                        $scope.customAttributeList[i].value = "yes";
                    } else
                    {
                        $scope.customAttributeList[i].value = "no";
                    }
                }
                if ($scope.customAttributeList[i].value != undefined)
                {
                    customattributeItem.value = $scope.customAttributeList[i].value;
                } else if ($scope.customAttributeList[i].default_value != undefined)
                {
                    customattributeItem.value = $scope.customAttributeList[i].default_value;
                } else
                {
                    customattributeItem.value = "";
                }

                customattributeItem.attribute_code = $scope.customAttributeList[i].attribute_code;
                createOrderParam.customattribute.push(customattributeItem);
            }
            if (parseFloat(createOrderParam.total_amount) > 0)
            {
                adminService.addPurchaseInvoice(createOrderParam, headers).then(function (response) {
                    if (response.data.success == true)
                    {
                        $scope.showAlert = false;
                        $scope.isDataSavingProcess = false;
//                        $scope.formReset();
                        $state.go('app.purchaseInvoiceView', {
                            'id': response.data.id
                        }, {
                            'reload': true
                        });
//                        if ($rootScope.userModel.new_user == 1)
//                        {
//                            $rootScope.closeDemoPopup(8);
//                        }
                    } else
                    {
                        $scope.isDataSavingProcess = false;
                    }
                }).catch(function (response) {
                    console.error('Error occurred:', response.status, response.data);
                    $scope.isDataSavingProcess = false;
                    var element = document.getElementById("btnLoad");
                    element.classList.remove("btn-loader");
                });
            } else
            {
                sweet.show('Oops...', 'Invoice amount cannot be zero', 'error');
                $scope.isDataSavingProcess = false;
                var element = document.getElementById("btnLoad");
                element.classList.remove("btn-loader");
            }
        }
        $scope.getCustomAttributeList = function () {

            var getListParam = {};
            getListParam.id = "";
            getListParam.attributetype_id = "";
            getListParam.attribute_code = "";
            getListParam.attributetypecode = "purchaseinvoice";

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getAttributeList(getListParam, configOption).then(function (response)
            {
                var data = response.data.list;
                $scope.customAttributeList = data;
                for (var i = 0; i < $scope.customAttributeList.length; i++)
                {
                    if ($scope.customAttributeList[i].input_type == "YesOrNo")
                    {
                        if ($scope.customAttributeList[i].default_value.toLowerCase() == "yes")
                        {
                            $scope.customAttributeList[i].value = true;
                        } else
                        {
                            $scope.customAttributeList[i].value = false;
                        }
                    }
                }

            });
        };

        $scope.$on('customSelectOptionChangeEvent', function (event, data)
        {
            console.log(data);
            $scope.updateCustomSelectValue(data);
        });

        $scope.updateCustomSelectValue = function (data)
        {
            for (var i = 0; i < $scope.customAttributeList.length; i++)
            {
                if ($scope.customAttributeList[i].attribute_code == data.code)
                {
                    $scope.customAttributeList[i].value = data.value;
                    break;
                }
            }
        }

        $scope.getCustomAttributeList();

        /*
         * Listening the Custom field value change
         */
        $scope.$on('customfieldValueChangeEvent', function (event, data)
        {
            console.log(data);
            $scope.updateCustomFieldValue(data);
        });

        $scope.updateCustomFieldValue = function (data)
        {
            for (var i = 0; i < $scope.customAttributeList.length; i++)
            {
                if ($scope.customAttributeList[i].attribute_code == data.code)
                {
                    $scope.customAttributeList[i].value = data.value;
                    break;
                }
            }
        }
        $scope.isPrefixListLoaded = false;
        $scope.getPrefixList = function ()
        {
            $scope.defaultprefix = false;
            var getListParam = {};
            getListParam.setting = 'purchase_invoice_prefix';
            var headers = {};
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getAppSettingsList(getListParam, configOption, headers).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    if (data.list.length > 0)
                    {
                        var perfix = data.list[0].value;
                        perfix = perfix.split('\n');
                        for (var i = 0; i < perfix.length; i++)
                        {
                            if (perfix[i].split('-')[1] == 'default')
                            {
                                perfix[i] = perfix[i].split('-')[0];
                                $scope.orderListModel.invoicePrefix = perfix[i] + '';
                                $scope.defaultprefix = true;

                            }
                            if ($scope.defaultprefix == false)
                            {
                                $scope.orderListModel.invoicePrefix = '';
                            }
                            $scope.orderListModel.prefixList.push(perfix[i]);
                        }
                    }

                }
                $scope.isPrefixListLoaded = true;
                $scope.initUpdateDetail();
            });

        };

        $scope.isNextAutoGeneratorNumberLoaded = false;
        $scope.getNextInvoiceno = function ()
        {
            $scope.isNextAutoGeneratorNumberLoaded = true;
            var getListParam = {};
            getListParam.type = "purchase_invoice";
            getListParam.prefix = $scope.orderListModel.invoicePrefix;

            var headers = {};
            var configOption = adminService.handleOnlyErrorResponseConfig;
            configOption.perfix = getListParam.prefix;
            adminService.getNextInvoiceNo(getListParam, configOption, headers).then(function (response)
            {
                var config = response.config.config;
                if (response.data.success === true && typeof config.perfix !== 'undefined' && config.perfix == $scope.orderListModel.invoicePrefix)
                {
                    if (!$scope.orderListModel.is_next_autogenerator_num_changed)
                    {
                        $scope.orderListModel.next_autogenerator_num = response.data.no;
                    }
                }
                $scope.isNextAutoGeneratorNumberLoaded = false;

            });

        };

        $scope.invoiceNumberChange = function ()
        {

            $scope.orderListModel.is_next_autogenerator_num_changed = true;
            $timeout(function () {
                $scope.orderListModel.purchase_no = $scope.orderListModel.next_autogenerator_num;
            }, 0);
        }
        $scope.event = null;
        $scope.keyupHandler = function (event)
        {
            if ($scope.orderListModel.customerInfo != '' && $scope.orderListModel.customerInfo != null && $scope.orderListModel.customerInfo.id != undefined && $scope.orderListModel.customerInfo.id != '' && $scope.orderListModel.reference_no != undefined && $scope.orderListModel.reference_no != '')
            {
                $timeout(function () {
                    $scope.getReferenceList(true);
                }, 1000)

            }

        }
        $scope.getReferenceList = function (fromKey) {

            if (typeof $scope.orderListModel.customerInfo.id != 'undefined' && $scope.orderListModel.customerInfo.id != '' && typeof $scope.orderListModel.reference_no != 'undefined' && $scope.orderListModel.reference_no != '')
            {
                var getListParam = {};
                getListParam.customer_id = $scope.orderListModel.customerInfo.id;
                getListParam.reference_no = $scope.orderListModel.reference_no;
                getListParam.purchase_invoice_id = 0;
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getReferenceDetail(getListParam, configOption).then(function (response)
                {
                    if (response.data.success == true)
                    {
                        if (fromKey == false)
                        {
                            $scope.saveOrder();
                        }
                    } else if (response.data.success == false)
                    {
                        sweet.show("Oops", 'Invoice number already exists', "error");
                        $scope.orderListModel.reference_no = '';
                        $scope.isDataSavingProcess = false;
                        var element = document.getElementById("btnLoad");
                        element.classList.remove("btn-loader");

                    }

                });
            }
        };

        $scope.getTaxListInfo();
        $scope.getPrefixList();
        $scope.getNextInvoiceno();

    }]);




