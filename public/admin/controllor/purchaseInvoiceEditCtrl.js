
app.controller('purchaseInvoiceEditCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', 'sweet', '$window', function ($scope, $rootScope, adminService, $httpService, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST, sweet, $window) {

        $scope.orderListModel =
                {
                    "id": 0,
                    "productInfo": "",
                    "productname": "",
                    "sku": "",
                    "unit": "",
                    "mrp": "",
                    "sales_price": "",
                    "qty": "",
                    "rowtotal": "",
                    "isActive": "1",
                    "total": 0,
                    "customername": "",
                    "phone": "",
                    "addressId": "",
                    "subtotal": "0.00",
                    "taxAmt": 0,
                    "taxList": "",
                    "discountAmt": 0,
                    list: [],
                    invoice_list: [],
                    "billno": "",
                    "billdate": "",
                    "coupon": "",
                    "shipping": 0,
                    "discamount": 0,
                    "code": "",
                    "purchase_no": "",
                    "invoice_no": "",
                    "purchase_code": "",
                    "customerInfo": "",
                    "invoiceDetail": {},
                    "paymenttype": "CASH",
                    "couponcode": "",
                    "invoicePrefix": "",
                    "paymentTerm": "",
                    "taxPercentage": "",
                    "discountPercentage": "",
                    "totalDisPercentage": "0",
                    "totalTaxPercentage": "",
                    "discountMode": "",
                    "discount_value": '',
                    "discountamount": "",
                    "taxamount": "",
                    "cardDetail": "",
                    "duedate": "",
                    "tax_id": '',
                    "paid_amount": "",
                    "productList": [],
                    "productId": '',
                    "isLoadingProgress": false,
                    "taxAmountDetail": {
                        "tax_amount": ''
                    },
                    "taxAmountDetail": '',
                    "subtotalWithDiscount": '',
                    "roundoff": '0.00',
                    "round": "0.00",
                    "prefixList": [],
                    "reference_no": '',
                    taxGroupList: []
                };
        $scope.quantityFormat = $rootScope.appConfig.qty_format;
        $scope.adminService = adminService;
        $scope.customAttributeList = [];
        $scope.validationFactory = ValidationFactory;
        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function ()
        {
            if (typeof $scope.purchase_order_edit_form != 'undefined' && typeof $scope.purchase_order_edit_form.$pristine != 'undefined' && !$scope.purchase_order_edit_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function ()
        {
            $scope.purchase_order_edit_form.$setPristine();
            $scope.orderListModel.invoiceDetail = {};
            $scope.orderListModel.list = [];
            $scope.orderListModel.tax_id = "";
            $scope.getInvoiceInfo();
        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.resetOrderCreate = function () {

            $scope.orderListModel.user = '';
        }

        $scope.invoiceEditId = $stateParams.id;
        $scope.opened = false;
        $scope.initTableFilterTimeoutPromise = null;
        $scope.deletedProductList = [];
        $scope.undeletedProductList = [];
        $scope.showbillPopup = false;
        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }
        $scope.maxDate = $scope.maxDate ? null : new Date();
        $scope.startDateOpen = false;
        $scope.dueDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currencyFormat = $rootScope.appConfig.currency;
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.currentDate = new Date();
        $scope.openDate = function (index)
        {
            if (index == 0)
            {
                $scope.startDateOpen = true;
            } else if (index == 1)
            {
                $scope.dueDateOpen = true;
            }
        }

        $scope.senderEmail = '';
        $scope.receiver = [];
        $scope.ccEmail = '';
        $scope.listType = "product";
        if ($rootScope.appConfig.sales_product_list.toLowerCase() == "purchase based")
        {
            $scope.listType = "purchse";
        }
        $scope.showPopup = function (index) {
            $scope.showpopup = true;
            $scope.mailHeader = "Send a Receipt";
            $scope.senderEmail = $scope.orderListModel.customerInfo.email;
            $scope.receiver = [];
            var newRow = {
                "email": $scope.orderListModel.customerInfo.email
            };
            $scope.receiver.push(newRow);
            $scope.ccEmail = $scope.orderListModel.customerInfo.email;
            console.log("Sender Email ", $scope.senderEmail);
            console.log("Sender Email ", $scope.receiverEmail);
            console.log("Sender Email ", $scope.ccEmail);
            $scope.$broadcast("initPopupEvent");
        };
        $scope.addMoreReceiverEmail = function ()
        {
            var newRow = {
                "email": ""
            };
            $scope.receiver.push(newRow);
        }

        $scope.closePopup = function () {
            $scope.showpopup = false;
        }

        $scope.getCustomerList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            autosearchParam.mode = 2;
            return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = [];
                if (data.length > 0)
                {
                    for (var i = 0; i < data.length; i++)
                    {
                        if (data[i].is_purchase == '1' || data[i].is_purchase == 1)
                        {
                            hits.push(data[i]);
                        }
                    }
                }
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatCustomerModel = function (model)
        {
            if (model != null)
            {
                if (model.fname != undefined && model.phone != undefined && model.email != undefined)
                {
                    return model.fname + '(' + model.phone + ',' + model.email + ')';
                } else if (model.fname != undefined && model.phone != undefined)
                {
                    return model.fname + '(' + model.phone + ')';
                }
            }
            return  '';
        };
        $scope.updateAddress = function (type, model)
        {
            if ($scope.orderListModel.customerInfo.shopping_city != "" && $scope.orderListModel.customerInfo.shopping_city != null)
            {
                if ($scope.orderListModel.customerInfo.shopping_address != "")
                {
                    $scope.orderListModel.customerInfo.shopping_address = $scope.orderListModel.customerInfo.shopping_address + "," + $scope.orderListModel.customerInfo.shopping_city;
                } else
                {
                    $scope.orderListModel.customerInfo.shopping_address = $scope.orderListModel.customerInfo.shopping_city;
                }
            }
            if ($scope.orderListModel.customerInfo.shopping_state != "" && $scope.orderListModel.customerInfo.shopping_state != null)
            {
                if ($scope.orderListModel.customerInfo.shopping_address != "")
                {
                    $scope.orderListModel.customerInfo.shopping_address = $scope.orderListModel.customerInfo.shopping_address + "," + $scope.orderListModel.customerInfo.shopping_state;
                } else
                {
                    $scope.orderListModel.customerInfo.shopping_address = $scope.orderListModel.customerInfo.shopping_state;
                }
            }
            if ($scope.orderListModel.customerInfo.shopping_country != "" && $scope.orderListModel.customerInfo.shopping_country != null)
            {
                if ($scope.orderListModel.customerInfo.shopping_address != "")
                {
                    $scope.orderListModel.customerInfo.shopping_address = $scope.orderListModel.customerInfo.shopping_address + "," + $scope.orderListModel.customerInfo.shopping_country;
                } else
                {
                    $scope.orderListModel.customerInfo.shopping_address = $scope.orderListModel.customerInfo.shopping_country;
                }
            }
            if (type == "customer_dropdown")
            {
                $scope.checkOldData("customer_dropdown", model)
            }
        }
        $scope.checkOldData = function (type, model, index)
        {
            var found = false;
            if (type == "customer_dropdown")
            {
                $rootScope.newData = $("form").serializeArray();
                if ($rootScope.newData.length == $rootScope.oldData.length)
                {
                    for (var i = 0; i < $rootScope.newData.length; i++)
                    {
                        if ($rootScope.newData[i].name == "customer_dropdown")
                        {
                            if (model.fname != undefined && model.phone != undefined && model.email != undefined)
                            {
                                $rootScope.newData[i].value = model.fname + '(' + model.phone + ',' + model.email + ')';
                            } else if (model.fname != undefined && model.phone != undefined)
                            {
                                $rootScope.newData[i].value = model.fname + '(' + model.phone + ')';
                            }
                            if (String($rootScope.newData[i].value) != String($rootScope.oldData[i].value))
                            {
                                found = true;
                            }
                        }
                    }
                    if (found == true)
                    {
                        var element = document.getElementById("btnLoad");
                        element.classList.remove("disabled");
                    } else
                    {
                        var element = document.getElementById("btnLoad");
                        element.classList.add("disabled");
                    }
                }
            } else if (type == "startingdate")
            {
                $rootScope.newData = $("form").serializeArray();
                for (var i = 0; i < $rootScope.newData.length; i++)
                {
                    if ($rootScope.newData[i].name == "startingdate")
                    {
                        $rootScope.newData[i].value = utilityService.parseDateToStr(model, $scope.adminService.appConfig.date_format);
                        ;
                        if (String($rootScope.newData[i].value) != String($rootScope.oldData[i].value))
                        {
                            found = true;
                        }
                    }
                }
                if (found == true)
                {
                    var element = document.getElementById("btnLoad");
                    element.classList.remove("disabled");
                } else
                {
                    var element = document.getElementById("btnLoad");
                    element.classList.add("disabled");
                }
            } else if (type == "duedate")
            {
                $rootScope.newData = $("form").serializeArray();
                for (var i = 0; i < $rootScope.newData.length; i++)
                {
                    if ($rootScope.newData[i].name == "duedate")
                    {
                        $rootScope.newData[i].value = utilityService.parseDateToStr(model, $scope.adminService.appConfig.date_format);
                        ;
                        if (String($rootScope.newData[i].value) != String($rootScope.oldData[i].value))
                        {
                            found = true;
                        }
                    }
                }
                if (found == true)
                {
                    var element = document.getElementById("btnLoad");
                    element.classList.remove("disabled");
                } else
                {
                    var element = document.getElementById("btnLoad");
                    element.classList.add("disabled");
                }
            } else if (type == "product_dropdown")
            {
                $rootScope.newData = $("form").serializeArray();
                if ($rootScope.newData.length == $rootScope.oldData.length)
                {
                    for (var i = 0; i < $rootScope.newData.length; i++)
                    {
                        if ($rootScope.newData[i].name == "product_dropdown_" + index)
                        {
                            $rootScope.newData[i].value = model.name;

                            if (String($rootScope.newData[i].value) != String($rootScope.oldData[i].value))
                            {
                                found = true;
                            }
                        }
                    }
                    if (found == true)
                    {
                        var element = document.getElementById("btnLoad");
                        element.classList.remove("disabled");
                    } else
                    {
                        var element = document.getElementById("btnLoad");
                        element.classList.add("disabled");
                    }
                }
            } else if (type == "qty")
            {
                $rootScope.newData = $("form").serializeArray();
                if ($rootScope.newData.length == $rootScope.oldData.length)
                {
                    for (var i = 0; i < $rootScope.newData.length; i++)
                    {
                        if ($rootScope.newData[i].name == "qty_" + index)
                        {
                            if (parseFloat($rootScope.newData[i].value) != parseFloat($rootScope.oldData[i].value))
                            {
                                found = true;
                            }
                        }
                    }
                    if (found == true)
                    {
                        var element = document.getElementById("btnLoad");
                        element.classList.remove("disabled");
                    } else
                    {
                        var element = document.getElementById("btnLoad");
                        element.classList.add("disabled");
                    }
                }
            } else if (type == "purchase_price")
            {
                $rootScope.newData = $("form").serializeArray();
                if ($rootScope.newData.length == $rootScope.oldData.length)
                {
                    for (var i = 0; i < $rootScope.newData.length; i++)
                    {
                        if ($rootScope.newData[i].name == "purchase_price_" + index)
                        {
                            if (parseFloat($rootScope.newData[i].value) != parseFloat($rootScope.oldData[i].value))
                            {
                                found = true;
                            }
                        }
                    }
                    if (found == true)
                    {
                        var element = document.getElementById("btnLoad");
                        element.classList.remove("disabled");
                    } else
                    {
                        var element = document.getElementById("btnLoad");
                        element.classList.add("disabled");
                    }
                }
            } else if (type == "taxDropDown")
            {
                $rootScope.newData = $("form").serializeArray();
                if ($rootScope.newData.length == $rootScope.oldData.length)
                {
                    for (var i = 0; i < $rootScope.newData.length; i++)
                    {
                        if ($rootScope.newData[i].name == "taxDropDown_" + index)
                        {
                            if (String($rootScope.newData[i].value) != String($rootScope.oldData[i].value))
                            {
                                found = true;
                            }
                        }
                    }
                    if (found == true)
                    {
                        var element = document.getElementById("btnLoad");
                        element.classList.remove("disabled");
                    } else
                    {
                        var element = document.getElementById("btnLoad");
                        element.classList.add("disabled");
                    }
                }
            } else if (type == "mrp")
            {
                $rootScope.newData = $("form").serializeArray();
                if ($rootScope.newData.length == $rootScope.oldData.length)
                {
                    for (var i = 0; i < $rootScope.newData.length; i++)
                    {
                        if ($rootScope.newData[i].name == "mrp_" + index)
                        {
                            if (parseFloat($rootScope.newData[i].value) != parseFloat($rootScope.oldData[i].value))
                            {
                                found = true;
                            }
                        }
                    }
                    if (found == true)
                    {
                        var element = document.getElementById("btnLoad");
                        element.classList.remove("disabled");
                    } else
                    {
                        var element = document.getElementById("btnLoad");
                        element.classList.add("disabled");
                    }
                }
            } else if (type == "sellingtaxDropDown")
            {
                $rootScope.newData = $("form").serializeArray();
                if ($rootScope.newData.length == $rootScope.oldData.length)
                {
                    for (var i = 0; i < $rootScope.newData.length; i++)
                    {
                        if ($rootScope.newData[i].name == "sellingtaxDropDown_" + index)
                        {
                            if (String($rootScope.newData[i].value) != String($rootScope.oldData[i].value))
                            {
                                found = true;
                            }
                        }
                    }
                    if (found == true)
                    {
                        var element = document.getElementById("btnLoad");
                        element.classList.remove("disabled");
                    } else
                    {
                        var element = document.getElementById("btnLoad");
                        element.classList.add("disabled");
                    }
                }
            } else if (type == "discountMode")
            {
                $rootScope.newData = $("form").serializeArray();
                if ($rootScope.newData.length == $rootScope.oldData.length)
                {
                    for (var i = 0; i < $rootScope.newData.length; i++)
                    {
                        if ($rootScope.newData[i].name == "discountMode_" + index)
                        {
                            if ($rootScope.newData[i].value != $rootScope.oldData[i].value)
                            {
                                found = true;
                            }
                        }
                    }
                    if (found == true)
                    {
                        var element = document.getElementById("btnLoad");
                        element.classList.remove("disabled");
                    } else
                    {
                        var element = document.getElementById("btnLoad");
                        element.classList.add("disabled");
                    }
                }
            } else if (type == "discountPercentage")
            {
                $rootScope.newData = $("form").serializeArray();
                if ($rootScope.newData.length == $rootScope.oldData.length)
                {
                    for (var i = 0; i < $rootScope.newData.length; i++)
                    {
                        if ($rootScope.newData[i].name == "discountPercentage" + index)
                        {
                            if (parseFloat($rootScope.newData[i].value) != parseFloat($rootScope.oldData[i].value))
                            {
                                found = true;
                            }
                        }
                    }
                    if (found == true)
                    {
                        var element = document.getElementById("btnLoad");
                        element.classList.remove("disabled");
                    } else
                    {
                        var element = document.getElementById("btnLoad");
                        element.classList.add("disabled");
                    }
                }
            } else if (type == "purchase_invoice_item_custom1_label_value")
            {
                $rootScope.newData = $("form").serializeArray();
                if ($rootScope.newData.length == $rootScope.oldData.length)
                {
                    for (var i = 0; i < $rootScope.newData.length; i++)
                    {
                        if ($rootScope.newData[i].name == "purchase_invoice_item_custom1_label_value_" + index)
                        {
                            if (parseFloat($rootScope.newData[i].value) != parseFloat($rootScope.oldData[i].value))
                            {
                                found = true;
                            }
                        }
                    }
                    if (found == true)
                    {
                        var element = document.getElementById("btnLoad");
                        element.classList.remove("disabled");
                    } else
                    {
                        var element = document.getElementById("btnLoad");
                        element.classList.add("disabled");
                    }
                }
            } else if (type == "purchase_invoice_item_custom2_label_value")
            {
                $rootScope.newData = $("form").serializeArray();
                if ($rootScope.newData.length == $rootScope.oldData.length)
                {
                    for (var i = 0; i < $rootScope.newData.length; i++)
                    {
                        if ($rootScope.newData[i].name == "purchase_invoice_item_custom2_label_value_" + index)
                        {
                            if (parseFloat($rootScope.newData[i].value) != parseFloat($rootScope.oldData[i].value))
                            {
                                found = true;
                            }
                        }
                    }
                    if (found == true)
                    {
                        var element = document.getElementById("btnLoad");
                        element.classList.remove("disabled");
                    } else
                    {
                        var element = document.getElementById("btnLoad");
                        element.classList.add("disabled");
                    }
                }
            } else if (type == "purchase_invoice_item_custom3_label_value")
            {
                $rootScope.newData = $("form").serializeArray();
                if ($rootScope.newData.length == $rootScope.oldData.length)
                {
                    for (var i = 0; i < $rootScope.newData.length; i++)
                    {
                        if ($rootScope.newData[i].name == "purchase_invoice_item_custom3_label_value_" + index)
                        {
                            if (parseFloat($rootScope.newData[i].value) != parseFloat($rootScope.oldData[i].value))
                            {
                                found = true;
                            }
                        }
                    }
                    if (found == true)
                    {
                        var element = document.getElementById("btnLoad");
                        element.classList.remove("disabled");
                    } else
                    {
                        var element = document.getElementById("btnLoad");
                        element.classList.add("disabled");
                    }
                }
            } else if (type == "purchase_invoice_item_custom4_label_value")
            {
                $rootScope.newData = $("form").serializeArray();
                if ($rootScope.newData.length == $rootScope.oldData.length)
                {
                    for (var i = 0; i < $rootScope.newData.length; i++)
                    {
                        if ($rootScope.newData[i].name == "purchase_invoice_item_custom4_label_value_" + index)
                        {
                            if (parseFloat($rootScope.newData[i].value) != parseFloat($rootScope.oldData[i].value))
                            {
                                found = true;
                            }
                        }
                    }
                    if (found == true)
                    {
                        var element = document.getElementById("btnLoad");
                        element.classList.remove("disabled");
                    } else
                    {
                        var element = document.getElementById("btnLoad");
                        element.classList.add("disabled");
                    }
                }
            } else if (type == "purchase_invoice_item_custom5_label_value")
            {
                $rootScope.newData = $("form").serializeArray();
                if ($rootScope.newData.length == $rootScope.oldData.length)
                {
                    for (var i = 0; i < $rootScope.newData.length; i++)
                    {
                        if ($rootScope.newData[i].name == "purchase_invoice_item_custom5_label_value_" + index)
                        {
                            if (parseFloat($rootScope.newData[i].value) != parseFloat($rootScope.oldData[i].value))
                            {
                                found = true;
                            }
                        }
                    }
                    if (found == true)
                    {
                        var element = document.getElementById("btnLoad");
                        element.classList.remove("disabled");
                    } else
                    {
                        var element = document.getElementById("btnLoad");
                        element.classList.add("disabled");
                    }
                }
            }
        }
        $scope.$watch('orderListModel.customerInfo', function (newVal, oldVal)
        {
            if (typeof newVal == 'undefined' || newVal == '' || newVal == null)
            {
                $scope.orderListModel.customerInfo = '';
            }
        });
        $scope.getproductlist = function (val) {

            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.type = "Product";
            if ($rootScope.appConfig.gst_auto_suggest == 1)
            {
                if ($scope.orderListModel.customerInfo != '' && $scope.orderListModel.customerInfo != undefined && $scope.orderListModel.customerInfo != null && $scope.orderListModel.customerInfo.id != undefined)
                {
                    autosearchParam.customer_id = $scope.orderListModel.customerInfo.id;
                } else
                {
                    sweet.show("Oops", "Please select customer", "error");
                    return;
                }
            }
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.PRODUCT_LIST_WITH_TAX, autosearchParam, false).then(function (responseData) {
                var data = responseData.data.list;
                var hits = [];
                if (data.length > 0)
                {
                    for (var i = 0; i < data.length; i++)
                    {
                        if (data[i].is_purchase == '1' || data[i].is_purchase == 1)
                        {
                            hits.push(data[i]);
                        }
                    }
                }
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.getUomList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            return $httpService.get(APP_CONST.API.UOM, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatUomModel = function (model)
        {
            if (model != null)
            {
                return model.name;
            }
            return  '';
        };
        $scope.isLoadedTax = false;
        $scope.getTaxListInfo = function ()
        {
            $scope.isLoadedTax = false;
            var getListParam = {};
            getListParam.id = "";
            getListParam.name = "";
            getListParam.start = 0;
            getListParam.limit = 0;
            getListParam.is_display = 1;
            getListParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getActiveTaxList(getListParam, configOption).then(function (response)
            {
                var data = response.data.list;
                $scope.orderListModel.taxList = data;
                $scope.isLoadedTax = true;
            });
        };
        $scope.getPricebasedTaxInfo = function (index)
        {
            var getListParam = {};
            getListParam.hsn_code = $scope.orderListModel.list[index].hsncode;
            getListParam.purchase_price = $scope.orderListModel.list[index].purchase_price;
            if ($scope.orderListModel.list[index].discount_value != '' && $scope.orderListModel.list[index].discount_value != null)
            {
                if ($scope.orderListModel.list[index].discount_mode == '%')
                {
                    $scope.orderListModel.list[index].discount_amount = $scope.orderListModel.list[index].purchase_price * $scope.orderListModel.list[index].qty * ($scope.orderListModel.list[index].discount_value / 100);
                } else
                {
                    $scope.orderListModel.list[index].discount_amount = $scope.orderListModel.list[index].discount_value;
                }

                $scope.orderListModel.list[index].single_product_discount = $scope.orderListModel.list[index].discount_amount / $scope.orderListModel.list[index].qty;
                getListParam.purchase_price = parseFloat($scope.orderListModel.list[index].purchase_price - $scope.orderListModel.list[index].single_product_discount);
            }
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getPriceBasedTax(getListParam, configOption).then(function (response)
            {
                var data = response.data;
                $scope.orderListModel.pricebasedTax = data;
                $scope.orderListModel.list[index].tax_id = $scope.orderListModel.pricebasedTax.purchase_tax_id + '';
                $scope.initUpdateProductPurchasePrice();
            });
        }
        ;
        $scope.getSalesPricebasedTaxInfo = function (index)
        {
            var getListParam = {};
            getListParam.hsn_code = $scope.orderListModel.list[index].hsncode;
            getListParam.mrp_price = $scope.orderListModel.list[index].mrp_price;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getPriceBasedTax(getListParam, configOption).then(function (response)
            {
                var data = response.data;
                $scope.orderListModel.pricebasedTax = data;
                $scope.orderListModel.list[index].selling_tax_id = $scope.orderListModel.pricebasedTax.selling_tax_id + '';
                $scope.initUpdateProductPurchasePrice();
            });
        };

        $scope.getTaxGroupListInfo = function ()
        {
            $scope.isLoadedTax = false;
            var getListParam = {};
            getListParam.is_group = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getTaxList(getListParam, configOption).then(function (response)
            {
                var data = response.data.list;
                $scope.orderListModel.taxGroupList = data;
                $scope.isLoadedTax = true;
            });
        };
        $scope.totalcalculationTimeoutPromise != null
        $scope.updateInvoiceTotal = function ()
        {
            if ($scope.totalcalculationTimeoutPromise != null)
            {
                $timeout.cancel($scope.totalcalculationTimeoutPromise);
            }
            $scope.totalcalculationTimeoutPromise = $timeout($scope.calculatetotal, 300);
        }

        $scope.initUpdateDetailTimeoutPromise = null;
        $scope.initUpdateDetail = function ()
        {

            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
            }

            if ($scope.isLoadedTax && $scope.isPrefixListLoaded)
            {
                $scope.updateInvoiceDetails();
            } else
            {
                $scope.initUpdateDetailTimeoutPromise = $timeout($scope.initUpdateDetail, 300);
            }
        }

        $scope.updateInvoiceDetails = function ()
        {
            $scope.orderListModel.id = $scope.orderListModel.invoiceDetail.id;
            $scope.orderListModel.customerInfo = {};
            $scope.orderListModel.customerInfo.id = $scope.orderListModel.invoiceDetail.customer_id;
            $scope.orderListModel.customerInfo.fname = $scope.orderListModel.invoiceDetail.customer_fname;
            $scope.orderListModel.customerInfo.shopping_address = $scope.orderListModel.invoiceDetail.customer_address;
            $scope.orderListModel.customerInfo.phone = $scope.orderListModel.invoiceDetail.phone;
            $scope.orderListModel.customerInfo.email = $scope.orderListModel.invoiceDetail.email;
            $scope.orderListModel.subtotal = $scope.orderListModel.invoiceDetail.subtotal;
            $scope.orderListModel.taxAmt = $scope.orderListModel.invoiceDetail.tax_amount;
            $scope.orderListModel.taxAmountDetail.tax_amount = $scope.orderListModel.invoiceDetail.tax_amount;
            $scope.orderListModel.discountMode = $scope.orderListModel.invoiceDetail.discount_mode;
            $scope.orderListModel.discount_value = $scope.orderListModel.invoiceDetail.discount_percentage;
            $scope.orderListModel.discountAmt = $scope.orderListModel.invoiceDetail.discount_amount;
            $scope.orderListModel.total = $scope.orderListModel.invoiceDetail.total_amount;
            $scope.orderListModel.paid_amount = $scope.orderListModel.invoiceDetail.paid_amount;
            $scope.orderListModel.balance_amount = (parseFloat($scope.orderListModel.total - $scope.orderListModel.paid_amount)).toFixed(2);
            $scope.orderListModel.invoicePrefix = $scope.orderListModel.invoiceDetail.prefix;
            var date = utilityService.parseStrToDate($scope.orderListModel.invoiceDetail.date);
            $scope.orderListModel.billdate = date;
            var date = utilityService.parseStrToDate($scope.orderListModel.invoiceDetail.duedate);
//            $scope.orderListModel.duedate = $filter('date')(date, $rootScope.appConfig.date_format);
            $scope.orderListModel.duedate = date;
            $scope.orderListModel.purchase_code = $scope.orderListModel.invoiceDetail.purchase_code;
            $scope.orderListModel.purchase_no = $scope.orderListModel.invoiceDetail.purchase_no;
            $scope.orderListModel.reference_no = $scope.orderListModel.invoiceDetail.reference_no;
            $scope.customAttributeList = $scope.orderListModel.invoiceDetail.customattribute;
            for (var i = 0; i < $scope.customAttributeList.length; i++)
            {
                if ($scope.customAttributeList[i].input_type == 'date' && $scope.customAttributeList[i].value != '' && $scope.customAttributeList[i].value != null)
                {
                    var customdate = utilityService.parseStrToDate($scope.customAttributeList[i].value);
                    $scope.customAttributeList[i].value = $filter('date')(customdate, $rootScope.appConfig.date_format);
                }
                if ($scope.customAttributeList[i].input_type == "YesOrNo")
                {
                    if ($scope.customAttributeList[i].value != undefined)
                    {
                        if ($scope.customAttributeList[i].value.toLowerCase() == "yes")
                        {
                            $scope.customAttributeList[i].value = true;
                        } else
                        {
                            $scope.customAttributeList[i].value = false;
                        }
                    }
                }
            }

            for (var loop = 0; loop < $scope.orderListModel.invoiceDetail.item.length; loop++)
            {
                var newRow =
                        {
                            "id": $scope.orderListModel.invoiceDetail.item[loop].product_id,
                            "item_id": $scope.orderListModel.invoiceDetail.item[loop].id,
                            "productname": $scope.orderListModel.invoiceDetail.item[loop].product_name,
                            "sku": $scope.orderListModel.invoiceDetail.item[loop].product_sku,
                            "sales_price": $scope.orderListModel.invoiceDetail.item[loop].selling_price,
                            "qty": parseFloat($scope.orderListModel.invoiceDetail.item[loop].qty).toFixed($scope.quantityFormat),
                            "rowtotal": $scope.orderListModel.invoiceDetail.item[loop].total_price,
                            "uom": $scope.orderListModel.invoiceDetail.item[loop].uom_name,
                            "uom_id": $scope.orderListModel.invoiceDetail.item[loop].uom_id,
                            "custom_opt1": $scope.orderListModel.invoiceDetail.item[loop].custom_opt1,
                            "custom_opt2": $scope.orderListModel.invoiceDetail.item[loop].custom_opt2,
                            "custom_opt3": $scope.orderListModel.invoiceDetail.item[loop].custom_opt3,
                            "custom_opt4": $scope.orderListModel.invoiceDetail.item[loop].custom_opt4,
                            "custom_opt5": $scope.orderListModel.invoiceDetail.item[loop].custom_opt5,
                            "custom_opt1_key": $scope.orderListModel.invoiceDetail.item[loop].custom_opt1_key,
                            "custom_opt2_key": $scope.orderListModel.invoiceDetail.item[loop].custom_opt2_key,
                            "custom_opt3_key": $scope.orderListModel.invoiceDetail.item[loop].custom_opt3_key,
                            "custom_opt4_key": $scope.orderListModel.invoiceDetail.item[loop].custom_opt4_key,
                            "custom_opt5_key": $scope.orderListModel.invoiceDetail.item[loop].custom_opt5_key,
                            "mrp_price": $scope.orderListModel.invoiceDetail.item[loop].mrp_price,
                            "purchase_price": $scope.orderListModel.invoiceDetail.item[loop].purchase_price,
                            "purchase_price_with_tax": $scope.orderListModel.invoiceDetail.item[loop].purchase_price_w_tax,
                            "selling_tax_id": '-1',
                            "selected_selling_tax_id": $scope.orderListModel.invoiceDetail.item[loop].selling_tax_id,
                            "selling_tax_percentage": $scope.orderListModel.invoiceDetail.item[loop].selling_tax_percentage,
                            "tax_id": '-1',
                            "selected_taxId": $scope.orderListModel.invoiceDetail.item[loop].tax_id,
                            "selected_discount_mode": $scope.orderListModel.invoiceDetail.item[loop].discount_mode,
                            "tax_percentage": $scope.orderListModel.invoiceDetail.item[loop].tax_percentage,
                            "tax_amount": parseFloat($scope.orderListModel.invoiceDetail.item[loop].tax_amount),
                            "discount_mode": $scope.orderListModel.invoiceDetail.item[loop].discount_mode,
                            "discount_value": $scope.orderListModel.invoiceDetail.item[loop].discount_value,
                            "discount_amount": $scope.orderListModel.invoiceDetail.item[loop].discount_amount,
                            "is_new": $scope.orderListModel.invoiceDetail.item[loop].is_new,
                            "hsncode": $scope.orderListModel.invoiceDetail.item[loop].hsn_code,
                            "is_barcode_generated": $scope.orderListModel.invoiceDetail.item[loop].is_barcode_generated,
                            "discount_amount_display": parseFloat($scope.orderListModel.invoiceDetail.item[loop].discount_amount).toFixed(2)
                        };
                newRow.productInfo = {
                    "id": $scope.orderListModel.invoiceDetail.item[loop].product_id,
                    "name": $scope.orderListModel.invoiceDetail.item[loop].product_name,
                    "sku": $scope.orderListModel.invoiceDetail.item[loop].product_sku,
                    "code": $scope.orderListModel.invoiceDetail.item[loop].code,
                    "tax_id": $scope.orderListModel.invoiceDetail.item[loop].tax_id,
                    "hsncode": $scope.orderListModel.invoiceDetail.item[loop].hsn_code,
                    "init_update": true
                };

                newRow.uomInfo = {
                    "name": $scope.orderListModel.invoiceDetail.item[loop].uom_name,
                    "id": $scope.orderListModel.invoiceDetail.item[loop].uom_id
                };
                newRow.productTaxMapping = [];
                for (var t = 0; t < $scope.orderListModel.taxGroupList.length; t++)
                {
                    newRow.productTaxMapping.push($scope.orderListModel.taxGroupList[t]);
                }


                $scope.orderListModel.list.push(newRow);
                $scope.orderListModel.list[loop].productname = $scope.orderListModel.list[loop];
            }
            $scope.addNewProduct();
            $scope.orderListModel.isLoadingProgress = false;
            $scope.calculatetotal();
        }

        $scope.isPrefixListLoaded = false;
        $scope.getPrefixList = function ()
        {
            $scope.defaultprefix = false;
            var getListParam = {};
            getListParam.setting = 'purchase_invoice_prefix';
            var headers = {};
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getAppSettingsList(getListParam, configOption, headers).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    if (data.list.length > 0)
                    {
                        var perfix = data.list[0].value;
                        perfix = perfix.split('\n');
                        for (var i = 0; i < perfix.length; i++)
                        {
                            if (perfix[i].split('-')[1] == 'default')
                            {
                                perfix[i] = perfix[i].split('-')[0];
                                $scope.orderListModel.invoicePrefix = perfix[i] + '';
                                $scope.defaultprefix = true;
                            }
                            if ($scope.defaultprefix == false)
                            {
                                $scope.orderListModel.invoicePrefix = '';
                            }
                            $scope.orderListModel.prefixList.push(perfix[i]);
                        }
                    }
                    $scope.isPrefixListLoaded = true;
                }


            });
        };

        $scope.isNextAutoGeneratorNumberLoaded = false;
        $scope.getNextInvoiceno = function ()
        {
            $scope.isNextAutoGeneratorNumberLoaded = true;
            var getListParam = {};
            getListParam.type = "purchase_invoice";
            getListParam.prefix = $scope.orderListModel.invoicePrefix;
            var headers = {};
            var configOption = adminService.handleOnlyErrorResponseConfig;
            configOption.perfix = getListParam.prefix;
            adminService.getNextInvoiceNo(getListParam, configOption, headers).then(function (response)
            {
                var config = response.config.config;
                if (response.data.success === true && typeof config.perfix !== 'undefined' && config.perfix == $scope.orderListModel.invoicePrefix)
                {
                    if (!$scope.orderListModel.is_next_autogenerator_num_changed)
                    {
                        $scope.orderListModel.purchase_no = response.data.no;
                    }
                }
                $scope.isNextAutoGeneratorNumberLoaded = false;
            });
        };
        $scope.formatProductModel = function (model, index)
        {
            if (model != null && model != "")
            {
                if (model.init_update != undefined && model.init_update == true)
                {
                    for (var i = 0; i < $scope.orderListModel.taxGroupList.length; i++)
                    {
                        if ($scope.orderListModel.taxGroupList[i].id == model.tax_id)
                        {
                            $scope.orderListModel.list[index].tax_name = $scope.orderListModel.taxGroupList[i].tax_name + '-' + $scope.orderListModel.taxGroupList[i].tax_percentage;
                        }
                    }
                    $timeout(function () {

                        if ($scope.orderListModel.list[index].selected_taxId == 0)
                        {
                            $scope.orderListModel.list[index].selected_taxId = '';
                        }
                        $scope.orderListModel.list[index].tax_id = $scope.orderListModel.list[index].selected_taxId + '';
                        if ($scope.orderListModel.list[index].selected_discount_mode == '')
                        {
                            $scope.orderListModel.list[index].discount_mode = '%';
                            $scope.orderListModel.list[index].discount_value = '';
                        }
                        if ($scope.orderListModel.list[index].selected_selling_tax_id == 0)
                        {
                            $scope.orderListModel.list[index].selected_selling_tax_id = '';
                        }
                        $scope.orderListModel.list[index].selling_tax_id = $scope.orderListModel.list[index].selected_selling_tax_id + '';
                        if (model.taxMapping != 'undefined' && model.taxMapping != null && model.taxMapping != '')
                        {
                            $scope.orderListModel.list[index].tax_id = model.taxMapping[0].tax_id + '';
                            $scope.orderListModel.list[index].taxPercentage = model.taxMapping[0].tax_percentage;
                            $scope.orderListModel.list[index].tax_name = model.taxMapping[0].tax_name + '-' + model.taxMapping[0].tax_percentage;
                        }

                    }, 300);
                    return model.name;
                }
                $scope.updateProductInfo(model, index);
                $timeout(function () {
                    for (var i = 0; i < model.taxMapping.length; i++)
                    {
                        if (model.taxMapping[i].id != 0)
                        {
//                            $scope.orderListModel.list[index].discount_mode = '%';
//                            $scope.orderListModel.list[index].discount_value = $scope.orderListModel.discount_value + '';
                            $scope.orderListModel.list[index].tax_id = model.taxMapping[i].tax_id + '';
                            $scope.orderListModel.list[index].tax_percentage = model.taxMapping[i].tax_percentage + '';
                            $scope.orderListModel.list[index].selling_tax_id = model.taxMapping[i].tax_id + '';
                            $scope.orderListModel.list[index].selling_tax_percentage = model.taxMapping[i].tax_percentage + '';
                            break;
                        }
                    }
                    $scope.updateProductPurchasePrice(index);
                }, 300);
                return model.name;
            }
            return  '';
        }

        $scope.updateProductInfo = function (model, index)
        {
            if ($scope.orderListModel.list.length != 0)
            {
                if (typeof model == 'undefined')
                    return;
                $scope.orderListModel.list[index].id = model.id;
                $scope.orderListModel.list[index].sku = model.sku;
                $scope.orderListModel.list[index].sales_price = model.sales_price;
                $scope.orderListModel.list[index].qty = 1;
                $scope.orderListModel.list[index].rowtotal = model.sales_price;
                $scope.orderListModel.list[index].hsncode = model.hsn_code;
                $scope.orderListModel.list[index].discount_value = $scope.orderListModel.list[0].discount_value;
                $scope.orderListModel.list[index].discountPercentage = $scope.orderListModel.list[0].discount_value;
                $scope.orderListModel.list[index].discount_mode = $scope.orderListModel.list[0].discount_mode;
                if (model.taxMapping != undefined && model.taxMapping.length > 0)
                {
                    var found = false;
                    var findIndex = 0;
                    for (var i = 0; i < model.taxMapping.length; i++)
                    {
                        if (model.taxMapping[i].product_id == model.id)
                        {
                            found = true;
                            findIndex = i;
                        }
                    }
                    if (found == true)
                    {
                        $scope.orderListModel.list[index].tax_id = model.taxMapping[findIndex].tax_id + '';
                        $scope.orderListModel.list[index].taxPercentage = model.taxMapping[findIndex].tax_percentage;
                        $scope.orderListModel.list[index].tax_name = model.taxMapping[findIndex].tax_name + '-' + model.taxMapping[findIndex].tax_percentage;
                    } else
                    {
                        $scope.orderListModel.list[index].tax_id = '';
                        $scope.orderListModel.list[index].tax_name = '';
                        $scope.orderListModel.list[index].taxPercentage = '';
                    }

                } else
                {
                    $scope.orderListModel.list[index].tax_id = '';
                    $scope.orderListModel.list[index].tax_name = '';
                }
                $scope.orderListModel.list[index].taxMapping = model.taxMapping;
                $scope.orderListModel.list[index].selling_tax_id = "";
                $scope.orderListModel.list[index].mrp_price = model.sales_price;
                $scope.orderListModel.list[index].purchase_price = model.sales_price;
                $scope.orderListModel.list[index].purchase_price_with_tax = model.sales_price;
                $scope.orderListModel.list[index].uomInfo = {
                    "name": model.uom,
                    "id": model.uom_id
                }
                if (model.uomid != undefined)
                {
                    $scope.orderListModel.list[index].uomid = model.uomid;
                }
                if (model.uom_id != undefined)
                {
                    $scope.orderListModel.list[index].uomid = model.uom_id;
                }
                if (model.uomid != undefined && model.uom_id != undefined)
                {
                    $scope.orderListModel.list[index].uomid = "";
                }

//                 newRow.productTaxMapping = [];
//                    for (var t = 0; t < $scope.orderListModel.taxList.length; t++)
//                    {
//                        newRow.productTaxMapping.push($scope.orderListModel.taxList[t]);
//                    }
//
//                    $scope.orderListModel.list.push(newRow);

                $scope.updateInvoiceTotal();
                console.log("Index for item edit ", index);
                console.log("List Length ", $scope.orderListModel.list.length);
                $scope.createNewProduct(index);
                return;
            } else
            {
                $scope.createNewProduct(-1);
            }
        }
        $scope.initvalidateProductDataAndAddNewPromise = null;
        $scope.initValidateProductDataAndAddNew = function (index)
        {

            if ($scope.initvalidateProductDataAndAddNewPromise != null)
            {

                $timeout.cancel($scope.initvalidateProductDataAndAddNewPromise);
            }

            $scope.formValidator();
            $scope.initvalidateProductDataAndAddNewPromise = $timeout(
                    function () {
                        $scope.createNewProduct(index);
                    },
                    300);
        };
        $scope.formValidator = function ()
        {
            //performace issue in form validator bug
            if ($scope.purchase_order_edit_form.$submitted)
            {
                $timeout(function () {
                    $scope.purchase_order_edit_form.$submitted = false;
                }, 0);
                $timeout(function () {
                    $scope.purchase_order_edit_form.$setSubmitted();
                }, 100);
            } else
            {
                $timeout(function () {
                    $scope.purchase_order_edit_form.$setSubmitted();
                }, 100);
            }
        }

        $scope.createNewProduct = function (index)
        {
            var formDataError = false;
            if (index != -1)
            {
                var productContainer = window.document.getElementById('purchaseinvoice_edit_product_container');
                var errorCell = angular.element(productContainer).find('.has-error').length;
                if (errorCell > 0)
                {
                    formDataError = true;
                }
            }
            if (!formDataError)
            {
                if (index == $scope.orderListModel.list.length - 1 && !$scope.deleteEvent)
                {

                    var newRow = {
                        "id": '',
                        "productInfo": '',
                        "productName": '',
                        "sku": '',
                        "sales_price": '',
                        "qty": '',
                        "rowtotal": '',
                        "discountPercentage": '',
                        "uom": '',
                        "uom_id": '',
                        "custom_opt1": '',
                        "custom_opt2": '',
                        "custom_opt3": '',
                        "custom_opt4": '',
                        "custom_opt5": '',
                        "mrp_price": '',
                        "purchase_price": '',
                        "purchase_price_with_tax": '',
                        "tax_id": '',
                        "taxPercentage": '',
                        "selling_tax_id": '',
                        "selling_tax_percentage": '',
                        "discount_mode": '%',
                        "discount_value": '',
                        "discount_amount": 0,
                        "discount_amount_display": "0.00",
                    }

                    newRow.productTaxMapping = [];
                    for (var t = 0; t < $scope.orderListModel.taxGroupList.length; t++)
                    {
                        newRow.productTaxMapping.push($scope.orderListModel.taxGroupList[t]);
                    }

                    $scope.orderListModel.list.push(newRow);
                }
                $scope.deleteEvent = false;
            }

        }
        $scope.addNewProduct = function ()
        {
            var newRow = {
                "id": '',
                "productInfo": '',
                "productName": '',
                "sku": '',
                "sales_price": '',
                "qty": '',
                "rowtotal": '',
                "discountPercentage": '',
                "uom": '',
                "uom_id": '',
                "custom_opt1": '',
                "custom_opt2": '',
                "custom_opt3": '',
                "custom_opt4": '',
                "custom_opt5": '',
                "mrp_price": '',
                "purchase_price": '',
                "purchase_price_with_tax": '',
                "tax_id": '',
                "taxPercentage": '',
                "selling_tax_id": '',
                "selling_tax_percentage": '',
                "discount_mode": '%',
                "discount_value": '',
                "discount_amount": 0,
                "discount_amount_display": "0.00",
                "is_barcode_generated": ''
            }

            newRow.productTaxMapping = [];
            for (var t = 0; t < $scope.orderListModel.taxGroupList.length; t++)
            {
                newRow.productTaxMapping.push($scope.orderListModel.taxGroupList[t]);
            }
            $scope.orderListModel.list.push(newRow);
        }
        $scope.deleteEvent = false;
        $scope.initUpdateProductPurchasePriceTimeoutPromise != null
        $scope.initUpdateProductPurchasePrice = function (type, model, index)
        {
            if ($scope.initUpdateProductPurchasePriceTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateProductPurchasePriceTimeoutPromise);
            }
            $scope.initUpdateProductPurchasePriceTimeoutPromise = $timeout(function () {
                $scope.updateProductPurchasePrice();
                if (typeof index != "undefined")
                {
                    if (type == "qty")
                    {
                        $scope.checkOldData('qty', model, index)
                    }
                    if (type == "purchase")
                    {
                        $scope.checkOldData('purchase_price', model, index)
                    }
                    if (type == "mrp")
                    {
                        $scope.checkOldData('mrp', model, index)
                    }
                    if (type == "sellingtaxDropDown")
                    {
                        $scope.checkOldData('sellingtaxDropDown', model, index)
                    }
                    if (type == "discountMode")
                    {
                        $scope.checkOldData('discountMode', model, index)
                    }
                    if (type == "discountPercentage")
                    {
                        $scope.checkOldData('discountPercentage', model, index)
                    }
                }
            }, 300);
        };
        $scope.initUpdateProductDiscountPriceTimeoutPromise != null
        $scope.initUpdateProductDiscountPrice = function ()
        {
            if ($scope.initUpdateProductDiscountPriceTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateProductDiscountPriceTimeoutPromise);
            }
            $scope.initUpdateProductDiscountPriceTimeoutPromise = $timeout(function () {
                $scope.updateProductDiscountPrice();
            }, 300);
        }
        $scope.updateProductDiscountPrice = function ()
        {
            for (var i = 0; i < $scope.orderListModel.list.length; i++)
            {
                var qty = parseFloat($scope.orderListModel.list[i].qty);
                //var mrp_price = parseFloat($scope.orderListModel.list[index].mrp_price);
                var purchase_price = parseFloat($scope.orderListModel.list[i].purchase_price);
                var discount_mode = $scope.orderListModel.discountMode;
                var discount_value = parseFloat($scope.orderListModel.discount_value);
                var discount_amount = 0;
                var discount_amount_display = "0.00";
                if (discount_value == '' || isNaN(discount_value))
                {
                    discount_value = 0;
                }

                if (discount_mode == '%')
                {
                    discount_amount = purchase_price * qty * (discount_value / 100);
                } else
                {
                    discount_amount = discount_value;
                }
                $scope.orderListModel.list[i].discount_mode = discount_mode;
                $scope.orderListModel.list[i].discount_value = discount_value;
                $scope.orderListModel.list[i].discount_amount = discount_amount;
                $scope.orderListModel.list[i].discount_amount_display = parseFloat(discount_amount).toFixed(2);
            }
            $scope.updateProductPurchasePrice();
            $scope.calculatetotal();
        }

        $scope.updateProductPurchasePrice = function ()
        {

//            if (index < $scope.orderListModel.list.length)
//            {
            for (var i = 0; i < $scope.orderListModel.list.length; i++)
            {
                for (var j = 0; j < $scope.orderListModel.list[i].productTaxMapping.length; j++)
                {
                    var taxObj = $scope.orderListModel.list[i].productTaxMapping[j];
                    if ($scope.orderListModel.list[i].tax_id == taxObj.id)
                    {
                        $scope.orderListModel.list[i].tax_percentage = taxObj.tax_percentage;
                        break;
                    }
                }
            }
            for (var i = 0; i < $scope.orderListModel.list.length; i++)
            {
                for (var j = 0; j < $scope.orderListModel.list[i].productTaxMapping.length; j++)
                {
                    var taxObj = $scope.orderListModel.list[i].productTaxMapping[j];
                    if ($scope.orderListModel.list[i].selling_tax_id == taxObj.id)
                    {
                        $scope.orderListModel.list[i].selling_tax_percentage = taxObj.tax_percentage;
                        break;
                    }
                }
            }
            for (var i = 0; i < $scope.orderListModel.list.length; i++)
            {
                if ($scope.orderListModel.list[i].tax_id == '')
                {
                    $scope.orderListModel.list[i].tax_percentage = 0;
                }
                if ($scope.orderListModel.list[i].selling_tax_id == '')
                {
                    $scope.orderListModel.list[i].selling_tax_percentage = 0;
                }
                var qty = parseFloat($scope.orderListModel.list[i].qty);
                if ($scope.orderListModel.list[i].mrp_price != null && $scope.orderListModel.list[i].mrp_price != '')
                {
                    var mrp_price = parseFloat($scope.orderListModel.list[i].mrp_price);
                } else {
                    var mrp_price = 0;
                }
//                var mrp_price = parseFloat($scope.orderListModel.list[i].mrp_price);
                var purchase_price = parseFloat($scope.orderListModel.list[i].purchase_price);
                var productTaxPercentage = parseFloat($scope.orderListModel.list[i].tax_percentage);
                var productSellingTaxPercentage = parseFloat($scope.orderListModel.list[i].selling_tax_percentage);
                var discount_mode = $scope.orderListModel.list[i].discount_mode;
                var discount_value = parseFloat($scope.orderListModel.list[i].discount_value);
                var discount_amount = 0;
                var discount_amount_display = "0.00";
                if (discount_value == '' || isNaN(discount_value))
                {
                    discount_value = 0;
                }

                if (!isNaN(qty) && !isNaN(purchase_price) && !isNaN(mrp_price) && !isNaN(productTaxPercentage) && !isNaN(productSellingTaxPercentage))
                {
                    if (discount_mode == '%')
                    {
                        discount_amount = purchase_price * qty * (discount_value / 100);
                    } else
                    {
                        discount_amount = discount_value;
                    }
                    $scope.orderListModel.list[i].single_product_discount = discount_amount / qty;
//                    if (discount_mode == '%')
//                    {
//                        $scope.orderListModel.list[i].single_product_discount = discount_amount / qty;
//                    }

                    $scope.orderListModel.list[i].single_product_discount = parseFloat($scope.orderListModel.list[i].single_product_discount).toFixed();
                    $scope.orderListModel.list[i].discount_amount = discount_amount;
                    $scope.orderListModel.list[i].discount_amount_display = parseFloat(discount_amount).toFixed(2);
                    var purchase_price_tax_incl_discount = (purchase_price * 1) - parseFloat($scope.orderListModel.list[i].single_product_discount);
//                    var purchase_price_tax_incl_discount = parseFloat(purchase_price * 1);
                    var singleProducttax_amount = parseFloat(purchase_price_tax_incl_discount * productTaxPercentage / 100).toFixed(2);
                    $scope.orderListModel.list[i].purchase_price_with_tax = parseFloat(purchase_price_tax_incl_discount) + parseFloat(singleProducttax_amount);
                    $scope.orderListModel.list[i].purchase_price_with_tax = parseFloat($scope.orderListModel.list[i].purchase_price_with_tax).toFixed(2);
                    var row_total_with_tax_incl_discount = (purchase_price * qty) - discount_amount;
//                    var row_total_with_tax_incl_discount = (purchase_price * qty);
                    $scope.orderListModel.list[i].tax_amount = parseFloat(row_total_with_tax_incl_discount * productTaxPercentage / 100).toFixed(2);
//                    $scope.orderListModel.list[index].row_total_with_tax = parseFloat(row_total_with_tax_incl_discount) + parseFloat($scope.orderListModel.list[index].tax_amount);
                    $scope.orderListModel.list[i].row_total_with_tax = parseFloat($scope.orderListModel.list[i].tax_amount);
                    $scope.orderListModel.list[i].row_total_with_tax = parseFloat($scope.orderListModel.list[i].row_total_with_tax).toFixed(2);
                    $scope.orderListModel.list[i].sales_price = parseFloat(mrp_price / (1 + productSellingTaxPercentage / 100)).toFixed(2);
                } else
                {
                    $scope.orderListModel.list[i].discount_amount = 0;
                    $scope.orderListModel.list[i].tax_amount = 0;
                    $scope.orderListModel.list[i].purchase_price_with_tax = '';
                    $scope.orderListModel.list[i].discount_amount_display = "0.00";
                }

            }
            $scope.calculatetotal();
            // }

        }
        $scope.isdeletePurchaseItem = false;
        $scope.deleteProduct = function (index, barcodegenerate, itemid)
        {
            $scope.isdeletePurchaseItem = true;
            var itemId = itemid;
            var itemIndex = index;
            if (index < $scope.orderListModel.list.length)
            {
                if (barcodegenerate != undefined && barcodegenerate != null && barcodegenerate == 1)
                {
                    $scope.deletePurchaseInvoiceItem(itemId, itemIndex);
                } else
                {
                    var element = document.getElementById("btnLoad");
                    element.classList.remove("disabled");
                    $scope.orderListModel.list.splice(index, 1);
                    $scope.calculatetotal();
                    $scope.deleteEvent = true;
                    $scope.isdeletePurchaseItem = false;
                }
            }

        };
        $scope.deletePurchaseInvoiceItem = function (itemId, itemIndex)
        {
            var getListParam = {};
            getListParam.id = itemId;
            var headers = {};
            headers['screen-code'] = 'purhaseinvoice';
            adminService.deletePurInvoiceItem(getListParam, getListParam.id, headers).then(function (response)
            {
                var data = response.data;
                if (data.success == true)
                {
                    var element = document.getElementById("btnLoad");
                    element.classList.remove("disabled");
                    $scope.orderListModel.list.splice(itemIndex, 1);
                    $scope.calculatetotal();
                    $scope.isdeletePurchaseItem = false;
                }

            });
        }

        $scope.taxList = [];
        $scope.calculatetotal = function ()
        {
            var subTotal = 0;
            var totalDiscount = 0;
            var totalTaxAmount = 0;
            var totDiscountPercentage = 0;
            var totTaxPercentage = 0;
            var totalTaxPercentage = 0;
            var totMapTax = 0;
            var totQty = 0;
            for (var i = 0; i < $scope.orderListModel.list.length; i++)
            {

                if ($scope.orderListModel.list[i].productInfo != null && $scope.orderListModel.list[i].productInfo.id != undefined &&
                        !isNaN($scope.orderListModel.list[i].qty) &&
                        !isNaN($scope.orderListModel.list[i].sales_price) &&
                        !isNaN($scope.orderListModel.list[i].purchase_price) &&
                        !isNaN($scope.orderListModel.list[i].mrp_price) &&
                        !isNaN($scope.orderListModel.list[i].purchase_price_with_tax))
                {
                    totalDiscount += parseFloat($scope.orderListModel.list[i].discount_amount);
                    totalTaxAmount += parseFloat($scope.orderListModel.list[i].tax_amount);
                    totQty += parseFloat($scope.orderListModel.list[i].qty);
                    $scope.orderListModel.list[i].rowtotal = $scope.orderListModel.list[i].purchase_price * $scope.orderListModel.list[i].qty;
                    subTotal += parseFloat($scope.orderListModel.list[i].rowtotal);
                    $scope.orderListModel.list[i].rowtotal = $scope.orderListModel.list[i].rowtotal.toFixed(2);
                }

            }
//            if ($scope.orderListModel.discountMode == 'amount')
//            {
//                totalDiscount = $scope.orderListModel.discount_value;
//            }
            $scope.orderListModel.totalQty = parseFloat(totQty).toFixed($scope.quantityFormat);
            $scope.orderListModel.taxAmt = parseFloat(totalTaxAmount).toFixed(2);
            $scope.orderListModel.subtotal = parseFloat(subTotal).toFixed(2);
            $scope.orderListModel.discountAmt = parseFloat(totalDiscount).toFixed(2);
            if ($scope.orderListModel.subtotal > 0)
            {
                $scope.orderListModel.subtotalWithDiscount = $scope.orderListModel.subtotal - totalDiscount;
                $scope.orderListModel.total = (parseFloat(subTotal) + parseFloat(totalTaxAmount) - parseFloat($scope.orderListModel.discountAmt)).toFixed(2);
            } else
            {
                $scope.orderListModel.subtotalWithDiscount = 0.00;
                $scope.orderListModel.total = 0.00;
            }
            $scope.orderListModel.balance_amount = (parseFloat($scope.orderListModel.total - $scope.orderListModel.paid_amount)).toFixed(2);
            $scope.updateRoundoff();
        }

        $scope.updateRoundoff = function ()
        {
            $scope.orderListModel.round = Math.round($scope.orderListModel.total);
            $scope.orderListModel.roundoff = $scope.orderListModel.round - parseFloat($scope.orderListModel.total);
            $scope.orderListModel.roundoff = parseFloat($scope.orderListModel.roundoff).toFixed(2);
            $scope.orderListModel.round = parseFloat($scope.orderListModel.round).toFixed(2);
        }

        $scope.$on("updateSelectedProductEvent", function (event, selectedItems)
        {
            if (typeof selectedItems == 'undefined')
                return;
            if ($scope.orderListModel.list.length != 0)
            {
                var isDuplicateProduct = false;
                var duplicateProductIndex = -1;
                for (i = 0; i < $scope.orderListModel.list.length; i++)
                {
                    if (selectedItems.id == $scope.orderListModel.list[i].id)
                    {
                        isDuplicateProduct = true;
                        duplicateProductIndex = i;
                    }
                }
                if (isDuplicateProduct)
                {
                    $scope.orderListModel.list[duplicateProductIndex].qty = $scope.orderListModel.list[duplicateProductIndex].qty + 1;
                    console.log("upd 4");
                    $scope.updateInvoiceTotal();
                } else
                {
                    var newRow =
                            {
                                "id": selectedItems.id,
                                "productName": selectedItems.name,
                                "sku": selectedItems.sku,
                                "sales_price": selectedItems.sales_price,
                                "qty": 1,
                                "rowtotal": selectedItems.sales_price,
                                "discountPercentage": selectedItems.discountPercentage,
                                "taxPercentage": selectedItems.taxPercentage,
                                "uom": selectedItems.uom,
                                "uom_id": selectedItems.uom_id
                            }
                    $scope.orderListModel.list.push(newRow);
                    console.log("upd 2");
                    $scope.updateInvoiceTotal();
                    return;
                }
            } else
            {
                var newRow = {
                    "id": selectedItems.id,
                    "productName": selectedItems.name,
                    "sku": selectedItems.sku,
                    "sales_price": selectedItems.sales_price,
                    "qty": 1,
                    "rowtotal": selectedItems.sales_price,
                    "discountPercentage": selectedItems.discountPercentage,
                    "taxPercentage": selectedItems.taxPercentage,
                    "uom": selectedItems.uom,
                    "uom_id": selectedItems.uom_id
                }
                $scope.orderListModel.list.push(newRow);
                $scope.updateInvoiceTotal();
            }
        });

        $scope.getInvoiceInfo = function ()
        {
            $scope.orderListModel.isLoadingProgress = true;
            var getListParam = {};
            getListParam.id = $stateParams.id;
            getListParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getPurchaseInvoiceDetail(getListParam, configOption).then(function (response)
            {
                var data = response.data.data;
                $scope.orderListModel.invoiceDetail = data;
                $scope.initUpdateDetail();
                $scope.getTaxListInfo();
                $scope.getTaxGroupListInfo();
            });
        }

        $scope.$on('customSelectOptionChangeEvent', function (event, data)
        {
            console.log(data);
            $scope.updateCustomSelectValue(data);
        });
        $scope.updateCustomSelectValue = function (data)
        {
            for (var i = 0; i < $scope.customAttributeList.length; i++)
            {
                if ($scope.customAttributeList[i].attribute_code == data.code)
                {
                    $scope.customAttributeList[i].value = data.value;
                    break;
                }
            }
        }

        $scope.$on('customfieldValueChangeEvent', function (event, data)
        {
            console.log(data);
            $scope.updateCustomFieldValue(data);
        });
        $scope.updateCustomFieldValue = function (data)
        {
            for (var i = 0; i < $scope.customAttributeList.length; i++)
            {
                if ($scope.customAttributeList[i].attribute_code == data.code)
                {
                    $scope.customAttributeList[i].value = data.value;
                    break;
                }
            }
        }


        $scope.getInvoiceInfo();
        $scope.updateOrder = function (forceSave)
        {
            if (!forceSave)
            {
                if ($scope.isDataSavingProcess)
                {
                    return;
                }
            }
            $scope.isDataSavingProcess = true;
            $scope.isSave = false;
            for (var i = 0; i < $scope.orderListModel.list.length; i++)
            {
                if (i != $scope.orderListModel.list.length - 1)
                {
                    if ($rootScope.appConfig.uomdisplay)
                    {


                        if ($scope.orderListModel.list[i].productInfo != '' && $scope.orderListModel.list[i].productInfo.id != undefined &&
                                $scope.orderListModel.list[i].uomInfo != '' && $scope.orderListModel.list[i].uomInfo != undefined &&
                                !isNaN($scope.orderListModel.list[i].qty) &&
                                !isNaN($scope.orderListModel.list[i].sales_price) && ($scope.orderListModel.list[i].sales_price != null) && ($scope.orderListModel.list[i].sales_price != '') &&
                                !isNaN($scope.orderListModel.list[i].purchase_price) &&
                                !isNaN($scope.orderListModel.list[i].mrp_price) && ($scope.orderListModel.list[i].mrp_price != null) && ($scope.orderListModel.list[i].mrp_price != '') &&
                                !isNaN($scope.orderListModel.list[i].purchase_price_with_tax))
                        {
                            $scope.isSave = true;
                        } else
                        {
                            $scope.isSave = false;
                            break;
                        }
                    } else
                    {
                        if ($scope.orderListModel.list[i].productInfo != '' && $scope.orderListModel.list[i].productInfo != undefined &&
                                !isNaN($scope.orderListModel.list[i].qty) && ($scope.orderListModel.list[i].qty != null) && ($scope.orderListModel.list[i].qty != '') &&
                                !isNaN($scope.orderListModel.list[i].sales_price) && ($scope.orderListModel.list[i].sales_price != null) && ($scope.orderListModel.list[i].sales_price != '') &&
                                !isNaN($scope.orderListModel.list[i].purchase_price) && ($scope.orderListModel.list[i].purchase_price != null) && ($scope.orderListModel.list[i].purchase_price != '') &&
                                !isNaN($scope.orderListModel.list[i].mrp_price) && ($scope.orderListModel.list[i].mrp_price != null) && ($scope.orderListModel.list[i].mrp_price != '') &&
                                !isNaN($scope.orderListModel.list[i].purchase_price_with_tax))
                        {
                            $scope.isSave = true;
                        } else
                        {
                            $scope.isSave = false;
                            break;
                        }
                    }
                }
                if ($scope.orderListModel.list.length == 1)
                {
                    if ($rootScope.appConfig.uomdisplay)
                    {
                        if ($scope.orderListModel.list[i].productInfo != '' && $scope.orderListModel.list[i].productInfo.id != undefined &&
                                $scope.orderListModel.list[i].uomInfo != '' && $scope.orderListModel.list[i].uomInfo != undefined &&
                                !isNaN($scope.orderListModel.list[i].qty) &&
                                !isNaN($scope.orderListModel.list[i].sales_price) && ($scope.orderListModel.list[i].sales_price != null) && ($scope.orderListModel.list[i].sales_price != '') &&
                                !isNaN($scope.orderListModel.list[i].purchase_price) &&
                                !isNaN($scope.orderListModel.list[i].mrp_price) && ($scope.orderListModel.list[i].mrp_price != null) && ($scope.orderListModel.list[i].mrp_price != '') &&
                                !isNaN($scope.orderListModel.list[i].purchase_price_with_tax))
                        {
                            $scope.isSave = true;
                        } else
                        {
                            $scope.isSave = false;
                            break;
                        }
                    } else
                    {
                        if ($scope.orderListModel.list[i].productInfo != '' && $scope.orderListModel.list[i].productInfo != undefined &&
                                !isNaN($scope.orderListModel.list[i].qty) &&
                                !isNaN($scope.orderListModel.list[i].sales_price) && ($scope.orderListModel.list[i].sales_price != null) && ($scope.orderListModel.list[i].sales_price != '') &&
                                !isNaN($scope.orderListModel.list[i].purchase_price) &&
                                !isNaN($scope.orderListModel.list[i].mrp_price) && ($scope.orderListModel.list[i].mrp_price != null) && ($scope.orderListModel.list[i].mrp_price != '') &&
                                !isNaN($scope.orderListModel.list[i].purchase_price_with_tax))
                        {
                            $scope.isSave = true;
                        } else
                        {
                            $scope.isSave = false;
                            break;
                        }
                    }
                }
            }

            if ($scope.isSave)
            {

                $scope.getReferenceList(false);
            } else
            {
                if ($scope.orderListModel.list[i].purchase_price != "" || $scope.orderListModel.list[i].sales_price == "" || $scope.orderListModel.list[i].sales_price == null || $scope.orderListModel.list[i].mrp_price == null)
                {
                    sweet.show('Oops...', 'Fill Product Details', 'error');
                    $scope.isDataSavingProcess = false;
                } else
                {
                    if ($scope.orderListModel.list[i].purchase_price == "" || $scope.orderListModel.list[i].sales_price == "" || $scope.orderListModel.list[i].sales_price == null || $scope.orderListModel.list[i].mrp_price == null)
                    {
                        sweet.show('Oops...', 'Check Price Detail', 'error');
                        $scope.isDataSavingProcess = false;
                    } else
                    {
                        sweet.show('Oops...', 'Fill Product Details', 'error');
                        $scope.isDataSavingProcess = false;
                    }
                }
                var element = document.getElementById("btnLoad");
                element.classList.remove("btn-loader");
            }

        };
        $scope.modifyOrder = function ()
        {
            var updateOrderParam = {};
            var headers = {};
            headers['screen-code'] = 'purchaseinvoice';
            //updateOrderParam.status = 'unpaid';                
            updateOrderParam.prefix = $scope.orderListModel.invoicePrefix;
            updateOrderParam.purchase_no = $scope.orderListModel.purchase_no;
            updateOrderParam.reference_no = $scope.orderListModel.invoice_no;
            updateOrderParam.purchase_code = $scope.orderListModel.purchase_code;
            if (typeof $scope.orderListModel.billdate == 'object')
            {
                var billdate = utilityService.parseDateToStr($scope.orderListModel.billdate, $scope.adminService.appConfig.date_format);
            }
            updateOrderParam.date = utilityService.changeDateToSqlFormat(billdate, $scope.adminService.appConfig.date_format);
            if (typeof $scope.orderListModel.duedate == 'object')
            {
                var duedate = utilityService.parseDateToStr($scope.orderListModel.duedate, $scope.adminService.appConfig.date_format);
            }
            updateOrderParam.duedate = utilityService.changeDateToSqlFormat(duedate, $scope.adminService.appConfig.date_format);
            updateOrderParam.customer_address = $scope.orderListModel.customerInfo.shopping_address;
            updateOrderParam.subtotal = $scope.orderListModel.subtotal;
            updateOrderParam.tax_amount = (parseFloat($scope.orderListModel.taxAmt)).toFixed(2);
            updateOrderParam.discount_amount = $scope.orderListModel.discountAmt;
            updateOrderParam.discount_percentage = $scope.orderListModel.discountPercentage;
            updateOrderParam.discount_mode = $scope.orderListModel.discountMode;
            updateOrderParam.discount_percentage = $scope.orderListModel.discount_value;
            updateOrderParam.total_amount = $scope.orderListModel.round;
            updateOrderParam.round_off = (parseFloat($scope.orderListModel.roundoff)).toFixed(2);
            updateOrderParam.paymentmethod = "";
            updateOrderParam.notes = "";
            updateOrderParam.item = [];
            updateOrderParam.reference_no = $scope.orderListModel.reference_no;
            updateOrderParam.customer_id = $scope.orderListModel.customerInfo.id;
            //Don't remove below line overall qty
            updateOrderParam.overall_qty = $scope.orderListModel.totalQty;
            //Don't remove above line it must sent
            updateOrderParam.item.orderDetails = [];
            for (var i = 0; i < $scope.orderListModel.list.length - 1; i++)
            {
//                if ($scope.orderListModel.list[i].is_barcode_generated != 1)
//                        //if ($scope.orderListModel.list[i].sku != '')
//                        {
                var ordereditems = {};
                if (typeof $scope.orderListModel.list[i].productInfo != undefined && $scope.orderListModel.list[i].productInfo.id != undefined)
                {
                    ordereditems.product_id = $scope.orderListModel.list[i].productInfo.id;
                    ordereditems.product_sku = $scope.orderListModel.list[i].productInfo.sku;
                    ordereditems.product_name = $scope.orderListModel.list[i].productInfo.name;
                }

                ordereditems.unit_price = parseFloat($scope.orderListModel.list[i].purchase_price);
                ordereditems.qty = parseFloat($scope.orderListModel.list[i].qty).toFixed($scope.quantityFormat);
                ;
                ordereditems.hsn_code = $scope.orderListModel.list[i].hsncode;
                if ($scope.orderListModel.list[i].uomInfo != undefined && $scope.orderListModel.list[i].uomInfo.id != undefined && $scope.orderListModel.list[i].uomInfo.id != undefined)
                {
                    ordereditems.uom_name = $scope.orderListModel.list[i].uomInfo.name;
                    ordereditems.uom_id = $scope.orderListModel.list[i].uomInfo.id;
                }

                ordereditems.custom_opt1 = '';
                ordereditems.custom_opt2 = '';
                ordereditems.custom_opt3 = '';
                ordereditems.custom_opt4 = '';
                ordereditems.custom_opt5 = '';
                if ($scope.orderListModel.list[i].custom_opt1 != '' && $scope.orderListModel.list[i].custom_opt1 != null)
                {
                    ordereditems.custom_opt1 = $scope.orderListModel.list[i].custom_opt1;
                }
                if ($scope.orderListModel.list[i].custom_opt2 != '' && $scope.orderListModel.list[i].custom_opt2 != null)
                {
                    ordereditems.custom_opt2 = $scope.orderListModel.list[i].custom_opt2;
                }
                if ($scope.orderListModel.list[i].custom_opt3 != '' && $scope.orderListModel.list[i].custom_opt3 != null)
                {
                    ordereditems.custom_opt3 = $scope.orderListModel.list[i].custom_opt3;
                }
                if ($scope.orderListModel.list[i].custom_opt4 != '' && $scope.orderListModel.list[i].custom_opt4 != null)
                {
                    ordereditems.custom_opt4 = $scope.orderListModel.list[i].custom_opt4;
                }
                if ($scope.orderListModel.list[i].custom_opt5 != '' && $scope.orderListModel.list[i].custom_opt5 != null)
                {
                    ordereditems.custom_opt5 = $scope.orderListModel.list[i].custom_opt5;
                }
                ordereditems.custom_opt1_key = $rootScope.appConfig.purchase_invoice_item_custom1_label;
                ordereditems.custom_opt2_key = $rootScope.appConfig.purchase_invoice_item_custom2_label;
                ordereditems.custom_opt3_key = $rootScope.appConfig.purchase_invoice_item_custom3_label;
                ordereditems.custom_opt4_key = $rootScope.appConfig.purchase_invoice_item_custom4_label;
                ordereditems.custom_opt5_key = $rootScope.appConfig.purchase_invoice_item_custom5_label;
                ordereditems.mrp_price = $scope.orderListModel.list[i].mrp_price;
                ordereditems.purchase_price = $scope.orderListModel.list[i].purchase_price;
                ordereditems.selling_price = $scope.orderListModel.list[i].sales_price;
                ordereditems.purchase_price_w_tax = $scope.orderListModel.list[i].purchase_price_with_tax;
                ordereditems.discount_mode = $scope.orderListModel.list[i].discount_mode;
                ordereditems.discount_value = $scope.orderListModel.list[i].discount_value;
                ordereditems.discount_amount = parseFloat($scope.orderListModel.list[i].discount_amount).toFixed(2);
                ordereditems.selling_tax_id = $scope.orderListModel.list[i].selling_tax_id;
                ordereditems.selling_tax_percentage = $scope.orderListModel.list[i].selling_tax_percentage;
                ordereditems.tax_id = $scope.orderListModel.list[i].tax_id;
                ordereditems.tax_percentage = $scope.orderListModel.list[i].tax_percentage;
                ordereditems.tax_amount = ordereditems.purchase_price * ordereditems.qty * (ordereditems.tax_percentage / 100);
                ordereditems.tax_amount = parseFloat(ordereditems.tax_amount).toFixed(2);
                ordereditems.total_price = parseFloat($scope.orderListModel.list[i].purchase_price) * parseFloat(ordereditems.qty) - parseFloat(ordereditems.discount_amount) + parseFloat(ordereditems.tax_amount);
                ordereditems.total_price = parseFloat(ordereditems.total_price).toFixed(2);
                updateOrderParam.item.push(ordereditems);
//                        }
            }
            updateOrderParam.customattribute = [];
            for (var i = 0; i < $scope.customAttributeList.length; i++)
            {
                var customattributeItem = {};
                if ($scope.customAttributeList[i].input_type == 'date' && $scope.customAttributeList[i].value != '' && $scope.customAttributeList[i].value != null)
                {
                    if (typeof $scope.customAttributeList[i].value == 'object')
                    {
                        $scope.customdate = utilityService.parseDateToStr($scope.customAttributeList[i].value, $rootScope.appConfig.date_format);
                    } else
                    {
                        $scope.customdate = $scope.customAttributeList[i].value;
                    }
                    $scope.customAttributeList[i].value = utilityService.changeDateToSqlFormat($scope.customdate, $rootScope.appConfig.date_format);
                }
                if ($scope.customAttributeList[i].input_type == "YesOrNo")
                {
                    if ($scope.customAttributeList[i].value == true || $scope.customAttributeList[i].value == "yes")
                    {
                        $scope.customAttributeList[i].value = "yes";
                    } else
                    {
                        $scope.customAttributeList[i].value = "no";
                    }
                }
                if ($scope.customAttributeList[i].value != undefined)
                {
                    customattributeItem.value = $scope.customAttributeList[i].value;
                } else if ($scope.customAttributeList[i].default_value != undefined)
                {
                    customattributeItem.value = $scope.customAttributeList[i].default_value;
                } else
                {
                    customattributeItem.value = "";
                }

                customattributeItem.attribute_code = $scope.customAttributeList[i].attribute_code;
                updateOrderParam.customattribute.push(customattributeItem);
            }
            if (parseFloat(updateOrderParam.total_amount) > 0)
            {
                adminService.editPurchaseInvoice(updateOrderParam, $stateParams.id, headers).then(function (response)
                {
                    if (response.data.success == true)
                    {
                        $scope.isDataSavingProcess = false;
                        $scope.formReset();
                        $state.go('app.purchaseInvoice');
                    } else
                    {
                        $scope.isDataSavingProcess = false;
                    }
                }).catch(function (response) {
                    console.error('Error occurred:', response.status, response.data);
                    $scope.isDataSavingProcess = false;
                    var element = document.getElementById("btnLoad");
                    element.classList.remove("btn-loader");
                });
            } else
            {
                sweet.show('Oops...', 'Invoice amount cannot be zero', 'error');
                $scope.isDataSavingProcess = false;
                var element = document.getElementById("btnLoad");
                element.classList.remove("btn-loader");
            }
        }
        $scope.event = null;
        $scope.keyupHandler = function (event)
        {
            if ($scope.orderListModel.customerInfo != '' && $scope.orderListModel.customerInfo != null && $scope.orderListModel.customerInfo.id != undefined && $scope.orderListModel.customerInfo.id != '' && $scope.orderListModel.reference_no != undefined && $scope.orderListModel.reference_no != '')
            {
                $timeout(function () {
                    $scope.getReferenceList(true);
                }, 1000)

            }

        }
        $scope.getReferenceList = function (fromKey) {
            if ($scope.orderListModel.customerInfo.id != undefined && $scope.orderListModel.customerInfo.id != '')
            {
                var getListParam = {};
                getListParam.customer_id = $scope.orderListModel.customerInfo.id;
                getListParam.reference_no = $scope.orderListModel.reference_no;
                getListParam.purchase_invoice_id = $stateParams.id;
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getReferenceDetail(getListParam, configOption).then(function (response)
                {
                    if (response.data.success == true)
                    {
                        if (fromKey == false)
                        {
                            $scope.modifyOrder();
                        }
                    } else if (response.data.success == false)
                    {
                        sweet.show("Oops", 'Invoice number already exists', "error");
                        $scope.orderListModel.reference_no = '';
                        $scope.isDataSavingProcess = false;
                        var element = document.getElementById("btnLoad");
                        element.classList.remove("btn-loader");
                    }

                });
            }
        };
        $scope.getPrefixList();
//        $(document).ready(function () {
//            $("#btnLoad").attr('disabled', 'disabled');
//            var values = $("#myForm").serialize();
//            $window.localStorage.setItem("formData", values);
//            $("form").keyup(function (event) {
//                // To Disable Submit Button
//                alert(event)
//                $("#btnLoad").attr('disabled', 'disabled');
//                // To Enable Submit Button
//                $("#btnLoad").removeAttr('disabled');
//            });
//        });
        $rootScope.oldData = [];
        $('#myForm').each(function () {
            var elem = $(this);
            elem.data('oldVal', elem.val());
            var element = document.getElementById("btnLoad");
            element.classList.add("disabled");
            elem.bind("click", function (event) {
                // Save current value of element
                var value = $("form").serializeArray();
                if ($rootScope.oldData.length == 0)
                {
                    $rootScope.oldData = value;
                }
            });
            // Look for changes in the value
            elem.bind("propertychange change keyup input paste", function (event) {
                // If value has changed...
                $rootScope.newData = $("form").serializeArray();
                if ($rootScope.newData.length == $rootScope.oldData.length)
                {
                    var found = false;
                    for (var i = 0; i < $rootScope.newData.length; i++)
                    {
                        if (String($rootScope.newData[i].value) != String($rootScope.oldData[i].value))
                        {
                            found = true;
                        }
                    }
                    if (found == true)
                    {
                        var element = document.getElementById("btnLoad");
                        element.classList.remove("disabled");
                    } else
                    {
                        var element = document.getElementById("btnLoad");
                        element.classList.add("disabled");
                    }
                }

            });
        });

    }]);
