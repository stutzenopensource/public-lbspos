app.controller('receivableReportCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', function ($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout) {
        $rootScope.getNavigationBlockMsg = null;
        $scope.receivableReportModel = {
            currentPage: 1,
            total: 0,
            total_Amount: 0,
            limit: 10,
            list: [],
            length: [],
            date: '',
            invoice_id: '',
            customer_id: '',
            amount: '',
            status: '',
            //             sku: '',

            serverList: null,
            isLoadingProgress: false,
            isSearchLoadingProgress: false
        };
        $scope.showDetails = false;
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.receivableReportModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat);
        $scope.validationFactory = ValidationFactory;
        $scope.openDate = function (index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            } else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };

        $scope.searchFilter = {
            fromdate: '',
            todate: '',
            customerInfo: {}

        };
        $scope.searchFilter.todate = $scope.currentDate;

        $scope.clearFilters = function ()
        {
            $scope.searchFilter = {
                fromdate: '',
                todate: '',
                customerInfo: ''

            };

            $scope.searchFilter.todate = $scope.currentDate;
        }

        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.validateDateFilterData = function ()
        {
            var retVal = false;
            if ($scope.searchFilter.todate !== '' || $scope.searchFilter.fromdate !== '')
            {
                retVal = true;
            }
            return retVal;
        };
        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'receivable_form';


        $scope.$on('onLocalStorageFormRenterCompleteEvent', function (event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });

        $scope.$on('onLocalStorageFieldRenterCompleteEvent', function (event, formName) {

            if ($scope.localStorageFormName == formName)
            {
                $scope.localStorageRenterCount++;
            }
            if ($scope.localStorageRenterCount >= $scope.localStorageCount)
            {
                console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
            }

        });

        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function (event, formName) {

            if ($scope.localStorageFormName == formName)
            {
                $scope.localStorageRetrieveCount++;
            }
            if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
            {
                $scope.initTableFilter();
            }
            console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);

        });

        $scope.init = function ()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');

        };

        $scope.$on('onLocalStorageReadyEvent', function () {

            $timeout(function () {
                $scope.init();
            }, 300);
        });


        $scope.show = function (index)
        {
            if ($scope.showDetails[index])
            {
                $scope.showDetails[index] = false;
            } else
                $scope.showDetails[index] = true;
        };
        $scope.getList = function () {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'receivablereport';
            getListParam.name = '';
            if ($scope.searchFilter.customerInfo != null && typeof $scope.searchFilter.customerInfo != 'undefined' && typeof $scope.searchFilter.customerInfo.id != 'undefined')
            {
                getListParam.customer_id = $scope.searchFilter.customerInfo.id;
            } else
            {
                getListParam.customer_id = '';
            }
//            getListParam.from_date = $scope.searchFilter.fromdate;
            getListParam.to_date = $scope.searchFilter.todate;

//            if ($scope.searchFilter.todate != '' && ($scope.searchFilter.fromdate == null || $scope.searchFilter.fromdate == ''))
//            {
//                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
//                {
//                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
//                }
//                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
//                getListParam.from_date = getListParam.to_date;
//            } else if ($scope.searchFilter.fromdate != '' && ($scope.searchFilter.todate == null || $scope.searchFilter.todate == ''))
//            {
//                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
//                {
//                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
//                }
//                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
//                getListParam.to_date = getListParam.from_date;
//            } else
//            {
//                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
//                {
//                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
//                }
//                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
//                {
//                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
//                }
//                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
//                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
//            }

//            if ($scope.searchFilter.fromdate != null && $scope.searchFilter.fromdate != '')
//            {
//                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
//                {
//                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.adminService.appConfig.date_format);
//                }
//                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.adminService.appConfig.date_format);
//            }
            if ($scope.searchFilter.todate != null && $scope.searchFilter.todate != '')
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.adminService.appConfig.date_format);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.adminService.appConfig.date_format);
            }

            getListParam.is_active = $scope.searchFilter.is_active;
            getListParam.start = 0;
            getListParam.limit = 0;

            $scope.receivableReportModel.isLoadingProgress = true;
            $scope.receivableReportModel.isSearchLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.payableAndReceivableList(getListParam, configOption, headers).then(function (response)
            {
                var totalAmt = 0.00;
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.receivableReportModel.list = data.receivable;
                    //  $scope.showDetails[i] = false;
                    for (var i = 0; i < $scope.receivableReportModel.list.length; i++)
                    {
                        $scope.receivableReportModel.list[i].newdate = utilityService.parseStrToDate($scope.receivableReportModel.list[i].date);
                        $scope.receivableReportModel.list[i].date = utilityService.parseDateToStr($scope.receivableReportModel.list[i].newdate, $scope.adminService.appConfig.date_format);
                        totalAmt = totalAmt + parseFloat($scope.receivableReportModel.list[i].amount);
                        $scope.receivableReportModel.list[i].amount = utilityService.changeCurrency($scope.receivableReportModel.list[i].amount, $rootScope.appConfig.thousand_seperator);
                    }

                    $scope.receivableReportModel.total = data.total;
                    $scope.receivableReportModel.total_Amount = totalAmt;
                    $scope.receivableReportModel.total_Amount = parseFloat($scope.receivableReportModel.total_Amount).toFixed(2);
                    $scope.receivableReportModel.total_Amount = utilityService.changeCurrency($scope.receivableReportModel.total_Amount, $rootScope.appConfig.thousand_seperator);

                }
                $scope.receivableReportModel.isLoadingProgress = false;
                $scope.receivableReportModel.isSearchLoadingProgress = false;
            });

        };
        $scope.getCustomerList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            autosearchParam.mode=1;
            return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatCustomerModel = function (model)
        {
            if (model != null)
            {
                if (model.fname != undefined && model.phone != undefined && model.email != undefined)
                {
                    return model.fname + '(' + model.phone + ',' + model.email + ')';
                } else if (model.fname != undefined && model.phone != undefined)
                {
                    return model.fname + '(' + model.phone + ')';
                }
            }
            return  '';
        };
        //        $scope.formatcustomerModel = function(model) {
        //
        //            if (model !== null)
        //            {
        //                $scope.searchFilter.id = model.id;
        //                return model.fname;
        //            }
        //            return  '';
        //        };

        $scope.viewRedirect = function (id)
        {
            if ($rootScope.appConfig.invoice_print_template === 't1')
            {
                $state.go('app.invoiceView1', {
                    'id': id
                }, {
                    'reload': true
                });
            } else if ($rootScope.appConfig.invoice_print_template === 't2')
            {
                $state.go('app.invoiceView2', {
                    'id': id
                }, {
                    'reload': true
                });
            } else
            {
                $state.go('app.invoiceView1', {
                    'id': id
                }, {
                    'reload': true
                });
            }
        };
        $scope.print = function (div)
        {
            var docHead = document.head.outerHTML;
            var printContents = document.getElementById(div).outerHTML;
            var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=865, height=600, resizable=yes, screenX=200, screenY=200, personalbar=no, scrollbars=yes";

            var newWin = window.open("", "_blank", winAttr);
            var writeDoc = newWin.document;
            writeDoc.open();
            writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
            writeDoc.close();
            newWin.focus();
        };

        $scope.view = function (id)
        {
            if ($rootScope.appConfig.invoice_print_template === 't1')
            {
                $state.go('app.invoiceView1', {
                    'id': id
                }, {
                    'reload': true
                });
            } else if ($rootScope.appConfig.invoice_print_template === 't2')
                $state.go('app.invoiceView2', {
                    'id': id
                }, {
                    'reload': true
                });

            else

            {
                $state.go('app.invoiceView1', {
                    'id': id
                }, {
                    'reload': true
                });
            }
        };

        //$scope.getList();
    }]);




