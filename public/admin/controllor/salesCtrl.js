app.controller('salesCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$localStorage', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', function ($scope, $rootScope, adminService, $httpService, $localStorage, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout) {
        $rootScope.getNavigationBlockMsg = null;
        $scope.salesModel = {
            currentPage: 1,
            total: 0,
            total_Amount: 0,
            limit: 10,
            list: [],
            length: [],
            date: '',
            invoice_id: '',
            customer_id: '',
            amount: '',
            status: '',
            prefixList: [],
            prefixArrayList: [],
            //             sku: '',

            serverList: null,
            isLoadingProgress: false,
            isSearchLoadingProgress: false,
            noPrefix: false
        };
        $scope.showDetail = false;
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.salesModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
        $scope.validationFactory = ValidationFactory;
        $scope.openDate = function (index) {
            if (index === 0)
            {
                $scope.fromDateOpen = true;
            } else if (index === 1)
            {
                $scope.toDateOpen = true;
            }
        };
        $scope.searchFilter = {
            fromdate: '',
            todate: '',
            customerInfo: {},
            prefix: ''
        };
        $scope.searchFilter.fromdate = $scope.currentDate;

        $scope.clearFilters = function ()
        {
            $scope.searchFilter = {
                fromdate: '',
                todate: '',
                customerInfo: {},
                prefix: ''
            };
            $scope.salesModel.list = [];
            $scope.searchFilter.fromdate = $scope.currentDate;
            $scope.prefixunclear();

            // $scope.searchFilter.fromdate = $filter('date')($scope.currentDate, $scope.dateFormat);
            // $scope.initTableFilter();
        }
        $scope.prefixunclear = function ()
        {
            if ($scope.salesModel.prefixArrayList.length > 0)
            {
                for (var i = 0; i < $scope.salesModel.prefixArrayList.length; i++) {
                    $scope.salesModel.prefixArrayList[i].index = i;
                    $scope.salesModel.prefixArrayList[i].is_select = true;
                    $scope.selectedPrefixCount = $scope.salesModel.prefixArrayList.length;
                }
            }
        }


        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.validateDateFilterData = function ()
        {
            var retVal = false;
            if ($scope.searchFilter.todate !== '' || $scope.searchFilter.fromdate !== '')
            {
                retVal = true;
            }
            return retVal;
        };

//    $scope.$watch('searchFilter.customerInfo', function(newVal, oldVal)
//    {
//        if (typeof newVal == 'undefined' || newVal == '')
//        {
//        {
//            $scope.initTableFilter();
//        }
//        }
//    });

        $scope.getList = function () {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'salesreport';
            getListParam.name = '';
            if ($scope.searchFilter.customerInfo != null && typeof $scope.searchFilter.customerInfo != 'undefined' && typeof $scope.searchFilter.customerInfo.id != 'undefined')
            {
                getListParam.customer_id = $scope.searchFilter.customerInfo.id;
            } else
            {
                getListParam.customer_id = '';
            }
            if ($scope.showDetail)
            {
                getListParam.show_all = 1;
            } else
            {
                getListParam.show_all = '';
            }
//            getListParam.prefix = $scope.searchFilter.prefix;
            getListParam.from_date = $scope.searchFilter.fromdate;
            getListParam.to_date = $scope.searchFilter.todate;
//            if ($scope.searchFilter.todate != '' && ($scope.searchFilter.fromdate == null || $scope.searchFilter.fromdate == ''))
//            {
//                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
//                {
//                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
//                }
//                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
//                getListParam.from_date = getListParam.to_date;
//            } else if ($scope.searchFilter.fromdate != '' && ($scope.searchFilter.todate == null || $scope.searchFilter.todate == ''))
//            {
//                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
//                {
//                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
//                }
//                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
//                getListParam.to_date = getListParam.from_date;
//            } else
//            {
//                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
//                {
//                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
//                }
//                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
//                {
//                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
//                }
//                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
//                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
//            }
            if ($scope.searchFilter.fromdate != null && $scope.searchFilter.fromdate != '')
            {
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.adminService.appConfig.date_format);
                }
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.adminService.appConfig.date_format);
            }
            if ($scope.searchFilter.todate != null && $scope.searchFilter.todate != '')
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.adminService.appConfig.date_format);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.adminService.appConfig.date_format);
            }

            var prefix = '';
            for (var i = 0; i < $scope.salesModel.prefixArrayList.length; i++)
            {
                if ($scope.salesModel.prefixArrayList[i].is_select == true)
                {
                    prefix = prefix + $scope.salesModel.prefixArrayList[i].value + ',';
                }
            }
            if (prefix.length > 0)
            {
                prefix = prefix.substr(0, prefix.length - 1);
            }
            getListParam.prefix = prefix;
            $scope.prefix1 = prefix;
            getListParam.noPrefix = $scope.salesModel.noPrefix === true ? -1 : '';
            getListParam.is_active = 1;
            getListParam.mode = 2;
            getListParam.from_duedate = '';
            getListParam.to_duedate = '';
            getListParam.discount_persentage = '';
            getListParam.status = '';
            getListParam.start = 0;
            getListParam.limit = 0;
            $scope.salesModel.isLoadingProgress = true;
            $scope.salesModel.isSearchLoadingProgress = true;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            if ($scope.searchFilter.fromdate != '' && $scope.searchFilter.fromdate != null && $scope.searchFilter.fromdate != undefined)
            {
                adminService.getInvoiceList(getListParam, configOption, headers).then(function (response)
                {
                    var totalAmt = 0.00;
                    var totalPaidAmt = 0.00;
                    if (response.data.success === true)
                    {
                        var data = response.data.list;
                        $scope.salesModel.list = data;
                        for (var i = 0; i < $scope.salesModel.list.length; i++)
                        {
                            $scope.salesModel.list[i].newdate = utilityService.parseStrToDate($scope.salesModel.list[i].date);
                            $scope.salesModel.list[i].date = utilityService.parseDateToStr($scope.salesModel.list[i].newdate, $scope.adminService.appConfig.date_format);
                            totalAmt = totalAmt + parseFloat($scope.salesModel.list[i].total_amount);
                            totalPaidAmt = totalPaidAmt + parseFloat($scope.salesModel.list[i].paid_amount);

                        }
                        $scope.salesModel.total = data.total;
                        $scope.salesModel.total_Amount = totalAmt;
                        $scope.salesModel.total_Amount = parseFloat($scope.salesModel.total_Amount).toFixed(2);
                        $scope.salesModel.total_Amount = utilityService.changeCurrency($scope.salesModel.total_Amount, $rootScope.appConfig.thousand_seperator);
                        $scope.salesModel.total_paid_Amount = totalPaidAmt;
                        $scope.salesModel.total_paid_Amount = parseFloat($scope.salesModel.total_paid_Amount).toFixed(2);
                        $scope.salesModel.total_paid_Amount = utilityService.changeCurrency($scope.salesModel.total_paid_Amount, $rootScope.appConfig.thousand_seperator);
                        if ($scope.showDetail)
                        {
                            for (var i = 0; i < $scope.salesModel.list.length; i++)
                            {
                                for (var j = 0; j < $scope.salesModel.list[i].item.length; j++)
                                {
                                    var rowtotal = (parseInt($scope.salesModel.list[i].item[j].qty, 10)) * (parseInt($scope.salesModel.list[i].item[j].unit_price, 10));
                                    $scope.salesModel.list[i].item[j].rowtotal = rowtotal;
                                }
                            }
                        }
                    }
                    $scope.salesModel.isLoadingProgress = false;
                    $scope.salesModel.isSearchLoadingProgress = false;
                });
            } else {
                $scope.salesModel.isLoadingProgress = false;
                $scope.salesModel.isSearchLoadingProgress = false;
            }

        };
        $scope.getCustomerList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            autosearchParam.mode = 1;
            return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatcustomerModel = function (model)
        {
            if (model != null)
            {
                if (model.fname != undefined && model.phone != undefined && model.email != undefined)
                {
                    return model.fname + '(' + model.phone + ',' + model.email + ')';
                } else if (model.fname != undefined && model.phone != undefined)
                {
                    return model.fname + '(' + model.phone + ')';
                }
            }
            return  '';
        };
        $scope.viewRedirect = function (id)
        {
            if ($rootScope.appConfig.invoice_print_template === 't1')
            {
                $state.go('app.invoiceView1', {
                    'id': id
                }, {
                    'reload': true
                });
            } else if ($rootScope.appConfig.invoice_print_template === 't2')
            {
                $state.go('app.invoiceView2', {
                    'id': id
                }, {
                    'reload': true
                });
            } else
            {
                $state.go('app.invoiceView1', {
                    'id': id
                }, {
                    'reload': true
                });
            }
        };
        $scope.getPrefixList = function ()
        {
            var getListParam = {};
            getListParam.setting = 'sales_invoice_prefix';
            var headers = {};
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getAppSettingsList(getListParam, configOption, headers).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    if (data.list.length > 0)
                    {
                        var perfix = data.list[0].value;
                        perfix = perfix.split('\n');
                        for (var i = 0; i < perfix.length; i++)
                        {
                            if (perfix[i].split('-')[1] == 'default')
                            {
                                perfix[i] = perfix[i].split('-')[0];
                            }
                            $scope.salesModel.prefixList.push(perfix[i]);
                        }
                    }

                    for (var i = 0; i < $scope.salesModel.prefixList.length; i++) {
                        $scope.salesModel.prefixArrayList.push({value: $scope.salesModel.prefixList[i]});
                    }
                    for (var i = 0; i < $scope.salesModel.prefixArrayList.length; i++) {
                        $scope.salesModel.prefixArrayList[i].index = i;
                        $scope.salesModel.prefixArrayList[i].is_select = false;
                        $scope.salesModel.noPrefix = false;
                        $scope.selectedPrefixCount = $scope.salesModel.prefixArrayList.length;
                    }
                    console.log($scope.salesModel.prefixArrayList);
                }
            });
        };
        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'sales_form';
        $scope.$on('onLocalStorageFormRenterCompleteEvent', function (event, data) {
            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });

        $scope.$on('onLocalStorageFieldRenterCompleteEvent', function (event, formName) {
            if ($scope.localStorageFormName == formName)
            {
                $scope.localStorageRenterCount++;
            }
            if ($scope.localStorageRenterCount >= $scope.localStorageCount)
            {
                console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
            }
        });
        $scope.$on('updateTypeaheadFieldValue', function (event, data) {

            if (data.fieldName == "sales_form-customer_dropdown")
            {
                $scope.searchFilter.customerInfo = data.value;
            }
        });
        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function (event, formName) {
            if ($scope.localStorageFormName == formName)
            {
                $scope.localStorageRetrieveCount++;
            }
            if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
            {
                $scope.initTableFilter();
            }
            console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);
        });

        $scope.init = function ()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');
        };

        $scope.$on('onLocalStorageReadyEvent', function () {
            $timeout(function () {
                $scope.init();
            }, 300);
        });
        $scope.print = function (div)
        {
            var docHead = document.head.outerHTML;
            var printContents = document.getElementById(div).outerHTML;
            var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=865, height=600, resizable=yes, screenX=200, screenY=200, personalbar=no, scrollbars=yes";

            var newWin = window.open("", "_blank", winAttr);
            var writeDoc = newWin.document;
            writeDoc.open();
            writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
            writeDoc.close();
            newWin.focus();
        };

        $scope.searchPrefix = '';

        $scope.selectAllMultiSelect1 = false;
        $scope.checkAllMultiSelect = function (index)
        {
            if (index == 1)
            {
                for (var i = 0; i < $scope.salesModel.prefixArrayList.length; i++)
                {
                    if ($scope.selectAllMultiSelect1 == true)
                    {
                        $scope.salesModel.prefixArrayList[i].is_select = true;
                        $scope.salesModel.noPrefix = true;
                    } else
                    {
                        $scope.salesModel.prefixArrayList[i].is_select = false;
                        $scope.salesModel.noPrefix = false;
                    }
                }
            }
            $scope.showMultiSelect(index);
            $scope.findSelectedCount(index);
        };

        $scope.noneCheckAllMultiSelect = function (index)
        {
            $scope.selectAllMultiSelect1 = false;
            if ($scope.salesModel.noPrefix == false)
            {
                $scope.salesModel.noPrefix = false;
            } else if ($scope.salesModel.noPrefix == true)
            {
                $scope.salesModel.noPrefix = true;
            }
//            for (var i = 0; i < $scope.salesModel.prefixArrayList.length; i++)
//            {
//                if ($scope.selectAllMultiSelect1 == true)
//                {
//                    $scope.salesModel.prefixArrayList[i].is_select = true;
//                    $scope.salesModel.noPrefix = true;
//                } else
//                {
//                    $scope.salesModel.prefixArrayList[i].is_select = false;
//                }
//            }
            if ($scope.salesModel.noPrefix == true)
            {
                $scope.selectedPrefixCount = 1;
            } else {
                $scope.selectedPrefixCount = 0;
            }
        }

        $scope.showMultiSelectList1 = false;
        $scope.showMultiSelect = function (index)
        {
            if (index == 1)
            {
                if ($scope.showMultiSelectList1 == false)
                {
                    $scope.showMultiSelectList1 = true;
//                    $scope.salesModel.noPrefix = false;
                } else if ($scope.showMultiSelectList1 == true)
                {
                    $scope.showMultiSelectList1 = false;
                }
            }
        };
        $scope.closeMultiSelect = function (index)
        {
            if (index == 1)
            {
                $timeout(function () {
                    $scope.showMultiSelectList1 = false;
                }, 0)
            }
        };

        $scope.findSelectedCount = function (index)
        {
            if (index == 1)
            {
                $scope.selectedPrefixCount = 0;
                for (var i = 0; i < $scope.salesModel.prefixArrayList.length; i++)
                {
                    if ($scope.salesModel.prefixArrayList[i].is_select == true)
                    {
                        $scope.selectedPrefixCount = $scope.selectedPrefixCount + 1;
//                        $scope.salesModel.noPrefix = false;
                    }
                }
            }
        };

        //$scope.getList();
        $scope.getPrefixList();
    }]);




