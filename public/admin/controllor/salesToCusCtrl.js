app.controller('salesToCusCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', function ($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout) {
        $rootScope.getNavigationBlockMsg = null;
        $scope.salesToCusModel = {
            currentPage: 1,
            total: 0,
            total_Amount: 0,
            limit: 10,
            list: [],
            length: [],
            productname: '',
            uom: '',
            colspan: '',
            quantity: '',
            amount: '',
            serverList: null,
            isLoadingProgress: false,
            isSearchLoadingProgress: false,
            tax_list: [],
            total_Tax_Amount: 0
        };
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.salesToCusModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
        $scope.validationFactory = ValidationFactory;
        $scope.openDate = function (index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            } else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };
        $scope.searchFilter = {
            fromdate: '',
            todate: '',
            invoice_code: '',
            customer_type: ''
        };
        $scope.searchFilter.fromdate = $scope.currentDate;
        $scope.clearFilters = function ()
        {
            $scope.searchFilter = {
                fromdate: '',
                todate: '',
                invoice_code: '',
                customer_type: ''
            };
            $scope.salesToCusModel.list = [];
            $scope.searchFilter.fromdate = $scope.currentDate;
        }
        $scope.print = function (div)
        {
            var docHead = document.head.outerHTML;
            var printContents = document.getElementById(div).outerHTML;
            var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=865, height=600, resizable=yes, screenX=200, screenY=200, personalbar=no, scrollbars=yes";

            var newWin = window.open("", "_blank", winAttr);
            var writeDoc = newWin.document;
            writeDoc.open();
            writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
            writeDoc.close();
            newWin.focus();
        };

        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.validateDateFilterData = function ()
        {
            var retVal = false;
            if ($scope.searchFilter.todate !== '' || $scope.searchFilter.fromdate !== '')
            {
                retVal = true;
            }
            return retVal;
        };      
        $scope.todate = '';
        $scope.getList = function () {
            $scope.salesToCusModel.colspan = 18;
            $scope.totalspan = 0;
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'salestocus';

            if ($scope.searchFilter.todate != '' && ($scope.searchFilter.fromdate == null || $scope.searchFilter.fromdate == ''))
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
                getListParam.from_date = getListParam.to_date;
                $scope.todate = getListParam.to_date;
            } else if ($scope.searchFilter.fromdate != '' && ($scope.searchFilter.todate == null || $scope.searchFilter.todate == ''))
            {
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
                getListParam.to_date = getListParam.from_date;
                $scope.todate = getListParam.to_date;
            } else
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
                $scope.todate = getListParam.to_date;
            }
            getListParam.invoice_code = $scope.searchFilter.invoice_code;
            getListParam.customer_type = $scope.searchFilter.customer_type;
            $scope.salesToCusModel.isLoadingProgress = true;
            adminService.getSalesToCusList(getListParam, headers).then(function (response)
            {
                var totalAmt = 0.00;
                var totalTaxAmt = 0.00;
                var totalInvoiceAmt = 0.00;
                var totalQty = 0;
                var totalDiscAmt = 0.00;
                $scope.salesToCusModel.colspan = 18;
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.salesToCusModel.list = data;
                    $scope.salesToCusModel.tax_list = response.data.tax_list;
                    $scope.salesToCusModel.colspan = $scope.salesToCusModel.colspan + $scope.salesToCusModel.tax_list.length;
                    for (var i = 0; i < $scope.salesToCusModel.list.length; i++)
                    {
                        totalAmt = totalAmt + parseFloat($scope.salesToCusModel.list[i].valauable_amount);
                        totalDiscAmt = totalDiscAmt + parseFloat($scope.salesToCusModel.list[i].discount_amount);
//                        $scope.salesToCusModel.list[i].valauable_amount = utilityService.changeCurrency($scope.salesToCusModel.list[i].valauable_amount, $rootScope.appConfig.thousand_seperator);
                        totalTaxAmt = totalTaxAmt + parseFloat($scope.salesToCusModel.list[i].tax_amount);
                        totalInvoiceAmt = totalInvoiceAmt + parseFloat($scope.salesToCusModel.list[i].total_amount);
                        $scope.salesToCusModel.list[i].total_amount = utilityService.changeCurrency($scope.salesToCusModel.list[i].total_amount, $rootScope.appConfig.thousand_seperator);
                        $scope.salesToCusModel.list[i].valauable_amount = utilityService.changeCurrency($scope.salesToCusModel.list[i].valauable_amount, $rootScope.appConfig.thousand_seperator);
//                        totalQty = totalQty + parseFloat($scope.salesToCusModel.list[i].overall_qty === '' ?  $scope.salesToCusModel.list[i].overall_qty : 0 + $scope.salesToCusModel.list[i].overall_qty);
                        totalQty = totalQty + parseFloat($scope.salesToCusModel.list[i].overall_qty === '' || $scope.salesToCusModel.list[i].overall_qty === null ? 0 : $scope.salesToCusModel.list[i].overall_qty);
                    }
                    for (var i = 0; i < $scope.salesToCusModel.tax_list.length; i++)
                    {
                        var totalTax = 0.00;
                        for (var j = 0; j < $scope.salesToCusModel.list.length; j++)
                        {
                            totalTax = totalTax + parseFloat($scope.salesToCusModel.list[j].tax_detail[i].tax_amount);
                        }
                        $scope.salesToCusModel.tax_list[i].total_tax = totalTax;
                        $scope.salesToCusModel.tax_list[i].total_tax = parseFloat(totalTax).toFixed(2);
                    }
                    $scope.salesToCusModel.total_Amount = totalAmt;
                    $scope.salesToCusModel.total_Amount = parseFloat($scope.salesToCusModel.total_Amount).toFixed(2);
                    $scope.salesToCusModel.total_Amount = utilityService.changeCurrency($scope.salesToCusModel.total_Amount, $rootScope.appConfig.thousand_seperator);
                    $scope.salesToCusModel.total_Disc_Amount = totalDiscAmt;
                    $scope.salesToCusModel.total_Disc_Amount = parseFloat($scope.salesToCusModel.total_Disc_Amount).toFixed(2);
                    $scope.salesToCusModel.total_Disc_Amount = utilityService.changeCurrency($scope.salesToCusModel.total_Disc_Amount, $rootScope.appConfig.thousand_seperator);
                    $scope.salesToCusModel.total_Tax_Amount = totalTaxAmt;
                    $scope.salesToCusModel.total_Tax_Amount = parseFloat($scope.salesToCusModel.total_Tax_Amount).toFixed(2);
                    $scope.salesToCusModel.total_Tax_Amount = utilityService.changeCurrency($scope.salesToCusModel.total_Tax_Amount, $rootScope.appConfig.thousand_seperator);
                    $scope.salesToCusModel.total_Invoice_Amount = totalInvoiceAmt;
                    $scope.salesToCusModel.total_Invoice_Amount = parseFloat($scope.salesToCusModel.total_Invoice_Amount).toFixed(2);
                    $scope.salesToCusModel.total_Invoice_Amount = utilityService.changeCurrency($scope.salesToCusModel.total_Invoice_Amount, $rootScope.appConfig.thousand_seperator);
                    $scope.salesToCusModel.total_Overall_Qty = totalQty;
                }
                $scope.salesToCusModel.isLoadingProgress = false;
            });

        };

        $scope.viewRedirect = function (id)
        {
            if ($rootScope.appConfig.invoice_print_template === 't1')
            {
                $state.go('app.invoiceView1', {'id': id}, {'reload': true});
            } else if ($rootScope.appConfig.invoice_print_template === 't2')
            {
                $state.go('app.invoiceView2', {'id': id}, {'reload': true});
            } else
            {
                $state.go('app.invoiceView1', {'id': id}, {'reload': true});
            }
        };
        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'salestocus_report_form';
        $scope.$on('onLocalStorageFormRenterCompleteEvent', function (event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });

        $scope.$on('onLocalStorageFieldRenterCompleteEvent', function (event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRenterCount++;
                }
                if ($scope.localStorageRenterCount >= $scope.localStorageCount)
                {
                    console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                    $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
                }

            }
        });
        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function (event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRetrieveCount++;
                }
                if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
                {
                    $scope.initTableFilter();
                }
                console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);
            }
        });

        $scope.init = function ()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');

        };

        $scope.$on('onLocalStorageReadyEvent', function () {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                $timeout(function () {
                    $scope.init();
                }, 300);

            }
        });


        //$scope.getList();
    }]);




