app.controller('sendMailCtrl', ['$scope', '$rootScope', 'adminService', '$filter', '$state', 'Auth', '$timeout', 'APP_CONST', function($scope, $rootScope, adminService, $filter, $state, Auth, $timeout, APP_CONST) {

        $scope.$on('initPopupEvent', function(e)
        {

        });
        $rootScope.$on('mailInfo', function(event, mailDetail) {
            //$scope.receiverEmail = [];
            $scope.receiverEmail = mailDetail.receiver.email;
            $scope.invoice_id = mailDetail.receiver.idDetail;
            if (typeof mailDetail === 'undefined')
                return;
        });
//        $rootScope.$on('idInfo', function(event, idDetail) {
//            //$scope.receiverEmail = [];
//             $scope.invoice_id = idDetail;
//            if (typeof idDetail === 'undefined')
//                return;
//        });
        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.send_mail_form != 'undefined' && typeof $scope.send_mail_form.$pristine != 'undefined' && !$scope.send_mail_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }

        $scope.senderEmail = $rootScope.appConfig.invoicefrom;
        $scope.ccEmail = "";
        $scope.subject = "";
        //$scope.invoice_id = mailDetail.receiver.email;
       // $scope.receiverEmail = [];
        //$scope.receiverEmail = $rootScope.receiver;
        $scope.attahed_pdf_copy = "";
        $scope.formReset = function()
        {
            if (typeof $scope.send_mail_form != 'undefined')
            {
                $scope.reset_sendMail_form.$setPristine();
            }
        }

        $scope.sendEmail = function()
        {
            var sendParam = {};
            console.log("Sender Email ", $scope.senderEmail);
            console.log("Sender Email ", $scope.receiverEmail);
            console.log("Sender Email ", $scope.ccEmail);

            sendParam.from = $scope.senderEmail;
            sendParam.to = $scope.receiverEmail;
            sendParam.cc = $scope.ccEmail;
            sendParam.invoice_id = $scope.invoice_id;
            sendParam.subject = $scope.subject;
            sendParam.attahed_pdf_copy = $scope.attahed_pdf_copy;
            adminService.createSendMail(sendParam).then(function(response) {

                if (response.data.success == true)
                {
                    $scope.formReset();
                    if ($rootScope.appConfig.invoice_print_template === 't1')
                    {
                        $state.go('app.invoiceView1', {'id': response.data.id}, {'reload': true});
                    }
                    else if ($rootScope.appConfig.invoice_print_template === 't2')
                    {
                        $state.go('app.invoiceView2', {'id': response.data.id}, {'reload': true});
                    }
                    else
                    {
                        $state.go('app.invoiceView1', {'id': response.data.id}, {'reload': true});
                    }

                }
                $scope.isDataSavingProcess = false;

            });

            $scope.closePopup();
        }
    }]);




