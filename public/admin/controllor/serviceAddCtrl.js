

app.controller('serviceAddCtrl', ['$scope', '$sce', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', 'ValidationFactory', '$state', '$localStorageService', '$timeout', 'APP_CONST', function ($scope, $sce, $rootScope, adminService, $httpService, $filter, Auth, ValidationFactory, $state, $localStorageService, $timeout, APP_CONST) {

        $scope.serviceModel = {
            id: '',
            name: '',
            sku: '',
            uomName: "",
            uomList: [],
            description: '',
            salesPrice: '',
            isActive: true,
            isPurchase: true,
            isSale: true,
            hsncode: '',
            attachment: [],
            hsnList: [],
            hsncodeTaxinfo: [],
            hsnCodeInfo: []
        }

        $scope.validationFactory = ValidationFactory;

        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function ()
        {
            if (typeof $scope.service_add_form != 'undefined' && typeof $scope.service_add_form.$pristine != 'undefined' && !$scope.service_add_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function ()
        {
            $scope.service_add_form.$setPristine();
            if (typeof $scope.service_add_form != 'undefined')
            {
                $scope.serviceModel.name = "";
                $scope.serviceModel.sku = "";
                $scope.serviceModel.salesPrice = '';
                $scope.serviceModel.description = "";
                $scope.serviceModel.uomName = "";
                $scope.serviceModel.isActive = true;
                $scope.serviceModel.isPurchase = true;
                $scope.serviceModel.isSale = true;
                $scope.serviceModel.hsncode = "";
                $scope.serviceModel.hsnList = [];
                $scope.serviceModel.hsncodeTaxinfo = [];
                $scope.serviceModel.hsnCodeInfo = [];
            }
        }

        $scope.getCUOMNameList = function ()
        {
            var getListParam = {};
            getListParam.id = "";
            getListParam.name = "";
            getListParam.start = ($scope.serviceModel.currentPage - 1) * $scope.serviceModel.limit;
            getListParam.limit = $scope.serviceModel.limit;
            $scope.isDataSavingProcess = true;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getUom(getListParam, configOption).then(function (response)
            {
                var data = response.data;
                $scope.serviceModel.uomList = data.list;
                $scope.isDataSavingProcess = false;
            });
        };
        $scope.getCUOMNameList();
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        /*
         *  It used to format the speciality model value in UI
         */
        $scope.addServiceInfo = function () {

            if ($scope.isDataSavingProcess)
            {
                return;
            }

            $scope.isDataSavingProcess = true;
            var addProductParam = {};
            var headers = {};
            headers['screen-code'] = 'service';
            // addProductParam.id = 0;
            addProductParam.name = $scope.serviceModel.name;
            addProductParam.sku = $scope.serviceModel.sku;
            addProductParam.hsn_code = $scope.serviceModel.hsncode;
            addProductParam.sales_price = parseFloat($scope.serviceModel.salesPrice);
            addProductParam.description = $scope.serviceModel.description;
            addProductParam.type = "Service";
            //   addProductParam.uom = $scope.serviceModel.uomName;

            addProductParam.is_active = $scope.serviceModel.isActive == true ? 1 : 0;
            addProductParam.is_purchase = $scope.serviceModel.isPurchase == true ? 1 : 0;
            addProductParam.is_sale = $scope.serviceModel.isSale == true ? 1 : 0;
            addProductParam.min_stock_qty = '';
            addProductParam.opening_stock = '';
            addProductParam.has_inventory = 0;
            addProductParam.hsn_id = $scope.hsn_id;
            addProductParam.attachment = [];
            if ($scope.serviceModel.uomName != "")
            {
                for (var loopIndex = 0; loopIndex < $scope.serviceModel.uomList.length; loopIndex++)
                {
                    if ($scope.serviceModel.uomName == $scope.serviceModel.uomList[loopIndex].name)
                    {
                        addProductParam.uom_id = $scope.serviceModel.uomList[loopIndex].id;
                        break;
                    }
                }
            } else
            {
                addProductParam.uom_id = "";
            }
            adminService.addService(addProductParam, headers).then(function (response)
            {
                if (response.data.success == true)
                {
                    $scope.saveHsnCode();
                } else {
                    $scope.isDataSavingProcess = false;
                    var element = document.getElementById("btnLoad");
                    element.classList.remove("btn-loader");
                }
            }).catch(function (response) {
                console.error('Error occurred:', response.status, response.data);
                $scope.isDataSavingProcess = false;
                var element = document.getElementById("btnLoad");
                element.classList.remove("btn-loader");
            });
        };
        $scope.showHSNDetail = false;
        $scope.hsnList = [];
        $scope.showHsnPopup = function ()
        {
            $scope.showHSNDetail = true;
//            var url ="http://sso2.accozen.com/public/data.php";
        }
        $scope.closeHsnPopup = function ()
        {
            $scope.showHSNDetail = false;
        }
        $scope.searchFilter = {
            hsn_code: '',
            search: ''
        };
        $scope.clearFilters = function ()
        {
            $scope.searchFilter = {
                hsn_code: '',
                search: ''
            };
            $scope.serviceModel.hsnList = [];
            $scope.msgflag = false;
        };
        
        $scope.getHsnList = function () {
            
//              $.ajax({
//                url: "http://api.open-notify.org/astros.json", // <== Make sure this DOESN'T have ?callback= on it
//                dataType: "jsonp"
//            })
//            .done(function(response) {
//                doSomething();
//                alert("done");
//            })
//            .fail(function(error) {
//                console.log(error);
//                alert("hai");
//            });
            $.getJSON('http://sso2.accozen.com/public/data.php', function (data) {
                $scope.hsnList = data;
                $scope.serviceModel.hsnList = [];
                for (var i = 0; i < $scope.hsnList.length; i++)
                {
                    $scope.serviceModel.hsnList.push($scope.hsnList[i]);
                }
            });
           
        };
        $scope.updateHsn = function (service) {
            $scope.clearFilters();
            $scope.closeHsnPopup();
            $scope.serviceModel.hsnList = [];
            $scope.serviceModel.hsnCodeInfo = service;
            $scope.serviceModel.hsncode = service.hsn_code;
            $scope.hsn_id = service.id;
            $scope.serviceModel.hsncodeTaxinfo = [];
            if (service.hsnTaxMapping.length > 0 && service.hsnTaxMapping != null && typeof service.hsnTaxMapping != "undefined")
            {
                for (var i = 0; i < service.hsnTaxMapping.length; i++)
                {
                    $scope.serviceModel.hsncodeTaxinfo.push(service.hsnTaxMapping[i]);
                }
            }
        }
        $scope.saveHsnCode = function () {
            var createHSNParam = {};
            var headers = {};
            headers['screen-code'] = 'service';
            createHSNParam.hsn_code = $scope.serviceModel.hsnCodeInfo.hsn_code;
            createHSNParam.description = $scope.serviceModel.hsnCodeInfo.description;
            createHSNParam.Type = $scope.serviceModel.hsnCodeInfo.type;
            createHSNParam.is_approved = $scope.serviceModel.hsnCodeInfo.is_approved;
            createHSNParam.is_active = $scope.serviceModel.hsnCodeInfo.is_active;
            createHSNParam.hsnTaxMapping = [];
            for (var i = 0; i < $scope.serviceModel.hsnCodeInfo.hsnTaxMapping.length; i++)
            {
                var taxMap = {
                    group_tax_id: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].group_tax_id,
                    from_price: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].from_price,
                    to_price: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].to_price,
                    group_tax_ids: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].group_tax_ids,
                    intra_group_tax_ids: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].intra_group_tax_ids
                    
                };
                createHSNParam.hsnTaxMapping.push(taxMap);
            }
            createHSNParam.taxDetail = [];
            for (var i = 0; i < $scope.serviceModel.hsnCodeInfo.hsnTaxMapping.length; i++)
            {
                if ($scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail.length > 0)
                {
                    for (var j = 0; j < $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail.length; j++)
                    {
                        var taxDetail =
                                {
                                    sso_tax_id: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].id,
                                    tax_no: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].tax_no,
                                    tax_name: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].tax_name,
                                    tax_percentage: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].tax_percentage,
                                    is_group: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].is_group,
                                    tax_type: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].tax_type,
                                    is_active: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].is_active,
                                    comments: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].comments,
                                    is_display: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].is_display,
                                    hsn_flag: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].hsn_flag,
                                    intra_group_tax_ids: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].intra_group_tax_ids
                                };
                        createHSNParam.taxDetail.push(taxDetail);
                    }
                }
                if ($scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail.length > 0)
                {
                    for (var j = 0; j < $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail.length; j++)
                    {
                        var taxDetail =
                                {
                                    sso_tax_id: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].id,
                                    tax_no: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].tax_no,
                                    tax_name: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].tax_name,
                                    tax_percentage: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].tax_percentage,
                                    is_group: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].is_group,
                                    tax_type: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].tax_type,
                                    is_active: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].is_active,
                                    comments: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].comments,
                                    is_display: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].is_display,
                                    hsn_flag: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].hsn_flag,
                                    intra_group_tax_ids: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].intra_group_tax_ids
                                };
                        createHSNParam.taxDetail.push(taxDetail);
                    }
                }
                var detail =
                        {
                            sso_tax_id: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].group_tax_id,
                            tax_no: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].tax_no,
                            tax_name: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].tax_name,
                            tax_percentage: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].tax_percentage,
                            is_group: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].is_group,
                            tax_type: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].tax_type,
                            is_active: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].is_active,
                            comments: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].comments,
                            is_display: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].is_display,
                            hsn_flag: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].hsn_flag,
                            group_tax_ids: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].group_tax_ids,
                            intra_group_tax_ids: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].intra_group_tax_ids
                        }
                createHSNParam.taxDetail.push(detail);
//                createHSNParam.hsnTaxMapping.push(taxMap);
            }
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.saveHSNCode(createHSNParam, configOption, headers).then(function (response)
            {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.serviceList');
                }
                if (response.data.success == false)
                {
                    $scope.formReset();
                    $state.go('app.serviceList');
                }
                $scope.isDataSavingProcess = false;
            });
        }
    }]);
