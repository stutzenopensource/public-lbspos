

app.controller('serviceEditCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', 'ValidationFactory', '$state', '$localStorageService', '$timeout', 'APP_CONST', '$stateParams', function ($scope, $rootScope, adminService, $httpService, $filter, Auth, ValidationFactory, $state, $localStorageService, $timeout, APP_CONST, $stateParams) {

        $scope.serviceModel = {
            id: '',
            name: '',
            sku: '',
            uom: '',
            uomList: [],
            description: '',
            salesPrice: '',
            is_active: '',
            is_purchase: '',
            is_sale: '',
            hsncode: '',
            isLoadingProgress: false,
            attachment: [],
            serviceDetail: [],
            hsncodeTaxinfo: [],
            hsnList: [],
            hsnCodeInfo: []
        }

        $scope.validationFactory = ValidationFactory;

        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function ()
        {
            if (typeof $scope.service_edit_form != 'undefined' && typeof $scope.service_edit_form.$pristine != 'undefined' && !$scope.service_edit_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function ()
        {
            $scope.service_edit_form.$setPristine();
            if (typeof $scope.service_edit_form != 'undefined')
            {
                $scope.serviceModel.name = "";
                $scope.serviceModel.sku = "";
                $scope.serviceModel.uom = "";
                $scope.serviceModel.salesPrice = '';
                $scope.serviceModel.description = "";
                $scope.serviceModel.is_active = '';
                $scope.serviceModel.is_purchase = '';
                $scope.serviceModel.is_sale = '';
                $scope.serviceModel.hsncode = '';
                $scope.serviceModel.hsnList = [];
                $scope.serviceModel.hsncodeTaxinfo = [];
            }
            $scope.getServiceInfo();
        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.initUpdateDetailTimeoutPromise = null;

        $scope.initUpdateDetail = function ()
        {
            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
            }

            if ($scope.isUOMLoaded)
            {
                $scope.getServiceInfo();
            } else
            {
                $scope.initUpdateDetailTimeoutPromise = $timeout($scope.initUpdateDetail, 300);
            }
        }

        $scope.isUOMLoaded = false;
        $scope.getCUOMList = function ()
        {
            var getListParam = {};
            getListParam.id = "";
            getListParam.name = "";
//            getListParam.start = 0;
//            getListParam.limit = $scope.serviceModel.limit;
            $scope.isDataSavingProcess = true;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getUom(getListParam, configOption).then(function (response)
            {
                var data = response.data;
                $scope.serviceModel.uomList = data.list;
                $scope.isUOMLoaded = true;
                $scope.isDataSavingProcess = false;
            });
        };

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.getServiceInfo = function () {

            $scope.serviceModel.isLoadingProgress = true;
            if (typeof $stateParams.id != 'undefined')
            {
                var serviceListParam = {};
                serviceListParam.id = $stateParams.id;

                serviceListParam.start = 0;
                serviceListParam.limit = 1;
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getProductList(serviceListParam, configOption).then(function (response) {
                    var data = response.data;
                    $scope.serviceDetail = data.list[0];
                    if (data.list.length > 0)
                    {
                        $scope.serviceModel.hsncode = $scope.serviceDetail.hsn_code;
                        $scope.serviceModel.name = $scope.serviceDetail.name;
                        $scope.serviceModel.sku = $scope.serviceDetail.sku;
                        $scope.serviceModel.uom = $scope.serviceDetail.uom;
                        $scope.serviceModel.sales_price = $scope.serviceDetail.sales_price;
                        $scope.serviceModel.description = $scope.serviceDetail.description;
                        $scope.serviceModel.is_active = $scope.serviceDetail.is_active == 1 ? true : false;
                        $scope.serviceModel.is_sale = $scope.serviceDetail.is_sale == 1 ? true : false;
                        $scope.serviceModel.is_purchase = $scope.serviceDetail.is_purchase == 1 ? true : false;
                        $scope.hsn_id = $scope.serviceDetail.hsn_id;
                        $.getJSON('http://sso2.accozen.com/public/data.php', function (data) {
                            $scope.hsnList = data;
                            for (var i = 0; i < $scope.hsnList.length; i++)
                            {
                                if ($scope.hsnList[i].hsn_code == $scope.serviceDetail.hsn_code)
                                {
                                    $scope.serviceModel.hsnList.push($scope.hsnList[i]);
                                    $scope.clearFilters();
                                    $scope.closeHsnPopup();
                                    $scope.serviceModel.hsnList = [];
                                    $scope.serviceModel.hsnCodeInfo = $scope.hsnList[i];
                                    $scope.serviceModel.hsncode = $scope.hsnList[i].hsn_code;
                                    $scope.hsn_id = $scope.hsnList[i].id;
                                    $scope.serviceModel.hsncodeTaxinfo = [];
                                    if ($scope.hsnList[i].hsnTaxMapping.length > 0 && $scope.hsnList[i].hsnTaxMapping != null && typeof $scope.hsnList[i].hsnTaxMapping != "undefined")
                                    {
                                        for (var j = 0; j < $scope.hsnList[i].hsnTaxMapping.length; j++)
                                        {
                                            $scope.serviceModel.hsncodeTaxinfo.push($scope.hsnList[i].hsnTaxMapping[j]);
                                        }
                                    }
                                }
                            }
                        });

                    }
                    $scope.serviceModel.isLoadingProgress = false;
                });
            }
        };

        $scope.initUpdateDetail();
//        $scope.getServiceInfo();
        /*
         *  It used to format the speciality model value in UI
         */
        $scope.editServiceInfo = function () {

            if ($scope.isDataSavingProcess)
            {
                return;
            }

            $scope.isDataSavingProcess = true;
            var addServiceParam = {};
            var headers = {};
            headers['screen-code'] = 'service';
            //  addServiceParam.id = 0;
            addServiceParam.name = $scope.serviceModel.name;
            addServiceParam.sku = $scope.serviceModel.sku;
            addServiceParam.hsn_code = $scope.serviceModel.hsncode;
            addServiceParam.sales_price = parseFloat($scope.serviceModel.sales_price);
            addServiceParam.comments = $scope.serviceModel.description;
            // addServiceParam.uom = $scope.serviceModel.uom;
            addServiceParam.type = "Service";
            addServiceParam.is_active = $scope.serviceModel.is_active ? 1 : 0;
            addServiceParam.is_sale = $scope.serviceModel.is_sale ? 1 : 0;
            addServiceParam.is_purchase = $scope.serviceModel.is_purchase ? 1 : 0;
            addServiceParam.hsn_id = $scope.hsn_id;
            addServiceParam.attachment = [];
            if ($scope.serviceModel.uom != "")
            {
                for (var loopIndex = 0; loopIndex < $scope.serviceModel.uomList.length; loopIndex++)
                {
                    if ($scope.serviceModel.uom == $scope.serviceModel.uomList[loopIndex].name)
                    {
                        addServiceParam.uom_id = $scope.serviceModel.uomList[loopIndex].id;
                        break;
                    }
                }
            } else
            {
                addServiceParam.uom_id = "";
            }
            addServiceParam.min_stock_qty = '';
            addServiceParam.opening_stock = '';
            addServiceParam.has_inventory = 0;
            addServiceParam.description = $scope.serviceModel.description;
            adminService.editService(addServiceParam, $stateParams.id, headers).then(function (response) {
                if (response.data.success == true)
                {
                    $scope.saveHsnCode();
                } else {
                    $scope.isDataSavingProcess = false;
                    var element = document.getElementById("btnLoad");
                    element.classList.remove("btn-loader");
                }
            }).catch(function (response) {
                console.error('Error occurred:', response.status, response.data);
                $scope.isDataSavingProcess = false;
                var element = document.getElementById("btnLoad");
                element.classList.remove("btn-loader");
            });
        };

        $scope.showHSNDetail = false;
        $scope.hsnList = [];
        $scope.showHsnPopup = function ()
        {
            $scope.showHSNDetail = true;
            //var url ="http://sso2.accozen.com/public/data.php";
            $.getJSON('http://sso2.accozen.com/public/data.php', function (data) {
                $scope.hsnList = data;
            });
        }
        $scope.closeHsnPopup = function ()
        {
            $scope.showHSNDetail = false;
        }
        $scope.searchFilter = {
            hsn_code: '',
            search: ''
        };
        $scope.clearFilters = function ()
        {
            $scope.searchFilter = {
                hsn_code: '',
                search: ''
            };
            $scope.serviceModel.hsnList = [];
            $scope.msgflag = false;
        };
        $scope.getHsnList = function () {
            $.getJSON('http://sso2.accozen.com/public/data.php', function (data) {
                $scope.hsnList = data;
                $scope.serviceModel.hsnList = [];
                for (var i = 0; i < $scope.hsnList.length; i++)
                {
                    $scope.serviceModel.hsnList.push($scope.hsnList[i]);
                }
            });
        };
        $scope.updateHsn = function (service) {
            $scope.clearFilters();
            $scope.closeHsnPopup();
            $scope.serviceModel.hsnList = [];
            $scope.serviceModel.hsnCodeInfo = service;
            $scope.serviceModel.hsncode = service.hsn_code;
            $scope.hsn_id = service.id;
            $scope.serviceModel.hsncodeTaxinfo = [];
            if (service.hsnTaxMapping.length > 0 && service.hsnTaxMapping != null && typeof service.hsnTaxMapping != "undefined")
            {
                for (var i = 0; i < service.hsnTaxMapping.length; i++)
                {
                    $scope.serviceModel.hsncodeTaxinfo.push(service.hsnTaxMapping[i]);
                }
            }
        };
        $scope.saveHsnCode = function () {
            var createHSNParam = {};
            var headers = {};
            headers['screen-code'] = 'service';
            createHSNParam.hsn_code = $scope.serviceModel.hsnCodeInfo.hsn_code;
            createHSNParam.description = $scope.serviceModel.hsnCodeInfo.description;
            createHSNParam.Type = $scope.serviceModel.hsnCodeInfo.type;
            createHSNParam.is_approved = $scope.serviceModel.hsnCodeInfo.is_approved;
            createHSNParam.is_active = $scope.serviceModel.hsnCodeInfo.is_active;
            createHSNParam.hsnTaxMapping = [];
            for (var i = 0; i < $scope.serviceModel.hsnCodeInfo.hsnTaxMapping.length; i++)
            {
                var taxMap = {
                    group_tax_id: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].group_tax_id,
                    from_price: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].from_price,
                    to_price: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].to_price
                }
                createHSNParam.hsnTaxMapping.push(taxMap);
            }
            createHSNParam.taxDetail = [];
            for (var i = 0; i < $scope.serviceModel.hsnCodeInfo.hsnTaxMapping.length; i++)
            {
                if ($scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail.length > 0)
                {
                    for (var j = 0; j < $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail.length; j++)
                    {
                        var taxDetail =
                                {
                                    sso_tax_id: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].id,
                                    tax_no: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].tax_no,
                                    tax_name: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].tax_name,
                                    tax_percentage: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].tax_percentage,
                                    is_group: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].is_group,
                                    tax_type: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].tax_type,
                                    is_active: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].is_active,
                                    comments: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].comments,
                                    is_display: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].is_display,
                                    hsn_flag: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].hsn_flag,
                                    intra_group_tax_ids: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].inter_tax_detail[j].intra_group_tax_ids
                                };
                        createHSNParam.taxDetail.push(taxDetail);
                    }
                }
                if ($scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail.length > 0)
                {
                    for (var j = 0; j < $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail.length; j++)
                    {
                        var taxDetail =
                                {
                                    sso_tax_id: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].id,
                                    tax_no: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].tax_no,
                                    tax_name: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].tax_name,
                                    tax_percentage: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].tax_percentage,
                                    is_group: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].is_group,
                                    tax_type: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].tax_type,
                                    is_active: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].is_active,
                                    comments: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].comments,
                                    is_display: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].is_display,
                                    hsn_flag: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].hsn_flag,
                                    intra_group_tax_ids: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].intra_tax_detail[j].intra_group_tax_ids
                                };
                        createHSNParam.taxDetail.push(taxDetail);
                    }
                }
                var detail =
                        {
                            sso_tax_id: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].group_tax_id,
                            tax_no: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].tax_no,
                            tax_name: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].tax_name,
                            tax_percentage: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].tax_percentage,
                            is_group: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].is_group,
                            tax_type: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].tax_type,
                            is_active: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].is_active,
                            comments: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].comments,
                            is_display: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].is_display,
                            hsn_flag: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].hsn_flag,
                            group_tax_ids: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].group_tax_ids,
                            intra_group_tax_ids: $scope.serviceModel.hsnCodeInfo.hsnTaxMapping[i].intra_group_tax_ids
                        };
                createHSNParam.taxDetail.push(detail);
//                createHSNParam.hsnTaxMapping.push(taxMap);
            }
            var configOption = adminService.handleBothSuccessAndErrorResponseConfig;
            adminService.saveHSNCode(createHSNParam, configOption, headers).then(function (response)
            {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.serviceList');
                }
                if (response.data.success == false)
                {
                    $scope.formReset();
                    $state.go('app.serviceList');
                }
                $scope.isDataSavingProcess = false;
            });
        };
        $scope.getCUOMList();

    }]);



