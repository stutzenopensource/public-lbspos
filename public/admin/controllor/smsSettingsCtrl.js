
app.controller('smsSettingsCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$timeout', '$filter', 'Auth', 'ValidationFactory', '$state', 'APP_CONST', '$stateParams', '$http', '$window', function($scope, $rootScope, adminService, $httpService, utilityService, $timeout, $filter, Auth, ValidationFactory, $state, APP_CONST, $stateParams, $http, $window) {

        $scope.smsSettingsModel = {
            "id": 0,
            "date_format": '',
            "isactive": 1,
            "currency": "",
            "sms_gateway_url": "",
            list: [],
            message: '',
            applist: '',
            isLoadingProgress: false
        }

        $scope.validationFactory = ValidationFactory;
        //$scope.smsSettingsModel.sms_setting_url = $rootScope.appConfig.sms_gateway_url;
        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.sms_settings_form != 'undefined' && typeof $scope.sms_settings_form.$pristine != 'undefined' && !$scope.sms_settings_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.formReset = function() {
            if (typeof $scope.sms_settings_form != 'undefined')
            {
                $scope.sms_settings_form.$setPristine();
                $scope.getSmsSettingsInfo();
            }
        }


        $scope.getSmsSettingsInfo = function()
        {
            $scope.smsSettingsModel.isLoadingProgress = true;
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'smssettings';
            getListParam.type = 'sms';
            getListParam.tplname=$stateParams.id;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getSmsSettingsList(getListParam, configOption, headers).then(function(response)
            {
                if (response.data.success === true)

                {
                    var data = response.data;
                    if (data.list.length > 0)
                    {
                        $scope.smsSettingsModel.list = data.list;
                    }
                }
                $scope.smsSettingsModel.isLoadingProgress = false;
            });
        };

        $scope.getappSettingsInfo = function()
        {
            var getListParam = {};
            getListParam.setting = "sms_gateway_url";
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getAppSettingsList(getListParam, configOption).then(function(response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    if (data.list.length > 0)
                    {
                        $scope.smsSettingsModel.applist = data.list[0];
                        $scope.smsSettingsModel.sms_gateway_url = $scope.smsSettingsModel.applist.value;
                    }

                }
            });
        };

        $scope.updateSmsSettings = function()
        {

            $scope.isDataSavingProcess = true;
            var attributeParam = [];
            var headers = {};
            headers['screen-code'] = 'smssettings';
            for (var i = 0; i < $scope.smsSettingsModel.list.length; i++)
            {
                var attributeItem = {};
                attributeItem.id = $scope.smsSettingsModel.list[i].id;
                attributeItem.tplname = $scope.smsSettingsModel.list[i].tplname;
                attributeItem.subject = $scope.smsSettingsModel.list[i].subject;
                attributeItem.message = $scope.smsSettingsModel.list[i].message;
                attributeParam.push(attributeItem);
            }
            adminService.editSmsSettings(attributeParam, headers).then(function(response)
            {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.smssettings', {'reload': true});
                    $scope.getappSettingsInfo();
                }
                $scope.isDataSavingProcess = false;
            });

        }
        $scope.updateAppSettings = function()
        {
            if ($scope.isDataSavingProcess == false)
            {
                $scope.isDataSavingProcess = true;
                var attributeParam = [];

                var newRow = {
                    "setting": "sms_gateway_url",
                    "value": $scope.smsSettingsModel.sms_gateway_url
                }
                attributeParam.push(newRow);
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.editAppSettings(attributeParam, configOption).then(function(response)
                {
                    if (response.data.success == true)
                    {

                        $scope.updateSmsSettings();
                    }
                    else
                    {
                        $scope.isDataSavingProcess = false;
                    }
                });
            }
        }
        $scope.getSmsSettingsInfo();
        $scope.getappSettingsInfo();
    }]);

