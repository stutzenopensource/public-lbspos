app.controller('stockAdjusmentReportCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', '$stateParams', function ($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout, $stateParams) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.stockadjustmentModel = {
            currentPage: 1,
            total: 0,
            total_Amount: 0,
            limit: 10,
            list: [],
            length: [],
            date: '',
            invoice_id: '',
            customer_id: '',
            amount: '',
            status: '',
            //             sku: '',

            serverList: null,
            isLoadingProgress: false,
            isSearchLoadingProgress: false
        };
        $scope.showDetails = [];
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.stockadjustmentModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
        $scope.validationFactory = ValidationFactory;
        $scope.searchFilter = {
            from_date: '',
            to_date: '',
            stockAdjNo: '',
            reason: ''

        };
        $scope.searchFilter.fromdate = $scope.currentDate;
        $scope.openDate = function (index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            } else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };


        $scope.clearFilters = function ()
        {
            $scope.searchFilter = {
                from_date: '',
                to_date: '',
                stockAdjNo: '',
                reason: ''

            };
            $scope.stockadjustmentModel.list = [];
        }
        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'stk_adjust_form';


        $scope.$on('onLocalStorageFormRenterCompleteEvent', function (event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });

        $scope.$on('onLocalStorageFieldRenterCompleteEvent', function (event, formName) {

            if ($scope.localStorageFormName == formName)
            {
                $scope.localStorageRenterCount++;
            }
            if ($scope.localStorageRenterCount >= $scope.localStorageCount)
            {
                console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
            }


        });
        $scope.$on('updateTypeaheadFieldValue', function (event, data) {

            if (data.fieldName == "stk_adjust_form-product_dropdown")
            {
                $scope.searchFilter.productInfo = data.value;
            }
        });

        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function (event, formName) {

            if ($scope.localStorageFormName == formName)
            {
                $scope.localStorageRetrieveCount++;
            }
            if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
            {
                $scope.initTableFilter();
            }
            console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);

        });

        $scope.init = function ()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');

        };

        $scope.$on('onLocalStorageReadyEvent', function () {

            $timeout(function () {
                $scope.init();
            }, 300);


        });
        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.validateDateFilterData = function ()
        {
            var retVal = false;
            if ($scope.searchFilter.todate !== '' || $scope.searchFilter.fromdate !== '')
            {
                retVal = true;
            }
            return retVal;
        };
        $scope.show = function (index)
        {
            if ($scope.showDetails[index])
            {
                $scope.showDetails[index] = false;
            } else
                $scope.showDetails[index] = true;
        };

        $scope.getList = function () {
            $scope.validateDateFlag = false;
            if ($scope.searchFilter.from_date != null && typeof $scope.searchFilter.from_date != 'undefined' && $scope.searchFilter.from_date != '' && $scope.searchFilter.to_date != null && typeof $scope.searchFilter.to_date != 'undefined' && $scope.searchFilter.to_date != '')
            {
                var minDateValue = $filter('date')($scope.searchFilter.from_date, 'yyyy-MM-dd');
                var value = $filter('date')($scope.searchFilter.to_date, 'yyyy-MM-dd');
                minDateValue = moment(minDateValue).valueOf();
                value = moment(value).valueOf();
                if (minDateValue < value)
                {
                    $scope.validateDateFlag = true;
                }
            }
            if ($scope.searchFilter.from_date != null && typeof $scope.searchFilter.from_date != 'undefined' && $scope.searchFilter.from_date != '' && $scope.searchFilter.to_date == null || typeof $scope.searchFilter.to_date == 'undefined' || $scope.searchFilter.to_date == '')
            {
                $scope.validateDateFlag = true;
            }
            if ($scope.searchFilter.to_date != null && typeof $scope.searchFilter.to_date != 'undefined' && $scope.searchFilter.to_date != '' && $scope.searchFilter.from_date == null || typeof $scope.searchFilter.from_date == 'undefined' || $scope.searchFilter.from_date == '')
            {
                $scope.validateDateFlag = true;
            }
            if ($scope.searchFilter.to_date == null || typeof $scope.searchFilter.to_date == 'undefined' || $scope.searchFilter.to_date == '' && $scope.searchFilter.from_date == null || typeof $scope.searchFilter.from_date == 'undefined' || $scope.searchFilter.from_date == '')
            {
                $scope.validateDateFlag = true;
            }
            if ($scope.validateDateFlag == true)
            {
                var getListParam = {};
                var headers = {};
                headers['screen-code'] = 'itemwiseageingreport';
                getListParam.type = 'stock_adjust';
                getListParam.from_date = '';
                getListParam.to_date = '';
                if ($scope.searchFilter.from_date != null && $scope.searchFilter.from_date != '')
                {
                    if ($scope.searchFilter.from_date != null && typeof $scope.searchFilter.from_date == 'object')
                    {
                        getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.from_date, $scope.adminService.appConfig.date_format);
                    }
                    getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.adminService.appConfig.date_format);
                }
                if ($scope.searchFilter.to_date != null && $scope.searchFilter.to_date != '')
                {
                    if ($scope.searchFilter.to_date != null && typeof $scope.searchFilter.to_date == 'object')
                    {
                        getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.to_date, $scope.adminService.appConfig.date_format);
                    }
                    getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.adminService.appConfig.date_format);
                }
                getListParam.auto_no = $scope.searchFilter.stockAdjNo;
                getListParam.reason = $scope.searchFilter.reason;
                getListParam.is_active = 1;
                getListParam.start = 0;
                getListParam.limit = 0;
                $scope.stockadjustmentModel.isLoadingProgress = true;
                $scope.stockadjustmentModel.isSearchLoadingProgress = true;
                var config = adminService.handleOnlyErrorResponseConfig;
                adminService.getNewStockAdjustmentList(getListParam, config, headers).then(function (response)
                {
                    var totalPurchaseAmt = 0.00;
                    var totalSalesAmt = 0.00;
                    var totalMrpAmt = 0.00;
                    var detailPurchaseAmt = 0.00;
                    var detailMrpAmt = 0.00;
                    if (response.data.success === true)
                    {
                        var data = response.data.list;
                        $scope.stockadjustmentModel.list = data;
                        for (var i = 0; i < $scope.stockadjustmentModel.list.length; i++)
                        {
                            $scope.stockadjustmentModel.list[i].newdate = utilityService.parseStrToDate($scope.stockadjustmentModel.list[i].date);
                            $scope.stockadjustmentModel.list[i].date = utilityService.parseDateToStr($scope.stockadjustmentModel.list[i].newdate, $scope.adminService.appConfig.date_format);
                            totalSalesAmt = totalSalesAmt + parseFloat($scope.stockadjustmentModel.list[i].overall_sales_price);
                            totalPurchaseAmt = totalPurchaseAmt + parseFloat($scope.stockadjustmentModel.list[i].overall_purchase_price);
    //                        $scope.stockadjustmentModel.list[i].purchase_price = utilityService.changeCurrency($scope.stockadjustmentModel.list[i].purchase_price, $rootScope.appConfig.thousand_seperator);
                            totalMrpAmt = totalMrpAmt + parseFloat($scope.stockadjustmentModel.list[i].overall_mrp_price);
    //                        $scope.stockadjustmentModel.list[i].mrp_price = utilityService.changeCurrency($scope.stockadjustmentModel.list[i].mrp_price, $rootScope.appConfig.thousand_seperator);
                            $scope.stockadjustmentModel.list[i].flag = i;
                            $scope.showDetails[i] = false;

                            for (var j = 0; j < $scope.stockadjustmentModel.list[i].detail.length; j++)
                            {
                                $scope.stockadjustmentModel.list[i].detail[j].flag = i;
                                $scope.stockadjustmentModel.list[i].detail[j].overall_sales_price = parseFloat($scope.stockadjustmentModel.list[i].detail[j].overall_sales_price).toFixed(2);
                                $scope.stockadjustmentModel.list[i].detail[j].overall_purchase_price = parseFloat($scope.stockadjustmentModel.list[i].detail[j].overall_purchase_price).toFixed(2);
                                $scope.stockadjustmentModel.list[i].detail[j].overall_mrp_price = parseFloat($scope.stockadjustmentModel.list[i].detail[j].overall_mrp_price).toFixed(2);
    //                            detailPurchaseAmt = detailPurchaseAmt + parseFloat($scope.stockadjustmentModel.list[i].detail[j].purchase_price);
    //                            $scope.stockadjustmentModel.list[i].detail[j].purchase_price = utilityService.changeCurrency($scope.stockadjustmentModel.list[i].detail[j].purchase_price, $rootScope.appConfig.thousand_seperator);
    //                            detailMrpAmt = detailMrpAmt + parseFloat($scope.stockadjustmentModel.list[i].detail[j].mrp_price);
    //                            $scope.stockadjustmentModel.list[i].detail[j].mrp_price = utilityService.changeCurrency($scope.stockadjustmentModel.list[i].detail[j].mrp_price, $rootScope.appConfig.thousand_seperator);
                            }

                        }


                        $scope.stockadjustmentModel.total = data.total;
                        $scope.stockadjustmentModel.totalSalesAmount = totalSalesAmt;
                        $scope.stockadjustmentModel.totalSalesAmount = parseFloat($scope.stockadjustmentModel.totalSalesAmount).toFixed(2);
                        $scope.stockadjustmentModel.totalSalesAmount = utilityService.changeCurrency($scope.stockadjustmentModel.totalSalesAmount, $rootScope.appConfig.thousand_seperator);

                        $scope.stockadjustmentModel.totalPurchaseAmount = totalPurchaseAmt;
                        $scope.stockadjustmentModel.totalPurchaseAmount = parseFloat($scope.stockadjustmentModel.totalPurchaseAmount).toFixed(2);
                        $scope.stockadjustmentModel.totalPurchaseAmount = utilityService.changeCurrency($scope.stockadjustmentModel.totalPurchaseAmount, $rootScope.appConfig.thousand_seperator);

                        $scope.stockadjustmentModel.totalMrpAmount = totalMrpAmt;
                        $scope.stockadjustmentModel.totalMrpAmount = parseFloat($scope.stockadjustmentModel.totalMrpAmount).toFixed(2);
                        $scope.stockadjustmentModel.totalMrpAmount = utilityService.changeCurrency($scope.stockadjustmentModel.totalMrpAmount, $rootScope.appConfig.thousand_seperator);
                    }
                    $scope.stockadjustmentModel.isLoadingProgress = false;
                    $scope.stockadjustmentModel.isSearchLoadingProgress = false;
                });
            }
            else
            {
                $scope.stockadjustmentModel.isLoadingProgress = false;
                $scope.stockadjustmentModel.isSearchLoadingProgress = false;
            }

        };
        $scope.getProductList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            return $httpService.get(APP_CONST.API.PRODUCT_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatProductModel = function (model)
        {
            if (model != null)
            {
                return model.name;
            }
            return  '';
        };

        $scope.print = function (div)
        {
           var docHead = document.head.outerHTML;
            var printContents = document.getElementById(div).outerHTML;
            var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=865, height=600, resizable=yes, screenX=200, screenY=200, personalbar=no, scrollbars=yes";

            var newWin = window.open("", "_blank", winAttr);
            var writeDoc = newWin.document;
            writeDoc.open();
            writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
            writeDoc.close();
            newWin.focus();
        };

//        $scope.getList();
    }]);




