
app.controller('stockAdjustmentViewCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', 'sweet', function ($scope, $rootScope, adminService, $httpService, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST, sweet) {

        $scope.orderListModel =
                {
                    "id": 0,
                    "productname": "",
                    "sku": "",
                    "uom_id": "",
                    "qty": "",
                    "isActive": "1",
                    "taxInfo": "",
                    list: [],
                    "uomInfo": {},
                    "productList": [],
                    "productId": '',
                    adjustmentlist: [],
                    "attachments": []

                };
        $scope.onFocus = function (e) {
            $timeout(function () {
                $(e.target).trigger('input');
            });
        };
        $scope.showItems = false;
        $scope.showAreaPopup = function ()
        {
            $scope.showItems = true;
        }

        $scope.validationFactory = ValidationFactory;
        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function ()
        {
            if (typeof $scope.stock_adj_edit_form != 'undefined' && typeof $scope.stock_adj_edit_form.$pristine != 'undefined' && !$scope.stock_adj_edit_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function () {

            $scope.stock_adj_edit_form.$setPristine();
            $scope.orderListModel.id = "";
            $scope.orderListModel.productname = "";
            $scope.orderListModel.product = "";
            $scope.orderListModel.qty = '';
            $scope.orderListModel.purchase_price = '';
            $scope.orderListModel.rowtotal = "";
            $scope.orderListModel.productId = "";
            $scope.orderListModel.uom_id = "";
            $scope.orderListModel.sku = "";
            $scope.orderListModel.product_id = "";
            $scope.orderListModel.taxInfo = "";
            $scope.orderListModel.uomInfo = "";
            $scope.orderListModel.length = "";
            $scope.orderListModel.isActive = true;
        }
        $scope.currencyFormat = $rootScope.appConfig.currency;
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.initTableFilterTimeoutPromise = null;
        $scope.deletedProductList = [];
        $scope.undeletedProductList = [];
        $scope.showbillPopup = false;
        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }

        $scope.getUomList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            return $httpService.get(APP_CONST.API.UOM, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatUomModel = function (model)
        {
            if (model != null)
            {
                return model.name;
            }
            return  '';
        };
//        $scope.isLoadedTax = false;
//        $scope.getTaxListInfo = function ()
//        {
//            $scope.isLoadedTax = false;
//            var getListParam = {};
//            getListParam.id = "";
//            getListParam.name = "";
//            getListParam.start = 0;
//            getListParam.is_active = 1;
//            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
//            adminService.getTaxList(getListParam, configOption).then(function (response)
//            {
//                var data = response.data.list;
//                $scope.orderListModel.taxInfo = data;
//                $scope.orderListModel.tax_id = $scope.orderListModel.taxInfo[0].id + '';
//                $scope.isLoadedTax = true;
//            });
//        };
        $scope.deleteEvent = false;
        $scope.deleteProduct = function (id)
        {
            var index = -1;
            for (var i = 0; i < $scope.orderListModel.list.length; i++) {
                if (id == $scope.orderListModel.list[i].id) {
                    index = i;
                    break;
                }
            }
            $scope.orderListModel.list.splice(index, 1);
            $scope.deleteEvent = true;
            // $scope.updateStockadjustmentTotal();
        };
        $scope.totalcalculationTimeoutPromise != null
        $scope.updateStockadjustmentTotal = function ()
        {
            if ($scope.totalcalculationTimeoutPromise != null)
            {
                $timeout.cancel($scope.totalcalculationTimeoutPromise);
            }
            $scope.totalcalculationTimeoutPromise = $timeout($scope.calculatetotal, 300);
        }

        $scope.initUpdateDetailTimeoutPromise = null;
        $scope.initUpdateDetail = function ()
        {
            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
                $scope.getCustomerList();
            }
            if ($stateParams.id != "")
            {
                $scope.updateStockadjustmentDetails();
            } else
            {
                $scope.initUpdateDetailTimeoutPromise = $timeout($scope.getCustomerList, 300);
                //$scope.getCustomerList();
            }

        }
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.getStockadjustmentListInfo = function () {

            // $scope.orderListModel.isLoadingProgress = true;
            var materialListParam = {};
            var headers = {};
            headers['screen-code'] = 'stockadjustment';
            materialListParam.type = 'stock_adjust';
            materialListParam.is_active = 1;
            materialListParam.id = $stateParams.id;
            $scope.orderListModel.isLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getNewStockAdjustmentDetail(materialListParam, configOption, headers).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.orderListModel.adjustmentlist = data.data[0];
                    $scope.orderListModel.total = data.total;
                    $scope.updateStockadjustmentDetails();
                }
                $scope.orderListModel.isLoadingProgress = false;
            });
        };

        $scope.updateStockadjustmentDetails = function ()
        {
            if ($scope.orderListModel.adjustmentlist != '' && $scope.orderListModel.adjustmentlist != null)
            {
                $scope.orderListModel.reason = $scope.orderListModel.adjustmentlist.reason;
                $scope.orderListModel.userInfo = {
                    id: $scope.orderListModel.adjustmentlist.user_id,
                    f_name: $scope.orderListModel.adjustmentlist.user_name
                }
                $scope.orderListModel.stockAdj_no = $scope.orderListModel.adjustmentlist.auto_no;
                $scope.orderListModel.overall_purchase_price = $scope.orderListModel.adjustmentlist.overall_purchase_price;
                $scope.orderListModel.overall_sales_price = $scope.orderListModel.adjustmentlist.overall_sales_price;
                $scope.orderListModel.overall_qty = $scope.orderListModel.adjustmentlist.overall_qty;
                $scope.orderListModel.overall_mrp_price = $scope.orderListModel.adjustmentlist.overall_mrp_price;
                var idate = utilityService.parseStrToDate($scope.orderListModel.adjustmentlist.date);
                $scope.orderListModel.date = $filter('date')(idate, 'dd/MM/yyyy');
                $scope.orderListModel.attachments = $scope.orderListModel.adjustmentlist.attachment;
                if (typeof $scope.orderListModel.attachments != "undefined" && $scope.orderListModel.attachments.length > 0)
                {
                    for (var i = 0; i < $scope.orderListModel.attachments.length; i++)
                    {
                        $scope.orderListModel.attachments[i].path = $scope.orderListModel.attachments[i].url;
                    }
                }
                for (var i = 0; i < $scope.orderListModel.adjustmentlist.detail.length; i++)
                {
                    var newRow = {
                        "id": $scope.orderListModel.adjustmentlist.detail[i].product_id,
                        "code": $scope.orderListModel.adjustmentlist.detail[i].barcode,
                        "name": $scope.orderListModel.adjustmentlist.detail[i].product_name,
                        "purchase_price": $scope.orderListModel.adjustmentlist.detail[i].purchase_price,
                        "mrp_price": $scope.orderListModel.adjustmentlist.detail[i].mrp_price,
                        "sales_price": $scope.orderListModel.adjustmentlist.detail[i].sales_price,
                        "qty": $scope.orderListModel.adjustmentlist.detail[i].qty,
                        "rowtotal": $scope.orderListModel.adjustmentlist.detail[i].total_price,
                        "new": false
                    }
                    $scope.orderListModel.list.push(newRow);
                    $scope.orderListModel.list[i].productname = newRow;
                }
                $scope.orderListModel.isLoadingProgress = false;
            }
        }

        $scope.calculatetotal = function ()
        {
            var subTotal = 0.00;
            var totalQty = 0.00;
            var totalMrpAmount = 0.00;
            $scope.orderListModel.totalQty = 0.00;
            $scope.orderListModel.subtotal = 0.00;
            $scope.orderListModel.total_mrp = 0.00;
            for (var i = 0; i < $scope.orderListModel.list.length; i++)
            {
                if ($scope.orderListModel.list[i].id != '' && $scope.orderListModel.list[i].id != 'undefined' && $scope.orderListModel.list[i].id != null)
                {
                    totalQty = parseFloat(totalQty) + parseFloat($scope.orderListModel.list[i].qty);
                    $scope.orderListModel.list[i].rowtotal = $scope.orderListModel.list[i].purchase_price * $scope.orderListModel.list[i].qty;
                    subTotal += parseFloat($scope.orderListModel.list[i].rowtotal);
                    $scope.orderListModel.list[i].rowtotal = $scope.orderListModel.list[i].rowtotal.toFixed(2);
                    totalMrpAmount = parseFloat($scope.orderListModel.list[i].mrp_price) * parseFloat($scope.orderListModel.list[i].qty);
                }
            }
            $scope.orderListModel.totalQty = totalQty;
            $scope.orderListModel.subtotal = parseFloat(subTotal).toFixed(2);
            $scope.orderListModel.total_mrp = parseFloat(totalMrpAmount).toFixed(2);
        }

        $scope.updateProductInfo = function (model, index)
        {
            if ($scope.orderListModel.list.length != 0)
            {
                if (typeof model == 'undefined')
                    return;
                $scope.orderListModel.list[index].id = model.id;
                $scope.orderListModel.list[index].sku = model.sku;
                $scope.orderListModel.list[index].product = model.name;
                $scope.orderListModel.list[index].purchase_price = model.purchase_price;
                $scope.orderListModel.list[index].qty = 1;
                $scope.orderListModel.list[index].rowtotal = model.purchase_price;
                $scope.orderListModel.list[index].mrp_price = model.mrp_price;
                $scope.orderListModel.list[index].uomInfo = {
                    "name": model.uom,
                    "id": model.uom_id
                }
                $scope.orderListModel.list[index].uomid = model.uom_id;
                $scope.orderListModel.list[index].uom_name = model.uom;
                $scope.orderListModel.list[index].purchase_invoice_id = model.purchase_invoice_id;
                $scope.orderListModel.list[index].purchase_invoice_item_id = model.ref_id;
                $scope.orderListModel.list[index].barcode = model.code;
                $scope.updateStockadjustmentTotal();
                console.log("Index for item edit ", index);
                console.log("List Length ", $scope.orderListModel.list.length);
                $scope.createNewProduct(index);
                return;
            } else
            {
                $scope.createNewProduct(-1);
            }
        }
        $scope.initvalidateProductDataAndAddNewPromise = null;
        $scope.initValidateProductDataAndAddNew = function (index)
        {

            if ($scope.initvalidateProductDataAndAddNewPromise != null)
            {

                $timeout.cancel($scope.initvalidateProductDataAndAddNewPromise);
            }

            $scope.formValidator();
            $scope.initvalidateProductDataAndAddNewPromise = $timeout(
                    function () {
                        $scope.createNewProduct(index);
                    },
                    300);
        };
        $scope.formValidator = function ()
        {
            //performace issue in form validator bug
            if ($scope.stock_adj_edit_form.$submitted)
            {
                $timeout(function () {
                    $scope.stock_adj_edit_form.$submitted = false;
                }, 0);
                $timeout(function () {
                    $scope.stock_adj_edit_form.$setSubmitted();
                }, 100);
            } else
            {
                $timeout(function () {
                    $scope.stock_adj_edit_form.$setSubmitted();
                }, 100);
            }
        }

        $scope.createNewProduct = function (index)
        {
            var formDataError = false;
//            if (index != -1)
//            {
//                var productContainer = window.document.getElementById('invoice_product_container');
//                var errorCell = angular.element(productContainer).find('.has-error').length;
//                if (errorCell > 0)
//                {
//                    formDataError = true;
//                }
//            }

            if (!formDataError)
            {
                if (index == $scope.orderListModel.list.length - 1 && !$scope.deleteEvent)
                {

                    var newRow = {
                        "id": '',
                        "productName": '',
                        "product": '',
                        "purchase_price": '',
                        "qty": '',
                        "mrp_price": '',
                        "rowtotal": '',
                        "discountPercentage": '',
                        "taxPercentage": '',
                        "uom": '',
                        "uomid": '',
                        "custom_opt1": '',
                        "custom_opt2": '',
                        "custom_opt3": '',
                        "custom_opt4": '',
                        "custom_opt5": ''
                    }
                    $scope.orderListModel.list.push(newRow);
                }
                $scope.deleteEvent = false;
            }

        }

        $scope.$on("updateSelectedProductEvent", function (event, selectedItems)
        {
            if (typeof selectedItems == 'undefined')
                return;
            if ($scope.orderListModel.list.length != 0)
            {
                var isDuplicateProduct = false;
                var duplicateProductIndex = -1;
                for (i = 0; i < $scope.orderListModel.list.length; i++)
                {
                    if (selectedItems.id == $scope.orderListModel.list[i].id)
                    {
                        isDuplicateProduct = true;
                        duplicateProductIndex = i;
                    }

                }
                if (isDuplicateProduct)
                {
                    $scope.orderListModel.list[duplicateProductIndex].qty = $scope.orderListModel.list[duplicateProductIndex].qty + 1;
                    console.log("upd 4");
                    $scope.updateStockadjustmentTotal();
                } else
                {
                    var newRow = {
                        "id": selectedItems.id,
                        "productName": selectedItems.name,
                        "sku": selectedItems.sku,
                        "sales_price": selectedItems.sales_price,
                        "qty": 1,
                        "rowtotal": selectedItems.sales_price,
                        "discountPercentage": selectedItems.discountPercentage,
                        "taxPercentage": selectedItems.taxPercentage,
                        "uom": selectedItems.uom,
                        "uomid": selectedItems.uom_id
                    }
                    $scope.orderListModel.list.push(newRow);
                    console.log("upd 2");
                    $scope.updateStockadjustmentTotal();
                    return;
                }
            } else
            {
                var newRow = {
                    "id": selectedItems.id,
                    "productName": selectedItems.name,
                    "sku": selectedItems.sku,
                    "sales_price": selectedItems.sales_price,
                    "qty": 1,
                    "rowtotal": selectedItems.sales_price,
                    "discountPercentage": selectedItems.discountPercentage,
                    "taxPercentage": selectedItems.taxPercentage,
                    "uom": selectedItems.uom,
                    "uomid": selectedItems.uom_id
                }
                $scope.orderListModel.list.push(newRow);
                $scope.updateStockadjustmentTotal();
            }
        });
//        $scope.getProductList = function (val)
//        {
//            $scope.orderListModel.isLoadingProgress = true;
//            var getParam = {};
//            getParam.is_active = 1;
//            getParam.has_inventory = 1;
//            getParam.name = val;
//            return $httpService.get(APP_CONST.API.PRODUCT_LIST, getParam, false).then(function (responseData)
//            {
//                var data = responseData.data.list;
//                var hits = data;
//                if (hits.length > 10)
//                {
//                    hits.splice(10, hits.length);
//                }
//                return hits;
//            });
////            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
////            adminService.getProductList(getParam, configOption).then(function(response) {
////                var data = response.data;
////                $scope.orderListModel.productList = data.list;
////                $scope.orderListModel.isLoadingProgress = false;
////            });
//
//
//        }
//        $scope.getTaxListInfo();

//        $scope.getProductList( );

        $scope.createOrder = function (val)
        {
            if (!$scope.isDataSavingProcess)
            {
                $scope.isDataSavingProcess = true;
                $scope.isSave = false;
                for (var i = 0; i < $scope.orderListModel.list.length; i++)
                {
                    if (i != $scope.orderListModel.list.length - 1)
                    {
                        if ($rootScope.appConfig.uomdisplay)
                        {
                            if ($scope.orderListModel.list[i].productname != '' && $scope.orderListModel.list[i].productname != null &&
                                    $scope.orderListModel.list[i].uomInfo != '' && $scope.orderListModel.list[i].uomInfo != null &&
                                    $scope.orderListModel.list[i].qty != '' && $scope.orderListModel.list[i].qty != null && $scope.orderListModel.list[i].qty > 0)
//                                $scope.orderListModel.list[i].sales_price != '' && $scope.orderListModel.list[i].sales_price != null &&
//                                $scope.orderListModel.list[i].rowtotal != '' && $scope.orderListModel.list[i].rowtotal != null)
                            {
                                $scope.isSave = true;
                            } else
                            {
                                $scope.isSave = false;
                                break;
                            }
                        } else
                        {
                            if ($scope.orderListModel.list[i].productname != '' && $scope.orderListModel.list[i].productname != null &&
                                    $scope.orderListModel.list[i].qty != '' && $scope.orderListModel.list[i].qty != null && $scope.orderListModel.list[i].qty > 0)
//                                $scope.orderListModel.list[i].sales_price != '' && $scope.orderListModel.list[i].sales_price != null &&
//                                $scope.orderListModel.list[i].rowtotal != '' && $scope.orderListModel.list[i].rowtotal != null)
                            {
                                $scope.isSave = true;
                            } else
                            {
                                $scope.isSave = false;
                                break;
                            }
                        }
                    }
                }
                if ($scope.isSave)
                {
                    var createOrderParam = {};
                    var headers = {};
                    headers['screen-code'] = 'stockadjustment';
                    createOrderParam.product = [];
                    createOrderParam.reason = $scope.orderListModel.reason;
                    if ($scope.orderListModel.userInfo != undefined && $scope.orderListModel.userInfo != null && $scope.orderListModel.userInfo.id != '')
                    {
                        createOrderParam.user_id = $scope.orderListModel.userInfo.id;
                        createOrderParam.user_name = $scope.orderListModel.userInfo.f_name;
                    } else {
                        createOrderParam.user_id = '';
                        createOrderParam.user_name = '';
                    }
                    if (typeof $scope.orderListModel.date == 'object')
                    {
                        $scope.orderListModel.date = utilityService.parseDateToStr($scope.orderListModel.date, 'yyyy-MM-dd');
                    }
                    createOrderParam.date = utilityService.changeDateToSqlFormat($scope.orderListModel.date, 'yyyy-MM-dd');
                    createOrderParam.type = 'stock_adjust';
                    createOrderParam.overall_qty = $scope.orderListModel.totalQty;
                    createOrderParam.overall_purchase_price = $scope.orderListModel.subtotal;
                    createOrderParam.overall_mrp_price = $scope.orderListModel.total_mrp;
                    createOrderParam.auto_no = $scope.orderListModel.stockAdj_no;
                    createOrderParam.attachment = [];
                    if (typeof $scope.orderListModel.attachments != "undefined" && $scope.orderListModel.attachments.length > 0)
                    {
                        for (var i = 0; i < $scope.orderListModel.attachments.length; i++)
                        {
                            var imageParam = {};
                            imageParam.id = $scope.orderListModel.attachments[i].id;
                            imageParam.url = $scope.orderListModel.attachments[i].url;
                            // imageParam.ref_id = $scope.orderListModel.attachments[i].ref_id;
                            imageParam.type = 'stock_adjust';
                            createOrderParam.attachment.push(imageParam);
                        }
                    }
                    for (var i = 0; i < $scope.orderListModel.list.length - 1; i++)
                    {
                        if ($scope.orderListModel.list[i].sku != "")
                        {
                            var ordereditems = {};
                            ordereditems.comments = 'stockadjustment';
                            if ($scope.orderListModel.list[i].sku != '' && $scope.orderListModel.list[i].sku != null && $scope.orderListModel.list[i].sku != undefined)
                            {
                                ordereditems.product_sku = $scope.orderListModel.list[i].sku;
                            } else {
                                ordereditems.product_sku = '';
                            }
                            ordereditems.barcode = $scope.orderListModel.list[i].barcode;
                            ordereditems.purchase_price = $scope.orderListModel.list[i].purchase_price;
                            ordereditems.mrp_price = $scope.orderListModel.list[i].mrp_price;
                            ordereditems.total_price = $scope.orderListModel.subtotal;
                            ordereditems.purchase_invoice_id = $scope.orderListModel.list[i].purchase_invoice_id;
                            ordereditems.purchase_invoice_item_id = $scope.orderListModel.list[i].purchase_invoice_item_id;
                            ordereditems.product_id = $scope.orderListModel.list[i].id;
                            ordereditems.product_name = $scope.orderListModel.list[i].product;
                            ordereditems.qty = parseFloat($scope.orderListModel.list[i].qty);
                            if ($scope.orderListModel.list[i].uomInfo.id != null && $scope.orderListModel.list[i].uomInfo.id != '')
                            {
                                ordereditems.uom_id = $scope.orderListModel.list[i].uomInfo.id;
                                ordereditems.uom_name = $scope.orderListModel.list[i].uomInfo.name;
                            } else
                            {
                                ordereditems.uom_id = 0;
                                ordereditems.uom_name = '';
                            }
                            createOrderParam.product.push(ordereditems);
                        }
                    }
                    adminService.editNewStockAdjustment(createOrderParam, headers).then(function (response) {
                        if (response.data.success == true)
                        {
                            $scope.formReset();
                            $state.go('app.stockadjustment');
                        }
                        $scope.isDataSavingProcess = false;
                    });
                } else
                {
                    sweet.show('Oops...', 'Fill Product Detail..', 'error');
                    $scope.isDataSavingProcess = false;
                }
            }
        }

        $scope.getProductList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.PRODUCT_BASED_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatProductModel = function (model, index)
        {
            if (model != null && model != 'undefined' && model != '')
            {
                $scope.updateProductInfo(model, index);
                if (model.code != '' && model.code != null && model.code != 'undefined')
                {
                    return model.code;
                }
            }
            return  '';
        }
        $scope.showUploadMoreFilePopup = false;
        $scope.showPhotoGalaryPopup = false;
        $scope.showPopup = function (index, attachindex)
        {
            if (index == 'morefileupload')
            {
                $scope.showUploadMoreFilePopup = true;
            } else if (index === 'photogalary')
            {
                $scope.showPhotoGalaryPopup = true;
            }
        }
        $scope.closePopup = function (index)
        {
            if (index == 'morefileupload')
            {
                $scope.showUploadMoreFilePopup = false;
            } else if (index === 'photogalary')
            {
                $scope.showPhotoGalaryPopup = false;
            }
        }

        $scope.uploadedFileQueue = [];
        $scope.uploadedFileCount = 0;
        $scope.isImageSavingProcess = true;
        $scope.$on("updateFileUploadEvent", updateFileUploadEventHandler);
        function updateFileUploadEventHandler(event, data, category, imgcaption, index)
        {
            $scope.isImageSavingProcess = true;
            $scope.uploadedFileQueue = data;
            if (typeof category != undefined && category != null && category != '')
            {
                $scope.uploadedFileQueue[index].category = category;
            }
            if (typeof imgcaption != undefined && imgcaption != null && imgcaption != '')
            {
                $scope.uploadedFileQueue[index].imgcaption = imgcaption;
            }
            console.log('$scope.uploadedFileQueue');
            console.log($scope.uploadedFileQueue);
            $scope.validateFileUploadStatus();
        }

        $scope.$on("deletedUploadFileEvent", deletedUploadFileEventHandler);
        function deletedUploadFileEventHandler(event, index)
        {
            if ($scope.showUploadMoreFilePopup)
            {
                if ($scope.orderListModel.attachments.length > index)
                {
                    $scope.orderListModel.attachments.splice(index, 1);
                }
            }
        }

        $scope.validateFileUploadStatus = function ()
        {
            $scope.isImageSavingProcess = true;
            var hasUploadCompleted = true;
            for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
            {
                if (!$scope.uploadedFileQueue[i].isSuccess)
                {
                    hasUploadCompleted = false;
                    $scope.$broadcast('genericErrorEvent', 'Image upload is in progress.');
                    break;
                }
            }
            if (hasUploadCompleted && $scope.uploadedFileQueue.length > 0)
            {
                $scope.uploadedFileCount = $scope.uploadedFileQueue.length;
                $scope.isImageSavingProcess = false;
                $scope.isImageUploadComplete = true;
            }

        }
        $scope.deleteImage = function ($index)
        {
            $scope.orderListModel.attachments.splice($index, 1);
        }

        $scope.saveImagePath = function ()
        {
            if ($scope.uploadedFileQueue.length == 0)
            {
                if ($scope.showUploadMoreFilePopup)
                {
                    $scope.closePopup('morefileupload');
                }
            }
            var hasUploadCompleted = true;
            for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
            {
                if (!$scope.uploadedFileQueue[i].isSuccess)
                {
                    hasUploadCompleted = false;
                    $scope.$broadcast('genericErrorEvent', 'Image upload is in progress.');
                    break;
                }
            }
            if (hasUploadCompleted && $scope.uploadedFileQueue.length > 0)
            {
                $scope.orderListModel.attachments = [];
                $scope.uploadedFileCount = $scope.uploadedFileQueue.length;
                $scope.isImageSavingProcess = true;
                $scope.isImageUploadComplete = true;
                for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
                {
                    //               $scope.orderListModel.attachments.push($scope.uploadedFileQueue[i]);
                    var imageUpdateParam = {};
                    //imageUpdateParam.id = 0;
                    imageUpdateParam.url = $scope.uploadedFileQueue[i].urlpath;
                    //imageUpdateParam.ref_id = $stateParams.id;
                    imageUpdateParam.type = 'stock_adjust';
                    imageUpdateParam.comments = '';
                    imageUpdateParam.signedUrl = $scope.uploadedFileQueue[i].signedUrl;
                    imageUpdateParam.path = $scope.uploadedFileQueue[i].signedUrl;
                    $scope.orderListModel.attachments.push(imageUpdateParam);
                    if (i == $scope.uploadedFileQueue.length - 1)
                    {
                        $scope.closePopup('morefileupload');
                    }
                }
            } else
            {
                $scope.closePopup('morefileupload');
            }
        };
        $scope.getUserList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            return $httpService.get(APP_CONST.API.GET_USER_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatUserModel = function (model)
        {
            if (model != null)
            {
                return model.f_name;
            }
            return  '';
        };
        $scope.getStockadjustmentListInfo();
    }]);




