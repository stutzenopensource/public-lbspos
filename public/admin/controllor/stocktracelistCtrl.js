app.controller('stocktracelistCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', 'APP_CONST', '$window', '$httpService', '$filter', 'Auth', '$state', '$timeout', 'ValidationFactory', function ($scope, $rootScope, adminService, utilityService, APP_CONST, $window, $httpService, $filter, Auth, $state, $timeout, ValidationFactory) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.stocktracelistModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
            id: '',
            companyname: '',
            companyno: '',
            companycode: '',
            barcode: '',
            companyphone: '',
            companymail: '',
//             sku: '',

            serverList: null,
            isLoadingProgress: false,
            isSearchLoadingProgress: false

        };
        $scope.adminService = adminService;
        $scope.pagePerCount = [10, 20, 50, 100];
        $scope.stocktracelistModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.validationFactory = ValidationFactory;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        //$scope.dateFormat = 'dd-MM-yyyy';
        $scope.currentDate = new Date();
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)

        $scope.openDate = function (index) {

            if (index == 0)
            {
                $scope.fromDateOpen = true;
            } else if (index == 1)
            {
                $scope.toDateOpen = true;
            }

        }
        $scope.searchFilter = {
            fromdate: '',
            todate: '',
            barcode: '',
            type: ''
        };
        $scope.searchFilter.fromdate = $scope.currentDate;
        //$scope.searchFilter.fromdate = $filter('date')($scope.currentDate, $scope.dateFormat);
        $scope.clearFilters = function ()
        {
            $scope.searchFilter = {
                fromdate: '',
                todate: '',
                barcode: '',
                productInfo: {}

            };
            $scope.stocktracelistModel.list = [];
            $scope.searchFilter.fromdate = $scope.currentDate;
            //$scope.searchFilter.fromdate = $filter('date')($scope.currentDate, $scope.dateFormat);

        }

        // $scope.avaLangsupport = [{"id": 5, "name": "tamil"}, {"id": 6, "name": "tamil1"}, {"id": 7, "name": "tamil2"}];

        $scope.searchFilterValue = "";
        $scope.initTableFilterTimeoutPromise = null;
        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.validateDateFilterData = function ()
        {
            var retVal = false;
            if ($scope.searchFilter.todate !== '' || $scope.searchFilter.fromdate !== '')
            {
                retVal = true;
            }
            return retVal;
        };
//        $scope.showSaveShoplistPopup = false;
//        $scope.showPopup = function() {
//
//            $scope.showSaveShoplistPopup = true;
//            $scope.shoplistModel.items = '';
//            $scope.shoplistModel.shopitemamt = '';
//        };
//        $scope.closePopup = function() {
//
//            $scope.showSaveShoplistPopup = false;
//        };
//        $scope.issaveNameLoadingProgress = false;
//        $scope.saveName = function() {
//
//            var saveNameParam = {};
//
//            saveNameParam.productname = $scope.stocktracelistModel.id;
//            saveNameParam.uom = $scope.stocktracelistModel.product_id;
//            saveNameParam.description = $scope.stocktracelistModel.sku;
//            saveNameParam.quantity = $scope.stocktracelistModel.uom_name;
//            saveNameParam.stockvalue = $scope.stocktracelistModel.total_stock;
//
//            $scope.issaveNameLoadingProgress = true;
//            adminService.saveName(saveNameParam).then(function(response) {
//                var data = response.data.list;
//                if (data.success == true)
//                {
//                    $scope.issaveNameLoadingProgress = false;
//                    $scope.closePopup();
//                    $state.go('app.stocktracelist');
//                    //$scope.getList();
//                }
//
//            });
//        };
//        $scope.$watch('searchFilter.productInfo', function (newVal, oldVal)
//        {
//            if (typeof newVal === 'undefined' || newVal === '')
//            {
////            if ($scope.searchFilter.productInfo != null && typeof $scope.searchFilter.productInfo != 'undefined' && typeof $scope.searchFilter.productInfo.id != 'undefined')
//                {
//                    $scope.initTableFilter();
//                }
//            }
//        });
        $scope.formatproductlistModel = function (model) {
            if (model != null)
            {
                return model.name;
            }
            return  '';
        };


        $scope.getproductlist = function (val) {

            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.type = "Product";
            autosearchParam.is_active=1;
            return $httpService.get(APP_CONST.API.PRODUCT_LIST, autosearchParam, false).then(function (responseData) {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.getList = function ()
        {
            var headers = {};
            headers['screen-code'] = 'stocktrace';
            var getListParam = {};
            //getListParam.fromDate = $scope.searchFilter.fromdate;
            //getListParam.toDate = $scope.searchFilter.todate;

            if ($scope.searchFilter.todate != '' && ($scope.searchFilter.fromdate == null || $scope.searchFilter.fromdate == ''))
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
                getListParam.from_date = getListParam.to_date;
            } else if ($scope.searchFilter.fromdate != '' && ($scope.searchFilter.todate == null || $scope.searchFilter.todate == ''))
            {
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
                getListParam.to_date = getListParam.from_date;
            } else
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
            }
//            getListParam.fromDate = $filter('date')($scope.searchFilter.fromdate, 'yyyy-MM-dd');
//            getListParam.toDate = $filter('date')($scope.searchFilter.todate, 'yyyy-MM-dd');
//            if (getListParam.fromDate === '' || getListParam.toDate === '')
//            {
//                if (getListParam.fromDate !== '')
//                {
//                    getListParam.toDate = getListParam.fromDate;
//                }
//                if (getListParam.toDate !== '')
//                {
//                    getListParam.fromDate = getListParam.toDate;
//                }
//            }
            getListParam.barcode = $scope.searchFilter.barcode;
            getListParam.is_active = 1;
            getListParam.stocktracelist = $scope.searchFilter.stocktracelist;
            if ($scope.searchFilter.productInfo != null && typeof $scope.searchFilter.productInfo != 'undefined' && typeof $scope.searchFilter.productInfo.id != 'undefined')
            {
                getListParam.product_id = $scope.searchFilter.productInfo.id;
            } else
            {
                getListParam.product_id = '';
            }
            $scope.stocktracelistModel.isLoadingProgress = true;
            $scope.stocktracelistModel.isSearchLoadingProgress = true;
            if (getListParam.product_id != '')
            {
                adminService.getstocktracelist(getListParam, headers).then(function (response) {
                    if (response.data.success === true)
                    {
                        var data = response.data.list;
                        $scope.stocktracelistModel.list = data;
                        $scope.stocktracelistModel.openingBalance = response.data.OpeningStock;
                        $scope.stocktracelistModel.openingBalance.credit = parseFloat($scope.stocktracelistModel.openingBalance.credit_qty).toFixed(2);
                        $scope.stocktracelistModel.openingBalance.debit = parseFloat($scope.stocktracelistModel.openingBalance.debit_qty).toFixed(2);
                        var openingBalance = parseFloat($scope.stocktracelistModel.openingBalance.credit_qty) - parseFloat($scope.stocktracelistModel.openingBalance.debit_qty);
                        $scope.stocktracelistModel.openingBalance.balance = openingBalance;
                        $scope.stocktracelistModel.openingBalance.balance = parseFloat($scope.stocktracelistModel.openingBalance.balance).toFixed(2);
                        $scope.stocktracelistModel.openingAmount = utilityService.changeCurrency($scope.stocktracelistModel.openingBalance.balance, $rootScope.appConfig.thousand_seperator);
                        $scope.stocktracelistModel.openingBalance.balance = parseFloat($scope.stocktracelistModel.openingBalance.balance).toFixed(2);
                        $scope.stocktracelistModel.openingCredit = utilityService.changeCurrency($scope.stocktracelistModel.openingBalance.credit, $rootScope.appConfig.thousand_seperator);
                        $scope.stocktracelistModel.openingDebit = utilityService.changeCurrency($scope.stocktracelistModel.openingBalance.debit, $rootScope.appConfig.thousand_seperator);

                        if ( $scope.stocktracelistModel.list.length != 0 ) {
                            $scope.updateBalanceQty();
                            for (var i = 0; i < $scope.stocktracelistModel.list.length; i++)
                            {
                                $scope.stocktracelistModel.list[i].newdate = utilityService.parseStrToDate($scope.stocktracelistModel.list[i].fromdate);
                                $scope.stocktracelistModel.list[i].fromdate = utilityService.parseDateToStr($scope.stocktracelistModel.list[i].newdate, $scope.dateFormat);
                                $scope.stocktracelistModel.list[i].balance = parseFloat($scope.stocktracelistModel.list[i].balance).toFixed(2);
                                $scope.stocktracelistModel.list[i].quantity_debit = utilityService.changeCurrency($scope.stocktracelistModel.list[i].quantity_debit, $rootScope.appConfig.thousand_seperator);
                                $scope.stocktracelistModel.list[i].quantity_credit = utilityService.changeCurrency($scope.stocktracelistModel.list[i].quantity_credit, $rootScope.appConfig.thousand_seperator);
                            
                            }
                        }
                        $scope.stocktracelistModel.total = data.total;
                    }
                    $scope.stocktracelistModel.isLoadingProgress = false;
                    $scope.stocktracelistModel.isSearchLoadingProgress = false;
                });
            } else {
                $scope.stocktracelistModel.isLoadingProgress = false;
                $scope.stocktracelistModel.isSearchLoadingProgress = false;
            }

        };
        $scope.print = function ()
        {
            $window.print();
        };
        
        $scope.updateBalanceQty = function ()
        {
            if ($scope.stocktracelistModel.list.length >= 1)
            {
                var openingQty = parseFloat($scope.stocktracelistModel.openingBalance.credit_qty) - parseFloat($scope.stocktracelistModel.openingBalance.debit_qty);
                for (var i = 0; i < $scope.stocktracelistModel.list.length; i++)
                {
                    //var openingQty = parseFloat($scope.stocktracelistModel.list[0].quantity_credit) + parseFloat($scope.stocktracelistModel.list[0].quantity_debit);
                    var quantity_credit = $scope.stocktracelistModel.list[i].quantity_credit == null ? 0 : parseFloat($scope.stocktracelistModel.list[i].quantity_credit);
                    var quantity_debit = $scope.stocktracelistModel.list[i].quantity_debit == null ? 0 : parseFloat($scope.stocktracelistModel.list[i].quantity_debit);
                    if (quantity_credit != 0)
                    {
                        openingQty = openingQty + quantity_credit;
                    } else if (quantity_debit != 0)
                    {
                        openingQty = openingQty - quantity_debit;
                    }
                    $scope.stocktracelistModel.list[i].balance = openingQty;
                }

            } 
//            else
//            {
//                $scope.stocktracelistModel.totalbalance = 0.00;
//            }
        };

        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'stocktrace_form';


        $scope.$on('onLocalStorageFormRenterCompleteEvent', function (event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });

        $scope.$on('onLocalStorageFieldRenterCompleteEvent', function (event, formName) {

            if ($scope.localStorageFormName == formName)
            {
                $scope.localStorageRenterCount++;
            }
            if ($scope.localStorageRenterCount >= $scope.localStorageCount)
            {
                console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
            }
        });
        $scope.$on('updateTypeaheadFieldValue', function (event, data) {

            if (data.fieldName == "stocktrace_form-product_dropdown")
            {
                $scope.searchFilter.productInfo = data.value;
            }
        });



        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function (event, formName) {

            if ($scope.localStorageFormName == formName)
            {
                $scope.localStorageRetrieveCount++;
            }
            if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
            {
                $scope.initTableFilter();
            }
            console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);
        });

        $scope.init = function ()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');
        };

        $scope.$on('onLocalStorageReadyEvent', function () {
            $timeout(function () {
                $scope.init();
            }, 300);
        });

        //      $scope.getList();
    }]);
