app.controller('stockwastageCtrl', ['$scope', '$rootScope', 'adminService', 'ValidationFactory', 'utilityService', '$httpService', 'APP_CONST', '$filter', 'Auth', '$timeout', '$window', function ($scope, $rootScope, adminService, ValidationFactory, utilityService, $httpService, APP_CONST, $filter, Auth, $timeout, $window) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.stockwastageModel = {
            scopeId: "",
            configId: 5,
            type: "",
            from_date: "",
            currentPage: 1,
            total: 0,
            productInfo: '',
            product_id: '',
            id: '',
            limit: 4,
            list: [],
            status: '',
//        currencyFormat:'',
//        serverList: null,
            isLoadingProgress: true
        };
        $scope.searchFilterNameValue = ''
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.stockwastageModel.limit = $scope.pagePerCount[0];
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;

        $scope.validationFactory = ValidationFactory;
        $scope.searchFilter = {
            from_date: '',
            to_date: '',
            id: '',
            stockAdjNo: '',
            reason: ''

        };
        $scope.stockwastageModel.productInfo = "";
//        $scope.searchFilter.from_date = $scope.currentDate;
        // $scope.searchFilter.from_date = $filter('date')($scope.currentDate, $scope.dateFormat);
        $scope.openDate = function (index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            } else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };


        $scope.clearFilters = function ()
        {
            $scope.searchFilter = {
                from_date: '',
                to_date: '',
                id: '',
                stockAdjNo: '',
                reason: ''
            };
            $scope.stockwastageModel.list = [];
            $scope.stockwastageModel.productInfo = "";
            $scope.stockwastage_form.$setPristine();
//            $scope.searchFilter.from_date = $scope.currentDate;
//            $scope.searchFilterValue = "";
//            $scope.searchFilter.from_date = $filter('date')($scope.currentDate, $scope.dateFormat);
            $scope.initTableFilter();
        }

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getStockwastageListInfo, 300);
        }

        $scope.getProductList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;

            return $httpService.get(APP_CONST.API.PRODUCT_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatproductModel = function (model) {

            if (model !== null)
            {
                $scope.searchFilter.id = model.id;
                return model.name;
            }
            return  '';
        };
        $scope.$watch('searchFilter.productInfo', function (newVal, oldVal)
        {
            if (typeof newVal == 'undefined' || newVal == '' || newVal == null)
            {
                $scope.initTableFilter();
            }
        });

        $scope.isdeleteProgress = false;
        $scope.selectedId = '';
        $scope.showdeletePopup = false;
        $scope.showPopup = function (id)
        {
            $scope.selectedId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function ()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteProgress = false;
        };
        $scope.deleteStockItem = function ( )
        {
            if ($scope.isdeleteProgress == false)
            {
                $scope.isdeleteProgress = true;
                var getListParam = {};
                var headers = {};
                headers['screen-code'] = 'stockwastage';
                getListParam.id = $scope.selectedId;
//                getListParam.type = 'stock_wastage';
                adminService.deleteNewStockAdjustment(getListParam, getListParam.id, headers).then(function (response)
                {
                    var data = response.data;
                    if (data.success === true)
                    {
                        $scope.getStockwastageListInfo();
                    }
                    $scope.closePopup();

                    $scope.isdeleteProgress = false;
                });
            }
        };

        $scope.getStockwastageListInfo = function ()
        {
            $scope.validateDateFlag = false;
            if ($scope.searchFilter.from_date != null && typeof $scope.searchFilter.from_date != 'undefined' && $scope.searchFilter.from_date != '' && $scope.searchFilter.to_date != null && typeof $scope.searchFilter.to_date != 'undefined' && $scope.searchFilter.to_date != '')
            {
                var minDateValue = $filter('date')($scope.searchFilter.from_date, 'yyyy-MM-dd');
                var value = $filter('date')($scope.searchFilter.to_date, 'yyyy-MM-dd');
                minDateValue = moment(minDateValue).valueOf();
                value = moment(value).valueOf();
                if (minDateValue < value)
                {
                    $scope.validateDateFlag = true;
                }
            }
            if ($scope.searchFilter.from_date != null && typeof $scope.searchFilter.from_date != 'undefined' && $scope.searchFilter.from_date != '' && $scope.searchFilter.to_date == null || typeof $scope.searchFilter.to_date == 'undefined' || $scope.searchFilter.to_date == '')
            {
                $scope.validateDateFlag = true;
            }
            if ($scope.searchFilter.to_date != null && typeof $scope.searchFilter.to_date != 'undefined' && $scope.searchFilter.to_date != '' && $scope.searchFilter.from_date == null || typeof $scope.searchFilter.from_date == 'undefined' || $scope.searchFilter.from_date == '')
            {
                $scope.validateDateFlag = true;
            }
            if ($scope.searchFilter.to_date == null || typeof $scope.searchFilter.to_date == 'undefined' || $scope.searchFilter.to_date == '' && $scope.searchFilter.from_date == null || typeof $scope.searchFilter.from_date == 'undefined' || $scope.searchFilter.from_date == '')
            {
                $scope.validateDateFlag = true;
            }
            if ($scope.validateDateFlag == true)
            {
                var materialListParam = {};
                var headers = {};
                headers['screen-code'] = 'stockwastage';
                materialListParam.type = 'stock_wastage';
                if ($scope.searchFilter.from_date != null && $scope.searchFilter.from_date != '')
                {
                    if ($scope.searchFilter.from_date != null && typeof $scope.searchFilter.from_date == 'object')
                    {
                        materialListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.from_date, $scope.adminService.appConfig.date_format);
                    }
                    materialListParam.from_date = utilityService.changeDateToSqlFormat(materialListParam.from_date, $scope.adminService.appConfig.date_format);
                }
                if ($scope.searchFilter.to_date != null && $scope.searchFilter.to_date != '')
                {
                    if ($scope.searchFilter.to_date != null && typeof $scope.searchFilter.to_date == 'object')
                    {
                        materialListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.to_date, $scope.adminService.appConfig.date_format);
                    }
                    materialListParam.to_date = utilityService.changeDateToSqlFormat(materialListParam.to_date, $scope.adminService.appConfig.date_format);
                }
                materialListParam.auto_no = $scope.searchFilter.stockAdjNo;
                materialListParam.reason = $scope.searchFilter.reason;
                materialListParam.is_active = 1;
                materialListParam.start = ($scope.stockwastageModel.currentPage - 1) * $scope.stockwastageModel.limit;
                materialListParam.limit = $scope.stockwastageModel.limit;
                $scope.stockwastageModel.isLoadingProgress = true;
                var configOption = adminService.handleOnlyErrorResponseConfig;
                adminService.getNewStockAdjustmentList(materialListParam, configOption, headers).then(function (response)
                {
                    if (response.data.success === true)
                    {
                        var data = response.data;
                        $scope.stockwastageModel.list = data.list;
                        for (var i = 0; i < $scope.stockwastageModel.list.length; i++)
                        {
                            $scope.stockwastageModel.list[i].newdate = utilityService.parseStrToDate($scope.stockwastageModel.list[i].date);
                            $scope.stockwastageModel.list[i].date = utilityService.parseDateToStr($scope.stockwastageModel.list[i].newdate, $scope.adminService.appConfig.date_format);
                        }
                        $scope.stockwastageModel.total = data.total;
                    }
                    $scope.stockwastageModel.isLoadingProgress = false;
                });
            }
            else
            {
                $scope.stockwastageModel.isLoadingProgress = false;
            }
        };

        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'stockwastage_form';


        $scope.$on('onLocalStorageFormRenterCompleteEvent', function (event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });

        $scope.$on('onLocalStorageFieldRenterCompleteEvent', function (event, formName) {

            if ($scope.localStorageFormName == formName)
            {
                $scope.localStorageRenterCount++;
            }
            if ($scope.localStorageRenterCount >= $scope.localStorageCount)
            {
                console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
            }
        });
        $scope.$on('updateTypeaheadFieldValue', function (event, data) {

            if (data.fieldName == "stockwastage_form-product_dropdown")
            {
                $scope.stockwastageModel.productInfo = data.value;
            }
        });



        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function (event, formName) {

            if ($scope.localStorageFormName == formName)
            {
                $scope.localStorageRetrieveCount++;
            }
            if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
            {
                $scope.initTableFilter();
            }
            console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);
        });

        $scope.init = function ()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');
        };

        $scope.$on('onLocalStorageReadyEvent', function () {
            $timeout(function () {
                $scope.init();
            }, 300);
        });
        $scope.showOptions = false;
        $scope.openOptions = function (index)
        {
            for (var i = 0; i < $scope.stockwastageModel.list.length; i++)
            {
                $scope.stockwastageModel.list[i].show = false;
                var element = "#highlight_" + i;
                $(element).removeClass("highlight");
                var popup = "#popup_" + i;
                $(popup).hide();
            }
            var selectedElement = "#highlight_" + index;
            $(selectedElement).addClass("highlight");
            var selectedPopup = "#popup_" + index;
            $(selectedPopup).show();
            $scope.stockwastageModel.list[index].show = true;
            $scope.showOptions = true;
        }
        $scope.closeOptions = function (index)
        {
            for (var i = 0; i < $scope.stockwastageModel.list.length; i++)
            {
                $scope.stockwastageModel.list[i].show = false;
            }
            $scope.stockwastageModel.list[index].show = false;
            $scope.showOptions = false;
        }
        //$scope.getStockwastageListInfo();

    }]);




