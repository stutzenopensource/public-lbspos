
app.controller('stockwastageEditCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', 'sweet', function ($scope, $rootScope, adminService, $httpService, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST, sweet) {

        $scope.orderListModel =
                {
                    "id": 0,
                    "productname": "",
                    "sku": "",
                    "uom_id": "",
                    "qty": "",
                    "isActive": "1",
                    "taxInfo": "",
                    list: [],
                    "uomInfo": {},
                    "employeeInfo": {},
                    "productList": [],
                    "productId": '',
                    date: '',
                    comments: '',
                    wastageList: [],
                    reason: '',
                    stockAdj_no: ''

                };

        $scope.onFocus = function (e) {
            $timeout(function () {
                $(e.target).trigger('input');
            });
        };

        $scope.showItems = false;
        $scope.showAreaPopup = function ()
        {
            $scope.showItems = true;
        }

        $scope.validationFactory = ValidationFactory;
        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function ()
        {
            if (typeof $scope.stock_wastage_edit_form != 'undefined' && typeof $scope.stock_wastage_edit_form.$pristine != 'undefined' && !$scope.stock_wastage_edit_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function () {

            $scope.stock_wastage_edit_form.$setPristine();
            $scope.orderListModel.id = "";
            $scope.orderListModel.productname = "";
            $scope.orderListModel.qty = '';
            $scope.orderListModel.productList = "";
            $scope.orderListModel.productId = "";
            $scope.orderListModel.uom_id = "";
            $scope.orderListModel.sku = "";
            $scope.orderListModel.product_id = "";
            $scope.orderListModel.taxInfo = "";
            $scope.orderListModel.uomInfo = "";
            $scope.orderListModel.length = "";
            $scope.orderListModel.isActive = true;
        }
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.initTableFilterTimeoutPromise = null;
        $scope.deletedProductList = [];
        $scope.undeletedProductList = [];
        $scope.showbillPopup = false;
        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }

        $scope.getUomList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;

            return $httpService.get(APP_CONST.API.UOM, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatUomModel = function (model)
        {
            if (model != null)
            {
                return model.name;
            }
            return  '';
        };

        $scope.startDateOpen = false;
        $scope.openDate = function (index)
        {
            if (index == 0)
            {
                $scope.startDateOpen = true;
            }
        };

//        $scope.isLoadedTax = false;
//        $scope.getTaxListInfo = function ( )
//        {
//            $scope.isLoadedTax = false;
//            var getListParam = {};
//            getListParam.id = "";
//            getListParam.name = "";
//            getListParam.start = 0;
//            getListParam.is_active = 1;
//            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
//            adminService.getTaxList(getListParam, configOption).then(function (response)
//            {
//                var data = response.data.list;
//                $scope.orderListModel.taxInfo = data;
//                $scope.orderListModel.tax_id = $scope.orderListModel.taxInfo[0].id + '';
//                $scope.isLoadedTax = true;
//            });
//        };
        $scope.showWastagePopup = false;
        $scope.productIndex = '';
        $scope.showStockWastagePopup = function (index) {
            if ($scope.orderListModel.list[index].id != '' && $scope.orderListModel.list[index].id != undefined && $scope.orderListModel.list[index].isNew == false) {
                $scope.productIndex = index;
                $scope.showWastagePopup = true;
            } else {
                $scope.orderListModel.list.splice(index, 1);
                $scope.count--;
                if($scope.orderListModel.list.length >= $scope.orderListModel.wastageList.detail.length )
                {
                    var element = document.getElementById("btnLoad");
                    element.classList.remove("disabled");
                }
                else
                {
                    var element = document.getElementById("btnLoad");
                    element.classList.add("disabled");
                }
                $scope.deleteEvent = true;
                $scope.updateStockwastageTotal();

            }
        }

        $scope.closeWastagePopup = function () {

            $scope.showWastagePopup = false;
            $scope.orderListModel.comments = '';
        }

//        $scope.deleteProduct = function (id)
//        {
//            var index = -1;
//            for (var i = 0; i < $scope.orderListModel.list.length; i++) {
//                if (id == $scope.orderListModel.list[i].id) {
//                    index = i;
//                    break;
//                }
//            }
//            $scope.orderListModel.list.splice(index, 1);
//            $scope.updateStockwastageTotal();
//        };
        $scope.deleteEvent = false;
        $scope.deleteProduct = function (index)
        {
            if ($scope.orderListModel.comments != '') {
                $scope.orderListModel.list[index].is_active = 0;
                var getListParam = {};
                getListParam.id = $scope.orderListModel.list[index].detail_id;
                getListParam.comments = $scope.orderListModel.comments;
                adminService.deleteStockAdjs(getListParam, getListParam.id, getListParam.comments, "").then(function(response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.count--;
                        if ($scope.orderListModel.list.length > 0)
                        {
                            var found = false;
                            for (var i = 0; i < $scope.orderListModel.list.length; i++)
                            {
                                if ($scope.orderListModel.list[i].is_active == 1)
                                {
                                    found = true;
                                }
                            }
                            if (found == true)
                            {
                                var element = document.getElementById("btnLoad");
                                element.classList.remove("disabled");
                            } else
                            {
                                var element = document.getElementById("btnLoad");
                                element.classList.add("disabled");
                            }
                        }
                    }
                });
                $scope.closeWastagePopup();
                $scope.updateStockwastageTotal();
            }

        };
//        $scope.deleteProduct = function (index)
//        {
//            if ($scope.orderListModel.comments != '') {
//
//                for (var i = 0; i < $scope.orderListModel.list.length; i++) {
//                    if (index == i) {
//                        $scope.orderListModel.list[index].is_active = 0;
//                        $scope.orderListModel.list[index].comments = $scope.orderListModel.comments;
//                    }
////                    else {
////                        $scope.orderListModel.list[index].is_active = 1;
////                        $scope.orderListModel.list[index].comments = '';
////                    }
//                }
//                if ($scope.orderListModel.list.length > 0)
//                {
//                        var found = false;
//                        for (var i = 0; i < $scope.orderListModel.list.length; i++)
//                        {
//                            if ($scope.orderListModel.list[i].is_active == 1)
//                            {
//                                found = true;
//                            }
//                        }
//                        if (found == true)
//                        {
//                            var element = document.getElementById("btnLoad");
//                            element.classList.remove("disabled");
//                        }
//                        else
//                        {
//                            var element = document.getElementById("btnLoad");
//                            element.classList.add("disabled");
//                        }
//
//                }
//                $scope.closeWastagePopup();
//                $scope.updateStockwastageTotal();
//            }
//        };

        $scope.totalcalculationTimeoutPromise != null
        $scope.updateStockwastageTotal = function ()
        {
            if ($scope.totalcalculationTimeoutPromise != null)
            {
                $timeout.cancel($scope.totalcalculationTimeoutPromise);
            }
            $scope.totalcalculationTimeoutPromise = $timeout($scope.calculatetotal, 300);
        }

        $scope.initUpdateDetailTimeoutPromise = null;
        $scope.initUpdateDetail = function ()
        {
            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
                $scope.getCustomerList();
            }
            if ($stateParams.id != "")
            {
                $scope.updateStockwastageDetails();
            } else
            {
                $scope.initUpdateDetailTimeoutPromise = $timeout($scope.getCustomerList, 300);
                //$scope.getCustomerList();
            }

        }
        $scope.updateStockwastageDetails = function ()
        {
            if ($scope.orderListModel.wastageList != '' && $scope.orderListModel.wastageList != null)
            {
                $scope.orderListModel.id = $scope.orderListModel.wastageList.id;
                $scope.orderListModel.reason = $scope.orderListModel.wastageList.reason;
                $scope.orderListModel.employeeInfo = {
                    id: $scope.orderListModel.wastageList.emp_id,
                    name: $scope.orderListModel.wastageList.emp_name
                };
                $scope.orderListModel.stockAdj_no = $scope.orderListModel.wastageList.auto_no;
//                var idate = utilityService.parseStrToDate($scope.orderListModel.wastageList.date);
                $scope.orderListModel.date = new Date($scope.orderListModel.wastageList.date);
                $scope.orderListModel.attachments = $scope.orderListModel.wastageList.attachment;
                if (typeof $scope.orderListModel.attachments != "undefined" && $scope.orderListModel.attachments.length > 0)
                {
                    for (var i = 0; i < $scope.orderListModel.attachments.length; i++)
                    {
                        $scope.orderListModel.attachments[i].path = $scope.orderListModel.attachments[i].url;
                    }
                }
                $scope.orderListModel.totalQty = $scope.orderListModel.wastageList.overall_qty;
                $scope.orderListModel.subtotal = $scope.orderListModel.wastageList.overall_purchase_price;
                $scope.orderListModel.total_mrp = $scope.orderListModel.wastageList.overall_mrp_price;
                $scope.count = $scope.orderListModel.wastageList.detail.length;
                for (var i = 0; i < $scope.orderListModel.wastageList.detail.length; i++)
                {
                    var newRow = {
                        "id": $scope.orderListModel.wastageList.detail[i].product_id,
                        "detail_id": $scope.orderListModel.wastageList.detail[i].id,
                        "code": $scope.orderListModel.wastageList.detail[i].barcode,
                        "bcn_id": $scope.orderListModel.wastageList.detail[i].bcn_id,
                        "uomInfo": {
                            id: $scope.orderListModel.wastageList.detail[i].uom_id,
                            name: $scope.orderListModel.wastageList.detail[i].uom_name
                        },
                        "name": $scope.orderListModel.wastageList.detail[i].product_name,
                        "purchase_price": $scope.orderListModel.wastageList.detail[i].purchase_price,
                        "mrp_price": $scope.orderListModel.wastageList.detail[i].mrp_price,
                        "sales_price": $scope.orderListModel.wastageList.detail[i].sales_price,
                        "sku": $scope.orderListModel.wastageList.detail[i].product_sku,
                        "qty": $scope.orderListModel.wastageList.detail[i].qty,
                        "rowtotal": $scope.orderListModel.wastageList.detail[i].total_price,
                        "purchase_invoice_id": $scope.orderListModel.wastageList.detail[i].purchase_invoice_id,
                        "purchase_invoice_item_id": $scope.orderListModel.wastageList.detail[i].purchase_invoice_item_id,
                        "is_active": $scope.orderListModel.wastageList.detail[i].is_active,
                        "comments": $scope.orderListModel.wastageList.detail[i].comments,
                        "isNew": false
                    }
                    $scope.orderListModel.list.push(newRow);
                    $scope.orderListModel.list[i].productname = newRow;
                }
//                var newRow = {
//                    "id": '',
//                    "product": '',
//                    "productName": '',
//                    "purchase_price": '',
//                    "qty": '',
//                    "mrp_price": '',
//                    "rowtotal": '',
//                    "sku": ''
//                }
//                $scope.orderListModel.list.push(newRow);
                $scope.orderListModel.isLoadingProgress = false;
            }
        }

        $scope.calculatetotal = function ()
        {
            var subTotal = 0.00;
            var totalQty = 0.00;
            var salesPrice = 0.00;
            var purchasePrice = 0.00;
            var totalMrpAmount = 0.00;
            $scope.orderListModel.salesPrice = 0.00;
            $scope.orderListModel.purchasePrice = 0.00;
            $scope.orderListModel.totalQty = 0.00;
            $scope.orderListModel.subtotal = 0.00;
            $scope.orderListModel.total_mrp = 0.00;
            for (var i = 0; i < $scope.orderListModel.list.length; i++)
            {
                if ($scope.appConfig.sales_product_list.toUpperCase() == 'PURCHASE BASED')
                {
                    if ($scope.orderListModel.list[i].id != '' && $scope.orderListModel.list[i].id != 'undefined' && $scope.orderListModel.list[i].id != null && $scope.orderListModel.list[i].is_active == 1)
                    {
                        totalQty = parseFloat(totalQty) + parseFloat($scope.orderListModel.list[i].qty);
                        $scope.orderListModel.list[i].rowtotal = $scope.orderListModel.list[i].mrp_price * $scope.orderListModel.list[i].qty;
                        subTotal += parseFloat($scope.orderListModel.list[i].rowtotal);
                        salesPrice = salesPrice + (parseFloat($scope.orderListModel.list[i].sales_price) * parseFloat($scope.orderListModel.list[i].qty))
                        purchasePrice = purchasePrice + (parseFloat($scope.orderListModel.list[i].purchase_price) * parseFloat($scope.orderListModel.list[i].qty))
                        $scope.orderListModel.list[i].rowtotal = $scope.orderListModel.list[i].rowtotal.toFixed(2);
                        totalMrpAmount = parseFloat($scope.orderListModel.list[i].mrp_price) * parseFloat($scope.orderListModel.list[i].qty);
                    }
                } else {
                    if ($scope.orderListModel.list[i].id != '' && $scope.orderListModel.list[i].id != 'undefined' && $scope.orderListModel.list[i].id != null && $scope.orderListModel.list[i].is_active == 1)
                    {
                        totalQty = parseFloat(totalQty) + parseFloat($scope.orderListModel.list[i].qty);
                        $scope.orderListModel.list[i].rowtotal = $scope.orderListModel.list[i].sales_price * $scope.orderListModel.list[i].qty;
                        subTotal += parseFloat($scope.orderListModel.list[i].rowtotal);
                        salesPrice = salesPrice + (parseFloat($scope.orderListModel.list[i].sales_price) * parseFloat($scope.orderListModel.list[i].qty))
                        purchasePrice = purchasePrice + (parseFloat($scope.orderListModel.list[i].purchase_price) * parseFloat($scope.orderListModel.list[i].qty))
                    }
                }
            }
            $scope.orderListModel.salesPrice = parseFloat(salesPrice).toFixed(2);
            $scope.orderListModel.purchasePrice = parseFloat(purchasePrice).toFixed(2);
            $scope.orderListModel.totalQty = totalQty;
            $scope.orderListModel.subtotal = parseFloat(subTotal).toFixed(2);
            $scope.orderListModel.total_mrp = parseFloat(totalMrpAmount).toFixed(2);
        }

        $scope.updateProductInfo = function (model, index)
        {
            if ($scope.orderListModel.list.length != 0)
            {
                if (typeof model == 'undefined')
                    return;

                $scope.orderListModel.list[index].id = model.id;
                $scope.orderListModel.list[index].sku = model.sku;
                $scope.orderListModel.list[index].product = model.name;
                if ($scope.appConfig.sales_product_list.toUpperCase() == 'PURCHASE BASED')
                {
                    if ($scope.orderListModel.list[index].isNew == false)
                    {
                        $scope.orderListModel.list[index].qty = $scope.orderListModel.list[index].qty;
                        $scope.orderListModel.list[index].purchase_price = $scope.orderListModel.list[index].purchase_price;
                        $scope.orderListModel.list[index].sales_price = $scope.orderListModel.list[index].sales_price;
                    } else {
                        $scope.orderListModel.list[index].qty = 1;
                        $scope.orderListModel.list[index].purchase_price = model.purchase_price_w_tax;
                        $scope.orderListModel.list[index].sales_price = model.selling_price;
                    }
                } else {
                    if ($scope.orderListModel.list[index].isNew == false)
                    {
                        $scope.orderListModel.list[index].qty = $scope.orderListModel.list[index].qty;
                        $scope.orderListModel.list[index].sales_price = $scope.orderListModel.list[index].sales_price;
                    } else {
                        $scope.orderListModel.list[index].qty = 1;
                        $scope.orderListModel.list[index].sales_price = model.sales_price;
                    }
                }

                $scope.orderListModel.list[index].mrp_price = model.mrp_price;
                $scope.orderListModel.list[index].uomInfo = {
                    "name": model.uom,
                    "id": model.uom_id
                }
                $scope.orderListModel.list[index].uomid = model.uom_id;
                $scope.orderListModel.list[index].purchase_invoice_id = model.purchase_invoice_id;
                if ($scope.orderListModel.list[index].isNew == false)
                {
                    $scope.orderListModel.list[index].purchase_invoice_item_id = $scope.orderListModel.list[index].purchase_invoice_item_id;
                } else {
                    $scope.orderListModel.list[index].purchase_invoice_item_id = model.purchase_invoice_item_id;
                }
                $scope.orderListModel.list[index].bcn_id = model.bcn_id;
                $scope.orderListModel.list[index].barcode = model.code;
                $scope.updateStockwastageTotal();

                console.log("Index for item edit ", index);
                console.log("List Length ", $scope.orderListModel.list.length);
//                $scope.createNewProduct(index);
                return;
            } else
            {
                $scope.createNewProduct(-1);
            }
        }
        $scope.initvalidateProductDataAndAddNewPromise = null;

        $scope.initValidateProductDataAndAddNew = function (index)
        {
            if ($scope.initvalidateProductDataAndAddNewPromise != null)
            {
                $timeout.cancel($scope.initvalidateProductDataAndAddNewPromise);
            }
            $scope.formValidator();
            $scope.initvalidateProductDataAndAddNewPromise = $timeout(
                    function () {
                        $scope.createNewProduct(index);
                    },
                    300);
        };

        $scope.formValidator = function ()
        {
            //performace issue in form validator bug
            if ($scope.stock_wastage_edit_form.$submitted)
            {
                $timeout(function () {
                    $scope.stock_wastage_edit_form.$submitted = false;
                }, 0);
                $timeout(function () {
                    $scope.stock_wastage_edit_form.$setSubmitted();
                }, 100);
            } else
            {
                $timeout(function () {
                    $scope.stock_wastage_edit_form.$setSubmitted();
                }, 100);
            }
        }

        $scope.createNewProduct = function (index)
        {
            var formDataError = false;
            var productContainer = window.document.getElementById('invoice_product_container');
            var errorCell = angular.element(productContainer).find('.has-error').length;
//            if (errorCell > 0)
//            {
//                formDataError = true;
//            }
            if (!formDataError)
            {
                if (index == $scope.orderListModel.list.length - 1 && !$scope.deleteEvent)
                {

                    var newRow = {
                        "id": '',
                        "productName": '',
                        "sku": '',
                        "purchase_price": '',
                        "mrp_price": '',
                        "qty": '',
                        "rowtotal": '',
                        "discountPercentage": '',
                        "taxPercentage": '',
                        "uom": '',
                        "uomid": '',
                        "custom_opt1": '',
                        "custom_opt2": '',
                        "custom_opt3": '',
                        "custom_opt4": '',
                        "custom_opt5": '',
                        "sales_price": ''
                    }
                    $scope.orderListModel.list.push(newRow);
                }
                $scope.deleteEvent = false;
            }
        }

        $scope.addNewProduct = function ()
        {

            var newRow = {
                "id": '',
                "productName": '',
                "product": '',
                "purchase_price": '',
                "qty": '',
                "mrp_price": '',
                "rowtotal": '',
                "discountPercentage": '',
                "taxPercentage": '',
                "uom": '',
                "uomid": '',
                "custom_opt1": '',
                "custom_opt2": '',
                "custom_opt3": '',
                "custom_opt4": '',
                "custom_opt5": '',
                "sales_price": '',
                "is_active": 1,
                "comments": '',
                "isNew": true
            }
            $scope.orderListModel.list.push(newRow);
            $scope.count++;
            if ($scope.orderListModel.list.length > 0)
            {
                var element = document.getElementById("btnLoad");
                element.classList.remove("disabled");
            }
        }

        $scope.deleteEvent = false;
        $scope.$on("updateSelectedProductEvent", function (event, selectedItems)
        {
            if (typeof selectedItems == 'undefined')
                return;
            if ($scope.orderListModel.list.length != 0)
            {
                var isDuplicateProduct = false;
                var duplicateProductIndex = -1;
                for (i = 0; i < $scope.orderListModel.list.length; i++)
                {
                    if (selectedItems.id == $scope.orderListModel.list[i].id)
                    {
                        isDuplicateProduct = true;
                        duplicateProductIndex = i;
                    }

                }
                if (isDuplicateProduct)
                {
                    $scope.orderListModel.list[duplicateProductIndex].qty = $scope.orderListModel.list[duplicateProductIndex].qty + 1;
                    console.log("upd 4");
                    $scope.updateStockwastageTotal();
                } else
                {
                    var newRow = {
                        "id": selectedItems.id,
                        "productName": selectedItems.name,
                        "sku": selectedItems.sku,
                        "purchase_price": selectedItems.purchase_price,
                        "mrp_price": selectedItems.mrp_price,
                        "qty": 1,
                        "rowtotal": selectedItems.purchase_price,
                        "discountPercentage": selectedItems.discountPercentage,
                        "taxPercentage": selectedItems.taxPercentage,
                        "uom": selectedItems.uom,
                        "uomid": selectedItems.uom_id
                    }
                    $scope.orderListModel.list.push(newRow);
                    console.log("upd 2");
                    $scope.updateStockwastageTotal();
                    return;
                }
            } else
            {
                var newRow = {
                    "id": selectedItems.id,
                    "productName": selectedItems.name,
                    "sku": selectedItems.sku,
                    "purchase_price": selectedItems.purchase_price,
                    "mrp_price": selectedItems.mrp_price,
                    "qty": 1,
                    "rowtotal": selectedItems.purchase_price,
                    "discountPercentage": selectedItems.discountPercentage,
                    "taxPercentage": selectedItems.taxPercentage,
                    "uom": selectedItems.uom,
                    "uomid": selectedItems.uom_id
                }
                $scope.orderListModel.list.push(newRow);
                $scope.updateStockwastageTotal();
            }
        });

//        $scope.getTaxListInfo();

//        $scope.getProductList( );

        $scope.createOrder = function (val)
        {
            if (!$scope.isDataSavingProcess)
            {
                $scope.isDataSavingProcess = true;
                $scope.isSave = false;
                for (var i = 0; i < $scope.orderListModel.list.length; i++)
                {
                    if ($scope.orderListModel.list[i].productname != '' && $scope.orderListModel.list[i].productname != null &&
                            $scope.orderListModel.list[i].qty != '' && $scope.orderListModel.list[i].qty != null && $scope.orderListModel.list[i].qty > 0)
//                                $scope.orderListModel.list[i].sales_price != '' && $scope.orderListModel.list[i].sales_price != null &&
//                                $scope.orderListModel.list[i].rowtotal != '' && $scope.orderListModel.list[i].rowtotal != null)
                    {
                        $scope.isSave = true;
                    } else
                    {
                        $scope.isSave = false;
                        break;
                    }
                }
                if ($scope.isSave)
                {
                    var createOrderParam = {};
                    var headers = {};
                    headers['screen-code'] = 'stockwastage';
                    createOrderParam.product = [];
                    createOrderParam.id = $scope.orderListModel.id;
                    createOrderParam.reason = $scope.orderListModel.reason;
                    createOrderParam.comments = $scope.orderListModel.comments;
                    if ($scope.orderListModel.employeeInfo != undefined && $scope.orderListModel.employeeInfo != null && $scope.orderListModel.employeeInfo.id != '')
                    {
                        createOrderParam.emp_id = $scope.orderListModel.employeeInfo.id;
                        createOrderParam.emp_name = $scope.orderListModel.employeeInfo.name;
                    } else {
                        createOrderParam.emp_id = '';
                        createOrderParam.emp_name = '';
                    }
                    if (typeof $scope.orderListModel.date == 'object')
                    {
                        var date = utilityService.parseDateToStr($scope.orderListModel.date, 'yyyy-MM-dd');
                    }
                    createOrderParam.date = utilityService.changeDateToSqlFormat(date, 'yyyy-MM-dd');
                    createOrderParam.type = 'stock_wastage';
                    createOrderParam.overall_qty = $scope.orderListModel.totalQty;
                    createOrderParam.overall_purchase_price = $scope.orderListModel.purchasePrice;
                    createOrderParam.overall_sales_price = $scope.orderListModel.salesPrice;
                    createOrderParam.overall_mrp_price = $scope.orderListModel.total_mrp;
                    createOrderParam.auto_no = $scope.orderListModel.stockAdj_no;
                    createOrderParam.attachment = [];
                    if (typeof $scope.orderListModel.attachments != "undefined" && $scope.orderListModel.attachments.length > 0)
                    {
                        for (var i = 0; i < $scope.orderListModel.attachments.length; i++)
                        {
                            var imageParam = {};
                            imageParam.id = $scope.orderListModel.attachments[i].id;
                            imageParam.url = $scope.orderListModel.attachments[i].url;
                            // imageParam.ref_id = $scope.orderListModel.attachments[i].ref_id;
                            imageParam.type = 'stock_wastage';
                            createOrderParam.attachment.push(imageParam);
                        }
                    }
                    for (var i = 0; i < $scope.orderListModel.list.length; i++)
                    {
                        var ordereditems = {};
                        //ordereditems.comments = 'stockwastage';
                        ordereditems.bcn_id = $scope.orderListModel.list[i].bcn_id;
                        ordereditems.barcode = $scope.orderListModel.list[i].barcode;
                        ordereditems.purchase_price = $scope.orderListModel.list[i].purchase_price;
                        ordereditems.sales_price = $scope.orderListModel.list[i].sales_price;
                        ordereditems.mrp_price = $scope.orderListModel.list[i].mrp_price;
                        ordereditems.is_active = $scope.orderListModel.list[i].is_active;
                        ordereditems.comments = $scope.orderListModel.list[i].comments;
                        ordereditems.total_price = $scope.orderListModel.list[i].rowtotal;
                        if ($scope.appConfig.sales_product_list.toUpperCase() == 'PURCHASE BASED')
                        {
                            ordereditems.purchase_invoice_id = $scope.orderListModel.list[i].purchase_invoice_id;
                            ordereditems.purchase_invoice_item_id = $scope.orderListModel.list[i].purchase_invoice_item_id;
                        } else {
                            ordereditems.purchase_invoice_id = 0;
                            ordereditems.purchase_invoice_item_id = 0;
                        }
                        ordereditems.product_id = $scope.orderListModel.list[i].id;
                        ordereditems.product_name = $scope.orderListModel.list[i].product;
                        ordereditems.qty = parseFloat($scope.orderListModel.list[i].qty);
                        if ($scope.orderListModel.list[i].sku != null && $scope.orderListModel.list[i].sku != '' && $scope.orderListModel.list[i].sku != undefined)
                        {
                            ordereditems.product_sku = $scope.orderListModel.list[i].sku;
                        } else {
                            ordereditems.product_sku = '';
                        }
                        if ($scope.orderListModel.list[i].uomInfo.id != null && $scope.orderListModel.list[i].uomInfo.id != '' && $scope.orderListModel.list[i].uomInfo.id != undefined)
                        {
                            ordereditems.uom_id = $scope.orderListModel.list[i].uomInfo.id;
                            ordereditems.uom_name = $scope.orderListModel.list[i].uomInfo.name;
                        } else
                        {
                            ordereditems.uom_id = '';
                            ordereditems.uom_name = '';
                        }
                        createOrderParam.product.push(ordereditems);
                    }
                    var configOption = adminService.handleBothSuccessAndErrorResponseConfig;
                    adminService.editNewStockAdjustment(createOrderParam, $stateParams.id, configOption, headers).then(function (response) {
                        if (response.data.success == true)
                        {
                            $scope.formReset();
                            $state.go('app.stockwastage');
                        } else {
                            $scope.isDataSavingProcess = false;
                            var element = document.getElementById("btnLoad");
                            element.classList.remove("btn-loader");
                        }
                    }).catch(function (response) {
                        console.error('Error occurred:', response.status, response.data);
                        $scope.isDataSavingProcess = false;
                        var element = document.getElementById("btnLoad");
                        element.classList.remove("btn-loader");
                    })
                } else
                {
                    sweet.show('Oops...', 'Fill Product Detail..', 'error');
                    $scope.isDataSavingProcess = false;
                }
            }
        }
        $scope.getProductList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.PRODUCT_BASED_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatProductModel = function (model, index)
        {
            if (model != null && model != 'undefined' && model != '')
            {
                $scope.updateProductInfo(model, index);
                if (model.code != '' && model.code != null && model.code != 'undefined')
                {
                    return model.name + '(' + model.code + ')';
                }
            }
            return  '';
        }
        $scope.showUploadMoreFilePopup = false;
        $scope.showPopup = function (index)
        {
            if (index == 'morefileupload')
            {
                $scope.showUploadMoreFilePopup = true;
            }
        }
        $scope.closePopup = function (index)
        {
            if (index == 'morefileupload')
            {
                $scope.showUploadMoreFilePopup = false;
            }
        }

        $scope.uploadedFileQueue = [];
        $scope.uploadedFileCount = 0;
        $scope.isImageSavingProcess = true;
        $scope.$on("updateFileUploadEvent", updateFileUploadEventHandler);
        function updateFileUploadEventHandler(event, data, category, imgcaption, index)
        {
            $scope.isImageSavingProcess = true;
            $scope.uploadedFileQueue = data;
            if (typeof category != undefined && category != null && category != '')
            {
                $scope.uploadedFileQueue[index].category = category;
            }
            if (typeof imgcaption != undefined && imgcaption != null && imgcaption != '')
            {
                $scope.uploadedFileQueue[index].imgcaption = imgcaption;
            }
            console.log('$scope.uploadedFileQueue');
            console.log($scope.uploadedFileQueue);
            $scope.validateFileUploadStatus();
        }

        $scope.$on("deletedUploadFileEvent", deletedUploadFileEventHandler);
        function deletedUploadFileEventHandler(event, index)
        {
            if ($scope.showUploadMoreFilePopup)
            {
                if ($scope.orderListModel.attachments.length > index)
                {
                    $scope.orderListModel.attachments.splice(index, 1);
                }
            }
        }

        $scope.validateFileUploadStatus = function ()
        {
            $scope.isImageSavingProcess = true;
            var hasUploadCompleted = true;
            for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
            {
                if (!$scope.uploadedFileQueue[i].isSuccess)
                {
                    hasUploadCompleted = false;
                    $scope.$broadcast('genericErrorEvent', 'Image upload is in progress.');
                    break;
                }
            }
            if (hasUploadCompleted && $scope.uploadedFileQueue.length > 0)
            {
                $scope.uploadedFileCount = $scope.uploadedFileQueue.length;
                $scope.isImageSavingProcess = false;
                $scope.isImageUploadComplete = true;
            }

        }
        $scope.deleteImage = function ($index)
        {
            $scope.orderListModel.attachments.splice($index, 1);
        }

        $scope.saveImagePath = function ()
        {
            if ($scope.uploadedFileQueue.length == 0)
            {
                if ($scope.showUploadMoreFilePopup)
                {
                    $scope.closePopup('morefileupload');
                }
            }
            var hasUploadCompleted = true;
            for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
            {
                if (!$scope.uploadedFileQueue[i].isSuccess)
                {
                    hasUploadCompleted = false;
                    $scope.$broadcast('genericErrorEvent', 'Image upload is in progress.');
                    break;
                }
            }
            if (hasUploadCompleted && $scope.uploadedFileQueue.length > 0)
            {

                $scope.uploadedFileCount = $scope.uploadedFileQueue.length;
                $scope.isImageSavingProcess = true;
                $scope.isImageUploadComplete = true;
                for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
                {
                    //               $scope.orderListModel.attachments.push($scope.uploadedFileQueue[i]);
                    var imageUpdateParam = {};
                    //imageUpdateParam.id = 0;
                    imageUpdateParam.url = $scope.uploadedFileQueue[i].urlpath;
                    //imageUpdateParam.ref_id = $stateParams.id;
                    imageUpdateParam.type = 'stock_wastage';
                    imageUpdateParam.comments = '';
                    imageUpdateParam.signedUrl = $scope.uploadedFileQueue[i].signedUrl;
                    // imageUpdateParam.path = $scope.uploadedFileQueue[i].signedUrl;
                    $scope.orderListModel.attachments.push(imageUpdateParam);
                    if (i == $scope.uploadedFileQueue.length - 1)
                    {
                        $scope.closePopup('morefileupload');
                    }
                }
            } else
            {
                $scope.closePopup('morefileupload');
            }
        };
        $scope.getEmployeeList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.EMPLOYEE_LIST_ALL, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatEmployeeModel = function (model)
        {
            if (model != null)
            {
                return model.name;
            }
            return  '';
        };
        $scope.getStockWastageListInfo = function () {

            // $scope.orderListModel.isLoadingProgress = true;
            var materialListParam = {};
            materialListParam.type = 'stock_wastage';
            materialListParam.is_active = 1;
            materialListParam.id = $stateParams.id;
            $scope.orderListModel.isLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getNewStockAdjustmentDetail(materialListParam, configOption).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.orderListModel.wastageList = data.data[0];
                    $scope.orderListModel.total = data.total;
                    $scope.updateStockwastageDetails();
                }
                $scope.orderListModel.isLoadingProgress = false;
            });
        };
        $scope.getStockWastageListInfo();
    }]);




