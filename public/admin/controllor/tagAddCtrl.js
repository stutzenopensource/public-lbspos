



app.controller('tagAddCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$timeout', 'ValidationFactory', 'APP_CONST', 'StutzenHttpService', function($scope, $rootScope, adminService, $filter, Auth, $state, $timeout, ValidationFactory, APP_CONST, StutzenHttpService) {

    $scope.tagAddModel = {
        id: '',
        name: '',
        tagPhoto: '',
        limit: '',
        isActive: true
    };
    $scope.pagePerCount = [50, 100];
    $scope.tagAddModel.limit = $scope.pagePerCount[0];

    $scope.searchFilterValue = "";
    $scope.initTableFilterTimeoutPromise = null;

    $scope.initTableFilter = function()
    {
        if ($scope.initTableFilterTimeoutPromise != null)
        {
            $timeout.cancel($scope.initTableFilterTimeoutPromise);
        }
        $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
    }


    $scope.validationFactory = ValidationFactory;


    $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
    $scope.getNavigationBlockMsg = function()
    {
        if (typeof $scope.noticeboard_add_form != 'undefined' && typeof $scope.noticeboard_add_form.$pristine != 'undefined' && !$scope.noticeboard_add_form.$pristine)
        {
            return $scope.NAVIGATION_BLOCKER_MSG;
        }

        return "";
    }
    $scope.isDataSavingProcess = false;

    $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
    $scope.uploadedFileQueue = [];
    $scope.uploadedFileCount = 0;
    $scope.isImageSavingProcess = false;
    $scope.$on("updateFileUploadEvent", updateFileUploadEventHandler);
    function updateFileUploadEventHandler(event, data)
    {
        $scope.uploadedFileQueue = data;
        console.log('$scope.uploadedFileQueue');
        console.log($scope.uploadedFileQueue);
        $scope.saveImagePath();

    }
    $scope.saveImagePath = function()
    {
        if ($scope.uploadedFileQueue.length == 0)
        {
            $scope.closePopup('fileupload');
        }
        var hasUploadCompleted = true;
        for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
        {
            if (!$scope.uploadedFileQueue[i].isSuccess)
            {
                hasUploadCompleted = false;
                $scope.$broadcast('genericErrorEvent', 'Image upload is in progress.');
                break;
            }
        }
        if (hasUploadCompleted && $scope.uploadedFileQueue.length > 0)
        {

            $scope.uploadedFileCount = $scope.uploadedFileQueue.length;
            $scope.isImageSavingProcess = true;
            $scope.tagAddModel.attach = [];
            for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
            {
                var imageuploadparam = {};
                imageuploadparam.path = $scope.uploadedFileQueue[i].urlpath;
                imageuploadparam.urlpath = $scope.uploadedFileQueue[i].urlpath;
                $scope.tagAddModel.attach.push(imageuploadparam);

            }
            $scope.isImageSavingProcess = false;
            $scope.closePopup('fileupload');
        }

    }

    $scope.formCancel = function() {
        $scope.tag_add_form.$setPristine();
        if (typeof $scope.tag_add_form != 'undefined')
        {
            $scope.tagAddModel.name = "";
            if($scope.uploadedFileQueue.length > 0)
            {    
                $scope.uploadedFileQueue[0].remove();
            }
            $scope.tagAddModel.attach = [];                
            $scope.saveImagePath();
        }
    }

    $scope.createTag = function() {

        if ($scope.isDataSavingProcess == false)
        {

            $scope.isDataSavingProcess = true;
            var createTagParam = {};
            var headers = {};
            headers['screen-code'] = 'tag';
            createTagParam.id = 0;
            createTagParam.name = $scope.tagAddModel.name;
            createTagParam.tagPhoto = "";
            if (typeof $scope.tagAddModel.attach != "undefined" && $scope.tagAddModel.attach.length > 0)
            {
                createTagParam.tagPhoto = $scope.tagAddModel.attach[0].urlpath;
            }
            createTagParam.is_active = 1;
            adminService.createTagList(createTagParam, headers).then(function(response) {
                if (response.data.success == true)
                {
                    $scope.formCancel();
                    $state.go('app.tag');
                }
                $scope.isDataSavingProcess = false;
            });
        }
    };
}]);




