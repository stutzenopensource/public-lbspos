app.controller('tagCtrl', ['$scope', '$rootScope', 'adminService', '$localStorage', '$filter', 'Auth', '$state', '$timeout', function ($scope, $rootScope, adminService, $localStorage, $filter, Auth, $state, $timeout) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.tagModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
            status: '',
            //             sku: '',
            serverList: null,
            isLoadingProgress: false
        };

        $scope.pagePerCount = [50, 100, 150];
        $scope.tagModel.limit = $scope.pagePerCount[0];
        $scope.searchFilter = {
            tag_name: ''
        }
        $scope.refreshScreen = function ()
        {
            $scope.searchFilter = {
                tag_name: ''
            };
            $scope.initTableFilter();
        }
        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }

        $scope.selectTagId = '';
        $scope.showdeletePopup = false;
        $scope.isdeleteProgress = false;
        $scope.showPopup = function (id)
        {
            $scope.selectTagId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function ()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteProgress = false;
        };
        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'tag_form';


        $scope.$on('onLocalStorageFormRenterCompleteEvent', function (event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });

        $scope.$on('onLocalStorageFieldRenterCompleteEvent', function (event, formName) {

            if ($scope.localStorageFormName == formName)
            {
                $scope.localStorageRenterCount++;
            }
            if ($scope.localStorageRenterCount >= $scope.localStorageCount)
            {
                console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
            }


        });

        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function (event, formName) {

            if ($scope.localStorageFormName == formName)
            {
                $scope.localStorageRetrieveCount++;
            }
            if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
            {
                $scope.initTableFilter();
            }
            console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);

        });

        $scope.init = function ()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');

        };

        $scope.$on('onLocalStorageReadyEvent', function () {

            $timeout(function () {
                $scope.init();
            }, 300);


        });

        $scope.getList = function ()
        {
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'tag';
            getListParam.id = "";
            getListParam.is_active = 1;
            getListParam.start = ($scope.tagModel.currentPage - 1) * $scope.tagModel.limit;
            getListParam.limit = $scope.tagModel.limit;
            getListParam.tag_name = $scope.searchFilter.tag_name;
            $scope.tagModel.isLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getTagDetail(getListParam, configOption, headers).then(function (response) {
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.tagModel.list = data.list;
                    $scope.tagModel.total = data.total;
                }
                $scope.tagModel.isLoadingProgress = false;
            });
        };
        $scope.getList();
    }]);




