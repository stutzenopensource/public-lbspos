app.controller('tagEditCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', 'StutzenHttpService', function($scope, $rootScope, adminService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST, StutzenHttpService) {

    $scope.tagAddModel = {
        id: '',
        tagId: '',
        name: '',
        tagPhoto: '',
        note: '',
        is_active: ' ',
        isLoadingProgress: false,
        tagDetail: []
    };
    $scope.pagePerCount = [50, 100];
    $scope.tagAddModel.limit = $scope.pagePerCount[0];

    $scope.validationFactory = ValidationFactory;

    $scope.userRoleList = ['', 'ROLE_ADMIN', 'ROLE_WORKFORCE'];

    $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
    $scope.getNavigationBlockMsg = function()
    {
        if (typeof $scope.tag_edit_form != 'undefined' && typeof $scope.tag_edit_form.$pristine != 'undefined' && !$scope.tag_edit_form.$pristine)
        {
            return $scope.NAVIGATION_BLOCKER_MSG;
        }

        return "";
    };

    $scope.isDataSavingProcess = false;

    $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
    $scope.formReset = function() {
        $scope.tag_edit_form.$setPristine();
        if (typeof $scope.tag_edit_form != 'undefined')
        {
            $scope.tag_edit_form.$setPristine();
            $scope.updateTagInfo();
        }
    }
    $scope.uploadedFileQueue = [];
    $scope.uploadedFileCount = 0;
    $scope.isImageSavingProcess = false;
    $scope.$on("updateFileUploadEvent", updateFileUploadEventHandler);
    function updateFileUploadEventHandler(event, data)
    {
        $scope.uploadedFileQueue = data;
        console.log('$scope.uploadedFileQueue');
        console.log($scope.uploadedFileQueue);
        $scope.saveImagePath();
                       
    }
    $scope.validateFileUploadStatus = function()
    {
        $scope.tagAddModel.img = [];
        $scope.isImageSavingProcess = true;
        var hasUploadCompleted = true;
        for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
        {
            if (!$scope.uploadedFileQueue[i].isSuccess)
            {
                hasUploadCompleted = false;
                $scope.$broadcast('genericErrorEvent', 'Image upload is in progress.');
                break;
            }
        }
        if (hasUploadCompleted && $scope.uploadedFileQueue.length > 0)
        {
            $scope.uploadedFileCount = $scope.uploadedFileQueue.length;
            $scope.isImageSavingProcess = false;
            $scope.isImageUploadComplete = true;
            $scope.saveImagePath();
        }
    }
    $scope.saveImagePath = function ()
    {
        if ($scope.uploadedFileQueue.length == 0)
        {
            $scope.closePopup('fileupload');
        }
        var hasUploadCompleted = true;
        for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
        {
            if (!$scope.uploadedFileQueue[i].isSuccess)
            {
                hasUploadCompleted = false;
                $scope.$broadcast('genericErrorEvent', 'Image upload is in progress.');
                break;
            }
        }
        if (hasUploadCompleted && $scope.uploadedFileQueue.length > 0)
        {

            $scope.uploadedFileCount = $scope.uploadedFileQueue.length;
            $scope.isImageSavingProcess = true;
            $scope.tagAddModel.attach = [];
            for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
            {
                var imageuploadparam = {};
                imageuploadparam.path = $scope.uploadedFileQueue[i].urlpath;
                imageuploadparam.urlpath = $scope.uploadedFileQueue[i].urlpath;
                $scope.tagAddModel.attach.push(imageuploadparam);

            }
            $scope.isImageSavingProcess = false;
            $scope.closePopup('fileupload');
        }

    }
    // 3.display the data
    $scope.createTag = function() {
        if ($scope.isDataSavingProcess == false)
        {

            $scope.isDataSavingProcess = true;
            var modifyTagParam = {};
            var headers = {};
            headers['screen-code'] = 'tag';
            modifyTagParam.tagId = $scope.tagAddModel.id;
            modifyTagParam.name = $scope.tagAddModel.name;
            if($scope.tagAddModel.attach.length > 0)
            {    
                modifyTagParam.tagPhoto=$scope.tagAddModel.attach[0].urlpath;
            }
            modifyTagParam.is_active = 1;
            adminService.createTagList(modifyTagParam, $scope.tagAddModel.id, headers).then(function(response) {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.tag');
                }
                $scope.isDataSavingProcess = false;
            });
        }
    };

    //2.get the updated data and keep it in local
    $scope.updateTagInfo = function()
    {
        if ($scope.tagAddModel.tagDetail != null)
        {
            $scope.tagAddModel.id = $scope.tagAddModel.tagDetail.tagId;
            $scope.tagAddModel.name = $scope.tagAddModel.tagDetail.name;
            $scope.tagAddModel.attach=[];
            if($scope.tagAddModel.tagDetail.tagPhoto != "undefined" && $scope.tagAddModel.tagDetail.tagPhoto != "")
            {
                var file ={
                    urlpath :$scope.tagAddModel.tagDetail.tagPhoto
                };
                $scope.tagAddModel.attach.push(file);
            }
                
            //                $scope.tagAddModel.is_active = ($scope.tagDetail.is_active == 1 ? true : false);
            $scope.tagAddModel.isLoadingProgress = false;
        }
    }

    //1.get the data from the list    
    $scope.getTagInfo = function() {

        $scope.tagAddModel.isLoadingProgress = true;
        if (typeof $stateParams.tagId != 'undefined')
        {
            var getListParam = {};
            getListParam.tag_id = $stateParams.tagId;
            getListParam.limit = 1;
            getListParam.start = 0;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getTagDetail(getListParam, configOption).then(function(response) {
                var data = response.data.list;
                if (data.list.length > 0)
                {
                    $scope.tagAddModel.tagDetail = data.list[0];
                    $scope.updateTagInfo();
                }

            });
        }
    };

    $scope.getTagInfo();

}]);



