app.controller('taxCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$stateParams', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', function ($scope, $rootScope, adminService, $httpService, APP_CONST, $stateParams, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.taxModel = {
            currentPage: 1,
            total: 0,
            total_Amount: 0,
            limit: 10,
            list: [],
            length: [],
            date: '',
            sno: '',
            name: '',
            taxlist: '',
            taxName: '',
            particulars: '',
            amount: '',
            status: '',
//             sku: '',

            serverList: null,
            isLoadingProgress: false,
            isSearchLoadingProgress: false
        };
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.taxModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
        $scope.validationFactory = ValidationFactory;
        $scope.openDate = function (index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            } else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };

        $scope.searchFilter = {
            id: '',
            fromdate: '',
            todate: '',
            tax_name: ''
        };

        $scope.clearFilters = function ()
        {
            $scope.searchFilter = {
                tax_id: '',
                fromdate: '',
                todate: '',
                tax_name: ''
            };
            $scope.taxModel.list = [];
            // $scope.initTableFilter();
        }

        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.validateDateFilterData = function ()
        {
            var retVal = false;
            if ($scope.searchFilter.todate !== '' || $scope.searchFilter.fromdate !== '')
            {
                retVal = true;
            }
            return retVal;
        };

        $scope.getTaxListInfo = function ( )
        {

            var getListParam = {};
            getListParam.id = "";
            getListParam.name = "";
            getListParam.start = 0;
            getListParam.is_active = 1;
            getListParam.is_group = 0;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getTaxList(getListParam, configOption).then(function (response)
            {
                var data = response.data.list;
                $scope.taxModel.taxlist = data;
//                if ($scope.taxModel.taxlist.length != 0)
//                {
//                    $scope.taxModel.tax_id = $scope.taxModel.taxlist[0].id;
//
//                }

            });
        };
        $scope.findTaxName = function ()
        {
            if ($scope.searchFilter.tax_id != null && $scope.searchFilter.tax_id != '')
            {
                for (var i = 0; i < $scope.taxModel.taxlist.length; i++)
                {
                    if ($scope.searchFilter.tax_id == $scope.taxModel.taxlist[i].id)
                    {
                        $scope.taxModel.taxName = $scope.taxModel.taxlist[i].tax_name;
                    }
                }
            }
        };
        $scope.viewRedirect = function (id)
        {
            if ($rootScope.appConfig.invoice_print_template === 't1')
            {
                $state.go('app.invoiceView1', {'id': id}, {'reload': true});
            } else if ($rootScope.appConfig.invoice_print_template === 't2')
            {
                $state.go('app.invoiceView2', {'id': id}, {'reload': true});
            } else
            {
                $state.go('app.invoiceView1', {'id': id}, {'reload': true});
            }
        };

        $scope.todate = '';
        $scope.getList = function () {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'taxreport';
            getListParam.tax_id = $scope.searchFilter.tax_id;
            getListParam.to_Date = '';
            getListParam.from_Date = '';
            if ($scope.searchFilter.todate != '' && ($scope.searchFilter.fromdate == null || $scope.searchFilter.fromdate == ''))
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_Date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                getListParam.to_Date = utilityService.changeDateToSqlFormat(getListParam.to_Date, $scope.dateFormat);
                getListParam.from_Date = getListParam.to_Date;
                $scope.todate = getListParam.to_Date;
            } else if ($scope.searchFilter.fromdate != '' && ($scope.searchFilter.todate == null || $scope.searchFilter.todate == ''))
            {
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_Date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                getListParam.from_Date = utilityService.changeDateToSqlFormat(getListParam.from_Date, $scope.dateFormat);
                getListParam.to_Date = getListParam.from_Date;
                $scope.todate = getListParam.to_Date;
            } else
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_Date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_Date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                getListParam.to_Date = utilityService.changeDateToSqlFormat(getListParam.to_Date, $scope.dateFormat);
                getListParam.from_Date = utilityService.changeDateToSqlFormat(getListParam.from_Date, $scope.dateFormat);
                $scope.todate = getListParam.to_Date;
            }
            getListParam.start = 0;
            getListParam.limit = 0;
            $scope.taxModel.isLoadingProgress = true;
            $scope.taxModel.isSearchLoadingProgress = true;
            if ($scope.searchFilter.todate != '' && $scope.searchFilter.todate != null && $scope.searchFilter.todate != 'undefined' && $scope.searchFilter.fromdate != '' && $scope.searchFilter.fromdate != null)
            {
                adminService.getTaxReport(getListParam, headers).then(function (response)
                {
                    var totalAmt = 0.00;
                    if (response.data.success === true)
                    {
                        var data = response.data.list;
                        $scope.taxModel.list = data;

                        for (var i = 0; i < $scope.taxModel.list.length; i++)
                        {
                            if ($scope.taxModel.list[i].tax_amount == null)
                            {
                                $scope.taxModel.list[i].tax_amount = 0.00;
                            }
                            totalAmt = totalAmt + parseFloat($scope.taxModel.list[i].tax_amount)
                            $scope.taxModel.list[i].tax_amount = utilityService.changeCurrency($scope.taxModel.list[i].tax_amount, $rootScope.appConfig.thousand_seperator);

                        }
                        $scope.taxModel.total = data.total;
                        $scope.taxModel.total_Amount = totalAmt;
                        $scope.taxModel.total_Amount = parseFloat($scope.taxModel.total_Amount).toFixed(2);
                        $scope.taxModel.total_Amount = utilityService.changeCurrency($scope.taxModel.total_Amount, $rootScope.appConfig.thousand_seperator);
                    }
                    $scope.taxModel.isLoadingProgress = false;
                    $scope.taxModel.isSearchLoadingProgress = false;
                });
            } else {
                $scope.taxModel.isLoadingProgress = false;
                $scope.taxModel.isSearchLoadingProgress = false;
            }

        };

        $scope.print = function (div)
        {
            var docHead = document.head.outerHTML;
            var printContents = document.getElementById(div).outerHTML;
            var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=865, height=600, resizable=yes, screenX=200, screenY=200, personalbar=no, scrollbars=yes";

            var newWin = window.open("", "_blank", winAttr);
            var writeDoc = newWin.document;
            writeDoc.open();
            writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
            writeDoc.close();
            newWin.focus();
        };

        $scope.validateAmount = function (amount)
        {
            if (amount > 0)
            {
                return true;
            } else
            {
                return false;
            }
        };
        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'tax_form';
        $scope.$on('onLocalStorageFormRenterCompleteEvent', function (event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });

        $scope.$on('onLocalStorageFieldRenterCompleteEvent', function (event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRenterCount++;
                }
                if ($scope.localStorageRenterCount >= $scope.localStorageCount)
                {
                    console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                    $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
                }

            }
        });

        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function (event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRetrieveCount++;
                }
                if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
                {
                    $scope.initTableFilter();
                }
                console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);
            }
        });

        $scope.init = function ()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');

        };

        $scope.$on('onLocalStorageReadyEvent', function () {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                $timeout(function () {
                    $scope.init();
                }, 300);

            }
        });



























        $scope.getTaxListInfo();
        //   $scope.getList();
    }]);




