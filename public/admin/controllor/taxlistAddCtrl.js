




app.controller('taxlistAddCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$filter', 'Auth', '$state', '$timeout', 'ValidationFactory', '$window', function ($scope, $rootScope, adminService, utilityService, $filter, Auth, $state, $timeout, ValidationFactory, $window) {

        $scope.taxlistModel = {
            "tax_no": "",
            "tax_name": "",
            "tax_percentage": "",
            "tax_applicable_amt": "",
            "start_date": "",
            "end_date": "",
            "comments": "",
            "isActive": 1,
            "tax_type": '',
            "isdisplay": true
        }


        $scope.getNavigationBlockMsg = function (pageReload)
        {
            if (typeof $scope.create_Taxlist_form != 'undefined' && typeof $scope.create_Taxlist_form.$pristine != 'undefined' && !$scope.create_Taxlist_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.startDateOpen = false;
        $scope.endDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.openDate = function (index)
        {
            if (index == 1)
            {
                $scope.startDateOpen = true;
            }
            if (index == 2)
            {
                $scope.endDateOpen = true;
            }
        }

        $scope.formReset = function () {

            $scope.create_Taxlist_form.$setPristine();
            $scope.taxlistModel.tax_name = '';
            $scope.taxlistModel.tax_no = '';
            $scope.taxlistModel.tax_percentage = '';
            $scope.taxlistModel.tax_applicable_amt = '';
            $scope.taxlistModel.start_date = '';
            $scope.taxlistModel.end_date = '';
            $scope.taxlistModel.comments = '';
            $scope.taxlistModel.tax_type = '';
            $scope.taxlistModel.isdisplay = true;
        }
        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.resetTaxlistCreate = function () {

            $scope.taxlistModel.user = '';

        }

        $scope.validationFactory = ValidationFactory;

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.createTaxlist, 300);
        }


//        $scope.showAlert = true;
//        setTimeout(function () {
//            if ($rootScope.userModel.new_user == 1 && $rootScope.hideAlert == false)
//            {
//                $window.localStorage.setItem("demo_guide", "false");
//                swal({
//                    title: "Guide Tour!",
//                    text: "Kindly fill all the required details and click submit to proceed the demo..",
//                    confirmButtonText: "OK"
//                },
//                        function (isConfirm) {
//                            if (isConfirm) {
//                                $rootScope.hideAlert = true;
//                            }
//                        });
//            }
//        }, 2000);
        $scope.createTaxlist = function () {

            $scope.isDataSavingProcess = true;
            var createTaxlistParam = {};
            var headers = {};
            headers['screen-code'] = 'tax';
            createTaxlistParam.tax_name = $scope.taxlistModel.tax_name;
            createTaxlistParam.tax_percentage = $scope.taxlistModel.tax_percentage;
            createTaxlistParam.tax_no = $scope.taxlistModel.tax_no;
            createTaxlistParam.is_group = 0;
            createTaxlistParam.tax_type = $scope.taxlistModel.tax_type;
            // createTaxlistParam.tax_applicable_amt = $scope.taxlistModel.tax_applicable_amt;
            createTaxlistParam.comments = $scope.taxlistModel.comments;
//            createTaxlistParam.end_date = $filter('date')($scope.taxlistModel.end_date, 'yyyy-MM-dd');
//            createTaxlistParam.start_date = $filter('date')($scope.taxlistModel.start_date, 'yyyy-MM-dd');
            createTaxlistParam.start_date = null;
            createTaxlistParam.end_date = null;
            if ($scope.taxlistModel.start_date != null && $scope.taxlistModel.start_date != "")
            {
                createTaxlistParam.start_date = utilityService.parseDateToStr($scope.taxlistModel.start_date, 'yyyy-MM-dd');
            }
            if ($scope.taxlistModel.end_date != null && $scope.taxlistModel.end_date != "")
            {
                createTaxlistParam.end_date = utilityService.parseDateToStr($scope.taxlistModel.end_date, 'yyyy-MM-dd');
            }


            createTaxlistParam.is_display = $scope.taxlistModel.isdisplay == true ? 1 : 0;

            adminService.addTax(createTaxlistParam, headers).then(function (response) {
                if (response.data.success == true)
                {
                    $scope.showAlert = false;
                    $scope.isDataSavingProcess = false;
                    $scope.formReset();
                    $state.go('app.taxlist');
//                    if ($rootScope.userModel.new_user == 1)
//                    {
//                        $window.localStorage.setItem("demo_guide", "true");
//                        $rootScope.closeDemoPopup(3);
//                    }
                }
            }).catch(function (response) {
                console.error('Error occurred:', response.status, response.data);
                $scope.isDataSavingProcess = false;
                var element = document.getElementById("btnLoad");
                element.classList.remove("btn-loader");
            });
        };



    }]);




