app.controller('taxlistCtrl', ['$scope', '$rootScope', 'adminService', 'APP_CONST', '$filter', 'Auth', '$state', '$timeout', '$httpService', '$window', function ($scope, $rootScope, adminService, APP_CONST, $filter, Auth, $state, $timeout, $httpService, $window) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.taxlistModel = {
            currentPage: 1,
            total: 0,
            limit: 5,
            list: [],
            status: '',
//             sku: '',

            serverList: null,
            isLoadingProgress: false
        };

        $scope.pagePerCount = [50, 100];
        $scope.taxlistModel.limit = $scope.pagePerCount[0];

        $scope.searchFilter = {
            start: '',
            tax_name: '',
            tax_percentage: '',
            activeOrDeactiveTax: ''


        };

        $scope.searchFilterValue = "";
        $scope.refreshScreen = function ()
        {
            $scope.searchFilter.tax_name = '';
            $scope.searchFilter.tax_percentage = '';
            $scope.searchFilter.activeOrDeactiveTax = '';
            $scope.getList();
        }

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.selecttaxlistId = '';
        $scope.showdeletePopup = false;
        $scope.isdeleteLoadingProgress = false;
        $scope.showPopup = function (id)
        {
            $scope.selecttaxlistId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function ()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteLoadingProgress = false;
        };

        $scope.deletetaxlistInfo = function ( )
        {
            if ($scope.isdeleteLoadingProgress == false)
            {
                $scope.isdeleteLoadingProgress = true;
                var getListParam = {};
                var headers = {};
                headers['screen-code'] = 'tax';
                getListParam.id = $scope.selecttaxlistId;
                adminService.deleteTax(getListParam, getListParam.id, headers).then(function (response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getList();
                    }
                    $scope.isdeleteLoadingProgress = false;
                });
            }
        };
        $scope.getList = function ()
        {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'tax';
            getListParam.tax_name = $scope.searchFilter.tax_name;
            getListParam.is_group = 0;
            getListParam.tax_percentage = $scope.searchFilter.tax_percentage;
            getListParam.start = ($scope.taxlistModel.currentPage - 1) * $scope.taxlistModel.limit;
            getListParam.limit = $scope.taxlistModel.limit;
            if ($scope.searchFilter.activeOrDeactiveTax == 1)
            {
                getListParam.is_active = 0;
            } else
            {
                getListParam.is_active = 1;
            }
            //getListParam.is_active =1;
            $scope.taxlistModel.isLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getTaxList(getListParam, configOption, headers).then(function (response) {
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.taxlistModel.list = data;
                    $scope.taxlistModel.total = data.total;
                }
                $scope.taxlistModel.isLoadingProgress = false;
            });
        };
        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'user_role';


        $scope.$on('onLocalStorageFormRenterCompleteEvent', function (event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });

        $scope.$on('onLocalStorageFieldRenterCompleteEvent', function (event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRenterCount++;
                }
                if ($scope.localStorageRenterCount >= $scope.localStorageCount)
                {
                    console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                    $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
                }

            }
        });

        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function (event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRetrieveCount++;
                }
                if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
                {
                    $scope.initTableFilter();
                    if ($rootScope.userModel.new_user == 1)
                    {
                        $window.localStorage.setItem("demo_guide", "true");
                        $rootScope.closeDemoPopup(3);
                    }
                }
                console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);
            }
        });

        $scope.init = function ()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');

        };

        $scope.$on('onLocalStorageReadyEvent', function () {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                $timeout(function () {
                    $scope.init();
                }, 300);

            }
        });

//        $scope.getList();
    }]);




