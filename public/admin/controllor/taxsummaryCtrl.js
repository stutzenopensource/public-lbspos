app.controller('taxsummaryCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', function ($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.taxsummaryModel = {
            currentPage: 1,
            total: 0,
            total_Amount: 0,
            limit: 10,
            list: [],
            length: [],
            date: '',
            sno: '',
            name: '',
            taxlist: '',
            taxName: '',
            amount: '',
            status: '',
//             sku: '',

            serverList: null,
            isLoadingProgress: false,
            isSearchLoadingProgress: false
        };
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.taxsummaryModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
        $scope.validationFactory = ValidationFactory;
        $scope.openDate = function (index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            } else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };
        $scope.searchFilter = {
            id: '',
            fromdate: '',
            todate: '',
            tax_id: ''
        };
        $scope.clearFilters = function ()
        {
            $scope.searchFilter = {
                tax_id: '',
                fromdate: '',
                todate: ''
            };
            $scope.taxsummaryModel.list = [];
        }

        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.validateDateFilterData = function ()
        {
            var retVal = false;
            if ($scope.searchFilter.todate !== '' || $scope.searchFilter.fromdate !== '')
            {
                retVal = true;
            }
            return retVal;
        };

        $scope.fromdate = '';
        $scope.getList = function () {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'taxsummaryreport';
            getListParam.from_Date = '';
            getListParam.to_Date = '';
            if ($scope.searchFilter.todate != '' && ($scope.searchFilter.fromdate == null || $scope.searchFilter.fromdate == ''))
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_Date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                getListParam.to_Date = utilityService.changeDateToSqlFormat(getListParam.to_Date, $scope.dateFormat);
                getListParam.from_Date = getListParam.to_Date;
                $scope.fromdate = getListParam.from_Date;
            } else if ($scope.searchFilter.fromdate != '' && ($scope.searchFilter.todate == null || $scope.searchFilter.todate == ''))
            {
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_Date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                getListParam.from_Date = utilityService.changeDateToSqlFormat(getListParam.from_Date, $scope.dateFormat);
                getListParam.to_Date = getListParam.from_Date;
                $scope.fromdate = getListParam.from_Date;
            } else
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_Date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_Date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                getListParam.to_Date = utilityService.changeDateToSqlFormat(getListParam.to_Date, $scope.dateFormat);
                getListParam.from_Date = utilityService.changeDateToSqlFormat(getListParam.from_Date, $scope.dateFormat);
                $scope.fromdate = getListParam.from_Date;
            }
//            if ($scope.searchFilter.todate !== '' && $scope.searchFilter.fromdate === '')
//            {
//                getListParam.to_Date = $filter('date')($scope.searchFilter.todate, $scope.dateFormat);
//                getListParam.from_Date = $filter('date')($scope.searchFilter.todate, $scope.dateFormat);
//            }
//            else if ($scope.searchFilter.fromdate !== '' && $scope.searchFilter.todate === '')
//            {
//                getListParam.to_Date = $filter('date')($scope.searchFilter.fromdate, $scope.dateFormat);
//                getListParam.from_Date = $filter('date')($scope.searchFilter.fromdate, $scope.dateFormat);
//            }
//            else
//            {
//                getListParam.to_Date = $filter('date')($scope.searchFilter.todate, $scope.dateFormat);
//                getListParam.from_Date = $filter('date')($scope.searchFilter.fromdate, $scope.dateFormat);
//            }
            getListParam.tax_id = $scope.searchFilter.tax_id;
            getListParam.start = 0;
            getListParam.limit = 0;
            $scope.taxsummaryModel.isLoadingProgress = true;
            $scope.taxsummaryModel.isSearchLoadingProgress = true;
            if ($scope.searchFilter.todate != '' && $scope.searchFilter.todate != null && $scope.searchFilter.todate != 'undefined' && $scope.searchFilter.fromdate != '' && $scope.searchFilter.fromdate != null)
            {
                adminService.gettaxsummarylist(getListParam, headers).then(function (response)
                {
                    var totalAmt = 0.00;
                    if (response.data.success === true)
                    {
                        var data = response.data.list;
                        $scope.taxsummaryModel.list = data;

                        for (var i = 0; i < $scope.taxsummaryModel.list.length; i++)
                        {
                            if ($scope.taxsummaryModel.list[i].amount == null)
                            {
                                $scope.taxsummaryModel.list[i].amount = 0.00;
                            }
                            totalAmt = totalAmt + parseFloat($scope.taxsummaryModel.list[i].amount);
                            $scope.taxsummaryModel.list[i].amount = utilityService.changeCurrency($scope.taxsummaryModel.list[i].amount, $rootScope.appConfig.thousand_seperator);

                        }
                        $scope.taxsummaryModel.total = data.total;
                        $scope.taxsummaryModel.total_Amount = totalAmt;
                        $scope.taxsummaryModel.total_Amount = parseFloat($scope.taxsummaryModel.total_Amount).toFixed(2);
                        $scope.taxsummaryModel.total_Amount = utilityService.changeCurrency($scope.taxsummaryModel.total_Amount, $rootScope.appConfig.thousand_seperator);

                        $scope.taxsummaryModel.isLoadingProgress = false;
                        $scope.taxsummaryModel.isSearchLoadingProgress = false;
                    }
                });
            } else {
                $scope.taxsummaryModel.isLoadingProgress = false;
                $scope.taxsummaryModel.isSearchLoadingProgress = false;
            }

        };

        $scope.print = function (div)
        {
            var docHead = document.head.outerHTML;
            var printContents = document.getElementById(div).outerHTML;
            var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=865, height=600, resizable=yes, screenX=200, screenY=200, personalbar=no, scrollbars=yes";

            var newWin = window.open("", "_blank", winAttr);
            var writeDoc = newWin.document;
            writeDoc.open();
            writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
            writeDoc.close();
            newWin.focus();
        };

        $scope.validateAmount = function (amount)
        {
            if (amount > 0)
            {
                return true;
            } else
            {
                return false;
            }
        };
        $scope.getTaxListInfo = function ( )
        {

            var getListParam = {};
            getListParam.id = "";
            getListParam.name = "";
            getListParam.start = 0;
            getListParam.is_active = 1;
            getListParam.is_group = 0;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getTaxList(getListParam, configOption).then(function (response)
            {
                var data = response.data.list;
                $scope.taxsummaryModel.taxlist = data;
//                if ($scope.taxModel.taxlist.length != 0)
//                {
//                    $scope.taxModel.tax_id = $scope.taxModel.taxlist[0].id;
//
//                }

            });
        };
        $scope.findTaxNames = function ()
        {
            if ($scope.searchFilter.tax_id !== null && $scope.searchFilter.tax_id !== '')
            {
                for (var i = 0; i < $scope.taxsummaryModel.taxlist.length; i++)
                {
                    if ($scope.searchFilter.tax_id == $scope.taxsummaryModel.taxlist[i].id)
                    {
                        $scope.taxsummaryModel.taxName = $scope.taxsummaryModel.taxlist[i].tax_name;
                    }
                }
            }
        };


        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'payable_form';


        $scope.$on('onLocalStorageFormRenterCompleteEvent', function (event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });

        $scope.$on('onLocalStorageFieldRenterCompleteEvent', function (event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRenterCount++;
                }
                if ($scope.localStorageRenterCount >= $scope.localStorageCount)
                {
                    console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                    $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
                }

            }
        });

        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function (event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRetrieveCount++;
                }
                if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
                {
                    $scope.initTableFilter();
                }
                console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);
            }
        });

        $scope.init = function ()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');

        };

        $scope.$on('onLocalStorageReadyEvent', function () {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                $timeout(function () {
                    $scope.init();
                }, 300);

            }
        });
        $scope.getTaxListInfo();
        //$scope.getList();
    }]);




