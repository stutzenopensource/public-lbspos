





app.controller('transactionCtrl', ['$scope', '$rootScope', 'adminService', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', '$window', function($scope, $rootScope, adminService, ValidationFactory, utilityService, $filter, Auth, $state, $timeout, $window) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.transactionModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
            date: '',
            description: '',
            paymentmode: '',
            created_by: '',
            credit: '',
            debit: '',
            userlist: [],
            user: [],
            status: '',
            serverList: null,
            isLoadingProgress: false
        };

        $scope.pagePerCount = [50, 100];
        $scope.transactionModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;

        $scope.validationFactory = ValidationFactory;
        $scope.openDate = function(index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            }
            else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };

        $scope.searchFilter = {
            fromdate: '',
            todate: '',
            id: '',
            name: '',
            user: ''

        };

        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };

        $scope.print = function()
        {
            $window.print();
        }

        $scope.getList = function() {

            $scope.transactionModel.isLoadingProgress = true;
            var getListParam = {};
            getListParam.fromDate = $filter('date')($scope.searchFilter.fromdate, 'yyyy-MM-dd');
            getListParam.toDate = $filter('date')($scope.searchFilter.todate, 'yyyy-MM-dd');
            if (getListParam.fromDate === '' || getListParam.toDate === '')
            {
                if (getListParam.fromDate !== '')
                {
                    getListParam.toDate = getListParam.fromDate;
                }
                if (getListParam.toDate !== '')
                {
                    getListParam.fromDate = getListParam.toDate;
                }
            }
            getListParam.id = '';
            getListParam.name = '';
            getListParam.userId = $scope.searchFilter.user;
//            getListParam.start = ($scope.transactionModel.currentPage - 1) * $scope.transactionModel.limit;
//            getListParam.limit = $scope.transactionModel.limit;
            $scope.transactionModel.isLoadingProgress = true;
            adminService.gettransactionlist(getListParam).then(function(response) {
                var data = response.data.list;
                $scope.transactionModel.list = data;
                for (var i = 0; i < $scope.transactionModel.list.length; i++)
                {
                    $scope.transactionModel.list[i].newdate = utilityService.parseStrToDate($scope.transactionModel.list[i].created_at);
                    $scope.transactionModel.list[i].created_at = utilityService.parseDateToStr($scope.transactionModel.list[i].newdate, $scope.dateFormat);
                }

                $scope.transactionModel.total = data.total;
                $scope.transactionModel.isLoadingProgress = false;
            });

        };
        $scope.getUserList = function() {

            var getListParam = {};
            getListParam.date = '';
            getListParam.description = '';
            getListParam.type = '';
            getListParam.credited_by = '';
            getListParam.credit = '';
            getListParam.debit = '';
            adminService.getUserList(getListParam).then(function(response) {
                var data = response.data;
                $scope.transactionModel.userlist = data.list;
            });
        };

        $scope.getList();
        $scope.getUserList();
    }]);




