




app.controller('transferlistAddCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$httpService', 'APP_CONST', '$filter', 'Auth', '$state', 'sweet', '$stateParams', '$timeout', 'ValidationFactory', function ($scope, $rootScope, adminService, utilityService, $httpService, APP_CONST, $filter, Auth, $state, sweet, $stateParams, $timeout, ValidationFactory) {

        $scope.transferlistModel = {
            "source_account": "",
            "desig_account": "",
            "amount": "",
            "comments": "",
            "date": "",
            "status": "",
            "length": "",
            "accountInfo": "",
            "accountlistInfo": ""

        };
        $scope.validationFactory = ValidationFactory;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function ()
        {
            if (typeof $scope.create_transferlist_form !== 'undefined' && typeof $scope.create_transferlist_form.$pristine !== 'undefined' && !$scope.create_transferlist_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        };
        $scope.isDataSavingProcess = false;
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.formReset = function ()
        {
            $scope.create_transferlist_form.$setPristine();
            $scope.transferlistModel.source_account = "";
            $scope.transferlistModel.desig_account = "";
            $scope.transferlistModel.amount = "";
            $scope.transferlistModel.date = "";
            $scope.transferlistModel.accountInfo = '';
            $scope.transferlistModel.accountlistInfo = '';
            $scope.transferlistModel.comments = "";
            //$scope.transferlistModel.isActive = true;
        }
        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }

        $scope.currentDate = new Date();
//       $scope.transferlistModel.date = $filter('date')($scope.currentDate, $scope.dateFormat);

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.startDateOpen = false;

        $scope.openDate = function (index) {

            if (index === 0)
            {
                $scope.startDateOpen = true;
            }

        };

        $scope.getAccountlist = function (val)
        {
            var autosearchParam = {};
            autosearchParam.account = val;
            return $httpService.get(APP_CONST.API.GET_ACCOUNT_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formataccountModel = function (model) {

            if (model !== null && model != undefined)
            {

                return model.account;
            }
            return  '';
        };

        $scope.createtransferlist = function ()
        {
            if ($scope.isDataSavingProcess == false)
            {
                if ($scope.transferlistModel.accountlistInfo.id != $scope.transferlistModel.accountInfo.id)
                {

                    $scope.isDataSavingProcess = true;
                    var createTransferlistParam = {};
                    var headers = {};
                    headers['screen-code'] = 'transfer';
                    createTransferlistParam.date = $filter('date')($scope.transferlistModel.date, 'yyyy-MM-dd');
                    createTransferlistParam.comments = $scope.transferlistModel.comments;
                    createTransferlistParam.source_account = $scope.transferlistModel.accountInfo.id;
                    createTransferlistParam.desig_account = $scope.transferlistModel.accountlistInfo.id;
                    createTransferlistParam.amount = $scope.transferlistModel.amount;
                    // createTransferlistParam.is_active = 1,
                    adminService.createtransferlist(createTransferlistParam, headers).then(function (response) {
                        if (response.data.success === true)
                        {
                            $scope.formReset();
                            $state.go('app.transferlist');
                        }
                        $scope.isDataSavingProcess = false;
                    }).catch(function (response) {
                        console.error('Error occurred:', response.status, response.data);
                        $scope.isDataSavingProcess = false;
                        var element = document.getElementById("btnLoad");
                        element.classList.remove("btn-loader");
                    });
                } else
                {
                    sweet.show('Oops...', 'Choose Different Account... ', 'error');
                    $scope.isDataSavingProcess = false;
                    var element = document.getElementById("btnLoad");
                    element.classList.remove("btn-loader");
                }
            }

        };


        //$scope.getAccountlist();

    }
]);




