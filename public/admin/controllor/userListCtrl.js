app.controller('userListCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$timeout', 'ValidationFactory', function($scope, $rootScope, adminService, $filter, Auth, $state, $timeout, ValidationFactory) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.userModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            email: '',
            list: [],
            country: '',
            city: '',
            id: '',
            status: '',
            serverList: null,
            isLoadingProgress: false
        };
        $scope.pagePerCount = [50, 100];
        $scope.userModel.limit = $scope.pagePerCount[0];
        $scope.validationFactory = ValidationFactory;
        $scope.searchFilter = {
            email: '',
            username: '',
            phno: '',
            user_type: '',
            name: '',
            country: '',
            city: ''
        };

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }
        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.save_invite_form != 'undefined' && typeof $scope.save_invite_form.$pristine != 'undefined' && !$scope.save_invite_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.formReset = function() {

            $scope.save_invite_form.$setPristine();
            $scope.userModel.email = '';
        }
        $scope.refreshScreen = function()
        {
            $scope.searchFilter = {
                email: '',
                phno: '',
                name: '',
                city: ''
            };
            $scope.initTableFilter();
        }
        $scope.selectUserId = '';
        $scope.showdeletePopup = false;
        $scope.showInvitePopup = false;
        $scope.isdeleteUserLoadingProgress = false;
        $scope.invitePopup = function()
        {
            $scope.userModel.email = '';
            $scope.showInvitePopup = true;
        };

        $scope.closeInvitePopup = function()
        {
            $scope.showInvitePopup = false;
        };

        $scope.showPopup = function(id)
        {
            $scope.selectUserId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteUserLoadingProgress = false;
        };

        $scope.deleteUserInfo = function( )
        {
            if ($scope.isdeleteUserLoadingProgress == false)
            {
                $scope.isdeleteUserLoadingProgress = true;
                var getListParam = {};
                getListParam.id = $scope.selectUserId;
                var headers = {};
                headers['screen-code'] = 'users';
                adminService.deleteUser(getListParam, getListParam.id, headers).then(function(response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getList();
                    }
                    $scope.isdeleteUserLoadingProgress = false;
                });
            }
        };

        $scope.getList = function() {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'users';
            getListParam.userId = '';
            getListParam.companyId = $rootScope.userModel.companyId;
            getListParam.name = $scope.searchFilter.name;
            getListParam.phno = $scope.searchFilter.phno;
            getListParam.email = $scope.searchFilter.email;
            getListParam.country = '';
            getListParam.city = $scope.searchFilter.city;
            getListParam.userType = $scope.searchFilter.user_type;
            getListParam.userName = '';
            getListParam.isactive = 1;
            getListParam.start = ($scope.userModel.currentPage - 1) * $scope.userModel.limit;
            getListParam.limit = $scope.userModel.limit;
            $scope.userModel.isLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getUserList(getListParam,configOption, headers).then(function(response) {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.userModel.list = data.list;
                    $scope.userModel.total = data.total;
                }
                $scope.userModel.isLoadingProgress = false;
            });

        };
        $scope.saveUserInvite = function() {

            $scope.isDataSavingProcess = true;
            var userInviteParam = [];
            var userInfo = {};
            userInfo.email = $scope.userModel.email;
            userInviteParam.push(userInfo);
            adminService.createUserInvite(userInviteParam).then(function(response) {
                if (response.data.success == true)
                {
                    $scope.showInvitePopup = false;
                    $scope.formReset();
                    $state.go('app.userlist')
                }
                $scope.isDataSavingProcess = false;

            });
        };
        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'user_list_form';


        $scope.$on('onLocalStorageFormRenterCompleteEvent', function(event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });  
        
          $scope.$on('onLocalStorageFieldRenterCompleteEvent', function(event, formName) {
             if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRenterCount++;
                }
                if ($scope.localStorageRenterCount >= $scope.localStorageCount)
                {
                    console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                    $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
                }
            
              }
        });
              
       $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function(event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRetrieveCount++;
                }
                if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
                {
                    $scope.initTableFilter();
                }
                console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);
            }
        });

        $scope.init = function()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');

        };

        $scope.$on('onLocalStorageReadyEvent', function() {
           if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                $timeout(function() {
                    $scope.init();
                }, 300);
            
            }
        });            
        
        //$scope.getList();
    }]);








