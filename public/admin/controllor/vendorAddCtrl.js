
app.controller('vendorAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$timeout', '$filter', 'Auth', 'ValidationFactory', '$state', '$stateParams', 'APP_CONST', 'customerFactory', '$window', function ($scope, $rootScope, adminService, $httpService, utilityService, $timeout, $filter, Auth, ValidationFactory, $state, $stateParams, APP_CONST, customerFactory, $window) {



        $scope.customerAddModel = {
            "id": 0,
            "customerno": "",
            "customerprefix": "",
            "name": "",
            "companyName": "",
            "email": "",
            "phone": "",
            "city": "",
            "state": "",
            "country": "",
            "postcode": "",
            "customer_type": "suplier",
            "address": "",
            "tags": "",
            "currency": "",
            "group": "",
            "password": "",
            "confirmpassword": "",
            "iswelcomeEmail": true,
            "isactive": 1,
            "isPurchase": true,
            "isSale": true,
            "userList": [],
            "billingaddr": '',
            "billingcity": '',
            "billingstate": '',
            "billingpostcode": '',
            "billingcountry": '',
            "shoppingaddr": '',
            "shoppingcity": '',
            "shoppingstate": '',
            "shoppingpostcode": '',
            "shoppingcountry": '',
            "isSameAddr": '',
            "billingCountryList": [],
            "billingStateList": [],
            "billingCityList": [],
            "shoppingCountryList": [],
            "shoppingStateList": [],
            "shoppingCityList": [],
            "acode": '',
            "dob": '',
            "gst_no": '',
            "special_date1": '',
            "special_date2": '',
            "special_date3": '',
            "special_date4": '',
            "special_date5": '',
            "other_contact_no": '',
            "pageName": '',
            "billingaddress2": '',
            "billingaddress3": '',
            "billing_company_name": '',
            "billing_mail_id": '',
            "billing_website": '',
            "billing_mobile_no": '',
            "billing_phone_no": '',
            "shippingaddress2": '',
            "shippingaddress3": '',
            "shipping_mobile_no": '',
            "shipping_phone_no": '',
            "shipping_mail_id": '',
            "shipping_website": '',
            "shipping_company_name": '',
            "bankname": '',
            "account_number": '',
            "branch": '',
            "ifsccode": '',
            "personname": '',
            "designation": ''
        }

        $scope.customerDualAddModel = {
            "id": 0,
            "customer_id": 0,
            "name": "",
            "companyName": "",
            "customer_type": "suplier",
            "email": "",
            "phone": "",
            "city": "",
            "state": "",
            "country": "",
            "postcode": "",
            "address": "",
            "tags": "",
            "currency": "",
            "group": "",
            "password": "",
            "confirmpassword": "",
            "iswelcomeEmail": true,
            "isactive": 1,
            "isPurchase": true,
            "isSale": true,
            "userList": [],
            "billingaddr": '',
            "billingcity": '',
            "billingstate": '',
            "billingpostcode": '',
            "billingcountry": '',
            "shoppingaddr": '',
            "shoppingcity": '',
            "shoppingstate": '',
            "shoppingpostcode": '',
            "shoppingcountry": '',
            "isSameAddr": '',
            "billingCountryList": [],
            "billingStateList": [],
            "billingCityList": [],
            "shoppingCountryList": [],
            "shoppingStateList": [],
            "shoppingCityList": [],
            "acode": '',
            "dob": '',
            "otherContact": '',
            "billingaddress2": '',
            "billingaddress3": '',
            "billing_company_name": '',
            "billing_mail_id": '',
            "billing_website": '',
            "billing_mobile_no": '',
            "billing_phone_no": '',
            "shippingaddress2": '',
            "shippingaddress3": '',
            "shipping_mobile_no": '',
            "shipping_phone_no": '',
            "shipping_mail_id": '',
            "shipping_website": '',
            "shipping_company_name": '',
            "bankname": '',
            "account_number": '',
            "branch": '',
            "ifsccode": '',
            "personname": '',
            "designation": ''
        }
        $scope.customAttributeList = [];

        $scope.customAttributeBaseList = [];
        $scope.customAttributeDualSupportList = [];
        $scope.customerInfo = customerFactory.get();
        if ($scope.customerInfo != '' && $scope.customerInfo != null)
        {
            $scope.customerAddModel.personname = $scope.customerInfo.name;
            $scope.customerAddModel.email = $scope.customerInfo.email;
            $scope.customerAddModel.mobile = $scope.customerInfo.mobile;
            $scope.customerAddModel.billingaddr = $scope.customerInfo.billingaddr;
            $scope.customerAddModel.billingcountry = $scope.customerInfo.country;
            $scope.customerAddModel.billingstate = $scope.customerInfo.state;
            $scope.customerAddModel.billingcity = $scope.customerInfo.city;
            $scope.customerAddModel.billingpostcode = $scope.customerInfo.billingpostcode;
            $scope.customerAddModel.pageName = $scope.customerInfo.pageName;
            if ($scope.customerAddModel.pageName == 'invoiceAdd')
            {
                $scope.customerAddModel.customer_type = 'suplier';
            }
        }
        customerFactory.set('');


        $scope.validationFactory = ValidationFactory;
        $scope.getNavigationBlockMsg = function (pageReload)
        {
            if (typeof $scope.create_customer_form != 'undefined' && typeof $scope.create_customer_form.$pristine != 'undefined' && !$scope.create_customer_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.formReset = function () {
            if (typeof $scope.create_customer_form != 'undefined')
            {
                $scope.create_customer_form.$setPristine();
                $scope.customerAddModel = {
                    "id": 0,
                    "customer_type": "suplier",
                    "customerno": "",
                    "customerprefix": "",
                    "name": "",
                    "companyName": "",
                    "email": "",
                    "phone": "",
                    "city": "",
                    "state": "",
                    "country": "",
                    "postcode": "",
                    "address": "",
                    "tags": "",
                    "currency": "",
                    "group": "",
                    "password": "",
                    "confirmpassword": "",
                    "iswelcomeEmail": true,
                    "isactive": 1,
                    "isPurchase": true,
                    "isSale": true,
                    "userList": [],
                    "billingaddr": '',
                    "billingcity": '',
                    "billingstate": '',
                    "billingpostcode": '',
                    "billingcountry": '',
                    "shoppingaddr": '',
                    "shoppingcity": '',
                    "shoppingstate": '',
                    "shoppingpostcode": '',
                    "shoppingcountry": '',
                    "isSameAddr": '',
                    "dob": '',
                    "gst_no": '',
                    "special_date1": '',
                    "special_date2": '',
                    "special_date3": '',
                    "special_date4": '',
                    "special_date5": '',
                    "other_contact_no": '',
                    "billingaddress2": '',
                    "billingaddress3": '',
                    "billing_company_name": '',
                    "billing_mail_id": '',
                    "billing_website": '',
                    "billing_mobile_no": '',
                    "billing_phone_no": '',
                    "shippingaddress2": '',
                    "shippingaddress3": '',
                    "shipping_mobile_no": '',
                    "shipping_phone_no": '',
                    "shipping_mail_id": '',
                    "shipping_website": '',
                    "shipping_company_name": '',
                    "bankname": '',
                    "account_number": '',
                    "branch": '',
                    "ifsccode": '',
                    "personname": '',
                    "designation": ''
                }
                $scope.customerDualAddModel = {
                    "id": 0,
                    "customer_id": 0,
                    "customer_type": "suplier",
                    "name": "",
                    "companyName": "",
                    "email": "",
                    "phone": "",
                    "city": "",
                    "state": "",
                    "country": "",
                    "postcode": "",
                    "address": "",
                    "tags": "",
                    "currency": "",
                    "group": "",
                    "password": "",
                    "confirmpassword": "",
                    "iswelcomeEmail": true,
                    "isactive": 1,
                    "isPurchase": true,
                    "isSale": true,
                    "userList": [],
                    "billingaddr": '',
                    "billingcity": '',
                    "billingstate": '',
                    "billingpostcode": '',
                    "billingcountry": '',
                    "shoppingaddr": '',
                    "shoppingcity": '',
                    "shoppingstate": '',
                    "shoppingpostcode": '',
                    "shoppingcountry": '',
                    "isSameAddr": '',
                    "dob": '',
                    "gst_no": '',
                    "special_date1": '',
                    "special_date2": '',
                    "special_date3": '',
                    "special_date4": '',
                    "special_date5": '',
                    "billingaddress2": '',
                    "billingaddress3": '',
                    "billing_company_name": '',
                    "billing_mail_id": '',
                    "billing_website": '',
                    "billing_mobile_no": '',
                    "billing_phone_no": '',
                    "shippingaddress2": '',
                    "shippingaddress3": '',
                    "shipping_mobile_no": '',
                    "shipping_phone_no": '',
                    "shipping_mail_id": '',
                    "shipping_website": '',
                    "shipping_company_name": '',
                    "bankname": '',
                    "account_number": '',
                    "branch": '',
                    "ifsccode": '',
                    "personname": '',
                    "designation": ''
                }
                $scope.customAttributeList = [];
                $scope.getCustomAttributeList();
            }
        }
        $scope.getAcodeList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.code = val;
            return $httpService.get(APP_CONST.API.ACODE_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatAcodeModel = function (model)
        {
            if (model != null)
            {
                return model.code;
            }
            return  '';
        };
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.dobDateOpen = false;
        $scope.specialDate1 = false;
        $scope.specialDate2 = false;
        $scope.specialDate3 = false;
        $scope.specialDate4 = false;
        $scope.specialDate5 = false;
        $scope.openDate = function (index) {

            if (index == 0)
            {
                $scope.dobDateOpen = true;
            }
            if (index == 1)
            {
                $scope.specialDate1 = true;
            }
            if (index == 2)
            {
                $scope.specialDate2 = true;
            }
            if (index == 3)
            {
                $scope.specialDate3 = true;
            }
            if (index == 4)
            {
                $scope.specialDate4 = true;
            }
            if (index == 5)
            {
                $scope.specialDate5 = true;
            }

        }
        $scope.checkAddress = function ()
        {
            if ($scope.customerAddModel.isSameAddr == true)
            {
                $scope.updateShoppingAddr();
            }
        }

        $scope.updateShoppingAddr = function (string)
        {
            if ($scope.customerAddModel.isSameAddr == true)
            {
                $scope.customerAddModel.shoppingaddr = $scope.customerAddModel.billingaddr;
                $scope.customerAddModel.shoppingcity = $scope.customerAddModel.billingcity;
                $scope.customerAddModel.shoppingstate = $scope.customerAddModel.billingstate;
                $scope.customerAddModel.shoppingpostcode = $scope.customerAddModel.billingpostcode;
                $scope.customerAddModel.shoppingcountry = $scope.customerAddModel.billingcountry;
                $scope.customerAddModel.shipping_company_name = $scope.customerAddModel.companyName;
                $scope.customerAddModel.shippingaddress2 = $scope.customerAddModel.billingaddress2;
                $scope.customerAddModel.shippingaddress3 = $scope.customerAddModel.billingaddress3;
                $scope.customerAddModel.shipping_mobile_no = $scope.customerAddModel.billing_mobile_no;
                $scope.customerAddModel.shipping_phone_no = $scope.customerAddModel.billing_phone_no;
                $scope.customerAddModel.shipping_website = $scope.customerAddModel.billing_website;
                $scope.customerAddModel.shipping_mail_id = $scope.customerAddModel.billing_mail_id;

                $scope.customerDualAddModel.shoppingaddr = $scope.customerDualAddModel.billingaddr;
                $scope.customerDualAddModel.shoppingcity = $scope.customerDualAddModel.billingcity;
                $scope.customerDualAddModel.shoppingstate = $scope.customerDualAddModel.billingstate;
                $scope.customerDualAddModel.shoppingpostcode = $scope.customerDualAddModel.billingpostcode;
                $scope.customerDualAddModel.shoppingcountry = $scope.customerDualAddModel.billingcountry;
                $scope.customerDualAddModel.shipping_company_name = $scope.customerDualAddModel.companyName;
                $scope.customerDualAddModel.shippingaddress2 = $scope.customerDualAddModel.billingaddress2;
                $scope.customerDualAddModel.shippingaddress3 = $scope.customerDualAddModel.billingaddress3;
                $scope.customerDualAddModel.shipping_mobile_no = $scope.customerDualAddModel.billing_mobile_no;
                $scope.customerDualAddModel.shipping_phone_no = $scope.customerDualAddModel.billing_phone_no;
                $scope.customerDualAddModel.shipping_website = $scope.customerDualAddModel.billing_website;
                $scope.customerDualAddModel.shipping_mail_id = $scope.customerDualAddModel.billing_mail_id;
            } else if ($scope.customerAddModel.isSameAddr == false)
            {
                if (string != 'update')
                {
                    $scope.customerAddModel.shoppingaddr = '';
                    $scope.customerAddModel.shoppingaddr = '';
                    $scope.customerAddModel.shoppingcity = '';
                    $scope.customerAddModel.shoppingstate = '';
                    $scope.customerAddModel.shoppingpostcode = '';
                    $scope.customerAddModel.shoppingcountry = '';
                    $scope.customerAddModel.shipping_company_name = '';
                    $scope.customerAddModel.shippingaddress2 = '';
                    $scope.customerAddModel.shippingaddress3 = '';
                    $scope.customerAddModel.shipping_mobile_no = '';
                    $scope.customerAddModel.shipping_phone_no = '';
                    $scope.customerAddModel.shipping_website = '';
                    $scope.customerAddModel.shipping_mail_id = '';
                }
                $scope.customerDualAddModel.shoppingaddr = '';
                $scope.customerDualAddModel.shoppingcity = '';
                $scope.customerDualAddModel.shoppingstate = '';
                $scope.customerDualAddModel.shoppingpostcode = '';
                $scope.customerDualAddModel.shoppingcountry = '';
                $scope.customerDualAddModel.shipping_company_name = '';
                $scope.customerDualAddModel.shippingaddress2 = '';
                $scope.customerDualAddModel.shippingaddress3 = '';
                $scope.customerDualAddModel.shipping_mobile_no = '';
                $scope.customerDualAddModel.shipping_phone_no = '';
                $scope.customerDualAddModel.shipping_website = '';
                $scope.customerDualAddModel.shipping_mail_id = '';
            }
        }

        $scope.isAddressDetail = false;
        $scope.isOthersDetail = false;
        $scope.showAddressInfo = function ()
        {
            $scope.isAddressDetail = !$scope.isAddressDetail;
        }

        $scope.isCustomInfo = false;
        $scope.showCustomInfo = function ()
        {
            $scope.isCustomInfo = !$scope.isCustomInfo;
        }
        $scope.showOthersInfo = function ()
        {
            $scope.isOthersDetail = !$scope.isOthersDetail;
        }
//        $scope.showAlert = true;
//        setTimeout(function () {
//            if ($rootScope.userModel.new_user == 1 && $rootScope.hideAlert == false)
//            {
//                $window.localStorage.setItem("demo_guide", "false");
//                swal({
//                    title: "Guide Tour!",
//                    text: "Kindly fill all the required details and click submit to proceed the demo..",
//                    confirmButtonText: "OK"
//                },
//                        function (isConfirm) {
//                            if (isConfirm) {
//                                $rootScope.hideAlert = true;
//                            }
//                        });
//            }
//        }, 2000);
        $scope.createcustomer = function () {
            $scope.isDataSavingProcess = true;
            var headers = {};
            headers['screen-code'] = 'vendor';
            var createcustomerParam = {};
            createcustomerParam.id = 0;
            createcustomerParam.customer_no = '';
            createcustomerParam.prefix = $scope.customerAddModel.customerprefix;
            createcustomerParam.fname = $scope.customerAddModel.personname;
            createcustomerParam.jobtitle = $scope.customerAddModel.designation;
            createcustomerParam.customer_type = 'suplier';
            createcustomerParam.company = $scope.customerAddModel.companyName;
            createcustomerParam.billing_address2 = $scope.customerAddModel.billingaddress2;
            createcustomerParam.billing_address3 = $scope.customerAddModel.billingaddress3;
            createcustomerParam.billing_company_name = $scope.customerAddModel.companyName;
            createcustomerParam.billing_mail_id = $scope.customerAddModel.billing_mail_id;
            createcustomerParam.billing_website = $scope.customerAddModel.billing_website;
            createcustomerParam.billing_mobile_no = $scope.customerAddModel.billing_mobile_no;
            createcustomerParam.billing_phone_no = $scope.customerAddModel.billing_phone_no;
            createcustomerParam.shipping_address2 = $scope.customerAddModel.shippingaddress2;
            createcustomerParam.shipping_address3 = $scope.customerAddModel.shippingaddress3;
            createcustomerParam.shipping_mobile_no = $scope.customerAddModel.shipping_mobile_no;
            createcustomerParam.shipping_phone_no = $scope.customerAddModel.shipping_phone_no;
            createcustomerParam.shipping_mail_id = $scope.customerAddModel.shipping_mail_id;
            createcustomerParam.shipping_website = $scope.customerAddModel.shipping_website;
            createcustomerParam.shipping_company_name = $scope.customerAddModel.shipping_company_name;
            createcustomerParam.bankname = $scope.customerAddModel.bankname;
            createcustomerParam.bankacct = $scope.customerAddModel.account_number;
            createcustomerParam.bank_branch = $scope.customerAddModel.branch;
            createcustomerParam.bankcode = $scope.customerAddModel.ifsc_code;
            createcustomerParam.gst_no = $scope.customerAddModel.gst_no;
            createcustomerParam.email = $scope.customerAddModel.email;
            createcustomerParam.phone = $scope.customerAddModel.mobile;
            createcustomerParam.address = $scope.customerAddModel.address;
            createcustomerParam.city = $scope.customerAddModel.city;
            createcustomerParam.state = $scope.customerAddModel.state;
            createcustomerParam.country = $scope.customerAddModel.country;
            createcustomerParam.zip = $scope.customerAddModel.postcode;
            createcustomerParam.tags = $scope.customerAddModel.tags;
            createcustomerParam.currency = $scope.customerAddModel.currency;
            createcustomerParam.group = $scope.customerAddModel.group;
            createcustomerParam.other_contact_no = $scope.customerAddModel.other_contact_no;
            createcustomerParam.password = $scope.customerAddModel.password;
            createcustomerParam.confirmPassword = $scope.customerAddModel.confirmpassword;
            createcustomerParam.iswelcomeEmail = ($scope.customerAddModel.iswelcomeEmail == true ? 1 : 0);

            createcustomerParam.is_purchase = 1;
            createcustomerParam.billing_address = $scope.customerAddModel.billingaddr;
            createcustomerParam.billing_city = $scope.customerAddModel.billingcity;
            createcustomerParam.billing_state = $scope.customerAddModel.billingstate;
            createcustomerParam.billing_country = $scope.customerAddModel.billingcountry;
            for (var i = 0; i < $scope.customerAddModel.billingCountryList.length; i++)
            {
                if ($scope.customerAddModel.billingcountry == $scope.customerAddModel.billingCountryList[i].name)
                {
                    createcustomerParam.billing_country_id = $scope.customerAddModel.billingCountryList[i].id;
                }
            }
            for (var i = 0; i < $scope.customerAddModel.billingStateList.length; i++)
            {
                if ($scope.customerAddModel.billingstate == $scope.customerAddModel.billingStateList[i].name)
                {
                    createcustomerParam.billing_state_id = $scope.customerAddModel.billingStateList[i].id;
                }
            }
            for (var i = 0; i < $scope.customerAddModel.billingCityList.length; i++)
            {
                if ($scope.customerAddModel.billingcity == $scope.customerAddModel.billingCityList[i].name)
                {
                    createcustomerParam.billing_city_id = $scope.customerAddModel.billingCityList[i].id;
                }
            }
            createcustomerParam.billing_pincode = $scope.customerAddModel.billingpostcode;
            createcustomerParam.shopping_address = $scope.customerAddModel.shoppingaddr;
            createcustomerParam.shopping_city = $scope.customerAddModel.shoppingcity;
            createcustomerParam.shopping_state = $scope.customerAddModel.shoppingstate;
            createcustomerParam.shopping_country = $scope.customerAddModel.shoppingcountry;
            createcustomerParam.shopping_pincode = $scope.customerAddModel.shoppingpostcode;
            for (var i = 0; i < $scope.customerAddModel.shoppingCountryList.length; i++)
            {
                if ($scope.customerAddModel.shoppingcountry == $scope.customerAddModel.shoppingCountryList[i].name)
                {
                    createcustomerParam.shopping_country_id = $scope.customerAddModel.shoppingCountryList[i].id;
                }
            }
            for (var i = 0; i < $scope.customerAddModel.shoppingStateList.length; i++)
            {
                if ($scope.customerAddModel.shoppingstate == $scope.customerAddModel.shoppingStateList[i].name)
                {
                    createcustomerParam.shopping_state_id = $scope.customerAddModel.shoppingStateList[i].id;
                }
            }
            for (var i = 0; i < $scope.customerAddModel.shoppingCityList.length; i++)
            {
                if ($scope.customerAddModel.shoppingcity == $scope.customerAddModel.shoppingCityList[i].name)
                {
                    createcustomerParam.shopping_city_id = $scope.customerAddModel.shoppingCityList[i].id;
                }
            }
            createcustomerParam.acode = '';
            if (typeof $scope.customerAddModel.acode != undefined && $scope.customerAddModel.acode != null && $scope.customerAddModel.acode != '')
            {
                createcustomerParam.acode = $scope.customerAddModel.acode.code;
            }
            createcustomerParam.is_active = 1;
            createcustomerParam.customattribute = [];
            for (var i = 0; i < $scope.customAttributeList.length; i++)
            {
                var attribute_prefix = $scope.customAttributeList[i].attribute_code.split('_')[0];
                if (attribute_prefix == $rootScope.appConfig.dual_language_label)
                {
                    continue;
                }

                var customattributeItem = {};

                if ($scope.customAttributeList[i].value != undefined)
                {
                    customattributeItem.value = $scope.customAttributeList[i].value;
                } else if ($scope.customAttributeList[i].default_value != undefined)
                {
                    customattributeItem.value = $scope.customAttributeList[i].default_value;
                } else
                {
                    customattributeItem.value = "";
                }

                customattributeItem.attribute_code = $scope.customAttributeList[i].attribute_code;
                createcustomerParam.customattribute.push(customattributeItem);
            }
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            if ($rootScope.appConfig.dual_language_support)
            {
                configOption = adminService.handleOnlyErrorResponseConfig;
            }
            if (typeof $scope.customerAddModel.dob == 'object')
            {
                var dob = utilityService.parseDateToStr($scope.customerAddModel.dob, 'yyyy-MM-dd');
            }

            createcustomerParam.dob = utilityService.changeDateToSqlFormat(dob, 'yyyy-MM-dd');
            if (createcustomerParam.dob == "" || createcustomerParam.dob == "Invalid date")
            {
                createcustomerParam.dob = null;
            }
            var special_date1 = "";
            var special_date2 = "";
            var special_date3 = "";
            var special_date4 = "";
            var special_date5 = "";
            if (typeof $scope.customerAddModel.special_date1 == 'object')
            {
                special_date1 = utilityService.parseDateToStr($scope.customerAddModel.special_date1, $scope.dateFormat);
            }
            createcustomerParam.special_date1 = utilityService.changeDateToSqlFormat(special_date1, $scope.dateFormat);
            if (createcustomerParam.special_date1 == "")
            {
                createcustomerParam.special_date1 = '';
            }
            if (typeof $scope.customerAddModel.special_date2 == 'object')
            {
                special_date2 = utilityService.parseDateToStr($scope.customerAddModel.special_date2, $scope.dateFormat);
            }
            createcustomerParam.special_date2 = utilityService.changeDateToSqlFormat(special_date2, $scope.dateFormat);
            if (createcustomerParam.special_date2 == "")
            {
                createcustomerParam.special_date2 = '';
            }
            if (typeof $scope.customerAddModel.special_date3 == 'object')
            {
                special_date3 = utilityService.parseDateToStr($scope.customerAddModel.special_date3, $scope.dateFormat);
            }
            createcustomerParam.special_date3 = utilityService.changeDateToSqlFormat(special_date3, $scope.dateFormat);
            if (createcustomerParam.special_date3 == "")
            {
                createcustomerParam.special_date3 = '';
            }
            if (typeof $scope.customerAddModel.special_date4 == 'object')
            {
                special_date4 = utilityService.parseDateToStr($scope.customerAddModel.special_date4, $scope.dateFormat);
            }
            createcustomerParam.special_date4 = utilityService.changeDateToSqlFormat(special_date4, $scope.dateFormat);
            if (createcustomerParam.special_date4 == "")
            {
                createcustomerParam.special_date4 = '';
            }
            if (typeof $scope.customerAddModel.special_date5 == 'object')
            {
                special_date5 = utilityService.parseDateToStr($scope.customerAddModel.special_date5, $scope.dateFormat);
            }
            createcustomerParam.special_date5 = utilityService.changeDateToSqlFormat(special_date5, $scope.dateFormat);
            if (createcustomerParam.special_date5 == "")
            {
                createcustomerParam.special_date5 = '';
            }

            if ($rootScope.appConfig.dual_language_support)
            {
                configOption = adminService.handleOnlyErrorResponseConfig;
            }
            adminService.addCustomer(createcustomerParam, configOption, headers).then(function (response) {

                if (response.data.success == true)
                {
                    $scope.showAlert = false;
                    $rootScope.$broadcast('genericSuccessEvent', response.data.message)
                    $scope.customerDualAddModel.customer_id = response.data.id;
                    if ($rootScope.appConfig.dual_language_support)
                    {
                        $scope.createcustomerDualInfo();
                    } else
                    {
                        $scope.formReset();
                        $state.go('app.vendor');
                        $scope.isDataSavingProcess = false;
//                        if ($rootScope.userModel.new_user == 1)
//                        {
//                            $window.localStorage.setItem("demo_guide", "true");
//                            $rootScope.closeDemoPopup(6);
//                        }
                    }
                } else
                {
                    $scope.isDataSavingProcess = false;
                    $rootScope.$broadcast('genericErrorEvent', response.data.message)
                    var element = document.getElementById("btnLoad");
                    element.classList.remove("btn-loader");
                }
            }).catch(function (response) {
                console.error('Error occurred:', response.status, response.data);
                $scope.isDataSavingProcess = false;
                var element = document.getElementById("btnLoad");
                element.classList.remove("btn-loader");
            });
        };


        $scope.createcustomerDualInfo = function () {
            $scope.isDataSavingProcess = true;
            var headers = {};
            headers['screen-code'] = 'vendor';
            var createcustomerParam = {};
            createcustomerParam.id = 0;
            createcustomerParam.customer_id = $scope.customerDualAddModel.customer_id;
            createcustomerParam.billing_address2 = $scope.customerDualAddModel.billingaddress2;
            createcustomerParam.billing_address3 = $scope.customerDualAddModel.billingaddress3;
            createcustomerParam.billing_company_name = $scope.customerDualAddModel.billing_company_name;
            createcustomerParam.billing_mail_id = $scope.customerDualAddModel.billing_mail_id;
            createcustomerParam.billing_website = $scope.customerDualAddModel.billing_website;
            createcustomerParam.billing_mobile_no = $scope.customerDualAddModel.billing_mobile_no;
            createcustomerParam.billing_phone_no = $scope.customerDualAddModel.billing_phone_no;
            createcustomerParam.shipping_address2 = $scope.customerDualAddModel.shippingaddress2;
            createcustomerParam.shipping_address3 = $scope.customerDualAddModel.shippingaddress3;
            createcustomerParam.shipping_mobile_no = $scope.customerDualAddModel.shipping_mobile_no;
            createcustomerParam.shipping_phone_no = $scope.customerDualAddModel.shipping_phone_no;
            createcustomerParam.shipping_mail_id = $scope.customerDualAddModel.shipping_mail_id;
            createcustomerParam.shipping_website = $scope.customerDualAddModel.shipping_website;
            createcustomerParam.shipping_company_name = $scope.customerDualAddModel.shipping_company_name;
            createcustomerParam.bankname = $scope.customerDualAddModel.bankname;
            createcustomerParam.bankacct = $scope.customerDualAddModel.account_number;
            createcustomerParam.bank_branch = $scope.customerDualAddModel.branch;
            createcustomerParam.bankcode = $scope.customerDualAddModel.ifsc_code;
            createcustomerParam.fname = $scope.customerDualAddModel.personname;
            createcustomerParam.designation = $scope.customerDualAddModel.designation;
            createcustomerParam.customer_type = 'suplier';
            createcustomerParam.company = $scope.customerDualAddModel.companyName;
            createcustomerParam.gst_no = $scope.customerAddModel.gst_no;
            createcustomerParam.email = $scope.customerAddModel.email;
            createcustomerParam.phone = $scope.customerAddModel.mobile;
            //     createcustomerParam.otherContact = $scope.customerAddModel.otherContact;
            createcustomerParam.address = $scope.customerAddModel.address;
            createcustomerParam.city = $scope.customerAddModel.city;
            createcustomerParam.state = $scope.customerAddModel.state;
            createcustomerParam.country = $scope.customerAddModel.country;
            createcustomerParam.zip = $scope.customerAddModel.postcode;
            createcustomerParam.tags = $scope.customerAddModel.tags;
            createcustomerParam.currency = $scope.customerAddModel.currency;
            createcustomerParam.group = $scope.customerAddModel.group;
            createcustomerParam.password = $scope.customerAddModel.password;
            createcustomerParam.confirmPassword = $scope.customerAddModel.confirmpassword;
            createcustomerParam.iswelcomeEmail = ($scope.customerAddModel.iswelcomeEmail == true ? 1 : 0);
//            if($scope.customerAddModel.customer_type == 'retailer')
//            {
//                createcustomerParam.is_purchase = 0;
//            }
//            else
//            {    
            createcustomerParam.is_purchase = 1;
//            }
            createcustomerParam.billing_address = $scope.customerAddModel.billingaddr;
            createcustomerParam.billing_city = $scope.customerAddModel.billingcity;
            createcustomerParam.billing_state = $scope.customerAddModel.billingstate;
            createcustomerParam.billing_country = $scope.customerAddModel.billingcountry;
            for (var i = 0; i < $scope.customerAddModel.billingCountryList.length; i++)
            {
                if ($scope.customerAddModel.billingcountry == $scope.customerAddModel.billingCountryList[i].name)
                {
                    createcustomerParam.billing_country_id = $scope.customerAddModel.billingCountryList[i].id;
                }
            }
            for (var i = 0; i < $scope.customerAddModel.billingStateList.length; i++)
            {
                if ($scope.customerAddModel.billingstate == $scope.customerAddModel.billingStateList[i].name)
                {
                    createcustomerParam.billing_state_id = $scope.customerAddModel.billingStateList[i].id;
                }
            }
            for (var i = 0; i < $scope.customerAddModel.billingCityList.length; i++)
            {
                if ($scope.customerAddModel.billingcity == $scope.customerAddModel.billingCityList[i].name)
                {
                    createcustomerParam.billing_city_id = $scope.customerAddModel.billingCityList[i].id;
                }
            }
            createcustomerParam.billing_pincode = $scope.customerAddModel.billingpostcode;
            createcustomerParam.shopping_address = $scope.customerAddModel.shoppingaddr;
            createcustomerParam.shopping_city = $scope.customerAddModel.shoppingcity;
            createcustomerParam.shopping_state = $scope.customerAddModel.shoppingstate;
            createcustomerParam.shopping_country = $scope.customerAddModel.shoppingcountry;
            createcustomerParam.shopping_pincode = $scope.customerAddModel.shoppingpostcode;
            for (var i = 0; i < $scope.customerAddModel.shoppingCountryList.length; i++)
            {
                if ($scope.customerAddModel.shoppingcountry == $scope.customerAddModel.shoppingCountryList[i].name)
                {
                    createcustomerParam.shopping_country_id = $scope.customerAddModel.shoppingCountryList[i].id;
                }
            }
            for (var i = 0; i < $scope.customerAddModel.shoppingStateList.length; i++)
            {
                if ($scope.customerAddModel.shoppingstate == $scope.customerAddModel.shoppingStateList[i].name)
                {
                    createcustomerParam.shopping_state_id = $scope.customerAddModel.shoppingStateList[i].id;
                }
            }
            for (var i = 0; i < $scope.customerAddModel.shoppingCityList.length; i++)
            {
                if ($scope.customerAddModel.shoppingcity == $scope.customerAddModel.shoppingCityList[i].name)
                {
                    createcustomerParam.shopping_city_id = $scope.customerAddModel.shoppingCityList[i].id;
                }
            }
            createcustomerParam.is_active = 1;
            createcustomerParam.customattribute = [];
            if (typeof $scope.customerAddModel.dob == 'object')
            {
                $scope.customerAddModel.dob = utilityService.parseDateToStr($scope.customerAddModel.dob, 'yyyy-MM-dd');
            }
            createcustomerParam.dob = utilityService.changeDateToSqlFormat($scope.customerAddModel.dob, 'yyyy-MM-dd');
            if (createcustomerParam.dob == "")
            {
                createcustomerParam.dob = null;
            }
            if (typeof $scope.customerAddModel.special_date1 == 'object')
            {
                $scope.customerAddModel.special_date1 = utilityService.parseDateToStr($scope.customerAddModel.special_date1, $scope.dateFormat);
            }
            createcustomerParam.special_date1 = utilityService.changeDateToSqlFormat($scope.customerAddModel.special_date1, $scope.dateFormat);
            if (createcustomerParam.special_date1 == "")
            {
                createcustomerParam.special_date1 = '';
            }
            if (typeof $scope.customerAddModel.special_date2 == 'object')
            {
                $scope.customerAddModel.special_date2 = utilityService.parseDateToStr($scope.customerAddModel.special_date2, $scope.dateFormat);
            }
            createcustomerParam.special_date2 = utilityService.changeDateToSqlFormat($scope.customerAddModel.special_date2, $scope.dateFormat);
            if (createcustomerParam.special_date2 == "")
            {
                createcustomerParam.special_date2 = '';
            }
            if (typeof $scope.customerAddModel.special_date3 == 'object')
            {
                $scope.customerAddModel.special_date3 = utilityService.parseDateToStr($scope.customerAddModel.special_date3, $scope.dateFormat);
            }
            createcustomerParam.special_date3 = utilityService.changeDateToSqlFormat($scope.customerAddModel.special_date3, $scope.dateFormat);
            if (createcustomerParam.special_date3 == "")
            {
                createcustomerParam.special_date3 = '';
            }
            if (typeof $scope.customerAddModel.special_date4 == 'object')
            {
                $scope.customerAddModel.special_date4 = utilityService.parseDateToStr($scope.customerAddModel.special_date4, $scope.dateFormat);
            }
            createcustomerParam.special_date4 = utilityService.changeDateToSqlFormat($scope.customerAddModel.special_date4, $scope.dateFormat);
            if (createcustomerParam.special_date4 == "")
            {
                createcustomerParam.special_date4 = '';
            }
            if (typeof $scope.customerAddModel.special_date5 == 'object')
            {
                $scope.customerAddModel.special_date5 = utilityService.parseDateToStr($scope.customerAddModel.special_date5, $scope.dateFormat);
            }
            createcustomerParam.special_date5 = utilityService.changeDateToSqlFormat($scope.customerAddModel.special_date5, $scope.dateFormat);
            if (createcustomerParam.special_date5 == "")
            {
                createcustomerParam.special_date5 = '';
            }
            createcustomerParam.acode = '';
            if (typeof $scope.customerAddModel.acode != undefined && $scope.customerAddModel.acode != null && $scope.customerAddModel.acode != '')
            {
                createcustomerParam.acode = $scope.customerAddModel.acode.code;
            }
            for (var i = 0; i < $scope.customAttributeList.length; i++)
            {
                var attribute_prefix = $scope.customAttributeList[i].attribute_code.split('_')[0];
                if (attribute_prefix == $rootScope.appConfig.dual_language_label)
                {
                    var customattributeItem = {};

                    if ($scope.customAttributeList[i].value != undefined)
                    {
                        customattributeItem.value = $scope.customAttributeList[i].value;
                    } else if ($scope.customAttributeList[i].default_value != undefined)
                    {
                        customattributeItem.value = $scope.customAttributeList[i].default_value;
                    } else
                    {
                        customattributeItem.value = "";
                    }

                    customattributeItem.attribute_code = $scope.customAttributeList[i].attribute_code;
                    createcustomerParam.customattribute.push(customattributeItem);

                }



            }
            var configOption = adminService.handleBothSuccessAndErrorResponseConfig;
            adminService.addCustomerDualInfo(createcustomerParam, configOption, headers).then(function (response) {

                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.vendor');
                    if ($rootScope.userModel.new_user == 1)
                    {
                        $window.localStorage.setItem("demo_guide", "true");
                        $rootScope.closeDemoPopup(3);
                    }
                }
                $scope.isDataSavingProcess = false;
                var element = document.getElementById("btnLoad");
                element.classList.remove("btn-loader");

            }).catch(function (response) {
                console.error('Error occurred:', response.status, response.data);
                $scope.isDataSavingProcess = false;
                var element = document.getElementById("btnLoad");
                element.classList.remove("btn-loader");
            });
        };



        $scope.getCustomAttributeList = function () {

            var getListParam = {};
            getListParam.id = "";
            getListParam.attributetype_id = "";
            getListParam.attribute_code = "";
            getListParam.attributetypecode = "crm";
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getAttributeList(getListParam, configOption).then(function (response)
            {
                var data = response.data.list;
                $scope.customAttributeList = data;
                $scope.customAttributeBaseList = [];
                $scope.customAttributeDualSupportList = [];

                if ($rootScope.appConfig.dual_language_support)
                {
                    for (var i = 0; i < $scope.customAttributeList.length; i++)
                    {
                        var attribute_prefix = $scope.customAttributeList[i].attribute_code.split('_')[0];
                        if (attribute_prefix == $rootScope.appConfig.dual_language_label)
                        {
                            $scope.customAttributeDualSupportList.push($scope.customAttributeList[i]);
                        } else
                        {
                            $scope.customAttributeBaseList.push($scope.customAttributeList[i]);
                        }
                    }
                } else
                {
                    $scope.customAttributeBaseList = $scope.customAttributeList;
                }

            });
        };

        $scope.$on('customSelectOptionChangeEvent', function (event, data)
        {
            console.log(data);
            $scope.updateCustomSelectValue(data);
        });

        $scope.updateCustomSelectValue = function (data)
        {
            for (var i = 0; i < $scope.customAttributeList.length; i++)
            {
                if ($scope.customAttributeList[i].attribute_code == data.code)
                {
                    $scope.customAttributeList[i].value = data.value;
                    break;
                }
            }
        }

        $scope.getCustomAttributeList();

        /*
         * Listening the Custom field value change
         */
        $scope.$on('customfieldValueChangeEvent', function (event, data)
        {
            console.log(data);
            $scope.updateCustomFieldValue(data);
        });

        $scope.updateCustomFieldValue = function (data)
        {
            for (var i = 0; i < $scope.customAttributeList.length; i++)
            {
                if ($scope.customAttributeList[i].attribute_code == data.code)
                {
                    $scope.customAttributeList[i].value = data.value;
                    break;
                }
            }
        }




        $scope.getCountryList = function () {

            var getListParam = {};
            getListParam.id = "";
            getListParam.name = "";
            getListParam.code = "";
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getCountryList(getListParam, configOption).then(function (response)
            {
                var data = response.data.list;

                $timeout(function () {
                    $scope.customerAddModel.billingCountryList = data;
//                    if ($scope.customerAddModel.billingCountryList.length == 1)
//                    {
//                        $scope.customerAddModel.billingcountry = $scope.customerAddModel.billingCountryList[0].name;
//                    }
                    if($scope.customerAddModel.billingcountry == "" || $scope.customerAddModel.billingcountry == null || typeof $scope.customerAddModel.billingcountry == "undefined")
                    {
                        for (var i = 0; i < $scope.customerAddModel.billingCountryList.length; i++)
                        {
                            if ($scope.customerAddModel.billingCountryList[i].is_default == 1)
                            {
                                $scope.customerAddModel.billingcountry = $scope.customerAddModel.billingCountryList[i].name;
                                $scope.customerAddModel.shoppingcountry = $scope.customerAddModel.billingCountryList[i].name;
                                $scope.customerDualAddModel.billingcountry = $scope.customerAddModel.billingCountryList[i].name;
                                $scope.customerDualAddModel.shoppingcountry = $scope.customerAddModel.billingCountryList[i].name;
                            }
                        }
                    }
                    else
                    {
                        $scope.customerAddModel.billingcountry = $scope.customerAddModel.billingcountry;
                        $scope.customerAddModel.shoppingcountry = $scope.customerAddModel.billingcountry;
                    }
                    $scope.customerAddModel.shoppingCountryList = data;

                    $scope.customerDualAddModel.billingCountryList = data;
                    $scope.customerDualAddModel.shoppingCountryList = data;

                });
            });
        };


        $scope.initBillingAddressStateListTimeoutPromise = null;

        $scope.initBillingAddressStateList = function (searchkey)
        {
            if ($scope.initBillingAddressStateListTimeoutPromise != null)
            {
                $timeout.cancel($scope.initBillingAddressStateListTimeoutPromise);
            }
            $scope.initBillingAddressStateListTimeoutPromise = $timeout(function () {
                $scope.getStateList(searchkey, 'billingstate');
            }, 100);

        };

        $scope.initShippingAddressStateListTimeoutPromise = null;

        $scope.initShippingAddressStateList = function (searchkey)
        {
            if ($scope.initShippingAddressStateListTimeoutPromise != null)
            {
                $timeout.cancel($scope.initShippingAddressStateListTimeoutPromise);
            }
            $scope.initShippingAddressStateListTimeoutPromise = $timeout(function () {
                $scope.getStateList(searchkey, 'shippingstate');
            }, 100);

        };

        $scope.initDualBillingAddressStateListTimeoutPromise = null;

        $scope.initDualBillingAddressStateList = function (searchkey)
        {
            if ($scope.initDualBillingAddressStateListTimeoutPromise != null)
            {
                $timeout.cancel($scope.initDualBillingAddressStateListTimeoutPromise);
            }
            $scope.initDualBillingAddressStateListTimeoutPromise = $timeout(function () {
                $scope.getStateList(searchkey, 'dualbillingstate');
            }, 100);

        };

        $scope.initDualShippingAddressStateListTimeoutPromise = null;

        $scope.initDualShippingAddressStateList = function (searchkey)
        {
            if ($scope.initDualShippingAddressStateListTimeoutPromise != null)
            {
                $timeout.cancel($scope.initDualShippingAddressStateListTimeoutPromise);
            }
            $scope.initDualShippingAddressStateListTimeoutPromise = $timeout(function () {
                $scope.getStateList(searchkey, 'dualshippingstate');
            }, 100);

        };


        $rootScope.billingState = false;
        $rootScope.shippingState = false;
        $rootScope.dualbillingState = false;
        $rootScope.dualshippingstate = false;
        $scope.getStateList = function (searchkey, fieldName) {

            console.log('searchkey  = ' + searchkey);
            console.log('fieldName  = ' + fieldName);

            var getListParam = {};
            getListParam.id = "";
            getListParam.name = searchkey;
            getListParam.country_name = "";

            if (fieldName == 'billingstate')
            {
                getListParam.country_name = $scope.customerAddModel.billingcountry;
            } else if (fieldName == 'shippingstate')
            {
                getListParam.country_name = $scope.customerAddModel.shoppingcountry;
            } else if (fieldName == 'dualbillingstate')
            {
                getListParam.country_name = $scope.customerDualAddModel.shoppingcountry;
            } else if (fieldName == 'dualshippingstate')
            {
                getListParam.country_name = $scope.customerDualAddModel.shoppingcountry;
            }

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            configOption.fieldName = fieldName;
            configOption.searchkey = searchkey;

            adminService.getStateList(getListParam, configOption).then(function (response)
            {
                if (!response.data.success)
                {
                    return;
                }
                var data = response.data.list;
                if (typeof response.config != 'undefined' && typeof response.config.config != 'undefined'
                        && response.config.config.fieldName != undefined && response.config.config.searchkey != undefined)
                {

//                    if (data.length == 0)
//                    {
//                        data.push({"name": response.config.config.searchkey});
//                    }
                    $timeout(function () {
                        if (fieldName == 'billingstate')
                        {
                            $scope.customerAddModel.billingStateList = data;
//                            if ($scope.customerAddModel.billingStateList.length == 1)
//                            {
//                                $scope.customerAddModel.billingstate = $scope.customerAddModel.billingStateList[0].name;
//                            }
                            if ($rootScope.billingState == false)
                            {
                                for (var i = 0; i < $scope.customerAddModel.billingStateList.length; i++)
                                {
                                    if ($scope.customerAddModel.billingStateList[i].is_default == 1)
                                    {
                                        $scope.customerAddModel.billingstate = $scope.customerAddModel.billingStateList[i].name;
                                        $rootScope.billingState = true;
                                    }
                                }
                            }

                        } else if (fieldName == 'shippingstate')
                        {
                            $scope.customerAddModel.shoppingStateList = data;
                            if ($rootScope.shippingState == false)
                            {
                                for (var i = 0; i < $scope.customerAddModel.shoppingStateList.length; i++)
                                {
                                    if ($scope.customerAddModel.shoppingStateList[i].is_default == 1)
                                    {
                                        $scope.customerAddModel.shoppingstate = $scope.customerAddModel.shoppingStateList[i].name;
                                        $rootScope.shippingState = true;
                                    }
                                }

                            }
                        } else if (fieldName == 'dualbillingstate')
                        {
                            $scope.customerDualAddModel.billingStateList = data;
                            if ($rootScope.dualbillingstate == false)
                            {
                                for (var i = 0; i < $scope.customerDualAddModel.billingStateList.length; i++)
                                {
                                    if ($scope.customerDualAddModel.billingStateList[i].is_default == 1)
                                    {
                                        $scope.customerDualAddModel.billingstate = $scope.customerDualAddModel.billingStateList[i].name;
                                        $rootScope.dualbillingstate = true;
                                    }
                                }
                            }
                        } else if (fieldName == 'dualshippingstate')
                        {
                            $scope.customerDualAddModel.shoppingStateList = data;
                            if ($rootScope.dualshippingstate == false)
                            {
                                for (var i = 0; i < $scope.customerDualAddModel.shoppingStateList.length; i++)
                                {
                                    if ($scope.customerDualAddModel.shoppingStateList[i].is_default == 1)
                                    {
                                        $scope.customerDualAddModel.shoppingstate = $scope.customerDualAddModel.shoppingStateList[i].name;
                                        $rootScope.dualshippingstate = true;
                                    }
                                }
                            }
                        }
                    }, 0);
                }
            });

        };
        $scope.customerAddModel.billingstate = $rootScope.appConfig.gst_state_name;
        $scope.customerDualAddModel.billingstate = $rootScope.appConfig.gst_state_name;

        $scope.initBillingAddressCityListTimeoutPromise = null;

        $scope.initBillingAddressCityList = function (searchkey)
        {
            if ($scope.initBillingAddressCityListTimeoutPromise != null)
            {
                $timeout.cancel($scope.initBillingAddressCityListTimeoutPromise);
            }
            $scope.initBillingAddressCityListTimeoutPromise = $timeout(function () {
                $scope.getCityList(searchkey, 'billingcity');
            }, 300);

        };
        $scope.initShippingAddressCityListTimeoutPromise = null;

        $scope.initShippingAddressCityList = function (searchkey)
        {
            if ($scope.initShippingAddressCityListTimeoutPromise != null)
            {
                $timeout.cancel($scope.initShippingAddressCityListTimeoutPromise);
            }
            $scope.initShippingAddressCityListTimeoutPromise = $timeout(function () {
                $scope.getCityList(searchkey, 'shippingcity');
            }, 300);

        };

        $scope.initDualBillingAddressCityListTimeoutPromise = null;

        $scope.initDualBillingAddressCityList = function (searchkey)
        {
            if ($scope.initDualBillingAddressCityListTimeoutPromise != null)
            {
                $timeout.cancel($scope.initDualBillingAddressCityListTimeoutPromise);
            }
            $scope.initDualBillingAddressCityListTimeoutPromise = $timeout(function () {
                $scope.getCityList(searchkey, 'dualbillingcity');
            }, 300);

        };
        $scope.initDualShippingAddressCityListTimeoutPromise = null;

        $scope.initDualShippingAddressCityList = function (searchkey)
        {
            if ($scope.initDualShippingAddressCityListTimeoutPromise != null)
            {
                $timeout.cancel($scope.initDualShippingAddressCityListTimeoutPromise);
            }
            $scope.initDualShippingAddressCityListTimeoutPromise = $timeout(function () {
                $scope.getCityList(searchkey, 'dualshippingcity');
            }, 300);

        };


        $scope.getCityList = function (searchkey, fieldName) {

            console.log('searchkey  = ' + searchkey);
            console.log('fieldName  = ' + fieldName);

            var getListParam = {};
            getListParam.id = "";
            getListParam.name = searchkey;
            getListParam.country_name = "";
            getListParam.state_name = "";

            if (fieldName == 'billingcity')
            {
                getListParam.state_name = $scope.customerAddModel.billingstate;
                getListParam.country_name = $scope.customerAddModel.billingcountry;
            } else if (fieldName == 'shippingcity')
            {
                getListParam.state_name = $scope.customerAddModel.shoppingstate;
                getListParam.country_name = $scope.customerAddModel.shoppingcountry;
            } else if (fieldName == 'dualbillingcity')
            {
                getListParam.state_name = $scope.customerDualAddModel.billingstate;
                getListParam.country_name = $scope.customerDualAddModel.billingcountry;
            } else if (fieldName == 'dualshippingcity')
            {
                getListParam.state_name = $scope.customerDualAddModel.shoppingstate;
                getListParam.country_name = $scope.customerDualAddModel.shoppingcountry;
            }

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            configOption.fieldName = fieldName;
            configOption.searchkey = searchkey;

            adminService.getCityList(getListParam, configOption).then(function (response)
            {
                if (!response.data.success)
                {
                    return;
                }
                var data = response.data.list;
                if (typeof response.config != 'undefined' && typeof response.config.config != 'undefined'
                        && response.config.config.fieldName != undefined && response.config.config.searchkey != undefined)
                {

//                    if (data.length == 0)
//                    {
//                        data.push({"name": response.config.config.searchkey});
//                    }
                    $timeout(function () {
                        if (fieldName == 'billingcity')
                        {
                            $scope.customerAddModel.billingCityList = data;
                            if ($scope.customerAddModel.billingCityList.length == 1)
                            {
                                $scope.customerAddModel.billingcity = $scope.customerAddModel.billingCityList[0].name;
                            }
                        } else if (fieldName == 'shippingcity')
                        {
                            $scope.customerAddModel.shoppingCityList = data;
                        } else if (fieldName == 'dualbillingcity')
                        {
                            $scope.customerDualAddModel.billingCityList = data;
                        } else if (fieldName == 'dualshippingcity')
                        {
                            $scope.customerDualAddModel.shoppingCityList = data;
                        }
                    }, 0);

                }

            });

        };

        $scope.billingAddressCountryChange = function ()
        {
            $scope.customerAddModel.billingstate = '';
            $scope.customerAddModel.billingcity = '';

            $scope.customerAddModel.billingCityList = [];
            $scope.customerAddModel.billingStateList = [];

            $scope.checkAddress();
        }

        $scope.billingAddressStateChange = function ()
        {

            $scope.customerAddModel.billingcity = '';
            $scope.customerAddModel.billingCityList = [];

            $scope.checkAddress();

        }

        $scope.shippingAddressCountryChange = function ()
        {
            $scope.customerAddModel.shoppingstate = '';
            $scope.customerAddModel.shoppingcity = '';

            $scope.customerAddModel.shoppingCityList = [];
            $scope.customerAddModel.shoppingStateList = [];

            $scope.updateShoppingAddr('update');
        }

        $scope.shippingAddressStateChange = function ()
        {
            $scope.customerAddModel.shoppingcity = '';
            $scope.customerAddModel.shoppingCityList = [];

            $scope.updateShoppingAddr('update');
        }

        $scope.selectflag = false;
        $scope.checkSelect = function ()
        {
            if ($scope.customerAddModel.billingcity == '' || $scope.customerAddModel.billingcity == undefined) {
                $scope.selectFlag = true;
            } else {
                $scope.selectFlag = false;
            }
        };
        $scope.checkdeselect = function ()
        {
            if ($scope.customerAddModel.billingcity != '' || $scope.customerAddModel.billingcity != undefined) {
                $scope.selectFlag = false;
            } else {
                $scope.selectFlag = true;
            }
        };

        $scope.getCountryList();

    }]);




