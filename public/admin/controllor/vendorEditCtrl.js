app.controller('vendorEditCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', '$window', 'utilityService', function ($scope, $rootScope, adminService, $httpService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST, $window, utilityService) {
        $scope.customerEditModel = {
            "id": "",
            "name": "",
            "companyname": "",
            "customer_type": "",
            "email": "",
            "phone": "",
            "other_contact_no": "",
            "city": "",
            "state": "",
            "country": "",
            "address": "",
            "tags": "",
            "currency": "",
            "group": "",
            "password": "",
            "confirmpassword": "",
            "postcode": "",
            "iswelcomeEmail": '',
            showPassword: false,
            customerInfo: [],
            userList: [],
            categorylist: [],
            "isactive": 1,
            is_purchase: '',
            is_sale: '',
            isLoadingProgress: false,
            "img": [],
            "billingaddr": '',
            "billingcity": '',
            "billingstate": '',
            "billingpostcode": '',
            "billingcountry": '',
            "shoppingaddr": '',
            "shoppingcity": '',
            "shoppingstate": '',
            "shoppingpostcode": '',
            "shoppingcountry": '',
            "isSameAddr": '',
            "billingCountryList": [],
            "billingStateList": [],
            "billingCityList": [],
            "shoppingCountryList": [],
            "shoppingStateList": [],
            "shoppingCityList": [],
            "acode": '',
            "dob": '',
            "special_date1": '',
            "special_date2": '',
            "special_date3": '',
            "special_date4": '',
            "special_date5": '',
            "billingaddress2": '',
            "billingaddress3": '',
            "billing_company_name": '',
            "billing_mail_id": '',
            "billing_website": '',
            "billing_mobile_no": '',
            "billing_phone_no": '',
            "shippingaddress2": '',
            "shippingaddress3": '',
            "shipping_mobile_no": '',
            "shipping_phone_no": '',
            "shipping_mail_id": '',
            "shipping_website": '',
            "shipping_company_name": '',
            "bankname": '',
            "account_number": '',
            "branch": '',
            "ifsccode": '',
            "personname": '',
            "designation": ''
        }

        $scope.customerDualEditModel = {
            "id": "",
            "customer_id": 0,
            "name": "",
            "companyname": "",
            "customer_type": "",
            "email": "",
            "phone": "",
            "city": "",
            "state": "",
            "country": "",
            "address": "",
            "tags": "",
            "currency": "",
            "group": "",
            "password": "",
            "confirmpassword": "",
            "postcode": "",
            "iswelcomeEmail": '',
            showPassword: false,
            customerInfo: [],
            userList: [],
            categorylist: [],
            "isactive": 1,
            is_purchase: '',
            is_sale: '',
            isLoadingProgress: false,
            "img": [],
            "billingaddr": '',
            "billingcity": '',
            "billingstate": '',
            "billingpostcode": '',
            "billingcountry": '',
            "shoppingaddr": '',
            "shoppingcity": '',
            "shoppingstate": '',
            "shoppingpostcode": '',
            "shoppingcountry": '',
            "isSameAddr": '',
            "billingCountryList": [],
            "billingStateList": [],
            "billingCityList": [],
            "shoppingCountryList": [],
            "shoppingStateList": [],
            "shoppingCityList": [],
            "acode": '',
            "dob": '',
            "special_date1": '',
            "special_date2": '',
            "special_date3": '',
            "special_date4": '',
            "special_date5": '',
            "billingaddress2": '',
            "billingaddress3": '',
            "billing_company_name": '',
            "billing_mail_id": '',
            "billing_website": '',
            "billing_mobile_no": '',
            "billing_phone_no": '',
            "shippingaddress2": '',
            "shippingaddress3": '',
            "shipping_mobile_no": '',
            "shipping_phone_no": '',
            "shipping_mail_id": '',
            "shipping_website": '',
            "shipping_company_name": '',
            "bankname": '',
            "account_number": '',
            "branch": '',
            "ifsccode": '',
            "personname": '',
            "designation": ''
        }

        $scope.customAttributeList = [];
        $scope.customAttributeBaseList = [];
        $scope.customAttributeDualSupportList = [];

        $scope.validationFactory = ValidationFactory;
        $scope.getNavigationBlockMsg = function (pageReload)
        {
            if (typeof $scope.modify_customer_form != 'undefined' && typeof $scope.modify_customer_form.$pristine != 'undefined' && !$scope.modify_customer_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.formReset = function ()
        {
            if (typeof $scope.modify_customer_form != 'undefined')
            {
                $scope.modify_customer_form.$setPristine();
                $scope.getCustomerInfo( );
            }
        }
        $scope.showUploadFilePopup = false;
        $scope.showPhotoGalaryPopup = false;
        $scope.showPopup = function (value) {

            if (value == 'fileupload')
            {
                $scope.showUploadFilePopup = true;
            } else if (value == 'photogalary')
            {
                $scope.showPhotoGalaryPopup = true;
            }
        }
        $scope.isOthersDetail = false;
        $scope.showOthersInfo = function ()
        {
            $scope.isOthersDetail = !$scope.isOthersDetail;
        }
        $scope.closePopup = function (value) {

            if (value == 'fileupload')
            {
                $scope.showUploadFilePopup = false;

            } else if (value == 'photogalary')
            {
                $scope.showPhotoGalaryPopup = false;
            }

        }
        $scope.uploadedFileQueue = [];
        $scope.uploadedFileCount = 0;
        $scope.isImageSavingProcess = false;
        $scope.isImageUploadComplete = false;
        $scope.$on("updateFileUploadEvent", updateFileUploadEventHandler);
        function updateFileUploadEventHandler(event, data)
        {
            $scope.uploadedFileQueue = data;
            console.log('$scope.uploadedFileQueue');
            console.log($scope.uploadedFileQueue);
            $scope.validateFileUploadStatus();
        }
        $scope.validateFileUploadStatus = function ()
        {
            $scope.isImageSavingProcess = true;
            var hasUploadCompleted = true;
            for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
            {
                if (!$scope.uploadedFileQueue[i].isSuccess)
                {
                    hasUploadCompleted = false;
                    $scope.$broadcast('genericErrorEvent', 'Image upload is in progress.');
                    break;
                }
            }
            if (hasUploadCompleted && $scope.uploadedFileQueue.length > 0)
            {
                $scope.uploadedFileCount = $scope.uploadedFileQueue.length;
                $scope.isImageSavingProcess = false;
                $scope.isImageUploadComplete = true;
            }

        }
        $scope.dobDateOpen = false;
        $scope.specialDate1 = false;
        $scope.specialDate2 = false;
        $scope.specialDate3 = false;
        $scope.specialDate4 = false;
        $scope.specialDate5 = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.openDate = function (index) {

            if (index == 0)
            {
                $scope.dobDateOpen = true;
            }
            if (index == 1)
            {
                $scope.specialDate1 = true;
            }
            if (index == 2)
            {
                $scope.specialDate2 = true;
            }
            if (index == 3)
            {
                $scope.specialDate3 = true;
            }
            if (index == 4)
            {
                $scope.specialDate4 = true;
            }
            if (index == 5)
            {
                $scope.specialDate5 = true;
            }
        }
        $scope.saveImagePath = function ()
        {
            if ($scope.uploadedFileQueue.length == 0)
            {
                $scope.closePopup('fileupload');
            }
            var hasUploadCompleted = true;
            for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
            {
                if (!$scope.uploadedFileQueue[i].isSuccess)
                {
                    hasUploadCompleted = false;
                    $scope.$broadcast('genericErrorEvent', 'Image upload is in progress.');
                    break;
                }
            }
            if (hasUploadCompleted && $scope.uploadedFileQueue.length > 0)
            {
                $scope.customerEditModel.img = $scope.uploadedFileQueue;
                $scope.closePopup('fileupload');
            }
        }

        //        $scope.isCategoryLoaded = false;
        //        $scope.getCategoryList = function() {
        //            var categoryListParam = {};
        //            categoryListParam.id = "";
        //            categoryListParam.name = "";
        //            categoryListParam.start = 0;
        //            categoryListParam.limit = 100;
        //            adminService.getCategoryList(categoryListParam).then(function(response) {
        //                var data = response.data.list;
        //                $scope.customerEditModel.categorylist = data;
        //                $scope.isCategoryLoaded = true;
        //            });
        //
        //        };
        $scope.checkAddress = function ()
        {
            if ($scope.customerEditModel.isSameAddr == true)
            {
                $scope.updateShoppingAddr();
            }
        }
        $scope.updateShoppingAddr = function (string)
        {
            if ($scope.customerEditModel.isSameAddr == true)
            {
                $scope.customerEditModel.shoppingaddr = $scope.customerEditModel.billingaddr;
                $scope.customerEditModel.shoppingcity = $scope.customerEditModel.billingcity;
                $scope.customerEditModel.shoppingstate = $scope.customerEditModel.billingstate;
                $scope.customerEditModel.shoppingpostcode = $scope.customerEditModel.billingpostcode;
                $scope.customerEditModel.shoppingcountry = $scope.customerEditModel.billingcountry;
                $scope.customerEditModel.shipping_company_name = $scope.customerEditModel.companyName;
                $scope.customerEditModel.shippingaddress2 = $scope.customerEditModel.billingaddress2;
                $scope.customerEditModel.shippingaddress3 = $scope.customerEditModel.billingaddress3;
                $scope.customerEditModel.shipping_mobile_no = $scope.customerEditModel.billing_mobile_no;
                $scope.customerEditModel.shipping_phone_no = $scope.customerEditModel.billing_phone_no;
                $scope.customerEditModel.shipping_website = $scope.customerEditModel.billing_website;
                $scope.customerEditModel.shipping_mail_id = $scope.customerEditModel.billing_mail_id;

                $scope.customerDualEditModel.shoppingaddr = $scope.customerDualEditModel.billingaddr;
                $scope.customerDualEditModel.shoppingcity = $scope.customerDualEditModel.billingcity;
                $scope.customerDualEditModel.shoppingstate = $scope.customerDualEditModel.billingstate;
                $scope.customerDualEditModel.shoppingpostcode = $scope.customerDualEditModel.billingpostcode;
                $scope.customerDualEditModel.shoppingcountry = $scope.customerDualEditModel.billingcountry;
                $scope.customerEditModel.shipping_company_name = $scope.customerEditModel.companyName;
                $scope.customerEditModel.shippingaddress2 = $scope.customerEditModel.billingaddress2;
                $scope.customerEditModel.shippingaddress3 = $scope.customerEditModel.billingaddress3;
                $scope.customerEditModel.shipping_mobile_no = $scope.customerEditModel.billing_mobile_no;
                $scope.customerEditModel.shipping_phone_no = $scope.customerEditModel.billing_phone_no;
                $scope.customerEditModel.shipping_website = $scope.customerEditModel.billing_website;
                $scope.customerEditModel.shipping_mail_id = $scope.customerEditModel.billing_mail_id;
            }  else if ($scope.customerEditModel.isSameAddr == false)
            {
                if (string != 'update')
                {
                    $scope.customerEditModel.shoppingaddr = '';
                    $scope.customerEditModel.shoppingaddr = '';
                    $scope.customerEditModel.shoppingcity = '';
                    $scope.customerEditModel.shoppingstate = '';
                    $scope.customerEditModel.shoppingpostcode = '';
                    $scope.customerEditModel.shoppingcountry = '';
                    $scope.customerEditModel.shipping_company_name = '';
                    $scope.customerEditModel.shippingaddress2 = '';
                    $scope.customerEditModel.shippingaddress3 = '';
                    $scope.customerEditModel.shipping_mobile_no = '';
                    $scope.customerEditModel.shipping_phone_no = '';
                    $scope.customerEditModel.shipping_website = '';
                    $scope.customerEditModel.shipping_mail_id = '';
                }
                $scope.customerDualEditModel.shoppingaddr = '';
                $scope.customerDualEditModel.shoppingcity = '';
                $scope.customerDualEditModel.shoppingstate = '';
                $scope.customerDualEditModel.shoppingpostcode = '';
                $scope.customerDualEditModel.shoppingcountry = '';
                $scope.customerDualEditModel.shipping_company_name = '';
                $scope.customerDualEditModel.shippingaddress2 = '';
                $scope.customerDualEditModel.shippingaddress3 = '';
                $scope.customerDualEditModel.shipping_mobile_no = '';
                $scope.customerDualEditModel.shipping_phone_no = '';
                $scope.customerDualEditModel.shipping_website = '';
                $scope.customerDualEditModel.shipping_mail_id = '';
            }
        }

        $scope.initUpdateDetailTimeoutPromise = null;

        $scope.initUpdateDetail = function ()
        {

            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
            }

            if ( $scope.customerDualInfoLoaded && $scope.countryLoaded && $scope.cityLoaded && $scope.stateLoaded)
            {
                $scope.updateCustomerInfo( );
            } else
            {
                $scope.initUpdateDetailTimeoutPromise = $timeout($scope.initUpdateDetail, 300);
            }
        }

        $scope.updateCustomerInfo = function ( )
        {
            $scope.customAttributeList = $scope.customerEditModel.customerInfo.customattribute;

            $scope.customerEditModel.billingaddr = $scope.customerEditModel.customerInfo.billing_address;
            if ($scope.customerEditModel.customerInfo.billing_country == null || $scope.customerEditModel.customerInfo.billing_country == '')
            {
                if ($scope.customerEditModel.billingCountryList.length == 1)
                {
                    $scope.customerEditModel.billingcountry = $scope.customerEditModel.billingCountryList[0].name;
                }
            } else
            {
                $scope.customerEditModel.billingcountry = $scope.customerEditModel.customerInfo.billing_country;
            }
            if ($scope.customerEditModel.customerInfo.billing_state == 0 || $scope.customerEditModel.customerInfo.billing_state == '')
            {
                if ($scope.customerEditModel.billingStateList.length == 1)
                {
                    $scope.customerEditModel.billingstate = $scope.customerEditModel.billingStateList[0].name;
                    $rootScope.billingState = true;
                }
            } else
            {
                $scope.customerEditModel.billingstate = $scope.customerEditModel.customerInfo.billing_state;
                $rootScope.billingState = true;
            }
            if ($scope.customerEditModel.customerInfo.billing_city == 0 || $scope.customerEditModel.customerInfo.billing_city == '')
            {
                if ($scope.customerEditModel.billingCityList.length == 1)
                {
                    $scope.customerEditModel.billingcity = $scope.customerEditModel.billingCityList[0].name;
                }
            } else
            {
                $scope.customerEditModel.billingcity = $scope.customerEditModel.customerInfo.billing_city;
            }
            $scope.customerEditModel.billingaddress2 = $scope.customerEditModel.customerInfo.billing_address2;
            $scope.customerEditModel.billingaddress3 = $scope.customerEditModel.customerInfo.billing_address3;
            $scope.customerEditModel.companyName = $scope.customerEditModel.customerInfo.company;
            $scope.customerEditModel.billing_mail_id = $scope.customerEditModel.customerInfo.billing_mail_id;
            $scope.customerEditModel.billing_website = $scope.customerEditModel.customerInfo.billing_website;
            $scope.customerEditModel.billing_mobile_no = $scope.customerEditModel.customerInfo.billing_mobile_no;
            $scope.customerEditModel.billing_phone_no = $scope.customerEditModel.customerInfo.billing_phone_no;
            $scope.customerEditModel.shippingaddress2 = $scope.customerEditModel.customerInfo.shipping_address2;
            $scope.customerEditModel.shippingaddress3 = $scope.customerEditModel.customerInfo.shipping_address3;
            $scope.customerEditModel.shipping_mobile_no = $scope.customerEditModel.customerInfo.shipping_mobile_no;
            $scope.customerEditModel.shipping_phone_no = $scope.customerEditModel.customerInfo.shipping_phone_no;
            $scope.customerEditModel.shipping_mail_id = $scope.customerEditModel.customerInfo.shipping_mail_id;
            $scope.customerEditModel.shipping_website = $scope.customerEditModel.customerInfo.shipping_website;
            $scope.customerEditModel.shipping_company_name = $scope.customerEditModel.customerInfo.shipping_company_name;
            $scope.customerEditModel.bankname = $scope.customerEditModel.customerInfo.bankname;
            $scope.customerEditModel.account_number = $scope.customerEditModel.customerInfo.bankacct;
            $scope.customerEditModel.branch = $scope.customerEditModel.customerInfo.bank_branch;
            $scope.customerEditModel.ifsc_code = $scope.customerEditModel.customerInfo.bankcode;
            $scope.customerEditModel.personname = $scope.customerEditModel.customerInfo.fname;
            $scope.customerEditModel.designation = $scope.customerEditModel.customerInfo.jobtitle;
            $scope.customerEditModel.customer_type = 'suplier';
            $scope.customerEditModel.billingpostcode = $scope.customerEditModel.customerInfo.billing_pincode;
            $scope.customerEditModel.shoppingaddr = $scope.customerEditModel.customerInfo.shopping_address;
            $scope.customerEditModel.shoppingcity = $scope.customerEditModel.customerInfo.shopping_city;
            $scope.customerEditModel.shoppingstate = $scope.customerEditModel.customerInfo.shopping_state;
            $scope.customerEditModel.shoppingcountry = $scope.customerEditModel.customerInfo.shopping_country;
            $scope.customerEditModel.shoppingpostcode = $scope.customerEditModel.customerInfo.shopping_pincode;
            $scope.customerEditModel.acode = $scope.customerEditModel.customerInfo.acode;
            $scope.customerEditModel.email = $scope.customerEditModel.customerInfo.email;
            $scope.customerEditModel.mobile = $scope.customerEditModel.customerInfo.phone;
            $scope.customerEditModel.gst_no = $scope.customerEditModel.customerInfo.gst_no;
            var dob1 = new Date($scope.customerEditModel.customerInfo.dob);
            if (dob1 > 0)
            {
                $scope.customerEditModel.dob = new Date($scope.customerEditModel.customerInfo.dob);
            } else
                $scope.customerEditModel.dob = '';
            $scope.customerEditModel.isLoadingProgress = false;
            var datevalue1 = moment($scope.customerEditModel.customerInfo.special_date1).valueOf();
            if (datevalue1 > 0)
            {
                $scope.customerEditModel.special_date1 = new Date($scope.customerEditModel.customerInfo.special_date1);
            } else
                $scope.customerEditModel.special_date1 = '';
            var datevalue2 = moment($scope.customerEditModel.customerInfo.special_date2).valueOf();
            if (datevalue2 > 0)
            {
                $scope.customerEditModel.special_date2 = new Date($scope.customerEditModel.customerInfo.special_date2);
            } else
                $scope.customerEditModel.special_date2 = '';
            var datevalue3 = moment($scope.customerEditModel.customerInfo.special_date3).valueOf();
            if (datevalue3 > 0)
            {
                $scope.customerEditModel.special_date3 = new Date($scope.customerEditModel.customerInfo.special_date3);
            } else
                $scope.customerEditModel.special_date3 = '';
            var datevalue4 = moment($scope.customerEditModel.customerInfo.special_date4).valueOf();
            if (datevalue4 > 0)
            {
                $scope.customerEditModel.special_date4 = new Date($scope.customerEditModel.customerInfo.special_date4);
            } else
                $scope.customerEditModel.special_date4 = '';
            var datevalue5 = moment($scope.customerEditModel.customerInfo.special_date5).valueOf();
            if (datevalue5 > 0)
            {
                $scope.customerEditModel.special_date5 = new Date($scope.customerEditModel.customerInfo.special_date5);
            } else
                $scope.customerEditModel.special_date5 = '';
            if ($scope.customerEditModel.billingaddr == $scope.customerEditModel.shoppingaddr &&
                    $scope.customerEditModel.billingcity == $scope.customerEditModel.shoppingcity &&
                    $scope.customerEditModel.billingstate == $scope.customerEditModel.shoppingstate &&
                    $scope.customerEditModel.billingcountry == $scope.customerEditModel.shoppingcountry &&
                    $scope.customerEditModel.billingpostcode == $scope.customerEditModel.shoppingpostcode)
            {
                $scope.customerEditModel.isSameAddr = true;
            } else
            {
                $scope.customerEditModel.isSameAddr = false;
            }
            if ($scope.customerEditModel.customerInfo.img != '' && $scope.customerEditModel.customerInfo.img != null)
            {
                var fileItem = {}
                fileItem.urlpath = $scope.customerEditModel.customerInfo.img;
                $scope.customerEditModel.img.push(fileItem);
                console.log('imgtest');
                console.log($scope.customerEditModel.img);
            } else
            {
                $scope.customerEditModel.img = [];

            }
            if ($scope.customerDualEditModel.customerInfo != undefined && $scope.customerDualEditModel.customerInfo != null
                    && $scope.customerDualEditModel.customerInfo != '')
            {
                $scope.customerDualEditModel.id = $scope.customerDualEditModel.customerInfo.id;
                $scope.customerDualEditModel.billingaddress2 = $scope.customerDualEditModel.customerInfo.billing_address2;
                $scope.customerDualEditModel.billingaddress3 = $scope.customerDualEditModel.customerInfo.billing_address3;
                $scope.customerDualEditModel.billing_company_name = $scope.customerDualEditModel.customerInfo.billing_company_name;
                $scope.customerDualEditModel.billing_mail_id = $scope.customerDualEditModel.customerInfo.billing_mailid;
                $scope.customerDualEditModel.billing_website = $scope.customerDualEditModel.customerInfo.billing_website;
                $scope.customerDualEditModel.billing_mobile_no = $scope.customerDualEditModel.customerInfo.billing_mobile_no;
                $scope.customerDualEditModel.billing_phone_no = $scope.customerDualEditModel.customerInfo.billing_phone_no;
                $scope.customerDualEditModel.shippingaddress2 = $scope.customerDualEditModel.customerInfo.shipping_address2;
                $scope.customerDualEditModel.shippingaddress3 = $scope.customerDualEditModel.customerInfo.shipping_address3;
                $scope.customerDualEditModel.shipping_mobile_no = $scope.customerDualEditModel.customerInfo.shipping_mobile_no;
                $scope.customerDualEditModel.shipping_phone_no = $scope.customerDualEditModel.customerInfo.shipping_phone_no;
                $scope.customerDualEditModel.shipping_mail_id = $scope.customerDualEditModel.customerInfo.shipping_mail_id;
                $scope.customerDualEditModel.shipping_website = $scope.customerDualEditModel.customerInfo.shipping_website;
                $scope.customerDualEditModel.shipping_company_name = $scope.customerDualEditModel.customerInfo.shipping_company_name;
                $scope.customerDualEditModel.bankname = $scope.customerDualEditModel.customerInfo.bankname;
                $scope.customerDualEditModel.account_number = $scope.customerDualEditModel.customerInfo.bankacct;
                $scope.customerDualEditModel.branch = $scope.customerDualEditModel.customerInfo.bank_branch;
                $scope.customerDualEditModel.ifsccode = $scope.customerDualEditModel.customerInfo.bankcode;
                $scope.customerDualEditModel.personname = $scope.customerDualEditModel.customerInfo.fname;
                $scope.customerDualEditModel.designation = $scope.customerDualEditModel.customerInfo.jobtitle;
                $scope.customerDualEditModel.customer_type = 'suplier';
                $scope.customerDualEditModel.customer_id = $scope.customerDualEditModel.customerInfo.customer_id;
                // $scope.customerDualEditModel.name = $scope.customerDualEditModel.customerInfo.fname;
                $scope.customerDualEditModel.companyname = $scope.customerDualEditModel.customerInfo.company;
                $scope.customerDualEditModel.gst_no = $scope.customerDualEditModel.customerInfo.gst_no;
                $scope.customerEditModel.billingaddr = $scope.customerDualEditModel.customerInfo.billing_address;
                $scope.customerEditModel.billingcity = $scope.customerDualEditModel.customerInfo.billing_city;
                $scope.customerEditModel.billingstate = $scope.customerDualEditModel.customerInfo.billing_state;
                $scope.customerEditModel.billingcountry = $scope.customerDualEditModel.customerInfo.billing_country;
                $scope.customerEditModel.billingpostcode = $scope.customerDualEditModel.customerInfo.billing_pincode;
                $scope.customerEditModel.shoppingaddr = $scope.customerDualEditModel.customerInfo.shopping_address;
                $scope.customerEditModel.shoppingcity = $scope.customerDualEditModel.customerInfo.shopping_city;
                $scope.customerEditModel.shoppingstate = $scope.customerDualEditModel.customerInfo.shopping_state;
                $scope.customerEditModel.shoppingcountry = $scope.customerDualEditModel.customerInfo.shopping_country;
                $scope.customerEditModel.shoppingpostcode = $scope.customerDualEditModel.customerInfo.shopping_pincode;
                $scope.customerDualEditModel.acode = $scope.customerDualEditModel.customerInfo.acode;
                $scope.customerDualEditModel.mobile = $scope.customerDualEditModel.customerInfo.phone;
                var dob1 = new Date($scope.customerDualEditModel.customerInfo.dob);
                if (dob1 > 0)
                {
                    $scope.customerDualEditModel.dob = new Date($scope.customerDualEditModel.customerInfo.dob);
                } else
                    $scope.customerDualEditModel.dob = '';
                var datevalue1 = moment($scope.customerDualEditModel.customerInfo.special_date1).valueOf();
                if (datevalue1 > 0)
                {
                    $scope.customerDualEditModel.special_date1 = new Date($scope.customerDualEditModel.customerInfo.special_date1);
                } else
                    $scope.customerDualEditModel.special_date1 = '';
                var datevalue2 = moment($scope.customerDualEditModel.customerInfo.special_date2).valueOf();
                if (datevalue2 > 0)
                {
                    $scope.customerDualEditModel.special_date2 = new Date($scope.customerDualEditModel.customerInfo.special_date2);
                } else
                    $scope.customerDualEditModel.special_date2 = '';
                var datevalue3 = moment($scope.customerDualEditModel.customerInfo.special_date3).valueOf();
                if (datevalue3 > 0)
                {
                    $scope.customerDualEditModel.special_date3 = new Date($scope.customerDualEditModel.customerInfo.special_date3);
                } else
                    $scope.customerDualEditModel.special_date3 = '';
                var datevalue4 = moment($scope.customerDualEditModel.customerInfo.special_date4).valueOf();
                if (datevalue4 > 0)
                {
                    $scope.customerDualEditModel.special_date4 = new Date($scope.customerDualEditModel.customerInfo.special_date4);
                } else
                    $scope.customerDualEditModel.special_date4 = '';
                var datevalue5 = moment($scope.customerDualEditModel.customerInfo.special_date5).valueOf();
                if (datevalue5 > 0)
                {
                    $scope.customerDualEditModel.special_date5 = new Date($scope.customerDualEditModel.customerInfo.special_date5);
                } else
                    $scope.customerDualEditModel.special_date5 = '';
            }
            $scope.customAttributeBaseList = [];
            $scope.customAttributeDualSupportList = [];

            if ($rootScope.appConfig.dual_language_support)
            {
                for (var i = 0; i < $scope.customAttributeList.length; i++)
                {
                    var attribute_prefix = $scope.customAttributeList[i].attribute_code.split('_')[0];
                    if (attribute_prefix == $rootScope.appConfig.dual_language_label)
                    {
                        $scope.customAttributeDualSupportList.push($scope.customAttributeList[i]);
                    } else
                    {
                        $scope.customAttributeBaseList.push($scope.customAttributeList[i]);
                    }
                }
            } else
            {
                $scope.customAttributeBaseList = $scope.customAttributeList;
            }

        }

        $scope.isAddressDetail = false;
        $scope.showAddressInfo = function ()
        {
            $scope.isAddressDetail = !$scope.isAddressDetail;
        }

        $scope.isCustomInfo = false;
        $scope.showCustomInfo = function ()
        {
            $scope.isCustomInfo = !$scope.isCustomInfo;
        }
        $scope.modifyCustomerDetails = function (imgSave) {
            var headers = {};
            headers['screen-code'] = 'vendor';
            $scope.isDataSavingProcess = true;
            var modifyCustomerParam = {};
            modifyCustomerParam.id = $scope.customerEditModel.customerInfo.id;
            modifyCustomerParam.customer_no = $scope.customerEditModel.customerInfo.customer_no;
            modifyCustomerParam.billing_address2 = $scope.customerEditModel.billingaddress2;
            modifyCustomerParam.billing_address3 = $scope.customerEditModel.billingaddress3;
            modifyCustomerParam.billing_company_name = $scope.customerEditModel.companyName;
            modifyCustomerParam.billing_mail_id = $scope.customerEditModel.billing_mail_id;
            modifyCustomerParam.billing_website = $scope.customerEditModel.billing_website;
            modifyCustomerParam.billing_mobile_no = $scope.customerEditModel.billing_mobile_no;
            modifyCustomerParam.billing_phone_no = $scope.customerEditModel.billing_phone_no;
            modifyCustomerParam.shipping_address2 = $scope.customerEditModel.shippingaddress2;
            modifyCustomerParam.shipping_address3 = $scope.customerEditModel.shippingaddress3;
            modifyCustomerParam.shipping_mobile_no = $scope.customerEditModel.shipping_mobile_no;
            modifyCustomerParam.shipping_phone_no = $scope.customerEditModel.shipping_phone_no;
            modifyCustomerParam.shipping_mail_id = $scope.customerEditModel.shipping_mail_id;
            modifyCustomerParam.shipping_website = $scope.customerEditModel.shipping_website;
            modifyCustomerParam.shipping_company_name = $scope.customerEditModel.shipping_company_name;
            modifyCustomerParam.bankname = $scope.customerEditModel.bankname;
            modifyCustomerParam.bankacct = $scope.customerEditModel.account_number;
            modifyCustomerParam.bank_branch = $scope.customerEditModel.branch;
            modifyCustomerParam.bankcode = $scope.customerEditModel.ifsc_code;
            modifyCustomerParam.fname = $scope.customerEditModel.personname;
            modifyCustomerParam.jobtitle = $scope.customerEditModel.designation;
            modifyCustomerParam.customer_type = 'suplier';
            modifyCustomerParam.prefix = $scope.customerEditModel.customerInfo.prefix;
            // modifyCustomerParam.fname = $scope.customerEditModel.customerInfo.fname;
            modifyCustomerParam.company = $scope.customerEditModel.companyName;
            modifyCustomerParam.gst_no = $scope.customerEditModel.gst_no;
            modifyCustomerParam.customer_type = $scope.customerEditModel.customerInfo.customer_type;
            modifyCustomerParam.phone = $scope.customerEditModel.mobile;
            modifyCustomerParam.other_contact_no = $scope.customerEditModel.customerInfo.other_contact_no;
            modifyCustomerParam.email = $scope.customerEditModel.email;
            modifyCustomerParam.city = $scope.customerEditModel.customerInfo.city;
            modifyCustomerParam.country = $scope.customerEditModel.customerInfo.country;
            modifyCustomerParam.address = $scope.customerEditModel.customerInfo.address;
            modifyCustomerParam.state = $scope.customerEditModel.customerInfo.state;
            modifyCustomerParam.zip = $scope.customerEditModel.customerInfo.zip;
            modifyCustomerParam.password = $scope.customerEditModel.password;
            modifyCustomerParam.iswelcomeEmail = ($scope.customerEditModel.iswelcomeEmail == true ? 1 : 0);
            modifyCustomerParam.is_purchase = 1;
            modifyCustomerParam.isactive = 1;
            modifyCustomerParam.acode = $scope.customerEditModel.acode;
            if ($scope.customerEditModel.img.length > 0)
            {
                modifyCustomerParam.img = $scope.customerEditModel.img[0].urlpath;
            } else
            {
                modifyCustomerParam.img = '';
            }
            if (typeof $scope.customerEditModel.dob == 'object')
            {
                var dob = utilityService.parseDateToStr($scope.customerEditModel.dob, $scope.dateFormat);
            }
            else
            {
                var dob = '';
            }
            modifyCustomerParam.dob = utilityService.changeDateToSqlFormat(dob, $scope.dateFormat);
            var special_date1 = "";
            var special_date2 = "";
            var special_date3 = "";
            var special_date4 = "";
            var special_date5 = "";
            modifyCustomerParam.dob = utilityService.changeDateToSqlFormat(dob, $scope.dateFormat);
            if (typeof $scope.customerEditModel.special_date1 == 'object')
            {
                special_date1 = utilityService.parseDateToStr($scope.customerEditModel.special_date1, $scope.dateFormat);
            }
            modifyCustomerParam.special_date1 = utilityService.changeDateToSqlFormat(special_date1, $scope.dateFormat);
            if (typeof $scope.customerEditModel.special_date2 == 'object')
            {
                special_date2 = utilityService.parseDateToStr($scope.customerEditModel.special_date2, $scope.dateFormat);
            }
            modifyCustomerParam.special_date2 = utilityService.changeDateToSqlFormat(special_date2, $scope.dateFormat);
            if (typeof $scope.customerEditModel.special_date3 == 'object')
            {
                special_date3 = utilityService.parseDateToStr($scope.customerEditModel.special_date3, $scope.dateFormat);
            }
            modifyCustomerParam.special_date3 = utilityService.changeDateToSqlFormat(special_date3, $scope.dateFormat);
            if (typeof $scope.customerEditModel.special_date4 == 'object')
            {
                special_date4 = utilityService.parseDateToStr($scope.customerEditModel.special_date4, $scope.dateFormat);
            }
            modifyCustomerParam.special_date4 = utilityService.changeDateToSqlFormat(special_date4, $scope.dateFormat);
            if (typeof $scope.customerEditModel.special_date5 == 'object')
            {
                special_date5 = utilityService.parseDateToStr($scope.customerEditModel.special_date5, $scope.dateFormat);
            }
            modifyCustomerParam.special_date5 = utilityService.changeDateToSqlFormat(special_date5, $scope.dateFormat);
            modifyCustomerParam.billing_address = $scope.customerEditModel.billingaddr;
            modifyCustomerParam.billing_city = $scope.customerEditModel.billingcity;
            modifyCustomerParam.billing_state = $scope.customerEditModel.billingstate;
            modifyCustomerParam.billing_country = $scope.customerEditModel.billingcountry;
            modifyCustomerParam.billing_pincode = $scope.customerEditModel.billingpostcode;
            modifyCustomerParam.shopping_address = $scope.customerEditModel.shoppingaddr;
            modifyCustomerParam.shopping_city = $scope.customerEditModel.shoppingcity;
            modifyCustomerParam.shopping_state = $scope.customerEditModel.shoppingstate;
            modifyCustomerParam.shopping_country = $scope.customerEditModel.shoppingcountry;
            modifyCustomerParam.shopping_pincode = $scope.customerEditModel.shoppingpostcode;
            for (var i = 0; i < $scope.customerEditModel.billingCountryList.length; i++)
            {
                if ($scope.customerEditModel.billingcountry == $scope.customerEditModel.billingCountryList[i].name)
                {
                    modifyCustomerParam.billing_country_id = $scope.customerEditModel.billingCountryList[i].id;
                }
            }
            for (var i = 0; i < $scope.customerEditModel.billingStateList.length; i++)
            {
                if ($scope.customerEditModel.billingstate == $scope.customerEditModel.billingStateList[i].name)
                {
                    modifyCustomerParam.billing_state_id = $scope.customerEditModel.billingStateList[i].id;
                }
            }
            for (var i = 0; i < $scope.customerEditModel.billingCityList.length; i++)
            {
                if ($scope.customerEditModel.billingcity == $scope.customerEditModel.billingCityList[i].name)
                {
                    modifyCustomerParam.billing_city_id = $scope.customerEditModel.billingCityList[i].id;
                }
            }
            for (var i = 0; i < $scope.customerEditModel.shoppingCountryList.length; i++)
            {
                if ($scope.customerEditModel.shoppingcountry == $scope.customerEditModel.shoppingCountryList[i].name)
                {
                    modifyCustomerParam.shopping_country_id = $scope.customerEditModel.shoppingCountryList[i].id;
                }
            }
            for (var i = 0; i < $scope.customerEditModel.shoppingStateList.length; i++)
            {
                if ($scope.customerEditModel.shoppingstate == $scope.customerEditModel.shoppingStateList[i].name)
                {
                    modifyCustomerParam.shopping_state_id = $scope.customerEditModel.shoppingStateList[i].id;
                }
            }
            for (var i = 0; i < $scope.customerEditModel.shoppingCityList.length; i++)
            {
                if ($scope.customerEditModel.shoppingcity == $scope.customerEditModel.shoppingCityList[i].name)
                {
                    modifyCustomerParam.shopping_city_id = $scope.customerEditModel.shoppingCityList[i].id;
                }
            }
            modifyCustomerParam.is_active = 1;
            modifyCustomerParam.customattribute = [];
            for (var i = 0; i < $scope.customAttributeList.length; i++)
            {
                var attribute_prefix = $scope.customAttributeList[i].attribute_code.split('_')[0];
                if (attribute_prefix == $rootScope.appConfig.dual_language_label)
                {
                    continue;
                }

                var customattributeItem = {};

                if ($scope.customAttributeList[i].value != undefined)
                {
                    customattributeItem.value = $scope.customAttributeList[i].value;
                } else if ($scope.customAttributeList[i].default_value != undefined)
                {
                    customattributeItem.value = $scope.customAttributeList[i].default_value;
                } else
                {
                    customattributeItem.value = "";
                }

                customattributeItem.attribute_code = $scope.customAttributeList[i].attribute_code;
                modifyCustomerParam.customattribute.push(customattributeItem);
            }
            var configOption = adminService.handleBothSuccessAndErrorResponseConfig;
            adminService.editCustomerInfo(modifyCustomerParam, $scope.customerEditModel.customerInfo.id, configOption, headers).then(function (response) {
                if (response.data.success == true)
                {
                    if ($scope.customerDualEditModel.customerInfo != '')
                    {
                        $scope.modifyCustomerDualInfoDetails(imgSave);
                    } else
                    {
                        $scope.isDataSavingProcess = false;
                        $scope.formReset();
                        $state.go('app.vendor');
                    }
                } else
                {
                    $scope.isDataSavingProcess = false;
                }

            }).catch(function (response) {
                console.error('Error occurred:', response.status, response.data);
                $scope.isDataSavingProcess = false;
                var element = document.getElementById("btnLoad");
                element.classList.remove("btn-loader");
            });
        };

        $scope.modifyCustomerDualInfoDetails = function (imgSave) {
            var headers = {};
            headers['screen-code'] = 'vendor';
            $scope.isDataSavingProcess = true;
            var modifyCustomerParam = {};
            modifyCustomerParam.id = $scope.customerEditModel.customerInfo.id;
            modifyCustomerParam.customer_id = $scope.customerDualEditModel.customer_id;
            modifyCustomerParam.billing_address2 = $scope.customerDualEditModel.billingaddress2;
            modifyCustomerParam.billing_address3 = $scope.customerDualEditModel.billingaddress3;
            modifyCustomerParam.billing_company_name = $scope.customerDualEditModel.billing_company_name;
            modifyCustomerParam.billing_mail_id = $scope.customerDualEditModel.billing_mail_id;
            modifyCustomerParam.billing_website = $scope.customerDualEditModel.billing_website;
            modifyCustomerParam.billing_mobile = $scope.customerDualEditModel.billing_mobile_no;
            modifyCustomerParam.billing_phone = $scope.customerDualEditModel.billing_phone_no;
            modifyCustomerParam.shipping_address2 = $scope.customerDualEditModel.shippingaddress2;
            modifyCustomerParam.shipping_address3 = $scope.customerDualEditModel.shippingaddress3;
            modifyCustomerParam.shipping_mobile_no = $scope.customerDualEditModel.shipping_mobile_no;
            modifyCustomerParam.shipping_phone_no = $scope.customerDualEditModel.shipping_phone_no;
            modifyCustomerParam.shipping_mail_id = $scope.customerDualEditModel.shipping_mail_id;
            modifyCustomerParam.shipping_website = $scope.customerDualEditModel.shipping_website;
            modifyCustomerParam.shipping_company_name = $scope.customerDualEditModel.shipping_company_name;
            modifyCustomerParam.bankname = $scope.customerDualEditModel.bankname;
            modifyCustomerParam.bankacct = $scope.customerDualEditModel.account_number;
            modifyCustomerParam.bank_branch = $scope.customerDualEditModel.branch;
            modifyCustomerParam.bankcode = $scope.customerDualEditModel.ifsc_code;
            modifyCustomerParam.fname = $scope.customerDualEditModel.personname;
            modifyCustomerParam.jobtitle = $scope.customerDualEditModel.designation;
            modifyCustomerParam.customer_type = 'suplier';
            //   modifyCustomerParam.fname = $scope.customerDualEditModel.name;
            modifyCustomerParam.company = $scope.customerDualEditModel.companyname;
            modifyCustomerParam.gst_no = $scope.customerDualEditModel.gst_no;
//            modifyCustomerParam.customer_type = $scope.customerDualEditModel.customer_type;
            modifyCustomerParam.phone = $scope.customerEditModel.customerInfo.phone;
            modifyCustomerParam.email = $scope.customerEditModel.customerInfo.email;
            modifyCustomerParam.city = $scope.customerEditModel.customerInfo.city;
            modifyCustomerParam.country = $scope.customerEditModel.customerInfo.country;
            modifyCustomerParam.address = $scope.customerEditModel.customerInfo.address;
            modifyCustomerParam.state = $scope.customerEditModel.customerInfo.state;
            modifyCustomerParam.zip = $scope.customerEditModel.customerInfo.zip;
            modifyCustomerParam.password = $scope.customerEditModel.password;
            if (typeof $scope.customerEditModel.dob == 'object')
            {
                $scope.customerEditModel.dob = utilityService.parseDateToStr($scope.customerEditModel.dob, $scope.dateFormat);
            }
            modifyCustomerParam.dob = utilityService.changeDateToSqlFormat($scope.customerEditModel.dob, $scope.dateFormat);
            if (typeof $scope.customerEditModel.special_date1 == 'object')
            {
                $scope.customerEditModel.special_date1 = utilityService.parseDateToStr($scope.customerEditModel.special_date1, $scope.dateFormat);
            }
            modifyCustomerParam.special_date1 = utilityService.changeDateToSqlFormat($scope.customerEditModel.special_date1, 'yyyy-MM-dd');
            if (typeof $scope.customerEditModel.special_date2 == 'object')
            {
                $scope.customerEditModel.special_date2 = utilityService.parseDateToStr($scope.customerEditModel.special_date2, $scope.dateFormat);
            }
            modifyCustomerParam.special_date2 = utilityService.changeDateToSqlFormat($scope.customerEditModel.special_date2, 'yyyy-MM-dd');
            if (typeof $scope.customerEditModel.special_date3 == 'object')
            {
                $scope.customerEditModel.special_date3 = utilityService.parseDateToStr($scope.customerEditModel.special_date3, $scope.dateFormat);
            }
            modifyCustomerParam.special_date3 = utilityService.changeDateToSqlFormat($scope.customerEditModel.special_date3, 'yyyy-MM-dd');
            if (typeof $scope.customerEditModel.special_date4 == 'object')
            {
                $scope.customerEditModel.special_date4 = utilityService.parseDateToStr($scope.customerEditModel.special_date4, $scope.dateFormat);
            }
            modifyCustomerParam.special_date4 = utilityService.changeDateToSqlFormat($scope.customerEditModel.special_date4, 'yyyy-MM-dd');
            if (typeof $scope.customerEditModel.special_date5 == 'object')
            {
                $scope.customerEditModel.special_date5 = utilityService.parseDateToStr($scope.customerEditModel.special_date5, $scope.dateFormat);
            }
            modifyCustomerParam.special_date5 = utilityService.changeDateToSqlFormat($scope.customerEditModel.special_date5, 'yyyy-MM-dd');
            modifyCustomerParam.iswelcomeEmail = ($scope.customerEditModel.iswelcomeEmail == true ? 1 : 0);
            modifyCustomerParam.is_purchase = 1;
            modifyCustomerParam.isactive = 1;
            modifyCustomerParam.acode = $scope.customerEditModel.acode;
            if ($scope.customerEditModel.img.length > 0)
            {
                modifyCustomerParam.img = $scope.customerEditModel.img[0].filepath;
            } else
            {
                modifyCustomerParam.img = '';
            }
            if (typeof $scope.customerEditModel.dob == 'object')
            {
                $scope.customerEditModel.dob = utilityService.parseDateToStr($scope.customerEditModel.dob, $scope.dateFormat);
            }
            if (typeof $scope.customerEditModel.dob == '')
            {
                $scope.customerEditModel.dob = '';
            }
            modifyCustomerParam.dob = utilityService.changeDateToSqlFormat($scope.customerEditModel.dob, $scope.dateFormat);

            modifyCustomerParam.billing_address = $scope.customerEditModel.billingaddr;
            modifyCustomerParam.billing_city = $scope.customerEditModel.billingcity;
            modifyCustomerParam.billing_state = $scope.customerEditModel.billingstate;
            modifyCustomerParam.billing_country = $scope.customerEditModel.billingcountry;
            modifyCustomerParam.billing_pincode = $scope.customerEditModel.billingpostcode;
            modifyCustomerParam.shopping_address = $scope.customerEditModel.shoppingaddr;
            modifyCustomerParam.shopping_city = $scope.customerEditModel.shoppingcity;
            modifyCustomerParam.shopping_state = $scope.customerEditModel.shoppingstate;
            modifyCustomerParam.shopping_country = $scope.customerEditModel.shoppingcountry;
            modifyCustomerParam.shopping_pincode = $scope.customerEditModel.shoppingpostcode;
            for (var i = 0; i < $scope.customerEditModel.billingCountryList.length; i++)
            {
                if ($scope.customerEditModel.billingcountry == $scope.customerEditModel.billingCountryList[i].name)
                {
                    modifyCustomerParam.billing_country_id = $scope.customerEditModel.billingCountryList[i].id;
                }
            }
            for (var i = 0; i < $scope.customerEditModel.billingStateList.length; i++)
            {
                if ($scope.customerEditModel.billingstate == $scope.customerEditModel.billingStateList[i].name)
                {
                    modifyCustomerParam.billing_state_id = $scope.customerEditModel.billingStateList[i].id;
                }
            }
            for (var i = 0; i < $scope.customerEditModel.billingCityList.length; i++)
            {
                if ($scope.customerEditModel.billingcity == $scope.customerEditModel.billingCityList[i].name)
                {
                    modifyCustomerParam.billing_city_id = $scope.customerEditModel.billingCityList[i].id;
                }
            }
            for (var i = 0; i < $scope.customerEditModel.shoppingCountryList.length; i++)
            {
                if ($scope.customerEditModel.shoppingcountry == $scope.customerEditModel.shoppingCountryList[i].name)
                {
                    modifyCustomerParam.shopping_country_id = $scope.customerEditModel.shoppingCountryList[i].id;
                }
            }
            for (var i = 0; i < $scope.customerEditModel.shoppingStateList.length; i++)
            {
                if ($scope.customerEditModel.shoppingstate == $scope.customerEditModel.shoppingStateList[i].name)
                {
                    modifyCustomerParam.shopping_state_id = $scope.customerEditModel.shoppingStateList[i].id;
                }
            }
            for (var i = 0; i < $scope.customerEditModel.shoppingCityList.length; i++)
            {
                if ($scope.customerEditModel.shoppingcity == $scope.customerEditModel.shoppingCityList[i].name)
                {
                    modifyCustomerParam.shopping_city_id = $scope.customerEditModel.shoppingCityList[i].id;
                }
            }
            modifyCustomerParam.is_active = 1;
            modifyCustomerParam.customattribute = [];
            for (var i = 0; i < $scope.customAttributeList.length; i++)
            {

                var attribute_prefix = $scope.customAttributeList[i].attribute_code.split('_')[0];
                if (attribute_prefix == $rootScope.appConfig.dual_language_label)
                {
                    var customattributeItem = {};

                    if ($scope.customAttributeList[i].value != undefined)
                    {
                        customattributeItem.value = $scope.customAttributeList[i].value;
                    } else if ($scope.customAttributeList[i].default_value != undefined)
                    {
                        customattributeItem.value = $scope.customAttributeList[i].default_value;
                    } else
                    {
                        customattributeItem.value = "";
                    }

                    customattributeItem.attribute_code = $scope.customAttributeList[i].attribute_code;
                    modifyCustomerParam.customattribute.push(customattributeItem);
                }

            }

            adminService.editCustomerDualInfo(modifyCustomerParam, $scope.customerDualEditModel.customerInfo.id, headers).then(function (response) {
                if (response.data.success == true)
                {
                    if (imgSave == 1)
                    {
                        $scope.formReset();
                        $state.go('app.vendorEdit', {
                            'id': $stateParams.id
                        }, {
                            'reload': true
                        });
                        // $state.go('app.customerEdit',{'reload': true});
                        // $window.location.reload();
                    } else
                    {
                        $scope.formReset();
                        $state.go('app.vendor');
                    }
                }
                $scope.isDataSavingProcess = false;
            }).catch(function (response) {
                console.error('Error occurred:', response.status, response.data);
                $scope.isDataSavingProcess = false;
                var element = document.getElementById("btnLoad");
                element.classList.remove("btn-loader");
            });
        };

        $scope.belongtoUserLoaded = false;
        $scope.getBelongtoUserList = function () {
            var userListParam = {};
            userListParam.id = "";
            userListParam.name = "";
            userListParam.start = 0;
            userListParam.limit = 100;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getUserList(userListParam, configOption).then(function (response) {
                var data = response.data.data;
                $scope.customerEditModel.userList = data;
                $scope.belongtoUserLoaded = true;
            });
        };

        $scope.customerDualInfoLoaded = false;
        $scope.getCustomerDualInfo = function () {
            var getListParam = {};
            getListParam.customer_id = $stateParams.id;
            getListParam.name = '';
            getListParam.is_active = 1;
            adminService.getCustomerDualInfoDetail(getListParam).then(function (response) {
                var data = response.data;
                if (data.data != undefined && data.data != null)
                {
                    $scope.customerDualEditModel.customerInfo = data.data;
                } else
                {
                    $scope.customerDualEditModel.customerInfo = '';
                }
                $scope.customerDualInfoLoaded = true;
                $scope.initUpdateDetail();
            });
        };
        $scope.getCustomerInfo = function () {
            $scope.customerEditModel.isLoadingProgress = true;
            var getListParam = {};
            getListParam.id = $stateParams.id;
            getListParam.name = '';
            getListParam.mode = 1;
            getListParam.sort = 1;
            getListParam.is_active = 1;
            adminService.getCustomerDetail(getListParam).then(function (response) {
                var data = response.data;
                if (data.total != 0)
                {
                    $scope.customerEditModel.customerInfo = data.data;
                    $scope.customerEditModel.iswelcomeEmail = $scope.customerEditModel.customerInfo.iswelcomeEmail == 1 ? true : false;
                    $scope.customerEditModel.is_sale = $scope.customerEditModel.customerInfo.is_sale == 1 ? true : false;
                    $scope.customerEditModel.is_purchase = $scope.customerEditModel.customerInfo.is_purchase == 1 ? true : false;
                }
                $scope.customerEditModel.total = data.total;
//                $scope.getBelongtoUserList();
                $scope.getCustomerDualInfo();
                $scope.initUpdateDetail();
            });
        };
        $scope.$on('customSelectOptionChangeEvent', function (event, data)
        {
            console.log(data);
            $scope.updateCustomSelectValue(data);
        });

        $scope.updateCustomSelectValue = function (data)
        {
            for (var i = 0; i < $scope.customAttributeList.length; i++)
            {
                if ($scope.customAttributeList[i].attribute_code == data.code)
                {
                    $scope.customAttributeList[i].value = data.value;
                    break;
                }
            }
        }

        $scope.$on('customfieldValueChangeEvent', function (event, data)
        {
            console.log(data);
            $scope.updateCustomFieldValue(data);
        });

        $scope.updateCustomFieldValue = function (data)
        {
            for (var i = 0; i < $scope.customAttributeList.length; i++)
            {
                if ($scope.customAttributeList[i].attribute_code == data.code)
                {
                    $scope.customAttributeList[i].value = data.value;
                    break;
                }
            }
        };

        $scope.getCountryList = function () {

            var getListParam = {};
            getListParam.id = "";
            getListParam.name = "";
            getListParam.code = "";
            $scope.countryLoaded = false;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getCountryList(getListParam, configOption).then(function (response)
            {
                var data = response.data.list;

                $timeout(function () {
                    $scope.customerEditModel.billingCountryList = data;
                    $scope.customerEditModel.shoppingCountryList = data;

                    $scope.customerDualEditModel.billingCountryList = data;
                    $scope.customerDualEditModel.shoppingCountryList = data;
                    for (var i = 0; i < $scope.customerEditModel.billingCountryList.length; i++)
                    {
                        if ($scope.customerEditModel.billingCountryList[i].is_default == 1)
                        {
                            $scope.customerEditModel.billingcountry = $scope.customerEditModel.billingCountryList[i].name;
                            $scope.customerEditModel.shoppingcountry = $scope.customerEditModel.billingCountryList[i].name;
                            $scope.customerDualEditModel.billingcountry = $scope.customerEditModel.billingCountryList[i].name;
                            $scope.customerDualEditModel.shoppingcountry = $scope.customerEditModel.billingCountryList[i].name;
                        }
                    }

                });
                $scope.countryLoaded = true;
            });
        };


        $scope.initBillingAddressStateListTimeoutPromise = null;

        $scope.initBillingAddressStateList = function (searchkey)
        {
            if ($scope.initBillingAddressStateListTimeoutPromise != null)
            {
                $timeout.cancel($scope.initBillingAddressStateListTimeoutPromise);
            }
            $scope.initBillingAddressStateListTimeoutPromise = $timeout(function () {
                $scope.getStateList(searchkey, 'billingstate');
            }, 300);

        };

        $scope.initShippingAddressStateListTimeoutPromise = null;

        $scope.initShippingAddressStateList = function (searchkey)
        {
            if ($scope.initShippingAddressStateListTimeoutPromise != null)
            {
                $timeout.cancel($scope.initShippingAddressStateListTimeoutPromise);
            }
            $scope.initShippingAddressStateListTimeoutPromise = $timeout(function () {
                $scope.getStateList(searchkey, 'shippingstate');
            }, 300);

        };

        $scope.initDualBillingAddressStateListTimeoutPromise = null;

        $scope.initDualBillingAddressStateList = function (searchkey)
        {
            if ($scope.initDualBillingAddressStateListTimeoutPromise != null)
            {
                $timeout.cancel($scope.initDualBillingAddressStateListTimeoutPromise);
            }
            $scope.initDualBillingAddressStateListTimeoutPromise = $timeout(function () {
                $scope.getStateList(searchkey, 'dualbillingstate');
            }, 300);

        };

        $scope.initDualShippingAddressStateListTimeoutPromise = null;

        $scope.initDualShippingAddressStateList = function (searchkey)
        {
            if ($scope.initDualShippingAddressStateListTimeoutPromise != null)
            {
                $timeout.cancel($scope.initDualShippingAddressStateListTimeoutPromise);
            }
            $scope.initDualShippingAddressStateListTimeoutPromise = $timeout(function () {
                $scope.getStateList(searchkey, 'dualshippingstate');
            }, 300);

        };


        $rootScope.billingState =  false;
        $rootScope.shippingState =  false;
        $rootScope.dualbillingState =  false;
        $rootScope.dualshippingstate =  false;
        $scope.getStateList = function (searchkey, fieldName) {

            console.log('searchkey  = ' + searchkey);
            console.log('fieldName  = ' + fieldName);
            $scope.stateLoaded = false;
            var getListParam = {};
            getListParam.id = "";
            getListParam.name = searchkey;
            getListParam.country_name = "";

            if (fieldName == 'billingstate')
            {
                getListParam.country_name = $scope.customerEditModel.billingcountry;
            } else if (fieldName == 'shippingstate')
            {
                getListParam.country_name = $scope.customerEditModel.shoppingcountry;
            } else if (fieldName == 'dualbillingstate')
            {
                getListParam.country_name = $scope.customerDualEditModel.shoppingcountry;
            } else if (fieldName == 'dualshippingstate')
            {
                getListParam.country_name = $scope.customerDualEditModel.shoppingcountry;
            }

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            configOption.fieldName = fieldName;
            configOption.searchkey = searchkey;

            adminService.getStateList(getListParam, configOption).then(function (response)
            {
                if (!response.data.success)
                {
                    return;
                }
                var data = response.data.list;
                if (typeof response.config != 'undefined' && typeof response.config.config != 'undefined'
                        && response.config.config.fieldName != undefined && response.config.config.searchkey != undefined)
                {

                    //                    if (data.length == 0)
                    //                    {
                    //                        data.push({"name": response.config.config.searchkey});
                    //                    }
                    $timeout(function () {
                        if (fieldName == 'billingstate')
                        {
                            $scope.customerEditModel.billingStateList = data;
                            if($rootScope.billingState == false)
                            {
                            for (var i = 0; i < $scope.customerEditModel.billingStateList.length; i++)
                            {
                                if ($scope.customerEditModel.billingStateList[i].is_default == 1)
                                {
                                    $scope.customerEditModel.billingstate = $scope.customerEditModel.billingStateList[i].name;
                                    $scope.customerEditModel.shoppingstate = $scope.customerEditModel.billingStateList[i].name;
                                        $rootScope.billingState = true;
                                        $rootScope.shippingState = true;
                                }
                            }
                            }
                        } else if (fieldName == 'shippingstate')
                        {
                            $scope.customerEditModel.shoppingStateList = data;
//                            if($rootScope.shippingState == false)
//                            {
//                                for (var i = 0; i < $scope.customerEditModel.shoppingStateList.length; i++)
//                                {
//                                    if ($scope.customerEditModel.shoppingStateList[i].is_default == 1)
//                                    {
//                                        $scope.customerEditModel.shoppingstate = $scope.customerEditModel.shoppingStateList[i].name;
//                                        $rootScope.shippingState =  true;
//                                    }
//                                }
//                            }
                        } else if (fieldName == 'dualbillingstate')
                        {
                            $scope.customerDualEditModel.billingStateList = data;
                            if($rootScope.dualbillingstate == false)
                            {
                            for (var i = 0; i < $scope.customerDualEditModel.billingStateList.length; i++)
                            {
                                if ($scope.customerDualEditModel.billingStateList[i].is_default == 1)
                                {
                                    $scope.customerDualEditModel.billingstate = $scope.customerDualEditModel.billingStateList[i].name;
                                        $rootScope.dualbillingstate =  true;
                                }
                            }
                            }
                        } else if (fieldName == 'dualshippingstate')
                        {
                            $scope.customerDualEditModel.shoppingStateList = data;
                            if($rootScope.dualshippingstate == false)
                            {
                            for (var i = 0; i < $scope.customerDualEditModel.shoppingStateList.length; i++)
                            {
                                if ($scope.customerDualEditModel.shoppingStateList[i].is_default == 1)
                                {
                                    $scope.customerDualEditModel.shoppingstate = $scope.customerDualEditModel.shoppingStateList[i].name;
                                        $rootScope.dualshippingstate =  true;
                                }
                            }
                        }
                        }
                    }, 0);
                    $scope.stateLoaded = true;
                }

            });

        };


        $scope.initBillingAddressCityListTimeoutPromise = null;

        $scope.initBillingAddressCityList = function (searchkey)
        {
            if ($scope.initBillingAddressCityListTimeoutPromise != null)
            {
                $timeout.cancel($scope.initBillingAddressCityListTimeoutPromise);
            }
            $scope.initBillingAddressCityListTimeoutPromise = $timeout(function () {
                $scope.getCityList(searchkey, 'billingcity');
            }, 300);

        };
        $scope.initShippingAddressCityListTimeoutPromise = null;

        $scope.initShippingAddressCityList = function (searchkey)
        {
            if ($scope.initShippingAddressCityListTimeoutPromise != null)
            {
                $timeout.cancel($scope.initShippingAddressCityListTimeoutPromise);
            }
            $scope.initShippingAddressCityListTimeoutPromise = $timeout(function () {
                $scope.getCityList(searchkey, 'shippingcity');
            }, 300);

        };

        $scope.initDualBillingAddressCityListTimeoutPromise = null;

        $scope.initDualBillingAddressCityList = function (searchkey)
        {
            if ($scope.initDualBillingAddressCityListTimeoutPromise != null)
            {
                $timeout.cancel($scope.initDualBillingAddressCityListTimeoutPromise);
            }
            $scope.initDualBillingAddressCityListTimeoutPromise = $timeout(function () {
                $scope.getCityList(searchkey, 'dualbillingcity');
            }, 300);

        };
        $scope.initDualShippingAddressCityListTimeoutPromise = null;

        $scope.initDualShippingAddressCityList = function (searchkey)
        {
            if ($scope.initDualShippingAddressCityListTimeoutPromise != null)
            {
                $timeout.cancel($scope.initDualShippingAddressCityListTimeoutPromise);
            }
            $scope.initDualShippingAddressCityListTimeoutPromise = $timeout(function () {
                $scope.getCityList(searchkey, 'dualshippingcity');
            }, 300);

        };


        $scope.getCityList = function (searchkey, fieldName) {

            console.log('searchkey  = ' + searchkey);
            console.log('fieldName  = ' + fieldName);
            $scope.cityLoaded = false;
            var getListParam = {};
            getListParam.id = "";
            getListParam.name = searchkey;
            getListParam.country_name = "";
            getListParam.state_name = "";

            if (fieldName == 'billingcity')
            {
                getListParam.state_name = $scope.customerEditModel.billingstate;
                getListParam.country_name = $scope.customerEditModel.billingcountry;
            } else if (fieldName == 'shippingcity')
            {
                getListParam.state_name = $scope.customerEditModel.shoppingstate;
                getListParam.country_name = $scope.customerEditModel.shoppingcountry;
            } else if (fieldName == 'dualbillingcity')
            {
                getListParam.state_name = $scope.customerDualEditModel.billingstate;
                getListParam.country_name = $scope.customerDualEditModel.billingcountry;
            } else if (fieldName == 'dualshippingcity')
            {
                getListParam.state_name = $scope.customerDualEditModel.shoppingstate;
                getListParam.country_name = $scope.customerDualEditModel.shoppingcountry;
            }

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            configOption.fieldName = fieldName;
            configOption.searchkey = searchkey;

            adminService.getCityList(getListParam, configOption).then(function (response)
            {
                if (!response.data.success)
                {
                    return;
                }
                var data = response.data.list;
                if (typeof response.config != 'undefined' && typeof response.config.config != 'undefined'
                        && response.config.config.fieldName != undefined && response.config.config.searchkey != undefined)
                {

                    //                    if (data.length == 0)
                    //                    {
                    //                        data.push({"name": response.config.config.searchkey});
                    //                    }
                    $timeout(function () {
                        if (fieldName == 'billingcity')
                        {
                            $scope.customerEditModel.billingCityList = data;
                        } else if (fieldName == 'shippingcity')
                        {
                            $scope.customerEditModel.shoppingCityList = data;
                        } else if (fieldName == 'dualbillingcity')
                        {
                            $scope.customerDualEditModel.billingCityList = data;
                        } else if (fieldName == 'dualshippingcity')
                        {
                            $scope.customerDualEditModel.shoppingCityList = data;
                        }
                    }, 0);
                    $scope.cityLoaded = true;

                }

            });

        };

        $scope.billingAddressCountryChange = function ()
        {
            $scope.customerEditModel.billingstate = '';
            $scope.customerEditModel.billingcity = '';

            $scope.customerEditModel.billingCityList = [];
            $scope.customerEditModel.billingStateList = [];

            $scope.checkAddress();
        }

        $scope.billingAddressStateChange = function ()
        {

            $scope.customerEditModel.billingcity = '';
            $scope.customerEditModel.billingCityList = [];

            $scope.checkAddress();

        }

        $scope.shippingAddressCountryChange = function ()
        {
            $scope.customerEditModel.shoppingstate = '';
            $scope.customerEditModel.shoppingcity = '';

            $scope.customerEditModel.shoppingCityList = [];
            $scope.customerEditModel.shoppingStateList = [];

            $scope.updateShoppingAddr('update');
        }

        $scope.shippingAddressStateChange = function ()
        {
            $scope.customerEditModel.shoppingcity = '';
            $scope.customerEditModel.shoppingCityList = [];

            $scope.updateShoppingAddr('update');
        }


        $scope.getCountryList();
        $scope.getCustomerInfo();
    }]);




