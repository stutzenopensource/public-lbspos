app.controller('vendorListCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$localStorage', '$filter', 'Auth', '$state', '$timeout', 'APP_CONST', 'ValidationFactory','$window', function ($scope, $rootScope, adminService, $httpService, $localService, $filter, Auth, $state, $timeout, APP_CONST, ValidationFactory,$window) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.customerModel = {
            currentPage: 1,
            id: '',
            total: 0,
            limit: 10,
            country: '',
            city: '',
            list: [],
            printList: [],
            status: '',
            serverList: null,
            isLoadingProgress: false,
            customerInfo: '',
            newAddedAmount: '',
            totalAmt: '',
            categoryList: []
        };

        $scope.validationFactory = ValidationFactory;
        $scope.pagePerCount = [50, 100];
        $scope.customerModel.limit = $scope.pagePerCount[0];

        $scope.searchFilter = {
            email: '',
            name: '',
            phone: '',
            category: '',
            city: '',
            familycode: '',
            customerOf: '',
            accno: '',
            salesOrPurchase: '',
            customAttribute: [],
            customer_type: '',
            activeOrDeactiveCutomer: ''
        };
        $scope.print = function () {
            window.print();
        }

        $scope.initDetailTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initDetailTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
            // $scope.initTableFilterTimeoutPromise = $timeout($scope.getListPrint, 300);
        }

        $scope.selectCustomerId = '';
        $scope.showdeletePopup = false;
        $scope.isdeleteCategoryLoadingProgress = false;
        $scope.customAttributeList = [];
        $scope.showPopup = function (id)
        {
            $scope.selectCustomerId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function ()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteCategoryLoadingProgress = false;
        };
        
        $scope.showActivatePopup = false;
        $scope.isActivateVendorLoadingProgress = false;
        $scope.showActdactPopup = function (id)
        {
            $scope.selectCustomerId = id;
            $scope.showActivatePopup = true;
        };

        $scope.closeActivatePopup = function ()
        {
            $scope.showActivatePopup = false;
            $scope.isActivateVendorLoadingProgress = false;
        };
        
        $scope.activateCustomerInfo = function ( )
        {
            if ($scope.isActivateVendorLoadingProgress == false)
            {
                $scope.isActivateVendorLoadingProgress = true;
                var getListParam = {};
                getListParam.id = $scope.selectCustomerId;
                getListParam.is_active = 1;
                var headers = {};
                headers['screen-code'] = 'customer';
                adminService.deleteCustomer(getListParam, getListParam.id, getListParam.is_active, headers).then(function (response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closeActivatePopup();
                        $scope.getList();
                    }
                    $scope.isActivateVendorLoadingProgress = false;
                });
            }
        };

        $scope.refreshScreen = function ()
        {
            $scope.searchFilter = {
                email: '',
                name: '',
                phone: '',
                salesOrPurchase: '',
                customAttribute: [],
                customer_type: '',
                activeOrDeactiveCutomer: ''
            };
            $scope.initTableFilter();
        }
        $scope.update = function (isfilteropen)
        {
            $scope.searchFilter.customAttribute = [];
            if (isfilteropen == false)
            {
                $scope.initTableFilter();
            }
        }

        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'vendor_list_form';


        $scope.$on('onLocalStorageFormRenterCompleteEvent', function (event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });

        $scope.$on('onLocalStorageFieldRenterCompleteEvent', function (event, formName) {

            if ($scope.localStorageFormName == formName)
            {
                $scope.localStorageRenterCount++;
            }
            if ($scope.localStorageRenterCount >= $scope.localStorageCount)
            {
                console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
            }


        });

        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function (event, formName) {

            if ($scope.localStorageFormName == formName)
            {
                $scope.localStorageRetrieveCount++;
            }
            if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
            {
                $scope.initTableFilter();
            }
            console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);

        });

        $scope.init = function ()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');

        };

        $scope.$on('onLocalStorageReadyEvent', function () {

            $timeout(function () {
                $scope.init();
            }, 300);


        });
        $scope.deleteCustomerInfo = function ( )
        {
            if ($scope.isdeleteCategoryLoadingProgress == false)
            {
                $scope.isdeleteCategoryLoadingProgress = true;
                var getListParam = {};
                getListParam.id = $scope.selectCustomerId;
                var headers = {};
                headers['screen-code'] = 'customer';
                adminService.deleteCustomer(getListParam, getListParam.id, headers).then(function (response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getList();
                    }
                    $scope.isdeleteCategoryLoadingProgress = false;
                });
            }
        };

//        
//        if ($scope.customerFilter != '' && $scope.customerFilter != null && $scope.customerFilter != undefined)
//        {
//            $scope.searchFilter.customerInfo = $scope.customerFilter;
//            $localStorage["sales_order_list_form-order_customer_dropdown"] = $scope.searchFilter.customerInfo;
//        }

        $scope.getcustomerList = function (val) {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.companyId = $rootScope.userModel.companyId;
            autosearchParam.mode = 1;
            autosearchParam.sort = 1;
            autosearchParam.is_active = 1;
            autosearchParam.start = ($scope.accountModel.currentPage - 1) * $scope.accountModel.limit;
            autosearchParam.limit = $scope.accountModel.limit;
            return $httpService.get(APP_CONST.API.CUSTOMERS_LIST, autosearchParam, false).then(function (responseData) {
                var data = responseData.data;
                var hits = data.list;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.length = 0;
        $scope.colspan = 9;
        $scope.pagespan = 7;
        $scope.getCustomAttributeList = function () {

            var getListParam = {};
            getListParam.id = "";
            getListParam.attributetype_id = "";
            getListParam.attribute_code = "";
            getListParam.attributetypecode = "crm";
            getListParam.is_show_in_list = "1";
            getListParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getAttributeList(getListParam, configOption).then(function (response)
            {
                var data = response.data.list;
                if (data.length != 0)
                {
                    $scope.customAttributeList = data;
                    for (var i = 0; i < $scope.customAttributeList.length; i++)
                    {
                        if ($scope.customAttributeList[i].input_type == 'dropdown')
                        {
                            $scope.optionList = [];
                            $scope.optionList = $scope.customAttributeList[i].option_value.split(',');
                            $scope.customAttributeList[i].option_value = $scope.optionList;
                        }
                    }
                    $scope.length = $scope.customAttributeList.length;
                    $scope.colspan = $scope.colspan + $scope.length;
                    $scope.pagespan = $scope.colspan - 2;
                }
            });
        };
        $scope.getList = function (val) {
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'customer';
            getListParam.id = "";
            getListParam.fname = $scope.searchFilter.name;
            getListParam.email = $scope.searchFilter.email;
            getListParam.phone = $scope.searchFilter.phone;
            getListParam.customer_type = $scope.searchFilter.customer_type;
            getListParam.is_purchase = 1;
            if ($scope.searchFilter.activeOrDeactiveCutomer == 1)
            {
                getListParam.is_active = 0;
            } else
            {
                getListParam.is_active = 1;
            }
            var j = 0;
            for (var i = 0; i < $scope.customAttributeList.length; i++)
            {
                if ($scope.searchFilter.customAttribute[i] != 'undefined' && $scope.searchFilter.customAttribute[i] != null && $scope.searchFilter.customAttribute[i] != '')
                {
                    var j = j + 1;
                    getListParam['cus_attr_' + j] = $scope.customAttributeList[i].attribute_code + '::' + $scope.searchFilter.customAttribute[i];
                }
            }
            if ($rootScope.vendorNext == false) //For Demo Guide purpose
            {
                getListParam.start = 0;
                getListParam.limit = 10;
            } else {
                getListParam.start = ($scope.customerModel.currentPage - 1) * $scope.customerModel.limit;
                getListParam.limit = $scope.customerModel.limit;
            }
//            getListParam.start = ($scope.customerModel.currentPage - 1) * $scope.customerModel.limit;
//            getListParam.limit = $scope.customerModel.limit;
            $scope.customerModel.isLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getCustomersList(getListParam, configOption, headers).then(function (response)
            {
                if (response.data.success == true)
                {
                    var data = response.data.list;
                    $scope.customerModel.list = data;
                    $scope.customerModel.total = response.data.total;
                     if ($scope.customerModel.list.length > 0)
                    {
                        $window.localStorage.setItem("vendorAdded", "true");
                    }
                    if ($rootScope.userModel.new_user == 1 && $rootScope.vendorNext != false)
                    {
                        $window.localStorage.setItem("demo_guide", "true");
                        $rootScope.closeDemoPopup(4);
                    }
                }
                $scope.customerModel.isLoadingProgress = false;
            });
        };

        $scope.getListPrint = function (val)
        {
            var getListParam = {};
            getListParam.Id = "";
            getListParam.start = 0;
            $scope.customerModel.isLoadingProgress = true;
            adminService.getCustomersList(getListParam).then(function (response) {
                var data = response.data.list;
                $scope.customerModel.printList = data;
                $scope.customerModel.isLoadingProgress = false;
            });
        };
        $scope.init();

//        $scope.getCustomAttributeList();
        // $scope.getListPrint();
    }]);








