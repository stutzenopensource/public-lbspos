

'use strict';
/* Controllers */

angular.module('app')
        .controller('AppCtrl', ['$scope', '$rootScope', '$translate', '$localStorage', '$window', '$location', '$state', '$timeout', '$cookies', 'Auth', 'StutzenHttpService', 'APP_CONST', "$httpService", "adminService", function ($scope, $rootScope, $translate, $localStorage, $window, $location, $state, $timeout, $cookies, Auth, StutzenHttpService, APP_CONST, httpService, adminService) {

                $rootScope.userModel = {
                    userName: '',
                    role_id: '',
                    role: [''],
                    business_list: [],
                    notificationList: [],
                    notificationcount: ''
                };
                $scope.tamlag = false;
                $rootScope.selectedLanguage = $translate.use();
                $rootScope.changeLanguage = function (selectedLanguage) {
                    $translate.use(selectedLanguage);
                    if (selectedLanguage === 'tl') {
                        window.localStorage.setItem("tamil", "true");
                        $scope.tamlag = true;
                    } else {
                        window.localStorage.setItem("tamil", "false");
                        $scope.tamlag = false;
                    }
                };
                window.reload = function () {
                    if (localStorage.getItem("tamil") === "true") {
                        $scope.tamlag = true;
                    } else {
                        $scope.tamlag = false;
                    }
                };
                window.reload();
                $rootScope.appDomain = $window.location.host;
                $rootScope.screenAccessDetail = {
                    "customer": {
                        'is_view': false
                    },
                    "expense": {
                        'is_view': false
                    },
                    "purchaseestimate": {
                        'is_view': false
                    },
                    "purchaseinvoice": {
                        'is_view': false
                    },
                    "salesestimate": {
                        'is_view': false
                    },
                    "salesinvoice": {
                        'is_view': false
                    },
                    "salesorder": {
                        'is_view': false
                    },
                    "product": {
                        'is_view': false
                    },
                    "service": {
                        'is_view': false
                    },
                    "uom": {
                        'is_view': false
                    },
                    "users": {
                        'is_view': false
                    },
                    "role": {
                        'is_view': false
                    },
                    "category": {
                        'is_view': false
                    },
                    "incomecategory": {
                        'is_view': false
                    },
                    "expensecategory": {
                        'is_view': false
                    },
                    "attribute": {
                        'is_view': false
                    },
                    "appsettings": {
                        'is_view': false
                    },
                    "changepassword": {
                        'is_view': false
                    },
                    "purchaseheader": {
                        'is_view': false
                    },
                    "salesheader": {
                        'is_view': false
                    },
                    "produtserviceheader": {
                        'is_view': false
                    },
                    "settingsheader": {
                        'is_view': false
                    },
                    "photographyheader": {
                        'is_view': false
                    },
                    "dealheader": {
                        'is_view': false
                    },
                    "catalogheader": {
                        'is_view': false
                    },
                    "humanresourceheader": {
                        'is_view': false
                    }
                };
                $rootScope.screenStateCode = {
                    "customer": "app.customers",
                    "purchaseestimate": "app.purchaseQuote",
                    "purchaseinvoice": "app.purchaseInvoice",
                    "salesestimate": "app.salesquote",
                    "salesorder": "app.salesorder",
                    "salesinvoice": "app.invoice",
                    "product": "app.productList",
                    "service": "app.serviceList",
                    "uom": "app.uom",
                    "users": "app.userlist",
                    "role": "app.userrole",
                    "tax": "app.taxlist",
                    "incomecategory": "app.incomecategory",
                    "expensecategory": "app.expensecategory",
                    "attribute": "app.attribute",
                    "appSettings": "app.appSettings",
                    "businesssettings": "app.businessSettings",
                    "changepassword": "app.changepassword",
                    "dashboard": "app.dashboard",
                    "account": "app.account",
                    "accountbalance": "app.accountbalance",
                    "transactionreport": "app.transaction",
                    "salesreport": "app.sales",
                    "itemsalesreport": "app.itemizedsales",
                    "paymentreport": "app.paymentreport",
                    "receivablereport": "app.receivablereport",
                    "payablereport": "app.payablereport",
                    "partysalesreport": "app.partysalesstatement",
                    "partypurchasereport": "app.partypurchasestatement",
                    "invoicesettings": "app.invoicesettings",
                    "smssettings": "app.smssettings",
                    "emailsettings": "app.emailsettings",
                    "taxreport": "app.tax",
                    "taxsummaryreport": "app.taxsummary",
                    "transdeposit": "app.deposit",
                    "transexpense": "app.transexpense",
                    "accountstatement": "app.accountstatement",
                    "stockadjustment": "app.stockadjustment",
                    "stockwastage": "app.stockwastage",
                    "minstock": "app.minstock",
                    "inventorystock": "app.inventorystock",
                    "stocktrace": "app.stocktracelist",
                    "activity": "app.activitylist",
                    "activitylog": "app.activitylog",
                    "category": "app.categorylist",
                    "transfer": "app.transferlist",
                    "paymentterms": "app.paymentterms",
                    "databasebackup": "app.databasebackup",
                    "taxgroup": "app.taxgroup",
                    "expensereport": "app.expensereport",
                    "albumlist": "app.albumlist",
                    "deal": "app.deal",
                    "stage": "app.stage",
                    "country": "app.country",
                    "state": "app.state",
                    "city": "app.city",
                    "prefixsettings": "app.prefixsettings",
                    "pos": "app.pos",
                    "discountedreport": "app.discountedreport",
                    "partywisepurchasereport": "app.partywisepurchasereport",
                    "salestocus": "app.salestocus",
                    "purfromcus": "app.purfromcus",
                    "purchasebaseinventory": "app.itemwisesalespurchase",
                    "partysalespurbased": "app.partysalespurchasebased",
                    "customersettings": "app.customersettings",
                    "categorystockreport": "app.categorystockreport",
                    "categorywisereport": "app.categorywisereport",
                    "barcodewisestockreport": "app.barcodewisestockreport",
                    "debitnote": "app.debitnote",
                    "creditnote": "app.creditnote",
                    "acodestatement": "app.acodestatement",
                    "tag": "app.tag",
                    "catalogproduct": "app.catalogproduct",
                    "catalogcategory": "app.catalogcategory",
                    "categoryCatlog": "app.catelogcategory",
                    "purchaseinvoicereturn": "app.purchaseInvoiceReturn",
                    "accountsummaryreport": "app.accountsummary",
                    "accountdetailreport": "app.accountdetail",
                    "itemwiseageingreport": "app.itemwiseageingreport",
                    "categorywiseageingstockreport": "app.categorywiseageingstockreport",
                    "stockadjustmentreport": "app.stockadjustmentreport",
                    "stockwastagereport": "app.stockwastagereport",
                    "categorystockadjustreport": "app.categorystockadjustreport",
                    "categorystockwastagereport": "app.categorystockwastagereport",
                    "employee": "app.employee",
                    "designation": "app.designation",
                    "empsalesreport": "app.empsalesreport",
                    "deliverynotereport": "app.deliverynotereport",
                    "daylogreport": "app.daylogreport",
                    "deliverynote": "app.deliverynote",
                    "vendor": "app.vendor",
                    "purchasepayment":"app.purchasepayment"
                };

                $rootScope.appConfig = {
                    selectedLanguage: 'en',
                    date_format: "dd/MM/yyyy",
                    navMenuExpand: true,
                    currency: "Rs",
                    thousand_seperator: '',
                    uomdisplay: '',
                    uomlabel: '',
                    invoice_item_custom1: '',
                    invoice_item_custom2: '',
                    invoice_item_custom3: '',
                    invoice_item_custom4: '',
                    invoice_item_custom5: '',
                    invoice_item_custom1_label: '',
                    invoice_item_custom2_label: '',
                    invoice_item_custom3_label: '',
                    invoice_item_custom4_label: '',
                    invoice_item_custom5_label: '',
                    purchase_invoice_item_custom1: '',
                    purchase_invoice_item_custom2: '',
                    purchase_invoice_item_custom3: '',
                    purchase_invoice_item_custom4: '',
                    purchase_invoice_item_custom5: '',
                    purchase_invoice_item_custom1_label: '',
                    purchase_invoice_item_custom2_label: '',
                    purchase_invoice_item_custom3_label: '',
                    purchase_invoice_item_custom4_label: '',
                    purchase_invoice_item_custom5_label: '',
                    purchase_uom: '',
                    purchase_uom_label: '',
                    app_tile: '',
                    invoice_prefix: '',
                    pay_to_address: '',
                    signature: '',
                    sms_count: '',
                    poscusid: '',
                    bank_detail: '',
                    invoicefrom: '',
                    invoicecc: '',
                    invoice_print_template: '',
                    timezone: '',
                    pos_customer_id: '',
                    pos_bank_account_id: '',
                    sales_product_list: '',
                    gst_state: '',
                    gst_state_name: '',
                    gst_auto_suggest: '',
                    customer_gst_no: '',
                    companynameinprint: '',
                    'paytocity':'',
                    'paytostate':'',
                    'paytocountry':'',
                    'paytophone_no':'',
                    'paytomobile_no':'',
                    'company_website':'',
                    'company_email' :''
                };
                $rootScope.CONTEXT_ROOT = "/";
                /*
                 *  It used to take APP_CONST value in UI
                 *
                 */
                $rootScope.APP_CONST = angular.copy(APP_CONST);
                // config
                $rootScope.app = {
                    roleSelectPopup: false,
                    selectedRoleIndex: -1,
                    selectedRole: '',
                    selectedClinicId: '',
                    selectedClinicName: ''
                }

                $rootScope.getNavigationBlockMsg = null;
                $rootScope.cookieDisconnectError = '';
                window.onbeforeunload = function (event) {

                    var message = null;
                    if ($rootScope.getNavigationBlockMsg != null)
                    {
                        message = $rootScope.getNavigationBlockMsg(true);
                    }
                    if (message != null && message.length > 0)
                    {
                        return message;
                    }
                }
                window.addEventListener('online', onLineIndicatorHandler);
                window.addEventListener('offline', onLineIndicatorHandler);
                function onLineIndicatorHandler($event, response)
                {
                    if ($event.type !== undefined && $event.type.toLowerCase() === 'offline')
                    {
                        $timeout(function () {
                            $scope.serverDisconnectError = "Disconnected from the server";
                        });
                    } else
                    {
                        $timeout(function () {
                            $scope.serverDisconnectError = "";
                        });
                    }
                }
                ;

                $scope.$on('$destroy', function () {
                    delete window.onbeforeunload;
                    angular.element(window).off('online', onLineIndicatorHandler);
                    angular.element(window).off('offline', onLineIndicatorHandler);
                });
                $scope.getContainerClass = function ()
                {
                    return $rootScope.containerClass;
                }

                $scope.print = function ()
                {
                    window.print();
                }

                $scope.expandHandler = function ()
                {
                    $timeout(function () {
                        $rootScope.appConfig.navMenuExpand = !$rootScope.appConfig.navMenuExpand;
                    }, 0);
                }

                $rootScope.refer = "";
                $rootScope.cloudImageAccessKey = {};
                $rootScope.isLogined = false;
                $rootScope.isUserModelUpdated = false;
                $rootScope.cloudImagePrefixUrl = APP_CONST.CLOUD_IMAGE_PREFIX_URL;
                $rootScope.autoSearchList = [];
                $rootScope.autoSearchLimit = 5;
                $rootScope.searchMapLocation = "";
                $rootScope.searchKeyword = "";
                $scope.cookieAcception = typeof $cookies.get('cookie-acception') == 'undefined' ? false : true;
                $scope.signupRedirectPopup = false;
                $scope.signinRedirectPopup = false;
                $scope.serverDisconnectError = "";
                $scope.actionMsgSuccess = "";
                $scope.actionMsgWarning = "";
                $scope.actionMsgError = "";
                $scope.errorMsgAliveTime = 5000;
                $scope.successMsgAliveTime = 5000;
                $scope.warningMsgAliveTime = 5000;
                $scope.autoSearchServerSyncDelayTime = 200;
                $scope.autoCompleteFilterList = '';
                $scope.mapSearchOptions = {
                    //country: 'uk',
                    //types: '(cities)'
                };
                //            $scope.search = {
                //
                //                autosuggestlist : []
                //
                //            };
                $scope.searchMapLocationGecodeDetail = null;
                $scope.searchMapLocationDetail = null;
                //$scope.searchMapLocation = "";
                $scope.autoSearchDetail = null;
                $scope.aliveAutoSearch = false;
                $scope.autoSearchList = [];
                $scope.defaultState = '';
                $scope.isScreenDataLoaded = false;
                $scope.autoSearchTimeoutPromise = null;
                $scope.autoSearchList1 = [{
                        name: 'Global',
                        searchType: 'Company'
                    }, {
                        name: 'Rosy',
                        searchType: 'Staff'
                    }];
                //            $scope.tags = [ 'bootstrap', 'list', 'angular' ];
                //            $scope.allTags = [ 'bootstrap', 'list', 'angular', 'directive', 'edit', 'label', 'modal', 'close', 'button', 'grid', 'javascript', 'html', 'badge', 'dropdown'];
                //
                $scope.$on('updateUserInfoEvent', updateUserInfoEventHandler);
                $scope.$on('updateScreenAccessInfoEvent', updateScreenAccessInfoHandler);
                $scope.$on('updateBusinessListInfoEvent', updateBusinessListInfoHandler);
                $scope.$on('updateAppSettings', updateAppSettingHandler);
                $scope.$on('httpGenericErrorEvent', genericHttpErrorHandler);
                $scope.$on('httpGenericResponseSuccessEvent', genericHttpResponseHandler);
                $scope.$on('genericErrorEvent', genericErrorHandler);
                $scope.$on('genericSuccessEvent', genericSuccessHandler);
                //temp fix for search keyword update.
                //Started temp fix to update the map location.
                $scope.keywoardChange = function (value) {
                    $rootScope.$broadcast('updateSearchKeywordEvent', value);
                }

                $scope.beyondChange = function (value) {
                    $rootScope.$broadcast('updateSearchBeyondEvent', value);
                }

                //$scope.$on('updateSearchKeywordEvent', updateSearchKeywordEventHandler);
                //$scope.$on('updateSearchBeyondEvent', updateSearchBeyondEventHandler);
                // $scope.$on('updateGeocodeEvent', updateGeocodeEventHandler);

                function updateSearchKeywordEventHandler($event, data)
                {
                    $scope.keyword = data;
                }

                function updateSearchBeyondEventHandler($event, data)
                {
                    $scope.homeSearchBeyond = data;
                }

                $scope.clearPreviousError = function ()
                {
                    $scope.actionMsgError = '';
                    $scope.actionMsgSuccess = '';
                    $scope.actionMsgWarning = '';
                }
                $scope.changeMsgSucessAction = function ()
                {
                    $scope.actionMsgSuccess = '';
                }
                $scope.changeMsgWarningAction = function ()
                {
                    $scope.actionMsgWarning = '';
                }
                $scope.changeMsgErrorAction = function ()
                {
                    $scope.actionMsgError = '';
                }
                $scope.setErrorMessage = function (msg)
                {
                    if (msg != '') {
                        $scope.clearPreviousError();
                    }
                    $scope.actionMsgError = msg;
                    $timeout(function () {
                        $scope.actionMsgError = '';
                    }, $scope.errorMsgAliveTime);
                };
                $scope.setSuccessMessage = function (msg)
                {
                    if (msg != '') {
                        $scope.clearPreviousError();
                    }
                    $scope.actionMsgSuccess = msg;
                    $timeout(function () {
                        $scope.actionMsgSuccess = '';
                    }, $scope.successMsgAliveTime);
                };
                $scope.setWarningMessage = function (msg)
                {
                    if (msg != '') {
                        $scope.clearPreviousError();
                    }
                    $scope.actionMsgWarning = msg;
                    $timeout(function () {
                        $scope.actionMsgWarning = '';
                    }, $scope.warningMsgAliveTime);
                };
                $scope.logout = function ()
                {
                    httpService.get(APP_CONST.LOGOUT_API_URL, {}, true).then(function (response) {
                        $scope.setSuccessMessage("You have logout successfully.");
                        var cookieOption = {
                            path: "/"
                        };
                        $cookies.remove("api-token", cookieOption);
                        window.localStorage.clear();
                        $timeout(function () {
                            $window.location.href = APP_CONST.SSO_LOGOUT_URL;

                        }, 100);
                    });
                }
                $rootScope.showHelpMenu = false;
                $rootScope.showDemoPopup = function ()
                {
                    $window.localStorage.setItem("demo_guide", "true");
                    if ($rootScope.userModel.new_user == 0)
                    {
                        $scope.updateNewUser();
                    } else {
                        $rootScope.showHelpMenu = true;
                    }
                }

                $rootScope.endDemo = function () {
                    $('.tour-backdrop').hide();
                    $('.tour-step-background').hide();
                    $('.popover').remove();
                    $(".app-aside").css("position", "fixed");
                    $(".app-aside").css("z-index", "97");
                    $(".dashboard_details").css("height", "auto");
                }
                $scope.hideNow = false;
                $rootScope.closeDemoPopup = function (value)
                {
                    if (value == 'end')
                    {
                        if ($rootScope.userModel.new_user == 1)
                        {
                            swal({
                                title: "Guide Tour!",
                                text: "If you need any help click the help icon above the header!",
                                icon: "warning",
                                buttons: true,
                                dangerMode: true,
                            })
                                    .then((willDelete) => {
                                        if (willDelete) {
                                            $scope.updateOldUser();
                                        } else {

                                        }
                                    });
                        }
                        $rootScope.showHelpMenu = false;
                    } else {
                        $rootScope.showHelpMenu = false;
                        if ($window.localStorage.getItem("demo_guide") == 'true')
                        {
                            $(document).ready(function () {
                                var tour = new Tour({
                                    name: "tour",
                                    steps: [],
                                    template: "<div class='popover tour'><div class='arrows'></div><a ><img id='end_tour' data-role='end'  class='close_First_Pop' src='../img/close icon.png'/></a><h3 class='popover-title text-bold'></h3><div class='popover-content'></div><div class='popover-navigation clear_b'><div class='col-sm-6 prev'><button class='' style='border:0px;background:transparent;color:#000' data-role='prev'>« " + LANG.prev + "</button></div><div class='col-sm-6 nxt'><button class='' style='border:0px;background:transparent;color: #fff;' id='next' data-role='next'>" + LANG.next + " »</button><button class='' style='border:0px;background:transparent;color: #fff;'  data-role='end'>" + LANG.end_tour + " »</button></div></div></div>",
                                    backdrop: true,
                                    orphan: false,
                                    smartPlacement: false,
                                    reflex: false,
                                    autoscroll: true
                                });
                                if (typeof value != "undefined")
                                {

                                    $($scope.element).removeClass("tour-step-backdrop");
                                    $($scope.element + ' ' + ".hideMenu").css("display", "none");
                                    tour.setCurrentStep(value);
                                }
                                tour.addSteps([
                                    {
                                        path: '',
                                        element: '#tour_step2',
                                        title: LANG.tour_step2_title,
                                        content: LANG.tour_step2_content,
                                        template: "<div class='popover tour'><div class='arrows'></div><a id='end_tour'><img class='close_First_Pop'  src='../img/close icon.png'/></a><h3 class='popover-title text-bold'></h3><div class='popover-content'></div><ul class='paginer'><li class='selected_step'>1</li><li id='tour_step3'>2</li><li id='tour_step5'>3</li><li id='tour_step6'>4</li><li>...</li><li id='tour_step10'>9</li><li class='skip' style='float:right;text-decoration: underline;'><a id='end' data-role='end'>Skip</a></li></ul><div class='popover-navigation clear_b'><div class='col-sm-6 prev' data-role='prev'><button class='' style='border:0px;background:transparent;color:#000' >« " + LANG.prev + "</button></div><div class='col-sm-6 nxt' data-role='next'><button class='' style='border:0px;background:transparent;color: #fff;' id='next' >" + LANG.next + " »</button></div></div></div>",
                                        onShow: function (tour) {
                                            if (!$('#tour_step2_menu').closest('li').hasClass('active')) {
                                                $('#tour_step2').trigger('click');
                                            }
                                            $('#tour_step2_menu').addClass('open');
                                            $(".app-aside").css("position", "absolute");
                                            $(".app-aside").css("z-index", "auto");
                                            $(".app-content-body").css("height", "2000px");
                                            $("#login_Box_Div").show();
//                                            $rootScope.hideAlert = false;

                                        },
                                        onHide: function (tour) {
                                            $(".app-aside").css("position", "fixed");
                                            $(".app-aside").css("z-index", "97");
                                            $(".app-content-body").css("height", "auto");
                                            $(".tour-tour-element .hideMenu").css("display", "none");
                                            $(".dashboard_details").css("height", "auto");
                                        },
                                        onShown: function (tour) {
                                            var step = tour._options.steps[tour.getCurrentStep()];
                                            $scope.element = step.element;
                                            if ($rootScope.userModel.new_user == 1)
                                            {
                                                $(".nxt").css("opacity", "0.5");
                                                $(".nxt").css("pointer-events", "none");
//                                                $(".skip").css("display", "none");
                                                $($scope.element + ' ' + ".hideMenu").css("display", "block");
                                            }
//                                            setTimeout(function () {
                                            if ($window.localStorage.getItem("appSettingsAdded") == 'true')
                                            {
                                                $(".nxt").css("opacity", "1");
                                                $(".nxt").css("pointer-events", "auto");
                                                $window.localStorage.setItem("demo_guide", "true");
                                            }
//                                            }, 1000);
                                            $("#login_Box_Div").show();
                                            $("#end_tour").click(function () {
//                                                $scope.updateOldUser();
                                                $rootScope.closeDemoPopup('end');
                                            });
                                            if ($window.localStorage.getItem("demo_guide") == 'false')
                                            {
                                                $(".popover").css("display", "none");
                                                $(".tour-tour-element .hideMenu").css("display", "none");
                                                $(".app-content-body").css("height", "auto");
                                                $(".app-aside").css("position", "fixed");
                                                $("#login_Box_Div").show();
                                            }
                                            //$window.localStorage.setItem("demo_guide", "false");
                                            $rootScope.productNext = true;
                                        },
                                        onHidden: function (tour) {
                                            $(".tour-tour-element .hideMenu").css("display", "none");
                                        },

                                        onNext: function (tour) {
                                            $window.localStorage.setItem("demo_guide", "true");
                                            $rootScope.productNext = false;
                                            $(".dashboard_details").css("height", "2500px");
                                            $("#login_Box_Div5").show();
                                            $state.go('app.productList');
                                        }
                                    },

                                    {
                                        path: '',
                                        element: '#tour_step3',
                                        title: LANG.tour_step3_title,
                                        content: LANG.tour_step3_content,
                                        template: "<div class='popover tour' style='width:400px;'><div class='arrows'></div><a ><img id='end_tour' class='close_First_Pop' style='width:8%;' src='../img/close icon.png'/></a><h3 class='popover-title text-bold'></h3><div class='popover-content'></div><ul class='paginer'><li>1</li><li class='selected_step'>2</li><li>3</li><li>4</li><li>...</li><li>9</li><li style='float:right;text-decoration: underline;' class='skip'><a id='end' data-role='end'>Skip</a></li></ul><div class='popover-navigation clear_b'><div class='col-sm-6 prev' data-role='prev'><button class='' style='border:0px;background:transparent;color:#000' >« " + LANG.prev + "</button></div><div class='col-sm-6 nxt' data-role='next'><button class='' style='border:0px;background:transparent;color: #fff;' id='next' >" + LANG.next + " »</button></div></div></div>",
                                        onShow: function (tour) {
                                            if (!$('#tour_step2_menu').closest('li').hasClass('active')) {
                                                $('#tour_step3').trigger('click');
                                            }
                                            $(".app-aside").css("position", "absolute");
                                            $(".app-aside").css("z-index", "auto");
                                            $(".app-content-body").css("height", "2000px");
                                            $("#login_Box_Div5").show();
//                                            $rootScope.hideAlert = false;
//                                        $("#tour_step2").removeClass("tour-step-backdrop");
                                        },
                                        onHide: function (tour) {
                                            $(".app-aside").css("position", "fixed");
                                            $(".app-aside").css("z-index", "97");
                                            $(".app-content-body").css("height", "auto");
                                            $(".tour-tour-element .hideMenu").css("display", "none");
                                            var step = tour._options.steps[tour.getCurrentStep()];
                                            $scope.element = step.element;
                                            $(".dashboard_details").css("height", "auto");
                                        },
                                        onShown: function (tour) {

                                            var step = tour._options.steps[tour.getCurrentStep()];
                                            $scope.element = step.element;
                                            if ($rootScope.userModel.new_user == 1)
                                            {
                                                $($scope.element + ' ' + ".hideMenu").css("display", "block");
                                            }
                                            if ($rootScope.userModel.new_user == 1)
                                            {
                                                $(".nxt").css("opacity", "0.5");
                                                $(".nxt").css("pointer-events", "none");
                                                $($scope.element + ' ' + ".hideMenu").css("display", "block");
                                            }
                                            if ($window.localStorage.getItem("productAdded") == 'true')
                                            {
                                                $(".nxt").css("opacity", "1");
                                                $(".nxt").css("pointer-events", "auto");
                                                $window.localStorage.setItem("demo_guide", "true");
                                            }
                                            $("#login_Box_Div5").show();
                                            $("#end_tour").click(function () {
//                                                $scope.updateOldUser();
                                                $rootScope.closeDemoPopup('end');
                                            });
                                            if ($window.localStorage.getItem("demo_guide") == 'false')
                                            {
                                                $(".popover").css("display", "none");
                                                $(".tour-tour-element .hideMenu").css("display", "none");
                                                $(".app-content-body").css("height", "auto");
                                                $(".app-aside").css("position", "fixed");
                                                $("#login_Box_Div5").show();
                                            }
                                            $window.localStorage.setItem("demo_guide", "false");
                                            $window.localStorage.setItem("tour_current_step", "1");
                                            $rootScope.customerNext = true;
                                        },
                                        onHidden: function (tour) {
                                            $(".tour-tour-element .hideMenu").css("display", "none");
                                        },
                                        onPrev: function (tour) {
                                            var step = tour._options.steps[tour.getCurrentStep()];
                                            $scope.element = step.element;
                                            if ($window.localStorage.getItem("demo_guide") == 'false')
                                            {
                                                $(".popover").css("display", "block");
                                                $window.localStorage.setItem("demo_guide", "true");
                                            }
                                        },

                                        onNext: function (tour) {
                                            $window.localStorage.setItem("demo_guide", "true");
                                            $rootScope.customerNext = false;
                                            $(".dashboard_details").css("height", "2500px");
                                            $("#login_Box_Div1").show();
                                            $state.go('app.customers');
                                        },
                                    },
                                    {
                                        path: '',
                                        element: '#tour_step4',
                                        title: LANG.tour_step4_title,
                                        content: LANG.tour_step4_content,
                                        template: "<div class='popover tour' style='width:400px;'><div class='arrows'></div><a ><img id='end_tour' class='close_First_Pop' style='width:8%;' src='../img/close icon.png'/></a><h3 class='popover-title text-bold'></h3><div class='popover-content'></div><ul class='paginer'><li>1</li><li >2</li><li class='selected_step'>3</li><li>4</li><li>...</li><li>9</li><li style='float:right;text-decoration: underline;' class='skip'><a id='end' data-role='end'>Skip</a></li></ul><div class='popover-navigation clear_b'><div class='col-sm-6 prev' data-role='prev'><button class='' style='border:0px;background:transparent;color:#000' >« " + LANG.prev + "</button></div><div class='col-sm-6 nxt' data-role='next'><button class='' style='border:0px;background:transparent;color: #fff;' id='next' >" + LANG.next + " »</button></div></div></div>",
                                        onShow: function (tour) {
                                            if (!$('#tour_step4_menu').closest('li').hasClass('active')) {
                                                $('#tour_step4').trigger('click');
                                            }
                                            $(".app-aside").css("position", "absolute");
                                            $(".app-aside").css("z-index", "auto");
                                            $(".app-content-body").css("height", "2000px");
                                            $("#login_Box_Div1").show();
//                                            $rootScope.hideAlert = false;
//                                        $("#tour_step2").removeClass("tour-step-backdrop");
                                        },
                                        onHide: function (tour) {
                                            $(".app-aside").css("position", "fixed");
                                            $(".app-aside").css("z-index", "97");
                                            $(".app-content-body").css("height", "auto");
                                            $(".tour-tour-element .hideMenu").css("display", "none");
                                            var step = tour._options.steps[tour.getCurrentStep()];
                                            $scope.element = step.element;
                                            $(".dashboard_details").css("height", "auto");
                                        },
                                        onShown: function (tour) {

                                            var step = tour._options.steps[tour.getCurrentStep()];
                                            $scope.element = step.element;
                                            if ($rootScope.userModel.new_user == 1)
                                            {
                                                $($scope.element + ' ' + ".hideMenu").css("display", "block");
                                            }
                                            if ($rootScope.userModel.new_user == 1)
                                            {
                                                $(".nxt").css("opacity", "0.5");
                                                $(".nxt").css("pointer-events", "none");
                                                $($scope.element + ' ' + ".hideMenu").css("display", "block");
                                                $("#login_Box_Div1").show();
                                            }
                                            if ($window.localStorage.getItem("customerAdded") == 'true')
                                            {
                                                $(".nxt").css("opacity", "1");
                                                $(".nxt").css("pointer-events", "auto");
                                                $window.localStorage.setItem("demo_guide", "true");
                                            }
                                            $("#login_Box_Div1").show();
                                            $("#end_tour").click(function () {
//                                                $scope.updateOldUser();
                                                $rootScope.closeDemoPopup('end');
                                            });
                                            if ($window.localStorage.getItem("demo_guide") == 'false')
                                            {
                                                $(".popover").css("display", "none");
                                                $(".tour-tour-element .hideMenu").css("display", "none");
                                                $(".app-content-body").css("height", "auto");
                                                $(".app-aside").css("position", "fixed");
                                                $("#login_Box_Div1").show();
                                            }
                                            $window.localStorage.setItem("demo_guide", "false");
                                            $window.localStorage.setItem("tour_current_step", "2");
                                            $rootScope.vendorNext = true;
                                        },
                                        onHidden: function (tour) {
                                            $(".tour-tour-element .hideMenu").css("display", "none");
                                        },
                                        onPrev: function (tour) {
                                            var step = tour._options.steps[tour.getCurrentStep()];
                                            $scope.element = step.element;
                                            if ($window.localStorage.getItem("demo_guide") == 'false')
                                            {
                                                $(".popover").css("display", "block");
                                                $window.localStorage.setItem("demo_guide", "true");
                                            }
                                        },

                                        onNext: function (tour) {
                                            $window.localStorage.setItem("demo_guide", "true");
                                            $rootScope.vendorNext = false;
                                            $(".dashboard_details").css("height", "2500px");
                                            $("#login_Box_Div2").show();
                                            $state.go('app.vendor');
                                        },
                                    },
                                    {
                                        path: '',
                                        element: '#tour_step5',
                                        title: LANG.tour_step5_title,
                                        content: LANG.tour_step5_content,
                                        template: "<div class='popover tour' style='width:400px;'><div class='arrows'></div><a ><img id='end_tour' class='close_First_Pop' style='width:8%;' src='../img/close icon.png'/></a><h3 class='popover-title text-bold'></h3><div class='popover-content'></div><ul class='paginer'><li>1</li><li >2</li><li>3</li><li class='selected_step'>4</li><li>...</li><li>9</li><li style='float:right;text-decoration: underline;' class='skip'><a id='end' data-role='end'>Skip</a></li></ul><div class='popover-navigation clear_b'><div class='col-sm-6 prev' data-role='prev'><button class='' style='border:0px;background:transparent;color:#000' >« " + LANG.prev + "</button></div><div class='col-sm-6 nxt' data-role='next'><button class='' style='border:0px;background:transparent;color: #fff;' id='next' >" + LANG.next + " »</button></div></div></div>",
                                        onShow: function (tour) {
                                            if (!$('#tour_step5_menu').closest('li').hasClass('active')) {
                                                $('#tour_step5').trigger('click');
                                            }
                                            $(".app-aside").css("position", "absolute");
                                            $(".app-aside").css("z-index", "auto");
                                            $(".app-content-body").css("height", "2000px");
                                            $("#login_Box_Div2").show();
//                                            $rootScope.hideAlert = false;
//                                        $("#tour_step2").removeClass("tour-step-backdrop");
                                        },
                                        onHide: function (tour) {
                                            $(".app-aside").css("position", "fixed");
                                            $(".app-aside").css("z-index", "97");
                                            $(".app-content-body").css("height", "auto");
                                            $(".tour-tour-element .hideMenu").css("display", "none");
                                            var step = tour._options.steps[tour.getCurrentStep()];
                                            $scope.element = step.element;
                                            $(".dashboard_details").css("height", "auto");
                                            $(".popover").css("display", "none");
                                        },
                                        onShown: function (tour) {

                                            var step = tour._options.steps[tour.getCurrentStep()];
                                            $scope.element = step.element;
                                            if ($rootScope.userModel.new_user == 1)
                                            {
                                                $($scope.element + ' ' + ".hideMenu").css("display", "block");
                                            }
                                            if ($rootScope.userModel.new_user == 1)
                                            {
                                                $(".nxt").css("opacity", "0.5");
                                                $(".nxt").css("pointer-events", "none");
                                                $($scope.element + ' ' + ".hideMenu").css("display", "block");
                                                $("#login_Box_Div2").show();
                                            }
                                            if ($window.localStorage.getItem("customerAdded") == 'true')
                                            {
                                                $(".nxt").css("opacity", "1");
                                                $(".nxt").css("pointer-events", "auto");
                                                $window.localStorage.setItem("demo_guide", "true");
                                            }
                                            $("#login_Box_Div2").show();
                                            $("#end_tour").click(function () {
//                                                $scope.updateOldUser();
                                                $rootScope.closeDemoPopup('end');
                                            });
                                            if ($window.localStorage.getItem("demo_guide") == 'false')
                                            {
                                                $(".popover").css("display", "none");
                                                $(".tour-tour-element .hideMenu").css("display", "none");
                                                $(".app-content-body").css("height", "auto");
                                                $(".app-aside").css("position", "fixed");
                                                $("#login_Box_Div2").show();
                                            }
                                            $window.localStorage.setItem("demo_guide", "false");
                                            $window.localStorage.setItem("tour_current_step", "3");
                                            $rootScope.paymentNext = true;
                                        },
                                        onHidden: function (tour) {
                                            $(".tour-tour-element .hideMenu").css("display", "none");
                                        },
                                        onPrev: function (tour) {
                                            var step = tour._options.steps[tour.getCurrentStep()];
                                            $scope.element = step.element;
                                            if ($window.localStorage.getItem("demo_guide") == 'false')
                                            {
                                                $(".popover").css("display", "block");
                                                $window.localStorage.setItem("demo_guide", "true");
                                            }
                                        },

                                        onNext: function (tour) {
                                            $window.localStorage.setItem("demo_guide", "true");
                                            $rootScope.paymentNext = false;
                                            $(".dashboard_details").css("height", "2500px");
                                            $("#login_Box_Div").show();
                                            $state.go('app.paymentterms');
                                        },
                                    },
                                    {
                                        path: '',
                                        element: '#tour_step6',
                                        title: LANG.tour_step6_title,
                                        content: LANG.tour_step6_content,
                                        template: "<div class='popover tour'><div class='arrows'></div><a ><img id='end_tour' style='width:7%;' class='close_First_Pop' src='../img/close icon.png'/></a><h3 class='popover-title text-bold'></h3><div class='popover-content'></div><ul class='paginer'><li>3</li><li >4</li><li class='selected_step'>5</li><li>6</li><li>...</li><li>9</li><li class='skip' style='float:right;text-decoration: underline;'><a id='end' data-role='end'>Skip</a></li></ul><div class='popover-navigation clear_b'><div class='col-sm-6 prev' data-role='prev'><button class='' style='border:0px;background:transparent;color:#000' >« " + LANG.prev + "</button></div><div class='col-sm-6 nxt' data-role='next'><button class='' style='border:0px;background:transparent;color: #fff;' id='next' >" + LANG.next + " »</button></div></div></div>",
                                        onShow: function (tour) {
                                            if (!$('#tour_step2_menu').closest('li').hasClass('active')) {
                                                $('#tour_step6').trigger('click');
                                            }
                                            $(".app-aside").css("position", "absolute");
                                            $(".app-aside").css("z-index", "auto");
                                            $(".app-content-body").css("height", "2000px");
                                            $("#login_Box_Div").show();
//                                            $rootScope.hideAlert = false;
//                                        $("#tour_step5").removeClass("tour-step-backdrop");
                                        },
                                        onHide: function (tour) {
                                            $(".app-aside").css("position", "fixed");
                                            $(".app-aside").css("z-index", "97");
                                            $(".app-content-body").css("height", "auto");
                                            $(".tour-tour-element .hideMenu").css("display", "none");
//                                            $window.localStorage.setItem("demo_guide", "true");
                                            $(".dashboard_details").css("height", "auto");
                                        },
                                        onHidden: function (tour) {
                                            $(".dashboard_details").css("height", "auto");
                                        },
                                        onShown: function () {
                                            var step = tour._options.steps[tour.getCurrentStep()];
                                            $scope.element = step.element;
                                            if ($rootScope.userModel.new_user == 1)
                                            {
                                                $(".nxt").css("opacity", "0.5");
                                                $(".nxt").css("pointer-events", "none");
//                                            $(".skip").css("display", "none");
//                                            $(".tour-tour-element .hideMenu").css("display", "block");
                                                $($scope.element + ' ' + ".hideMenu").css("display", "block");
                                            }
                                            $("#login_Box_Div").show();
//                                            setTimeout(function () {
                                            if ($window.localStorage.getItem("paymentTermsAdded") == 'true')
                                            {
                                                $(".nxt").css("opacity", "1");
                                                $(".nxt").css("pointer-events", "auto");
                                                $window.localStorage.setItem("demo_guide", "true");
                                            }
//                                            }, 1000)
                                            $("#end_tour").click(function () {
//                                                $scope.updateOldUser();
                                                $rootScope.closeDemoPopup('end');
                                            });
                                            if ($window.localStorage.getItem("demo_guide") == 'false')
                                            {
                                                $(".popover").css("display", "none");
                                                $(".tour-tour-element .hideMenu").css("display", "none");
                                                $(".app-content-body").css("height", "auto");
                                                $(".app-aside").css("position", "fixed");
                                                $("#login_Box_Div").show();
                                            }
                                            $window.localStorage.setItem("demo_guide", "false");
                                            $window.localStorage.setItem("tour_current_step", "4");
                                            $rootScope.accountNext = true;
                                        },
                                        onPrev: function (tour) {
                                            var step = tour._options.steps[tour.getCurrentStep()];
                                            $scope.element = step.element;
                                            if ($window.localStorage.getItem("demo_guide") == 'false')
                                            {
                                                $(".popover").css("display", "block");
                                                $window.localStorage.setItem("demo_guide", "true");
                                            }
                                        },
                                        onNext: function (tour) {
                                            $window.localStorage.setItem("demo_guide", "true");
                                            $rootScope.accountNext = false;
                                            $(".dashboard_details").css("height", "2500px");
                                            $("#login_Box_Div3").show();
                                            $state.go('app.account');
                                        }
                                    },
                                    {
                                        path: '',
                                        element: '#tour_step7',
                                        title: LANG.tour_step7_title,
                                        content: LANG.tour_step7_content,
                                        template: "<div class='popover tour'><div class='arrows'></div><a ><img id='end_tour' style='width:7%;' class='close_First_Pop' src='../img/close icon.png'/></a><h3 class='popover-title text-bold'></h3><div class='popover-content'></div><ul class='paginer'><li>4</li><li >5</li><li class='selected_step'>6</li><li>7</li><li>...</li><li>9</li><li class='skip' style='float:right;text-decoration: underline;'><a id='end' data-role='end'>Skip</a></li></ul><div class='popover-navigation clear_b'><div class='col-sm-6 prev' data-role='prev'><button class='' style='border:0px;background:transparent;color:#000' >« " + LANG.prev + "</button></div><div class='col-sm-6 nxt' data-role='next'><button class='' style='border:0px;background:transparent;color: #fff;' id='next' >" + LANG.next + " »</button></div></div></div>",
                                        onShow: function (tour) {
                                            if (!$('#tour_step7').closest('li').hasClass('active')) {
                                                $('#tour_step7').trigger('click');
                                            }
                                            $(".app-aside").css("position", "absolute");
                                            $(".app-aside").css("z-index", "auto");
                                            $(".app-content-body").css("height", "2000px");
                                            $("#login_Box_Div3").show();
//                                            $rootScope.hideAlert = false;
                                        },
                                        onHide: function (tour) {
                                            $(".app-aside").css("position", "fixed");
                                            $(".app-aside").css("z-index", "97");
                                            $(".app-content-body").css("height", "auto");
                                            $(".tour-tour-element .hideMenu").css("display", "none");
                                            $window.localStorage.setItem("demo_guide", "true");
                                            $(".dashboard_details").css("height", "auto");
                                        },
                                        onShown: function () {
                                            var step = tour._options.steps[tour.getCurrentStep()];
                                            $scope.element = step.element;
                                            if ($rootScope.userModel.new_user == 1)
                                            {
                                                $(".nxt").css("opacity", "0.5");
                                                $(".nxt").css("pointer-events", "none");
//                                            $(".skip").css("display", "none");
//                                            $(".tour-tour-element .hideMenu").css("display", "block");
                                                $($scope.element + ' ' + ".hideMenu").css("display", "block");
                                            }
                                            $("#login_Box_Div3").show();
//                                            setTimeout(function () {
                                            if ($window.localStorage.getItem("accountAdded") == 'true')
                                            {
                                                $(".nxt").css("opacity", "1");
                                                $(".nxt").css("pointer-events", "auto");
                                                $window.localStorage.setItem("demo_guide", "true");
                                            }
//                                            }, 1000)

                                            $("#end_tour").click(function () {
                                                $rootScope.closeDemoPopup('end');
                                            });
                                            if ($window.localStorage.getItem("demo_guide") == 'false')
                                            {
                                                $(".popover").css("display", "none");
                                                $(".tour-tour-element .hideMenu").css("display", "none");
                                                $(".app-content-body").css("height", "auto");
                                                $(".app-aside").css("position", "fixed");
                                                $("#login_Box_Div3").show();
                                            }
                                            $window.localStorage.setItem("demo_guide", "false");
                                            $window.localStorage.setItem("tour_current_step", "5");
                                            $rootScope.purchaseInvoiceNext = true;
                                        },
                                        onPrev: function (tour) {
                                            var step = tour._options.steps[tour.getCurrentStep()];
                                            $scope.element = step.element;
                                            if ($window.localStorage.getItem("demo_guide") == 'false')
                                            {
                                                $(".popover").css("display", "block");
                                                $window.localStorage.setItem("demo_guide", "true");
                                            }
                                        },

                                        onNext: function (tour) {
                                            $window.localStorage.setItem("demo_guide", "true");
                                            $rootScope.purchaseInvoiceNext = false;
                                            $("#login_Box_Div2").show();
                                            $(".dashboard_details").css("height", "2500px");
                                            $state.go('app.purchaseInvoice');
                                        }
                                    },
                                    {
                                        path: '',
                                        element: '#tour_step8',
                                        title: LANG.tour_step8_title,
                                        content: LANG.tour_step8_content,
                                        template: "<div class='popover tour'><div class='arrows'></div><a ><img id='end_tour' style='width:7%;' class='close_First_Pop' src='../img/close icon.png'/></a><h3 class='popover-title text-bold'></h3><div class='popover-content'></div><ul class='paginer'><li>5</li><li>6</li><li  class='selected_step'>7</li><li>...</li><li>9</li><li class='skip' style='float:right;text-decoration: underline;'><a id='end' data-role='end'>Skip</a></li></ul><div class='popover-navigation clear_b'><div class='col-sm-6 prev' data-role='prev'><button class='' style='border:0px;background:transparent;color:#000' >« " + LANG.prev + "</button></div><div class='col-sm-6 nxt' data-role='next'><button class='' style='border:0px;background:transparent;color: #fff;' id='next' >" + LANG.next + " »</button></div></div></div>",
                                        onShow: function (tour) {
                                            if (!$('#tour_step7_menu').closest('li').hasClass('active')) {
                                                $('#tour_step2').trigger('click');
                                            }
                                            $(".app-aside").css("position", "absolute");
                                            $(".app-aside").css("z-index", "auto");
                                            $(".app-content-body").css("height", "2000px");
                                            $("#login_Box_Div2").show();
//                                            $rootScope.hideAlert = false;
                                        },
                                        onHide: function (tour) {
                                            $(".app-aside").css("position", "fixed");
                                            $(".app-aside").css("z-index", "97");
                                            $(".app-content-body").css("height", "auto");
                                            $(".tour-tour-element .hideMenu").css("display", "none");
                                            $window.localStorage.setItem("demo_guide", "true");
                                        },
                                        onShown: function () {
                                            var step = tour._options.steps[tour.getCurrentStep()];
                                            $scope.element = step.element;
                                            if ($rootScope.userModel.new_user == 1)
                                            {
//                                                $(".nxt").css("opacity", "0.5");
//                                                $(".nxt").css("pointer-events", "none");                                            
                                                $($scope.element + ' ' + ".hideMenu").css("display", "block");
                                            }
                                            $("#login_Box_Div2").show();

                                            $(".app-content-body").css("height", "2000px");
                                            $("#end_tour").click(function () {
                                                $rootScope.closeDemoPopup('end');
                                            });
                                            if ($window.localStorage.getItem("demo_guide") == 'false')
                                            {
                                                $(".popover").css("display", "none");
                                                $(".tour-tour-element .hideMenu").css("display", "none");
                                                $(".app-content-body").css("height", "auto");
                                                $(".app-aside").css("position", "fixed");
                                                $("#login_Box_Div2").show();
                                            }
                                            $window.localStorage.setItem("demo_guide", "false");
                                            $window.localStorage.setItem("tour_current_step", "6");
                                            $rootScope.employeeNext = true;
                                        },
                                        onPrev: function (tour) {
                                            var step = tour._options.steps[tour.getCurrentStep()];
                                            $scope.element = step.element;
                                            if ($window.localStorage.getItem("demo_guide") == 'false')
                                            {
                                                $(".popover").css("display", "block");
                                                $window.localStorage.setItem("demo_guide", "true");
                                            }
                                        },
                                        onNext: function (tour) {
                                            $window.localStorage.setItem("demo_guide", "true");
                                            $rootScope.employeeNext = false;
                                            $(".dashboard_details").css("height", "2500px");
                                            $("#login_Box_Div4").show();
                                            $state.go('app.employee');
                                        }

                                    },
                                    {
                                        path: '',
                                        element: '#tour_step9',
                                        title: LANG.tour_step9_title,
                                        content: LANG.tour_step9_content,
                                        template: "<div class='popover tour'><div class='arrows'></div><a ><img id='end_tour' style='width:7%;' class='close_First_Pop' src='../img/close icon.png'/></a><h3 class='popover-title text-bold'></h3><div class='popover-content'></div><ul class='paginer'><li>6</li><li>7</li><li class='selected_step'>8</li><li>9</li><li class='skip' style='float:right;text-decoration: underline;'><a data-role='end'>Skip</a></li></ul><div class='popover-navigation clear_b'><div class='col-sm-6 prev' data-role='prev'><button class='' style='border:0px;background:transparent;color:#000' >« " + LANG.prev + "</button></div><div class='col-sm-6 nxt'  data-role='next'><button class='' style='border:0px;background:transparent;color: #fff;' id='next'>" + LANG.next + " »</button></div></div></div>",
                                        onShow: function (tour) {
                                            if (!$('#tour_step9_menu').closest('li').hasClass('active')) {
                                                $('#tour_step9').trigger('click');
                                            }
                                            $(".app-aside").css("position", "absolute");
                                            $(".app-aside").css("z-index", "auto");
                                            $(".app-content-body").css("height", "2000px");
                                            $("#login_Box_Div4").show();
//                                            $rootScope.hideAlert = false;
                                        },
                                        onHide: function (tour) {
                                            $(".app-aside").css("position", "fixed");
                                            $(".app-aside").css("z-index", "97");
                                            $(".app-content-body").css("height", "auto");
                                            $(".tour-tour-element .hideMenu").css("display", "none");
                                            $window.localStorage.setItem("demo_guide", "true");
                                        },
                                        onShown: function () {
                                            var step = tour._options.steps[tour.getCurrentStep()];
                                            $scope.element = step.element;
                                            if ($rootScope.userModel.new_user == 1)
                                            {
//                                            $(".tour-tour-element .hideMenu").css("display", "block");
                                                $(".nxt").css("opacity", "0.5");
                                                $(".nxt").css("pointer-events", "none");
//                                            $(".skip").css("display", "none");
                                                $($scope.element + ' ' + ".hideMenu").css("display", "block");
                                            }
                                            $("#login_Box_Div4").show();
//                                            setTimeout(function () {
                                            if ($window.localStorage.getItem("employeeAdded") == 'true')
                                            {
                                                $(".nxt").css("opacity", "1");
                                                $(".nxt").css("pointer-events", "auto");
                                            }
//                                            }, 1000)
                                            $(".app-content-body").css("height", "2000px");

                                            $("#end_tour").click(function () {
//                                                $scope.updateOldUser();
                                                $rootScope.closeDemoPopup('end');
                                            });
                                            if ($window.localStorage.getItem("demo_guide") == 'false')
                                            {
                                                $(".popover").css("display", "none");
                                                $(".tour-tour-element .hideMenu").css("display", "none");
                                                $(".app-content-body").css("height", "auto");
                                                $(".app-aside").css("position", "fixed");
                                                $("#login_Box_Div4").show();
                                            }
                                            $window.localStorage.setItem("demo_guide", "false");
                                            $window.localStorage.setItem("tour_current_step", "7");
                                            $rootScope.invoiceNext = true;
                                        },
                                        onPrev: function (tour) {
                                            var step = tour._options.steps[tour.getCurrentStep()];
                                            $scope.element = step.element;
                                            if ($window.localStorage.getItem("demo_guide") == 'false')
                                            {
                                                $(".popover").css("display", "block");
                                                $window.localStorage.setItem("demo_guide", "true");
                                            }
                                        },
                                        onNext: function (tour) {
                                            $window.localStorage.setItem("demo_guide", "true");
                                            $rootScope.invoiceNext = false;
                                            $("#login_Box_Div1").show();
                                            $state.go('app.invoice');
                                        }
                                    },
                                    {
                                        path: '',
                                        element: '#tour_step10',
                                        title: LANG.tour_step10_title,
                                        content: LANG.tour_step10_content,
                                        template: "<div class='popover tour'><div class='arrows'></div><a ><img id='end_tour' style='width:7%;' class='close_First_Pop' src='../img/close icon.png'/></a><h3 class='popover-title text-bold'></h3><div class='popover-content'></div><ul class='paginer'><li>7</li><li >8</li><li class='selected_step'>9</li><li class='skip' class='skip' style='float:right;text-decoration: underline;'><a id='end' data-role='end'>Skip</a></li></ul><div class='popover-navigation clear_b'><div class='col-sm-6 prev'  data-role='prev'><button class='' style='border:0px;background:transparent;color:#000'>« " + LANG.prev + "</button></div><div class='col-sm-6 nxt'  data-role='next'><button class='' style='border:0px;background:transparent;color: #fff;' id='next'>" + LANG.next + " »</button></div></div></div>",
                                        onShow: function (tour) {
                                            if (!$('#tour_step10_menu').closest('li').hasClass('active')) {
                                                $('#tour_step10').trigger('click');
                                            }
                                            $(".app-aside").css("position", "absolute");
                                            $(".app-aside").css("z-index", "auto");
                                            $(".app-content-body").css("height", "2000px");
                                            $("#login_Box_Div1").show();
                                        },
                                        onHide: function (tour) {
                                            $(".app-aside").css("position", "fixed");
                                            $(".app-aside").css("z-index", "97");
                                            $(".app-content-body").css("height", "auto");
                                            $(".tour-tour-element .hideMenu").css("display", "none");
                                            $window.localStorage.setItem("demo_guide", "true");
                                        },
                                        onShown: function () {
                                            var step = tour._options.steps[tour.getCurrentStep()];
                                            $scope.element = step.element;
                                            if ($rootScope.userModel.new_user == 1)
                                            {
//                                                $(".nxt").css("opacity", "0.5");
//                                                $(".nxt").css("pointer-events", "none");                                           
                                                $($scope.element + ' ' + ".hideMenu").css("display", "block");
                                            }
                                            $("#login_Box_Div1").show();

                                            $(".app-content-body").css("height", "2000px");

                                            $("#end_tour").click(function () {
//                                                $scope.updateOldUser();
                                                $rootScope.closeDemoPopup('end');
                                            });
                                            if ($window.localStorage.getItem("demo_guide") == 'false')
                                            {
                                                $(".popover").css("display", "none");
                                                $(".tour-tour-element .hideMenu").css("display", "none");
                                                $(".app-content-body").css("height", "auto");
                                                $(".app-aside").css("position", "fixed");
                                                $("#login_Box_Div1").show();
                                            }
                                            $window.localStorage.setItem("demo_guide", "false");
                                            $window.localStorage.setItem("tour_current_step", "0");
                                            $rootScope.invoiceNext = true;
                                        },
                                        onPrev: function (tour) {
                                            var step = tour._options.steps[tour.getCurrentStep()];
                                            $scope.element = step.element;
                                            if ($window.localStorage.getItem("demo_guide") == 'false')
                                            {
                                                $(".popover").css("display", "block");
                                                $window.localStorage.setItem("demo_guide", "true");
                                            }
                                        },
                                        onNext: function (tour) {

                                            $window.localStorage.setItem("demo_guide", "true");
                                            $rootScope.invoiceNext = false;
                                            $rootScope.closeDemoPopup('end');
                                        }
                                    },
//                                    {
//                                        path: '',
//                                        element: '#tour_step11',
//                                        title: LANG.tour_step11_title,
//                                        content: LANG.tour_step11_content,
//                                        template: "<div class='popover tour'><div class='arrows'></div><a ><img id='end_tour' style='width:7%;' class='close_First_Pop' src='../img/close icon.png'/></a><h3 class='popover-title text-bold'></h3><div class='popover-content'></div><ul class='paginer'><li>7</li><li>8</li><li class='selected_step'>9</li><li class='skip' style='float:right;text-decoration: underline;width:18%;'><a id='end'>End Demo</a></li></ul><div class='popover-navigation clear_b'><div class='col-sm-6 prev'  data-role='prev'><button class='' style='border:0px;background:transparent;color:#000'>« " + LANG.prev + "</button></div><div class='col-sm-6 nxt' data-role='next'><button class='' style='border:0px;background:transparent;color: #fff;' id='next' >" + LANG.next + " »</button></div></div></div>",
//                                        onShow: function (tour) {
//                                            if (!$('#tour_step11_menu').closest('li').hasClass('active')) {
//                                                $('#tour_step11').trigger('click');
//                                            }
//                                            $(".app-aside").css("position", "absolute");
//                                            $(".app-aside").css("z-index", "auto");
//                                            $(".app-content-body").css("height", "2000px");
//                                            $("#login_Box_Div1").show();
////                                            $rootScope.hideAlert = false;
//                                        },
//                                        onHide: function (tour) {
//                                            $(".app-aside").css("position", "fixed");
//                                            $(".app-aside").css("z-index", "97");
//                                            $(".app-content-body").css("height", "auto");
//                                            $(".tour-tour-element .hideMenu").css("display", "none");
//                                            $window.localStorage.setItem("demo_guide", "true");
//                                        },
//                                        onShown: function () {
//                                            if ($rootScope.userModel.new_user == 1)
//                                            {
//                                                $(".tour-tour-element .hideMenu").css("display", "block");
//                                            }
//                                            $(".app-content-body").css("height", "2000px");
//                                            $("#end_tour").click(function () {
//                                                $scope.updateOldUser();
//                                            });
//                                            $("#end").click(function () {
////                                                $scope.updateOldUser();
//                                                $rootScope.closeDemoPopup('end');
//                                            });
//                                            if ($window.localStorage.getItem("demo_guide") == 'false')
//                                            {
//                                                $(".popover").css("display", "none");
//                                                $(".tour-tour-element .hideMenu").css("display", "none");
//                                                $(".app-content-body").css("height", "auto");
//                                                $(".app-aside").css("position", "fixed");
//                                                $("#login_Box_Div").show();
//                                            }
//                                            $window.localStorage.setItem("demo_guide", "false");
//                                            $window.localStorage.setItem("tour_current_step", "0");
//                                        },
//                                        onPrev: function (tour) {
//                                            var step = tour._options.steps[tour.getCurrentStep()];
//                                            $scope.element = step.element;
//                                            if ($window.localStorage.getItem("demo_guide") == 'false')
//                                            {
//                                                $(".popover").css("display", "block");
//                                                $window.localStorage.setItem("demo_guide", "true");
//                                            }
//                                        },
//                                        onNext: function (tour) {
//
//                                            $window.localStorage.setItem("demo_guide", "true");
//                                            $rootScope.invoiceNext = false;
//                                            $rootScope.closeDemoPopup('end');
//                                        }
//                                    }
                                ]);
//                            $('#start_tour').click(function () {
//                            $(".app-aside").css("position", "absolute");
                                $(".app-aside").css("position", "absolute");
                                $(".app-aside").css("z-index", "auto");
                                tour.start();
                                tour.init();
                                tour.restart();
//                            });

                                if ($('#start_tour').length > 0 && localStorage.getItem("upos_app_tour_shown") !== 'true') {
                                    $('#start_tour').trigger('click');
                                    localStorage.setItem("upos_app_tour_shown", 'true');
                                }
                                $window.localStorage.setItem("demo_guide", "true");
                                var step = tour._options.steps[tour.getCurrentStep()];
                                var element = $(step.element);
                                $scope.element = step.element;
//                            var step = tour.getStep(step - 1);
                            });
                        }
                    }
                }
                $scope.updateOldUser = function ()
                {
                    var getListParam = {};
                    getListParam.type = 2;
                    getListParam.user_id = $rootScope.userModel.id;
                    var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                    adminService.upateNewUser(getListParam, configOption).then(function (response)
                    {
                        if (response.data.success === true)
                        {
                            $window.location.reload();
                            $window.localStorage.setItem("demo_guide", "true");
//                            $rootScope.showHelpMenu = true;
                        }
                    });
                }
                $scope.updateNewUser = function ()
                {
                    var getListParam = {};
                    getListParam.type = 1;
                    getListParam.user_id = $rootScope.userModel.id;
                    var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                    adminService.upateNewUser(getListParam, configOption).then(function (response)
                    {
                        if (response.data.success === true)
                        {
                            $window.location.reload();
                            $window.localStorage.setItem("demo_guide", "true");
                            $window.localStorage.setItem("skip_demo_guide", 'false');
                            $rootScope.showHelpMenu = true;
                        }
                    });
                }

                $scope.updateUserType = function (userType) {
                    $scope.homeSearchUserType = userType;
                }

                function updateUserInfoEventHandler($event, data)
                {

                    if (data.success == true)
                    {
                        $rootScope.isLogined = true;
                        $rootScope.userModel = data.data;
                        $scope.selectDefaultLoginRole();
                        console.log("user");
                    } else
                    {
                        $rootScope.isLogined = false;
                        $rootScope.userModel = {
                            userName: '',
                            role: ['']
                        };
                    }
                    $rootScope.isUserModelUpdated = true;
                }
                $rootScope.notificationListAll = function ()
                {
                    var getListParam = {};
                    getListParam.user_id = $rootScope.userModel.id;
                    getListParam.start = 0;
                    getListParam.limit = 0;
                    adminService.getNotificationList(getListParam).then(function (response) {
                        if (response.data.success == true && response.data.list.length > 0)
                        {
                            $rootScope.userModel.notificationList = response.data.list;
                        }
                    });
                }
                $rootScope.getNotificationCount = function ()
                {
                    var getListParam = {};
                    getListParam.user_id = $rootScope.userModel.id;
                    getListParam.read_or_not = 0;
                    adminService.getNotificationCount(getListParam).then(function (response) {
                        if (response.data.success == true)
                        {
                            $rootScope.userModel.notificationcount = response.data.count;
                        }
                    });
                }
                $rootScope.shownotification = function ()
                {
                    $rootScope.notify = true;
                }
                $rootScope.redirectPage = function (url, id, read)
                {
                    $window.location.href = location.origin + '/public/admin/#/' + url;
                    if (read == 0)
                    {
                        $rootScope.notificationUpdate(id);
                    }
                }
                $rootScope.notificationUpdate = function (notificationId)
                {
                    adminService.updateNotification(notificationId).then(function (response) {
                        if (response.data.success == true)
                        {
                            console.log('success');
                        }
                    });
                }
                $scope.autoRefreshDelay = 1000 * 300;

                $scope.autoRefreshTimeoutPromise = null;
                /*
                 * Auto refersh Init
                 *
                 * @returns {undefined}
                 */
                $scope.autoRefreshInit = function ()
                {
                    if ($scope.autoRefreshTimeoutPromise != null)
                    {
                        $timeout.cancel($scope.autoRefreshTimeoutPromise);
                    }
                    $scope.autoRefreshTimeoutPromise = $timeout($rootScope.autoRefresh, $scope.autoRefreshDelay);
                };
                /*
                 * Cancel Auto Refersh
                 * @returns {undefined}
                 */
                $scope.cancelAutoRefresh = function ()
                {
                    if ($scope.autoRefreshTimeoutPromise != null)
                    {
                        $timeout.cancel($scope.autoRefreshTimeoutPromise);
                    }
                };
                $rootScope.autoRefresh = function ()
                {
                    $rootScope.getNotificationCount();
                    $scope.autoRefreshInit();
                };


                $scope.firstScreenCode = false;
                function updateScreenAccessInfoHandler($event, data)
                {
                    var screenAccessInfo = data;
                    var loop;
                    for (loop = 0; loop < screenAccessInfo.length; loop++)
                    {
                        var screenAccess = screenAccessInfo[loop];
                        var screencode = screenAccess.screen_code;
                        if ($rootScope.screenAccessDetail[screencode] != 'undefined')
                        {
                            //                            if ($rootScope.userModel.rolename == 'superadmin')
                            //                            {
                            //                                $rootScope.screenAccessDetail[screencode] = {};
                            //                                $rootScope.screenAccessDetail[screencode].is_create = true;
                            //                                $rootScope.screenAccessDetail[screencode].is_modify = true;
                            //                                $rootScope.screenAccessDetail[screencode].is_view = true;
                            //                                if ($scope.defaultState == '' && !$scope.firstScreenCode && $rootScope.screenAccessDetail[screencode].is_view)
                            //                                {
                            //                                    $scope.firstScreenCode = true;
                            //                                    $scope.defaultState = $rootScope.screenStateCode[screencode];
                            //                                }
                            //                                $rootScope.screenAccessDetail[screencode].is_delete = true;
                            //                            }
                            //                            else
                            //                            {
                            $rootScope.screenAccessDetail[screencode] = {};
                            $rootScope.screenAccessDetail[screencode].is_create = (screenAccessInfo[loop].is_create == 1 ? true : false);
                            $rootScope.screenAccessDetail[screencode].is_modify = (screenAccessInfo[loop].is_modify == 1 ? true : false);
                            $rootScope.screenAccessDetail[screencode].is_view = (screenAccessInfo[loop].is_view == 1 ? true : false);
                            if ($scope.defaultState == '' && !$scope.firstScreenCode && $rootScope.screenAccessDetail[screencode].is_view)
                            {
                                $scope.firstScreenCode = true;
                                $scope.defaultState = $rootScope.screenStateCode[screencode];
                            }
                            $rootScope.screenAccessDetail[screencode].is_delete = (screenAccessInfo[loop].is_delete == 1 ? true : false);

                            //                            }
                        }

                    }
                    $scope.isScreenDataLoaded = true;
                    var screenCode = ['purchaseestimate', 'purchaseinvoice', 'salesestimate', 'salesinvoice', 'salesorder', 'pos',
                        'product', 'service', 'uom', 'users', 'role', 'tax', 'taxgroup', 'incomecategory', 'expensecategory', 'attribute', 'appsettings', 'businesssettings', 'invoicesettings', 'smssettings',
                        'emailsettings', 'country', 'state', 'city', 'prefixsettings', 'account', 'accountbalance', 'transdeposit', 'transexpense', 'salesreport', 'itemsalesreport', 'receivablereport',
                        'payablereport', 'partysalesreport', 'partypurchasereport', 'purchasebaseinventory', 'partysalespurbased', 'taxreport', 'taxsummaryreport', 'accountstatement', 'discountedreport', 'partywisepurchasereport', 'salestocus', 'purfromcus', 'stockadjustment', 'stockwastage',
                        'minstock', 'inventorystock', 'stocktrace', 'databasebackup', 'activitylog', 'albumlist', 'customersettings', 'categorystockreport', 'categorywisereport', 'barcodewisestockreport',
                        'debitnote', 'creditnote', 'acodestatement', 'tag', 'catalogproduct', 'categoryCatlog', 'purchaseinvoicereturn', 'accountsummaryreport', 'accountdetailreport', 'itemwiseageingreport', 'categorywiseageingstockreport',
                        'stockwastagereport', 'stockadjustmentreport', 'categorystockadjustreport', 'categorystockwastagereport', 'suggestions', 'helpmanual', 'reportbug', 'hsncode', 'employee', 'designation', 'empsalesreport', 'deliverynote', 'deliverynotereport', 'daylogreport', 'vendor', 'purchasepayment'];

                    for (var i = 0; i < screenCode.length; i++)
                    {
                        if ($rootScope.screenAccessDetail[screenCode[i]] == 'undefined' || $rootScope.screenAccessDetail[screenCode[i]] == '' || $rootScope.screenAccessDetail[screenCode[i]] == null)
                        {
                            $rootScope.screenAccessDetail[screenCode[i]] = {};
                            $rootScope.screenAccessDetail[screenCode[i]].is_create = false;
                            $rootScope.screenAccessDetail[screenCode[i]].is_modify = false;
                            $rootScope.screenAccessDetail[screenCode[i]].is_view = false;
                        }
                    }
                    if ($rootScope.screenAccessDetail.vendor.is_view || $rootScope.screenAccessDetail.purchaseestimate.is_view || $rootScope.screenAccessDetail.purchaseinvoice.is_view || $rootScope.screenAccessDetail.purchaseinvoicereturn.is_view || $rootScope.screenAccessDetail.purchasepayment.is_view)
                    {
                        $rootScope.screenAccessDetail.purchaseheader.is_view = true;
                    }
                    if ($rootScope.screenAccessDetail.salesestimate.is_view || $rootScope.screenAccessDetail.salesinvoice.is_view || $rootScope.screenAccessDetail.salesorder.is_view
                            || $rootScope.screenAccessDetail.pos.is_view || $rootScope.screenAccessDetail.deliverynote.is_view)
                    {
                        $rootScope.screenAccessDetail.salesheader.is_view = true;
                    }
                    if ($rootScope.screenAccessDetail.product.is_view || $rootScope.screenAccessDetail.service.is_view || $rootScope.screenAccessDetail.uom.is_view)
                    {
                        $rootScope.screenAccessDetail.produtserviceheader.is_view = true;
                    }
                    if ($rootScope.screenAccessDetail.tag.is_view || $rootScope.screenAccessDetail.catalogproduct.is_view || $rootScope.screenAccessDetail.categoryCatlog.is_view)
                    {
                        $rootScope.screenAccessDetail.catalogheader.is_view = true;
                    }
                    if ($rootScope.screenAccessDetail.users.is_view || $rootScope.screenAccessDetail.role.is_view || $rootScope.screenAccessDetail.tax.is_view || $rootScope.screenAccessDetail.taxgroup.is_view
                            || $rootScope.screenAccessDetail.incomecategory.is_view || $rootScope.screenAccessDetail.expensecategory.is_view || $rootScope.screenAccessDetail.attribute.is_view
                            || $rootScope.screenAccessDetail.appsettings.is_view || $rootScope.screenAccessDetail.businesssettings.is_view || $rootScope.screenAccessDetail.invoicesettings.is_view
                            || $rootScope.screenAccessDetail.smssettings.is_view || $rootScope.screenAccessDetail.emailsettings.is_view || $rootScope.screenAccessDetail.country.is_view || $rootScope.screenAccessDetail.state.is_view || $rootScope.screenAccessDetail.city.is_view
                            || $rootScope.screenAccessDetail.prefixsettings.is_view || $rootScope.screenAccessDetail.customersettings.is_view || $rootScope.screenAccessDetail.suggestions.is_view || $rootScope.screenAccessDetail.helpmanual.is_view || $rootScope.screenAccessDetail.reportbug.is_view || $rootScope.screenAccessDetail.hsncode.is_view)
                    {
                        $rootScope.screenAccessDetail.settingsheader.is_view = true;
                    }
                    if ($rootScope.screenAccessDetail.account.is_view || $rootScope.screenAccessDetail.accountbalance.is_view)
                    {
                        $rootScope.screenAccessDetail.bankheader = true;
                    }
                    if ($rootScope.screenAccessDetail.transdeposit.is_view || $rootScope.screenAccessDetail.transexpense.is_view || $rootScope.screenAccessDetail.debitnote.is_view || $rootScope.screenAccessDetail.creditnote.is_view)
                    {
                        $rootScope.screenAccessDetail.transactionheader = true;
                    }
                    if ($rootScope.screenAccessDetail.salesreport.is_view || $rootScope.screenAccessDetail.itemsalesreport.is_view ||
                            $rootScope.screenAccessDetail.receivablereport.is_view || $rootScope.screenAccessDetail.payablereport.is_view ||
                            $rootScope.screenAccessDetail.partysalesreport.is_view || $rootScope.screenAccessDetail.partypurchasereport.is_view
                            || $rootScope.screenAccessDetail.taxreport.is_view || $rootScope.screenAccessDetail.taxsummaryreport.is_view
                            || $rootScope.screenAccessDetail.accountstatement.is_view || $rootScope.screenAccessDetail.discountedreport.is_view || $rootScope.screenAccessDetail.partywisepurchasereport.is_view
                            || $rootScope.screenAccessDetail.salestocus.is_view || $rootScope.screenAccessDetail.purfromcus.is_view
                            || $rootScope.screenAccessDetail.partysalespurbased.is_view || $rootScope.screenAccessDetail.purchasebaseinventory.is_view || $rootScope.screenAccessDetail.categorystockreport.is_view || $rootScope.screenAccessDetail.categorywisereport.is_view
                            || $rootScope.screenAccessDetail.barcodewisestockreport.is_view || $rootScope.screenAccessDetail.acodestatement.is_view || $rootScope.screenAccessDetail.accountsummaryreport.is_view || $rootScope.screenAccessDetail.accountdetailreport.is_view
                            || $rootScope.screenAccessDetail.itemwiseageingreport.is_view || $rootScope.screenAccessDetail.categorywiseageingstockreport.is_view || $rootScope.screenAccessDetail.stockadjustmentreport.is_view || $rootScope.screenAccessDetail.stockwastagereport.is_view
                            || $rootScope.screenAccessDetail.categorystockadjustreport.is_view || $rootScope.screenAccessDetail.categorystockwastagereport.is_view || $rootScope.screenAccessDetail.empsalesreport.is_view || $rootScope.screenAccessDetail.deliverynotereport.is_view || $rootScope.screenAccessDetail.daylogreport.is_view)
                    {
                        $rootScope.screenAccessDetail.reportheader = true;
                    }
                    if ($rootScope.screenAccessDetail.stockadjustment.is_view || $rootScope.screenAccessDetail.stockwastage.is_view ||
                            $rootScope.screenAccessDetail.minstock.is_view || $rootScope.screenAccessDetail.inventorystock.is_view
                            || $rootScope.screenAccessDetail.stocktrace.is_view)
                    {
                        $rootScope.screenAccessDetail.inventoryheader = true;
                    }
                    if ($rootScope.screenAccessDetail.databasebackup.is_view || $rootScope.screenAccessDetail.activitylog.is_view)
                    {
                        $rootScope.screenAccessDetail.utilityheader = true;
                    }
                    if ($rootScope.screenAccessDetail.albumlist.is_view)
                    {
                        $rootScope.screenAccessDetail.photographyheader = true;
                    }
                    if ($rootScope.screenAccessDetail.stage.is_view || $rootScope.screenAccessDetail.deal.is_view)
                    {
                        $rootScope.screenAccessDetail.dealheader = true;
                    }
                    if ($rootScope.screenAccessDetail.employee.is_view || $rootScope.screenAccessDetail.designation.is_view)
                    {
                        $rootScope.screenAccessDetail.humanresourceheader.is_view = true;
                    }

                }
                $rootScope.displayUrl = '';
                function updateBusinessListInfoHandler($event, data)
                {
                    var businessList = data;
                    var sso_token = $cookies.get("sso-token");
                    for (var loop = 0; loop < businessList.length; loop++)
                    {
                        var url = businessList[loop].bu_name;
                        businessList[loop]['url'] = "http://" + businessList[loop].bu_name + '/public/authorization.php?sso-token=' + sso_token;
                    }
                    $rootScope.userModel.business_list = businessList;
                    for (var loop = 0; loop < businessList.length; loop++)
                    {
                        var url = businessList[loop].bu_name;
                        if (window.location.host == url)
                        {
                            businessList[loop].hide = true;
                            $rootScope.displayUrl = businessList[loop].bu_name;
                        } else
                        {
                            businessList[loop].hide = false;
                        }
                    }
                    $rootScope.userModel.business_list = businessList;
                }

                function updateAppSettingHandler($event, data)
                {
                    $rootScope.appConfig.cityInfo = {};
                    $rootScope.appConfig.stateInfo = {};
                    $rootScope.appConfig.countryInfo = {};
                    var loop;
                    if (data.total > 0)
                    {
                        for (loop = 0; loop < data.total; loop++)
                        {
                            if (data.list[loop].setting == "date_format")
                            {
                                $rootScope.appConfig.date_format = data.list[loop].value;

                            } else if (data.list[loop].setting == "currency")
                            {
                                $rootScope.appConfig.currency = data.list[loop].value;
                            } else if (data.list[loop].setting == "thousand_seperator")
                            {
                                $rootScope.appConfig.thousand_seperator = data.list[loop].value;
                            } else if (data.list[loop].setting == "invoice_item_uom")
                            {
                                $rootScope.appConfig.uomdisplay = data.list[loop].value == 1 ? true : false;
                            } else if (data.list[loop].setting == "invoice_item_uom_label")
                            {
                                $rootScope.appConfig.uomlabel = (data.list[loop].value == null || data.list[loop].value == '') ? 'Measure' : data.list[loop].value;
                            } else if (data.list[loop].setting == "invoice_item_custom1")
                            {
                                $rootScope.appConfig.invoice_item_custom1 = data.list[loop].value == 1 ? true : false;
                            } else if (data.list[loop].setting == "invoice_item_custom2")
                            {
                                $rootScope.appConfig.invoice_item_custom2 = data.list[loop].value == 1 ? true : false;
                            } else if (data.list[loop].setting == "invoice_item_custom3")
                            {
                                $rootScope.appConfig.invoice_item_custom3 = data.list[loop].value == 1 ? true : false;
                            } else if (data.list[loop].setting == "invoice_item_custom4")
                            {
                                $rootScope.appConfig.invoice_item_custom4 = data.list[loop].value == 1 ? true : false;
                            } else if (data.list[loop].setting == "invoice_item_custom5")
                            {
                                $rootScope.appConfig.invoice_item_custom5 = data.list[loop].value == 1 ? true : false;
                            } else if (data.list[loop].setting == "invoice_item_custom1_label")
                            {
                                $rootScope.appConfig.invoice_item_custom1_label = (data.list[loop].value == null || data.list[loop].value == '') ? 'Option 1' : data.list[loop].value;
                            } else if (data.list[loop].setting == "invoice_item_custom2_label")
                            {
                                $rootScope.appConfig.invoice_item_custom2_label = (data.list[loop].value == null || data.list[loop].value == '') ? 'Option 2' : data.list[loop].value;
                            } else if (data.list[loop].setting == "invoice_item_custom3_label")
                            {
                                $rootScope.appConfig.invoice_item_custom3_label = (data.list[loop].value == null || data.list[loop].value == '') ? 'Option 3' : data.list[loop].value;
                            } else if (data.list[loop].setting == "invoice_item_custom4_label")
                            {
                                $rootScope.appConfig.invoice_item_custom4_label = (data.list[loop].value == null || data.list[loop].value == '') ? 'Option 4' : data.list[loop].value;
                            } else if (data.list[loop].setting == "invoice_item_custom5_label")
                            {
                                $rootScope.appConfig.invoice_item_custom5_label = (data.list[loop].value == null || data.list[loop].value == '') ? 'Option 5' : data.list[loop].value;
                            } else if (data.list[loop].setting == "purchase_invoice_item_custom1")
                            {
                                $rootScope.appConfig.purchase_invoice_item_custom1 = data.list[loop].value == 1 ? true : false;
                            } else if (data.list[loop].setting == "purchase_invoice_item_custom2")
                            {
                                $rootScope.appConfig.purchase_invoice_item_custom2 = data.list[loop].value == 1 ? true : false;
                            } else if (data.list[loop].setting == "purchase_invoice_item_custom3")
                            {
                                $rootScope.appConfig.purchase_invoice_item_custom3 = data.list[loop].value == 1 ? true : false;
                            } else if (data.list[loop].setting == "purchase_invoice_item_custom4")
                            {
                                $rootScope.appConfig.purchase_invoice_item_custom4 = data.list[loop].value == 1 ? true : false;
                            } else if (data.list[loop].setting == "purchase_invoice_item_custom5")
                            {
                                $rootScope.appConfig.purchase_invoice_item_custom5 = data.list[loop].value == 1 ? true : false;
                            } else if (data.list[loop].setting == "purchase_invoice_item_custom1_label")
                            {
                                $rootScope.appConfig.purchase_invoice_item_custom1_label = (data.list[loop].value == null || data.list[loop].value == '') ? 'Option 1' : data.list[loop].value;
                            } else if (data.list[loop].setting == "purchase_invoice_item_custom2_label")
                            {
                                $rootScope.appConfig.purchase_invoice_item_custom2_label = (data.list[loop].value == null || data.list[loop].value == '') ? 'Option 2' : data.list[loop].value;
                            } else if (data.list[loop].setting == "purchase_invoice_item_custom3_label")
                            {
                                $rootScope.appConfig.purchase_invoice_item_custom3_label = (data.list[loop].value == null || data.list[loop].value == '') ? 'Option 3' : data.list[loop].value;
                            } else if (data.list[loop].setting == "purchase_invoice_item_custom4_label")
                            {
                                $rootScope.appConfig.purchase_invoice_item_custom4_label = (data.list[loop].value == null || data.list[loop].value == '') ? 'Option 4' : data.list[loop].value;
                            } else if (data.list[loop].setting == "purchase_invoice_item_custom5_label")
                            {
                                $rootScope.appConfig.purchase_invoice_item_custom5_label = (data.list[loop].value == null || data.list[loop].value == '') ? 'Option 5' : data.list[loop].value;
                            } else if (data.list[loop].setting == "purchase_uom")
                            {
                                $rootScope.appConfig.purchase_uom = data.list[loop].value == 1 ? true : false;
                            } else if (data.list[loop].setting == "purchase_uom_label")
                            {
                                $rootScope.appConfig.purchase_uom_label = (data.list[loop].value == null || data.list[loop].value == '') ? 'Measure' : data.list[loop].value;
                            } else if (data.list[loop].setting == "app_tile")
                            {
                                $rootScope.appConfig.app_tile = data.list[loop].value;
                            } else if (data.list[loop].setting == "invoice_prefix")
                            {
                                $rootScope.appConfig.invoice_prefix = data.list[loop].value;
                            } else if (data.list[loop].setting == "signature")
                            {
                                $rootScope.appConfig.signature = data.list[loop].value;
                            } else if (data.list[loop].setting == "sms_count")
                            {
                                $rootScope.appConfig.sms_count = data.list[loop].value;
                            } else if (data.list[loop].setting == "poscusid")
                            {
                                $rootScope.appConfig.poscusid = data.list[loop].value;
                            } else if (data.list[loop].setting == "pay_to_address")
                            {
                                $rootScope.appConfig.pay_to_address = data.list[loop].value;
                            } else if (data.list[loop].setting == "bank_detail")
                            {
                                $rootScope.appConfig.bank_detail = data.list[loop].value;
                            } else if (data.list[loop].setting == "invoicefrom")
                            {
                                $rootScope.appConfig.invoicefrom = data.list[loop].value;
                            } else if (data.list[loop].setting == "invoicecc")
                            {
                                $rootScope.appConfig.invoicecc = data.list[loop].value;
                            } else if (data.list[loop].setting == "invoice_print_template")
                            {
                                $rootScope.appConfig.invoice_print_template = data.list[loop].value;
                            } else if (data.list[loop].setting == "dual_language_support")
                            {
                                $rootScope.appConfig.dual_language_support = data.list[loop].value == 1 ? true : false;
                            } else if (data.list[loop].setting == "dual_language_label")
                            {
                                $rootScope.appConfig.dual_language_label = data.list[loop].value;
                            } else if (data.list[loop].setting == "pos_customer_id")
                            {
                                $rootScope.appConfig.pos_customer_id = data.list[loop].value;
                            } else if (data.list[loop].setting == "pos_bank_account_id")
                            {
                                $rootScope.appConfig.pos_bank_account_id = data.list[loop].value;
                            } else if (data.list[loop].setting == "sales_product_list")
                            {
                                $rootScope.appConfig.sales_product_list = data.list[loop].value;
                            } else if (data.list[loop].setting == "customer_special_date1")
                            {
                                $rootScope.appConfig.customer_special_date1 = data.list[loop].value == 1 ? true : false;
                            } else if (data.list[loop].setting == "customer_special_date2")
                            {
                                $rootScope.appConfig.customer_special_date2 = data.list[loop].value == 1 ? true : false;
                            } else if (data.list[loop].setting == "customer_special_date3")
                            {
                                $rootScope.appConfig.customer_special_date3 = data.list[loop].value == 1 ? true : false;
                            } else if (data.list[loop].setting == "customer_special_date4")
                            {
                                $rootScope.appConfig.customer_special_date4 = data.list[loop].value == 1 ? true : false;
                            } else if (data.list[loop].setting == "customer_special_date5")
                            {
                                $rootScope.appConfig.customer_special_date5 = data.list[loop].value == 1 ? true : false;
                            } else if (data.list[loop].setting == "customer_special_date1_label")
                            {
                                $rootScope.appConfig.customer_special_date1_label = (data.list[loop].value == null || data.list[loop].value == '') ? 'Option 1' : data.list[loop].value;
                            } else if (data.list[loop].setting == "customer_special_date2_label")
                            {
                                $rootScope.appConfig.customer_special_date2_label = (data.list[loop].value == null || data.list[loop].value == '') ? 'Option 2' : data.list[loop].value;
                            } else if (data.list[loop].setting == "customer_special_date3_label")
                            {
                                $rootScope.appConfig.customer_special_date3_label = (data.list[loop].value == null || data.list[loop].value == '') ? 'Option 3' : data.list[loop].value;
                            } else if (data.list[loop].setting == "customer_special_date4_label")
                            {
                                $rootScope.appConfig.customer_special_date4_label = (data.list[loop].value == null || data.list[loop].value == '') ? 'Option 4' : data.list[loop].value;
                            } else if (data.list[loop].setting == "customer_special_date5_label")
                            {
                                $rootScope.appConfig.customer_special_date5_label = (data.list[loop].value == null || data.list[loop].value == '') ? 'Option 5' : data.list[loop].value;
                            } else if (data.list[loop].setting == "qty_decimal_format")
                            {
                                $rootScope.appConfig.qty_format = data.list[loop].value;
                            } else if (data.list[loop].setting == "qty_decimal_format")
                            {
                                $rootScope.appConfig.qty_format = data.list[loop].value;
                            } else if (data.list[loop].setting == "gst_auto_suggest")
                            {
                                $rootScope.appConfig.gst_auto_suggest = data.list[loop].value;
                            } else if (data.list[loop].setting == "gst_state")
                            {
                                $rootScope.appConfig.gst_state = data.list[loop].value;
                            } else if (data.list[loop].setting == "gst_state_name")
                            {
                                $rootScope.appConfig.gst_state_name = data.list[loop].value;
                            } else if (data.list[loop].setting == "customer_gst_no")
                            {
                                $rootScope.appConfig.customer_gst_no = data.list[loop].value;
                            } else if (data.list[loop].setting == "companyname_print")
                            {
                                $rootScope.appConfig.companynameinprint = data.list[loop].value;
                            }
                            else if (data.list[loop].setting == "customer_city_name")
                            {
                                $rootScope.appConfig.cityInfo.name = data.list[loop].value;
                            }
                            else if (data.list[loop].setting == "customer_city_id")
                            {
                                $rootScope.appConfig.cityInfo.id = data.list[loop].value;
                            }
                            else if (data.list[loop].setting == "customer_state_name")
                            {
                                $rootScope.appConfig.stateInfo.name = data.list[loop].value;
                            }
                            else if (data.list[loop].setting == "customer_state_id")
                            {
                                $rootScope.appConfig.stateInfo.id = data.list[loop].value;
                            }
                            else if (data.list[loop].setting == "customer_country_name")
                            {
                                $rootScope.appConfig.countryInfo.name = data.list[loop].value;
                            }
                            else if (data.list[loop].setting == "customer_country_id")
                            {
                                $rootScope.appConfig.countryInfo.id = data.list[loop].value;
                            }
                            else if (data.list[loop].setting == "customer_phone_no")
                            {
                                $rootScope.appConfig.customer_phone_no = data.list[loop].value;
                            }
                            else if (data.list[loop].setting == "customer_mobile_no")
                            {
                                $rootScope.appConfig.customer_mobile_no = data.list[loop].value;
                            }
                            else if (data.list[loop].setting == "customer_email")
                            {
                                $rootScope.appConfig.customer_email = data.list[loop].value;
                            }
                            else if (data.list[loop].setting == "customer_website")
                            {
                                $rootScope.appConfig.customer_website = data.list[loop].value;
                            }
                        }
                        adminService.appConfig = $rootScope.appConfig;
                    }
                }

                function genericHttpErrorHandler($event, response)
                {
                    if (response.status == "404")
                    {
                        $scope.setErrorMessage("Request Not Acceptable.");
                    } else if (response.status == "406")
                    {
                        $scope.setErrorMessage("Request Not Acceptable.");
                    } else if (response.status == "500")
                    {
                        $scope.setErrorMessage("Internal Server Error.");
                    } else if (response.status == "503")
                    {
                        $scope.setErrorMessage("Service Unavailable");
                    } else if (response.status != "-1")
                    {
                        $scope.setErrorMessage(response.statusText + " (" + response.status + ")");
                    }


                }

                function genericHttpResponseHandler($event, response)
                {
                    if (typeof response.data != 'undefined')
                    {
                        if (typeof response.data.success != 'undefined')
                        {
                            if (response.data.success == true)
                            {
                                $scope.setSuccessMessage(response.data.message);
                            } else
                            {
                                $scope.setErrorMessage(response.data.message);
                            }

                        }
                    }
                }

                function genericErrorHandler($event, response)
                {
                    $scope.setErrorMessage(response);
                }
                function genericSuccessHandler($event, response)
                {
                    $scope.setSuccessMessage(response);
                }

                $scope.setCookie = function (key, value) {

                    var cookieOption = {
                        path: "/"
                    };
                    $cookies.put(key, value, cookieOption);
                };
                $scope.cookieAccept = function () {

                    $scope.setCookie('cookie-acception', true);
                    $scope.cookieAcception = typeof $cookies.get('cookie-acception') == 'undefined' ? false : true;
                };
                $scope.altSearch = function (userType) {

                    $scope.homeSearchUserType = userType;
                    $scope.search();
                }

                $scope.openPopup = function (value)
                {
                    if (value === 'register')
                    {
                        $scope.signupRedirectPopup = true;
                    } else if (value === 'signin')
                    {
                        $scope.signinRedirectPopup = true;
                    } else if (value === 'role')
                    {
                        if ($scope.app.roleSelectPopup)
                        {
                            $scope.app.roleSelectPopup = false;
                        } else
                        {
                            $scope.app.roleSelectPopup = true;
                        }
                    }
                }

                $scope.closePopup = function (value)
                {
                    if (value === 'register')
                    {
                        $scope.signupRedirectPopup = false;
                    } else if (value === 'signin')
                    {
                        $scope.signinRedirectPopup = false;
                    } else if (value === 'role')
                    {
                        $scope.app.roleSelectPopup = false;
                    }

                }

                $scope.selectDefaultLoginRole = function (value)
                {
                    $rootScope.app.selectedRole = $rootScope.userModel.rolename;
                    $rootScope.app.selectedRoleIndex = -1;
                    $rootScope.app.selectedClinicId = '';
                    $rootScope.app.selectedClinicName = '';
                    $scope.closePopup('role');
                }
                $scope.companyChangeHandler = function (index)
                {
                    if (index < $rootScope.userModel.adminCompId.length)
                    {
                        $rootScope.app.selectedRoleIndex = index;
                        $rootScope.app.selectedRole = APP_CONST.ROLE.ADMIN;
                        $rootScope.app.selectedClinicId = $rootScope.userModel.adminCompId[index].id;
                        $rootScope.app.selectedClinicName = $rootScope.userModel.adminCompId[index].name;
                    }

                    $scope.closePopup('role');
                }
                $scope.initUpdateDetailTimeoutPromise = null;
                $scope.initUpdateDetail = function ()
                {

                    if ($scope.initUpdateDetailTimeoutPromise != null)
                    {
                        $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
                    }
                    if ($scope.isScreenDataLoaded)
                    {
                        if ($state.current.name == 'app')
                        {
                            $state.go($scope.defaultState);
                        }
                    } else
                    {
                        $scope.initUpdateDetailTimeoutPromise = $timeout($scope.initUpdateDetail, 300);
                    }
                }

                $scope.$on('$viewContentLoaded', function () {
                    console.log('page load completed');
                    $scope.initUpdateDetail();
                });

            }])
        .factory('Auth', function ($http, $cookies, $location, $window, $rootScope, APP_CONST) {

            var factory = {};
            //factory.USER_INFO_SYNC_API ="/user/userInfo";

            factory.userModel = null;
            factory.isLogined = function ()
            {
                if ($rootScope.isUserModelUpdated)
                    return $rootScope.isLogined;
                return false;
            }
            factory.checkCookie = function ()
            {
                if (typeof $cookies.get("api-token") == 'undefined')
                {
                    $rootScope.isLogined = false;
                    $rootScope.actionMsgError = "Unauthorized";
                    $rootScope.cookieDisconnectError = "Already You loggedoff";

                }
            }
            factory.getUserInfo = function () {

                var cookieData = null;
                if (typeof $cookies.get("api-token") != 'undefined')
                {
                    cookieData = $cookies.get("api-token");
                } else
                {
                    //$window.location.href = $window.location.protocol + "//" + $window.location.host + $rootScope.CONTEXT_ROOT + "/login";
                    $window.location.href = APP_CONST.SSO_LOGIN_URL;
                }
                return $http({
                    method: 'GET',
                    url: APP_CONST.USER_INFO_SYNC_API,
                    headers: {
                        'api-token': cookieData
                    }

                }).success(function (data, status, headers, config) {

                    if (typeof data.data != 'undefined')
                        factory.userModel = data.data;
                    $rootScope.$broadcast('updateUserInfoEvent', data);
                    return $http({
                        method: 'GET',
                        url: APP_CONST.APP_SETTINGS_LIST,
                        headers: {
                            'api-token': cookieData
                        }
                    }).success(function (data, status, headers, config) {
                        if (typeof data.list != 'undefined')
                            $rootScope.$broadcast('updateAppSettings', data);


                        $http({
                            method: 'GET',
                            url: APP_CONST.API.USER_ACCESS_INFO,
                            params: {
                                role_id: $rootScope.userModel.role_id
                            },
                            headers: {
                                'api-token': cookieData
                            }
                        }).success(function (data, status, headers, config) {

                            if (typeof data.list != 'undefined')
                                $rootScope.$broadcast('updateScreenAccessInfoEvent', data.list);
                        }).error(function (data, status, headers, config) {

                        });
                        if (factory.userModel.new_user == 1 && $window.localStorage.getItem("demo_guide") == "true")
                        {
                            $window.localStorage.setItem("demo_guide", 'true');
                            $window.localStorage.setItem("skip_demo_guide", 'false');
                            $rootScope.showHelpMenu = true;
                        }
                        //Get busineess List 
                        $http({
                            method: 'GET',
                            url: APP_CONST.API.GET_USER_BUSINESS_LIST,
                            params: {},
                            headers: {
                                'api-token': cookieData
                            }
                        }).success(function (data, status, headers, config) {

                            if (typeof data.list != 'undefined')
                                $rootScope.$broadcast('updateBusinessListInfoEvent', data.list);
                            $rootScope.notificationListAll();
                            $rootScope.autoRefresh();
                        }).error(function (data, status, headers, config) {

                        });
                    }).error(function (data, status, headers, config) {

                    });
                }).error(function (data, status, headers, config) {

                });
            }

            factory.loginAsync = function ()
            {

                if (!factory.isLogined())
                {
                    var landingUrl = $window.location.protocol + "//" + $window.location.host + "/app/login/#/login-customer" + "?refer=" + encodeURIComponent($location.absUrl());
                    $window.location.href = landingUrl;
                }
            }

            factory.setUserModel = function (userData, referUrl)
            {
                if (userData != null)
                {
                    var landingUrl = $window.location.protocol + "//" + $window.location.host + $rootScope.CONTEXT_ROOT;
                } else
                {
                    var landingUrl = $window.location.protocol + "//" + $window.location.host + $rootScope.CONTEXT_ROOT + "/login";
                }
                var previousRole = "";
                if (factory.userModel != null)
                {
                    previousRole = factory.userModel.userType;
                }
                factory.userModel = userData;
                if (referUrl != "")
                {
                    landingUrl = decodeURIComponent(referUrl);
                }
                if (referUrl == "")
                {
                    if (factory.userModel != null)
                    {
                        var userRole = factory.userModel.role;
                        if (userRole[0] === APP_CONST.ROLE.STAFF)
                        {
                            landingUrl += APP_CONST.REDIRECT.STAFF_LOGIN_SUCCESS;
                        } else if (userRole[0] === APP_CONST.ROLE.APP_MANAGER)
                        {
                            landingUrl += APP_CONST.REDIRECT.APP_MANAGER_LOGIN_SUCCESS;
                        } else if (userRole[0] === APP_CONST.ROLE.ROOT_ADMIN)
                        {
                            landingUrl += APP_CONST.REDIRECT.ROOT_ADMIN_LOGIN_SUCCESS;
                        } else if (userRole[0] == "-1")
                        {
                            landingUrl += APP_CONST.REDIRECT.UNMAPPED_ROLE_REDIRECT;
                        }
                    }
                }

                $window.location.href = landingUrl;
            };
            factory.validatePageAccess = function () {
                var landingUrl = $window.location.protocol + "//" + $window.location.host;
                if ($rootScope.app.selectedRole === APP_CONST.ROLE.STAFF)
                {
                    landingUrl += APP_CONST.REDIRECT.STAFF_LOGIN_SUCCESS;
                } else if ($rootScope.app.selectedRole === APP_CONST.ROLE.APP_MANAGER)
                {
                    landingUrl += APP_CONST.REDIRECT.APP_MANAGER_LOGIN_SUCCESS;
                } else if ($rootScope.app.selectedRole === APP_CONST.ROLE.ROOT_ADMIN)
                {
                    landingUrl += APP_CONST.REDIRECT.ROOT_ADMIN_LOGIN_SUCCESS;
                } else if ($rootScope.app.selectedRole == "-1")
                {
                    landingUrl += APP_CONST.REDIRECT.UNMAPPED_ROLE_REDIRECT;
                }
                $window.location.href = landingUrl;
            };
            return factory;
        })
        .factory('StutzenHttpInterceptor', ['$q', '$injector', '$rootScope', '$window', '$location', function ($q, $injector, $rootScope, $window, $location) {
                var stutzenHttpInterceptor = {};
                stutzenHttpInterceptor.responseError = function (response) {

                    $rootScope.$broadcast('httpGenericErrorEvent', response);
                    return $q.reject(response);
                }

                stutzenHttpInterceptor.response = function (response) {


                    //Global Redirect for session out
                    var responseData = "" + response.data;
                    var searchKey = 'swf/login_check';
                    if (responseData.indexOf(searchKey) != -1)
                    {
                        $window.location.href = $window.location.protocol + "//" + $window.location.host + "/login";
                    }

                    if (typeof response.config != 'undefined' && typeof response.config.config != 'undefined')
                    {
                        var config = response.config.config;
                        if (config.handleResponse == true)
                        {
                            var hasSuccessResponse = false;
                            var hasFailureResponse = false;
                            if (typeof response.data != 'undefined')
                            {
                                if (typeof response.data.success != 'undefined')
                                {
                                    if (response.data.success == true)
                                    {
                                        hasSuccessResponse = true;
                                    } else
                                    {
                                        hasFailureResponse = true;
                                    }

                                }
                            }


                            if (config.has_success_alert != undefined && config.has_error_alert != undefined)
                            {
                                if (config.has_success_alert && hasSuccessResponse)
                                {
                                    $rootScope.$broadcast('httpGenericResponseSuccessEvent', response);
                                } else if (config.has_error_alert && hasFailureResponse)
                                {
                                    $rootScope.$broadcast('httpGenericResponseSuccessEvent', response);
                                }
                            } else
                            {
                                $rootScope.$broadcast('httpGenericResponseSuccessEvent', response);
                            }
                        }

                    }
                    return response;
                }



                return stutzenHttpInterceptor;
            }])

        .factory('StutzenHttpService', ['$http', function ($http) {
                var stutzenHttpService = {};
                stutzenHttpService.get = function (url, data, handleResponse, cookieData) {
                    if (typeof handleResponse == 'undefined') {
                        handleResponse = false;
                    }
                    var configOption = {};
                    configOption.handleResponse = handleResponse;
                    return $http({
                        method: 'GET',
                        url: url,
                        params: data,
                        headers: {
                            'api-token': cookieData
                        },
                        config: configOption
                    }).success(function (data, status, headers, config) {
                    }).error(function (data, status, headers, config) {

                    });
                }

                stutzenHttpService.post = function (url, data, handleResponse) {
                    if (typeof handleResponse == 'undefined') {
                        handleResponse = false;
                    }
                    var configOption = {};
                    configOption.handleResponse = handleResponse;
                    return $http({
                        method: 'POST',
                        url: url,
                        data: angular.toJson(data, 0),
                        config: configOption
                    }).success(function (data, status, headers, config) {
                    }).error(function (data, status, headers, config) {

                    });
                }
                stutzenHttpService.formPost = function (url, data, handleResponse) {
                    if (typeof handleResponse == 'undefined') {
                        handleResponse = false;
                    }
                    var configOption = {};
                    configOption.handleResponse = handleResponse;
                    return $http({
                        method: 'POST',
                        url: url,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded',
                            'handleResponse': handleResponse
                        },
                        transformRequest: function (obj) {
                            var str = [];
                            for (var p in obj)
                                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                            return str.join("&");
                        },
                        data: data,
                        config: configOption

                    }).success(function (data, status, headers, config) {
                    }).error(function (data, status, headers, config) {

                    });
                }
                return stutzenHttpService;
            }])
        .factory('MapLoaderService', function ($timeout) {

            var mapLoaderService = {};
            mapLoaderService.map = null;
            mapLoaderService.markerList = [];
            mapLoaderService.init = function (domId, centerPoint, markers) {

                var latValue = 51.1464659;
                var lngValue = 0.875019;
                if (centerPoint != null)
                {
                    latValue = centerPoint.lagtitude;
                    lngValue = centerPoint.longitude;
                } else if (markers != null && markers.length)
                {
                    latValue = markers[0].lagtitude;
                    lngValue = markers[0].longitude;
                }

                mapLoaderService.map = new google.maps.Map(document.getElementById(domId), {
                    zoom: 10,
                    center: {
                        lat: latValue,
                        lng: lngValue
                    }
                });
                $timeout(function () {
                    mapLoaderService.loadMarkerPoint(markers);
                }, 500);
            }
            mapLoaderService.loadMarkerPoint = function (markers) {

                if (markers != null)
                {
                    for (var i = 0; i < markers.length; i++) {
                        var position = new google.maps.LatLng(markers[i]['lagtitude'], markers[i]['longitude']);
                        //bounds.extend(position);
                        var marker = new google.maps.Marker({
                            position: position,
                            map: mapLoaderService.map,
                            title: markers[i]['name']
                        });
                        mapLoaderService.markerList.push(marker);
                    }
                }

            }
            mapLoaderService.clearMarker = function () {

                if (mapLoaderService.markerList == null)
                    return;
                for (var i = 0; i < mapLoaderService.markerList.length; i++) {

                    mapLoaderService.markerList[i].setMap(null);
                }
            }

            mapLoaderService.createDragableMarkerPoint = function (markers) {

                mapLoaderService.clearMarker();
                var latValue = 51.1464659;
                var lngValue = 0.875019;
                if (markers != null)
                {
                    latValue = markers['lagtitude'];
                    lngValue = markers['longitude'];
                }
                var position = new google.maps.LatLng(latValue, lngValue);
                //bounds.extend(position);
                var marker = new google.maps.Marker({
                    position: position,
                    draggable: true,
                    map: mapLoaderService.map

                });
                mapLoaderService.map.panTo(marker.getPosition());
                google.maps.event.addListener(marker, 'dragend', function (event) {
                    //$('#position').val('* '+this.getPosition().lat()+','+this.getPosition().lng());
                    //saveData(map,event);
                });
                mapLoaderService.markerList.push(marker);
            }


            return mapLoaderService;
        })
        .config(function () {
            var styleEl = document.createElement('style'),
                    styleSheet,
                    rippleCSS,
                    rippleLightCSS,
                    rippleKeyframes,
                    rippleWebkitKeyframes;
            rippleCSS = [
                '-webkit-animation: ripple 800ms ease-out;',
                'animation: ripple 800ms ease-out;',
                'background-color: rgba(0, 0, 0, 0.16);',
                'border-radius: 100%;',
                'height: 10px;',
                'pointer-events: none;',
                'position: absolute;',
                'transform: scale(0);',
                'width: 10px;'
            ];
            rippleLightCSS = 'background-color: rgba(255, 255, 255, 0.32);';
            rippleKeyframes = [
                '@keyframes ripple {',
                'to {',
                'transform: scale(2);',
                'opacity: 0;',
                '}',
                '}'
            ];
            rippleWebkitKeyframes = [
                '@-webkit-keyframes ripple {',
                'to {',
                '-webkit-transform: scale(2);',
                'opacity: 0;',
                '}',
                '}'
            ];
            document.head.appendChild(styleEl);
            styleSheet = styleEl.sheet;
            styleSheet.insertRule('.ripple-effect {' + rippleCSS.join('') + '}', 0);
            styleSheet.insertRule('.ripple-light .ripple-effect {' + rippleLightCSS + '}', 0);
            if (CSSRule.WEBKIT_KEYFRAMES_RULE) { // WebKit                 styleSheet.insertRule(rippleWebkitKeyframes.join(''), 0);
            } else if (CSSRule.KEYFRAMES_RULE) { // W3C
                styleSheet.insertRule(rippleKeyframes.join(''), 0);
            }
        })
        .directive('ripple', function () {
            return {
                restrict: 'C',
                link: function (scope, element, attrs) {
                    element[0].style.position = 'relative';
                    element[0].style.overflow = 'hidden';
                    element[0].style.userSelect = 'none';
                    element[0].style.msUserSelect = 'none';
                    element[0].style.mozUserSelect = 'none';
                    element[0].style.webkitUserSelect = 'none';
                    function createRipple(evt) {
                        var ripple = angular.element('<span class="ripple-effect animate">'),
                                rect = element[0].getBoundingClientRect(),
                                radius = Math.max(rect.height, rect.width),
                                left = evt.pageX - rect.left - radius / 2 - document.body.scrollLeft,
                                top = evt.pageY - rect.top - radius / 2 - document.body.scrollTop;
                        ripple[0].style.width = ripple[0].style.height = radius + 'px';
                        ripple[0].style.left = left + 'px';
                        ripple[0].style.top = top + 'px';
                        ripple.on('animationend webkitAnimationEnd', function () {
                            this.remove();
                        });
                        element.append(ripple);
                    }

                    element.on('click', createRipple);
                }
            };
        })
//        .directive('form', function () {
//            return function (scope, elem, attrs) {
//                for (var i = 0; i < elem.context.length; i++)
//                {
//                    if (elem.context[i].localName == 'input' || elem.context[i].localName == 'textarea')
//                    {
//                        elem.context[i].autocomplete = "__away";
//                    }
//                }
//            };
//        })
        .directive('form', function () {
            return function (scope, elem, attrs) {
                elem.attr('autocomplete', 'off');
            };
        })
        .directive('elemReady', function ($parse) {
            return {
                restrict: 'A',
                link: function ($scope, elem, attrs) {
                    enableSelectBoxes();
                }
            }
        })
        .directive('elemfocusSelect', function ($parse) {
            return {
                restrict: 'A',
                link: function (scope, element, attrs) {
                    element.on('click', function () {
                        this.select();
                    });
                }
            };
        })
        .directive('staticInclude', function () {
            return {
                restrict: 'A',
                templateUrl: function (ele, attrs) {
                    return attrs.staticInclude;
                }
            };
        })
        .directive('googleMap', ['$googleMapService', '$timeout', function ($googleMapService, $timeout) {
                return {
                    restrict: 'A',
                    scope: {
                        option: '='
                    },
                    link: function (scope, element, attrs) {
                        $timeout(function () {
                            $googleMapService.initMap(attrs.id, null, null, scope.option);
                        }, 300);
                    }
                };
            }])
        //        .directive('elemFormField', function() {
        //            return {
        //                restrict: 'A',
        //                // just postLink
        //                link: function(scope, element, attrs, ngModelCtrl) {
        //                    // do nothing in case of no 'name' attribiute
        //                    if (!attrs.name) {
        //                        return;
        //                    }
        //                    // fix what should be fixed
        //                    ngModelCtrl.$name = attrs.name;
        //                },
        //                // ngModel's priority is 0
        //                priority: '-100',
        //                // we need it to fix it's behavior
        //                require: 'ngModel'
        //            };
        //        })
        //
        //        $("#myInputField").focus(function(){
        //            // Select input field contents
        //            this.select(); //        });
        .filter('trustedhtml', function ($sce) {
            return $sce.trustAsHtml;
        })
        .filter('offset', function () {
            return function (input, start) {
                start = parseInt(start, 10);
                if (typeof input != 'undefined' && typeof input.slice != 'undefined')
                    return input.slice(start);
                else
                    return input;
            };
        })
        .directive('hcPieChart', function () {
            return {
                restrict: 'E',
                template: '<div></div>',
                scope: {
                    title: '@',
                    data: '='
                },
                link: function (scope, element) {
                    Highcharts.chart(element[0], {
                        chart: {
                            type: 'pie'
                        },
                        title: {
                            text: scope.title
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                showInLegend: true,
                                dataLabels: {
                                    enabled: true,
                                    format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                                }
                            }
                        },
                        series: [{
                                data: scope.data
                            }]
                    });
                }
            };
        })
        .filter('propsFilter', function () {
            return function (items, props) {
                var out = [];

                if (angular.isArray(items)) {
                    var keys = Object.keys(props);

                    items.forEach(function (item) {
                        var itemMatches = false;

                        for (var i = 0; i < keys.length; i++) {
                            var prop = keys[i];
                            var text = props[prop].toLowerCase();
                            if (item[prop] != undefined && item[prop].toLowerCase().indexOf(text) !== -1) {
                                itemMatches = true;
                                break;
                            }
                        }

                        if (itemMatches) {
                            out.push(item);
                        }
                    });
                } else {
                    // Let the output be the input untouched
                    out = items;
                }

                return out;
            };
        }).filter('htmlToPlaintext', function () {
            return function (text) {
                return  text ? String(text).replace(/<[^>]+>/gm, '') : '';
                };
            }).filter('highlightKeywords', function($sce) {
  
  
    // returns all words in a sentence as an array, ignoring extra whitespace
     var stringToArray = function(input) {
       if(input)
       {
         return input.match(/\S+/g);
           
       }
       else
       {
         return [];
       }
     };

    // returns regex that basically says 'match any word that starts with one of my keywords'
    var getRegexPattern = function(keywordArray) {
      

      var pattern = "(^|\\b)(" + keywordArray.join("|") + ")";
      
      return new RegExp(pattern, "gi");
      
    };
  
    return function(textToHighlight, keywords) {
      var filteredText = textToHighlight;
      if(keywords !== "") {
        
        var keywordList = stringToArray(keywords);
        
        var pattern = getRegexPattern(keywordList);
        console.log(pattern);
        
        filteredText = textToHighlight.replace(pattern, '<span class="highlighted">$2</span>');

      }
      
      return $sce.trustAsHtml(filteredText);
    };
});




