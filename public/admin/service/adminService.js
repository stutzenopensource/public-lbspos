app.service("adminService", function ($httpService, Auth, APP_CONST) {

    return  {
        handleOnlyErrorResponseConfig: {
            "has_success_alert": false,
            "has_error_alert": true
        },
        handleOnlySuccessResponseConfig: {
            "has_success_alert": true,
            "has_error_alert": false
        },
        handleBothSuccessAndErrorResponseConfig: {
            "has_success_alert": true,
            "has_error_alert": true
        },
        ignoreBothSuccessAndErrorResponseConfig: {
            "has_success_alert": false,
            "has_error_alert": false
        },
        appConfig: {
            'date_format': '',
            'invoice_prefix': ''
        },
        createUser: function (data, headers) {
            return $httpService.post(APP_CONST.API.USER_ADD, data, true, this.handleBothSuccessAndErrorResponseConfig, headers);
        },
        deleteUser: function (data, id, config, headers) {
            return $httpService.delete(APP_CONST.API.USER_DELETE.replace('{{id}}', id), data, true, config, headers);
        },
        deleteStockAdjs: function (data, id, comments, config, headers) {
            var api_url = APP_CONST.API.DELETE_STOCK_ADJS;
            api_url = api_url.replace('{{id}}', id);
            api_url = api_url.replace('{{comments}}', comments);
            return $httpService.delete(api_url, data, true, null, config, headers);
        },
        createPayment: function (data, configData, config, headers) {
            return $httpService.post(APP_CONST.API.PAYMENT_SAVE, data, configData, true, config, headers);
        },
        createPurchasePayment: function (data, headers) {
            return $httpService.post(APP_CONST.API.PURCHASE_PAYMENT_SAVE, data, true, this.handleBothSuccessAndErrorResponseConfig, headers);
        },
        getPaymentList: function (data, config) {
            return $httpService.get(APP_CONST.API.PAYMENT_LIST, data, true, config);
        },
        getPurchasePaymentList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.PURCHASE_PAYMENT_LIST, data, true, config, headers);
        },
        editPayment: function (data, id) {
            return $httpService.put(APP_CONST.API.PAYMENT_UPDATE.replace('{{id}}', id), data, true);
        },
        editPurchasePayment: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.PURCHASE_PAYMENT_UPDATE.replace('{{id}}', id), data, true, headers);
        },
        deletePayment: function (data, id, config, headers) {
            return $httpService.delete(APP_CONST.API.PAYMENT_DELETE.replace('{{id}}', id), data, true, config, headers);
        },
        deletePurchasePayment: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.PURCHASE_PAYMENT_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        createCategory: function (data, headers) {
            return $httpService.post(APP_CONST.API.CATEGORY_SAVE, data, true, null, headers);
        },
        modifyUser: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.USER_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        updateCategory: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.CATEGORY_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        updatePayment: function (data, id) {
            return $httpService.put(APP_CONST.API.PAYMENT_EDIT.replace('{{id}}', id), data, true);
        },
        getUserList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.GET_USER_LIST, data, true, config, headers);
        },
        getCategoryList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.GET_CATEGORY_LIST, data, true, config, null, headers);
        },
        changePassword: function (data) {
            return $httpService.post(APP_CONST.API.CHANGE_PASSWORD, data, true);
        },
        getProductListWithTax: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.PRODUCT_LIST_WITH_TAX, data, true, config, headers);
        },
        addProduct: function (data, config, headers) {
            return $httpService.post(APP_CONST.API.PRODUCT_ADD, data, true, config, headers);
        },
        getProductList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.PRODUCT_LIST, data, true, config, headers);
        },
        editProduct: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.PRODUCT_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        deleteProduct: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.PRODUCT_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        addDeal: function (data, headers) {
            return $httpService.post(APP_CONST.API.DEAL_ADD, data, true, null, headers);
        },
        getDealList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.DEAL_LIST, data, true, config, headers);
        },
        editDeal: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.DEAL_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        deleteDeal: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.DEAL_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        getDealActivityList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.DEAL_ACTIVITY_LIST, data, true, config, headers);
        },
        addDealActivity: function (data, headers) {
            return $httpService.post(APP_CONST.API.DEAL_ACTIVITY_ADD, data, true, null, headers);
        },
        addStage: function (data, headers) {
            return $httpService.post(APP_CONST.API.STAGE_ADD, data, true, null, headers);
        },
        getStageList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.STAGE_LIST, data, true, config, headers);
        },
        editStage: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.STAGE_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        deleteStage: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.STAGE_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        addService: function (data, headers) {
            return $httpService.post(APP_CONST.API.SERVICE_ADD, data, true, null, headers);
        },
        getServiceList: function (data, headers) {
            return $httpService.get(APP_CONST.API.SERVICE_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        editService: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.SERVICE_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        deleteService: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.SERVICE_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        getUom: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.UOM, data, true, config, null, headers);
        },
        createUomList: function (data, headers) {
            return $httpService.post(APP_CONST.API.UOM_ADD, data, true, null, headers);
        },
        postUpdateUom: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.UOM_MODIFY.replace('{{id}}', id), data, true, null, headers);
        },
        deleteUOM: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.UOM_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        addCustomer: function (data, config, headers) {
            return $httpService.post(APP_CONST.API.CUSTOMERS_ADD, data, true, config, headers);
        },
        getCustomersList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.CUSTOMERS_LIST, data, true, config, headers);
        },
        editCustomerInfo: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.CUSTOMERS_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        deleteCustomer: function (data, id, is_active, headers) {
            var api_url = APP_CONST.API.CUSTOMERS_DELETE;
            api_url = api_url.replace('{{id}}', id);
            api_url = api_url.replace('{{is_active}}', is_active);
            return $httpService.delete(api_url, data, true, null, headers);
        },
        getCustomerDetail: function (data) {
            return $httpService.get(APP_CONST.API.CUSTOMERS_DETAIL, data, true, this.ignoreBothSuccessAndErrorResponseConfig);
        },
        addCustomerDualInfo: function (data, config, headers) {
            return $httpService.post(APP_CONST.API.CUSTOMERS_DUALINFO_ADD, data, true, config, headers);
        },
        getCustomersDualInfoList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.CUSTOMERS_DUALINFO_LIST, data, true, config, headers);
        },
        editCustomerDualInfo: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.CUSTOMERS_DUALINFO_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        getCustomerDualInfoDetail: function (data) {
            return $httpService.get(APP_CONST.API.CUSTOMERS_DUALINFO_DETAIL, data, true, this.ignoreBothSuccessAndErrorResponseConfig);
        },
        addAttribute: function (data, headers) {
            return $httpService.post(APP_CONST.API.ATTRIBUTE_ADD, data, true, null, headers);
        },
        getAttributeList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.ATTRIBUTE_LIST, data, true, config, headers);
        },
        editAttribute: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.ATTRIBUTE_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        getAttributeTypeList: function (data, config) {
            return $httpService.get(APP_CONST.API.ATTRIBUTE_TYPE_ID_LIST, data, true, config);
        },
        deleteAttribute: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.ATTRIBUTE_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        getSalesquoteList: function (data) {
            return $httpService.get(APP_CONST.API.SALES_QUOTE_LIST, data, false);
        },
        addInvoice: function (data, headers) {
            return $httpService.post(APP_CONST.API.INVOICE_SAVE, data, true, null, headers);
        },
        getNextInvoiceNo: function (data, config, headers) {
            return $httpService.post(APP_CONST.API.NEXT_INVOICE_NO, data, false, config, headers);
        },
        getInvoiceList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.INVOICE_LIST, data, true, config, headers);
        },
        editInvoice: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.INVOICE_UPDATE.replace('{{id}}', id), data, true, null, headers);
        },
        deleteInvoice: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.INVOICE_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        getInvoiceDetail: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.INVOICE_DETAILS, data, true, config, headers);
        },
        addQuote: function (data, headers) {
            return $httpService.post(APP_CONST.API.QUOTE_SAVE, data, true, null, headers);
        },
        getQuoteList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.QUOTE_LIST, data, true, config, headers);
        },
        editQuote: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.QUOTE_UPDATE.replace('{{id}}', id), data, true, null, headers);
        },
        deleteQuote: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.QUOTE_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        getQuoteDetail: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.QUOTE_DETAILS, data, true, config, headers);
        },
        addSalesOrder: function (data, headers) {
            return $httpService.post(APP_CONST.API.SALESORDER_SAVE, data, true, null, headers);
        },
        getSalesOrderList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.SALESORDER_LIST, data, true, config, headers);
        },
        editSalesOrder: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.SALESORDER_UPDATE.replace('{{id}}', id), data, true, null, headers);
        },
        deleteSalesOrder: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.SALESORDER_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        getSalesOrderDetail: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.SALESORDER_DETAILS, data, true, config, headers);
        },
        getTaxList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.TAX_LIST, data, true, config, headers);
        },
        getActiveTaxList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.ACTIVE_TAX_LIST, data, true, config, headers);
        },
        getTaxReport: function (data, header) {
            return $httpService.get(APP_CONST.API.GET_TAX_LIST, data, true, this.handleOnlyErrorResponseConfig, header);
        },
        editTax: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.TAX_UPDATE.replace('{{id}}', id), data, true, null, headers);
        },
        deleteTax: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.TAX_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        addTax: function (data, headers) {
            return $httpService.post(APP_CONST.API.TAX_SAVE, data, true, null, headers);
        },
        addTaxMapping: function (data, config, headers) {
            return $httpService.post(APP_CONST.API.TAX_MAPPING_SAVE, data, config, true, null, headers);
        },
        deleteTaxMapping: function (data, id, config, headers) {
            return $httpService.post(APP_CONST.API.TAX_MAPPING_DELETE, data, config, true, null, headers);
        },
        editAppSettings: function (data, config, headers) {
            return $httpService.post(APP_CONST.API.APP_SETTINGS_SAVE, data, true, config, headers);
        },
        getAppSettingsList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.APP_SETTINGS_LIST, data, true, config, headers);
        },
        addPurchaseInvoice: function (data, headers) {
            return $httpService.post(APP_CONST.API.PURCHASE_INVOICE_SAVE, data, true, null, headers);
        },
        getPurchaseInvoiceList: function (data, headers) {
            return $httpService.get(APP_CONST.API.PURCHASE_INVOICE_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        editPurchaseInvoice: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.PURCHASE_INVOICE_UPDATE.replace('{{id}}', id), data, true, null, headers);
        },
        deletePurchaseInvoice: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.PURCHASE_INVOICE_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        getPurchaseInvoiceDetail: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.PURCHASE_INVOICE_DETAILS, data, true, config, headers);
        },
        getPurchaseInvoiceItemProductDetail: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.PURCHASE_INVOICEITEM_PRODUCT_DETAIL, data, true, config, headers);
        },
        addPurchaseQuote: function (data, headers) {
            return $httpService.post(APP_CONST.API.PURCHASE_QUOTE_SAVE, data, true, null, headers);
        },
        getPurchaseQuoteList: function (data, headers) {
            return $httpService.get(APP_CONST.API.PURCHASE_QUOTE_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        editPurchaseQuote: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.PURCHASE_QUOTE_UPDATE.replace('{{id}}', id), data, true, null, headers);
        },
        deletePurchaseQuote: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.PURCHASE_QUOTE_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        getPurchaseQuoteDetail: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.PURCHASE_QUOTE_DETAILS, data, true, config, headers);
        },
        getUserScreenAccessList: function (data, headers) {
            return $httpService.get(APP_CONST.API.SCREEN_ACCESS_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        getUserAccessDetail: function (data, headers)
        {
            return $httpService.get(APP_CONST.API.USER_ACCESS_INFO, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        editUserScreenAccess: function (data, headers) {
            return $httpService.post(APP_CONST.API.USER_SCREEN_ACCESS_MODIFY, data, true, null, headers);
        },
        getUserRoleList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.USER_ROLE_LIST, data, true, config, headers);
        },
        deleteUserRoleInfo: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.USER_ROLE_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        addUserRole: function (data, headers) {
            return $httpService.post(APP_CONST.API.USER_ROLE_SAVE, data, true, null, headers);
        },
        editUserRole: function (data, headers) {
            return $httpService.post(APP_CONST.API.USER_ROLE_UPDATE, data, true, null, headers);
        },
        getIncomeCategoryList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.GET_INCOME_CATEGORY_LIST, data, true, config, headers);
        },
        addIncomeCategory: function (data, headers) {
            return $httpService.post(APP_CONST.API.INCOME_CATEGORY_SAVE, data, true, null, headers);
        },
        editIncomeCategory: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.INCOME_CATEGORY_UPDATE.replace('{{id}}', id), data, true, null, headers);
        },
        getExpenseList: function (data, headers) {
            return $httpService.get(APP_CONST.API.GET_EXPENSE_LIST, data, true, null, headers);
        },
        addExpense: function (data, headers) {
            return $httpService.post(APP_CONST.API.EXPENSE_SAVE, data, true, null, headers);
        },
        editExpense: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.EXPENSE_UPDATE.replace('{{id}}', id), data, true, null, headers);
        },
        getInvoicePaymentList: function (data, noofdays) {
            return $httpService.get(APP_CONST.API.GET_INVOICE_PAYMENT_LIST.replace('{{noofdays}}', noofdays), data, false);
        },
        getInvoicePurchasePaymentList: function (data, noofdays) {
            return $httpService.get(APP_CONST.API.GET_INVOICE_PURCHASE_PAYMENT_LIST.replace('{{noofdays}}', noofdays), data, false);
        },
        getInvoiceExpensePaymentList: function (data, noofdays) {
            return $httpService.get(APP_CONST.API.GET_INVOICE_EXPENSE_PAYMENT_LIST.replace('{{noofdays}}', noofdays), data, false);
        },
        getCategoryIncomeSummaryList: function (data, noofdays) {
            return $httpService.get(APP_CONST.API.GET_CATEGORY_INCOME_SUMMARY.replace('{{noofdays}}', noofdays), data, false);
        },
        getCategoryExpenseSummaryList: function (data, noofdays) {
            return $httpService.get(APP_CONST.API.GET_CATEGORY_EXPENSE_SUMMARY.replace('{{noofdays}}', noofdays), data, false);
        },
        getInvoicePaymentJson: function (data, noofdays) {
            return $httpService.get(APP_CONST.API.GET_INVOICE_PAYMENT_JSON.replace('{{noofdays}}', noofdays), data, false);
        },
        getInvoicePurchasePaymentJson: function (data, noofdays) {
            return $httpService.get(APP_CONST.API.GET_INVOICE_PURCHASE_PAYMENT_JSON.replace('{{noofdays}}', noofdays), data, false);
        },
        getInvoiceExpensePaymentJson: function (data, noofdays) {
            return $httpService.get(APP_CONST.API.GET_INVOICE_EXPENSE_PAYMENT_JSON.replace('{{noofdays}}', noofdays), data, false);
        },
        getCategoryIncomeSummaryJson: function (data, noofdays) {
            return $httpService.get(APP_CONST.API.GET_CATEGORY_INCOME_SUMMARY_JSON.replace('{{noofdays}}', noofdays), data, false);
        },
        getCategoryExpenseSummaryJson: function (data, noofdays) {
            return $httpService.get(APP_CONST.API.GET_CATEGORY_EXPENSE_SUMMARY_JSON.replace('{{noofdays}}', noofdays), data, false);
        },
        deleteCategory: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.CATEGORY_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        deleteIncomeCategory: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.INCOME_CATEGORY_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        modifyUserRole: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.MODIFY_USER_ROLE.replace('{{id}}', id), data, true, this.handleBothSuccessAndErrorResponseConfig, headers);
        },
        gettransactionlist: function (data) {
            return $httpService.get(APP_CONST.API.GET_TRANSACTION_LIST, data, false);
        },
        getPartySalesList: function (data, headers) {
            return $httpService.get(APP_CONST.API.GET_PARTY_SALES_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        getPartyPurchaseList: function (data, headers) {
            return $httpService.get(APP_CONST.API.GET_PARTY_PURCHASE_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        getItemizedSalesList: function (data, headers) {
            return $httpService.get(APP_CONST.API.GET_ITEMIZED_SALES_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        getItemwiseStockpurchase: function (data, headers) {
            return $httpService.get(APP_CONST.API.GET_ITEMWISESTOCK_PURCHASE_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        getPartywiseSalespurchase: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.GET_PARTWISE_SALES_PURCHASE, data, true, config, headers);
        },
        getActivityList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.GET_ACTIVITY_LIST, data, true, config, headers);
        },
        checkAccount: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.ACCOUNT_CHECK, data, true, config, headers);
        },
        getAccountlist: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.GET_ACCOUNT_LIST, data, true, config, headers);
        },
        createAccount: function (data, headers) {
            return $httpService.post(APP_CONST.API.ACCOUNT_ADD, data, true, null, headers);
        },
        deleteAccount: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.ACCOUNT_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        modifyAccount: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.ACCOUNT_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        getSmsSettingsList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.GET_SMS_SETTINGS_LIST, data, true, config, headers);
        },
        editSmsSettings: function (data, headers) {
            return $httpService.put(APP_CONST.API.EDIT_SMS_SETTINGS, data, true, null, headers);
        },
        getEmailSettingsList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.GET_EMAIL_SETTINGS_LIST, data, true, config, headers);
        },
        editEmailSettings: function (data, headers) {
            return $httpService.put(APP_CONST.API.EDIT_EMAIL_SETTINGS, data, true, null, headers);
        },
        gettaxsummarylist: function (data, headers) {
            return $httpService.get(APP_CONST.API.GET_TAXSUMMARY_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        getDepositList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.GET_DEPOSIT_LIST, data, true, config, headers);
        },
        createDeposit: function (data, headers) {
            return $httpService.post(APP_CONST.API.DEPOSIT_ADD, data, true, null, headers);
        },
        modifyDeposit: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.DEPOSIT_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        deleteDeposit: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.DEPOSIT_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        deleteVoucher: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.VOUCHER_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        getAccountStatementList: function (data, headers) {
            return $httpService.get(APP_CONST.API.GET_ACCOUNTSTATEMENT_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        getTransexpenseList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.GET_TRANSEXPENSE_LIST, data, true, config, headers);
        },
        createTransexpense: function (data, headers) {
            return $httpService.post(APP_CONST.API.TRANSEXPENSE_ADD, data, true, null, headers);
        },
        modifYTransexpense: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.TRANSEXPENSE_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        deleteTransexpense: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.TRANSEXPENSE_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        deleteTransexpensePayment: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.PURCHASE_PAYMENT_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        getInvoiceSummaryList: function (data) {
            return $httpService.get(APP_CONST.API.GET_INVOICE_SUMMARY_LIST, data, false);
        },
        getPurchaseInvoiceSummaryList: function (data) {
            return $httpService.get(APP_CONST.API.GET_PURCHASE_INVOICE_SUMMARY_LIST, data, false);
        },
        getInventoryStockListInfo: function (data, headers) {
            return $httpService.get(APP_CONST.API.GET_INVENTORY_STOCK_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        getstocktracelist: function (data, headers) {
            return $httpService.get(APP_CONST.API.GET_STOCKTRACE_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        getStockadjustmentList: function (data, headers) {
            return $httpService.get(APP_CONST.API.GET_STOCKADJUSTMENT_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        addStockadjustment: function (data, headers) {
            return $httpService.post(APP_CONST.API.STOCKADJUSTMENT_ADD, data, true, null, headers);
        },
        getStockwastageList: function (data, headers) {
            return $httpService.get(APP_CONST.API.GET_STOCKWASTAGE_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        addStockwastage: function (data, headers) {
            return $httpService.post(APP_CONST.API.STOCKWASTAGE_ADD, data, true, null, headers);
        },
        getMinstockList: function (data, headers) {
            return $httpService.get(APP_CONST.API.GET_MINSTOCK_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        receivableList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.RECEIVABLE_LIST, data, true, config, headers);
        },
        payableList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.PAYABLE_LIST, data, true, config, headers);
        },
        getSalesPurchaseMonthlyCount: function (data, monthcode) {
            return $httpService.get(APP_CONST.API.GET_SALES_PURCHASE_MONTHLY_SUMMARY.replace('{{monthcode}}', monthcode), data, false, null, null);
        },
        getMonthwiseSalesList: function (data) {
            return $httpService.get(APP_CONST.API.MONTHWISE_SALES_REPORT, data, false);
        },
        gettransferlist: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.GET_TRANSFER_LIST, data, true, config, headers);
        },
        createtransferlist: function (data, headers) {
            return $httpService.post(APP_CONST.API.TRANSFERLIST_ADD, data, true, null, headers);
        },
        modifytransferlist: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.TRANSFERLIST_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        deletetransferlist: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.TRANSFERLIST_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        getpaymenttermslist: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.GET_PAYMENTTERMS_LIST, data, true, config, headers);
        },
        addpaymentterms: function (data, headers) {
            return $httpService.post(APP_CONST.API.PAYMENTTERMS_ADD, data, true, null, headers);
        },
        modifypaymentterms: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.PAYMENTTERMS_EDIT.replace('{{id}}', id), data, true, this.handleBothSuccessAndErrorResponseConfig, headers);
        },
        deletepaymentterms: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.PAYMENTTERMS_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        getexpensereportList: function (data, header) {
            return $httpService.get(APP_CONST.API.GET_EXPENSEREPORT_LIST, data, true, this.handleOnlyErrorResponseConfig, header);
        },
        getMonthwiseSalesCount: function (data, monthcode) {
            return $httpService.get(APP_CONST.API.GET_MONTHWISE_SALES_COUNT.replace('{{monthcode}}', monthcode), data, false, null, null);
        },
        getTaxGroupDetail: function (data, headers) {
            return $httpService.get(APP_CONST.API.GET_TAXGROUP_DETAIL, data, false, null, headers);
        },
        calculateTaxAmt: function (data) {
            return $httpService.get(APP_CONST.API.GET_CALCULATE_TAX, data, false);
        },
        gettodayBalancelist: function (data) {
            return $httpService.get(APP_CONST.API.GET_TODAYBALANCE_LIST, data, false);
        },
        getalbumlist: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.GET_ALBUM_LIST, data, true, config, headers);
        },
        createAlbum: function (data, headers) {
            return $httpService.post(APP_CONST.API.ALBUM_ADD, data, true, null, headers);
        },
        modifyAlbum: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.ALBUM_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        viewAlbum: function (data) {
            return $httpService.get(APP_CONST.API.GET_ALBUM_VIEW, data, false);
        },
        deletealbumlist: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.ALBUM_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        getimagelist: function (data) {
            return $httpService.get(APP_CONST.API.GET_IMAGE_LIST, data, true, null);
        },
        updateImageSortby: function (data, configData) {
            return $httpService.put(APP_CONST.API.UPDATE_IMAGE_SORTBY, data, true, configData);
        },
        getcommentlist: function (data, config) {
            return $httpService.get(APP_CONST.API.GET_COMMENT_LIST, data, false, config);
        },
        addcommentlist: function (data) {
            return $httpService.post(APP_CONST.API.COMMENTLIST_ADD, data, true, null);
        },
        getSharelist: function (data, config) {
            return $httpService.get(APP_CONST.API.GET_SHARE_LIST, data, false, config);
        },
        addSharelist: function (data) {
            return $httpService.post(APP_CONST.API.SHARELIST_ADD, data, false, null);
        },
        saveAsset: function (data) {
            return $httpService.post(APP_CONST.API.ASSET_ADD, data, true, null);
        },
        saveComment: function (data) {
            return $httpService.post(APP_CONST.API.SAVE_COMMENT, data, true, null);
        },
        deleteshare: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.SHARELIST_DELETE.replace('{{id}}', id), data, false, null, headers);
        },
        createUserInvite: function (data) {
            return $httpService.post(APP_CONST.API.SAVE_USERINVITE, data, true, null);
        },
        createSendMail: function (data) {
            return $httpService.post(APP_CONST.API.CREATE_SENDMAIL, data, true, null);
        },
        getAlbumAssetList: function (data, config) {
            return $httpService.get(APP_CONST.API.GET_ALBUM_ASSET_LIST, data, false, config);
        },
        getCountryList: function (data, config) {
            return $httpService.get(APP_CONST.API.COUNTRY_LIST, data, false, config);
        },
        saveCountry: function (data, config, headers) {
            return $httpService.post(APP_CONST.API.COUNTRY_ADD, data, true, config, headers);
        },
        editCountry: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.COUNTRY_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        deleteCountry: function (data, id, config, headers) {
            return $httpService.delete(APP_CONST.API.COUNTRY_DELETE.replace('{{id}}', id), data, true, config, headers);
        },
        getStateList: function (data, config) {
            return $httpService.get(APP_CONST.API.STATE_LIST, data, false, config);
        },
        saveState: function (data, headers) {
            return $httpService.post(APP_CONST.API.STATE_ADD, data, true, null, headers);
        },
        editState: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.STATE_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        deleteState: function (data, id, config, headers) {
            return $httpService.delete(APP_CONST.API.STATE_DELETE.replace('{{id}}', id), data, true, config, headers);
        },
        getCityList: function (data, config) {
            return $httpService.get(APP_CONST.API.CITY_LIST, data, false, config);
        },
        saveCity: function (data, headers) {
            return $httpService.post(APP_CONST.API.CITY_ADD, data, true, null, headers);
        },
        editCity: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.CITY_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        deleteCity: function (data, id, config, headers) {
            return $httpService.delete(APP_CONST.API.CITY_DELETE.replace('{{id}}', id), data, true, config, headers);
        },
        addAdvancepayment: function (data, headers) {
            return $httpService.post(APP_CONST.API.ADVANCEPAYMENT_ADD, data, true, null, headers);
        },
        getAdvancepaymentList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.ADVANCEPAYMENT_LIST, data, true, config, headers);
        },
        editAdvancepayment: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.ADVANCEPAYMENT_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        deleteAdvancepayment: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.ADVANCEPAYMENT_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        deleteTierPriceMapping: function (data, headers) {
            return $httpService.post(APP_CONST.API.DELETE_TIER_PRICE_MAPPING, data, true, null, headers);
        },
        updateTierPriceMapping: function (data, headers) {
            return $httpService.post(APP_CONST.API.UPDATE_TIER_PRICE_MAPPING, data, true, null, headers);
        },
        getTierPriceMappingList: function (data, config) {
            return $httpService.get(APP_CONST.API.GET_TIER_PRICE_MAPPING_LIST, data, false, config);
        },
        getProductBasedList: function (data, config, headers, abortFlag) {
            return $httpService.get(APP_CONST.API.PRODUCT_BASED_LIST, data, true, config, headers, abortFlag);
        },
        findCurrentDaycode: function (data, config) {
            return $httpService.get(APP_CONST.API.FIND_CURRENT_DAYCODE, data, false, config);
        },
        saveDaycode: function (data, headers) {
            return $httpService.post(APP_CONST.API.SAVE_DAYCODE, data, true, null, headers);
        },
        findDayCloseCredit: function (data, config) {
            return $httpService.get(APP_CONST.API.FIND_DAYCLOSE_CREDIT, data, false, config);
        },
        getPurFromCusList: function (data, headers) {
            return $httpService.get(APP_CONST.API.GET_PURCHASE_FROM_CUS_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        getSalesToCusList: function (data, headers) {
            return $httpService.get(APP_CONST.API.GET_SALES_TO_CUS_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        getCusPurDetail: function (data, headers) {
            return $httpService.get(APP_CONST.API.GET_CUS_PUR_DETAIL, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        getdiscountedReportList: function (data, header) {
            return $httpService.get(APP_CONST.API.DISCOUNTED_REPORT, data, true, this.handleOnlyErrorResponseConfig, header);
        },
        getPartywisePurchaseReportList: function (data, header) {
            return $httpService.get(APP_CONST.API.PARTYWISE_PURCHASE_REPORT, data, true, this.handleOnlyErrorResponseConfig, header);
        },
        invoiceReturnSave: function (data, header) {
            return $httpService.post(APP_CONST.API.INVOICE_RETURN_SAVE, data, true, this.handleBothSuccessAndErrorResponseConfig, header);
        },
        getSalesAgeingDetail: function (data, config) {
            return $httpService.get(APP_CONST.API.GET_SALES_AGEING_DETAIL, data, true, config);
        },
        saveRelabel: function (data, headers) {
            return $httpService.post(APP_CONST.API.SAVE_RELABEL, data, true, null, headers);
        },
        downloadCurrentLabel: function (data, headers) {
            return $httpService.get(APP_CONST.API.DOWNLOAD_CURRENT_LABEL, data, true, null, headers);
        },
        generateBarCode: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.GENERATE_BAR_CODE.replace('{{id}}', id), data, true, null, headers);
        },
        getReferenceDetail: function (data, config) {
            return $httpService.get(APP_CONST.API.REFERENCE_NO, data, true, config);
        },
        deletePurInvoiceItem: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.PUR_INVOICE_ITEM_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        getCategorywiseStock: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.CATEGORYWISE_STOCK_REPORT, data, true, config, headers);
        },
        getCategoryWise: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.CATEGORY_WISE_REPORT, data, true, config, headers);
        },
        getBarcodewiseStock: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.BARCODEWISE_STOCK_REPORT, data, true, config, headers);
        },
        getAcodeStatement: function (data, headers) {
            return $httpService.get(APP_CONST.API.ACODE_STATEMENT, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        getTag: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.TAG_LIST, data, true, config, null, headers);
        },
        createTagList: function (data, headers) {
            return $httpService.post(APP_CONST.API.TAG_ADD, data, true, null, headers);
        },
        getTagDetail: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.TAG_DETAIL, data, true, config, null, headers);
        },
        createCatalogCategory: function (data, headers) {
            return $httpService.post(APP_CONST.API.CATALOG_CATEGORY_SAVE, data, true, null, headers);
        },
        getCatalogCategory: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.CATALOG_CATEGORY_LIST, data, true, config, headers);
        },
        editCatalogCategory: function (data, headers) {
            return $httpService.post(APP_CONST.API.CATALOG_CATEGORY_EDIT, data, true, null, headers);
        },
        getCatalogProduct: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.CATALOGPRODUCT_LIST, data, true, config, null, headers);
        },
//        createCatalogProductList: function(data, headers) {
//            return $httpService.post(APP_CONST.API.CATALOGPRODUCT_ADD, data, true, null, headers);
//        },
        getCatalogProductDetail: function (data, headers) {
            return $httpService.get(APP_CONST.API.CATALOGPRODUCT_DETAIL, data, true, null, headers);
        },
        createCatalogProductList: function (data, categoryIds, tagIds, config, headers) {
            var api_url = APP_CONST.API.CATALOGPRODUCT_ADD;
            api_url = api_url.replace('{{tagIds}}', tagIds);
            api_url = api_url.replace('{{categoryIds}}', categoryIds);
            return $httpService.post(api_url, data, true, null, config, headers);
        },
        getPriceList: function (itemId, headers) {
            return $httpService.get(APP_CONST.API.PRICE_LIST.replace('{{itemId}}', itemId), true, null, headers);
        },
        savePrice: function (data, itemId, headers) {
            var api_url = APP_CONST.API.PRICE_SAVE;
            api_url = api_url.replace('{{itemId}}', itemId);
            return $httpService.post(api_url, data, true, null, headers);
        },
        createPurchaseInvoiceReturn: function (data, headers) {
            return $httpService.post(APP_CONST.API.PURCHASE_INVOICE_RETURN_SAVE, data, true, null, headers);
        },
        getPurchaseInvoiceReturn: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.PURCHASE_INVOICE_RETURN_LISTALL, data, true, config, headers);
        },
        getPurchaseInvoiceReturnDetail: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.PURCHASE_INVOICE_RETURN_DETAIL, data, true, config, headers);
        },
        deletePurchaseInvoiceReturn: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.PURCHASE_INVOICE_RETURN_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        editPurchaseInvoiceReturn: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.PURCHASE_INVOICE_RETURN_UPDATE.replace('{{id}}', id), data, true, null, headers);
        },
        getAccountSummaryDetailReport: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.ACCOUNT_SUMMARY_REPORT, data, true, config, headers);
        },
        getItemwiseAgeingReport: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.ITEMWISE_AGEING_REPORT, data, true, config, headers);
        },
        getcategorywiseAgeingStockReport: function (data, headers) {
            return $httpService.get(APP_CONST.API.CATEGORYWISE_AGEING_STOCK_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        createStockAdjustment: function (data, headers) {
            return $httpService.post(APP_CONST.API.STOCK_ADJUSTMENT_SAVE, data, true, null, headers);
        },
        getNewStockAdjustmentList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.STOCK_ADJUSTMENT_LISTALL, data, true, config, headers);
        },
        getNewStockAdjustmentDetail: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.STOCK_ADJUSTMENT_DETAIL, data, true, config, headers);
        },
        editNewStockAdjustment: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.STOCK_ADJUSTMENT_UPDATE.replace('{{id}}', id), data, true, null, headers);
        },
        deleteNewStockAdjustment: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.STOCK_ADJUSTMENT_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        getNextStockAdjNo: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.NEXT_STOCK_ADJUSTMENT_NO, data, false, config, headers);
        },
        getAdjustmentWastageReport: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.CATEGORYWISE_STOCK_ADJUST_WASTAGE_REPORT, data, true, config, headers);
        },
        getEmployeeList: function (data, config) {
            return $httpService.get(APP_CONST.API.EMPLOYEE_LIST_ALL, data, false, config);
        },
        saveEmployee: function (data, config, headers) {
            return $httpService.post(APP_CONST.API.EMPLOYEE_SAVE, data, true, config, headers);
        },
        editEmployee: function (data, id, config, headers) {
            return $httpService.put(APP_CONST.API.EMPLOYEE_EDIT.replace('{{id}}', id), data, true, config, headers);
        },
        deleteEmployee: function (data, id, config, headers) {
            return $httpService.delete(APP_CONST.API.EMPLOYEE_DELETE.replace('{{id}}', id), data, true, config, headers);
        },
        getDesignationList: function (data, config) {
            return $httpService.get(APP_CONST.API.DESIGNATION_LIST_ALL, data, false, config);
        },
        saveDesignation: function (data, headers) {
            return $httpService.post(APP_CONST.API.DESIGNATION_SAVE, data, true, null, headers);
        },
        editDesignation: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.DESIGNATION_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        deleteDesignation: function (data, id, config, headers) {
            return $httpService.delete(APP_CONST.API.DESIGNATION_DELETE.replace('{{id}}', id), data, true, config, headers);
        },
        getEmployeeSalesReport: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.EMP_SALES_REPORT, data, true, config, headers);
        },
        getCodeAutoGeneratedList: function (data, config) {
            return $httpService.get(APP_CONST.API.EMPLOYEE_CODE_AUTO_GENERATE, data, false, config);
        },
        upateNewUser: function (data) {
            return $httpService.post(APP_CONST.API.USER_UPDATE, data, true, null);
        },
        getNotificationList: function (data) {
            return $httpService.get(APP_CONST.API.NOTIFICATION_LIST_ALL, data, false, this.handleOnlyErrorResponseConfig);
        },
        updateNotification: function (id) {
            return $httpService.put(APP_CONST.API.NOTIFICATION_UPDATE.replace('{{id}}', id), true, null, this.handleOnlyErrorResponseConfig);
        },
        getNotificationCount: function (data) {
            return $httpService.get(APP_CONST.API.NOTIFICATION_COUNT, data, false, this.handleOnlyErrorResponseConfig);
        },
        payableAndReceivableList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.PAYABLE_AND_RECEIVABLE_REPORT, data, true, config, headers);
        },
        getDeliveryNoteList: function (data, config) {
            return $httpService.get(APP_CONST.API.DELIVERY_NOTE_LISTALL, data, false, config);
        },
        saveDeliveryNote: function (data, config, headers) {
            return $httpService.post(APP_CONST.API.DELIVERY_NOTE_SAVE, data, true, config, headers);
        },
        getDeliveryNoteReport: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.DELIVERY_NOTE_REPORT, data, true, config, headers);
        },
        getDayLogReport: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.DAY_LOG_REPORT, data, true, config, headers);
        },
        deleteDeliverynote: function (data, id, config, headers) {
            return $httpService.delete(APP_CONST.API.DELIVERY_NOTE_DELETE.replace('{{id}}', id), data, true, config, headers);
        },
        getDeliveryDetail: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.DELIVERY_NOTE_DETAIL, data, true, config, headers);
        },
        editDeliveryNoteDetails: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.DELIVERY_NOTE_MODIFY.replace('{{id}}', id), data, true, null, headers);
        },
        deliveryQtyUpdate: function (data, config) {
            return $httpService.post(APP_CONST.API.DELIVERY_NOTE_QTY_UPDATE, data, true, config);
        },
        getBalanceQty: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.BALANCE_QTY, data, true, config, headers);
        },
        getPriceBasedTax: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.GET_PRICE_BASED_TAX, data, true, config, headers);
        },
        getHSNTaxDetail: function (data, config) {
            return $httpService.get(APP_CONST.API.GET_HSN_TAX_DETAIL, data, true, config);
        },
        saveHSNCode: function (data, config, headers) {
            return $httpService.post(APP_CONST.API.SAVE_HSN_TAX_DETAIL, data, true, config, headers);
        },
        getHSNList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.HSN_CODE_LIST, data, true, config, headers);
        },
        editHSNCode: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.HSN_CODE_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
    }

});


