






app.controller('albumCtrl', ['$scope', '$rootScope', '$state', '$stateParams', '$cookies', '$location', 'albumService', 'Auth', 'ValidationFactory', '$window', function($scope, $rootScope, $state, $stateParams, $cookies, $location, albumService, Auth, ValidationFactory, $window) {

        $scope.validationFactory = ValidationFactory;
        //Tabs select
        $scope.tab = 1;
        $scope.setTab = function(newTab) {
            $scope.tab = newTab;
        };

        $scope.isSet = function(tabNum) {
            return $scope.tab === tabNum;
        };

        $scope.id = '';
        $scope.username = '';
        $scope.password = '';
        $scope.email = '';
        $scope.phone = '';
        $scope.album_code = '';
        $scope.customername = '';
        $scope.isDataProcessing = false;
        $scope.getAlbumpermission = function()
        {
            $scope.isDataSavingProcess = true;
            var getListParam = {};
            getListParam.album_code = $stateParams.code;
            getListParam.customername = $scope.username;
            getListParam.password = $scope.password;
            albumService.getAlbumPermission(getListParam).then(function(response) {
                if (response.data.success == true)
                {
                    var landingUrl = $window.location.protocol + "//" + "/lbs/public/#/albumView";
                    $window.location.href = landingUrl
                }
                $scope.isDataSavingProcess = false;
            });
        };

        $scope.getAlbumList = function()
        {
            $scope.isLoadingProgress = true;
            var getListParam = {};
            getListParam.code = $stateParams.code;
            getListParam.album_id = '';
            getListParam.customer_id = '';
            albumService.getalbumlist(getListParam).then(function(response)
            {
                $scope.showPrivate = false;
                // $scope.showPublic = false;
                var data = response.data.list;
                $scope.albumlist = data[0];
                if ($scope.albumlist.type == 'private')
                {
                    $scope.showPrivate = true;
                }
                else if ($scope.albumlist.type == 'public' && $state.current.name != 'albumView')
                {
                    $state.go('albumView', {'id': $stateParams.code}, {'reload': true});
                }
                $scope.total = data.total;
                $scope.isLoadingProgress = false;
            });

        };
        $scope.getAlbumList();

    }]);




