






app.service("albumService", function($httpService, Auth, APP_CONST) {

    return  {
        getAlbumPermission: function(data) {
            return $httpService.post(APP_CONST.API.ALBUM_PERMISSION, data, true);
        },
        getalbumlist: function(data) {
            return $httpService.get(APP_CONST.API.GET_ALBUM_LIST, data, true);
        },
        viewAlbum: function(data) {
            return $httpService.get(APP_CONST.API.GET_ALBUM_VIEW, data, false);
        },
        getcommentlist: function(data, config) {
            return $httpService.get(APP_CONST.API.GET_COMMENT_LIST, data, false, config);
        },
        saveComment: function(data) {
            return $httpService.post(APP_CONST.API.SAVE_COMMENT, data, true, null);
        },
        getAlbumAssetList: function(data) {
            return $httpService.get(APP_CONST.API.GET_ALBUM_ASSET_LIST, data, false, null);
        },
        saveAsset: function(data) {
            return $httpService.post(APP_CONST.API.ASSET_ADD, data, true, null);
        }
    }

});


