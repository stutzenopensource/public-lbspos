
app.controller('autocompleteContactCtrl', ['$scope', '$filter', '$httpService', 'APP_CONST', '$timeout', function($scope, $filter, $httpService, APP_CONST, $timeout) {

    //$scope.searchService = $searchService;

    $scope.selectedItem = null;
    $scope.searchKeyword = '';
    $scope.selectedIndex = -1;

    $scope.dropdown = {
        suggestions: [],
        specialitySuggestions: {
            list: [],
            currentpage: 1,
            limit: 5,
            total: 0
        },
        otherSuggestions: {
            list: [],
            currentpage: 1,
            limit: 5,
            total: 0
        },
        show: false,
        minChars: 1
    };


    $scope.hideDropdown = function()
    {
        if ($scope.dropdown.show)
        {
            $scope.dropdown.show = false;
            if ($scope.searchKeyword != $scope.searchKeywordCaption())
            {
                $scope.ngModel = null;
            }
        }
    }

    $scope.blurDropdown = function()
    {
        $timeout(function() {
            $scope.hideDropdown();
        }, 100);
    }




    $scope.updateSearchData = function() {

        if ($scope.selectedItem != null)
        {
            return;
        }


        //if ($scope.searchKeyword != "")
        {
            var getParam = {};
            getParam.name = $scope.searchKeyword;
            $httpService.get(APP_CONST.GET_CONTACTLIST_API_URL, getParam, false).then(function(responseData) {
                var data = responseData.data.data;
                $scope.dropdown.suggestions.list = data.list;
                if($scope.dropdown.suggestions.list.length > 0)
                {
                    $scope.selectedIndex = 0;
                }
                
            //$scope.dropdown.specialitySuggestions.list  = data.hits;
            //$scope.dropdown.specialitySuggestions.total = data.nbHits;
            });
        }

    };

    $scope.select = function(suggestion) 
    {
        $scope.dropdown.show = false;
        $scope.searchKeyword = suggestion.name + '-' + suggestion.city;
         
        var selectedValue = {
            id: suggestion.id,
            name: suggestion.name,
            city: suggestion.city
        }
            
        $scope.ngModel = selectedValue;   
    };
    

    $scope.updateInitSelectedSuggestionCaption = function() {

        if ($scope.ngModel != null)
        {
            $scope.searchKeyword = $scope.ngModel.name + '-' + $scope.ngModel.city;
        //$scope.searchKeyword = $scope.ngModel.name;
        }

    }

    $scope.updateInitSelectedSuggestionCaption = function() {

        if ($scope.ngModel != null)
        {
            $scope.searchKeyword = $scope.searchKeywordCaption();
        //$scope.searchKeyword = $scope.ngModel.name;
        }
        else
        {
            $scope.searchKeyword = '';
        }

    }

    $scope.searchKeywordCaption = function()
    {
        var retValue = '';
        if ($scope.ngModel != null)
        {
            if (typeof $scope.ngModel.name != 'undefined' && $scope.ngModel.city != 'undefined')
            {
                retValue += $scope.ngModel.name + '-' + $scope.ngModel.city;
            }
        }
        return retValue;
    }

    $scope.isValid = function()
    {
        if ($scope.searchKeyword != $scope.searchKeywordCaption())
        {
            return false;
        }
        return true;
    }
    $scope.isEmpty = function()
    {
        if ($scope.ngModel == null)
        {
            return true;
        }
        return false;
    }

    $scope.$watch('ngModel', function(ngModel) {
        $scope.updateInitSelectedSuggestionCaption();

    });


}]);




