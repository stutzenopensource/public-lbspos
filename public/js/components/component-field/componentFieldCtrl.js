
app.controller('componentFieldCtrl', ['$scope', '$filter', '$httpService', 'APP_CONST', '$timeout', 'ValidationFactory', function($scope, $filter, $httpService, APP_CONST, $timeout, ValidationFactory) {

        //$scope.searchService = $searchService;
        $scope.validationFactory = ValidationFactory;
        $scope.selectedItem = null;
        $scope.searchKeyword = '';
        $scope.selectedIndex = -1;

        $scope.dropdown = {
            suggestions: [],
            specialitySuggestions: {
                list: [],
                currentpage: 1,
                limit: 5,
                total: 0
            },
            otherSuggestions: {
                list: [],
                currentpage: 1,
                limit: 5,
                total: 0
            },
            show: false,
            minChars: 1
        };


        $scope.hideDropdown = function()
        {
            if ($scope.dropdown.show)
            {
                $scope.dropdown.show = false;
                if ($scope.searchKeyword != $scope.searchKeywordCaption())
                {
                    $scope.ngModel = null;
                }
            }
        }

        $scope.blurDropdown = function()
        {
            $timeout(function() {
                $scope.hideDropdown();
            }, 100);
        }

        $scope.updateSearchData = function() {

            if ($scope.selectedItem != null)
            {
                return;
            }

            //if ($scope.searchKeyword != "")
            {
                var getParam = {};
                getParam.userName = $scope.searchKeyword;
                getParam.limit = 10;
                $httpService.get(APP_CONST.API.GET_USER_LIST, getParam, false).then(function(responseData) {
                    var data = responseData.data.data;
                    $scope.dropdown.suggestions.list = data;
                    if ($scope.dropdown.suggestions.list.length > 0)
                    {
                        $scope.selectedIndex = 0;
                    }

                    //$scope.dropdown.specialitySuggestions.list  = data.hits;
                    //$scope.dropdown.specialitySuggestions.total = data.nbHits;
                });
            }

        };

        $scope.select = function(suggestion)
        {
            $scope.dropdown.show = false;
            $scope.searchKeyword = suggestion.name;

            var selectedValue = {
                id: suggestion.id,
                name: suggestion.name,
                city: suggestion.city
            }

            $scope.ngModel = selectedValue;
        };


        $scope.updateInitSelectedSuggestionCaption = function() {

            if ($scope.ngModel != null)
            {
                $scope.searchKeyword = $scope.ngModel.name;
                //$scope.searchKeyword = $scope.ngModel.name;
            }

        }

        $scope.updateInitSelectedSuggestionCaption = function() {

            if ($scope.ngModel != null)
            {
                $scope.searchKeyword = $scope.searchKeywordCaption();
                //$scope.searchKeyword = $scope.ngModel.name;
            }
            else
            {
                $scope.searchKeyword = '';
            }
        }

        $scope.ngChangeInfo = function( id,val)
        {
            console.log("ngChangeInfo calling",id);
            console.log("ngChangeInfo calling",val);
            $scope.$emit('updateItemChngEvent',id,val);
        }              

        $scope.searchKeywordCaption = function()
        {
            var retValue = '';
            if ($scope.ngModel != null)
            {
                if (typeof $scope.ngModel.name != 'undefined')
                {
                    retValue += $scope.ngModel.name;
                }
            }
            return retValue;
        }

        $scope.isValid = function()
        {
            if ($scope.searchKeyword != $scope.searchKeywordCaption())
            {
                return false;
            }
            return true;
        }
        $scope.isEmpty = function()
        {
            if ($scope.ngModel == null)
            {
                return true;
            }
            return false;
        }

        $scope.$watch('ngModel', function(ngModel) {
            $scope.updateInitSelectedSuggestionCaption();

        });

        $scope.validateInput = function( ) 
        {
            var retVal = false;
            
            console.log("Validate Input");
//            console.log("Validate Input",isreq);
//            console.log("Validate Input==>1",value);
//            
//            if (isreq != '0')
//            {
//                if (value == '')
//                {
//                    retVal = false;
//                }
//                if (value != '')
//                {
//                    retVal = true;
//                }
//            }
            return retVal;
        };


    }]);




