angular.module('app')
        .directive('customCheckbox', ['$timeout', '$window', '$rootScope', function($timeout, $window, $rootScope) {
                return {
                    restrict: 'E',
                    replace: true,
                    scope: {
                        type:'=',
                        fieldValue:'=', 
                        formName:'=',
                        name:'@'
                    },
                    controller: 'customCheckboxCtrl',
                    templateUrl: '../js/components/custom-checkbox/custom-checkbox.html',
                    link: function($scope, element, attrs, ngModelController) {
                        
                        /* BASIC SETTINGS FOR DROPDOWN */
                        console.log("$scope.fieldValue");
                        console.log($scope.fieldValue);
                        $scope.initField();
                    }
                }
            }]);