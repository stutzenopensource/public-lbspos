angular.module('app')
        .directive('customDate', ['$timeout', '$window', '$rootScope', function($timeout, $window, $rootScope) {
                return {
                    restrict: 'E',
                    replace: true,
                    scope: {
                        type:'=',
                        fieldValue:'=', 
                        formName:'=',
                        name:'@'
                    },
                    controller: 'customDateCtrl',
                    templateUrl: '../js/components/custom-date/custom-date.html',
                    link: function($scope, element, attrs, ngModelController) {
                        
                        /* BASIC SETTINGS FOR DROPDOWN */
                        console.log("$scope.fieldValue");
                        console.log($scope.fieldValue);
                        $scope.initField();
                    }
                }
            }]);