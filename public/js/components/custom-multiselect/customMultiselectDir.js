angular.module('app')
        .directive('customMultiselect', ['$timeout', '$window', '$rootScope', function($timeout, $window, $rootScope) {
                return {
                    restrict: 'E',
                    replace: true,
                    scope: {
                        type:'=',
                        fieldValue:'=', 
                        formName:'=',
                        name:'@'
                    },
                    controller: 'customMultiselectCtrl',
                    templateUrl: '../js/components/custom-multiselect/custom-multiselect.html',
                    link: function($scope, element, attrs, ngModelController) {
                        
                        /* BASIC SETTINGS FOR DROPDOWN */
                        console.log("$scope.fieldValue");
                        console.log($scope.fieldValue);
                        $scope.initField();
                    }
                }
            }]);