angular.module('app')
        .directive('customTextinput', ['$timeout', '$window', '$rootScope', function($timeout, $window, $rootScope) {
                return {
                    restrict: 'E',
                    replace: true,
                    scope: {
                        type:'=',
                        fieldValue:'=', 
                        formName:'=',
                        name:'@'
                    },
                    controller: 'customTextInputCtrl',
                    templateUrl: '../js/components/custom-textinput/custom-textinput.html',
                    link: function($scope, element, attrs, ngModelController) {
                        
                        /* BASIC SETTINGS FOR DROPDOWN */
                        console.log("$scope.fieldValue");
                        console.log($scope.fieldValue);
                        $scope.initField();
                    }
                }
            }]);