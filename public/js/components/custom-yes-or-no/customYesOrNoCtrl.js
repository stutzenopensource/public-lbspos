
app.controller('customYesOrNoCtrl', ['$scope', '$filter', '$httpService', 'APP_CONST', '$timeout', 'ValidationFactory', function($scope, $filter, $httpService, APP_CONST, $timeout, ValidationFactory) {
    $scope.dataModel = {
        name:'',
        value: '',
        label: '',
        list:[],
        values:[],
        isRequired:false,
        isEditable:true,
        validatePattern : null  
    };

    $scope.initField = function() {

        $timeout(function() {
            if ($scope.fieldValue != undefined && $scope.fieldValue != '')
            {
                if ($scope.fieldValue.value != undefined)
                {
                    $scope.dataModel.value = $scope.fieldValue.value;
                    var data =  {};
                    data.code = $scope.fieldValue.attribute_code;
                    if( $scope.dataModel.value == true)
                    {
                        $scope.dataModel.list[0] = "yes";
                        $scope.dataModel.value = true;
                        data.value = "yes";
                    }
                    else
                    {
                        $scope.dataModel.list[0] = "no";
                        $scope.dataModel.value = false;   
                        data.value = "no";
                    }
                }
                else if ($scope.fieldValue.default_value != undefined)
                {
                    $scope.dataModel.value = $scope.fieldValue.default_value;
                }
                    
                if ($scope.fieldValue.has_editable != undefined)
                {
                    $scope.dataModel.isEditable = $scope.fieldValue.has_editable == 1 ? true:false;
                }
                if ($scope.fieldValue.is_required != undefined)
                {
                    $scope.dataModel.isRequired = $scope.fieldValue.is_required == 1? true:false;
                }
                if ($scope.fieldValue.reg_pattern != undefined && $scope.fieldValue.reg_pattern != '')
                {                      
                    var patternStr = $scope.fieldValue.reg_pattern.replace(/\\/i, "\\");
                    patternStr = patternStr.replace(/,/i, "\,");
                    $scope.dataModel.validatePattern = new RegExp( patternStr);
                }
                if ($scope.fieldValue.attribute_code != undefined)
                {
                    $scope.dataModel.name = $scope.fieldValue.attribute_code;
                }
                    
                if(typeof $scope.fieldValue.option_value != "undefined" && $scope.fieldValue.option_value != null && $scope.fieldValue.option_value != "")
                {
                    console.log($scope.fieldValue.option_value);
                    $scope.dataModel.list = $scope.fieldValue.option_value.split(',');
                    console.log($scope.dataModel.list);
                }
                $scope.dataModel.label = $scope.fieldValue.attribute_label;                    
            }
        }, 0);

    };

    $scope.validateInput = function()
    {
        var retVal = false;
            
        if($scope.dataModel.isRequired && $scope.dataModel.validatePattern != null)
        {
            if ($scope.dataModel.validatePattern.test($scope.dataModel.value))
            {
                retVal = true;
            } 
        }
        else
        {
            retVal = true;
        }
        return retVal;
    };

    $scope.initChangeTimeoutPromise = null;

    $scope.initChangeHandler = function()
    {
        if ($scope.initChangeTimeoutPromise != null)
        {
            $timeout.cancel($scope.initChangeTimeoutPromise);
        }
        $scope.initChangeTimeoutPromise = $timeout($scope.changeHandler, 300);
    };
        
    $scope.changeHandler = function( index )
    {        
        var data =  {};
        data.code = $scope.fieldValue.attribute_code;
        
        if( $scope.dataModel.value == true)
        {
            $scope.dataModel.list[index] = "yes";
            $scope.dataModel.value = true;
            data.value = "yes";
        }
        else
        {
            $scope.dataModel.list[index] = "no";
            $scope.dataModel.value = false;   
            data.value = "no";
        }
        $scope.$emit('customfieldValueChangeEvent', data);
    };                     
}]);