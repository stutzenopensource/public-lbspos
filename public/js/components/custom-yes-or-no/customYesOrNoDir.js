angular.module('app')
        .directive('customYesOrNo', ['$timeout', '$window', '$rootScope', function($timeout, $window, $rootScope) {
                return {
                    restrict: 'E',
                    replace: true,
                    scope: {
                        type:'=',
                        fieldValue:'=', 
                        formName:'=',
                        name:'@'
                    },
                    controller: 'customYesOrNoCtrl',
                    templateUrl: '../js/components/custom-yes-or-no/custom-yes-no.html',
                    link: function($scope, element, attrs, ngModelController) {
                        
                        /* BASIC SETTINGS FOR DROPDOWN */
                        console.log("$scope.fieldValue");
                        console.log($scope.fieldValue);
                        $scope.initField();
                    }
                }
            }]);