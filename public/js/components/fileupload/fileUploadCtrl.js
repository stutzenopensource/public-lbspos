app.controller('fileUploadCtrl', ['$scope', '$rootScope', 'FileUploader', '$timeout', function($scope, $rootScope, FileUploader, $timeout) {

        console.log('$rootScope.appConst');
        console.log($rootScope.APP_CONST)

        $scope.retryLimit = 2;
        $scope.connectionTimeOutRetry = 1000 * 20;

        $scope.primaryImage = '';

        var uploader = $scope.uploader = new FileUploader({
            url: '',
            autoUpload: true,
            cloudSignedUrl: '../urlsign/url',
            cloudUploadPath: $scope.cloudUploadPath
        });

        // FILTERS
        uploader.filters.push({
            name: 'customFilter',
            fn: function(item /*{File|FileLikeObject}*/, options) {
                return this.queue.length < 10;
            }
        });

        // CALLBACKS

        uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
            //console.info('onWhenAddingFileFailed', item, filter, options);
        };
        uploader.onAfterAddingFile = function(fileItem) {
            $rootScope.$broadcast('updateFileUploadEvent', $scope.uploader.queue);
            //console.info('onAfterAddingFile', fileItem);
        };
        uploader.onAfterAddingAll = function(addedFileItems) {
            //console.info('onAfterAddingAll', addedFileItems);
        };
        uploader.onBeforeUploadItem = function(item) {
            //console.info('onBeforeUploadItem', item);
        };
        uploader.onProgressItem = function(fileItem, progress) {
            //console.info('onProgressItem', fileItem, progress);
        };
        uploader.onProgressAll = function(progress) {
            //console.info('onProgressAll', progress);
        };
        uploader.onSuccessItem = function(fileItem, response, status, headers) {
            //  $scope.$emit('updateFileUploadEvent', $scope.uploader.queue);
            $rootScope.$broadcast('updateFileUploadEvent', $scope.uploader.queue);
            //console.log('queue ');
            //console.log($scope.uploader.queue);
            
        };
        uploader.onErrorItem = function(fileItem, response, status, headers) {

            if (status == 500 || status == 501 || status == 503)
            {
                if (fileItem.retryCount < $scope.retryLimit)
                {
                    fileItem.retryCount++;
                    uploader._xhrTransport(fileItem);
                }
                else
                {
                    fileItem.isExitRetryLimit = true;
                }
            }
            else if (status == 0)
            {
                $timeout(function() {
                    uploader._xhrTransport(fileItem);
                }, $scope.connectionTimeOutRetry);

            }
            else {
                fileItem.isExitRetryLimit = true;
            }
        };
        uploader.onCancelItem = function(fileItem, response, status, headers) {
            //console.info('onCancelItem', fileItem, response, status, headers);
        };
        uploader.onCompleteItem = function(fileItem, response, status, headers) {
            //console.info('onCompleteItem', fileItem, response, status, headers);
        };
        uploader.onCompleteAll = function() {
            //console.info('onCompleteAll');
        };

        $scope.cancel = function(index)
        {
            $scope.uploader.queue[index].remove();
            $rootScope.$broadcast('updateFileUploadEvent', $scope.uploader.queue);
        };
        $scope.cancelUploadedImage = function(index)
        {
            //$scope.imageList[index].remove();
            $rootScope.$broadcast('deletedUploadFileEvent', index);
        };

        $scope.onCaptionChange = function()
        {
            $rootScope.$broadcast('updateFileUploadEvent', $scope.uploader.queue);
        };


        $scope.$on('fileDeletedSuccessEvent', $scope.fileDeleteSuccessEventHandler);
        $scope.fileDeleteSuccessEventHandler = function($event, value)
        {

            console.log('$scope.fileDeleteSuccessEventHandler');
        }

    }])