angular.module('app')
        .directive('hcAreaChart', function() {
            return {
                restrict: 'E',
                template: '<div></div>',
                scope: {
                    title: '@',
                    data: '=',
                    categories: '='
                },
                link: function(scope, element) {
                    Highcharts.chart(element[0], {
                        chart: {
                            type: 'area',
                            height: 400,
                            width: 500
                        },
                        plotOptions: {
                            area: {
                                stacking: 'normal'
                            }
                        },
                        xAxis: {
                            lineWidth: 0,
                            minorGridLineWidth: 0,
                            lineColor: 'transparent',
                            labels: {
                                enabled: false
                            },
                            minorTickLength: 0,
                            tickLength: 0
                        },
//                        xAxis: {
//                            categories: scope.categories,
//                            tickmarkPlacement: 'on',
//                            title: {
//                                enabled: false
//                            }
//                        },
                        yAxis: {
                            title: {
                                text: 'Amount'
                            }
                        },
                        series: scope.data,
                        title: {
                            text: scope.title
                        },
//                        tooltip: {
////                            split: true,
////                            valueDecimals: 2,
////                            pointFormat: '<span style="color:{series.color}">{series.name}</span>:this.x <b>{point.y}</b> {point.change}<br/>',
//                            formatter: function() {
//
//                                var txt = '<span style="color:#D31B22;font-weight:bold;">' + this.series.name + ': ' + '<b>' + this.point.y + '</b>' + this.point.change + '<br/>' +
//                                        '<b style="color:#D31B22;font-weight:bold;">' + this.x + '</b><span>';
//                                return txt;
//                            }
//                        },
//                        tooltip: {
//                            pointFormat: '<span style="color:{series.color}">{series.name}</span>:{categories[0]} <b>{point.y}</b> {point.change}<br/>',
//                            valueDecimals: 2,
//                            split: true
//                        },
                        tooltip: {
                            split: true,
                            valueSuffix: ''
                        },
                        legend: {
                            align: 'center',
                            verticalAlign: 'top',
                            layout: 'horizontal',
                            x: 0,
                            y: 30
                        },
                    }
                    );
                }
            };
        });