angular.module('app')
        .directive('londonClinicLoader', ['$http', '$parse', function($http, $parse) {
                return {
                    restrict: 'E',
                    replace: true,
                    scope: {
                        blogName: '@'
                    },
                    controller: 'londonClinicLoaderCtrl',
                    templateUrl: '/js/components/london-clinic-loader/london-clinic-loader.html',
                    link: function($scope, elem, attrs) {


                        console.log('$scope.blogName =' + $scope.blogName);
                        if (typeof $scope.blogName !== 'undefined')
                        {
                            var requestParam = {};
                            requestParam.key = $scope.blogName;
                            requestParam.objName = $scope.urlpath;
                            requestParam.start = 0;
                            requestParam.limit = 1;

                            $scope.showloder = true;
                            $http({
                                method: 'GET',
                                url: '/staticContent/listAll',
                                params: requestParam,
                                config: {
                                    'key': $scope.blogName
                                }
                            }).success(function(data, status, headers, config) {
                                /*
                                 * Update the singed Url in facory instance
                                 */
                                if (data.data.list.length > 0)
                                {
                                    $scope.staticBlockModel.jsonContent = data.data.list[0].stringValue;
                                    if ($scope.staticBlockModel.jsonContent != '')
                                    {
                                        var result = angular.fromJson($scope.staticBlockModel.jsonContent);

                                        if (typeof result.list != 'undefined')
                                        {
                                            $scope.staticBlockModel.jsonList = result.list;
                                        }
                                    }

                                }
                                $scope.showloder = false;

                            }).error(function(data, status, headers, config) {

                            });

                        }


                    }

                }


            }]);