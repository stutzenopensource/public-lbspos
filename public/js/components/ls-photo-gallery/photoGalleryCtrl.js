app.controller('photoGalleryCtrl', ['$scope', '$timeout', function($scope, $timeout) {

        $scope.selected = "";
        $scope.selectedIndex = 0;


       
        $scope.listposition = {left: (0) + "px"};
        $scope.IMAGE_WIDTH = 600;
        $scope.imageArr = [];

        $scope.scrollTo = function(image, ind) {
            if (ind == -1)
            {
                $scope.mapInitFlag = true;

            }
            else
            {
                $scope.mapInitFlag = false;
            }

            $scope.listposition = {left: ($scope.IMAGE_WIDTH * (ind) * -1) + "px"};
            $scope.selected = image;
            $scope.selectedIndex = ind + 1;
        };
        $scope.scrollPrevTimeoutPromise = null;
        $scope.scrollPrev = function() {
            if ($scope.scrollPrevTimeoutPromise != null)
            {
                $timeout.cancel($scope.scrollPrevTimeoutPromise);
            }
            $scope.scrollPrevTimeoutPromise = $timeout(function() {

                if ($scope.selectedIndex == 1)
                {
                    $scope.mapInitFlag = true;

                }

                $scope.listposition = {left: (($scope.IMAGE_WIDTH * ($scope.selectedIndex) * -1) + $scope.IMAGE_WIDTH) + "px"};
                $scope.selectedIndex = $scope.selectedIndex - 1;

            }, 300);
        };
        $scope.scrollNextTimeoutPromise = null;
        $scope.scrollNext = function() {

            if ($scope.scrollNextTimeoutPromise != null)
            {
                $timeout.cancel($scope.scrollNextTimeoutPromise);
            }

            $scope.scrollNextTimeoutPromise = $timeout(function() {
                $scope.listposition = {left: (($scope.IMAGE_WIDTH * ($scope.selectedIndex) * -1) - $scope.IMAGE_WIDTH) + "px"};
                $scope.selectedIndex = $scope.selectedIndex + 1;

            }, 300);



        };

        $scope.download = function(imagePath)
        {
            window.open(imagePath, '_blank');
        }


    }]);