angular.module('app')
        .directive('staticBlockLoader', ['$http', '$parse', function($http, $parse) {
                return {
                    restrict: 'E',
                    replace: true,
                    scope: {
                        blogName: '@'
                    },
                    controller: 'staticBlockLoaderCtrl',
                    templateUrl: '/js/components/static-block-loader/static-block-loader.html',
                    link: function($scope, elem, attrs) {


                        console.log('$scope.blogName =' + $scope.blogName);
                        if (typeof $scope.blogName !== 'undefined')
                        {
                            var requestParam = {};
                            requestParam.key = $scope.blogName;
                            requestParam.objName = $scope.urlpath;
                            requestParam.start = 0;
                            requestParam.limit = 1;

                            $scope.showloder = true;
                            $http({
                                method: 'GET',
                                url: '/staticContent/listAll',
                                params: requestParam,
                                config: {
                                    'key': $scope.blogName
                                }
                            }).success(function(data, status, headers, config) {
                                /*
                                 * Update the singed Url in facory instance
                                 */
                                if (data.data.list.length > 0)
                                {
                                    $scope.staticBlockModel.htmlContent = data.data.list[0].stringValue;
                                }
                                $scope.showloder = false;

                            }).error(function(data, status, headers, config) {

                            });

                        }


                    }

                }


            }]);