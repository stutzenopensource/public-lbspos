LANG = {

    'tour_step1_title': 'Guide Tour',
    'tour_step1_content': "Hi <p>{{userModel.f_name}}</p>,",
    'tour_step2_title': 'App Settings',
    'tour_step2_content': '<b>Step 1:</b> <br>Let us start by customising Accozen for your business needs. Edit the settings based on your requirements. App settings lets you modify your POS settings. You can enable dual language support here and change everything related to your business.',
    'tour_step3_title': 'Product Add',
    'tour_step3_content': '<b>Step 2:</b> <br>It is time to stock up your inventory. Enter one of your product details here to open your inventory. You can add your product details either fresh or can select from the drop-down menu. You can add products at any point of time during sales.',
    'tour_step4_title': ' Customer Add',
    'tour_step4_content': '<b>Step 3:</b> <br>Let us add your first customer and their details in here to create invoices. It will be easy to create invoices if you have customers added already. It is mandatory to add customers to generate invoices.',
    'tour_step5_title': ' Vendor Add',
    'tour_step5_content': '<b>Step 4:</b> <br>Let us add your first customer and their details in here to create invoices. It will be easy to create invoices if you have customers added already. It is mandatory to add customers to generate invoices.',
    'tour_step6_title': ' Payment Mode',
    'tour_step6_content': '<b>Step 5:</b> <br>Manage the different methods of payments by having them added here. You can use cash, card or on account method to initiate payment. On account method, lets you maintain credits to customers on an account basis.',
    'tour_step7_title': 'Account Add',
    'tour_step7_content': '<b>Step 6: </b> <br>Maintain all of your bank account or other account details in here. Try adding one now. You need not wander everywhere to know your bank account details.',
    'tour_step8_title': ' Adding Purchase Invoice',
    'tour_step8_content': '<b>Step 7:</b> <br>There you go, create your first purchase order invoice.',
    'tour_step9_title': 'Adding Employee',
    'tour_step9_content': '<b>Step 8: </b>You can’t daily do all of this task, so add your manager or the best employee details in here and give access to them. Every employee must possess a unique code. You can also restrict their usage to the screens you dont want them to see',
    'tour_step10_title': 'Adding Sales Invoice',
    'tour_step10_content': '<b>Step 9: </b> <br>Finally, we will create the first invoice of your sales now. Add the product and the customer details here. Barcode scanner will add product details and add quantities manually.',
    'label_no_product_error': 'Please, add atleast one product',
    'confirm_delete_customer_group': 'This customer group will be deleted.',
    'prev': 'Previous',
    'next': 'Next',
    'end_tour': 'End tour',
    "first": "First",
    "last": "Last",
    "previous": "Previous",    
};