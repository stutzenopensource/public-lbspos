app.service("bookingService", ['$rootScope', '$state', '$httpService', '$googleMapService', '$cookieService', 'APP_CONST', '$window', '$location', function($rootScope, $state, $httpService, $googleMapService, $cookieService, APP_CONST, $window, $location) {

        var bookingService = this;
        this.bookingModel = {
            staffId: "",
            clinicId: "",
            specialistId: "",
            startDate: "",
            endDate: "",
            limit: 7,
            list: [],
            uiList: [],
            isLoadingProgress: true,
            //selected time value
            date: '',
            startTime: '',
            endTime: '',
            duration: '',
            selectedTimeSlotVlaue: ''
        };


        this.getFreeSlot = function(data)
        {
            return $httpService.get(APP_CONST.API.FREESLOT_LIST, data, false);
        };
        this.saveAppointment = function(data)
        {
            return $httpService.post(APP_CONST.API.STAFF_APPOINTMENT_BOOKING, data, true);
        }


        return bookingService;
    }]);




