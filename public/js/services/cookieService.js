app.service('$cookieService', ['$cookies', function($cookies) {

        var $cookieService = this;

        this.setCookie = function(key, value) {

            var cookieOption = {
                path: "/"
            };
            $cookies.put(key, value, cookieOption);

        };

        this.getCookie = function(key) {

            var retValue = null;

            if (typeof $cookies.get(key) != 'undefined')
            {
                retValue = $cookies.get(key);
            }

            return retValue;
        };

        this.removeCookie = function(key) {
            var cookieOption = {
                path: "/"
            };
            $cookies.remove(key, cookieOption);
        };


        this.hasCookie = function(key) {

            var retValue = false;

            if (typeof $cookies.get(key) != 'undefined')
            {
                retValue = true;
            }

            return retValue;
        };

        return $cookieService
    }]);
