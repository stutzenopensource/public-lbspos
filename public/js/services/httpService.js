
app.service("$httpService", ['$http', '$cookies', '$q', function ($http, $cookies, $q) {

            var $httpService = this;

            var stutzenHttpService = {};
            var deferred = $q.defer();
            var pending = false;

            this.get = function (url, data, handleResponse, config, header, abortFlag) {
                  if (pending) {
                        deferred.resolve();
                        deferred = $q.defer();
                  }
                  pending = true;
                  if (typeof abortFlag == 'undefined' || abortFlag == null)
                  {
                        abortFlag = false;
                  }
                  if (typeof handleResponse == 'undefined') {
                        handleResponse = false;
                  }
                  var configOption = {};
                  configOption.handleResponse = handleResponse;
                  if (config != null)
                  {
                        angular.extend(configOption, config);
                  }
                  var headerOption = {};

                  if (header != null)
                  {
                        angular.extend(headerOption, header);
                  }

                  var cookieData = null;
                  if (typeof $cookies.get("api-token") != 'undefined')
                  {
                        cookieData = $cookies.get("api-token");
                  }
                  headerOption['api-token'] = cookieData;
                  if (abortFlag == true)
                  {
                        return $http({
                              method: 'GET',
                              url: url,
                              params: data,
                              headers: headerOption,
                              config: configOption,
                              timeout: deferred.promise

                        }).success(function (data, status, headers, config) {

                              console.log(deferred.promise);
                              pending = false;
                              deferred = $q.defer();
                              console.log("Pending Requests:+++++++++++" + $http.pendingRequests);
                              return data;
                        }).error(function (data, status, headers, config) {

                              pending = false;
                              deferred = $q.defer();
                              return data;
                        });
                  } else
                  {
                        return $http({
                              method: 'GET',
                              url: url,
                              params: data,
                              headers: headerOption,
                              config: configOption
                        }).success(function (data, status, headers, config) {

                        }).error(function (data, status, headers, config) {

                        });
                  }
            }

            this.put = function (url, data, handleResponse, config, header) {
                  if (typeof handleResponse == 'undefined') {
                        handleResponse = false;
                  }
                  var configOption = {};
                  configOption.handleResponse = handleResponse;
                  if (config != null)
                  {
                        angular.extend(configOption, config);
                  }
                  var headerOption = {};

                  if (header != null)
                  {
                        angular.extend(headerOption, header);
                  }

                  var cookieData = null;
                  if (typeof $cookies.get("api-token") != 'undefined')
                  {
                        cookieData = $cookies.get("api-token");
                  }
                  headerOption['api-token'] = cookieData;
                  return $http({
                        method: 'PUT',
                        url: url,
                        data: angular.toJson(data, 0),
                        headers: headerOption,
                        config: configOption
                  }).success(function (data, status, headers, config) {

                  }).error(function (data, status, headers, config) {

                  });
            }

            this.post = function (url, data, handleResponse, config, header) {
                  if (typeof handleResponse == 'undefined') {
                        handleResponse = false;
                  }
                  var configOption = {};
                  configOption.handleResponse = handleResponse;
                  if (config != null)
                  {
                        angular.extend(configOption, config);
                  }
                  var headerOption = {};

                  if (header != null)
                  {
                        angular.extend(headerOption, header);
                  }

                  var cookieData = null;
                  if (typeof $cookies.get("api-token") != 'undefined')
                  {
                        cookieData = $cookies.get("api-token");
                  }
                  headerOption['api-token'] = cookieData;
                  return $http({
                        method: 'POST',
                        url: url,
                        data: angular.toJson(data, 0),
                        headers: headerOption,
                        config: configOption
                  }).success(function (data, status, headers, config) {

                  }).error(function (data, status, headers, config) {

                  });
            }
            this.delete = function (url, data, handleResponse, config, header)
            {
                  if (typeof handleResponse == 'undefined') {
                        handleResponse = false;
                  }
                  var configOption = {};
                  configOption.handleResponse = handleResponse;
                  if (config != null)
                  {
                        angular.extend(configOption, config);
                  }
                  var headerOption = {};

                  if (header != null)
                  {
                        angular.extend(headerOption, header);
                  }

                  var cookieData = null;
                  if (typeof $cookies.get("api-token") != 'undefined')
                  {
                        cookieData = $cookies.get("api-token");
                  }
                  headerOption['api-token'] = cookieData;
                  return $http({
                        method: 'DELETE',
                        url: url,
                        data: angular.toJson(data, 0),
                        headers: headerOption,
                        config: configOption
                  }).success(function (data, status, headers, config) {
                  }).error(function (data, status, headers, config) {
                  });
            }

            this.formPost = function (url, data, handleResponse) {
                  if (typeof handleResponse == 'undefined') {
                        handleResponse = false;
                  }
                  var configOption = {};
                  configOption.handleResponse = handleResponse;
                  var cookieData = null;
                  if (typeof $cookies.get("api-token") != 'undefined')
                  {
                        cookieData = $cookies.get("api-token");
                  }
                  return $http({
                        method: 'POST',
                        url: url,
                        headers: {
                              'api-token': cookieData
                        },
                        transformRequest: function (obj) {
                              var str = [];
                              for (var p in obj)
                                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                              return str.join("&");
                        },
                        data: data,
                        config: configOption

                  }).success(function (data, status, headers, config) {

                  }).error(function (data, status, headers, config) {

                  });

            }
            return $httpService;

      }]);




