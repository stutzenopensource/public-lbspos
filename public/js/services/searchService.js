app.service("$searchService", ['$rootScope', '$state', '$googleMapService', '$cookieService', 'APP_CONST', '$window', '$location', function($rootScope, $state, $googleMapService, $cookieService, APP_CONST, $window, $location) {

        var $searchService = this;
        /*
         * Indicate the search loading is progress
         *
         * @type Boolean
         */
        this.isSearchProcessing = false;

        //Default geoAddress
        this.defaultGeoAddress = "United Kingdom";

        //Default geoCode
        this.defaultGeoCode = {
            lat: 51.5073509,
            lng: -0.1277583,
            hasDefault: true
        }

        //Default Radius Search Value
        this.defaultRadius = 5;

        //Default Radius Search Value
        this.defaultSort = '';

        this.searchModel = {
            userType: '',
            filter: '',
            keyword: '',
            lat: '',
            lng: '',
            radius: '',
            langsupport: "",
            insurer: "",
            patientsAge: [],
            availability: [],
            availSlot: [],
            gp: [],
            expmin: '',
            expmax: '',
            gender: [],
            sort: this.defaultSort
        };

        this.searchUIModel = {
            selectedGeoAddress: ''
        };
        //UI bind varibale
        //this.selectedUserTypeCaption = "";
        //this.selectedGeoAddress = ""

        //It used to hold the search data
        this.searchResult = {};

        this.getUserType = function() {

            return this.searchModel.userType;
        };
        this.setUserType = function(value) {

            this.searchModel.userType = value;
        };

        this.clearFacetValue = function() {

            this.searchModel.langsupport = "";
            this.searchModel.insurer = "";
            this.searchModel.expmin = "";
            this.searchModel.expmax = "";
            this.searchModel.patientsAge = [];
            this.searchModel.availability = [];
            this.searchModel.availSlot = [];
            this.searchModel.gp = [];
            this.searchModel.gender = [];
            if (typeof this.searchResult.facets_stats !== 'undefined' && typeof this.searchResult.facets_stats.experience !== 'undefined')
            {
                this.searchResult.facets_stats.experience.from = this.searchResult.facets_stats.experience.min;
                this.searchResult.facets_stats.experience.to = this.searchResult.facets_stats.experience.max;
            }
        };

        /*
         * It used to find the geocode point for given address,
         *
         * Update the geocode value in search service
         *
         */
        this.resolveLocationGeocode = function()
        {

            if (this.searchUIModel.selectedGeoAddress == "")
            {
                return;
            }

            var geocodeParam = {};
            geocodeParam.address = this.searchUIModel.selectedGeoAddress;
            geocodeParam.key = APP_CONST.API.GOOGLE_API_KEY;

            $googleMapService.getGeoCode(geocodeParam).then(function(responseData) {
                var data = responseData.data;
                if (data.results.length > 0)
                {
                    var searchMapLocationGecodeDetail = data.results[0].geometry.location;
                    $searchService.searchModel.lat = searchMapLocationGecodeDetail['lat'];
                    $searchService.searchModel.lng = searchMapLocationGecodeDetail['lng'];
                }
                else
                {
                    $searchService.searchModel.lat = '';
                    $searchService.searchModel.lng = '';
                }
            });
        }

        /*
         * It used to find the address for geocode point,
         *
         * And update the geocode Address in searchService.
         *
         */
        this.resolveLocationAddress = function()
        {

            var reverseGeocodeParam = {};
            reverseGeocodeParam.latlng = this.searchModel.lat + ',' + this.searchModel.lng;
            reverseGeocodeParam.key = APP_CONST.API.GOOGLE_API_KEY;

            $googleMapService.getLocationAddress(reverseGeocodeParam).then(function(responseData) {
                var data = responseData.data;
                if (data.results.length > 0)
                {
//                    if ($searchService.searchUIModel.selectedGeoAddress != '')
//                    {
//                        //Fix to sovle the component bindable watch issue.
//                        $searchService.searchUIModel.selectedGeoAddress = $searchService.searchUIModel.selectedGeoAddress + ' ';
//                    }
                    if ($cookieService.hasCookie('geoaddress'))
                    {
                        $searchService.searchUIModel.selectedGeoAddress = $cookieService.getCookie('geoaddress');
                        $cookieService.removeCookie('geoaddress')
                    }
                    else
                    {
                        $searchService.searchUIModel.selectedGeoAddress = '';
                        console.log('$searchService.searchUIModel.selectedGeoAddress = ' + $searchService.searchUIModel.selectedGeoAddress);
                        $searchService.searchUIModel.selectedGeoAddress = data.results[0].formatted_address;
                        console.log('data.results[0].formatted_address;' + data.results[0].formatted_address);
                        console.log('$searchService.searchUIModel.selectedGeoAddress = ' + $searchService.searchUIModel.selectedGeoAddress);
                        //$searchService.searchUIModel.selectedGeoAddress = 'stutzen';


                    }
                }
                else
                {
                    $searchService.searchUIModel.selectedGeoAddress = '';
                }

            });
        }

        this.updateGeocode = function(geoCode)
        {
            this.searchModel.lat = geoCode.lat;
            this.searchModel.lng = geoCode.lng;
            this.resolveLocationAddress();
        }

        this.updateDefaultGeocode = function()
        {

//            $searchService.searchModel.lat = $searchService.defaultGeoCode.lat;
//            $searchService.searchModel.lng = $searchService.defaultGeoCode.lng;
//            $searchService.selectedGeoAddress = $searchService.defaultGeoAddress;

            this.searchModel.lat = $searchService.defaultGeoCode.lat;
            this.searchModel.lng = $searchService.defaultGeoCode.lng;
            this.resolveLocationAddress();
        }


        this.locateMeHandler = function() {
            var geocode = {};
            if (navigator.geolocation) {

                navigator.geolocation.getCurrentPosition(function(position) {
                    geocode.lat = position.coords.latitude;
                    geocode.lng = position.coords.longitude
                    $searchService.updateGeocode(geocode);
                    //$rootScope.$broadcast('updateGeocodeEvent', geocode);

                }, function() {
                    //$rootScope.$broadcast('updateGeocodeEvent', $searchService.defaultGeoCode);
                    $searchService.updateDefaultGeocode();
                });
            }
            else
            {
                //$rootScope.$broadcast('updateGeocodeEvent', $searchService.defaultGeoCode);
                $searchService.updateDefaultGeocode();
            }
        };


        this.search = function()
        {
            this.isSearchProcessing = true;
            var searchPageUrl = $window.location.protocol + "//" + $window.location.host + "/app/search/#/";
            var paramkey = '';
            var filterType = 'clinic'
            if (this.searchModel.userType != "")
            {
                filterType = this.searchModel.userType;
            }

            if (this.searchUIModel.selectedGeoAddress != '')
            {
                paramkey += (paramkey != '' ? '-' : '') + this.searchModel.lat + "-" + this.searchModel.lng;
            }
            else
            {
                $cookieService.removeCookie('geoaddress');
                this.searchModel.lat = '';
                this.searchModel.lng = '';

            }

            if (this.searchModel.radius != '')
            {
                paramkey += (paramkey != '' ? '-' : '') + this.searchModel.radius;
            }
            else
            {
                paramkey += (paramkey != '' ? '-' : '') + this.defaultRadius;
            }
            if (this.searchModel.keyword != '')
            {
                paramkey += (paramkey != '' ? '-' : '') + this.searchModel.keyword;
            }
            var absURL = '' + $location.absUrl();
            if (absURL.indexOf(searchPageUrl) != -1)
            {
                this.showSearForm = false;
                $rootScope.skipReloadPage = true;
                $state.go('result', {'filtertype': filterType, 'filter': paramkey, 'perPage': '10', 'page': '1'});
                this.isSearchProcessing = false;
                this.showSearForm = true;
                //$rootScope.$broadcast('updateSearchEvent', '');
            }
            else
            {
                if (this.searchUIModel.selectedGeoAddress != '')
                {
                    $cookieService.setCookie('geoaddress', this.searchUIModel.selectedGeoAddress);
                }
                $window.location.href = searchPageUrl + filterType + "?filter=" + paramkey + "&perPage=10&page=1";
            }
        };

        return $searchService;
    }]);




