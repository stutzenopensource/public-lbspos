app.service('utilityService', ['$cookies', '$filter', function($cookies, $filter) {

        var utilityService = this;
        this.parseSqlDate = function(value) {

            var retVal = '';
            if (value != '')
            {
                value = new Date(value);
                retVal = $filter('date')(value, 'MM-dd-yyyy');
            }
            return retVal;
        };
        this.parseDateToStr = function(value, format) {

            var retVal = '';
            if (value != '')
            {
                value = new Date(value);
                retVal = $filter('date')(value, format);
            }
            return retVal;
        };
        this.changeDateToSqlFormat = function(value, format) {
            var format = format.toUpperCase();
            var retVal = '';
            if (value != '')
            {
                retVal = moment(value, format).format('YYYY-MM-DD');
            }
            return retVal;
        };
        this.parseStrToDate = function(value) {

            var retVal = '';
            if (value != '')
            {
                retVal = new Date(value);
            }
            return retVal;
        };
        this.browserPrint = function() {
            window.print();
        };
        this.currentDate = function()
        {
            var date = moment();
            console.log('date long = ' + date.valueOf());
            var dateStr = date.utc().format('YYYY-MM-DD');
            var retVal = moment(dateStr + 'T00:00:00+00:00');
            console.log('date long1 = ' + retVal.valueOf());
            return retVal;
        }

        this.getWeekDay = function(date)
        {
            var dat = moment(date);
            return dat.format('dddd');
        }

        this.incrementDate = function(date, incrementDay) {
            var retVal = moment(date).add(incrementDay, 'day');
            var dateStr = retVal.format('YYYY-MM-DD');
            retVal = moment(dateStr + 'T00:00:00+00:00');
            return retVal;
        }

        this.decrementDate = function(date, decrementDay) {
            var retVal = moment(date).subtract(decrementDay, 'day');
            var dateStr = retVal.format('YYYY-MM-DD');
            retVal = moment(dateStr + 'T00:00:00+00:00');
            return retVal;
        }

        this.convertToUTCDate = function(date) {
            var retVal = moment(date);
            var dateStr = retVal.format('YYYY-MM-DD');
            retVal = moment(dateStr + 'T00:00:00+00:00');
            return retVal;
        }
        this.convertNumberToWords = function(amount) {
            var words = new Array();
            words[0] = '';
            words[1] = 'One';
            words[2] = 'Two';
            words[3] = 'Three';
            words[4] = 'Four';
            words[5] = 'Five';
            words[6] = 'Six';
            words[7] = 'Seven';
            words[8] = 'Eight';
            words[9] = 'Nine';
            words[10] = 'Ten';
            words[11] = 'Eleven';
            words[12] = 'Twelve';
            words[13] = 'Thirteen';
            words[14] = 'Fourteen';
            words[15] = 'Fifteen';
            words[16] = 'Sixteen';
            words[17] = 'Seventeen';
            words[18] = 'Eighteen';
            words[19] = 'Nineteen';
            words[20] = 'Twenty';
            words[30] = 'Thirty';
            words[40] = 'Forty';
            words[50] = 'Fifty';
            words[60] = 'Sixty';
            words[70] = 'Seventy';
            words[80] = 'Eighty';
            words[90] = 'Ninety';
            amount = amount.toString();
            var atemp = amount.split(".");
            var number = atemp[0].split(",").join("");
            var n_length = number.length;
            var words_string = "";
            if (n_length <= 9) {
                var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
                var received_n_array = new Array();
                for (var i = 0; i < n_length; i++) {
                    received_n_array[i] = number.substr(i, 1);
                }
                for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
                    n_array[i] = received_n_array[j];
                }
                for (var i = 0, j = 1; i < 9; i++, j++) {
                    if (i == 0 || i == 2 || i == 4 || i == 7) {
                        if (n_array[i] == 1) {
                            n_array[j] = 10 + parseInt(n_array[j]);
                            n_array[i] = 0;
                        }
                    }
                }
                var value = "";
                for (var i = 0; i < 9; i++) {
                    if (i == 0 || i == 2 || i == 4 || i == 7) {
                        value = n_array[i] * 10;
                    } else {
                        value = n_array[i];
                    }
                    if (value != 0) {
                        words_string += words[value] + " ";
                    }
                    if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
                        words_string += "Crores ";
                    }
                    if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
                        words_string += "Lakhs ";
                    }
                    if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
                        words_string += "Thousand ";
                    }
                    if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
                        words_string += "Hundred and ";
                    } else if (i == 6 && value != 0) {
                        words_string += "Hundred ";
                    }
                }
                words_string = words_string.split("  ").join(" ");
            }
            return words_string;
        }
        this.changeCurrency = function(x, format) {
            if(typeof x == undefined || x == null || isNaN(x))
            {
                return 0;
            }
            if (format == 'dd,dd,dd,ddd')
            {
                var data = '';
                data = x.toString();
                var afterPoint = '';
                if (data.indexOf('.') > 0)
                    afterPoint = data.substring(data.indexOf('.'), data.length);
                data = Math.floor(data);
                data = data.toString();
                var lastThree = data.substring(data.length - 3);
                var otherNumbers = data.substring(0, data.length - 3);
                if (otherNumbers != '')
                    lastThree = ',' + lastThree;
                var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
                return res;
            }
            else if (format == 'ddd,ddd,ddd')
            {
                var parts = x.toString().split(".");
                return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
            }
            else if (format == 'd,dddd,dddd')
            {
                var parts = x.toString().split(".");
                return parts[0].replace(/\B(?=(\d{4})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
            }
            else if (format == 'ddd ddd ddd')
            {
                var parts = x.toString().split(".");
                return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ") + (parts[1] ? "." + parts[1] : "");
            }
            else if (format == 'd dddd dddd')
            {
                var parts = x.toString().split(".");
                return parts[0].replace(/\B(?=(\d{4})+(?!\d))/g, " ") + (parts[1] ? "." + parts[1] : "");
            }
        };

        return utilityService;
    }]);
