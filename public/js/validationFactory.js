app.factory('ValidationFactory', ['$filter', function ($filter) {

    var validationFactory = {};
    validationFactory.emailValidator = function (email) {

        if (!email) {
            return true;
        }
        var re = /^([a-z0-9,!\#\$%&'\*\+\/=\?\^_`\{\|\}~-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z0-9,!\#\$%&'\*\+\/=\?\^_`\{\|\}~-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*@([a-z0-9-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z0-9-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*\.(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]){2,})$/i;
        return re.test(email);
    };

    validationFactory.phoneNumberValidator = function (phoneNumber) {
        if (!phoneNumber) {
            return;
        }
        var re = /^(\+|\d)[\d]{5,}\d*$/;
        if (!re.test(phoneNumber))
        {
            return "Please enter a valid phone number. "
        }
        return true;
    };

    validationFactory.mobileValidator = function (mobileNumber) {
        if (!mobileNumber) {
            return;
        }
//        var re =/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
        var re = /^\+?\d{2}\d{3}\d{5}$/;
        if (!re.test(mobileNumber))
        {
            return "Please enter a valid phone number. "
        }
        return true;
    };


    validationFactory.hasEmpty = function (value) {
        var retVal = false;
        if (value == null || value === '') {
            retVal = true;
        }
        return retVal;
    };
    validationFactory.urlValidator = function (value) {

        var re = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
        if (!re.test(value)) {
            return false;
        } else
        {
            return true;
        }
    };
    validationFactory.integerValidator = function (no) {
        if (!no) {
            return;
        }
        var re = /^[0-9/\s]*$/;
        if (!re.test(no))
        {
            return "Enter valid integer. "
        }
        return true;
    }

    validationFactory.passwordValidator = function (password) {

        if (!password) {
            return;
        }
        var re = /\s/;
        if (re.test(password)) {
            return "Password should not contain space.";
        }

        if (password.length < 6) {
            return "Password must be at least " + 6 + " characters long";
        }
        return true;
    };
    validationFactory.isNumber = function (value) {

        var retVal = true;
        if (isNaN(value)) {
            retVal = false;
        }
        return retVal;
    };
    validationFactory.isPositiveNumber = function (value) {

        var retVal = false;
        if (!value)
        {
            retVal = true;
            return retVal;
        }
        if (!isNaN(value) && value >= 0) {
            retVal = true;
        }
        return retVal;
    };
    validationFactory.hasParam = function (paramName, value) {

        var retVal = true;
        if (typeof value[paramName] != 'undefined' && value[paramName] == '') {
            retVal = false;
        }
        return retVal;
    };
    validationFactory.isAlbhaNumeric = function (value) {

        var retVal = true;
        if (!value)
        {
            return retVal;
        }
        var re = /^[a-zA-Z0-9/\s]*$/;
        if (!re.test(value))
        {
            retVal = false;
        }
        return retVal;
    };
    validationFactory.isAlbha = function (value) {

        var retVal = true;
        if (!value)
        {
            return retVal;
        }
        var re = /^[a-zA-Z/\s]*$/;
        if (!re.test(value))
        {
            retVal = false;
        }
        return retVal;
    };
    validationFactory.isAlbhaNumericWithSpecialChar = function (value) {

        var retVal = true;
        if (!value)
        {
            return retVal;
        }
        var re = /^[a-zA-Z0-9\-\_\//\s]*$/;
        if (!re.test(value))
        {
            retVal = false;
        }
        return retVal;
    };
    validationFactory.minDate = function (minDate, value) {

        var retVal = true;
        if (minDate != null && value != null)
        {
            var minDateValue = $filter('date')(minDate, 'yyyy-MM-dd');
            var value = $filter('date')(value, 'yyyy-MM-dd');
            minDateValue = moment(minDateValue).valueOf();
            value = moment(value).valueOf();
            if (value < minDateValue)
            {
                retVal = false;
            }
        }
        return retVal;
    };
    validationFactory.maxDate = function (maxDate, value) {

        var retVal = true;
        if (maxDate != null && value != null)
        {
            var maxDateValue = $filter('date')(maxDate, 'yyyy-MM-dd');
            var value = $filter('date')(value, 'yyyy-MM-dd');
            maxDateValue = moment(maxDateValue).valueOf();
            value = moment(value).valueOf();
            if (value > maxDateValue)
            {
                retVal = false;
            }
        }
        return retVal;
    };
    validationFactory.minNumber = function (minValue, value) {

        var retVal = true;
        if (isNaN(minValue) || isNaN(value)) {
            retVal = false;
        }
        if (parseInt(value) > parseInt(minValue))
        {
            retVal = false;
        }


        return retVal;
    };
    validationFactory.maxNumber = function (maxValue, value) {

        var retVal = true;
        if (isNaN(maxValue) || isNaN(value))
        {
            retVal = false;
        }
        if (value < maxValue)
        {
            retVal = false;
        }
        return retVal;
    };
    validationFactory.timeFormat = function (value, format) {

        var retVal = true;
        if (!value)
        {
            return retVal;
        }
        if (format === 24)
        {
            var re = /^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/;
            if (!re.test(value))
            {
                retVal = false;
            }
        }
        return retVal;
    };
    validationFactory.hasRequired = function (value) {

        var retVal = false;
        if (!value)
        {
            return retVal;
        }
        if (value != '')
        {
            retVal = true;
        }
        return retVal;
    };
    validationFactory.postCodeValidate = function (value) {

        var retVal = true;
        if (!value)
        {
            return retVal;
        }
        var re = /^[0-9/\s][a-zA-Z0-9\-\_\//\s]*$/;
        if (!re.test(value))
        {
            retVal = false;
        }
        return retVal;
    };
    validationFactory.isGSTNumber = function (value) {
        var retVal = true;
        var re = /^([0][1-9]|[1-2][0-9]|[3][0-5])([a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9a-zA-Z]{1}[zZ]{1}[0-9a-zA-Z]{1})+$/;
        if (!re.test(value))
        {
            retVal = false;
        }
        return retVal;
    };


    validationFactory.isMultiPhoneNumber = function (value) {
        var retVal = true;
        if (!value)
        {
            return retVal;
        }
        var re = /^[0-9,\s]*$/;
        if (!re.test(value))
        {
            retVal = false;
        }
        return retVal;
    };

    return validationFactory;
}]);