/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
{
"global_language_en" : "English",
        "global_language_tl" : "Tamil",
        "customer" : "Customer",
        "purchase" : "Purchase",
        "purchase_order" : "Purchase Order",
        "purchase_invoice" : "Purchase Invoice",
        "purchase_payment" : "Purchase Payment",
        "sales" : "Sales",
        "sales_estimate" : "Sales Estimate",
        "sales_invoice" : "Sales Invoice",
        "sales_payment" : "Sales Payment",
        "inventory" : "Inventory",
        "inventory_stock" : "Inventory Stock",
        "stock_adjustment" : "Stock Adjustment",
        "stock_wastage" : "Stock Wastage",
        "stock_trace" : "Stock Trace",
        "bank_and_cash" : "Bank & Cash",
        "account_list" : "Account List",
        "account_balance" : "Account Balance",
        "account" : "Account",
        "deposit" : "Deposit",
        "expense" : "Expense",
        "journal_entry" : "Journal Entry",
        "debit_note" : "Debit Note",
        "credit_note" : "Credit Note",
        "product_and_service" : "Product & Service",
        "uom" : "UOM",
        "category" : "Category",
        "users" : "Users",
        "tax_list" : "Tax List",
        "tax_group" : "Tax Group",
        "income_division" : "Income Division",
        "expense_division" : "Expense Division",
        "payment_terms" : "Payment Terms",
        "city" : "City",
        "state" :  "State",
        "country" : "Country",
        "action" : "Action",
        "customers_list" : "Customers List",
        "name" : "Name",
        "company_name" : "Company Name",
        "email" : "Email",
        "phone" : "Phone",
        "billing_address" : "Billing Address",
        "shipping_address" : "Shipping Address",
        "post_code" : "Post Code",
        "purchase" : "Purchase",
        "sales" : "Sales",
        "vendor_name" : "Vendor Name",
        "advance_amount" : "Advance Amount",
        "total_amount" : "Total Amount",
        "order_amount" : "Order Amount",
        "order_date:" : "Order Date:",
        "valid_date" : "Valid Date",
        "status" : "Status",
        "quantity" : "Qty",
        "price" : "Price",
        "row_total" : "Row Total",
        "discount" : "Discount",
        "sub_total:" : "Sub Total:",
        "used_amount" : "Used Amount",
        "refresh" : "Refresh",
        "download_csv" : "Download CSV",
        "add_new" : "Add New",
        "id" : "ID",
        "show" : "Show",
        "billing_country" : "Billing Country",
        "billing_state" : "Billing State",
        "billing_city" : "Billing City",
        "billing_postcode" : "Billing Postcode",
        "is_shipping_address_same_as_billing_address" : "Is Shipping Address Same As Billing Address",
        "shipping_country" : "Shipping Country",
        "shipping_state" : "Shipping State",
        "shipping_city" : "Shipping City",
        "customer_number" : "Customer Number",
        "customer_add" : "Customer Add",
        "back_to_customer_list" : "Back to customer list",
        "save" : "Save",
        "date" : "Date",
        "tax" : "Tax",
        "grand_total" : "Grand Total",
        "discount_mode" : "Discount Mode",
        "discount_amount" : "Discount Amount",
        "amount_due" : "Amount Due",
        "paid_amount" : "Paid Amount",
        "invoice_code" : "Invoice Code",
        "total" : "Total",
        "invoice_date" : "Invoice Date",
        "purchase_invoice_add" : "Purchase Invoice Add",
        "invoice_number" : "Invoice No",
        "payment_terms" : "Payment Terms",
        "amount" : "Amount",
        "balance_amount:" : "Balance Amount:",
        "voucher_type" : "Voucher Type",
        "customer_name" : "Customer Name",
        "payment_date" : "Payment Date",
        "accounts" : "Accounts",
        "comments" : "Comments",
        "order_number:" : "Order No:",
        "bill_amount:" : "Bill Amount:",
        "contact" : "Contact",
        "sales_estimate_add" : "Sales Estimate Add",
        "print" : "Print",
        "edit" : "Edit",
        "payment_due:" : "Payment Due:",
        "due_amount" : "Due Amount",
        "balance_due_amount:" : "Balance Due Amount:",
        "invoice_activity" : "INVOICE ACTIVITY",
        "invoice_code" : "Invoice Code",
        "notes" : "Notes",
        "used_amount" : "Used Amount",
        "balance_amount" : "Balance Amount",
        "voucher_type" : "Voucher Type",
        "code" : "Code",
        "sales_order_date" : "Sales Order Date",
        "cancel" : "Cancel",
        "mode" : "Mode",
        "select_payment_mode" : "Select Payment Mode",
        "select_accounts" : "Select Accounts",
        "received_amount" : "Received Amount",
        "all_status" : "All Status",
        "new" : "New",
        "pending" : "Pending",
        "invoiced" : "Invoiced",
        "due_date" : "Due Date",
        "quick_customer_add" : "Quick Customer Add",
        "level" : "Level",
        "level_req" : "Level*",
        "submit" : "Submit",
        "reset" : "Reset",
        "sales_order" : "Sales order",
        "sales_order_add" : "Sales Order Add",
        "inventory_list" : "Inventory List",
        "select" : "Select",
        "all" : "All",
        "search" : "Search",
        "minimum_stock" : "Minimum Stock",
        "product_name" : "Product Name",
        "stock_adjustment_add" : "Stock Adjustment Add",
        "total_stock" : "Total Stock",
        "stock_adjustment_edit" : "Stock Adjustment Edit",
        "stock_wastage_add" : "Stock Wastage Add",
        "stock_wastage_edit" : "Stock Wastage Edit",
        "minimum_quantity" : "Minimum Quantity",
        "current_stock" : "Current Stock",
        "uom_name" : "UOM Name",
        "quantity(+)" : "Quantity(+)",
        "quantity(-)" : "Quantity(-)",
        "description": "Description",
        "account_name" : "Account Name",
        "account_number" : "Account No",
        "do_you_want_to_delete?" : "Do you want to delete?",
        "ok" : "OK",
        "account_add" : "Account Add",
        "back_to_account_list" : "Back to Account  List",
        "initial_balance_required" : "Initial Balance*",
        "contact_person" : "Contact Person",
        "internet_banking_URL" : "Internet Banking URL",
        "account_edit" : "Account Edit",
        "balance" : "Balance",
        "particular" : "Particular",
        "created_by" : "Created_by",
        "deposit_add" : "Deposit Add",
        "choose_an_account" : "Choose an Account",
        "deposit_edit" : "Deposit Edit",
        "deposit_details" : "Deposit Details",
        "credit_account" : "Credit Account",
        "debit_account" : "Debit Account",
        "expense_add" : "Expense Add",
        "back_to_expense_list" : "Back to Expense  List",
        "expense_edit" : "Expense Edit",
        "expense_details" : "Expense Details",
        "units_of_measurement" : "Units of Measurement",
        "note" : "Note",
        "category_list" : "Category List",
        "category_add" : "Category Add",
        "back_to_category_list" : "Back to Category  List",
        "category_edit" : "Category Edit",
        "users_list" : "Users List",
        "user_invite" : "User Invite",
        "user_name" : "Username",
        "user_role" : "User Role",
        "email_address" : "Email Address",
        "invite" : "Invite",
        "user_add" : "User Add",
        "first_name_required" : "First Name*",
        "last_name_required" : "Last Name*",
        "gender" : "Gender",
        "male" : "Male",
        "female" : "Female",
        "mobile" : "Mobile",
        "address" : "Address",
        "post_code" : "Postcode",
        "user_edit" : "User Edit",
        "tax_name" : "Tax Name",
        "tax_number" : "Tax No",
        "tax %" : "Tax %",
        "display" : "Display",
        "tax_add" : "Tax Add",
        "tax_type" : "Tax Type",
        "charge" : "Charge",
        "tax_+_charge" : "Tax + Charge",
        "start_date" : "Start Date",
        "end_date" : "End Date",
        "is_display" : "Is Display",
        "tax_edit" : "Tax Edit",
        "group" : "Group",
        "added_tax_group" : "Added Tax Group",
        "add" : "Add",
        "no_data" : "No Data",
        "tax_group_edit" : "Tax Group Edit",
        "is_default" : "Is Default",
        "payment_terms_add" : "Payment Terms Add",
        "payment_name" : "Payment Name",
        "payment_terms_edit" : "Payment Terms Edit",
        "country_list" : "Country List",
        "country_name" : "Country Name",
        "country_code" : "Country code",
        "add_country" : "Add Country",
        "state_name" : "State Name",
        "state_code" : "State Code",
        "clear" : "Clear",
        "state_add" : "State Add",
        "city_name" : "City Name",
        "city_code" :  "City Code",
        "city_add" : "City Add",
        "income_category" : "Income Category",
        "expense_category" : "Expense Category",
        "income_category_list" : "Income Category List",
        "income_category_add" : "Income Category Add",
        "customer_code" : "Customer Code",
        "view_/_change" : "View / Change",
        "phone_required" : "Phone*",
        "name_required" : "Name*",
        "discount:" : "Discount:",
        "tax:" : "Tax:",
        "code_required" : "Code*",
        "payment_name_required" : "Payment Name*",
        "income_category_edit" : "Income Category Edit",
        "tax_number_required" : "Tax No*",
        "tax %_required" : "Tax %*",
        "tax_type_required" : "Tax Type*",
        "expense_category_list" : "Expense Category List",
        "expense_category_add" : "Expense Category Add",
        "expense_category_edit" : "Expense Category Edit",
        "back_to_list" : "Back to List",
        "name_required" : "Name*",
        "no_data_found" : "-No Data Found-",
        "city_edit" : "City Edit",
        "state_edit" : "State Edit",
        "edit_country" : "Edit Country",
        "mobile_required" : "Mobile*",
        "email_required" : "Email*",
        "user_role_required" : "User Role*",
        "account_required" : "Account*",
        "date_required" : "Date*",
        "description_required" : "Description*",
        "amount_required" : "Amount*",
        "credit_account_required" : "Credit Account*",
        "debit_account_required" : "Debit Account*",
        "delete" : "Delete",
        "order_date" : "Order Date",
        "due_on_receipts" : "Due on receipts",
        "+3_days" : "+3 days",
        "+5_days" : "+5 days",
        "+7_days" : "+7 days",
        "+10_days" : "+10 days",
        "+15_days" : "+15 days",
        "+30_days" : "+30 days",
        "+45_days" : "+45 days",
        "+60_days" : "+60 days",
        "sales_estimate_edit" : "Sales Estimate Edit",
        "sales_estimate_view" : "Sales Estimate View",
        "order_date:" : "Order Date:",
        "order:" : "Order:",
        "add_new_contact" : "Add New Contact",
        "contact_add" : "Contact Add",
        "company" : "Company",
        "advance_payment" : "Advance Payment",
        "select_category" : "Select Category",
        "customer_name:" : "Customer Name:",
        "payment_date_required" : "Payment Date*",
        "enter_the_amount_required" : "Enter the Amount*",
        "payment_terms_required" : "Payment Terms*",
        "remove_invoice_payment" : "Remove Invoice Payment",
        "are_you_sure_you_want_to_remove_this_invoice_payment?" : "Are you sure you want to remove this invoice payment?",
        "remove" : "Remove",
        "add_new_customer" : "Add New Customer",
        "sales_order_edit" : "Sales Order Edit",
        "order_code" : "Order Code",
        "sales_payment_add" : "Sales Payment Add",
        "sales_payment_edit" : "Sales Payment Edit",
        "purchase_order_add" : "Purchase Order Add",
        "number" : "No",
        "purchase_order_edit" : "Purchase Order Edit",
        "no_results_found" : "No Results Found",
        "created_on:" : "Created on:",
        "last_sent_on:" : "Last Sent on:",
        "paid_amount:" : "Paid Amount:",
        "payment_status:" : "Payment Status:",
        "your_invoice_is_awaiting_payment." : "Your invoice is awaiting payment.",
        "timeline:" : "Timeline:",
        "valid_until" : "Valid Until",
        "purchase_id" : "Purchase ID",
        "purchase_invoice_edit" : "Purchase Invoice Edit",
        "purchase_number:" : "Purchase Number:",
        "payable:" : "Payable:",
        "make_payment" : "Make Payment",
        "payment" : "Payment",
        "choose_advance:" : "Choose Advance:",
        "payment_amount_required" : "Payment Amount*",
        "accounts_required" : "Accounts*",
        "purchase_payment_add" : "Purchase Payment Add",
        "purchase_order_date" : "Purchase Order Date",
        "customer_name_required" : "Customer Name*",
        "sales_invoice_add" : "Sales Invoice Add",
        "SMS_sent_to_(Additional Numbers)" : "SMS Sent To (Additional Numbers)",
        "invoice_number:" : "Invoice Number:",
        "invoice_date:" : "Invoice Date:",
        "total_quantity:" : "Total qty:",
        "received_amount:" : "Received Amount:",
        "payment_mode_required" : "Payment Mode*",
        "remove_payment" : "Remove Payment",
        "download_PDF" : "Download PDF",
        "other_details" : "Other Details",
        "thank_you" : "Thank You",
        "invoice_id" : "Invoice ID",
        "mode_required" : "Mode*",
        "sales_payment_details" : "Sales Payment Details",
        "date:" : "Date:",
        "status:" : "Status:",
        "payment_mode:" : "Payment Mode:",
        "total_amount:" : "Total Amount:",
        "used_amount:" : "Used Amount:",
        "account_name:" : "Account Name:",
        "all_level" : "All Level",
        "customer_edit" : "Customer Edit",
        "add_city" : "Add City",
        "click_to_view_customer_detail" : "Click To View customer Detail",
        "send_a_receipt" : "Send a Receipt",
        "send" : "Send",
        "date_of_birth" : "DOB",
        "zone" : "Zone",
        "all_zone" : "All Zone",
        "shipping_postcode" : "Shipping Postcode",
        "product_list" : "Product List",
        "products" : "Products",
        "services" : "Services",
        "sales_price" : "Sales Price",
        "HSN/legal_code" : "HSN/Legal Code",
        "product_add" : "Product Add",
        "sales_price_required" : "Sales Price*",
        "has_inventory" : "Has Inventory",
        "exclude(Tax & Discount)" : "Exclude(Tax & Discount)",
        "product_edit" : "Product Edit",
        "min_stock_required" : "Min Stock*",
        "tier_pricing" : "Tier Pricing",
        "from_qty" : "From Qty",
        "unit_price" : "Unit Price",
        "to_qty" : "To Qty",
        "upload_photo" : "Upload Photo",
        "file_upload" : "File Upload",
        "product_details" : "Product Details",
        "general_info" : "General Info",
        "pricing" : "Pricing",
        "purchase_history" : "Purchase History",
        "sales_history" : "Sales History",
        "in/out" : "In/Out",
        "serial_number" : "S.No",
        "purchase_code" : "Purchase Code",
        "invoice_code" : " Invoice Code",
        "service_list" : "Service List",
        "service_add" : "Service Add",
        "service_edit" : "Service Edit",
        "paid" : "Paid",
        "view" : "View",
        "unpaid" : "Unpaid",
        "partially_paid" : "Partially paid",
        "bill_to" : "BILL TO",
        "pending_sales_order" : "Pending Sales Order",
        "purchase_refund_add" : "Purchase Refund Add",
        "pending_purchase_order" : "Pending Purchase Order",
        "order_view" : "Order View",
        "create_invoice" : "Create Invoice",
        "select_status" : "-Select Status-",
        "order_number" : "Order Number",
        "balance_due_amount" : "Balance Due Amount",
        "payment_detail" : "Payment Detail",
        "send_remainder" : "Send Remainder",
        "sales_refund_add" : "Sales Refund Add",
        "sales_invoice_edit" : "Sales Invoice Edit",
        "send_SMS" : "Send SMS",
        "none" : "None",
        "invoiced_tax_rate" : "Invoiced Tax Rate",
        "account_number_required" : "Account Number*",
        "open_stock_required" : "Open Stock*",
        "get_customer_label" : "Get Customer Label",
        "customer_prefix" : "Customer Prefix",
        "acode" : "Acode",
        "bill_from" : "Bill From",
        "prefix" : "Prefix",
        "invoice_prefix" : "Invoice Prefix",
        "round_off" : "Round off:",
        "create_refund" : "Create Refund",
        "prefix_required" : "Prefix*",
        "select_prefix" : "Select Prefix",
        "order_prefix" : "Order Prefix",
        "over_due" : "OVERDUE",
        "today_due" : "TODAY DUE",
        "coming_due_within_ 30_days" : "COMING DUE WITHIN 30 DAYS",
        "SMS Send To" : "SMS Send To:",
        "more_than_minimum_stock" : "More than Minimum Stock",
        "less_than_minimum_stock" : "Less than Minimum Stock",
        "exceed_minimum_stock_product_report" : "Exceed Min stock Product Report",
        "add_UOM" : "Add UOM",
        "edit_UOM" : "Edit UOM",
        "remaining_qty":"Remaining Qty",
        "dashboard" : "Dashboard",
        "total_sales_bill" : "TOTAL SALES BILL",
        "total_expense_bill" : "TOTAL EXPENSE BILL ",
        "payment_received" : "PAYMENT RECEIVED",
        "this_month" : "(THIS MONTH)",
        "total_payment_done":"TOTAL PAYMENT DONE",
        "upcoming_week":"UPCOMING WEEK",
        "receivable":"Receivable",
        "income_and_expense_for_last6":"income & Expense for Last 6 month",
        "payable":"Payable",
        "income":"Income ",
        "activities":"ACTIVITIES",
        "month_wise_sale_for_last6":"Month wise Sales for Last 6 Months",
        "view_all":"View all",
        "sales_invoice_and_payment":"sales invoice & payment for 90 days",
        "purchase_invoice_and_payment":"Purchase invoice & payment for 90 days",
        "invoice":"Invoice",
        "paticular":"Paticular",
        "view_more" : "View More",
        "crm":"CRM",
        "stage":"Stage",
        "deal":"Deal",
        "contact_list" : "Contact List",
        "contact_type" : "Contact Type",
        "contact_type_required" : "Contact Type*",
        "back_to_contact_list"  : "Back to Contact List",
        "add_contact" : "Add Contact",
        "edit_contact" : "Edit Contact",
        "create_me_as_a_customer" : "Create Me as a Customer",
        "next_follow_up_date" : "Next Follow-up Date",
        "add_deal" : "Add Deal",
        "deal_id" : "Deal Id",
        "stage_list":"Stage List",
        "stage_name" : "Stage Name",
        "stage_name_req" : "Stage Name*",
        "level_req" : "Level *",
        "add_stage" : "Add Stage",
        "edit_stage" : "Edit Stage",
        "deal_list" : "Deal List",
        "to_add_deal":"To Add deal, go to corresponding contact and check",
        "assigned_to" : "Assigned to",
        "deal_edit" :"Deal Edit",
        "deals" : "Deals",
        "deal_stage_req" : "Deal Stage*",
        "contact_req":"Contact*",
        "close_date" : "Close Date",
        "user_req" : "User*",
        "log_activity":"Log Activity",
        "task" : "Task",
        "estimates":"Estimates",
        "no_activity_found":"-No Activity Found-",
        "list_of_customers_with_this_contact":"List of customers with this contact",
        "create":"Create",
        "this_contact_is_not_a_customer":"This contact is not a customer.Do you want to create customer with this contact ?",
        "please_select_one_customer" : "Please select one customer.",
        "order":"Order",
        "type_req" : "Type*",
        "sub_type":"Sub Type",
        "time_req" : "Time*",
        "type" : "Type",
        "contact_name_req":"Contact Name*",
        "deal_add":"Deal Add",
        "select_log_activity":"Select Log Activity",
        "call":"Call",
        "meeting":"Meeting",
        "sub_call_type":"Select Call Subtype",
        "busy" : "Busy",
        "wrong_number":"Wrong Number",
        "not_reachable":"Not Reachable",
        "attended":"Attended",
        "not_attended":"Not Attended",
        "task_management" : "Task Management",
        "create_task":"Create Task",
        "task_details":"Task Details",
        "task_title":"Task Title*",
        "assign_to_req":"Assign To*",
        "task_type_req":"Task Type*",
        "created_for_req":"Created For*",
        "priority" : "Priority",
        "deal_details" : "Deal Details",
        "task_status":"Task Status",
        "followers":"Followers",
        "journal":"Journal",
        "general_journal" : "General Journal",
        "particulars" : "Particulars",
        "debit" : "Debit",
        "credit" : "Credit",
        "settings" : "Settings",
        "user_role_edit": "User Role Edit",
        "role_name_req" : "Role Name*",
        "comments_req" :"Comments*",
        "screen_access" : "Screen Access",
        "screen_name":"Screen Name",
        "modify":"Modify",
        "reports":"Reports",
        "utilities":"Utilities",
        "help":"Help",
        "photography":"Photography",
        "human_resource" :"Human Resource",
        "change_password" : "Change Password",
        "attribute_list":"Attribute List",
        "attribute":"Attribute",
        "attribute_type" :"Attribute Type",
        "attribute_code":"Attribute Code",
        "attribute_labe1":"Attribute Label1",
        "attribute_add" :"Attribute Add",
        "attribute_code_req":"Attribute Code*",
        "attribute_type_req" :"Attribute Type*",
        "attribute_label_req" :"Attribute Label*",
        "sequence_number" : "Sequence Number",
        "input_type" :"Input Type",
        "default_value": "Default Value",
        "option_value": "Option Value",
        "validation_type": "Validation Type",
        "is_req": "Is Required",
        "has_editable": "Has Editable",
        "tax_enabled":"Tax Enabled",
        "show_in_list":"Show in List",
        "show_in_invoice_print":"Show in Invoice Print",
        "attribute_edit":"Attribute Edit",
        "customer_settings":"Customer Settings",
        "customer_special_date_settings":"Customer Special Date Settings",
        "special_date1":"Special Date 1",
        "special_date2":"Special Date 2",
        "special_date3":"Special Date 3",
        "special_date4":"Special Date 4",
        "special_date5":"Special Date 5",
        "app_settings":"App Settings",
        "time_zone":"Time Zone",
        "date_format":"Date Format",
        "currency_format":"Currency Format",
        "thousand_separator_placement":"Thousand Separator Placement",
        "app_comp_name":"Application Name / Company Name",
        "company_address":"Company Address",
        "logo":"Logo : (Best Fit - 150px * 50px)",
        "background":"Background : (Best Fit - 150px * 50px)",
        "signature":"Signature",
        "sms_balance":"SMS Balance",
        "bank_details":"Bank Details",
        "dual_language_support" : "Dual Language Support",
        "enable_dual_language":"Enable Dual Language",
        "background_upload":"Background Upload",
        "view_or_change":"View/Change",
        "invoice_settings":"Invoice Settings",
        "invoice_prefix":"Invoice Prefix",
        "invoice_from":"Invoice From",
        "invoice_cc":"Invoice CC",
        "invoice_print_format":"Invoice Print Format",
        "invoice_product_column":"Invoice Product Column",
        "invoice_item_measurement":"Invoice Item Measurement",
        "invoice_item_custom_option1":"Invoice Item Custom Option 1",
        "invoice_item_custom_option2":"Invoice Item Custom Option 2",
        "invoice_item_custom_option3":"Invoice Item Custom Option 3",
        "invoice_item_custom_option4":"Invoice Item Custom Option 4",
        "invoice_item_custom_option5":"Invoice Item Custom Option 5",
        "purchase_product_column":"Purchase Product Column",
        "purchase_invoice_item_custom1":"Purchase Invoice Item Custom Option 1",
        "purchase_invoice_item_custom2":"Purchase Invoice Item Custom Option 2",
        "purchase_invoice_item_custom3":"Purchase Invoice Item Custom Option 3",
        "purchase_invoice_item_custom4":"Purchase Invoice Item Custom Option 4",
        "purchase_invoice_item_custom5":"Purchase Invoice Item Custom Option 5",
        "purchase_invoice_item_measurement":"Purchase Invoice Item Measurement",
        "full_page":"Full Page",
        "half_page":"Half Page",
        "tally_style":"Tally Style",
        "tally_style2":"Tally Style 2",
        "tally_style3":"Tally Style 3",
        "prefix_settings":"Prefix Settings",
        "purchase_estimate":"Purchase Estimate",
        "sms_settings":"SMS Settings",
        "sms_template":"SMS Template",
        "template_name":"Template Name",
        "subject":"Subject",
        "sms_gateway_url":"SMS Gateway Url",
        "email_settings":"Email Settings",
        "email_template":"Email Template",
        "suggestions":"Suggestions",
        "help_manual":"Help Manual",
        "report_bug":"Report Bug",
        "sales_report":"Sales Report",
        "itemized_sales_report":"Itemized Sales Report",
        "with_prefix":"With Prefix",
        "with_out_prefix":"Without Prefix",
        "show_details":"Show Details",
        "from_date:":"From Date:",
        "to_date":"To Date: ",
        "type:" : "Type:",
        "receivable_report":"Receivable Report",
        "over_due":"Over Due",
        "payable_report":"Payable Report",
        "transaction_date":"Transaction Date",
        "narration":"Narration",
        "source_type":"Source Type",
        "partywise_sales_statement":"Partywise Sales Statement",
        "opening_balance":"Opening Balance",
        "partywise_purchase_statement":"Partywise Purchase Statement",
        "voucher_number":"Voucher Number",
        "voucher_type":"Voucher Type",
        "account_statement":"Account Statement",
        "tax_report":"Tax Report",
        "si_no":"S.No",
        "tax_name:" : "Tax Name:",
        "tax:" : "Tax:",
        "tax_summary_report":"Tax Summary Report",
        "tax_summary_report":"Tax Summary Report",
        "expense_report":"Expense Report",
        "user_name:" : "User Name:",
        "user":"User",
        "area_wise_sales_order_report":"Area Wise Sales Order Report",
        "special_date_report":"Special Date Report",
        "select_date":"Select Date",
        "event":"Event",
        "birthday":"Birthday",
        "activity_log":"Activity Log",
        "database_backup":"Database Backup",
        "no_activity_found":"-No Activity Found-",
        "you_can_download_sql":"You can download SQL queries on the current database",
        "photography":"Photography",
        "album":"Album",
        "select_type":"Select Type",
        "public":"Public",
        "private":"Private",
        "selected":"Selected",
        "send_email":"Send Email",
        "drop_folder_here":"Drop Folder Here",
        "drop_files_here":"Drop Files Here",
        "copy":"Copy",
        "permission":"Permission",
        "upload_failed":"Upload Failed..",
        "no_pending_files" :"No Pending Files",
        "album_add":"Album Add",
        "change_password" :"Change Password*",
        "new_password":"New Password*",
        "confrim_password":"Confrim Password*",
        "back_to_user_list":"back to User List",
        "test" :"Test",
        "point_of_sales":"POS",
        "payment_receipt":"Payment Receipt",
        "purchase_invoice_return":"Purchase Invoice Return"
    }