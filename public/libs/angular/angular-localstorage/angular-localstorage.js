angular.module('angularLocalstorage', []);
angular.module('angularLocalstorage')
        .directive('angularLocalstorage', ['$timeout', '$rootScope', '$localStorage', '$state',
            function ($timeout, $rootScope, $localStorage, $state) {
                return {
                    restrict: 'A',
                    link: function (scope, element, attrs, fn) {

                        console.log('angularLocalstorage link methode');
                        // This is the DOM form element
                        var formElement = angular.element(element)[0];
                        var localStorageFiledcount = 0;

                        // Intercept and handle submit events of the form
                        for (var i = 0; i < formElement.length; i++) {
                            // This ensures we are only watching form fields
                            if (i in formElement) {
                                var isLocalStorageField = false;

                                var element = angular.element(formElement[i]);
                                var localStorageArrributeValue = $(element).attr('angular-localstorage-field');
                                console.log('localStorageArrributeValue = ' + localStorageArrributeValue);
                                if (localStorageArrributeValue)
                                {
                                    localStorageFiledcount++;
                                }
                            }
                        }
                        console.log('localStorageFiledcount = ' + localStorageFiledcount);
                        var eventObj = {};
                        eventObj.formName = formElement.name;
                        eventObj.fieldCount = localStorageFiledcount;

                        scope.$broadcast('onLocalStorageFormRenterCompleteEvent', eventObj);

                    }
                };
            }])
        .directive('angularLocalstorageField', ['$timeout', '$rootScope', '$localStorage', '$state', function ($timeout, $rootScope, $localStorage) {
                return {
                    restrict: 'A',
                    replace: true,
                    require: 'ngModel',
                    ngModel: '=',
                    typeaheadInputFormatter: '=',
                    link: function (scope, element, attrs, ngModel) {

                        var formElement = null;
                        var localStorageFieldName = "";
                        var DOMForm = null;
                        var ngForm = null;
                        var scopeForm = null;


                        function initAngularLocalStorageField()
                        {
                            // var form =
                            // This is the DOM form element
                            formElement = angular.element(element)[0];
                            var parentForm = $(formElement).parents('form');
                            DOMForm = angular.element(parentForm)[0];

                            var formName = DOMForm.name;
                            var fieldName = $(formElement).attr('name');
                            if (formName != '' && fieldName != '')
                            {
                                localStorageFieldName = formName + '-' + fieldName;
                                console.log('localStorageFieldName = ' + localStorageFieldName);
                                angular.element(formElement).attr('ls-fieldname', localStorageFieldName);
                                angular.element(formElement).attr('fname', formName);
                                scope.$broadcast('onLocalStorageFieldRenterCompleteEvent', formName);
                            }
                        }

                        $timeout(initAngularLocalStorageField, 0);

                        scope.$on('initLocalStorageFieldRetrieveEvent', function (event, formName) {

                            retrieveLocalstorageValue(formName, ngModel);
                            console.log("initLocalStorageFieldRetrieveEvent " + formName);
                        });

                        function retrieveLocalstorageValue(formName, ngModelController)
                        {

                            var localStorageFieldName = $(formElement).attr('ls-fieldname');
                            var parentFormName = $(formElement).attr('fname');
                            console.log('retrieveLocalstorageValue = ' + localStorageFieldName);
                            console.log('retrieveLocalstorageValue formName = ' + formName);
                            if (parentFormName == formName)
                            {
                                var angularElement = angular.element(formElement);
                                var isTypeAheadField = $(angularElement).attr('uib-typeahead') != undefined ? true : false;

                                if (typeof $localStorage[localStorageFieldName] != 'undefined')
                                {
                                    if (isTypeAheadField)
                                    {
                                        var eveObj = {};
                                        eveObj.fieldName = localStorageFieldName;
                                        eveObj.value = $localStorage[localStorageFieldName];
                                        scope.$broadcast('updateTypeaheadFieldValue', eveObj);
                                    } else
                                    {
                                        console.log($localStorage[localStorageFieldName]);
                                        //formElement.value = $localStorage[localStorageFieldName];
                                        ngModelController.$setViewValue($localStorage[localStorageFieldName]);
                                        // scope.ngModel = $localStorage[localStorageFieldName];
                                        ngModelController.$render();
                                        console.log('localstorage value ' + scope.ngModel);
                                    }
                                }
                                scope.$broadcast('onLocalStorageFieldRetrieveCompleteEvent', formName);
                                setupWatch(formElement);

                            }

                        }
                        // Setup $watch on a single formfield
                        function setupWatch(elementToWatch) {

                            scope.$watch(function () {
                                return elementToWatch.value;
                            },
                                    function (newValue) {
                                        updateLocalstorageValue(elementToWatch);
                                    });
                        }

                        function updateLocalstorageValue(elementToWatch)
                        {

                            var localStorageFieldName = $(elementToWatch).attr('ls-fieldname');
                            var isDateField = $(elementToWatch).attr('uib-datepicker-popup') != undefined ? true : false;
                            if (isDateField)
                            {
                                $localStorage[localStorageFieldName] = formElement.value;
                            } else if (localStorageFieldName != '')
                            {
                                $localStorage[localStorageFieldName] = ngModel.$modelValue;
                            }
                            //$localStorage[key] = JSON.stringify(value);
                            //console.log(elementToWatch);
                        }
                    }
                }
            }])
        