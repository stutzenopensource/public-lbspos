angular.module('angularValidator', []);
angular.module('angularValidator').directive('angularValidator',
        function() {
            return {
                restrict: 'A',
                link: function(scope, element, attrs, fn) {

                    // This is the DOM form element
                    var DOMForm = angular.element(element)[0];
                    //console.log(DOMForm);
                    //console.log(DOMForm.name);
                    // This is the the scope form model
                    // All validation states are containted here
                    var scopeForm = scope[DOMForm.name];
                    // Set the default submitted state to false
                    scopeForm.submitted = false;
                    // Intercept and handle submit events of the form

                    element.on('submit', function(event) {
                        event.preventDefault();

                        scope.$apply(function() {
                            scopeForm.submitted = true;
                        });
                        console.log('msg in form submit');
                        console.log(scopeForm.$valid);
                        // If the form is valid then call the function that is declared in the angular-validator-submit atrribute on the form element
                        if (scopeForm.$valid) {
                            scope.$eval(DOMForm.attributes["angular-validator-submit"].value);
                        }
                    });
                    // Setup watches on all form fields
                    setupWatches(DOMForm, DOMForm.name);
                    // Iterate through the form fields and setup watches on each one
                    function setupWatches(formElement, formName) {

                        for (var i = 0; i < formElement.length; i++) {
                            // This ensures we are only watching form fields
                            if (i in formElement) {
                                angular.element(formElement[i]).attr('fname', formName);
                                setupWatch(formElement[i]);
                            }
                        }
                    }


                    // Setup $watch on a single formfield
                    function setupWatch(elementToWatch) {


                        scope.$watch(function() {
                            // We are watching both the value of the element, the value of form.submitted, the validity of the element and the $dirty property of the element
                            // We need to watch $dirty becuase angular will somtimes run $dirty checking after the watch functions have fired the first time.
                            // Adding the four items together is a bit of a trick
                            return elementToWatch.value + scopeForm.$submitted + checkElementValididty(elementToWatch) + getDirtyValue(scopeForm[elementToWatch.name]);
                        },
                                function() {
                                    //updateValidationMessage(elementToWatch);
                                    updateValidationClass(elementToWatch);
                                });
                    }


                    // Returns the $dirty value of the element if it exists
                    function getDirtyValue(element) {
                        if (element) {
                            if ("$dirty" in element) {
                                return element.$dirty;
                            }
                        }
                    }



                    function checkElementValididty(element) {
                        // If element has a custom validation function
                        if ("validator" in element.attributes) {
                            // Call the custom validator function
                            var isElementValid = scope.$eval(element.attributes.validator.value);
                            scopeForm[element.name].$setValidity("angularValidator", isElementValid);
                            return isElementValid;
                        }
                    }


                    // Adds and removes an error message as a sibling element of the form field
                    // depending on the validity of the form field and the submitted state of the form.
                    // Will use default message if a custom message is not given
                    function updateValidationMessage(element) {

                        var defaultRequiredMessage = function() {
                            return "<i class='fa fa-times'></i> Required";
                        };
                        var defaultInvalidMessage = function() {
                            return "<i class='fa fa-times'></i> Invalid";
                        };
                        // Make sure the element is a form field and not a button for example
                        // Only form elements should have names.
                        if (!(element.name in scopeForm)) {
                            return;
                        }

                        var scopeElementModel = scopeForm[element.name];
                        // Only add/remove validation messages if the form field is $dirty or the form has been submitted
                        if (scopeElementModel.$dirty || scope[element.form.name].submitted) {

                            // Remove all validation messages
                            var validationMessageElement = isValidationMessagePresent(element);
                            if (validationMessageElement) {
                                validationMessageElement.remove();
                            }

                            if (scopeElementModel.$error.required) {
                                // If there is a custom required message display it
                                if ("required-message" in element.attributes) {
                                    angular.element(element).after(generateErrorMessage(element.attributes['required-message'].value));
                                }
                                // Display the default require message
                                else {
                                    angular.element(element).after(generateErrorMessage(defaultRequiredMessage));
                                }
                            } else if (!scopeElementModel.$valid) {
                                // If there is a custom validation message add it
                                if ("invalid-message" in element.attributes) {
                                    angular.element(element).after(generateErrorMessage(element.attributes['invalid-message'].value));
                                }
                                // Display the default error message
                                else {
                                    angular.element(element).after(generateErrorMessage(defaultInvalidMessage));
                                }
                            }
                        }
                    }


                    function generateErrorMessage(messageText) {
                        //return "<label class='control-label has-error validationMessage'>" + scope.$eval(messageText) + "</label>";
                    }


                    // Returns the validation message element or False
                    function isValidationMessagePresent(element) {
                        var elementSiblings = angular.element(element).parent().children();
                        for (var i = 0; i < elementSiblings.length; i++) {
                            if (angular.element(elementSiblings[i]).hasClass("validationMessage")) {
                                return angular.element(elementSiblings[i]);
                            }
                        }
                        return false;
                    }


                    // Adds and removes .has-error class to both the form element and the form element's parent
                    // depending on the validity of the element and the submitted state of the form
                    function updateValidationClass(element) {

                        // Make sure the element is a form field and not a button for example
                        // Only form fields should have names.
                        if (!(element.name in scopeForm)) {
                            return;
                        }
                        var formField = scopeForm[element.name];
                        var fieldFName = angular.element(element).attr("fname");

                        if (!scopeForm.$submitted && scopeForm.$name == fieldFName && formField.$dirty != undefined && !formField.$dirty)
                        {
                            angular.element(element.parentNode).removeClass('has-error');
                            angular.element(element).removeClass('has-error');
                            return;
                        }

                        // Only add/remove validation classes if the field is $dirty or the form has been submitted
                        if (formField.$dirty || scopeForm.$submitted) {

                            if (formField.$valid) {
                                angular.element(element.parentNode).removeClass('has-error');
                                // This is extra for users wishing to implement the .has-error class on the field itself
                                // instead of on the parent element. Note that Bootstrap requires the .has-error class to be on the parent element
                                angular.element(element).removeClass('has-error');
                            } else if (formField.$invalid) {
                                angular.element(element.parentNode).addClass('has-error');
                                // This is extra for users wishing to implement the .has-error class on the field itself
                                // instead of on the parent element. Note that Bootstrap requires the .has-error class to be on the parent element
                                angular.element(element).addClass('has-error');
                            }
                        }
                    }

                }
            };
        }
)
        .directive('angularValidatorField', ['$timeout',
            function($timeout) {
                return {
                    restrict: 'A',
                    link: function(scope, element, attrs, fn) {

                        var formElement = null;
                        var DOMForm = null;
                        var ngForm = null;
                        var scopeForm = null;

                        function initAngularFormFieldValidator()
                        {
                            // var form =
                            // This is the DOM form element
                            formElement = angular.element(element)[0];
                            // This is the DOM form element
                            var parentForm = $(formElement).parents('form');
                            var ngFormName = $(parentForm).attr('name');
                            DOMForm = angular.element(parentForm)[0];
                            ngForm = scope[ngFormName];
                            // This is the the scope form model
                            // All validation states are containted here
                            scopeForm = scope[DOMForm.name];
                            // Setup watches on all form fields
                            for (var i = 0; i < DOMForm.length; i++) {
                                // This ensures we are watching correct form fields
                                if (formElement.name === DOMForm[i].name) {

                                    angular.element(DOMForm[i]).attr('fname', DOMForm.name);
                                    setupWatch(DOMForm[i]);
                                }
                            }

                        }

                        /*
                         *  Calling init function at timeout
                         *
                         *  To ensure observer all attr including {{$index}} issue
                         */
                        $timeout(initAngularFormFieldValidator, 0);

                        // Setup $watch on a single formfield
                        function setupWatch(elementToWatch) {
                            scope.$watch(function() {
                                // We are watching both the value of the element, the value of form.submitted, the validity of the element and the $dirty property of the element
                                // We need to watch $dirty becuase angular will somtimes run $dirty checking after the watch functions have fired the first time.
                                // Adding the four items together is a bit of a trick
                                return elementToWatch.value + ngForm.$submitted + checkElementValididty(elementToWatch) + getDirtyValue(ngForm[elementToWatch.name]);
                                //return elementToWatch.value + scopeForm.submitted + checkElementValididty(elementToWatch);
                            },
                                    function() {
                                        //updateValidationMessage(elementToWatch);
                                        updateValidationClass(elementToWatch);
                                    });
                        }


                        // Returns the $dirty value of the element if it exists
                        function getDirtyValue(element) {
                            if (element) {
                                if ("$dirty" in element) {
                                    return element.$dirty;
                                }
                            }
                        }



                        function checkElementValididty(element) {
                            // If element has a custom validation function
                            if ("validator" in element.attributes) {
                                // Call the custom validator function
                                var isElementValid = scope.$eval(element.attributes.validator.value);
                                ngForm[element.name].$setValidity("angularValidator", isElementValid);
                                //scopeForm[element.name].$setValidity("angularValidator", isElementValid);
                                return isElementValid;
                            }
                        }


                        // Adds and removes an error message as a sibling element of the form field
                        // depending on the validity of the form field and the submitted state of the form.
                        // Will use default message if a custom message is not given
                        function updateValidationMessage(element) {

                            var defaultRequiredMessage = function() {
                                return "<i class='fa fa-times'></i> Required";
                            };
                            var defaultInvalidMessage = function() {
                                return "<i class='fa fa-times'></i> Invalid";
                            };
                            // Make sure the element is a form field and not a button for example
                            // Only form elements should have names.
                            if (!(element.name in ngForm)) {
                                return;
                            }

                            var scopeElementModel = ngForm[element.name];
                            // Only add/remove validation messages if the form field is $dirty or the form has been submitted
                            if (scopeElementModel.$dirty || scope[element.form.name].submitted) {

                                // Remove all validation messages
                                var validationMessageElement = isValidationMessagePresent(element);
                                if (validationMessageElement) {
                                    validationMessageElement.remove();
                                }

                                if (scopeElementModel.$error.required) {
                                    // If there is a custom required message display it
                                    if ("required-message" in element.attributes) {
                                        angular.element(element).after(generateErrorMessage(element.attributes['required-message'].value));
                                    }
                                    // Display the default require message
                                    else {
                                        angular.element(element).after(generateErrorMessage(defaultRequiredMessage));
                                    }
                                } else if (!scopeElementModel.$valid) {
                                    // If there is a custom validation message add it
                                    if ("invalid-message" in element.attributes) {
                                        angular.element(element).after(generateErrorMessage(element.attributes['invalid-message'].value));
                                    }
                                    // Display the default error message
                                    else {
                                        angular.element(element).after(generateErrorMessage(defaultInvalidMessage));
                                    }
                                }
                            }
                        }


                        function generateErrorMessage(messageText) {
                            //return "<label class='control-label has-error validationMessage'>" + scope.$eval(messageText) + "</label>";
                        }


                        // Returns the validation message element or False
                        function isValidationMessagePresent(element) {
                            var elementSiblings = angular.element(element).parent().children();
                            for (var i = 0; i < elementSiblings.length; i++) {
                                if (angular.element(elementSiblings[i]).hasClass("validationMessage")) {
                                    return angular.element(elementSiblings[i]);
                                }
                            }
                            return false;
                        }


                        // Adds and removes .has-error class to both the form element and the form element's parent
                        // depending on the validity of the element and the submitted state of the form
                        function updateValidationClass(element) {

                            var formField = ngForm[element.name];
                            var fieldFName = angular.element(element).attr("fname");
//                            console.log('fieldFName = '+ fieldFName );
//                            console.log('ngForm.$name = '+ ngForm.$name );
//                            console.log('ngForm');
//                            console.log(ngForm);
//                            console.log('formField' );
//                            console.log(formField);
                            // Only add/remove validation classes if the field is $dirty or the form has been submitted
                            if (!ngForm.$submitted && ngForm.$name == fieldFName && formField.$dirty != undefined && !formField.$dirty)
                            {
                                angular.element(element.parentNode).removeClass('has-error');
                                angular.element(element).removeClass('has-error');
                                return;
                            }
                            if (formField.$dirty || ngForm.$submitted)
                            {
                                if (formField.$valid) {
                                    angular.element(element.parentNode).removeClass('has-error');
                                    // This is extra for users wishing to implement the .has-error class on the field itself
                                    // instead of on the parent element. Note that Bootstrap requires the .has-error class to be on the parent element
                                    angular.element(element).removeClass('has-error');
                                } else if (formField.$invalid) {
                                    angular.element(element.parentNode).addClass('has-error');
                                    // This is extra for users wishing to implement the .has-error class on the field itself
                                    // instead of on the parent element. Note that Bootstrap requires the .has-error class to be on the parent element
                                    angular.element(element).addClass('has-error');
                                }
                            }

                        }

                    }
                };
            }
        ])
        .directive('angularValidatorIsolatedField', ['$timeout',
            function($timeout) {
                return {
                    restrict: 'A',
                    link: function(scope, element, attrs, fn) {

                        var formElement = null;
                        var DOMForm = null;
                        var ngForm = null;
                        var scopeForm = null;

                        function initAngularFormFieldValidator()
                        {
                            // var form =
                            // This is the DOM form element
                            formElement = angular.element(element)[0];
                            // This is the DOM form element
                            var parentForm = $(formElement).parents('form');
                            var ngFormName = $(parentForm).attr('name');
                            DOMForm = angular.element(parentForm)[0];
                            
                            //Un listen the watch methode for Non form field. 
                            if(scope.$parent.$parent[ngFormName] == undefined)
                            {   
                                return;
                            }   
                            ngForm = scope.$parent.$parent[ngFormName];
                            // This is the the scope form model
                            // All validation states are containted here
                            scopeForm = scope[DOMForm.name];
                            // Setup watches on all form fields
                            for (var i = 0; i < DOMForm.length; i++) {
                                // This ensures we are watching correct form fields
                                if (formElement.name === DOMForm[i].name) {

                                    angular.element(DOMForm[i]).attr('fname', DOMForm.name);
                                    setupWatch(DOMForm[i]);
                                }
                            }

                        }

                        /*
                         *  Calling init function at timeout
                         *
                         *  To ensure observer all attr including {{$index}} issue
                         */
                        $timeout(initAngularFormFieldValidator, 0);

                        // Setup $watch on a single formfield
                        function setupWatch(elementToWatch) {
                            scope.$watch(function() {
                                // We are watching both the value of the element, the value of form.submitted, the validity of the element and the $dirty property of the element
                                // We need to watch $dirty becuase angular will somtimes run $dirty checking after the watch functions have fired the first time.
                                // Adding the four items together is a bit of a trick
                                return elementToWatch.value + ngForm.$submitted + checkElementValididty(elementToWatch) + getDirtyValue(ngForm[elementToWatch.name]);
                                //return elementToWatch.value + scopeForm.submitted + checkElementValididty(elementToWatch);
                            },
                                    function() {
                                        //updateValidationMessage(elementToWatch);
                                        updateValidationClass(elementToWatch);
                                    });
                        }


                        // Returns the $dirty value of the element if it exists
                        function getDirtyValue(element) {
                            if (element) {
                                if ("$dirty" in element) {
                                    return element.$dirty;
                                }
                            }
                        }



                        function checkElementValididty(element) {
                            // If element has a custom validation function
                            if ("validator" in element.attributes) {
                                // Call the custom validator function
                                var isElementValid = scope.$eval(element.attributes.validator.value);
                                ngForm[element.name].$setValidity("angularValidator", isElementValid);
                                //scopeForm[element.name].$setValidity("angularValidator", isElementValid);
                                return isElementValid;
                            }
                        }


                        // Adds and removes an error message as a sibling element of the form field
                        // depending on the validity of the form field and the submitted state of the form.
                        // Will use default message if a custom message is not given
                        function updateValidationMessage(element) {

                            var defaultRequiredMessage = function() {
                                return "<i class='fa fa-times'></i> Required";
                            };
                            var defaultInvalidMessage = function() {
                                return "<i class='fa fa-times'></i> Invalid";
                            };
                            // Make sure the element is a form field and not a button for example
                            // Only form elements should have names.
                            if (!(element.name in ngForm)) {
                                return;
                            }

                            var scopeElementModel = ngForm[element.name];
                            // Only add/remove validation messages if the form field is $dirty or the form has been submitted
                            if (scopeElementModel.$dirty || scope[element.form.name].submitted) {

                                // Remove all validation messages
                                var validationMessageElement = isValidationMessagePresent(element);
                                if (validationMessageElement) {
                                    validationMessageElement.remove();
                                }

                                if (scopeElementModel.$error.required) {
                                    // If there is a custom required message display it
                                    if ("required-message" in element.attributes) {
                                        angular.element(element).after(generateErrorMessage(element.attributes['required-message'].value));
                                    }
                                    // Display the default require message
                                    else {
                                        angular.element(element).after(generateErrorMessage(defaultRequiredMessage));
                                    }
                                } else if (!scopeElementModel.$valid) {
                                    // If there is a custom validation message add it
                                    if ("invalid-message" in element.attributes) {
                                        angular.element(element).after(generateErrorMessage(element.attributes['invalid-message'].value));
                                    }
                                    // Display the default error message
                                    else {
                                        angular.element(element).after(generateErrorMessage(defaultInvalidMessage));
                                    }
                                }
                            }
                        }


                        function generateErrorMessage(messageText) {
                            //return "<label class='control-label has-error validationMessage'>" + scope.$eval(messageText) + "</label>";
                        }


                        // Returns the validation message element or False
                        function isValidationMessagePresent(element) {
                            var elementSiblings = angular.element(element).parent().children();
                            for (var i = 0; i < elementSiblings.length; i++) {
                                if (angular.element(elementSiblings[i]).hasClass("validationMessage")) {
                                    return angular.element(elementSiblings[i]);
                                }
                            }
                            return false;
                        }


                        // Adds and removes .has-error class to both the form element and the form element's parent
                        // depending on the validity of the element and the submitted state of the form
                        function updateValidationClass(element) {

                            var formField = ngForm[element.name];
                            var fieldFName = angular.element(element).attr("fname");
//                            console.log('fieldFName = '+ fieldFName );
//                            console.log('ngForm.$name = '+ ngForm.$name );
//                            console.log('ngForm');
//                            console.log(ngForm);
//                            console.log('formField' );
//                            console.log(formField);
                            // Only add/remove validation classes if the field is $dirty or the form has been submitted
                            if (!ngForm.$submitted && ngForm.$name == fieldFName && formField.$dirty != undefined && !formField.$dirty)
                            {
                                angular.element(element.parentNode).removeClass('has-error');
                                angular.element(element).removeClass('has-error');
                                return;
                            }
                            if (formField.$dirty || ngForm.$submitted)
                            {
                                if (formField.$valid) {
                                    angular.element(element.parentNode).removeClass('has-error');
                                    // This is extra for users wishing to implement the .has-error class on the field itself
                                    // instead of on the parent element. Note that Bootstrap requires the .has-error class to be on the parent element
                                    angular.element(element).removeClass('has-error');
                                } else if (formField.$invalid) {
                                    angular.element(element.parentNode).addClass('has-error');
                                    // This is extra for users wishing to implement the .has-error class on the field itself
                                    // instead of on the parent element. Note that Bootstrap requires the .has-error class to be on the parent element
                                    angular.element(element).addClass('has-error');
                                }
                            }

                        }

                    }
                };
            }
        ])
        .directive('angularValidatorFormReset', ['$timeout',
            function($timeout) {
                return {
                    restrict: 'A',
                    link: function(scope, element, attrs, fn) {

                        var formElement = null;
                        var DOMForm = null;
                        var ngForm = null;
                        var scopeForm = null;

                        function initAngularFormFieldValidator()
                        {
                            // var form =
                            // This is the DOM form element
                            formElement = angular.element(element)[0];
                            // This is the DOM form element
                            var parentForm = $(formElement).parents('form');
                            var ngFormName = $(parentForm).attr('name');
                            DOMForm = angular.element(parentForm)[0];
                            ngForm = scope[ngFormName];

                            element.on('click', function(event) {
//                                event.preventDefault();
//
//                                scope.$apply(function() {
//                                    scopeForm.submitted = true;
//                                });
//                                console.log('msg in form submit');
//                                console.log(scopeForm.$valid);
//                                // If the form is valid then call the function that is declared in the angular-validator-submit atrribute on the form element
//                                if (scopeForm.$valid) {
//                                    scope.$eval(DOMForm.attributes["angular-validator-submit"].value);
//                                }
                                if (ngForm != undefined && ngForm.$setPristine != undefined)
                                {
                                    $timeout(function() {
                                        ngForm.$setPristine();
                                    }, 0);
                                }
                            });

                        }

                        /*
                         *  Calling init function at timeout
                         *
                         *  To ensure observer all attr including {{$index}} issue
                         */
                        $timeout(initAngularFormFieldValidator, 0);



                    }
                };
            }
        ]);