






app.controller('loginCtrl', ['$scope', '$rootScope', '$state', '$cookies', '$location', 'loginService', 'Auth', 'ValidationFactory','$window', function($scope, $rootScope, $state, $cookies, $location, loginService, Auth, ValidationFactory,$window) {

    $scope.validationFactory = ValidationFactory;
    //Tabs select
    $scope.tab = 1;
    $scope.setTab = function(newTab) {
        $scope.tab = newTab;
    };

    $scope.isSet = function(tabNum) {
        return $scope.tab === tabNum;
    };


    $scope.username = '';
    $scope.password = '';

    $scope.isDataProcessing = false;

    $scope.login = function() {

        var loginParam = {};
        loginParam.username = $scope.username;
        loginParam.password = $scope.password;
        loginParam.fromDevice = "browser";
        //loginParam.remember-me = $scope.password;
        $scope.isDataProcessing = true;
            
        loginService.validateUserLogin(loginParam).then(function(response) {
            console.log("login ");
            console.log(response.data);
            var data = response.data;

            if (data.success == true)
            {
                /*
                     if (data.data.isVerify === false)
                     {
                     loginService.logout();
                     $rootScope.$broadcast('genericErrorEvent', 'Incompleted Account Verification.');
                     return;
                     }
                     */
                console.log("data");
                console.log(data.data);
                console.log("api-token",data['api-token']);                    
                    
                //cookies set
                var cookieOption = {
                    path: "/"
                };
                $cookies.put('api-token', data['api-token'], cookieOption);
                //admin url redirection
                var landingUrl = $window.location.protocol + "//" + $window.location.host+$rootScope.CONTEXT_ROOT +'/admin';
                $window.location.href = landingUrl;
            }
            $scope.isDataProcessing = false;
        });

    };

}]);




