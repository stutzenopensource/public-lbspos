
app.constant('APP_CONST', {
    USER_INFO_SYNC_API: "user/userInfo",
    LOGOUT_API_URL: "/swf/logout",
    API: {
        COMPANY_SAVE: '/lbs/public/company/save'
    },
    BLOCKER_MSG: {
        PAGE_NAVIGATE: "You will lose unsaved changes if you leave this page",
        PAGE_RELOAD: "You will lose unsaved changes if you reload this page"
    }



});