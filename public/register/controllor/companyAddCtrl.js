





app.controller('companyAddCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$timeout', '$filter', 'Auth', 'ValidationFactory', '$state','$window','$cookies', function($scope, $rootScope, adminService, utilityService, $timeout, $filter, Auth, ValidationFactory, $state, $window,$cookies) {
    
    
    $scope.companyAddModel = {
        "id": 0,
        "name": "",
        "fname":"",
        "businessType": "",
        "phoneNumber": "",
        "email": "",            
        "city": "",
        "country": "",
        "address": "",
        "state": "",
        "postcode": "",
        "userName": "",
        "password": "",
        showPassword: false

    }

    $scope.validationFactory = ValidationFactory;




    $scope.getNavigationBlockMsg = function(pageReload)
    {
        if (typeof $scope.create_company_form != 'undefined' && typeof $scope.create_company_form.$pristine != 'undefined' && !$scope.create_company_form.$pristine)
        {
            return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
        }

        return "";
    }
    $scope.isDataSavingProcess = false;

    $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

    $scope.formReset = function() { 

        $scope.create_company_form.$setPristine();
        $scope.companyAddModel = {
            "id": 0,
            "name": "",
            "fname":"",
            "businessType": "",
            "email": "",
            "phoneNumber": "",
            "city": "",
            "country": "",
            "state": "",
            "postcode": "",
            "address": "",
            "userName": "",
            "password": "",
            showPassword: false

        };

    }

    $scope.companySave = function() {

        $scope.isDataSavingProcess = true;
        var createCompanyParam = {};
        createCompanyParam.id = 0;
        createCompanyParam.name = $scope.companyAddModel.name;
        createCompanyParam.fName = $scope.companyAddModel.fname;
        createCompanyParam.businessType = $scope.companyAddModel.businessType;
        createCompanyParam.phoneNumber = $scope.companyAddModel.mobile;
        createCompanyParam.email = $scope.companyAddModel.email;
        createCompanyParam.city = $scope.companyAddModel.city;
        createCompanyParam.country = $scope.companyAddModel.country;
        createCompanyParam.address = $scope.companyAddModel.address;
        createCompanyParam.state = $scope.companyAddModel.state;
        createCompanyParam.userName = $scope.companyAddModel.userName;
        createCompanyParam.password = $scope.companyAddModel.password;
        createCompanyParam.postcode = $scope.companyAddModel.postcode;
            
        adminService.createCompany(createCompanyParam).then(function(response) {
            if (response.data.success == true)
            {
                $scope.formReset();
                var landingUrl = $window.location.protocol + "//" + $window.location.host+$rootScope.CONTEXT_ROOT+'/login';
                $window.location.href = landingUrl;
            }
            $scope.isDataSavingProcess = false;

        });
    };
}]);




