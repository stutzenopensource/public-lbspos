<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Webpatser\\Uuid' => array($vendorDir . '/webpatser/laravel-uuid/src'),
    'Smtpapi' => array($vendorDir . '/sendgrid/smtpapi/lib'),
    'SendGrid' => array($vendorDir . '/sendgrid/sendgrid/lib'),
    'Prophecy\\' => array($vendorDir . '/phpspec/prophecy/src'),
    'Mockery' => array($vendorDir . '/mockery/mockery/library'),
    'Guzzle\\Tests' => array($vendorDir . '/guzzle/guzzle/tests'),
    'Guzzle' => array($vendorDir . '/guzzle/guzzle/src'),
    'Doctrine\\Common\\Inflector\\' => array($vendorDir . '/doctrine/inflector/lib'),
);
